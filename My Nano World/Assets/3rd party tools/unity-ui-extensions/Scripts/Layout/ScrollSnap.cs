/// Credit BinaryX 
/// Sourced from - http://forum.unity3d.com/threads/scripts-useful-4-6-scripts-collection.264161/page-2#post-1945602
/// Updated by ddreaper - removed dependency on a custom ScrollRect script. Now implements drag interfaces and standard Scroll Rect.
/// Update by xesenix - rewrote almost the entire code 
/// - configuration for direction move instead of 2 concurrent class (easier to change direction in editor)
/// - supports list layout with horizontal or vertical layout need to match direction with type of layout used
/// - dynamic checks if scrolled list size changes and recalculates anchor positions 
///   and item size based on itemsVisibleAtOnce and size of root container
///   if you don't wish to use this auto resize turn of autoLayoutItems
/// - fixed current page made it independent from pivot
/// - replaced pagination with delegate function
using System;
using System.Collections;
using System.Linq;
using DG.Tweening;
using UnityEngine.EventSystems;

namespace UnityEngine.UI.Extensions
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(ScrollRect))]
    [AddComponentMenu("UI/Extensions/Scroll Snap")]
    public class ScrollSnap : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
    {
        // needed because of reversed behaviour of axis Y compared to X
        // (positions of children lower in children list in horizontal directions grows when in vertical it gets smaller)
        public enum ScrollDirection
        {
            Horizontal,
            Vertical
        }

        public delegate void PageSnapChange(int page);

        public event PageSnapChange onPageChange;

        public ScrollDirection direction = ScrollDirection.Horizontal;

        protected ScrollRect scrollRect;

        protected RectTransform scrollRectTransform;

        protected Transform listContainerTransform;

        protected int pageCount;

        protected Vector2[] pageNormalizedPositions;

        protected Vector2 lerpNormalizedTarget;

        protected bool lerp;

        protected RectTransform listContainerRectTransform;

        protected int cachedChildCount = 0;

        protected float itemSize;

        protected int itemsCount = 0;

        [Tooltip("Button to go to the next page. (optional)")]
        public Button nextButton;

        [Tooltip("Button to go to the previous page. (optional)")]
        public Button prevButton;

        [Tooltip("Number of items visible in one page of scroll frame.")]
        [RangeAttribute(1, 100)]
        public int itemsVisibleAtOnce = 1;

        [Tooltip("Sets minimum width of list items to 1/itemsVisibleAtOnce.")]
        public bool autoLayoutItems = true;

        [Tooltip("If you wish to update scrollbar numberOfSteps to number of active children on list.")]
        public bool linkScrolbarSteps = false;

        [Tooltip("If you wish to update scrollrect sensitivity to size of list element.")]
        public bool linkScrolrectScrollSensitivity = false;

        public Boolean useFastSwipe = true;

        public int fastSwipeThreshold = 100;

        // drag related
        protected bool startDrag = true;

        protected Vector3 positionOnDragStart = new Vector3();

        protected int pageOnDragStart;

        protected bool fastSwipeTimer = false;

        protected int fastSwipeCounter = 0;

        protected int fastSwipeTarget = 10;

        public ScrollRect ScrollRect => scrollRect;

        public int PageCount => pageCount;

        private bool _locked;
        private Tween _slideTween;
        private float slideDuration = 0.3f;
        private float _step = 0;
        private int _containerWidthDelta;
        private HorizontalLayoutGroup _gridLayout;


        void Awake()
        {

            scrollRect = gameObject.GetComponent<ScrollRect>();
            scrollRectTransform = gameObject.GetComponent<RectTransform>();
            listContainerTransform = scrollRect.content;
            listContainerRectTransform = listContainerTransform.GetComponent<RectTransform>();
            _gridLayout = listContainerTransform.GetComponent<HorizontalLayoutGroup>();

            UpdateListItemsSize();
            UpdateListItemPositions();

            PageChanged(CurrentPageIndex());

            if (nextButton)
            {
                nextButton.onClick.AddListener(NextScreen);
            }

            if (prevButton)
            {
                prevButton.onClick.AddListener(PreviousScreen);
            }
        }

        private void AlignItemsToCenter()
        {
            _containerWidthDelta = (int) ((itemsVisibleAtOnce - Mathf.Clamp(itemsCount, 0, itemsVisibleAtOnce)) * itemSize / 2);
            RectOffset padding = _gridLayout.padding;

            if (_containerWidthDelta == padding.left)
            {
                return;
            }

            var tempPadding = new RectOffset(padding.left, padding.right, padding.top, padding.bottom);
            tempPadding.left = _containerWidthDelta;
            _gridLayout.padding = tempPadding;

            var containerTransform = (listContainerTransform as RectTransform);
            containerTransform.anchoredPosition = Vector3.zero;
        }

        public void UpdateListItemsSize()
        {
            AlignItemsToCenter();
            float size = 0;
            float currentSize = 0;
            if (direction == ScrollDirection.Horizontal)
            {
                size = scrollRectTransform.rect.width / itemsVisibleAtOnce;
                currentSize = listContainerRectTransform.rect.width / itemsCount;
            }
            else
            {
                size = scrollRectTransform.rect.height / itemsVisibleAtOnce;
                currentSize = listContainerRectTransform.rect.height / itemsCount;
            }

            itemSize = size;

            if (linkScrolrectScrollSensitivity)
            {
                scrollRect.scrollSensitivity = itemSize;
            }

            if (autoLayoutItems && currentSize != size && itemsCount > 0)
            {
                if (direction == ScrollDirection.Horizontal)
                {
                    foreach (var tr in listContainerTransform)
                    {
                        GameObject child = ((Transform)tr).gameObject;
                        if (child.activeInHierarchy)
                        {
                            var childLayout = child.GetComponent<LayoutElement>();

                            if (childLayout == null)
                            {
                                childLayout = child.AddComponent<LayoutElement>();
                            }

                            childLayout.minWidth = itemSize;
                        }
                    }
                }
                else
                {
                    foreach (var tr in listContainerTransform)
                    {
                        GameObject child = ((Transform)tr).gameObject;
                        if (child.activeInHierarchy)
                        {
                            var childLayout = child.GetComponent<LayoutElement>();

                            if (childLayout == null)
                            {
                                childLayout = child.AddComponent<LayoutElement>();
                            }

                            childLayout.minHeight = itemSize;
                        }
                    }
                }
            }
        }

        public void UpdateListItemPositions()
        {
            var activeCount = GetActiveChildCount(listContainerTransform);
            if (activeCount == cachedChildCount) return;

            UpdateSwipeLock(activeCount);

            cachedChildCount = activeCount;
            _step = 1f / activeCount;
            if (activeCount > 0)
            {
                pageCount = Mathf.Max(activeCount - itemsVisibleAtOnce + 1, 1);
                Array.Resize(ref pageNormalizedPositions, pageCount);
                if (direction == ScrollDirection.Horizontal)
                {
                    for (var i = 0; i < pageCount; i++)
                    {
                        pageNormalizedPositions[i] = new Vector2(i / (float) (pageCount - 1), scrollRect.normalizedPosition.y);
                    }
                }
                else
                {
                    for (var i = 0; i < pageCount; i++)
                    {
                        pageNormalizedPositions[i] = new Vector2(scrollRect.normalizedPosition.x, i / (float) pageCount);
                    }
                }

                UpdateScrollbar(linkScrolbarSteps);
                ResetPage();
            }

            if (itemsCount != activeCount)
            {
                PageChanged(CurrentPageIndex());
            }

            itemsCount = activeCount;
        }

        private void UpdateSwipeLock(int activeCount)
        {
            if (direction == ScrollDirection.Horizontal)
            {
                scrollRect.horizontal = activeCount > itemsVisibleAtOnce;
            }
            else
            {
                scrollRect.vertical = activeCount > itemsVisibleAtOnce;
            }
        }

        private int GetActiveChildCount(Transform target)
        {
            return target.Cast<Transform>().Count(childTransform => childTransform.gameObject.activeSelf);
        }

        public void ResetPage()
        {
            StopMovement();
            if (direction == ScrollDirection.Horizontal)
            {
                scrollRect.horizontalNormalizedPosition = 0;
            }
            else
            {
                scrollRect.verticalNormalizedPosition = 0;
            }
        }

        public void StopMovement()
        {
            _slideTween.Kill();
        }

        protected void UpdateScrollbar(bool linkSteps)
        {
            if (linkSteps)
            {
                if (direction == ScrollDirection.Horizontal)
                {
                    if (scrollRect.horizontalScrollbar != null)
                    {
                        scrollRect.horizontalScrollbar.numberOfSteps = pageCount;
                    }
                }
                else
                {
                    if (scrollRect.verticalScrollbar != null)
                    {
                        scrollRect.verticalScrollbar.numberOfSteps = pageCount;
                    }
                }
            }
            else
            {
                if (direction == ScrollDirection.Horizontal)
                {
                    if (scrollRect.horizontalScrollbar != null)
                    {
                        scrollRect.horizontalScrollbar.numberOfSteps = 0;
                    }
                }
                else
                {
                    if (scrollRect.verticalScrollbar != null)
                    {
                        scrollRect.verticalScrollbar.numberOfSteps = 0;
                    }
                }
            }
        }

        public void LockScroll()
        {
            StartCoroutine(LockScrollDelayed());
        }

        //workaround of lateupdate
        private IEnumerator LockScrollDelayed()
        {
            _locked = true;
            yield return new WaitForEndOfFrame();
            scrollRect.StopMovement();
            scrollRect.enabled = false;
            _slideTween.Kill();
        }

        public void UnlockScroll()
        {
            _locked = false;
            scrollRect.enabled = true;
        }

        void LateUpdate()
        {
            if (_locked) return;
            UpdateListItemsSize();
            UpdateListItemPositions();

            if (fastSwipeTimer)
            {
                fastSwipeCounter++;
            }
        }

        private bool fastSwipe = false; //to determine if a fast swipe was performed


        private void MoveTo(Vector2 target)
        {
            _slideTween.Kill();
            var time = slideDuration;
            var nearestDistance = scrollRect.normalizedPosition - target;
            var abs = Math.Abs(direction == ScrollDirection.Horizontal ? nearestDistance.x : nearestDistance.y);

            if (abs < _step)
            {
                time = slideDuration * (abs / _step);
            }

            _slideTween = DOTween.To(() => scrollRect.normalizedPosition, x => scrollRect.normalizedPosition = x, target, time).SetEase(Ease.OutCirc);
        }

        //Function for switching screens with buttons
        public void NextScreen()
        {
            if (_locked) return;
            UpdateListItemPositions();

            if (CurrentPageIndex() < pageCount - 1)
            {
                lerpNormalizedTarget = pageNormalizedPositions[CurrentPageIndex() + 1];
                MoveTo(lerpNormalizedTarget);
                PageChanged(CurrentPageIndex() + 1);
            }
        }

        //Function for switching screens with buttons
        public void PreviousScreen()
        {
            if (_locked) return;
            UpdateListItemPositions();

            if (CurrentPageIndex() > 0)
            {
                lerpNormalizedTarget = pageNormalizedPositions[CurrentPageIndex() - 1];
                MoveTo(lerpNormalizedTarget);
                PageChanged(CurrentPageIndex() - 1);
            }
        }

        //Because the CurrentScreen function is not so reliable, these are the functions used for swipes
        private void NextScreenCommand()
        {
            if (_locked) return;
            if (pageOnDragStart < pageCount - 1)
            {
                int targetPage = Mathf.Min(pageCount - 1, pageOnDragStart + itemsVisibleAtOnce);
                lerpNormalizedTarget = pageNormalizedPositions[targetPage];
                MoveTo(lerpNormalizedTarget);
                PageChanged(targetPage);
            }
        }

        //Because the CurrentScreen function is not so reliable, these are the functions used for swipes
        private void PrevScreenCommand()
        {
            if (_locked) return;
            if (pageOnDragStart > 0)
            {
                int targetPage = Mathf.Max(0, pageOnDragStart - itemsVisibleAtOnce);
                lerpNormalizedTarget = pageNormalizedPositions[targetPage];
                MoveTo(lerpNormalizedTarget);
                PageChanged(targetPage);
            }
        }

        public int CurrentPageIndex()
        {
            int pos;

            if (direction == ScrollDirection.Horizontal)
            {
                pos = Mathf.RoundToInt(scrollRect.normalizedPosition.x * (pageCount - 1));
            }
            else
            {
                pos = Mathf.RoundToInt(scrollRect.normalizedPosition.y * (pageCount - 1));
            }

            return Mathf.Clamp(pos, 0, pageCount - 1);
        }

        public void ChangePage(int page)
        {
            if (0 <= page && page < pageCount)
            {
                lerpNormalizedTarget = pageNormalizedPositions[page];
                MoveTo(lerpNormalizedTarget);
                PageChanged(page);
            }
        }

        //changes the bullets on the bottom of the page - pagination
        private void PageChanged(int currentPage)
        {
            RefreshButtons(currentPage);
            onPageChange?.Invoke(currentPage);
        }

        public void RefreshButtons(int currentPage)
        {
            if (nextButton)
            {
                nextButton.interactable = currentPage < pageCount - 1;
            }

            if (prevButton)
            {
                prevButton.interactable = currentPage > 0;
            }
        }

        #region Interfaces
        public void OnBeginDrag(PointerEventData eventData)
        {
            if (_locked) return;
            UpdateScrollbar(false);

            fastSwipeCounter = 0;
            fastSwipeTimer = true;

            positionOnDragStart = eventData.position;
            pageOnDragStart = CurrentPageIndex();
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (_locked) return;
            startDrag = true;
            float delta = 0;

            if (direction == ScrollDirection.Horizontal)
            {
                delta = positionOnDragStart.x - eventData.position.x;
            }
            else
            {
                delta = -positionOnDragStart.y + eventData.position.y;
            }

            if (useFastSwipe)
            {
                fastSwipe = false;
                fastSwipeTimer = false;

                if (fastSwipeCounter <= fastSwipeTarget)
                {
                    if (Math.Abs(delta) > fastSwipeThreshold)
                    {
                        fastSwipe = true;
                    }
                }
                if (fastSwipe)
                {
                    if (delta > 0)
                    {
                        NextScreenCommand();
                    }
                    else
                    {
                        PrevScreenCommand();
                    }
                }
                else
                {
                    lerpNormalizedTarget = pageNormalizedPositions[CurrentPageIndex()];
                    MoveTo(lerpNormalizedTarget);
                }
            }
            else
            {
                lerpNormalizedTarget = pageNormalizedPositions[CurrentPageIndex()];
                MoveTo(lerpNormalizedTarget);
            }

            RefreshButtons(CurrentPageIndex());
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (_locked) return;
            _slideTween.Kill();
            if (startDrag)
            {
                OnBeginDrag(eventData);
                startDrag = false;
            }
        }
        #endregion
    }
}