﻿using UnityEngine;
using System.Collections;

namespace UnityEngine.UI
{
    public abstract class LoopScrollDataSource
    {
        public abstract void ProvideData(Transform transform, int idx);
    }

	public class LoopScrollSendIndexSource : LoopScrollDataSource
    {
		public static readonly LoopScrollSendIndexSource Instance = new LoopScrollSendIndexSource();

		LoopScrollSendIndexSource(){}

        public override void ProvideData(Transform transform, int idx)
        {
            
        }
    }

	public class LoopScrollArraySource<T> : LoopScrollDataSource
    {
	    readonly T[] _objectsToFill;

		public LoopScrollArraySource(T[] objectsToFill)
        {
            _objectsToFill = objectsToFill;
        }

        public override void ProvideData(Transform transform, int idx)
        {
            
        }

	    protected T GetObjectByIndex(int index)
	    {
	        return _objectsToFill[index];
	    }
    }
}