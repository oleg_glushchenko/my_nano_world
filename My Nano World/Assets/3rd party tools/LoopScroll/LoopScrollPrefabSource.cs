﻿using Assets.NanoLib.Utilities.Pulls;

namespace UnityEngine.UI
{
    [System.Serializable]
    public class LoopScrollPrefabSource
    {
        public GameObjectPullable Prefab;
        public Transform ItemsPullContainer;

        private bool _inited;

        private IObjectsPull ItemsPull { get; set; }


        public GameObject GetObject()
        {
            InitIfNeeded();

            var item = ItemsPull.GetInstance();            

            var itemGameObject = item as Component;
            if (itemGameObject != null)
            {
                return itemGameObject.gameObject;
            }
            return null;
        }

        public void ReturnObject(Transform go)
        {
            InitIfNeeded();

            var item = go.GetComponent<IPullableObject>();
            if (item != null)
            {
                go.SetParent(ItemsPullContainer, false);
                item.FreeObject();
            }
        }

        private void InitIfNeeded()
        {
            if (!_inited)
            {
                ItemsPull = new MObjectsPull();
                Prefab.pullSource = ItemsPull;
                ItemsPull.InstanitePull(Prefab);
                _inited = true;
            }
        }
    }
}
