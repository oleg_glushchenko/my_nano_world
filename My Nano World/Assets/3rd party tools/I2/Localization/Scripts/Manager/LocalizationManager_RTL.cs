﻿using UnityEngine;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using System.Globalization;
using System.Collections;
using System.Text;

namespace I2.Loc
{
    public static partial class LocalizationManager
    {
        static string[] LanguagesRTL = {"ar-DZ", "ar","ar-BH","ar-EG","ar-IQ","ar-JO","ar-KW","ar-LB","ar-LY","ar-MA","ar-OM","ar-QA","ar-SA","ar-SY","ar-TN","ar-AE","ar-YE",
                                        "fa", "he","ur","ji"};

        public static string ApplyRTLfix(string line) { return ApplyRTLfix(line, 0, true); }
        
        public static string ApplyRTLfix(string line, int maxCharacters, bool ignoreNumbers)
        {
            if (string.IsNullOrEmpty(line))
                return line;

            // Fix !, ? and . signs not set correctly
            char firstC = line[0];
            
            if (firstC == '!' || firstC == '.' || firstC == '?')
                line = line.Substring(1) + firstC;

            int tagStart = -1, tagEnd = 0;
            const int tagBase = 40000;
            var tagKeys = new Dictionary<string, string>();
            var textList = new List<string> {""};

            string cacheLine = line;

            while (I2Utils.FindNextTag(cacheLine, tagEnd, out tagStart, out tagEnd))
            {
                string tag = "@@" + (char) (tagBase + tagKeys.Count) + "@@";

                tagKeys.Add(tag, cacheLine.Substring(tagStart, tagEnd - tagStart + 1));

                string textStart = cacheLine.Substring(0, tagStart);
                cacheLine = cacheLine.Substring(tagEnd + 1);

                textList[textList.Count - 1] = textStart;
                textList.Add(tag);
                textList.Add(cacheLine);
                tagEnd = 0;
            }

            StringBuilder newLine = new StringBuilder();
            
            foreach (var text in textList)
            {
                newLine.Append(text);
            }

            line = newLine.Length > 0 ? newLine.ToString() : line;

            line = line.Replace("\r\n", "\n");
            line = I2Utils.SplitLine(line, maxCharacters);
            line = RTLFixer.Fix(line, true, !ignoreNumbers);

            if (tagKeys.Count > 0 && line.Length > 0)
            {
                foreach (var tagKey in tagKeys)
                {
                    line = line.Replace(tagKey.Key, tagKey.Value);
                }
            }

            return line;
        }

       
        public static string FixRTL_IfNeeded(string text, int maxCharacters = 0, bool ignoreNumber=false)
		{
			if (IsRight2Left)
				return ApplyRTLfix(text, maxCharacters, ignoreNumber);
			else
				return text;
		}

		public static bool IsRTL(string Code)
		{
			return System.Array.IndexOf(LanguagesRTL, Code)>=0;
		}
    }

}
