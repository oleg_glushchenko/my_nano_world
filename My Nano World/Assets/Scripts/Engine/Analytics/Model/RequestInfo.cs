﻿namespace Assets.Scripts.Engine.Analytics.Model
{
    public class RequestSendInfo
    {
        /// <summary>
        /// type of the request (e.g., product, life, etc.)
        /// </summary>
        public string RequestType { get; set; }

        /// <summary>
        /// id of the requested product
        /// </summary>
        public string ProductId { get; set; }

        public RequestSendInfo(string requestType, string productId)
        {
            RequestType = requestType;
            ProductId = productId;
        }
    }

    public class RequestReceivedInfo
    {
        /// <summary>
        /// type of the request (e.g., product, life, etc.)
        /// </summary>
        public string RequestType { get; set; }

        /// <summary>
        /// id of the requested product
        /// </summary>
        public string ProductId { get; set; }

        public RequestReceivedInfo(string requestType, string productId)
        {
            RequestType = requestType;
            ProductId = productId;
        }
    }
}