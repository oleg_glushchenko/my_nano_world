﻿using NanoReality.GameLogic.SocialCommunicator.api;
using UnityEngine;

namespace Assets.Scripts.Engine.Analytics.Model
{
    public class SocialConnectInfo
    {
        /// <summary>
        /// TWITTER, FACEBOOK, etc.
        /// </summary>
        public string SocialPlatform { get; set; }

        /// <summary>
        /// social ID
        /// </summary>
        public string SocId { get; set; }

        /// <summary>
        /// number of social friends
        /// </summary>
        public int SocialFriends { get; set; }

        /// <summary>
        /// Год рождения игрока
        /// </summary>
        public int BirthYear { get; set; }

        /// <summary>
        /// Пол игрока
        /// </summary>
        public string Gender { get; set; }

        public SocialConnectInfo(AuthorizeResult connectResult)
        {
            SocialPlatform = connectResult.SocialNetworkCommunicator.SocialNetworkName.ToUpper();
            SocId = connectResult.AuthorizedUser.id;

            int socialFriendsAmount = 0;
            if(connectResult.AuthorizedUser.friends != null)
                socialFriendsAmount = connectResult.AuthorizedUser.friends.Length;

            SocialFriends = socialFriendsAmount;
            Gender = "None";
            BirthYear = 1000;
            if (connectResult.AuthorizedUser != null && connectResult.AuthorizedUser is BaseUserProfile)
            {
                BaseUserProfile baseProfileData = (BaseUserProfile) connectResult.AuthorizedUser;
                Gender = baseProfileData.Gender.Contains("female") ? "FEMALE" : "MALE";
                BirthYear = baseProfileData.BirthYear;
            }
        }
    }
}