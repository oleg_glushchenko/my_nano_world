﻿using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;

namespace Assets.Scripts.Engine.Analytics.Model
{
	public class ProductionStartInfo
	{
		public int BuildingId;
		public string BuildingName;

		public IProducingItemData ProducingItemData;

		public ProductionStartInfo(int buildingId, string buildingName, IProducingItemData itemData)
		{
			BuildingId = buildingId;
			BuildingName = buildingName;
			ProducingItemData = itemData;
		}
	}
}