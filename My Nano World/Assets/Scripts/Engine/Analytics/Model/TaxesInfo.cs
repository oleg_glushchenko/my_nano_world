﻿namespace Assets.Scripts.Engine.Analytics.Model
{
    /// <summary>
    /// Данные о сборе налогов
    /// </summary>
    public class TaxesInfo
    {
        /// <summary>
        /// Сколько собрали налогов
        /// </summary>
        public int CoinsAmount { get; set; }

        /// <summary>
        /// Текущий уровень игрока
        /// </summary>
        public int CurrentPlayerLevel { get; set; }

        /// <summary>
        /// Уровень "счастья"
        /// </summary>
        public int HappinessPercent { get; set; }

        /// <summary>
        /// Количество людей в городе
        /// </summary>
        public int PopulationAmount { get; set; }

        /// <summary>
        /// Количество получаемых монет за час
        /// </summary>
        public int CoinsPerHour { get; set; }

        public TaxesInfo(int coinsAmount, int currentPlayerLevel, int happinessPercent, int populationAmount, int coinsPerHour)
        {
            CoinsAmount = coinsAmount;
            CurrentPlayerLevel = currentPlayerLevel;
            HappinessPercent = happinessPercent;
            PopulationAmount = populationAmount;
            CoinsPerHour = coinsPerHour;
        }
    }
}