﻿namespace Assets.Scripts.Engine.Analytics.Model
{
    /// <summary>
    /// Reports the result of a request to an ad network for the next ad. 
    /// </summary>
    public class AdRequestInfo
    {
        public string AdProvider { get; set; } //The ad sdk that provided the ad.
        public int AdRequestTimeMs { get; set; } //Time taken for an ad request to complete in milliseconds
        public string AdProviderVersion { get; set; } //The version of the sdk that provided the ad.
        public string AdSdkVersion { get; set; } //The version of the sdk that provided the ad.
        public string AdStatus { get; set; } //Extra information about it the ad was shown or not.
        public string AdType { get; set; } //The type of ad.
        public int AdWaterfallIndex { get; set; } //The position of an ad network within the priority waterfall.

        public AdRequestInfo(string adProvider, int adRequestTimeMs, string adProviderVersion, string adSdkVersion, string adStatus,
            string adType, int adWaterfallIndex)
        {
            AdProvider = adProvider;
            AdRequestTimeMs = adRequestTimeMs;
            AdProviderVersion = adProviderVersion;
            AdSdkVersion = adSdkVersion;
            AdStatus = adStatus;
            AdType = adType;
            AdWaterfallIndex = adWaterfallIndex;
        }
    }

    /// <summary>
    /// Reports when an ad has been shown.
    /// </summary>
    public class AdShowInfo
    {
        public string AdProvider { get; set; } //The ad sdk that provided the ad.
        public string AdProviderVersion { get; set; } //The version of the sdk that provided the ad.
        public string AdSdkVersion { get; set; } //The version of the sdk that provided the ad.
        public string AdStatus { get; set; } //Extra information about it the ad was shown or not.
        public string AdType { get; set; } //The type of ad.

        public AdShowInfo(string adProvider, string adProviderVersion, string adSdkVersion, string adStatus, string adType)
        {
            AdProvider = adProvider;
            AdProviderVersion = adProviderVersion;
            AdSdkVersion = adSdkVersion;
            AdStatus = adStatus;
            AdType = adType;
        }
    }

    /// <summary>
    /// Reports when an ad was closed.
    /// </summary>
    public class AdClosedInfo
    {
        public bool IsAdClicked { get; set; } //If an ad was clicked.
        public int AdEcpm { get; set; } //The estimated eCPM for an ad.
        public bool AdLeftApplication { get; set; } //If the player left the game because of an ad.
        public string AdProvider { get; set; } //The ad sdk that provided the ad.
        public string AdProviderVersion { get; set; } //The version of the sdk that provided the ad.
        public string AdSdkVersion { get; set; } //The version of the sdk that provided the ad.
        public string AdStatus { get; set; } //Extra information about it the ad was shown or not.
        public string AdType { get; set; } //The type of ad.

        public AdClosedInfo(bool isAdClicked, int adEcpm, bool adLeftApplication, string adProvider, string adSdkVersion,
            string adProviderVersion, string adStatus, string adType)
        {
            IsAdClicked = isAdClicked;
            AdEcpm = adEcpm;
            AdLeftApplication = adLeftApplication;
            AdProvider = adProvider;
            AdProviderVersion = adProviderVersion;
            AdSdkVersion = adSdkVersion;
            AdStatus = adStatus;
            AdType = adType;
        }
    }
}