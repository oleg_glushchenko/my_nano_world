﻿using UnityEngine.Analytics;

namespace Assets.Scripts.Engine.Analytics.Model
{
    /// <summary>
    /// Represent data for sending user profile event to any supported analytic provider
    /// </summary>
    public class UserProfileEventData
    {
        /// <summary>
        /// User name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// User surname
        /// </summary>
        public string Surname { get; set; }

        /// <summary>
        /// User birth year
        /// </summary>
        public int BirthYear { get; set; }

        /// <summary>
        /// User gender
        /// </summary>
        public Gender Gender { get; set; }

        public override string ToString()
        {
            return "User profile event: " + " Name - " + Name +
                   " Surname - " + Surname + " Gender - " + Gender;
        }
    }
}
