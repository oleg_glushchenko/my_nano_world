﻿using System.Collections.Generic;
using System.Linq;

namespace Assets.Scripts.Engine.Analytics.Model
{
    /// <summary>
    /// Represent data for sending custom event to any supported analytic provider
    /// </summary>
    public class CustomEventData
    {
        /// <summary>
        /// Name of custom event
        /// </summary>
        public string EventName { get; set; }

        /// <summary>
        /// Cusom event parameters
        /// </summary>
        public IDictionary<string, object> Params { get; set; }

        public CustomEventData()
        {
            
        }

        public CustomEventData(string eventName)
        {
            EventName = eventName;
        }        
        
        public CustomEventData(string eventName, string paramName, string paramValue)
        {
            EventName = eventName;
            Params = new Dictionary<string, object>() {{paramName, paramValue}};
        }
        
        public override string ToString()
        {
            return EventName + ": " + string.Join("\n", Params.Select(pair => pair.Key + ": " + pair.Value).ToArray());
        }
    }
}
