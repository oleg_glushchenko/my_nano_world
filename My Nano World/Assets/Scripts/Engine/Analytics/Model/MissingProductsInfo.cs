﻿using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;

namespace Assets.Scripts.Engine.Analytics.Model
{
    public class MissingProductsInfo
    {
        /// <summary>
        /// Список продуктов (key - id продукта, value - количество)
        /// </summary>
        public Dictionary<Id<Product>, int> ProductsList { get; set; }

        /// <summary>
        /// Цена покупки недостающих продуктов
        /// </summary>
        public int BuyPrice { get; set; }

        public MissingProductsInfo(Dictionary<Id<Product>, int> productsList, int buyPrice)
        {
            ProductsList = productsList;
            BuyPrice = buyPrice;
        }
    }
}