﻿using System;
using Assets.Scripts.Engine.Analytics.Model;
using Assets.Scripts.Engine.Analytics.Model.api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.StartApplicationSequence.Controllers.Signals;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.ServerCommunication.Models;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.Analytics.Extensions.CurrencyInfo;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.api;
using Assets.Scripts.GameLogic.DailyBonus.Impl;
using NanoReality.Core.Resources;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.Engine.UI.Views.NotEnoughtResourcesPanel;
using NanoReality.Game.Analytics;
using NanoReality.Game.Notifications;
using NanoReality.Game.Services.OrderDesk.SeaportOrderDesk;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.GameLogic.Achievements;
using NanoReality.GameLogic.Bank;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Controllers.Factories;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.GameManager.Controllers;
using NanoReality.GameLogic.Orders;
using NanoReality.GameLogic.PlayerProfile.Models.Impl;
using NanoReality.GameLogic.Services;
using NanoReality.GameLogic.SocialCommunicator.api;
using NanoReality.GameLogic.StartApplicationSequence.Controllers.Commands;
using UnityEngine;

namespace Assets.Scripts.Engine.Analytics
{
    /// <summary>
    /// Sends one of EventData types to choosen analytics provider or to all providers
    /// </summary>
    public class NanoAnalytics : INanoAnalytics
    {
	    [Inject] public IAdsService jAdsService { get; set; }
	    [Inject] public IProductsData jProductsData { get; set; }
	    [Inject] public IServerRequest jServerRequest { get; set; }
	    [Inject] public IPlayerExperience PlayerExperience { get; set; }
	    [Inject] public IAnalyticsManager AnalyticsManager { get; set; }
	    [Inject] public IPlayerResources jIPlayerResources { get; set; }
	    [Inject] public SignalUserCreated jSignalUserCreated { get; set; }
	    [Inject] public SignalGameInitialized jSignalGameInitialized { get; set; }
	    [Inject] public SoftCurrencyBoughtSignal jSoftCurrencyBoughtSignal { get; set; }
	    [Inject] public UserLevelUpSignal JUserLevelUpSignal { get; set; }
	    [Inject] public FinishApplicationInitSignal jFinishApplicationInitSignal { get; set; }
	    [Inject] public StartTutorialSignal jStartTutorialSignal { get; set; }
	    [Inject] public SignalOnFinishTutorial jSignalOnFinishTutorial { get; set; }
	    [Inject] public AnaliticsTutorialStepCompletedSignal jAnaliticsTutorialStepCompletedSignal { get; set; }
	    [Inject] public SignalOnQuestStart jSignalOnQuestStart { get; set; }
	    [Inject] public SignalOnQuestCompletedOnServer jSignalOnQuestCompletedOnServer { get; set; }
	    [Inject] public SignalOnStartProduceGoodOnFactory jSignalOnStartProduceGoodOnFactory { get; set; }
	    [Inject] public SignalOnMapBuildingsUpgradeStarted jSignalOnMapBuildingsUpgradeStarted { get; set; }
	    [Inject] public SignalOnAchievementCompleted jSignalOnAchievementCompleted { get; set; }
	    [Inject] public SignalOnInviteSentAction jSignalOnInviteSentAction { get; set; }
	    [Inject] public SignalOnSocialConnectAction jSignalOnSocialConnectAction { get; set; }
	    [Inject] public SignalOnRequestSendAction jSignalOnRequestSendAction { get; set; }
	    [Inject] public SignalOnRequestReceivedAction jSignalOnRequestReceivedAction { get; set; }
	    [Inject] public SignalOnUnlockSectorAction jSignalOnUnlockSectorAction { get; set; }
	    [Inject] public SignalShowNotEnoughtResourcesPanel jSignalShowNotEnoughtResourcesPanel { get; set; }
	    [Inject] public SignalOnRepairBuildingAction jSignalOnRepairBuildingAction { get; set; }
	    [Inject] public SignalOnAuthorizeSocialProfile jSignalOnAuthorizeSocialProfile { get; set; }
	    [Inject] public AnalyticsAchievementUnlock jAnalyticsAchievementUnlock { get; set; }
	    [Inject] public SignalOnDailyBonusCollected jSignalOnDailyBonusCollected { get; set; }
	    [Inject] public SignalOnCityNameChanged jSignalOnCityNameChanged { get; set; }
	    [Inject] public SignalSeaportOrderSent jSignalSeaportOrderSent { get; set; }
	    [Inject] public SignalOnSettingsLanguageChanged jSignalOnSettingsLanguageChanged { get; set; }
	    [Inject] public SignalOnSubSectorUnlockedVerified jSignalOnSubSectorUnlockedVerified { get; set; }
	    [Inject] public ProductReadySignal jProductReadySignal { get; set; }
	    [Inject] public NotificationInitSignal jNotificationInitSignal { get; set; }
	    [Inject] public AuthorizationStartedSignal jAuthorizationStartedSignal { get; set; }
	    [Inject] public AuthorizationSuccessSignal jAuthorizationSuccessSignal { get; set; }
	    [Inject] public GameDataLoadedSignal jGameBalanceLoadedSignal { get; set; }
	    [Inject] public BundlesLoadingFinishedSignal jAssetBundlesLoadedSignal { get; set; }
	    [Inject] public SignalOnCityMapLoaded jSignalOnCityMapLoaded { get; set; }
	    [Inject] public SignalAnalyticBuildStared jSignalAnalyticBuildStared { get; set; }

	    private readonly List<float> _serverResponsesTimeList = new List<float>();
	    private IDisposable _timerDisposable;

	    private static bool _isGameReloaded;

	    [PostConstruct]
		public void PostConstruct()
		{
			jSoftCurrencyBoughtSignal.AddListener(SendSoftCurrencyBundleBoughtEvent);
			JUserLevelUpSignal.AddListener(OnLevelUp);

			jFinishApplicationInitSignal.AddOnce(() =>
			{
				SendCustomEvent(new CustomEventData
				{
					EventName = "finishLoadApplication",
					Params = new Dictionary<string, object> { }
				});
			});

			jAnaliticsTutorialStepCompletedSignal.AddListener(SendTutorialStepEvent);
			jStartTutorialSignal.AddListener(SendTutorialStartEvent);
			jSignalOnFinishTutorial.AddListener(SendTutorialCompletedEvent);

			//Quests analytics
			jSignalOnQuestCompletedOnServer.AddListener(OnQuestCompleted);

			jSignalOnStartProduceGoodOnFactory.AddListener(OnStartProduction);
			jProductReadySignal.AddListener(OnProductReady);

			jSignalAnalyticBuildStared.AddListener(OnStartBuildMapObject);
			jSignalOnMapBuildingsUpgradeStarted.AddListener(OnMapBuildingsUpgradeStarted);

			jSignalOnAchievementCompleted.AddListener(OnSendAchievementCompletedEvent);
			jSignalOnInviteSentAction.AddListener(OnInviteSentEvent);
			jSignalOnSocialConnectAction.AddListener(SendSocialConnectEvent);
			
			jSignalOnRequestSendAction.AddListener(OnRequestSendEvent);
			jSignalOnRequestReceivedAction.AddListener(OnRequestReceivedEvent);

			jSignalOnUnlockSectorAction.AddListener(OnUnlockSectorEvent);
			jSignalShowNotEnoughtResourcesPanel.AddListener(OnNotEnoughResourcesPanelShowSignal);
			jSignalOnRepairBuildingAction.AddListener(OnRepairBuildEvent);

			//Social signals
			jSignalOnAuthorizeSocialProfile.AddListener(OnSocialNetworkAuthorization);

			//Achievements
			jAnalyticsAchievementUnlock.AddListener(OnUserAchievementUnlocked);

			//Daily Bonus
			jSignalOnDailyBonusCollected.AddListener(OnDailyBonusCollected);

			//Video Ads
			jAdsService.VideoWatched += OnWatchedVideoAdSuccess;

			//City
			jSignalOnCityNameChanged.AddListener(OnCityNameChanged);

			//Order Desks
			jSignalSeaportOrderSent.AddListener(OnSeaportOrderSent);

			//Localization
			jSignalOnSettingsLanguageChanged.AddListener(OnSettingsLanguageChanged);

			//Sub Sectors
			jSignalOnSubSectorUnlockedVerified.AddListener(OnSubSectorUnlockedVerified);

			jSignalUserCreated.AddListener(OnUserCreated);
			jSignalGameInitialized.AddListener(OnGameInitializedFirstTime);
			jNotificationInitSignal.AddListener(OnNotificationInitSignal);
			jAuthorizationStartedSignal.AddListener(OnAuthorizationStarted);
			jAuthorizationSuccessSignal.AddListener(OnAuthorizationSuccess);
			jAssetBundlesLoadedSignal.AddListener(OnAssetBundlesLoad);
			jSignalOnCityMapLoaded.AddListener(OnCityMapLoaded);
			jGameBalanceLoadedSignal.AddListener(OnGameConfigLoaded);
			jServerRequest.ResponseReceived += LogServerResponseTime;
		}
		
	    public void Dispose()
	    {
		    jSoftCurrencyBoughtSignal.RemoveListener(SendSoftCurrencyBundleBoughtEvent);
			JUserLevelUpSignal.RemoveListener(OnLevelUp);
			jAnaliticsTutorialStepCompletedSignal.RemoveListener(SendTutorialStepEvent);
			jStartTutorialSignal.RemoveListener(SendTutorialStartEvent);
			jSignalOnFinishTutorial.RemoveListener(SendTutorialCompletedEvent);
			jSignalOnQuestCompletedOnServer.RemoveListener(OnQuestCompleted);
			jSignalOnStartProduceGoodOnFactory.RemoveListener(OnStartProduction);
			jProductReadySignal.RemoveListener(OnProductReady);
			jSignalAnalyticBuildStared.RemoveListener(OnStartBuildMapObject);
			jSignalOnMapBuildingsUpgradeStarted.RemoveListener(OnMapBuildingsUpgradeStarted);
			jSignalOnAchievementCompleted.RemoveListener(OnSendAchievementCompletedEvent);
			jSignalOnInviteSentAction.RemoveListener(OnInviteSentEvent);
			jSignalOnSocialConnectAction.RemoveListener(SendSocialConnectEvent);
			jSignalOnRequestSendAction.RemoveListener(OnRequestSendEvent);
			jSignalOnRequestReceivedAction.RemoveListener(OnRequestReceivedEvent);
			jSignalOnUnlockSectorAction.RemoveListener(OnUnlockSectorEvent);
			jSignalShowNotEnoughtResourcesPanel.RemoveListener(OnNotEnoughResourcesPanelShowSignal);
			jSignalOnRepairBuildingAction.RemoveListener(OnRepairBuildEvent);
			jSignalOnAuthorizeSocialProfile.RemoveListener(OnSocialNetworkAuthorization);
			jAnalyticsAchievementUnlock.RemoveListener(OnUserAchievementUnlocked);
			jSignalOnDailyBonusCollected.RemoveListener(OnDailyBonusCollected);
			jAdsService.VideoWatched -= OnWatchedVideoAdSuccess;
			jSignalOnCityNameChanged.RemoveListener(OnCityNameChanged);
			jSignalSeaportOrderSent.RemoveListener(OnSeaportOrderSent);
			jSignalOnSettingsLanguageChanged.RemoveListener(OnSettingsLanguageChanged);
			jSignalOnSubSectorUnlockedVerified.RemoveListener(OnSubSectorUnlockedVerified);
			jSignalUserCreated.RemoveListener(OnUserCreated);
			jSignalGameInitialized.RemoveListener(OnGameInitializedFirstTime);
			jNotificationInitSignal.RemoveListener(OnNotificationInitSignal);
			jAuthorizationStartedSignal.RemoveListener(OnAuthorizationStarted);
			jAuthorizationSuccessSignal.RemoveListener(OnAuthorizationSuccess);
			jAssetBundlesLoadedSignal.RemoveListener(OnAssetBundlesLoad);
			jSignalOnCityMapLoaded.RemoveListener(OnCityMapLoaded);
			jGameBalanceLoadedSignal.RemoveListener(OnGameConfigLoaded);

			jServerRequest.ResponseReceived -= LogServerResponseTime;

			_isGameReloaded = true;
	    }
		
		private void OnAuthorizationStarted() => SendCurrentLoadStep("AuthorizationStarted");
		private void OnAuthorizationSuccess(int _) => SendCurrentLoadStep("AuthorizationSuccess");
		private void OnAssetBundlesLoad(bool _) => SendCurrentLoadStep("AssetBundlesLoad");
		private void OnGameConfigLoaded() => SendCurrentLoadStep("GameConfigLoaded");
		private void OnCityMapLoaded(Id<IPlayerProfile> _, IUserCity __, ICityMap ___) => SendCurrentLoadStep("CityMapLoaded");

		/// <summary>
        /// Define the event params and send event
        /// </summary>
        /// <param name="eventData">UserProfile event data</param>
        /// <param name="analyticsType">Which tracker type need to use for send analytics data</param>
        public void SendEvent(UserProfileEventData eventData, AnalyticsType analyticsType)
        {
			if (eventData == null)
				return;
        }

		/// <summary>
        /// Define the event params and send event
        /// </summary>
        /// <param name="eventData">Custom event data</param>
        /// <param name="analyticsType">Which tracker type need to use for send analytics data</param>
        public void SendEvent(CustomEventData eventData, AnalyticsType analyticsType)
        {
            AnalyticsManager.LogEvent(eventData);
        }

		public void LogServerResponseTime(RequestData requestData)
		{
			_serverResponsesTimeList.Add(requestData.Duration);
		}

		public ServerResponseAnalytic GetServerResponseTime()
		{
			ServerResponseAnalytic serverResponseAnalytic = new ServerResponseAnalytic();

			int totalElementsCount = _serverResponsesTimeList.Count;
			
			if (totalElementsCount == 0) return serverResponseAnalytic;

			int medianElementIndex = totalElementsCount / 2;

			IOrderedEnumerable<float> sortedNumbers = _serverResponsesTimeList.OrderBy(n => n);

			serverResponseAnalytic.Response_time_quart1 = GetQuart(0.25f);

			serverResponseAnalytic.Response_time_quart3 = GetQuart(0.75f);

			if (totalElementsCount % 2 == 0)
			{
				serverResponseAnalytic.Response_time_median = (sortedNumbers.ElementAt(medianElementIndex) + sortedNumbers.ElementAt(medianElementIndex - 1)) / 2;
			}
			else
			{
				serverResponseAnalytic.Response_time_median = sortedNumbers.ElementAt(medianElementIndex);
			}

			float GetQuart(float quart)
			{
				if (totalElementsCount == 1)
				{
					return sortedNumbers.FirstOrDefault();
				}

				float indexPosition = (totalElementsCount - 1) * quart;
				int floorIndexPosition = (int) Math.Floor(indexPosition);
				float rest = indexPosition - floorIndexPosition;
				return sortedNumbers.ElementAt(floorIndexPosition) + rest *
					(sortedNumbers.ElementAt(floorIndexPosition + 1) - sortedNumbers.ElementAt(floorIndexPosition));
			}

			return serverResponseAnalytic;
		}

		public void OnUiClick(string target)
		{
			AnalyticsManager.LogEvent(new CustomEventData
			{
				EventName = "ui_click",
				Params = new Dictionary<string, object>
				{
					{"ui_target", target}
				}
			});
		}

        #region TheEvent

        public void OnInAppPurchase(string storeId, float price)
        {
	        var id = storeId.Split('.').ToList();
	        AnalyticsManager.LogEvent(new CustomEventData
	        {
		        EventName = "in_app_purchase",
		        Params = new Dictionary<string, object>
		        {
			        {"store_id", id.LastOrDefault()},
			        {"store_price", price}
		        }
	        });
        }

        public void OnInAppPurchaseFail(string orderId, string status)
        {
	        AnalyticsManager.LogEvent(new CustomEventData
	        {
		        EventName = "in_app_purchase_fail",
		        Params = new Dictionary<string, object>
		        {
			        {"purchase_ID", orderId},
			        {"purchase_status", status}
		        }
	        });
        }

        public void OnBuildStageBuilding(int buildingStage)
        {
            AnalyticsManager.LogEvent(new CustomEventData
            {
                EventName = "event_stage_completed",
                Params = new Dictionary<string, object>
                {
                    {"building_stage", buildingStage.ToString()}
				}
            });
        }

        public void OnPlaceOnTheMapRewardedBuilding()
        {
            AnalyticsManager.LogEvent(new CustomEventData
            {
                EventName = "event_place_building",
                Params = new Dictionary<string, object>
                {
                    {"place_on_map", true}
                }
            });
        }

		public void TheEventStartInfo(string eventName, int day, int buildingStage)
        {
            AnalyticsManager.LogEvent(new CustomEventData
            {
                EventName = "event_start_info",
                Params = new Dictionary<string, object>
                {
                    {"event_name", eventName},
                    {"event_day", day.ToString()},
                    {"building_stage", buildingStage.ToString()}
                }
            });
        }

        public void OnPurchaseNanoPass(string storeId)
        {
	        var id = storeId.Split('.').ToList();
            AnalyticsManager.LogEvent(new CustomEventData
            {
                EventName = "event_purchase_nanopass",
                Params = new Dictionary<string, object>
                {
                    {"store_id", id.LastOrDefault()}
                }
            });
        }

        #endregion

		public void OnShuffle(string target, string price, string slotId)
		{
			AnalyticsManager.LogEvent(new CustomEventData
			{
				EventName = "shuffle_order",
				Params = new Dictionary<string, object>
				{
					{"shuffle_target", target},
					{"shuffle_price", price},
					{"shuffle_slotId", slotId}
				}
			});
		}

		public void BuySoukSlot(string target, int slotId, string rewards)
		{
			OnOrderSent("Souk", slotId, rewards);
			AnalyticsManager.LogEvent(new CustomEventData
			{
				EventName = "buy_souk_slot",
				Params = new Dictionary<string, object>
				{
					{"souk_target", target},
					{"order_id", slotId},
					{"order_reward", rewards}
				}
			});
		}

		public void FoodSupplyCook(string foodSupplyRecipeId, string mealId)
		{
			AnalyticsManager.LogEvent(new CustomEventData
			{	
				EventName = "food_supply_cook",
				Params = new Dictionary<string, object>
				{
					{"food_supply_recipe", foodSupplyRecipeId},
					{"food_supply_meal", mealId},
				}
			});
		}

		public void FoodSupplyFeed(int resourcesAmount, int resourcesUsed, float totalFoodSupplyAmount)
		{
			OnOrderSent("FoodSupply", default, totalFoodSupplyAmount.ToString());
			AnalyticsManager.LogEvent(new CustomEventData
			{
				EventName = "food_supply_feed",
				Params = new Dictionary<string, object>
				{
					{"food_supply_resources_amount", resourcesAmount},
					{"food_supply_resources_used", resourcesUsed},
					{"food_supply_amount", totalFoodSupplyAmount},
				}
			});
		}

		public void FoodSupplyGift(int giftId)
		{
			AnalyticsManager.LogEvent(new CustomEventData
			{
				EventName = "food_supply_gift",
				Params = new Dictionary<string, object>
				{
					{"gift_id", giftId},
				}
			});
		}

		public void UnlockSlotEvent(string target, int slotId, string position = "main")
		{
			AnalyticsManager.LogEvent(new CustomEventData
			{
				EventName = "unlock_slot",
				Params = new Dictionary<string, object>
				{
					{"slot_target", target},
					{"slot_position", position},
					{"slot_id", slotId}
				}
			});
		}

		public void SkipIntroVideo()
        {
            AnalyticsManager.LogEvent("skip_intro_video");
        }

		public void SendRequestError(IErrorData errorData)
        {
	        AnalyticsManager.LogEvent(new CustomEventData
	        {
		        EventName = "request_error",
		        Params = new Dictionary<string, object>
		        {
			        {"error_type", errorData.HttpErrorCode.ToString()},
			        {"error_message", errorData.ErrorMessage ?? "Error message did not come"},
			        {"request_data", errorData.URL},
		        }
	        });
        }

        public void OnAdsWatched(string source)
        {
	        AnalyticsManager.LogEvent(new CustomEventData
	        {
		        EventName = "ads_wached",
		        Params = new Dictionary<string, object>
		        {
			        {"ads_source_of_use", source}
		        }
	        });
        }

        private void OnUserCreated(bool isCreated)
        {
	        if (_isGameReloaded) return;

	        AnalyticsManager.LogEvent(new CustomEventData
	        {
		        EventName = "user_created",
		        Params = new Dictionary<string, object>
		        {
			        {"is_new_user", isCreated}
		        }
	        });
        }
        
        private void SendCurrentLoadStep(string step)
        {
	        if (_isGameReloaded) return;
	        
	        AnalyticsManager.LogEvent(new CustomEventData
	        {
		        EventName = "loadStep",
		        Params = new Dictionary<string, object>
		        {
			        {"loadStepName", step}
		        }
	        });
        }
        
        public void OnExperienceUpdate(AwardSourcePlace sourcePlace, string source, int experience)
        {
	        AnalyticsManager.LogEvent(new CustomEventData
	        {
		        EventName = "experience_gain",
		        Params = new Dictionary<string, object>
		        {
			        {"userXP", experience},
			        {"award_source", sourcePlace},
			        {"currencies_source", source},
		        }
	        });
        }

        private void OnGameInitializedFirstTime()
        {
	        AnalyticsManager.LogEvent(new CustomEventData
	        {
		        EventName = "game_initialized"
	        });
        }

        private void SendSoftCurrencyBundleBoughtEvent(IBankProduct product)
        {
	        if (product == null)
		        return;

	        AnalyticsManager.LogEvent(new CustomEventData
	        {
		        EventName = "buy_soft_currency_bundle",
		        Params = new Dictionary<string, object>
		        {
			        {"bundle_id", product.Id.ToString()},
			        {"premium_amount_bundle", product.Price},
			        {"bundle_name", product.Name}
		        }
	        });
        }

        private void OnRepairBuildEvent(IMapBuilding mapBuild)
        {
	        AnalyticsManager.LogEvent(new CustomEventData
	        {
		        EventName = "repair_building",
		        Params = new Dictionary<string, object>
		        {
			        {"building_id", mapBuild.ObjectTypeID.Value.ToString()},
			        {"building_name", mapBuild.BalanceName},
			        {"building_level", mapBuild.Level}
		        }
	        });
        }
        
        private void OnUnlockSectorEvent(string sectorName)
        {
        }

        private void OnNotEnoughResourcesPanelShowSignal()
        {
        }

        private void SendCustomEvent(CustomEventData eventData)
		{
			if (eventData == null) return;

            AnalyticsManager.LogEvent(eventData);
		}

        private void SendTutorialStepEvent(StepType stepType, HintType hintStep)
        {
	        string tutorialStepName = stepType != StepType.None ? stepType.ToString() : hintStep.ToString();
	        int tutorialStep = stepType != StepType.None ? (int) stepType : (int) hintStep;
	        string tutorialType = stepType != StepType.None ? "Hard Tutorial" : "Hint Tutorial";

	        AnalyticsManager.LogEvent(new CustomEventData
	        {
		        EventName = "tutorialStepCompleted",
		        Params = new Dictionary<string, object>()
		        {
			        {"tutorialStepName", tutorialStepName},
			        {"tutorial_step", tutorialStep},
			        {"tutorial_type", tutorialType}
		        }
	        });
        }

        private void SendTutorialStartEvent(StepType eventData)
        {
	        AnalyticsManager.LogEvent(new CustomEventData()
	        {
		        EventName = "tutorialStarted",
		        Params = new Dictionary<string, object>()
		        {
			        {"tutorialStepName", eventData.ToString()},
			        {"tutorial_step", (int)eventData}
		        }
	        });
        }

        private void SendTutorialCompletedEvent()
        {
            AnalyticsManager.LogEvent(new CustomEventData
            {
                EventName = "tutorialCompleted"
            });
        }

        private void OnRequestSendEvent(RequestSendInfo requestInfoData)
        {
            AnalyticsManager.LogEvent(new CustomEventData()
            {
                EventName = "request_sent",
                Params = new Dictionary<string, object>()
                {
                    { "request_type", requestInfoData.RequestType },
                    { "product_id", requestInfoData.ProductId }
                }
            });
        }

        private void OnRequestReceivedEvent(RequestReceivedInfo requestInfoData)
        {
            AnalyticsManager.LogEvent(new CustomEventData()
            {
                EventName = "request_received",
                Params = new Dictionary<string, object>()
                {
                    { "request_type", requestInfoData.RequestType },
                    { "product_id", requestInfoData.ProductId }
                }
            });
        }

        private void OnLevelUp()
        {
	        AnalyticsManager.LogEvent(new CustomEventData()
	        {
		        EventName = "level_up",
		        Params = new Dictionary<string, object>
		        {
			        { "warehouse_amount", jIPlayerResources.ProductsCount},
			        { "warehouse_capacity", jIPlayerResources.WarehouseCapacity}
		        }
	        });
        }

		#region QUEST ANALITICS
		
        private void OnQuestCompleted(IQuest quest, List<IAwardAction> awards)
		{
		    AnalyticsManager.LogEvent(new CustomEventData()
		    {
		        EventName = "quest_completed",
		        Params = new Dictionary<string, object>()
		        {
			        { "quest_id", quest.QuestId.ToString() },
		            { "quest_name", quest.Name },
		        }
		    });
        }
        
        private void OnNotificationInitSignal(bool fromNotification)
        {
	        AnalyticsManager.LogEvent(new CustomEventData
	        {
		        EventName = "track_coming_back_source",
		        Params = new Dictionary<string, object>
		        {
			        {"initialized_source", fromNotification}
		        }
	        });
        }

        #endregion

        private void OnStartProduction(ProductionStartInfo info)
        {
	        var product = jProductsData.GetProduct(info.ProducingItemData.OutcomingProducts);

	        AnalyticsManager.LogEvent(new CustomEventData
	        {
		        EventName = "productionStart",
		        Params = new Dictionary<string, object>
		        {
			        {"buildingID", info.BuildingId.ToString()},
			        {"itemName", product.Name},
			        {"itemAmount", info.ProducingItemData.Amount},
		        }
	        });
        }
        
        private void OnProductReady(IProduceSlot slot, Id_IMapObject _)
        {
	        var product = jProductsData.GetProduct(slot.ItemDataIsProducing.OutcomingProducts); 

	        AnalyticsManager.LogEvent(new CustomEventData
	        {
		        EventName = "production_ready",
		        Params = new Dictionary<string, object>
		        {
			        {"itemID", product.ProductTypeID.Value.ToString()},
			        {"itemName", product.Name}
		        }
	        });
        }

        private void OnStartBuildMapObject(IMapObject mapObject, PriceType virtualCurrencyType)
        {
	        var curencyType = PlayerResources.CurencyType.Unknown;
	        switch (virtualCurrencyType)
	        {
		        case PriceType.Gold:
			        curencyType = PlayerResources.CurencyType.Gold;
			        break;
			        ;
		        case PriceType.Hard:
			        curencyType = PlayerResources.CurencyType.Bucks;
			        break;
		        case PriceType.Soft:
			        curencyType = PlayerResources.CurencyType.Coins;
			        break;
		        default:
			        curencyType = PlayerResources.CurencyType.Unknown;
			        break;
	        }
            AnalyticsManager.LogEvent(new CustomEventData
            {
                EventName = "buildStart",
                Params = new Dictionary<string, object>
                {
                    { "buildingID",  mapObject.ObjectTypeID.Value.ToString() },
                    { "buildingLevel", mapObject.Level },
                    { "virtualCurrencyType", curencyType },
                }
            });
		}

        private void OnMapBuildingsUpgradeStarted(IMapBuilding mapBuilding)
        {
	        if (mapBuilding.MapObjectType == MapObjectintTypes.CityHall)
	        {
		        AnalyticsManager.LogEvent(new CustomEventData
		        {
			        EventName = "upgrade_city_hall",
			        Params = new Dictionary<string, object>
			        {
				        {"building_level", mapBuilding.Level},
				        {"user_level", PlayerExperience.CurrentLevel},
			        }
		        });

		        return;
	        }

	        if (mapBuilding.MapObjectType == MapObjectintTypes.Warehouse)
	        {
		        AnalyticsManager.LogEvent(new CustomEventData
		        {
			        EventName = "upgrade_warehouse",
			        Params = new Dictionary<string, object>
			        {
				        {"buildingLevel", mapBuilding.Level},
				        {"buildingID", mapBuilding.ObjectTypeID.Value.ToString()},
			        }
		        });

		        return;
	        }

	        AnalyticsManager.LogEvent(new CustomEventData
	        {
		        EventName = "map_buildings_upgrade",
		        Params = new Dictionary<string, object>
		        {
			        {"buildingID", mapBuilding.ObjectTypeID.Value.ToString()},
			        {"buildingName", mapBuilding.BalanceName},
			        {"buildingLevel", mapBuilding.Level},
		        }
	        });
        }

        private void OnSendAchievementCompletedEvent(AchievementInfo achievementInfo)
        {
        }

        private void OnInviteSentEvent(InviteInfo inviteSentInfo)
        {
            AnalyticsManager.LogEvent(new CustomEventData()
            {
                EventName = "invite_sent",
                Params = new Dictionary<string, object>()
                {
                    { "invite_type", inviteSentInfo.InviteType },
                    { "unique_tracking", inviteSentInfo.UniqueTracking },
                }
            });
        }

        private void SendSocialConnectEvent(AuthorizeResult authorizeResultInfo)
        {
            // hotfix when login in google play game services

            SocialConnectInfo socialConnectInfo = new SocialConnectInfo(authorizeResultInfo);
	        
            AnalyticsManager.LogEvent(new CustomEventData
            {
                EventName = "social_connect",
                Params = new Dictionary<string, object>
                {
                    { "user_name", authorizeResultInfo.AuthorizedUser.userName },
                    { "is_authorized", authorizeResultInfo.IsAuthorized },
                    { "social_network_name", authorizeResultInfo.SocialNetworkCommunicator.SocialNetworkName },
                }
            });
        }
        
        private void OnSocialNetworkAuthorization(AuthorizeResult authorizeResult)
        {
            var eventName = "social_unknown_network_connected";

            switch (authorizeResult.SocialNetworkCommunicator.SocialNetworkType)
            {
                case SocialNetworkName.Facebook:
                    eventName = "social_facebook_connected";
                    break;
                case SocialNetworkName.GooglePlay:
                    eventName = "social_google_play_connected";
                    break;
                case SocialNetworkName.Twitter:
	                eventName = "social_twitter_connected";
                    break;
                case SocialNetworkName.GameCenter:
	                eventName = "social_game_center_connected";
                    break;
            }

            AnalyticsManager.LogEvent(eventName);
        }

		public void OnPlayerCurrenciesDecrease(Price price, CurrenciesSpendingPlace place, string target)
		{
			if (price.IsNeedSoft)
			{
				OnPlayerCurrenciesDecrease(PlayerResources.CurencyType.Coins, price.SoftPrice,
					place, target);
			}

			if (price.IsNeedHard)
			{
				OnPlayerCurrenciesDecrease(PlayerResources.CurencyType.Bucks, price.HardPrice,
					place, target);
			}
		}
		
		public void OnPlayerCurrenciesDecrease(PlayerResources.CurencyType curencyType, int amount, CurrenciesSpendingPlace place, string target)
		{
            AnalyticsManager.LogEvent(new CustomEventData()
            {
                EventName = "spend_currency",
                Params = new Dictionary<string, object>()
                {
                    { "virtualCurrencyType", curencyType },
                    { "virtualCurrencyAmount", amount },
                    {"currencies_spending_place", place.ToString()},
                }
            });
        }

        public void OnPlayerCurrenciesIncrease(PlayerResources.CurencyType curencyType, int amount, AwardSourcePlace sourcePlace, string source)
        {
            AnalyticsManager.LogEvent(new CustomEventData()
            {
                EventName = "get_currencies",
                Params = new Dictionary<string, object>()
                {
	                {"virtualCurrencyType", curencyType.ToString()},
	                {"currencies_source",source},
	                {"award_source", sourcePlace.ToString()},
                }
            });
        }

	    private void OnUserAchievementUnlocked(IUserAchievement achievement)
	    {
		    AnalyticsManager.LogEvent("unlock_achievement", "achievement_id", achievement.BalanceID.ToString());
	    }

	    private void OnDailyBonusCollected(DailyBonusData dailyBonusData)
	    {
		    AnalyticsManager.LogEvent("daily_bonus_collected", "day", dailyBonusData.Day.ToString());
	    }

	    private void OnWatchedVideoAdSuccess(object sender, EventArgs e)
	    {
		    switch (jAdsService.PlayingAdType)
		    {
			    case AdTypePanel.CasinoFreeSpin:
				    AnalyticsManager.LogEvent("ad_casino_free_spin");
				    break;
			    case AdTypePanel.RestaurantPlay:
				    AnalyticsManager.LogEvent("ad_burger_chanllenge_time_skip");
				    break;
			    case AdTypePanel.RefreshOrdersInMarket:
				    AnalyticsManager.LogEvent("ad_shop_skip_timer");
				    break;
		    }
	    }

	    private void OnCityNameChanged(string cityName)
	    {
		    AnalyticsManager.LogEvent("change_city_name", "name", cityName);
	    }

	    private void OnSeaportOrderSent(ISeaportOrderSlot slot)
	    {
		    string reward = slot.Awards.Product != null ? slot.Awards.Product.ProductId.Value.ToString() : "experience";
		    OnOrderSent("Seaport", slot.ServerPosition, reward);
	    }

	    public void OnOrderSent(string businessSectorsTypes, int orderId, string reward)
	    {
		    var orderSentEvent = new CustomEventData();
		    
		    orderSentEvent.EventName = "order_completed";

		    orderSentEvent.Params = new Dictionary<string, object>
		    {
			    {"business_sectors_type", businessSectorsTypes},
			    {"order_id", orderId},
			    {"order_reward", reward},
		    };

		    AnalyticsManager.LogEvent(orderSentEvent);
	    }

	    private void OnSettingsLanguageChanged(string languageName)
	    {
		    AnalyticsManager.LogEvent("localization_changed", "locale_name", languageName);
	    }

	    private void OnSubSectorUnlockedVerified(ILockedSector lockedSector)
	    {
		    var sectorType = lockedSector.BusinessSectorId.ToBusinessSectorType();

		    AnalyticsManager.LogEvent(new CustomEventData
		    {
			    EventName = "unlock_sub_sector",
			    Params = new Dictionary<string, object>
			    {
				    {"business_sectors_type", sectorType.ToString()}
			    }
		    });
	    }
    }
    
    public class ServerResponseAnalytic
    {
	    public float Response_time_quart1 = 0;
	    public float Response_time_quart3 = 0;
	    public float Response_time_median = 0;

	    public override string ToString()
	    {
		    return $"quart1 {Response_time_quart1} | quart3 {Response_time_quart3} | median {Response_time_median}";
	    }
    }
}
