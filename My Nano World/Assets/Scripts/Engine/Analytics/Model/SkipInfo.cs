﻿using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;

namespace Assets.Scripts.Engine.Analytics.Model
{
    /// <summary>
    /// Данные о скипе производства товаров
    /// </summary>'
    // TODO: need to remove this old data.
    public class SkipProductionInfo
    {
        /// <summary>
        /// Название здания
        /// </summary>
        public string BuildingName { get; set; }

        /// <summary>
        /// ID здания
        /// </summary>
        public string BuildingID { get; set; }

        /// <summary>
        /// Производимые продукты
        /// </summary>
        public IProducingItemData ProducingItemData { get; set; }

        /// <summary>
        /// Цена скипа
        /// </summary>
        public int SkipPrice { get; set; }

        public SkipProductionInfo(string buildingName, string buildingID, IProducingItemData itemData, int skipPrice)
        {
            BuildingName = buildingName;
            BuildingID = buildingID;
            ProducingItemData = itemData;
            SkipPrice = skipPrice;
        }
    }

    /// <summary>
    /// Данные о скипе здания или его апгрейде
    /// </summary>
    public class SkipBuildingInfo
    {
        /// <summary>
        /// Название здания
        /// </summary>
        public string BuildingName { get; set; }

        /// <summary>
        /// ID здания
        /// </summary>
        public int BuildingID { get; set; }

        /// <summary>
        /// Уровень здания
        /// </summary>
        public int BuildingLevel { get; set; }

        /// <summary>
        /// Цена скипа
        /// </summary>
        public int SkipPrice { get; set; }

        public SkipBuildingInfo(string buildingName, int buildingID, int buildingLevel, int skipPrice)
        {
            BuildingName = buildingName;
            BuildingID = buildingID;
            BuildingLevel = buildingLevel;
            SkipPrice = skipPrice;
        }
    }
}