﻿using System;
using Assets.NanoLib.ServerCommunication.Models;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.PlayerProfile.Models.Impl;

namespace Assets.Scripts.Engine.Analytics.Model.api
{
    /// <summary>
    /// Represents interface for sending one of EventData types to choosen analytics provider or to all providers
    /// </summary>
    public interface INanoAnalytics : IDisposable
    {
	    /// <summary>
        /// Define the event params and send event
        /// </summary>
        /// <param name="eventData">UserProfile event data</param>
        /// <param name="analyticsType">Which tracker type need to use for send analytics data</param>
        void SendEvent(UserProfileEventData eventData, AnalyticsType analyticsType);
		ServerResponseAnalytic GetServerResponseTime();
        void OnPlayerCurrenciesDecrease(PlayerResources.CurencyType curencyType, int amount, CurrenciesSpendingPlace place, string target);
        void OnPlayerCurrenciesDecrease(Price price, CurrenciesSpendingPlace place, string target);
        void OnPlayerCurrenciesIncrease(PlayerResources.CurencyType curencyType, int amount, AwardSourcePlace sourcePlace, string source);
        void OnInAppPurchase(string storeId, float price);
        void OnInAppPurchaseFail(string orderId, string status);

        #region TheEvent

        void OnBuildStageBuilding(int buildingStage);
        void OnPlaceOnTheMapRewardedBuilding();
        void TheEventStartInfo(string eventName, int day, int buildingStage);
        void OnPurchaseNanoPass(string storeId);

        #endregion

        /// <summary>
        /// Define the event params and send event
        /// </summary>
        /// <param name="eventData">Custom event data</param>
        /// <param name="analyticsType">Which tracker type need to use for send analytics data</param>
        void SendEvent(CustomEventData eventData, AnalyticsType analyticsType);

        void SkipIntroVideo();

        void SendRequestError(IErrorData errorData);
        void OnAdsWatched(string source);
        void OnOrderSent(string businessSectorsTypes, int orderId, string reward);
        void OnExperienceUpdate(AwardSourcePlace sourcePlace, string source, int experience);
        void OnUiClick(string target);
        void OnShuffle(string target, string price, string slotId);
        void BuySoukSlot(string target, int slotId, string rewards);
        void FoodSupplyCook(string foodSupplyRecipeId, string mealId);
        void FoodSupplyFeed(int resourcesAmount, int resourcesUsed, float totalFoodSupplyAmount);
        void FoodSupplyGift(int giftId);
        void UnlockSlotEvent(string target, int slotId, string position = "main");
    }
}
