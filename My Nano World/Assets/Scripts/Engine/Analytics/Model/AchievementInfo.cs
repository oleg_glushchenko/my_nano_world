﻿using System.Collections.Generic;
using NanoReality.GameLogic.Quests;

namespace Assets.Scripts.Engine.Analytics.Model
{
    public class AchievementInfo
    {
        /// <summary>
        /// Имя ачивки
        /// </summary>
        public string AchievementName { get; set; }

        /// <summary>
        /// ID ачивки
        /// </summary>
        public int AchievementID { get; set; }

        /// <summary>
        /// Награда за ачивку
        /// </summary>
        public List<IAwardAction> AchievementRewards { get; set; }

        public AchievementInfo(string achievementName, int achievementID, List<IAwardAction> achievementRewards)
        {
            AchievementName = achievementName;
            AchievementID = achievementID;
            AchievementRewards = achievementRewards;
        }
    }
}