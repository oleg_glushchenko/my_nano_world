﻿using Newtonsoft.Json;

namespace Assets.Scripts.Engine.Analytics.Model
{
    public class ConversionData
    {
        [JsonProperty("is_first_launch")]
        public bool IsFirstLaunch { get; set; }
        
        [JsonProperty("media_source")]
        public string AcquisitionChannel { get; set; }

        [JsonProperty("af_status")]
        public string AfStatus { get; set; }

        [JsonProperty("af_message")]
        public string AfMessage { get; set; }

        [JsonProperty("campaign")]
        public string Campaign { get; set; }

        [JsonProperty("clickid")]
        public string ClickId { get; set; }

        [JsonProperty("af_siteid")]
        public string AfSiteid { get; set; }

        [JsonProperty("af_sub1")]
        public string AfSub1 { get; set; }

        [JsonProperty("agency")]
        public string Agency { get; set; }

        [JsonProperty("is_fb")]
        public bool IsFb { get; set; }

        [JsonProperty("adgroup")]
        public string AdGroup { get; set; }

        [JsonProperty("adgroup_id")]
        public string AdGroupId { get; set; }

        [JsonProperty("campaign_id")]
        public string CampaignId { get; set; }

        [JsonProperty("adset")]
        public string AdSet { get; set; }

        [JsonProperty("adset_id")]
        public string AdsetId { get; set; }

        [JsonProperty("ad_id")]
        public string AdId { get; set; }
    }
}