﻿namespace Assets.Scripts.Engine.Analytics.Model
{
    /// <summary>
    /// Данные о действии в UI
    /// </summary>
    public class UiIteractionInfo
    {
        /// <summary>
        /// show, click etc
        /// </summary>
        public UiActionType UiAction { get; set; }

        /// <summary>
        /// interaction name
        /// </summary>
        public string UiName { get; set; }

        /// <summary>
        /// UI element type (banner,button etc)
        /// </summary>
        public UiElementType UiType { get; set; }

        /// <summary>
        /// main_menu, prefail_window etc
        /// </summary>
        public string UiLocation { get; set; }

        public UiIteractionInfo(UiActionType uiAction, string uiName, UiElementType uiType, string uiLocation = null)
        {
            UiAction = uiAction;
            UiName = uiName;
            UiType = uiType;
            UiLocation = uiLocation;
        }
    }

    public enum UiActionType
    {
        Click,
        Show,
        Hide
    }

    public enum UiElementType
    {
        Panel,
        Tab,
        Button,
        Popup
    }
}