﻿using Assets.Scripts.Engine.Analytics.Extensions.CurrencyInfo;
using Assets.Scripts.Engine.Analytics.Model;
using Assets.Scripts.GameLogic.DailyBonus.Impl;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.SocialCommunicator.api;
using strange.extensions.signal.impl;

namespace Assets.Scripts.Engine.Analytics
{
    public class SignalOnActionWithCurrency : Signal<CurrencyInfo> { }

    public class SignalOnFirstPay : Signal<int, string> { }
    
    public class SignalOnUIAction : Signal<UiIteractionInfo> { }

    public class SignalOnTaxesCollection : Signal<int> { }

    public class SignalOnSkipProduction : Signal<SkipProductionInfo> { }

    public class SignalOnSkipBuilding : Signal<SkipBuildingInfo> { }
    public class SignalOnAchievementCompleted : Signal<AchievementInfo> { }
    public class SignalOnInviteSentAction : Signal<InviteInfo> { }
    public class SignalOnSocialConnectAction : Signal<AuthorizeResult> { }
    public class SignalOnBuyMissingProductsAction : Signal<MissingProductsInfo> { }
    public class SignalOnRequestSendAction : Signal<RequestSendInfo> { }
    public class SignalOnRequestReceivedAction : Signal<RequestReceivedInfo> { }
    public class SignalOnUnlockSectorAction : Signal<string> { }
    public class SignalOnRepairBuildingAction : Signal<IMapBuilding> { }
    public class SignalOnDailyBonusCollected : Signal<DailyBonusData> { }

    public class SignalOnCityNameChanged : Signal<string> { }
    
    /// <summary>
    /// Dispatched when language was changed in settings menu
    /// string - locale name
    /// </summary>
    public class SignalOnSettingsLanguageChanged : Signal<string> { }
}
