﻿/// <summary>
/// тип действия связанный с просмотром рекламы
/// </summary>
public enum AdTypePanel
{
    None = -1, // no ads
    CreateNewOrder = 1, //a player can get a new order (or replace an order)
    FillOneOrder, //a player can watch a video to fill one order without loading resources
    RemoveOrderInMarket, //a player can remove an item from the market with a video (item he put for sale but don’t want to sell it anymore)
    RefreshOrdersInMarket, //a player can refresh the market with a video (we will increase the time to 10 mins instead of 30 seconds)
    FillFoodSupply, //a player can watch a video to fill it without loading it with resources
    RestaurantPlay, //a player can only play the game X times per day for free. Player can play more by Premium currency or watching a video
    CasinoFreeSpin,
    Products,
    NanoCoins,
    MetroShuffle,
    SeaportShuffle,
    BazaarShuffle
}