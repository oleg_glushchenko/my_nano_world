﻿using Newtonsoft.Json;

public class AdStructData
{
    [JsonProperty("count")]
    public int AvailableAdsCount { get; set; }
    
    [JsonProperty("time")]
    public long AdsGenerationTime { get; set; }
}