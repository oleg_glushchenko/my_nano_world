﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NanoLib.Core.Logging;
using Newtonsoft.Json;
using Sirenix.OdinInspector;
using UniRx;
using UnityEngine;
using UnityEngine.Networking;

namespace NanoReality.Engine.Settings.BuildSettings
{
    [CreateAssetMenu(fileName = "NetworkSettings", menuName = "NanoReality/Settings/NetworkSettings", order = 1)]
    [ShowOdinSerializedPropertiesInInspector]
    public class NetworkSettings : SerializedScriptableObject
    {
        [Header("Custom server Url")]
        [Tooltip("Game will load server url from player preferences and ignore current server from game settings.")]
        [SerializeField] private bool _useCustomServer;

        [ValueDropdown("GetServers", AppendNextDrawer = true)]
        [SerializeField] private ServerSettings _currentServer;
        
        [Header("Server")]
        [SerializeField] private List<ServerSettings> _serversList;

        [Header("Network")]
        [Tooltip("Are we need to simulate network?")]
        [SerializeField] private bool _simulateNetwork;

        [Tooltip("Amount of network Delay. For simulate Network.")]
        [SerializeField] private float _simulateNetworkDelay;
        
        [SerializeField] private int _networkTimeoutRequestTime = 10;

        [Tooltip("Interval in seconds to execute time syncronization with server")]
        [SerializeField]private int _serverTimeSyncInterval = 10;

        [Tooltip("In seconds")]
        [SerializeField] private float _poorConnectionAverageMaxLatency = 1.5f;
        
        [Header("Master Server")]
        [SerializeField] private bool _useMasterServer;

        [Tooltip("The master server determines a specific game server for this client.")]
        [SerializeField] private string _masterServerUrl;
        
        #region Properties

        public float SimulateNetworkDelay => _simulateNetworkDelay;

        public bool SimulateNetwork => _simulateNetwork;

        public int ServerTimeSyncInterval => _serverTimeSyncInterval;

        public int NetworkTimeoutRequestTime => _networkTimeoutRequestTime;

        public string CurrentServerName => _currentServer.Name;

        public string ServerAddress => ServerSettings.GameServerUrl;

        public List<ServerSettings> ServersList => _serversList;

        public string AssetBundlesUrl => ServerSettings.AssetBundlesUrl;

        public float PoorConnectionAverageMaxLatency => _poorConnectionAverageMaxLatency;
        
        public string MasterServerUrl => _masterServerUrl;

        public bool UseMasterServer
        {
            get => _useMasterServer;
            set => _useMasterServer = value;
        }
        
        public bool UseCustomServer
        {
            get => _useCustomServer;
            set => _useCustomServer = value;
        }

        public ServerSettings ServerSettings
        {
            get => _currentServer;
            set => _currentServer = value;
        }

        #endregion

#if UNITY_EDITOR

        [Button("Update Available Servers List")]
        public void UpdateAvailableServersList()
        {
            const string url = "https://development.nanoreality.com/server.json";
            
            UnityWebRequestAsyncOperation unityWebRequest = UnityWebRequest.Get(url).SendWebRequest();
            Observable.FromCoroutine(RequestCoroutine).Subscribe();

            unityWebRequest.completed += (c) =>
            {
                string responseString = unityWebRequest.webRequest.downloadHandler.text;
                List<ServerSettings> serversList = null;
                try
                {
                    serversList = JsonConvert.DeserializeObject<List<ServerSettings>>(responseString);
                }
                catch (Exception e)
                {
                    Debug.LogError($"Message: {responseString}. Exception:{e}");
                    return;
                }

                _serversList = serversList;
                UnityEditor.EditorUtility.SetDirty(this);
            };
            IEnumerator RequestCoroutine()
            {
                while (!unityWebRequest.isDone)
                {
                    yield return new WaitForSeconds(0.1f);
                    Logging.Log(LoggingChannel.Network, "Servers list updated.");
                }
            }
        }

        /// <summary>
        /// Used for editor serialization.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<ValueDropdownItem> GetServers()
        {
            return _serversList.Select(c => new ValueDropdownItem(c.Name, c));
        }
        
#endif
    }

    [Serializable]
    public class ServerSettings
    {
        [JsonProperty("name")]
        public string Name;
        
        [JsonProperty("url")]
        public string GameServerUrl;
        
        [JsonProperty("asset_bundles_url")]
        public string AssetBundlesUrl;
    }
}