﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Engine.UserReporting.Model.api
{
    public interface IUserReporting
    {
        void Send(string head, string text);
    }
}
