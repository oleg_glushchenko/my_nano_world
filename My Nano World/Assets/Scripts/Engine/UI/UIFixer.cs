using UnityEngine;

namespace Engine.UI
{
    public class UIFixer : MonoBehaviour
    {
        [SerializeField] private FixOptions _fixOption;
        [SerializeField] private float _valueToMove;

        void Start()
        {
            if (I2.Loc.LocalizationManager.CurrentLanguage == "en") return;

            RectTransform rectTransform = GetComponent<RectTransform>();
            switch (_fixOption)
            {
                case FixOptions.MoveY:
                    rectTransform.anchoredPosition = new Vector2(rectTransform.anchoredPosition.x, _valueToMove);
                    break;
            }
        }
    }

    enum FixOptions
    {
        MoveY
    }
}