using DG.Tweening;
using UnityEngine;

namespace NanoReality.Engine.UI
{
    public class LoadingSpinnerV2 : MonoBehaviour
    {
        private Tweener _rotationTween;

        [SerializeField] private int _angle = 360;
        [SerializeField] private float _duration =1f;
        [SerializeField] private RotateMode _mode = RotateMode.FastBeyond360;
        [SerializeField] private RectTransform _spinnerRectTransform;

        public void StartSpin()
        {
            transform.gameObject.SetActive(true);
            _rotationTween = _spinnerRectTransform.DOLocalRotate(new Vector3(0, 0, _angle), _duration, _mode).SetLoops(-1);
        }

        public void StopSpin()
        {
            _rotationTween?.Kill();
            transform.gameObject.SetActive(false);
            _spinnerRectTransform.localRotation = Quaternion.identity;
        }

        private void OnDisable()
        {
            _rotationTween?.Kill();
        }
    }
}