﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Engine.UI
{
    [ShowOdinSerializedPropertiesInInspector]
    [CreateAssetMenu(fileName = "UIPrefabLinks", menuName = "NanoReality/UI/UIPrefabLinks", order = 10)]
    public class UIPrefabLinks : ScriptableObject
    {
        [SerializeField]
        [OnValueChanged("SetType")]
        private List<GameObject> _prefabLinks = new List<GameObject>();

        private Dictionary<Type, GameObject> _prefabLinksLocal = new Dictionary<Type, GameObject>();

        private Dictionary<Type, GameObject> _prefabLinksPublic
        {
            get
            {
                if (_prefabLinksLocal.Count == 0)
                {
                    SetType();
                }

                return _prefabLinksLocal;
            }
        }

        public GameObject GetGameObject(Type type)
        {
            if (!_prefabLinksPublic.TryGetValue(type, out var prefab))
            {
                Debug.LogError($"ViewPrefab of type {type.Name} not found in PrefabsLinks.");
            }
            
            return prefab;
        }

        private void SetType()
        {
            _prefabLinksLocal.Clear();
            
            foreach (var gameObject in _prefabLinks)
            {
                if (gameObject == null)
                {
                    Debug.LogWarning($"UIPrefabLinks contains nullable list items.");
                    continue;
                }
                var viewPrefab = gameObject.GetComponent<View>();
                if (viewPrefab == null)
                {
                    Debug.LogError($"ViewPrefab of type {gameObject.name} not found in PrefabsLinks.", viewPrefab);
                    continue;
                }

                var viewType = viewPrefab.GetType();
                if (!_prefabLinksLocal.ContainsKey(viewType))
                {
                    _prefabLinksLocal.Add(viewType, gameObject);
                }
            }

        }

        [Button("Update links")]
        private void UpdateLinks()
        {
            SetType();
        }
    }
}