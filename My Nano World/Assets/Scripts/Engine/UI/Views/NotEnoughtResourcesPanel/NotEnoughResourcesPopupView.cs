using strange.extensions.signal.impl;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using Assets.NanoLib.UI.Core.Components;
using UnityEngine;
using UnityEngine.UI;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using Assets.Scripts.Engine.Analytics;
using Assets.Scripts.Engine.Analytics.Model;
using Assets.Scripts.Engine.Analytics.Model.api;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Assets.Scripts.Engine.UI.Views.Tooltips;
using Assets.Scripts.Engine.UI.Views.Trade.Items;
using Assets.Scripts.GameLogic.Utilites;
using Assets.Scripts.MultiLanguageSystem.Logic;
using Engine.UI.Extensions.ThereIsNoVideoPanel;
using NanoLib.Core.Services.Sound;
using NanoLib.UI.Core;
using NanoReality.Core.Resources;
using NanoReality.GameLogic.PlayerProfile.Models.Impl;
using TMPro;
using TooltipType = Assets.Scripts.Engine.UI.Views.Tooltips.TooltipType;
using NanoReality.GameLogic.Services;
using NanoReality.GameLogic.Utilites;

namespace NanoReality.Engine.UI.Views.NotEnoughtResourcesPanel
{
    public class SignalShowNotEnoughtResourcesPanel : Signal { }
    
    public class NotEnoughResourcesPopupView : BasePopupView
    {
        [SerializeField] private ItemContainer _container;
        [SerializeField] private ProductCountCard _productCountCard;
        [SerializeField] private TextMeshProUGUI _costTest;
        [SerializeField] private TextMeshProUGUI _message;
        [SerializeField] private RectTransform _panelRect;
        [SerializeField] private int _minPanelWidth = 720;
        [SerializeField] private int _maxPanelWidth = 960;
        [SerializeField] private int _itemsLimit = 7;
        [SerializeField] private Button _buyButton;
        [SerializeField] private Button _tradeButton;
        [SerializeField] private TextMeshProUGUI _tradeButtonTittle;
        [SerializeField] private Button _closeButton;
        [SerializeField] private Button _closeBGButton;
        [SerializeField] private Image _currencyIcon;

        private readonly Dictionary<Id<Product>, int> _notEnoughProducts = new Dictionary<Id<Product>, int>();

        private Price _itemPrice;
        private NotEnoughResourcesPopupSettings _thisSettings;
        
        private int _cost;
        private bool _isButtonsLocked = true;

        public Button BuyButton => _buyButton;
        
        #region Injects

        [Inject] public SignalShowNotEnoughtResourcesPanel jSignalShowNotEnoughtResourcesPanel { get; set; }
        [Inject] public SignalOnNeedToShowTooltip jSignalOnNeedToShowTooltip { get; set; }
        [Inject] public SignalOnNeedToHideTooltip jSignalOnNeedToHideTooltip { get; set; }
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public IResourcesManager jResourcesManager { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IProductService jProductService { get; set; }
        [Inject] public SignalOnBuyMissingProductsAction jSignalOnBuyMissingProductsAction { get; set; }
        [Inject] public SignalShowThereIsNoVideoPanel jSignalShowThereIsNoVideoPanel { get; set; }
        [Inject] public SignalOnUIAction jSignalOnUIAction { get; set; }
        [Inject] public IIconProvider jIconProvider { get; set; }
        [Inject] public IUIManager jUIManager { get; set; }
        [Inject] public INanoAnalytics jNanoAnalytics { get; set; }
        [Inject] public ISoundManager jSoundManager { get; set; }
        

        #endregion

        #region Methods

        /// <summary>
        /// Инициализация
        /// </summary>
        /// <param name="settings">Настройки</param>
        public override void Initialize(IPopupSettings settings)
        {
            base.Initialize(settings);
            jSignalShowNotEnoughtResourcesPanel.Dispatch();

            if (!_container.IsInstantiateed)
                _container.InstaniteContainer(_productCountCard, 0);

            _thisSettings = (NotEnoughResourcesPopupSettings)settings;
            
            jSoundManager.Play(SfxSoundTypes.AppWindow, SoundChannel.SoundFX);
            
            UpdatePanel(_thisSettings.Price);
            UnlockButtons();

            _buyButton.onClick.RemoveAllListeners();

            if (!_thisSettings.BuyResourcesForLanterns)
            {
                _currencyIcon.sprite = jIconProvider.GetCurrencySprite(PriceType.Hard);
                _buyButton.onClick.AddListener(OnBuy);
                _message.text = LocalizationUtils.Localize("NOTENOUGHRES_MSG_GET_OR_PURCHASE_RESOURCES"); 
            }
            else
            {
                _currencyIcon.sprite = jIconProvider.GetCurrencySprite(PriceType.Lantern);
                _buyButton.onClick.AddListener(OnBuyForLanterns);
                _costTest.text = _thisSettings.LanternPrice.ToString();
                _cost = _thisSettings.LanternPrice;
                _message.text = LocalizationUtils.LocalizeUI(LocalizationKeyConstants.EVENTS_PURCHASE_WITH_LANTERNS);
            }

            jSignalShowThereIsNoVideoPanel.AddListener(Hide);
        }

        /// <summary>
        /// Выставляем ширину панели в зависимости от количества итемов
        /// </summary>
        private void CheckPanelWidth()
        {
            if (_container.CurrentItems.Count >= _itemsLimit)
                _panelRect.sizeDelta = new Vector2(_maxPanelWidth, _panelRect.rect.height);
            else
                _panelRect.sizeDelta = new Vector2(_minPanelWidth, _panelRect.rect.height);
        }

        public override void Hide()
        {
            jSignalOnUIAction.Dispatch(new UiIteractionInfo(UiActionType.Hide, "NotEnoughtResources", UiElementType.Popup));
            jSignalShowThereIsNoVideoPanel.RemoveAllListeners();

            CurrentPopupResult = PopupResult.Hide;
            jSoundManager.Play(SfxSoundTypes.CloseWindow, SoundChannel.SoundFX);
            base.Hide();
        }

        private void UpdatePanel(Price price)
        {
            _itemPrice = price;
            _notEnoughProducts.Clear();
            _container.ClearCurrentItems();

            _cost = 0;

            if (_itemPrice.IsNeedProducts)
            {
                foreach (var product in _itemPrice.ProductsPrice)
                {
                    var current = jPlayerProfile.PlayerResources.GetProductCount(product.Key);
                    var productsCount = product.Value - current;
                    if (productsCount > 0)
                    {
                        var productObject = jProductService.GetProduct(product.Key);
                        var item = (ProductCountCard)_container.AddItem();

                        ProductCountCardInitData productInitData = new ProductCountCardInitData()
                        {
                            ProductObject = productObject,
                            ProductSprite = jResourcesManager.GetProductSprite(product.Key.Value),
                            SpecialOrderSprite = jIconProvider.GetSpecialOrderIcon(productObject.SpecialOrder),
                            ShowOnlySimpleTools = false,//true
                            ProductsCount = productsCount,
                            CurrentProductsCount = null,
                            NeedProductsCount = null,
                            IsCountNeed = true,
                            Type = TooltipProductType.Product
                        };

                        item.Initialize(productInitData);

                        item.SignalOnSelected.RemoveListener(OnSelectItem);
                        item.SignalOnSelected.AddListener(OnSelectItem);

                        item.SignalOnHideTooltip.RemoveListener(OnItemHide);
                        item.SignalOnHideTooltip.AddListener(OnItemHide);

                        _cost += (productsCount * jProductService.GetProduct(product.Key).HardPrice);
                        _notEnoughProducts.Add(product.Key, productsCount);
                    }
                }
                _costTest.text = _cost.ToString();
            }

            CheckPanelWidth();
            
            _tradeButtonTittle.SetLocalizedText(LocalizationUtils.Localize(LocalizationKeyConstants.TOOLTIP_BTN_GO_TO_SHOP));
        }

        private void OnItemHide()
        {
            jSignalOnNeedToHideTooltip.Dispatch();
        }

        private void OnSelectItem(TooltipData tooltipData)
        {
            switch (tooltipData.Type)
            {
                case TooltipType.ProductItems:
                    tooltipData.OnGoToClickAction = (Hide);
                    break;
            }
            jSignalOnNeedToShowTooltip.Dispatch(tooltipData);
        }

        private void OnBuy()
        {
            jSignalOnUIAction.Dispatch(new UiIteractionInfo(UiActionType.Click, "Confirm_NotEnoughtResources", UiElementType.Button));

            if (_cost <= jPlayerProfile.PlayerResources.HardCurrency)
            {
                var callback = CurrentPopupCallback;
                CurrentPopupCallback = null;
                Hide();

                jServerCommunicator.PurchaseProducts(jPlayerProfile.UserCity.Id, _notEnoughProducts);

                jPlayerProfile.PlayerResources.AddProducts(_notEnoughProducts);
                jPlayerProfile.PlayerResources.BuyHardCurrency(_cost);

                var products = new StringBuilder();
                foreach (var product in _notEnoughProducts)
                {
                    products.Append(product.Key.Value + " ");
                }

                jNanoAnalytics.OnPlayerCurrenciesDecrease(PlayerResources.CurencyType.Bucks, _cost, CurrenciesSpendingPlace.NotEnoughtResourcesBuy, products.ToString());
                jSignalOnBuyMissingProductsAction.Dispatch(new MissingProductsInfo(_notEnoughProducts, _cost));

                callback?.Invoke(PopupResult.Ok);
                return;
            }

            jPopupManager.Show(new NotEnoughMoneyPopupSettings(PriceType.Hard, _cost));
            OnCancelButtonClick();
        }

        private void OnBuyForLanterns()
        {

            jSignalOnUIAction.Dispatch(new UiIteractionInfo(UiActionType.Click, "Confirm_NotEnoughtResources",
                UiElementType.Button));

            if (_cost <= jPlayerProfile.PlayerResources.LanternCurrency)
            {
                var callback = CurrentPopupCallback;
                CurrentPopupCallback = null;
                Hide();

                jServerCommunicator.PurchaseEventProduct(_notEnoughProducts.First().Key, _notEnoughProducts.First().Value);
                jPlayerProfile.PlayerResources.AddProducts(_notEnoughProducts);
                jPlayerProfile.PlayerResources.RemoveLanternCurrency(_cost);

                var products = new StringBuilder();
                foreach (var product in _notEnoughProducts)
                {
                    products.Append(product.Key.Value + " ");
                }

                jNanoAnalytics.OnPlayerCurrenciesDecrease(PlayerResources.CurencyType.Lanterns, _cost, CurrenciesSpendingPlace.NotEnoughtResourcesBuy, products.ToString());
                jSignalOnBuyMissingProductsAction.Dispatch(new MissingProductsInfo(_notEnoughProducts, _cost));

                callback?.Invoke(PopupResult.Ok);

                return;
            }

            jPopupManager.Show(new NotEnoughMoneyPopupSettings(PriceType.Lantern, _thisSettings.LanternPrice));
       
            OnCancelButtonClick();
        }

        private void OnGoTo()
        {
            jUIManager.HideAll();
            jProductService.GoToShop(GetTargetProductForGoTo());
            Hide();
        }

        private Id<Product> GetTargetProductForGoTo()
        {
            Id<Product> result = -9999;

            if (_itemPrice == null || _itemPrice.ProductsPrice == null)
            {
                return result;
            }
            
            foreach (var item in _itemPrice.ProductsPrice)
            {
                var current = jPlayerProfile.PlayerResources.GetProductCount(item.Key);
                if (current < item.Value)
                {
                    result = item.Key;
                    break;
                }
            }
            
            return result;
        }

        private void LockButtons()
        {
            if (!_isButtonsLocked)
            {
                _isButtonsLocked = true;
                _buyButton.onClick.RemoveListener(OnBuy);
                _tradeButton.onClick.RemoveListener(OnGoTo);
                _closeButton.onClick.RemoveListener(Hide);
                _closeBGButton.onClick.RemoveListener(Hide);
            }
        }

        private void UnlockButtons()
        {
            if (_isButtonsLocked)
            {
                _isButtonsLocked = false;
                _buyButton.onClick.AddListener(OnBuy);
                _tradeButton.onClick.AddListener(OnGoTo);
                _closeButton.onClick.AddListener(Hide);
                _closeBGButton.onClick.AddListener(Hide);
            }
        }

        #endregion
    }
}
