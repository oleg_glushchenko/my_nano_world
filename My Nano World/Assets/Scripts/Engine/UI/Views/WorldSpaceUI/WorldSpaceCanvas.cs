﻿using System.Collections.Generic;
using Assets.NanoLib.Utilities.Pulls;
using NanoReality.Engine.UI.Views.WorldSpaceUI;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using strange.extensions.mediation.impl;
using UnityEngine;
using Assets.NanoLib.UI.Core.Components;
using NanoReality.Engine.UI.Components;
using Assets.Scripts.Engine.BuildingSystem.MapObjects.Components;
using Assets.Scripts.Engine.UI.Extensions.BuildFx;
using Assets.Scripts.Engine.UI.Extensions.FireFx;
using Assets.Scripts.Engine.UI.Extensions.Highlight;
using Assets.Scripts.Engine.UI.Extensions.TerrainFx;
using Assets.Scripts.Engine.UI.Views.WorldSpaceUI;
using NanoLib.Core.Pool;
using NanoReality.Engine.UI.UIAnimations;
using NanoReality.Engine.UI.Views.Panels;
using strange.extensions.injector.api;

namespace NanoReality.Engine.UI
{
    public class WorldSpaceCanvas : View, IWorldSpaceCanvas
    {
        [SerializeField] private RectTransform _buildingAttentionsParent;
        [SerializeField] private BuildingStatesAttentionView _buildingAttentionViewPrefab;
        [SerializeField] private RectTransform _longTapProgressParent;
        [SerializeField] private LongTapProgressView _longTapProgressPrefab;

        [SerializeField] private RectTransform _readyProductsParent;
        [SerializeField] private ReadyProductView _readyProductViewPrefab;

        [SerializeField] private Transform _progressBarContainer;
        
        [SerializeField] private Transform _citizenGiftsParent;
        [SerializeField] private CitizenGiftAttentionView _citizenGiftAttentionPrefab;
        [SerializeField] private BuildingSatisfactionEffectHandler _buildingSatisfactionEffectHandler;

        public RectTransform ReadyProductsParent => _readyProductsParent;
        public ReadyProductView ReadyProductViewPrefab => _readyProductViewPrefab;
        public RectTransform BuildingAttentionsParent => _buildingAttentionsParent;
        public BuildingStatesAttentionView BuildingAttentionViewPrefab => _buildingAttentionViewPrefab;
        public Transform CitizensGiftsAttentionsParent => _citizenGiftsParent;

        public WorkInProgressEffect WorkInProgressEffectPrefab;
        public HighlightEffect HighlightEffectPrefab;

        public BuildingSatisfactionChangeEffect BuildingSatisfactionChangePrefab;
        public RoadFinishedEffect RoadFinishedEffectPrefab;
        public SubSectorUnlockEffect SubSectorUnlockEffectPrefab;

        public PopulationInfoView PopulationInfoViewPrefab;

        public ConstructionProgressBar ConstructionProgressBarPrefab;

        public LockedSectorLabel lockedSectorPrefab;
        public NonPlayableBuildUI NonPlayableBuildPrefab;
        public HintOverObjectUI HintOverObjectPrefab;
        public FloatingTextObject FloatingTextObjectPrefab;


        public AoeHiglightParticle HappinessHiglightParticle;
        public AoeHiglightParticle EnergyiglightParticle;
        public AoeHiglightParticle SupplyHighlightParticle;

        public MapObjectLooker StartRoadTile;
        public MapObjectLooker EndRoadTile;

        public BuildingDowntimeEffect BuildingDowntimePrefab;
        public ItemContainer DowntimeItemContainer;

        public ItemContainer SatisfactionItemContainer;
        public ItemContainer RoadFinishItemContainer;
        public ItemContainer UnlockedSubSectorItemContainer;
        public ItemContainer WorkInProgressEffectContainer;
        public ItemContainer NonPlayableBuildsContainer;
        public ItemContainer HintOverObjectsContainer;
        public ItemContainer HintsContainer;
        public ItemContainer LockedSubSectorsContainer;
        public ItemContainer HappinessParticlesContainer;
        public ItemContainer SupplyParticlesContainer;
        public ItemContainer EnergyParticlesContainer;
        public ItemContainer PopulationInfoContainer;

        [Header("Effects")] 
        public ItemContainer TerrainGroundEffectItemContainer;
        public ItemContainer TerrainDustEffectItemContainer;
        public ItemContainer TerrainWaterEffectItemContainer;
        public ItemContainer TerrainRoadEffectItemContainer;

        public TerrainEffect TerrainGroundEffectPrefab;
        public TerrainEffect TerrainDustEffectPrefab;
        public TerrainEffect TerrainWaterEffectPrefab;
        public TerrainEffect TerrainRoadEffectPrefab;

        public BuildingUpgradeEffect BuildingUpgradeEffectPrefab;
        public ItemContainer UpgradeEffectItemContainer;

        public BuildingSpeedUpEffect BuildingSpeedUpEffectPrefab;
        public ItemContainer SpeedUpEffectItemContainer;

        public BuildingBuildEffect BuildingBuildEffectPrefab;
        public ItemContainer BuildEffectItemContainer;
        public ItemContainer HighlightEffectItemContainer;

        public FireEffect FireEffectPrefab;
        public ItemContainer FireEffectItemContainer;


        private readonly IUnityPool<CitizenGiftAttentionView> _citizenGiftsPool = new UnityPool<CitizenGiftAttentionView>();
        private readonly IUnityPool<LongTapProgressView> _longTapProgressPool = new UnityPool<LongTapProgressView>();
        
        private readonly List<ConstructionProgressBar> _activeConstractionProgressBars = new List<ConstructionProgressBar>();
        
        [Inject] public IObjectsPull jConstractionProgressbarsPull { get; set; }
        [Inject] public IObjectsPull jLockesSubSectorPull { get; set; }
        [Inject] public StartCityMapInitializationSignal jStartCityMapInitializationSignal { get; set; }
        [Inject] public UpdateWorldSpaceDepth jUpdateWorldSpaceDepthSignal { get; set; }
        [Inject] public IInjectionBinder jInjectionBinder { get; set; }

        protected override void Start ()
		{
			base.Start ();
            InitializePools();
            jStartCityMapInitializationSignal.AddOnce(map =>
            {
                InitializePools();
                PopLongTapProgress();
            });
            jUpdateWorldSpaceDepthSignal.AddListener(OrderProgressBars);
		}

        protected override void OnDestroy()
        {
            base.OnDestroy();
            jUpdateWorldSpaceDepthSignal.RemoveListener(OrderProgressBars);
        }

        private void InitializePools()
        {
            jConstractionProgressbarsPull.InstanitePull(ConstructionProgressBarPrefab, 0);
            jLockesSubSectorPull.InstanitePull(lockedSectorPrefab, 0);

            _citizenGiftsPool.InstanceProvider = new PrefabInstanceProvider(_citizenGiftAttentionPrefab);
            _longTapProgressPool.InstanceProvider = new PrefabInstanceProvider(_longTapProgressPrefab);

            DowntimeItemContainer.InstaniteContainer(BuildingDowntimePrefab, 2);
            WorkInProgressEffectContainer.InstaniteContainer(WorkInProgressEffectPrefab, 2);
            NonPlayableBuildsContainer.InstaniteContainer(NonPlayableBuildPrefab, 10);
            HintOverObjectsContainer.InstaniteContainer(HintOverObjectPrefab, 2);
            HintsContainer.InstaniteContainer(FloatingTextObjectPrefab, 2);
            BuildEffectItemContainer.InstaniteContainer(BuildingBuildEffectPrefab, 1);
            SpeedUpEffectItemContainer.InstaniteContainer(BuildingSpeedUpEffectPrefab, 1);
            UpgradeEffectItemContainer.InstaniteContainer(BuildingUpgradeEffectPrefab, 2);
            SatisfactionItemContainer.InstaniteContainer(BuildingSatisfactionChangePrefab, 0);
            RoadFinishItemContainer.InstaniteContainer(RoadFinishedEffectPrefab, 10);

            UnlockedSubSectorItemContainer.InstaniteContainer(SubSectorUnlockEffectPrefab, 1);
            HappinessParticlesContainer.InstaniteContainer(HappinessHiglightParticle, 10);

            EnergyParticlesContainer.InstaniteContainer(EnergyiglightParticle, 10);

            SupplyParticlesContainer.InstaniteContainer(SupplyHighlightParticle, 10);

            PopulationInfoContainer.InstaniteContainer(PopulationInfoViewPrefab, 1);
            HighlightEffectItemContainer.InstaniteContainer(HighlightEffectPrefab, 1);
            FireEffectItemContainer.InstaniteContainer(FireEffectPrefab, 1);
            TerrainGroundEffectItemContainer.InstaniteContainer(TerrainGroundEffectPrefab, 1);
            TerrainDustEffectItemContainer.InstaniteContainer(TerrainDustEffectPrefab, 1);
            TerrainWaterEffectItemContainer.InstaniteContainer(TerrainWaterEffectPrefab, 1);
            TerrainRoadEffectItemContainer.InstaniteContainer(TerrainRoadEffectPrefab, 1);
        }

        public PopulationInfoView GetPopulationInfoView()
        {
            var res = PopulationInfoContainer.AddItem() as PopulationInfoView;
            var _tmp = res.transform;
            _tmp.SetParent(PopulationInfoContainer.transform);
            _tmp.localScale = Vector3.one;
            return res;
        }

        public void ShowHint(Vector3 worldPoint, string message)
        {
            var hint = HintsContainer.AddItem() as FloatingTextObject;
            if (hint != null)
            {
                hint.Show(worldPoint, message);
            }
        }

        public BuildingDowntimeEffect GetDowntimeItem()
        {
            var res = DowntimeItemContainer.AddItem() as BuildingDowntimeEffect;
			Transform _tmp = res.transform;
			_tmp.SetParent(DowntimeItemContainer.transform);
			_tmp.localScale = Vector3.one;
			return res;
        }

        public WorkInProgressEffect GetWorkInProgressItem()
        {
            var res = WorkInProgressEffectContainer.AddItem() as WorkInProgressEffect;
            Transform _tmp = res.transform;
            _tmp.SetParent(transform);
            _tmp.localScale = Vector3.one;
            return res;
        }

        public BuildingBuildEffect GetBuildEffect()
        {
            var res = BuildEffectItemContainer.AddItem() as BuildingBuildEffect;
            Transform _tmp = res.transform;
            _tmp.SetParent(transform);
            _tmp.localScale = Vector3.one;
            return res;
        }

        public HighlightEffect GetHighlightFxItem()
        {
            if (HighlightEffectItemContainer == null)
            {
                return null;
            }
            var res = HighlightEffectItemContainer.AddItem() as HighlightEffect;
            Transform _tmp = res.transform;
            _tmp.SetParent(transform);
            _tmp.localScale = Vector3.one;
            return res;
        }

        public FireEffect GetFireFxItem()
        {
            if (FireEffectItemContainer == null)
            {
                return null;
            }
            var res = FireEffectItemContainer.AddItem() as FireEffect;
            Transform _tmp = res.transform;
            _tmp.SetParent(transform);
            _tmp.localScale = Vector3.one;
            return res;
        }

        public TerrainEffect GetTerrainGroundFxItem()
        {
            if (TerrainGroundEffectItemContainer == null)
            {
                return null;
            }
            var res = TerrainGroundEffectItemContainer.AddItem() as TerrainEffect;
            Transform _tmp = res.transform;
            _tmp.SetParent(transform);
            _tmp.localScale = Vector3.one;
            return res;
        }

        public TerrainEffect GetTerrainDustFxItem()
        {
            if (TerrainDustEffectItemContainer == null)
            {
                return null;
            }
            var res = TerrainDustEffectItemContainer.AddItem() as TerrainEffect;
            Transform _tmp = res.transform;
            _tmp.SetParent(transform);
            _tmp.localScale = Vector3.one;
            return res;
        }

        public TerrainEffect GetTerrainWaterFxItem()
        {
            if (TerrainWaterEffectItemContainer == null)
            {
                return null;
            }
            var res = TerrainWaterEffectItemContainer.AddItem() as TerrainEffect;
            Transform _tmp = res.transform;
            _tmp.SetParent(transform);
            _tmp.localScale = Vector3.one;
            return res;
        }

        public TerrainEffect GetTerrainRoadFxItem()
        {
            if (TerrainRoadEffectItemContainer== null)
            {
                return null;
            }
            var res = TerrainRoadEffectItemContainer.AddItem() as TerrainEffect;
            Transform _tmp = res.transform;
            _tmp.SetParent(transform);
            _tmp.localScale = Vector3.one;
            return res;
        }

        public BuildingSpeedUpEffect GetSpeedFxItem()
        {
            var res = SpeedUpEffectItemContainer.AddItem() as BuildingSpeedUpEffect;
            Transform _tmp = res.transform;
            _tmp.SetParent(transform);
            _tmp.localScale = Vector3.one;
            return res;
        }        

        public BuildingUpgradeEffect GetUpgradeFxItem()
        {
            var res = UpgradeEffectItemContainer.AddItem() as BuildingUpgradeEffect;
            Transform _tmp = res.transform;
            _tmp.SetParent(transform);
            _tmp.localScale = Vector3.one;
            return res;
        }

        public BuildingSatisfactionChangeEffect GetSatisfactionChangeItem()
        {
            var res = SatisfactionItemContainer.AddItem() as BuildingSatisfactionChangeEffect;
            _buildingSatisfactionEffectHandler.AddBuildingSatisfactionEffect(res);
            Transform _tmp = res.transform;
            _tmp.SetParent(res.transform);
            _tmp.localScale = Vector3.one;
            return res;
        }

        public RoadFinishedEffect GetRoadFinishedItem()
        {
            var res = RoadFinishItemContainer.AddItem() as RoadFinishedEffect;
            return res;
        }

        public SubSectorUnlockEffect GetSubSectorUnlockItem()
        {
            var res = (SubSectorUnlockEffect)UnlockedSubSectorItemContainer.AddItem();
            Transform _tmp = res.transform;
            _tmp.SetParent(transform);
            _tmp.localScale = Vector3.one;
            return res;
        }

        public ConstructionProgressBar GetConstractionProgressBar()
        {
            var res = (ConstructionProgressBar)jConstractionProgressbarsPull.GetInstance();
            jInjectionBinder.injector.Inject(res);
			Transform _tmp = res.transform;
			_tmp.SetParent(_progressBarContainer);
			_tmp.localScale = Vector3.one;

            _activeConstractionProgressBars.Add(res);

            return res;
        }

        public void RemoveConstractionProgressBar(ConstructionProgressBar bar)
        {
            _activeConstractionProgressBars.Remove(bar);
            bar.FreeObject();
        }

        public void OrderProgressBars()
        {
            _activeConstractionProgressBars.RemoveAll(bar => bar == null || bar.Target == null);
            
            _activeConstractionProgressBars.Sort(ConstructionProgressBarsComparison);
            
            for (int i = 0; i < _activeConstractionProgressBars.Count; i++)
            {
                _activeConstractionProgressBars[i].transform.SetSiblingIndex(i);
            }
        }

        private int ConstructionProgressBarsComparison(ConstructionProgressBar a, ConstructionProgressBar b)
        {
            return Mathf.RoundToInt((a.Target.IsoDepth - b.Target.IsoDepth) * 100f); // * 100f is needed because IsoDepth can differ by less than 1
        }

        public LockedSectorLabel GetLockedSubSectorBar()
        {
            var res = (LockedSectorLabel)jLockesSubSectorPull.GetInstance();
            Transform _tmp = res.transform;
            _tmp.SetParent(LockedSubSectorsContainer.transform);
            _tmp.localScale = Vector3.one;
            return res;
        }

        public NonPlayableBuildUI GetNonPlayableBuildingBar()
        {
            var res = NonPlayableBuildsContainer.AddItem() as NonPlayableBuildUI;
            return res;
        }

        public HintOverObjectUI GetHintOverOject()
        {
            var res = HintOverObjectsContainer.AddItem() as HintOverObjectUI;
            return res;
        }

        public CitizenGiftAttentionView PopCitizenGiftAttention()
        {
            var viewInstance = _citizenGiftsPool.Pop();
            viewInstance.transform.SetParent(_citizenGiftsParent, false);
            return viewInstance;
        }

        public void PushCitizenGiftAttention(CitizenGiftAttentionView attentionView)
        {
            attentionView.transform.SetParent(null, false);
            _citizenGiftsPool.Push(attentionView);
        }

        public MapObjectLooker GetEndRoadTile(bool isHead)
        {
            return isHead ? StartRoadTile : EndRoadTile;
        }

        public AoeHiglightParticle GetSupplyParticle()
        {
            return (AoeHiglightParticle)SupplyParticlesContainer.AddItem();
        }

        public AoeHiglightParticle GetHappinessParticle()
        {
            return (AoeHiglightParticle) HappinessParticlesContainer.AddItem();
        }

        public AoeHiglightParticle GetEnergyParticle()
        {
            return (AoeHiglightParticle) EnergyParticlesContainer.AddItem();
        }

        public LongTapProgressView PopLongTapProgress()
        {
            var viewInstance = _longTapProgressPool.Pop();
            viewInstance.transform.SetParent(_longTapProgressParent, false);
            return viewInstance;
        }

        public void PushLongTapProgress(LongTapProgressView longTapProgressView)
        {
            longTapProgressView.transform.SetParent(null, false);
            _longTapProgressPool.Push(longTapProgressView);
        }
    }
}