﻿using System;
using UnityEngine;
using Assets.NanoLib.Utilities.Pulls;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl;
using Assets.Scripts.MultiLanguageSystem.Logic;
using NanoLib.Core.Timers;
using NanoLib.UI.Core;
using NanoReality.Engine.UI.Components;
using NanoReality.Engine.UI.Extensions;
using NanoReality.Game.Attentions;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using UnityEngine.UI;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Services;
using strange.extensions.signal.impl;
using TMPro;

namespace NanoReality.Engine.UI.Views.WorldSpaceUI
{
    [RequireComponent(typeof (MapObjectLooker))]
    public class ConstructionProgressBar : MonoBehaviour, IPullableObject
    {
        [SerializeField] private GameObject _sliderGameObject;
        [SerializeField] private PriceButton _speedUpButton;
        [SerializeField] private Image _timerIcon;
        [SerializeField] private TextMeshProUGUI _buildNameLabel;

        public PriceButton SpeedUpButton => _speedUpButton;
        public HorizontalProgressBar FillArea;
        public TextMeshProUGUI TimeLabel;
        public MapObjectView Target { get; protected set; }
        private bool _isShowButtonEnabled;
        private bool _visible;
        private Action<int> _priceUpdateAction;
        private float _constructionProgress;
        private int _duration;
        private int _cachedTimeLeft;
        private int _cachedPrice = -1;

        #region Injects

        [Inject] public IPopupManager jPopupManager { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public SignalOnFastFinishButtonActivated jSignalOnFastFinishButtonActivated { get; set; }
        [Inject] public IConstructionController jConstructionController { get; set; }
        [Inject] public ServerTimeTickSignal jServerTimeTickSignal { get; set; }
        [Inject] public ISkipIntervalsData jSkipIntervalsData { get; set; }
        [Inject] public SignalOnMapMovedUnderTouch jSignalOnMapMovedUnderTouch { get; set; }
        [Inject] public IGameCamera jGameCamera { get; set; }
        [Inject] public IUIManager jUIManager { get; set; }
        [Inject] public IBuildingAttentionsController jBuildingAttentionsController { get; set; }
        [Inject] public IBuildingService jBuildingService { get; set; }
        
        #endregion

        private Signal OnPayedOperationNotActual;
        
        public virtual void StartTiming(MapObjectView target)
        {
            GetComponent<MapObjectLooker>().SetTarget(target);

            Target = target;
            
            _buildNameLabel.SetLocalizedText(Target.MapObjectModel.Name.ToUpper());
            _duration = jBuildingService.GetConstructionTime(target.MapObjectModel);;

            UpdateView();
        }

        protected void UpdateView()
        {
            if (Target == null) return;

            SetProgressBarVisibility(!(jConstructionController.IsBuildModeEnabled || jConstructionController.IsRoadBuildingMode));
            
            if (_cachedTimeLeft != Target.MapObjectModel.CurrentConstructTime)
            {
                _cachedTimeLeft = Target.MapObjectModel.CurrentConstructTime;                
                TimeLabel.SetLocalizedText(UiUtilities.GetFormattedTimeFromSeconds(_cachedTimeLeft));
                FillArea.FillAmount = (_duration - _cachedTimeLeft) / (float)_duration;
            }

            if (!_isShowButtonEnabled || !(Target.MapObjectModel is IMapBuilding model))
                return;

            int skipPrice = GetSkipPrice(model);
            if (_cachedPrice == skipPrice)
                return;

            _cachedPrice = skipPrice;
            _speedUpButton.Price = _cachedPrice;
        }

        private void SetProgressBarVisibility(bool status)
        {
            _sliderGameObject.SetActive(status);
            FillArea.enabled = status;
            TimeLabel.enabled = status;
            _timerIcon.enabled = status;

            if (!status)
	        {
		        TurnSpeedUpButton(false);
	        }
	    }

        public void Show()
        {
            _visible = true;
            _cachedPrice = -1;
            gameObject.SetActive(true);
            jServerTimeTickSignal.AddListener(OnServerTimeTick);
        }

        public void Hide()
        {
            if (!_visible)
                return;
            _visible = false;
            TurnSpeedUpButton(false);
            gameObject.SetActive(false);
            jServerTimeTickSignal.RemoveListener(OnServerTimeTick);
        }

        public void TrySetActiveSkipButton()
        {
            if (jConstructionController.IsBuildModeEnabled ||
                jConstructionController.IsRoadBuildingMode)
                return;

            jUIManager.HideAll();
            TurnSpeedUpButton(true);
        }
        
        public void TurnSpeedUpButton(bool enable)
        {
            if (_speedUpButton == null)
                return;

            if (enable)
            {
                FocusOnBuilding();
                jBuildingAttentionsController.SetActiveAttentions(false);
            }
            else
            {
                jBuildingAttentionsController.SetActiveAttentions(true);
            }
            
            _speedUpButton.gameObject.SetActive(enable);
            _buildNameLabel.enabled = enable;
            _isShowButtonEnabled = enable;
			
            if (!enable) return;

			transform.SetAsLastSibling();
            
            jSignalOnFastFinishButtonActivated?.Dispatch(Target);

            if (Target.MapObjectModel is IMapBuilding model)
            {
                _speedUpButton.Price = GetSkipPrice(model);
                _buildNameLabel.SetLocalizedText(Target.MapObjectModel.Name.ToUpper());
            }
        }
			
        public void OnSpeedUp()
        {
            var building = Target.MapObjectModel as IMapBuilding;
            int skipPrice = GetSkipPrice(building);
            
            if (skipPrice <= 0)
            {
                building.SkipConstructionTimer();
                return;
            }
            
            var popupSettings = new PurchaseByPremiumPopupSettings(skipPrice, OnPayedOperationNotActual);
            
            void UpdatePrice(long _)
            {
                popupSettings?.InvokePriceChangeAction(GetSkipPrice(building));
            }

            if (jPlayerProfile.PlayerResources.HardCurrency >= skipPrice)
            {
                OnPayedOperationNotActual = new Signal();
                
                jServerTimeTickSignal.AddListener(UpdatePrice);
                                
                jPopupManager.Show(popupSettings, result =>
                {
                    jServerTimeTickSignal.RemoveListener(UpdatePrice);
                    if (result == PopupResult.Ok)
                    {
                        building.SkipConstructionTimer();
                    }
                    
                    if (OnPayedOperationNotActual == null)
                        return;
                    
                    OnPayedOperationNotActual.RemoveAllOnceListeners();
                    OnPayedOperationNotActual = null;
                });
                return;
            }

            jPopupManager.Show(new NotEnoughMoneyPopupSettings(PriceType.Hard,
                skipPrice - jPlayerProfile.PlayerResources.HardCurrency));
        }

        private void OnServerTimeTick(long serverTime)
        {
            UpdateView();
        }

        public void ConstructionFinished()
	    {
            if (OnPayedOperationNotActual == null)
                return;
            
            OnPayedOperationNotActual.Dispatch();
            OnPayedOperationNotActual = null;
        }

        private int GetSkipPrice(IMapBuilding building)
        {
            return jSkipIntervalsData.GetCurrencyForTime(((MapBuilding)building).CurrentBuildTime);
        }
        
        protected void FocusOnBuilding()
        {
            jGameCamera.FocusOnMapObject(Target.MapObjectModel);
            jSignalOnMapMovedUnderTouch.Dispatch();
        }

        #region Pullable
        
        public object Clone()
        {
            return Instantiate(this);
        }

        public IObjectsPull pullSource { get; set; }
        
        public void OnPop()
        {
            FillArea.FillAmount = 0f;
            enabled = true;
            GetComponent<MapObjectLooker>().enabled = true;
        }

        public void OnPush()
        {
            enabled = false;
            var looker = GetComponent<MapObjectLooker>();
            looker.SetTarget(null);
            looker.enabled = false;
            Target = null;
            Hide();
        }

        public void FreeObject()
        {
            pullSource?.PushInstance(this);
        }

        public void DestroyObject()
        {
            Destroy(gameObject);
        }
        #endregion
    }
}
