﻿using Assets.NanoLib.UI.Core.Views;
using Assets.NanoLib.Utilities;
using Assets.NanoLib.Utilities.Pulls;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.UI.Components;
using NanoReality.Engine.UI.UIAnimations;
using UnityEngine;

public class WorkInProgressEffect : GameObjectPullable
{
	public void Play()
	{
		StartAnimation();
	}

	public void Stop()
	{
		StopAnimation();

		FreeObject();
	}

	private CacheMonobehavior _cacheMonobehavior;

	public CacheMonobehavior CacheMonobehavior
	{
		get { return _cacheMonobehavior ?? (_cacheMonobehavior = new CacheMonobehavior(this)); }
	}

	public void SetTarget(MapObjectView target)
	{
		CacheMonobehavior.GetCacheComponent<MapObjectLooker>().SetTarget(target);
	}

	private void StartAnimation()
    {
		CacheMonobehavior.GetCacheComponent<Animator>().Play("buildGears");
		gameObject.SetActive(true);
	}

    private void StopAnimation()
    {
		CacheMonobehavior.GetCacheComponent<Animator>().StopPlayback();
		gameObject.SetActive(false);
	}

	public override void OnPush()
	{
		StopAnimation();

		base.OnPush();
	}
}
