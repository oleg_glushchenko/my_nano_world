﻿using System.Collections;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.UI.UIAnimations;
using UnityEngine;
#pragma warning disable 649

/// <summary>
/// Visual effect for indicating population satisfaction changes on buildings
/// </summary>
public class BuildingSatisfactionChangeEffect : ParticleSpecialEffect
{
    [SerializeField]
    private Material _happySmileMaterial;

    [SerializeField]
    private Material _unhappySmileMaterial;

    [SerializeField] 
    private Material _sadSmileMaterial;

	public bool IsSatisfied { get; set; }
    public SatisfactionState SatisfactionState { get; set; }

    public override void SetTarget(MapObjectView target)
    {
        base.SetTarget(target);

        switch (SatisfactionState)
        {
            case SatisfactionState.Happy:
                SetMaterial(_happySmileMaterial);
                break;
            case SatisfactionState.Unhappy:
                SetMaterial(_unhappySmileMaterial);
                break;
            case SatisfactionState.Sad:
                SetMaterial(_sadSmileMaterial);
                break;
        }
    }

    private void SetMaterial(Material material)
    {
        DefaultParticleSystem.GetComponent<ParticleSystemRenderer>().material = material;
    }

    private void PlayEffect()
    {
        Play();
    }

    public void PlayEffectWithDelay(float maxDelay, SatisfactionState state)
    {
        if (SatisfactionState == state)
        {
            if (maxDelay < 0)
            {
                PlayEffect();
                return;
            }
            var delay = Random.Range(0, maxDelay);
            Invoke(nameof(PlayEffect), delay);
        }
    }
}

public enum SatisfactionState
{
    Happy,
    Unhappy,
    Sad
}