﻿using Assets.Scripts.MultiLanguageSystem.Logic;
using UnityEngine;
using strange.extensions.signal.impl;
using TMPro;

[RequireComponent (typeof (Animator))]
public class TooltipSettings : MonoBehaviour
{
    /// <summary>
    /// Текст сообщения подсказки
    /// </summary>
    [SerializeField] private TextMeshProUGUI _message;

    public Signal OnAnimationEndSignal = new Signal();

    public void Init ()
    {
        SetInitialData();
    }

    public void Init(string text)
    {
        SetInitialData();
        _message.SetLocalizedText(text);
    }

    // Use this for initialization
    private void SetInitialData()
    {
        Transform tr = transform;
        tr.localPosition = Vector3.zero;
        tr.localScale = Vector3.one;
        tr.localRotation = Quaternion.Euler(Vector3.zero);
        GetComponent<Animator>().SetTrigger("Show");
    }

    // Update is called once per frame
	public void Destroy ()
    {
        OnAnimationEndSignal.Dispatch();
        Destroy (gameObject);
	}
}