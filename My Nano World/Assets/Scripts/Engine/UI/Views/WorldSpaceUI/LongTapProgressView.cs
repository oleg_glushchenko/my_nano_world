using strange.extensions.mediation.impl;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine;
using strange.extensions.pool.api;
using System;

public class LongTapProgressView : View ,  IPoolable
{
    [SerializeField]private Image _filledImage;

    public void FIll(float fillTime, Action onComplete)
    {
        _filledImage.fillAmount = 0;
        _filledImage.DOFillAmount(1, fillTime/1000).OnComplete(() => { onComplete();});
    }

    public void StopFillAnimation()
    {
        _filledImage.DOKill();
    }

    #region IPoolable
    public bool retain { get; private set; }

    public void Release()
    {
    }

    public void Restore()
    {
        gameObject.SetActive(false);
    }

    public void Retain()
    {
        gameObject.SetActive(true);
    }
    #endregion
}
