﻿using System;
using Assets.NanoLib.UI.Core.Signals;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Game.Attentions;
using DG.Tweening;
using NanoReality.Engine.UI.Components;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.UI;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.Services;
using strange.extensions.pool.api;
using Random = UnityEngine.Random;

namespace NanoReality.Engine.UI.Views.WorldSpaceUI
{
    [RequireComponent(typeof (MapObjectLooker))]
    public class BuildingStatesAttentionView : View, IPoolable
    {
        #region Injections
        
        [Inject] public IIconProvider jIconProvider { get; set; }
        
        [Inject] public ShowWindowSignal jShowWindowSignal { get; set; }
        [Inject] public SignalOnAchievementAttentionTap jSignalOnAchievementAttentionTap { get; set; }
        [Inject] public SignalOnTaxesAttentionTap jSignalOnTaxesAttentionTap { get; set; }
        [Inject] public SignalOnFoodSupplyAttentionTap jSignalOnFoodSupplyAttentionTap { get; set; }
        [Inject] public SignalOnDwellingHouseCoinAttentionTap jSignalOnDwellingHouseCoinAttentionTap { get; set; }
        [Inject] public SignalOnBuildingUpgradeAttentionTap jSignalOnBuildingUpgradeAttentionTap { get; set; }
        [Inject] public SignalOnQuestsBuildingQuestsAttentionTap jSignalOnQuestsBuildingQuestsAttentionTap { get; set; }
        [Inject] public SignalOnBuildingAttentionTap jSignalOnBuildingAttentionTap { get; set; }
        [Inject] public SignalOnConstructControllerChangeState jSignalOnConstructControllerChangeState { get; set; }
        
        #endregion

        #region Layout

        public Image RoadImage;
        public Image AdImage;
        public GameObject OrderDeskImage;
        public Image ActievementImage;
        public Image CoinImage;
        public GameObject ShortPopup;
        public GameObject ProductReadyToLoadImage;
        public Image ReadyForUpgradeImage;
        public Image ReadyForConstructImage;
        public Button AttentionButton;

        [SerializeField] private float _minAnimationDelay;
        [SerializeField] private float _maxAnimationDelay;
        [SerializeField] private Transform _attentionBack;
        [SerializeField] private Image _buttonRaycastTarget;

        #endregion

        #region Fields

        private bool _isInitialized;

        private AttentionsTypes _currentAttentionType;
        private MapObjectView _currentTarget;
        
        private AttentionData _attentionData;
        private Transform _attentionIcon;
        private Tween _attentionAnimationTween;

        private MapObjectLooker _mapObjectLooker;

        public MapObjectView CurrentTarget => _currentTarget;
        
        public AttentionsTypes CurrentAttentionsType => _currentAttentionType;
        
        #endregion

        #region Mono, injections init logic 

        protected override void Awake()
        {
            base.Awake();

            _mapObjectLooker = GetComponent<MapObjectLooker>();
            _attentionIcon = RoadImage.transform;
        }

        protected override void Start()
        {
            base.Start();
            UpdateView(); // TODO: hot fix for correct initialization of this view with injects.
        }

        #endregion

        #region Construct imlementation

        [PostConstruct]
        public void PostConstruct()
        {
            _isInitialized = true;
            jSignalOnConstructControllerChangeState.AddListener(DisableWhileInConstructionMode);
        }

        [Deconstruct]
        public void Deconstruct()
        {
            jSignalOnConstructControllerChangeState.RemoveListener(DisableWhileInConstructionMode);
        }

        #endregion
        
        #region Pullable

        public Boolean retain { get; }
        
        public void Restore()
        {
            
        }

        public void Retain()
        {
            _mapObjectLooker.enabled = true;

            ShortPopup.SetActive(false);
            gameObject.SetActive(true);
        }

        public void Release()
        {
            _currentTarget = null;
            
            _mapObjectLooker.enabled = false;
            _mapObjectLooker.SetTarget(null);
            _mapObjectLooker.TargetCustomRotation = Vector3.zero;
            _mapObjectLooker.TargetCustomOffset = Vector3.zero;
            _attentionData = null;

            KillAnimationTween();

            ShortPopup.SetActive(false);
            gameObject.SetActive(false);
        }

        #endregion

        #region Methods

        private void SetAttentionIcon(AttentionsTypes attentionType, MapObjectView target)
        {
            _currentAttentionType = attentionType;

            ShortPopup.SetActive(CanShortPopupBeShown() && attentionType != AttentionsTypes.None);
            
            AdImage.enabled = false;
            OrderDeskImage.SetActive(false);
            ActievementImage.enabled = false;
            CoinImage.enabled = false;
            ProductReadyToLoadImage.SetActive(false);
            RoadImage.enabled = false;
            ReadyForUpgradeImage.enabled = false;
            ReadyForConstructImage.enabled = false;

            _currentAttentionType = attentionType.GetOnlyHighest();

            switch (_currentAttentionType)
            {
                case AttentionsTypes.None:
                    return;

                case AttentionsTypes.Road | AttentionsTypes.Electricity:
                case AttentionsTypes.Road:
                    RoadImage.enabled = true;
                    _attentionIcon = RoadImage.transform;
                    break;
                
                case AttentionsTypes.CinemaAd:
                    AdImage.enabled = true;
                    _attentionIcon = AdImage.transform;
                    break;

                case AttentionsTypes.OrderDesk:
                case AttentionsTypes.CityOrderDesk:
                    OrderDeskImage.SetActive(true);
                    _attentionIcon = OrderDeskImage.transform;
                    break;

                case AttentionsTypes.Achievement:
                    ActievementImage.enabled = true;
                    _attentionIcon = ActievementImage.transform;
                    break;

                case AttentionsTypes.Coin:
                    CoinImage.enabled = true;
                    _attentionIcon = CoinImage.transform;
                    break;

                case AttentionsTypes.CustomProductToLoad:
                    ProductReadyToLoadImage.SetActive(true);
                    _attentionIcon = ProductReadyToLoadImage.transform;
                    break;

                case AttentionsTypes.ReadyForUpgrade:
                    var model = target.MapObjectModel;
                    ReadyForUpgradeImage.enabled = true;
                    _attentionIcon = ReadyForUpgradeImage.transform;
                    break;
                
                case AttentionsTypes.NotEnoughProducts:
                    ReadyForConstructImage.enabled = true;
                    _attentionIcon = ReadyForConstructImage.transform;
                    break;
                
                default:
                    return;
            }

            _mapObjectLooker.SetTarget(target);

            EnableAttentionAnimation();
        }

        public void UpdateState(AttentionData data, MapObjectView target)
        {    
            if (data.AttentionType.HasFlag(AttentionsTypes.Achievement) ||
                data.AttentionType.HasFlag(AttentionsTypes.Crystals) ||
                data.AttentionType.HasFlag(AttentionsTypes.TradeProductBought) ||
                data.AttentionType.HasFlag(AttentionsTypes.CustomProductToLoad))
                return;

            if(data.Equals(_attentionData))
                return;
            
            _attentionData = data;
            _currentTarget = target;
            UpdateView();
        }

        private void UpdateView()
        {
            if (_attentionData == null)
            {
                return;
            }
            
            SetAttentionIcon(_attentionData.AttentionType, _currentTarget);
        }

        //TODO move to a new class
        private void EnableAttentionAnimation()
        {
            if (_attentionIcon == null)
                return;

            KillAnimationTween();

            Vector2 iconScale = new Vector3(0.2f, 0.2f);
            Vector2 backgroundScale = new Vector3(0.1f, 0.1f);

            float animationDuration = 0.7f;
            int vibrato = 2;
            float elasticity = 0;

            Tween attentionBackTween = _attentionBack.DOPunchScale(backgroundScale, animationDuration, vibrato, elasticity);
            Tween attentionIconTween = _attentionIcon.DOPunchScale(iconScale, animationDuration, vibrato, elasticity);

            float loopInterval = Random.Range(_minAnimationDelay, _maxAnimationDelay);

            Sequence attentionAnimationTween = DOTween.Sequence();
            attentionAnimationTween.Append(attentionBackTween);
            attentionAnimationTween.Join(attentionIconTween);
            attentionAnimationTween.AppendInterval(loopInterval);
            attentionAnimationTween.SetLoops(-1).Play();

            _attentionAnimationTween = attentionAnimationTween;
        }

        private void KillAnimationTween()
        {
            _attentionAnimationTween.Restart();
            _attentionAnimationTween?.Kill();
        }

        public void OnShowFullPopupButtonClick()
        {
            if (!_currentTarget.MapObjectModel.IsVerificated)
            {
                return;
            }
            
            if (_currentAttentionType.HasFlag(AttentionsTypes.Electricity))
            {
                jSignalOnBuildingAttentionTap.Dispatch(this);
                return;
            }

            if (_currentAttentionType.HasFlag(AttentionsTypes.Crystals))
            {
                Debug.LogWarning("Not implemented");
                return;
            }

            if (_currentAttentionType.HasFlag(AttentionsTypes.Achievement))
            {
                jSignalOnAchievementAttentionTap.Dispatch();
                return;
            }

            if (_currentAttentionType.HasFlag(AttentionsTypes.Coin))
            {
                switch (_currentTarget.MapObjectModel.MapObjectType)
                {
                    case MapObjectintTypes.CityHall:
                        jSignalOnTaxesAttentionTap.Dispatch();
                        break;
                    
                    case MapObjectintTypes.DwellingHouse:
                        jSignalOnDwellingHouseCoinAttentionTap.Dispatch(_currentTarget.MapObjectModel as IDwellingHouseMapObject);
                        break;
                    
                    case MapObjectintTypes.FoodSupply:
                        jSignalOnFoodSupplyAttentionTap.Dispatch();
                        break;
                }

                return;
            }

            if(_currentAttentionType.HasFlag(AttentionsTypes.ReadyForUpgrade))
            {
                if (_currentTarget.MapObjectModel.MapObjectType == MapObjectintTypes.CityHall)
                {
                    if (((IMapBuilding)_currentTarget.MapObjectModel).CanBeUpgraded && !((IMapBuilding)_currentTarget.MapObjectModel).IsLocked)
                    {
                        jShowWindowSignal.Dispatch(typeof(UpgradePanelView), (ICityHall)_currentTarget.MapObjectModel);
                    }
                    else
                    {
                        jSignalOnBuildingAttentionTap.Dispatch(this);
                    }
                    
                    return;
                }
                
                if (_currentTarget.MapObjectModel.MapObjectType == MapObjectintTypes.PowerPlant)
                {
                    if (((IMapBuilding)_currentTarget.MapObjectModel).CanBeUpgraded && !((IMapBuilding)_currentTarget.MapObjectModel).IsLocked)
                    {
                        jShowWindowSignal.Dispatch(typeof(UpgradePanelView), _currentTarget.MapObjectModel);
                    }
                    else
                    {
                        jSignalOnBuildingAttentionTap.Dispatch(this);
                    }
                    
                    return;
                }
                
                jSignalOnBuildingUpgradeAttentionTap.Dispatch((IMapBuilding)_currentTarget.MapObjectModel);
                return;
            }
            
            if(_currentAttentionType.HasFlag(AttentionsTypes.NotEnoughProducts))
            {
                if (_currentTarget.MapObjectModel.MapObjectType == MapObjectintTypes.CityHall)
                {
                    jShowWindowSignal.Dispatch(typeof(UpgradePanelView), _currentTarget.MapObjectModel);
                    return;
                }
                
                if (_currentTarget.MapObjectModel.MapObjectType == MapObjectintTypes.PowerPlant)
                {
                    if (((IMapBuilding)_currentTarget.MapObjectModel).CanBeUpgraded 
                        && !((IMapBuilding)_currentTarget.MapObjectModel).IsLocked)
                    {
                        jShowWindowSignal.Dispatch(typeof(UpgradePanelView), _currentTarget.MapObjectModel);
                    }

                    return;
                }
                
                jSignalOnBuildingUpgradeAttentionTap.Dispatch((IMapBuilding)_currentTarget.MapObjectModel);
                return;
            }
            
            if(_currentAttentionType.HasFlag(AttentionsTypes.QuestsAchieved))
            {
                jSignalOnQuestsBuildingQuestsAttentionTap.Dispatch(_currentTarget.MapObjectModel as IQuestsBuilding);
                return;
            }
            
            if(_currentAttentionType.HasFlag(AttentionsTypes.QuestsAvailable))
            {
                jSignalOnQuestsBuildingQuestsAttentionTap.Dispatch(_currentTarget.MapObjectModel as IQuestsBuilding);
                return;
            }

            if (_currentAttentionType.HasFlag(AttentionsTypes.OrderDesk))
            {
                if (_currentTarget.MapObjectModel.MapObjectType == MapObjectintTypes.CityOrderDesk)
                {
                    jShowWindowSignal.Dispatch(typeof(MetroOrderDeskPanelView), _currentTarget.MapObjectModel);
                    return;
                }

                if (_currentTarget.MapObjectModel.MapObjectType == MapObjectintTypes.OrderDesk)
                {
                    jShowWindowSignal.Dispatch(typeof(SeaportPanelView), _currentTarget.MapObjectModel);
                    return;
                }

                if (_currentTarget.MapObjectModel.MapObjectType == MapObjectintTypes.FoodSupply)
                {
                    jShowWindowSignal.Dispatch(typeof(FoodSupplyPanelView), _currentTarget.MapObjectModel);
                    return;
                }
            }

            jSignalOnBuildingAttentionTap.Dispatch(this);
        }

        private bool CanShortPopupBeShown()
        {
            return _isInitialized && _attentionData != null;
        }

        #endregion

        public void SetButtonInteractability(bool interactable)
        {
            AttentionButton.interactable = interactable;
        }
        
        private void DisableWhileInConstructionMode(bool isConstructionModeActive)
        {
            _buttonRaycastTarget.raycastTarget = !isConstructionModeActive;
        }
    }
}
