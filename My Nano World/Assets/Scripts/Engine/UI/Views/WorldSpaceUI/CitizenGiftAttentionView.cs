using System;
using Assets.NanoLib.UI.Core;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using DG.Tweening;
using NanoLib.Core.Services.Sound;
using NanoReality.Engine.UI.Components;
using strange.extensions.pool.api;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Views.WorldSpaceUI
{
    public sealed class CitizenGiftAttentionView : UIView, IPoolable
    {
        [SerializeField] private Image _rewardIconImage;
        [SerializeField] private Button _claimGiftButton;
        [SerializeField] private RectTransform _attentionBodyTransform;
        [SerializeField] private MapObjectLooker _mapObjectLooker;
        
        [Inject] public ISoundManager jSoundManager { get; set; }
        
        private Tween _animationTween;
        private int _giftId;

        public Button Claim => _claimGiftButton;
        public event Action<int> ClaimClicked;

        public Sprite RewardIconSprite
        {
            set => _rewardIconImage.sprite = value;
        }

        public bool Interactable
        {
            set => _claimGiftButton.interactable = value;
        }

        public void Init(int giftId, MapObjectView targetView, Vector3 randomizedOffset)
        {
            _giftId = giftId;
            
            _mapObjectLooker.RandomedOffset = randomizedOffset;
            _mapObjectLooker.Pivot = MapObjectLooker.PivotModes.Random;
            _mapObjectLooker.SetTarget(targetView);
            
            _claimGiftButton.onClick.AddListener(OnClick);
        }

        private void OnClick()
        {
            jSoundManager.Play(SfxSoundTypes.CollectItems, SoundChannel.SoundFX);
            ClaimClicked?.Invoke(_giftId);
        }

        public void Dispose()
        {
            _mapObjectLooker.SetTarget(null);
            RewardIconSprite = null;
            Interactable = false;
            _animationTween?.Kill();
            
            _claimGiftButton.onClick.RemoveListener(OnClick);
        }

        public void Show(Action onShowFinished = null)
        {
            _animationTween?.Kill();
            _attentionBodyTransform.localScale = Vector3.zero;
            _animationTween = _attentionBodyTransform.DOScale(Vector3.one, 1.0f).SetEase(Ease.InOutBounce).OnComplete(() => { onShowFinished?.Invoke(); });
        }

        public void Hide(Action onHideFinished = null)
        {
            _animationTween?.Kill();
            _animationTween = _attentionBodyTransform.DOScale(Vector3.zero, 1.0f).SetEase(Ease.InOutBounce).OnComplete(() => { onHideFinished?.Invoke(); });
        }

        #region IPoolable

        public void Restore()
        {
        }

        public void Retain()
        {
            gameObject.SetActive(true);
            _attentionBodyTransform.transform.localScale = Vector3.one;
        }

        public void Release()
        {
            gameObject.SetActive(false);
            _attentionBodyTransform.transform.localScale = Vector3.one;
            _claimGiftButton.onClick.RemoveListener(OnClick);
        }

        public bool retain { get; private set; }

        #endregion
    }
}