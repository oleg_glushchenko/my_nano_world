﻿using Assets.NanoLib.UI.Core.Views;
using Assets.NanoLib.Utilities;
using Assets.NanoLib.Utilities.Pulls;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.UI.Components;
using NanoReality.Engine.UI.UIAnimations;

/// <summary>
/// Visual Effect for production buildings currently not producing anything
/// </summary>
public class BuildingDowntimeEffect : ParticleSpecialEffect 
{

}
