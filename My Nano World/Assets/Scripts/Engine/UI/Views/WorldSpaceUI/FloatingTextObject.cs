﻿using Assets.NanoLib.Utilities.Pulls;
using UnityEngine;

namespace NanoReality.Engine.UI.Views.WorldSpaceUI
{
    public class FloatingTextObject : GameObjectPullable
    {
        [SerializeField]
        private TooltipSettings _hintPrefab;
        [SerializeField]
        private Transform _root;
        
        private TooltipSettings _textTooltip;

        public void Show(Vector3 worldPoint, string message)
        {
            transform.position = worldPoint;
            
            var hint = Instantiate(_hintPrefab.gameObject);
            hint.transform.SetParent(_root);

            _textTooltip = hint.GetComponent<TooltipSettings>();
            _textTooltip.Init(message);
            _textTooltip.OnAnimationEndSignal.AddListener(FreeObject);
        }

        public override void OnPush()
        {
            base.OnPush();

            if (_textTooltip != null)
            {
                _textTooltip.OnAnimationEndSignal.RemoveListener(FreeObject);
            }
        }
    }
}
