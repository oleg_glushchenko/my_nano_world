﻿using Assets.Scripts.Engine.BuildingSystem.CityMap;
using NanoReality.Engine.UI.Components;
using NanoReality.Engine.UI.UIAnimations;
using UnityEngine;

public class SubSectorUnlockEffect : ParticleSpecialEffect
{

    //check size of the needed target and set correct shape
    protected override void SetCorrectShape(Vector2 targetDimension)
    {
        var x = DefaultParticleSystem.shape;
        if (targetDimension.x > targetDimension.y)
        {
            x.scale = new Vector3(9f, 6f, 0.1f);
        } 
        else if (targetDimension.x < targetDimension.y)
        {
            x.scale = new Vector3(6f, 9f, 0.1f);
        }
        else
        {
            x.scale = new Vector3(5f, 5f, 0.1f);
        }
	}

	public void SetTarget(LockedSectorView target)
	{
		SubSectorLooker looker = CacheMonobehavior.GetCacheComponent<SubSectorLooker>();
		looker.enabled = true;
		looker.SetTarget(target);
		SetCorrectShape(target.GridDimensions);
	}


    #region Pullable

    public override void OnPush()
    {
        enabled = false;
        var looker = CacheMonobehavior.GetCacheComponent<SubSectorLooker>();
        looker.SetTarget(null);
        looker.enabled = false;
        gameObject.SetActive(false);
    }

    #endregion
}