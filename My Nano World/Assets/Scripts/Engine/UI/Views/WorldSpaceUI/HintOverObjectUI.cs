﻿using Assets.NanoLib.Utilities;
using Assets.NanoLib.Utilities.Pulls;
using Assets.Scripts.Engine.BuildingSystem.CityMap;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.UI.Components;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace NanoReality.Engine.UI.Views.WorldSpaceUI
{
    /// <summary>
    /// Для отображения подсказок над обьектами
    /// </summary>
    public class HintOverObjectUI : View, IPullableObject
    {
        public CacheMonobehavior CacheMonobehavior
        {
            get { return _cacheMonobehavior ?? (_cacheMonobehavior = new CacheMonobehavior(this)); }
        }
        private CacheMonobehavior _cacheMonobehavior;

        /// <summary>
        /// префаб всплывающей подсказки
        /// </summary>
        [SerializeField] private TooltipSettings _hintPrefub;
        /// <summary>
        /// контейнер для показа всплывающей подсказки
        /// </summary>
        [SerializeField] private Transform _hintHolder;

        public IObjectsPull pullSource { get; set; }

        private MapObjectLooker _mapObjectLooker;
        private TooltipSettings _textTooltip;

        public void SetTargetCustomRotation(Vector3 targetCustomRotation)
        {
            _mapObjectLooker.TargetCustomRotation = targetCustomRotation;
            _mapObjectLooker.UpdateLookerPosition();
        }

        /// <summary>
        /// Начальная инициализация обьекта по mapObjectView (для зданий)
        /// </summary>
        public void Init(MapObjectView target, string message)
        {
            _mapObjectLooker = CacheMonobehavior.GetCacheComponent<MapObjectLooker>();
            _mapObjectLooker.SetTarget(target);
            _mapObjectLooker.enabled = true;

            Transform tr = Instantiate(_hintPrefub.gameObject).transform;
            tr.SetParent(_hintHolder);

            _textTooltip = tr.GetComponent<TooltipSettings>();
            _textTooltip.Init(message);
            _textTooltip.OnAnimationEndSignal.AddListener(FreeObject);
        }

        #region Pullable

        public object Clone()
        {
            return Instantiate(this);
        }

        public void OnPop()
        {
            gameObject.SetActive(true);
            enabled = true;
        }

        public void OnPush()
        {
            if (_mapObjectLooker != null)
            {
                _mapObjectLooker.enabled = false;
                _mapObjectLooker.SetTarget(null);
            }

            if (_textTooltip != null)
                _textTooltip.OnAnimationEndSignal.RemoveListener(FreeObject);

            enabled = false;
            gameObject.SetActive(false);
        }

        public void FreeObject()
        {
            if(pullSource != null)
                pullSource.PushInstance(this);
        }

        public void DestroyObject()
        {
            Destroy(this);
        }

        #endregion
    }
}