﻿using Assets.NanoLib.UI.Core.Views;
using Assets.NanoLib.Utilities;
using Assets.NanoLib.Utilities.Pulls;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.UI.Components;
using UnityEngine;
using System.Collections;
using NanoReality.Engine.UI.UIAnimations;

/// <summary>
/// Visual Effect for road tiles building 
/// </summary>
public class RoadFinishedEffect : ParticleSpecialEffect
{
	public override void Play()
	{
		DefaultParticleSystem.randomSeed = (uint)Random.Range(0, uint.MaxValue);

        base.Play ();
	}
}