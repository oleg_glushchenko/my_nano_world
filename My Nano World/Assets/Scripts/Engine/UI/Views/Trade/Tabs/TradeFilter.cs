﻿using Assets.NanoLib.Utilities;
using NanoReality.Engine.UI.Views.Shared.Filters;
using strange.extensions.signal.impl;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// ReSharper disable once CheckNamespace
namespace Assets.Scripts.Engine.UI.Views.Trade.Items
{
    /// <summary>
    /// Переключатели трейда
    /// </summary>
    public class TradeFilter : MonoBehaviour
    {
#pragma warning disable 649

        /// <summary>
        /// Материал недоступной вкладки
        /// </summary>
        [SerializeField] private Material _grayScaleMaterial;

        /// <summary>
        /// Материал доступной вкладки
        /// </summary>
        [SerializeField] private Material _normalMaterial;

        /// <summary>
        /// Список вкладок трейда
        /// </summary>
        [SerializeField] private List<TradeToggleFilter> _tradeFilterList = new List<TradeToggleFilter>();

#pragma warning restore 649

        /// <summary>
        /// Кешированный объект
        /// </summary>
        public CacheMonobehavior CacheMonobehavior
        {
            get { return _cacheMonobehavior ?? (_cacheMonobehavior = new CacheMonobehavior(this)); }
        }

        private CacheMonobehavior _cacheMonobehavior;

        /// <summary>
        /// Происходит при смене состояния фильтра
        /// </summary>
        public Signal<TradeFilter> SignalOnFilterChanged = new Signal<TradeFilter>();

        protected void Awake()
        {
            foreach (var filter in _tradeFilterList)
            {
                filter.Toggle.onValueChanged.AddListener((state =>
                {
                    SignalOnFilterChanged.Dispatch(this);
                }));
                SetFilterEnabled(true, filter);
            }
        }

        /// <summary>
        /// Текущий выбранный трейд
        /// </summary>
        public TradeType CurrentSelectedCategory
        {
            get
            {
                foreach (var filter in _tradeFilterList)
                {
                    if (filter.Toggle.isOn) return filter.TradeCategory;
                }

                return TradeType.Unsupported;
            }

            set
            {
                foreach (var filter in _tradeFilterList)
                {
                    if (filter.TradeCategory == value && !filter.Toggle.isOn)
                    {
                        filter.Toggle.isOn = true;
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Установка состояния фильтра
        /// </summary>
        /// <param name="isEnabled">Выбран ли фильтр</param>
        /// <param name="filter">Тип фильтра</param>
        private void SetFilterEnabled(bool isEnabled, TradeToggleFilter filter)
        {
            filter.Toggle.interactable = isEnabled;
            SetFilterIconOpacity(filter, isEnabled ? 1.0f : 0.5f);
            filter.Icon.material = isEnabled ? _normalMaterial : _grayScaleMaterial;
        }

        private void SetFilterIconOpacity(TradeToggleFilter filter, float opacity)
        {
            var clr = filter.Icon.color;
            filter.Icon.color = new Color(clr.r, clr.g, clr.b, opacity);
        }
    }

    public enum TradeType
    {
        Shop,
        Market,
        Unsupported
    }

    [Serializable]
    public class TradeToggleFilter : Filter
    {
        public TradeType TradeCategory;
        public Image Icon;
    }
}