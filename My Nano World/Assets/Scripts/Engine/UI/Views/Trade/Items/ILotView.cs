﻿namespace Assets.Scripts.Engine.UI.Views.Trade.Items
{
    /// <summary>
    /// Интерфейс лота трейда
    /// </summary>
    public interface ILotView
    {
        /// <summary>
        /// Включение состояния ожидания ответа от сервера (иконка загрузки и доступность активной кнопки)
        /// </summary>
        /// <param name="isWait">включить или выключить</param>
        void WaitForServer(bool isWait);
    }
}