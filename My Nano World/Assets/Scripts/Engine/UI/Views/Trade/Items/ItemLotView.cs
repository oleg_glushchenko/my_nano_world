﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.UI.Views.Trade.Items
{
    /// <summary>
    /// Базовый класс лота с продуктом
    /// </summary>
    public abstract class ItemLotView : BaseLotView
    {
        /// <summary>
        /// Стоимость продажи
        /// </summary>
        [SerializeField]
        protected TextMeshProUGUI _sellPriceText;

        /// <summary>
        /// Иконка валюты
        /// </summary>
        [SerializeField]
        protected Image _currencyIcon;

        [SerializeField]
        protected TradeProductView _tradeProductView;

        /// <summary>
        /// Всплывающая подсказка
        /// </summary>
        [SerializeField]
        protected TradeTooltipView _tooltipView;       


        /// <summary>
        /// Иконка продаваемого продукта
        /// </summary>
        public Image ItemImage { get { return _tradeProductView.ItemImage; } }

        /// <summary>
        /// Иконка валюты
        /// </summary>
        public Image CurrencyImage { get { return _currencyIcon; } }
    }
}