using strange.extensions.signal.impl;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

#pragma warning disable 649

namespace Assets.Scripts.Engine.UI.Views.Trade.Items
{
    /// <summary>
    /// ������������� ���������� ������ ��������������� �����
    /// </summary>
    public class NewSlotView : BaseLotView
    {
        /// <summary>
        /// ����� ��������� ������ �����
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI _newSlotPrice;

        /// <summary>
        /// ������ ������� ������ ���� (�������, �� ���� ������ �����)
        /// </summary>
        [SerializeField]
        private Button _newSlotButton;
        
        /// <summary>
        /// ������ ������� ������ ���� (���������, ������)
        /// </summary>
        [SerializeField]
        private Button _newSlotButtonSmall;

        /// <summary>
        /// ������ ������� �� ������ ������� �����
        /// </summary>
        public Signal AddNewSlotSignal
        {
            get { return _addNewSlotSignal; }
        }

        private readonly Signal _addNewSlotSignal = new Signal();

        /// <summary>
        /// ���������� ��������� ��������������� �����
        /// </summary>
        /// <param name="slotPrice">���������</param>
        public void UpdateItem(int slotPrice)
        {
            _newSlotPrice.text = slotPrice.ToString();
        }

        /// <summary>
        /// ���������� ������� ������ �������� ������ ����� (������� �������������� ���� ��� ������� ��������)
        /// </summary>
        public void OnCreateButtonClick()
        {
            _addNewSlotSignal.Dispatch();
        }

        /// <summary>
        /// ��������� �������� ������ �� �������
        /// </summary>
        /// <param name="isWait">������� �� ����� �� �������</param>
        public override void WaitForServer(bool isWait)
        {
            _newSlotButtonSmall.interactable = _newSlotButton.interactable = !isWait;
        }
    }
}
