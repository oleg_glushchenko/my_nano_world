﻿using Assets.NanoLib.Utilities.Pulls;

#pragma warning disable 649


namespace Assets.Scripts.Engine.UI.Views.Trade.Items
{
    /// <summary>
    /// Базовый класс лота
    /// </summary>
    public abstract class BaseLotView : GameObjectPullable, ILotView
    {
        /// <summary>
        /// Установка ожидания ответа от сервера
        /// </summary>
        /// <param name="isWait">Ожидать ли ответ от сервера</param>
        public abstract void WaitForServer(bool isWait);

        /// <summary>
        /// Снятие выделения с лота
        /// </summary>
        public virtual void Deselect() { }
    }
}