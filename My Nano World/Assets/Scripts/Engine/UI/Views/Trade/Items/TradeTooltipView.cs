﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.UI.Views.Trade.Items
{
    /// <summary>
    /// Всплывающая подсказка трейда
    /// </summary>
    public class TradeTooltipView : MonoBehaviour
    {
        /// <summary>
        /// Текст заголовка
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI _title;
        
        /// <summary>
        /// Текст заголовка
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI _text;

        /// <summary>
        /// Установить текст подсказки
        /// </summary>
        /// <param name="title">Текст</param>
        public void SetTooltip(string title, string text)
        {
            _title.text = title;
            _text.text = text;
        }

        /// <summary>
        /// Показать подсказку
        /// </summary>
        public void ShowTooltip()
        {           
            gameObject.SetActive(true);
        }

        /// <summary>
        /// Скрыть подсказку
        /// </summary>
        public void HideTooltip()
        {
            gameObject.SetActive(false);
        }
    }
}