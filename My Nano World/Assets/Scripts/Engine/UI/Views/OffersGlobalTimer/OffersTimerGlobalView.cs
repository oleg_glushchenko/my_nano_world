using strange.extensions.mediation.impl;
using TMPro;
using UnityEngine;

namespace NanoReality.Engine.UI.Views.OffersTimer
{
    public class OffersTimerGlobalView : View
    {
        [SerializeField] private GameObject _timerGameObject;
        [SerializeField] private TextMeshProUGUI _timerText;

        public void SetText(string text)
        {
           _timerText.text = text;
        }

        public void SetActiveTimerGameObject(bool enable)
        {
           _timerGameObject.SetActive(enable);
        }
    }
}