﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.Utilities;
using strange.extensions.mediation.impl;

public class ScrollBarStartAligment : View
{
    [Range(0, 1)] public float HorizontalNormalizedPosition = 0;
    [Range(0, 1)] public float VerticalNormalizedPosition = 1;

    public bool IsNeedToWaitPanelShowAnimation = false;

    [Inject]
    public virtual SignalOnShownPanel jSignalOnShownPanel { get; set; }

    /// <summary>
    /// For optimization
    /// </summary>
    public CacheMonobehavior CacheMonobehavior
    {
        get { return _cacheMonobehavior ?? (_cacheMonobehavior = new CacheMonobehavior(this)); }
    }

    private CacheMonobehavior _cacheMonobehavior;

    public void OnEnable()
    {
        if (IsNeedToWaitPanelShowAnimation)
        {
            jSignalOnShownPanel.AddOnce(o =>
            {
                StartCoroutine(SetScrollValuesByNextFrame());
            });
        }
        else
            StartCoroutine(SetScrollValuesByNextFrame());
    }

    public void OnDisable()
    {
        SetScrollValues();
    }

    private IEnumerator SetScrollValuesByNextFrame()
    {
        yield return null; // wait next frame

        SetScrollValues();
    }

    private void SetScrollValues()
    {
        var scrollRect = CacheMonobehavior.GetCacheComponent<ScrollRect>();

        if (scrollRect == null) return;

        if (scrollRect.horizontal)
            scrollRect.horizontalNormalizedPosition = HorizontalNormalizedPosition;

        if (scrollRect.vertical)
            scrollRect.verticalNormalizedPosition = VerticalNormalizedPosition;
    }
}