﻿using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.UI.Views.Hud;
using DG.Tweening;
using Engine.UI.Extensions;
using NanoReality.Engine.UI.Extensions.MainPanel;
using NanoReality.Game.Services.BuildingsUnlockService;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.Quests;
using NanoReality.Game.UI.BuildingsShopPanel;
using NanoReality.StrangeIoC;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Views.Hud
{
    public sealed class HudBuildingsPanelView : AView
    {
        #region Injects

        [Inject] public IMapObjectsData jMapObjectsData { get; set; }
        [Inject] public IBuildingsUnlockService jBuildingsUnlockService { get; set; }
        [Inject] public IHintTutorial jHintTutorial { get; set; }
        [Inject] public IHardTutorial jHardTutorial { get; set; }
        [Inject] public RoadConstructSetActiveSignal jRoadConstructSetActiveSignal { get; set; }
        [Inject] public PlayerOpenedPanelSignal JSignalPlayerOpenedPanel { get; set; }
        [Inject] public SignalOnMapObjectBuildFinished jOnMapBuildFinishedSignal { get; set; }
        [Inject] public BuildingConstructionStartedSignal jBuildingConstructionStartedBuildingConstructionStartedSignal { get; set; }
        [Inject] public SignalOnBuildingUnlocked jOnBuildingUnlockedSignal { get; set; }
        [Inject] public SignalOnBuildingRepaired jSignalOnBuildingRepaired { get; set; }
        [Inject] public SignalOnUserAchievmentsInit jSignalOnUserAchievmentsInit { get; set; }
        [Inject] public AchievementCompletedSignal jAchievementCompletedSignal { get; set; }
        [Inject] public SignalOnUserAchievementUnlocked jSignalOnUserAchievementUnlocked { get; set; }
        [Inject] public SignalOnQuestAchive jSignalOnQuestAchive { get; set; }
        [Inject] public SignalOnQuestComplete jSignalOnQuestComplete { get; set; }
        [Inject] public ShowWindowSignal jShowWindowSignal { get; set; }
        [Inject] public UserLevelUpSignal jUserLevelUpSignal { get; set; }
        [Inject] public OnHudBottomPanelWithoutPanelVisibleSignal jOnHudBottomPanelWithoutPanelVisibleSignal { get; set; }
        [Inject] public SignalOnShownPanel jUiPanelOpenedSignal { get; set; }
        
        #endregion

        #region Fields and Properties

        [SerializeField] private Button _cityBuildingsButton;
        [SerializeField] private Button _farmBuildingsButton;
        [SerializeField] private Button _industryBuildingsButton;
        [SerializeField] private Button _buildingsPanelButton;
        [SerializeField] private Button _overlayHideButton;

        [SerializeField] private HudNotificationButton _cityShopNotification;
        [SerializeField] private HudNotificationButton _farmShopNotification;
        [SerializeField] private HudNotificationButton _heavyShopNotification;
        [SerializeField] private RectTransform _buildingShopNotification;

        [SerializeField] private Material _normal;
        [SerializeField] private Material _locked;
        [SerializeField] private HudViewTooltip _hudViewTooltip;

        [SerializeField] private RectTransform _buildingsPaneTransform;
        


        private Tween _cityButtonTween;
        private Tween _agroButtonTween;
        private Tween _industryButtonTween;
        private Tween _doorAnimationTween;
        private Tween _casinoButtonTween;
        private Vector2 StartPosition => new Vector2(_buildingsPaneTransform.sizeDelta.x, 50);
        private Vector2 FinishPosition => new Vector2(_buildingsPaneTransform.sizeDelta.x, 500);

        public Button CityBuildingsButton => _cityBuildingsButton;
        public Button FarmBuildingsButton => _farmBuildingsButton;
        public Button IndustryBuildingsButton => _industryBuildingsButton;
        public Button BuildingsPanelButton => _buildingsPanelButton;

        private bool _farmLocked = true;
        private Ease _animationEase= Ease.OutBounce;
        private float _animationDuration = .5f;
        private bool _isClickLock;

        public bool FarmLocked
        {
            get => _farmLocked;
            set
            {
                _farmLocked = value;
                _farmBuildingsButton.GetComponent<Image>().material = value ? _locked : _normal;
            }
        }

        #endregion

        #region MonoBehaviour implementation

        protected override void Start()
        {
            base.Start();

            _buildingsPanelButton.onClick.AddListener(OnBuildingsPanelButtonClicked);
            _overlayHideButton.onClick.AddListener(OnBuildingsPanelButtonClicked);
            _cityBuildingsButton.onClick.AddListener(OnCityClicked);
            _farmBuildingsButton.onClick.AddListener(OnAgroClicked);
            _industryBuildingsButton.onClick.AddListener(OnIndustryClicked);
            jUiPanelOpenedSignal.AddListener(ForceHidePanel);

            AddListenersForBuildingsUpdate();
            UpdateBuildingsNotifications();
        }

        protected override void OnDestroy()
        {
            _buildingsPanelButton.onClick.RemoveListener(OnBuildingsPanelButtonClicked);
            _overlayHideButton.onClick.RemoveListener(OnBuildingsPanelButtonClicked);
            _cityBuildingsButton.onClick.RemoveListener(OnCityClicked);
            _farmBuildingsButton.onClick.RemoveListener(OnAgroClicked);
            _industryBuildingsButton.onClick.RemoveListener(OnIndustryClicked);
            jUiPanelOpenedSignal.RemoveListener(ForceHidePanel);
            RemoveListenersForBuildingsUpdate();
            base.OnDestroy();
        }
        
        private void OnDisable()
        {
            ClosePanel();
        }

        #endregion
        
        #region Button animations

        public void DoCityAnimation(bool isPlay)
        {
            _cityButtonTween?.Kill();

            if (isPlay)
            {
                _cityButtonTween = PlayButtonScaleAnimation(_cityBuildingsButton);
            }
        }

        public void DoAgroAnimation(bool isPlay)
        {
            _agroButtonTween?.Kill();

            if (isPlay)
            {
                _agroButtonTween = PlayButtonScaleAnimation(_farmBuildingsButton);
            }
        }

        public void DoIndustryAnimation(bool isPlay)
        {
            _industryButtonTween?.Kill();

            if (isPlay)
            {
                _industryButtonTween = PlayButtonScaleAnimation(_industryBuildingsButton);
            }
        }

        private Tween PlayButtonScaleAnimation(Button button)
        {
            Vector2 punchScale = new Vector2(0.1f, 0.1f);
            float duration = 2.0f;
            return button.GetRectTransform().DOPunchScale(punchScale, duration, 1, 0.5f).SetLoops(-1).Play();
        }

        #endregion

        #region Button events
        
        private void OnCityClicked()
        {
            StartBuildingInSector(BusinessSectorsTypes.Town);
        }

        private void OnBuildingsPanelButtonClicked()
        {
            if (_isClickLock) return;
            
            _isClickLock = true;

            var isActive = _buildingsPaneTransform.gameObject.activeInHierarchy;
            jOnHudBottomPanelWithoutPanelVisibleSignal.Dispatch(HudType.Buildings, isActive);
            
            if (!isActive)
            {
                _buildingsPaneTransform.gameObject.SetActive(true);
                _overlayHideButton.gameObject.SetActive(true);
                DoAnimateSizeDelta(_buildingsPaneTransform, StartPosition, FinishPosition, () =>
                {
                    _isClickLock = false;
                });
                return;
            }

            ClosePanel();
        }

        private void ForceHidePanel(UIPanelView _)
        {
            if (!_buildingsPaneTransform.gameObject.activeInHierarchy || !jHardTutorial.IsTutorialCompleted || jHintTutorial.IsHintTutorialActive) return;
            
                jOnHudBottomPanelWithoutPanelVisibleSignal.Dispatch(HudType.Buildings, true);
                ClosePanel();
        }

        private void ClosePanel()
        {
            DoAnimateSizeDelta(_buildingsPaneTransform, FinishPosition, StartPosition, () =>
            {
                _buildingsPaneTransform.gameObject.SetActive(false);
                _overlayHideButton.gameObject.SetActive(false);
                _isClickLock = false;
                
            });
        }
        
        private void DoAnimateSizeDelta(RectTransform target, Vector2 start, Vector2 stop, TweenCallback action = null)
        {
            target.sizeDelta = start;
            _doorAnimationTween = target.DOSizeDelta(stop, _animationDuration).SetEase(_animationEase).OnComplete(action);
        }

        public void SetUnlockGetter(string unlockLevel)
        {
            _hudViewTooltip.SetUnlockLevel(unlockLevel);
        }

        private void OnAgroClicked()
        {
            if (FarmLocked)
            {
                OnFarmLockedClick();
            }
            else
            {
                OnFarmUnlockedClick();
            }
        }

        private void OnIndustryClicked()
        {
            StartBuildingInSector(BusinessSectorsTypes.HeavyIndustry);
        }

        private void OnFarmLockedClick()
        {
            _hudViewTooltip.transform.position = new Vector3( _hudViewTooltip.transform.position.x, _farmBuildingsButton.transform.position.y);
            _hudViewTooltip.AnimationIn();
        }

        private void OnFarmUnlockedClick()
        {
            StartBuildingInSector(BusinessSectorsTypes.Farm);
        }

        #endregion

        private void UpdateBuildingsNotifications()
        {
            bool buildingShopNotificationEnable = false;
            int cityNotificationsCount = GetCountBuildingsForSector((int) BusinessSectorsTypes.Town);
            int agroNotificationsCount = GetCountBuildingsForSector((int) BusinessSectorsTypes.Farm);
            int industryNotificationsCount = GetCountBuildingsForSector((int) BusinessSectorsTypes.HeavyIndustry);

            _buildingShopNotification.gameObject.SetActive(buildingShopNotificationEnable);
            
            _cityShopNotification.Value = cityNotificationsCount;
            _heavyShopNotification.Value = industryNotificationsCount;
            _farmShopNotification.Value = agroNotificationsCount;
            
            int GetCountBuildingsForSector(Id<IBusinessSector> sectorId)
            {
                List<int> buildings = GetMapObjectTypes(sectorId);
                int count = jBuildingsUnlockService.GetAvailableBuildingsCountFromList(buildings);
                if (count > 0) buildingShopNotificationEnable = true;
                return count;
            }
        }

        #region Buildings help methods

        private List<int> GetMapObjectTypes(Id<IBusinessSector> businessSectorID)
        {
            var mapObjects = new List<int>();

            List<IMapObject> sectorObjects = jMapObjectsData.GetAllMapObjectsForSector(businessSectorID);
            foreach (IMapObject mapObject in sectorObjects.Where(c => c.Level == 0 && c.IsAvailableBuildingType()))
            {
                int mapObjectId = mapObject.ObjectTypeID;
                if (!mapObjects.Contains(mapObjectId))
                {
                    mapObjects.Add(mapObjectId);
                }
            }

            return mapObjects;
        }

        #endregion

        private void AddListenersForBuildingsUpdate()
        {
            jUserLevelUpSignal.AddListener(UpdateBuildingsNotifications);
            jOnMapBuildFinishedSignal.AddListener(mapObject => UpdateBuildingsNotifications());
            jOnBuildingUnlockedSignal.AddListener(notifications => UpdateBuildingsNotifications());
            jBuildingConstructionStartedBuildingConstructionStartedSignal.AddListener(notifications => UpdateBuildingsNotifications());
            jSignalOnBuildingRepaired.AddListener((type, Id) => UpdateBuildingsNotifications());
            jSignalOnUserAchievmentsInit.AddListener(UpdateBuildingsNotifications);
            jAchievementCompletedSignal.AddListener(UpdateBuildingsNotifications);
            jSignalOnUserAchievementUnlocked.AddListener(UpdateBuildingsNotifications);
            jSignalOnQuestAchive.AddListener(quest => UpdateBuildingsNotifications());
            jSignalOnQuestComplete.AddListener((quest) => UpdateBuildingsNotifications());
        }

        private void RemoveListenersForBuildingsUpdate()
        {
            jUserLevelUpSignal.RemoveListener(UpdateBuildingsNotifications);
            jOnMapBuildFinishedSignal.RemoveListener(mapObject => UpdateBuildingsNotifications());
            jOnBuildingUnlockedSignal.RemoveListener(notifications => UpdateBuildingsNotifications());
            jBuildingConstructionStartedBuildingConstructionStartedSignal.RemoveListener(notifications => UpdateBuildingsNotifications());
            jSignalOnBuildingRepaired.RemoveListener((type, Id) => UpdateBuildingsNotifications());
            jSignalOnUserAchievmentsInit.RemoveListener(UpdateBuildingsNotifications);
            jAchievementCompletedSignal.RemoveListener(UpdateBuildingsNotifications);
            jSignalOnUserAchievementUnlocked.RemoveListener(UpdateBuildingsNotifications);
            jSignalOnQuestAchive.RemoveListener(quest => UpdateBuildingsNotifications());
            jSignalOnQuestComplete.RemoveListener((quest, view) => UpdateBuildingsNotifications());
        }

        private void StartBuildingInSector(BusinessSectorsTypes sector)
        {
            OnBuildingsPanelButtonClicked();
            jRoadConstructSetActiveSignal.Dispatch(false);
            jShowWindowSignal.Dispatch(typeof(BuildingsShopPanelView), new BuildingShopData(sector));
            JSignalPlayerOpenedPanel.Dispatch();
        }
    }

    public sealed class HudBuildingsPanelMediator : AMediator<HudBuildingsPanelView> { }
}