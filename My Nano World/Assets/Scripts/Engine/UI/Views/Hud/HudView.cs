﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using NanoLib.UI.Core;
using NanoReality.Engine.UI.Extensions.EditModePanels;
using NanoReality.Engine.UI.Extensions.Hud;
using NanoReality.Engine.UI.Views.Hud;
using NanoReality.Engine.UI.Views.TheEvent;
using NanoReality.Game.UI.SettingsPanel;
using NanoReality.GameLogic.Bank;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.TheEvent.Signals;
using UnityEngine;
using UnityEngine.UI;
using NanoReality.UI.Components;
using TMPro;

namespace Assets.Scripts.Engine.UI.Views.Hud
{
    public class HudView : UIPanelView
    {
        [SerializeField] private HudQuestButtonsView _hudQuestsPanel;
        [SerializeField] private HudBuildingsPanelView _hudBuildingsPanelView;
        [SerializeField] private HudRoadPanelView _hudRoadPanelView;
        [SerializeField] private GameObject _gameObjectAchievements;
        [SerializeField] private List<GameObject> _hidingButtons;
        [SerializeField] private List<HudObject> _hidingElements;

        [SerializeField] private Button _settingsButton;
        [SerializeField] private Button _editButton;
        [SerializeField] private Button _theEventButton;
        [SerializeField] private Button _offersButton;
        [SerializeField] private TextMeshProUGUI _theEventTimer;
        [SerializeField] private GameObject _hidingUIHolder;
        
        #region Injects

        [Inject] public PlayerOpenedPanelSignal JSignalPlayerOpenedPanel { get; set; }
        [Inject] public RoadConstructSetActiveSignal jRoadConstructSetActiveSignal { get; set; }
        [Inject] public OnHudBottomPanelIsVisibleSignal jOnHudBottomPanelIsVisibleSignal { get; set; }
        [Inject] public OnHudBottomPanelWithoutPanelVisibleSignal jOnHudBottomPanelWithoutPanelVisibleSignal { get; set; }
        [Inject] public SignalPopulationUpdate jSignalPopulationUpdate { get; set; }
        [Inject] public ShowWindowSignal jShowWindowSignal { get; set; }
        [Inject] public SignalOnConstructControllerChangeState jSignalOnConstructControllerChangeState { get; set; }
        [Inject] public IEditModeService jEditModeService { get; set; }
        [Inject] public IUIManager jUIManager { get; set; }
        [Inject] public SignalEnableEventButton JSignalEnableEventButton { get; set; }
        [Inject] public SignalHudIsInitialized JSignalHudIsInitialized { get; set; }
        [Inject] public SignalOpenTheEventPanel jSignalOpenTheEventPanel { get; set; }
        [Inject] public SignalSetEventTimerOnHud jSignalSetEventTimerOnHud { get; set; }
        [Inject] public OnHudButtonsIsClickableSignal jOnHudButtonsIsClickableSignal { get; set; }


        #endregion

        #region PublicFieldsSection

        public HudQuestButtonsView HudQuestsPanel => _hudQuestsPanel;

        public HudBuildingsPanelView HudBuildingsPanel => _hudBuildingsPanelView;

        public HudRoadPanelView HudRoadPanel => _hudRoadPanelView;

        public Button SettingsButton => _settingsButton;
        public Button EditButton => _editButton;
        public Button TheEventButton => _theEventButton;
        public Button OffersButton => _offersButton;

        #endregion

        private bool _isConstructionMode;
        private bool _eventButtonCanBeEnabled;

        public override void Initialize(object parameters)
        {
            base.Initialize(parameters);
            jSignalPopulationUpdate.Dispatch();
            jOnHudBottomPanelIsVisibleSignal.AddListener(OnHudBottomPanelIsVisible);
            jOnHudBottomPanelWithoutPanelVisibleSignal.AddListener(HideHudBottomPanelIsVisibleWithoutPanel);
            jSignalOnConstructControllerChangeState.AddListener(OnConstructionControllerStateChangedSignal);
            JSignalEnableEventButton.AddListener((enable) =>
            {
                _hidingElements.First(element => element.HudType == HudType.TheEvent).HudElement.SetActive(enable);
                _eventButtonCanBeEnabled = enable;
            });
            jSignalSetEventTimerOnHud.AddListener(SetEventTimer);
            JSignalHudIsInitialized.Dispatch();
        }

        private void OnEnable()
        {
            _settingsButton.onClick.AddListener(OnSettingButtonClick);
            _editButton.onClick.AddListener(OnEditButtonClick);
            _theEventButton.onClick.AddListener(OnTheEventButtonClick);
            _offersButton.onClick.AddListener(OnOffersButtonClick);
        }

        private void OnDisable()
        {
            _settingsButton.onClick.RemoveListener(OnSettingButtonClick);
            _theEventButton.onClick.RemoveListener(OnTheEventButtonClick);
            _editButton.onClick.RemoveListener(OnEditButtonClick);
            _offersButton.onClick.RemoveListener(OnOffersButtonClick);
        }

        #region OnSignalListener

        private void SetEventTimer(string text)
        {
            _theEventTimer.text = text;
        }

        private void OnHudBottomPanelIsVisible(bool isVisible)
        {
            if ((_isConstructionMode ||
                 _hidingUIHolder.activeInHierarchy == isVisible ||
                 jEditModeService.IsEditModeEnable ||
                 isVisible && jUIManager.IsNotDefaultViewVisible()) && !jUIManager.IsViewVisible(typeof(FxPopUpPanelView)))
                return;

            _hidingUIHolder.SetActive(isVisible);
            foreach (var hidingElement in _hidingElements)
            {
                if (hidingElement.HudType == HudType.TheEvent && !_eventButtonCanBeEnabled)
                {
                    continue;
                }
                hidingElement.HudElement.SetActive(isVisible);
            }

            if (!isVisible)
                FixButtonsAnimation();
        }

        private void OnConstructionControllerStateChangedSignal(bool isEnabledConstruct)
        {
            _isConstructionMode = isEnabledConstruct;
            if (!isEnabledConstruct) OnHudBottomPanelIsVisible(true);
        }

        private void HideHudBottomPanelIsVisibleWithoutPanel(HudType type, bool visible)
        {
            foreach (var hidingElement in _hidingElements)
            {
                if (hidingElement.HudType == type) continue;
                if (hidingElement.HudType == HudType.TheEvent && !_eventButtonCanBeEnabled) continue;

                hidingElement.HudElement.SetActive(visible);
            }
        }

        private void FixButtonsAnimation()
        {
            foreach (var button in _hidingButtons)
            {
                //button.GetComponent<Animator>().Play("Normal");
                button.transform.localScale = Vector3.one;
                button.GetComponent<Image>().color = Color.white;
            }
        }

        #endregion

        #region ButtonsEventLiseners

        private void OnSettingButtonClick()
        {
            jShowWindowSignal.Dispatch(typeof(SettingsPanelView), null);
            jRoadConstructSetActiveSignal.Dispatch(false);
            JSignalPlayerOpenedPanel.Dispatch();
        }

        private void OnOffersButtonClick()
        {
            jRoadConstructSetActiveSignal.Dispatch(false);
            JSignalPlayerOpenedPanel.Dispatch();
            jShowWindowSignal.Dispatch(typeof(BankView), BankTabType.Offers);
        }
        
        private void OnEditButtonClick()
        {
            jShowWindowSignal.Dispatch(typeof(LayoutEditorPanelView), null);
        }

        private void OnTheEventButtonClick()
        {
            jSignalOpenTheEventPanel.Dispatch();
        }

        public void OnFriendsButtonClick()
        {
            jRoadConstructSetActiveSignal.Dispatch(false);
            JSignalPlayerOpenedPanel.Dispatch();
        }

        public void OnAuctionButtonClick()
        {
            jRoadConstructSetActiveSignal.Dispatch(false);
            JSignalPlayerOpenedPanel.Dispatch();
        }

        #endregion

        [Serializable]
        public class HudObject
        {
            public GameObject HudElement;
            public HudType HudType = HudType.None;
        }
    }
    
    public enum HudType
    {
        None = 0,
        Settings = 1,
        Achivements = 2,
        Quests = 3,
        Roads = 4,
        Buildings = 5,
        TheEvent = 6,
        Offers = 7
    }
}