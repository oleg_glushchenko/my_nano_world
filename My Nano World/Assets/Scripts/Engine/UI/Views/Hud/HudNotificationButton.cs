﻿using DG.Tweening;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.Engine.UI.Views.Hud
{
    public class HudNotificationButton : MonoBehaviour
    {
        [SerializeField] public TextMeshProUGUI _value;

        private int _currentValue;
        private bool _isInitialized;
        private Tween _animationTween;
        private RectTransform _rectTransform;

        public int Value
        {
            get { return _currentValue; }
            set
            {
                var previousValue = _currentValue;
                _currentValue = value;
                _isInitialized = true;
                if (_value == null)
                {
                    Debug.LogError("Text object is not setted in: " + name);
                    return;
                }

                if (_currentValue != previousValue)
                    PlayCounterUpdatedAnimation();

                gameObject.SetActive(_currentValue > 0);
                _value.text = _currentValue.ToString();
            }
        }

        protected void Awake()
        {
            if(!_isInitialized)
                Value = 0;

            _rectTransform = (RectTransform) transform;
        }

        private void PlayCounterUpdatedAnimation()
        {
            if (_animationTween != null)
            {
                _animationTween.Kill(true);
            }

            _animationTween = _rectTransform.DOPunchScale(new Vector2(0.2f, 0.2f), 1, 5, 0.5f);
        }
    }
}