using NanoLib.UI.Model;
using NanoReality.UI.Components;

namespace NanoReality.Engine.UI.Extensions.MainPanel
{
    public sealed class HudPanelModel : Model
    {
        public UIViewModel RoadPanelModel { get; private set; }

        public UIViewModel BuildingsPanelModel { get; private set; }

        public UIViewModel QuestsPanelModel { get; private set; }
        
        public UIViewModel SettingsModel { get; private set; }
        
        public UIViewModel EditModel { get; private set; }

        public UIViewModel TheEventModel { get; private set; }
        
        public UIViewModel OffersModel { get; private set; }

        public HudPanelModel()
        {
            RoadPanelModel = new UIViewModel();
            BuildingsPanelModel = new UIViewModel();
            QuestsPanelModel = new UIViewModel();
            SettingsModel = new UIViewModel();
            EditModel = new UIViewModel();
            TheEventModel = new UIViewModel();
            OffersModel = new UIViewModel();
        }
    }
}