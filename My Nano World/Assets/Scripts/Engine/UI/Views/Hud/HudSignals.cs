﻿using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using strange.extensions.signal.impl;

namespace Assets.Scripts.Engine.UI.Views.Hud
{
    /// <summary>
    /// Dispatched when soft currency is update in hud view.
    /// </summary>
    public class SignalOnHudViewSoftCurrencyUpdated : Signal<int> { }

    /// <summary>
    /// Dispatched when hard currency is update in hud view.
    /// </summary>
    public class SignalOnHudViewHardCurrencyUpdated : Signal<int> { }

    public class SignalOnRoadControlsSetVisible : Signal<bool> { }

    public class OnHudBottomPanelIsVisibleSignal : Signal<bool> { }
    public class OnHudBottomPanelWithoutPanelVisibleSignal : Signal<HudType, bool> { }
    public class OnHudButtonsIsClickableSignal : Signal<bool> { }

    public class OnPlayerTooltipClickSignal : Signal { }
    
    public sealed class HighlightHudQuestButtonSignal : Signal<BusinessSectorsTypes, bool> {}
}