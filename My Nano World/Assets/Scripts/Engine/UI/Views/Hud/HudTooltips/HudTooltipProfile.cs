﻿using NanoLib.Services.InputService;
using UnityEngine;
using UnityEngine.UI;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using Assets.NanoLib.UI.Core.Components;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.UI.Views.Tooltips;
using Assets.Scripts.Engine.UI.Views.Trade.Items;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.api;
using Engine.UI.Components;
using NanoReality.Core.Resources;
using NanoReality.GameLogic.Services;
using NanoReality.GameLogic.SocialCommunicator.api;
using TMPro;

namespace NanoReality.Game.Ui.Hud
{
    public sealed class HudTooltipProfile : UIPanelView
    {
        #region Injects

        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IResourcesManager jResourcesManager { get; set; }

        [Inject] public IProductsData jProductsData { get; set; }

        [Inject] public SignalCameraSwipe jSignalCameraSwipe { get; set; }

        [Inject] public SignalOnNeedToShowTooltip jSignalOnNeedToShowTooltip { get; set; }

        [Inject] public SignalOnNeedToHideTooltip jSignalOnNeedToHideTooltip { get; set; }

        [Inject] public IIconProvider jIconProvider { get; set; }

        #endregion

        [SerializeField] private TextMeshProUGUI _cityName;
        [SerializeField] private TextMeshProUGUI _experienceValue;
        [SerializeField] private TextMeshProUGUI _userLevelText;
        [SerializeField] private Image _experienceBarImage;
        [SerializeField] private ProgressBar _experienceBar;

        [SerializeField] private GameObject _nextItemsPanel;
        [SerializeField] private ItemContainer _nextLevelProductsContainer;
        [SerializeField] private ProductCountCard _productItemPrefab;

        [SerializeField] private RawImage _playerAvatar;
        [SerializeField] private Texture2D _defaultImage;

        private int CurrentExperience => jPlayerProfile.CurrentLevelData.CurrentExperience;

        [PostConstruct]
        public void PostConstruct()
        {
            _nextLevelProductsContainer.InstaniteContainer(_productItemPrefab, 0);
        }

        [Deconstruct]
        public void Deconstruct()
        {
            _nextLevelProductsContainer.ClearContainer();
        }

        public override void Show()
        {
            if (Visible)
            {
                Hide();
                return;
            }

            base.Show();
            jSignalCameraSwipe.AddOnce(f => Hide());

            _cityName.text = jPlayerProfile.UserCity.CityName.ToUpper();
            UpdateCounters();

            _playerAvatar.texture = _defaultImage;
        }

        public void UpdateCounters()
        {
            _userLevelText.text = (jPlayerProfile.CurrentLevelData.CurrentLevel + 1).ToString();
            _experienceBarImage.fillAmount = CurrentExperience / (float) GetNextLevelExperience();
            _experienceBar.Value = CurrentExperience / (float) GetNextLevelExperience();
            _experienceValue.text = $"{CurrentExperience}/{GetNextLevelExperience()}";

            _nextLevelProductsContainer.ClearCurrentItems();

            var nextProducts = jProductsData.Products.FindAll(curr =>
                curr.UnlockLevel == jPlayerProfile.CurrentLevelData.CurrentLevel + 1);

            if (nextProducts.Count < 1)
            {
                _nextItemsPanel.SetActive(false);
                return;
            }

            _nextItemsPanel.SetActive(true);

            foreach (var product in nextProducts)
            {
                ProductCountCardInitData productInitData = new ProductCountCardInitData()
                {
                    ProductObject = product,
                    ProductSprite = jResourcesManager.GetProductSprite(product.ProductTypeID),
                    SpecialOrderSprite = jIconProvider.GetSpecialOrderIcon(product.SpecialOrder),
                    ShowOnlySimpleTools = false, //true
                    ProductsCount = null,
                    CurrentProductsCount = null,
                    NeedProductsCount = null,
                    IsCountNeed = false, //false
                    Type = TooltipProductType.Product
                };
                var item = (ProductCountCard) _nextLevelProductsContainer.AddItem();
                item.Initialize(productInitData);

                item.SignalOnSelected.RemoveListener(OnSelectSignal);
                item.SignalOnSelected.AddListener(OnSelectSignal);

                item.SignalOnHideTooltip.RemoveListener(OnHideSelectedItem);
                item.SignalOnHideTooltip.AddListener(OnHideSelectedItem);
            }
        }

        private int GetNextLevelExperience()
        {
            if (jPlayerProfile.CurrentLevelData.ObjectUserLevelsesBalanceData.Levels.Count <=
                jPlayerProfile.CurrentLevelData.CurrentLevel)
            {
                return jPlayerProfile.CurrentLevelData.ObjectUserLevelsesBalanceData
                    .Levels[jPlayerProfile.CurrentLevelData.CurrentLevel - 1].Experience;
            }

            return jPlayerProfile.CurrentLevelData.ObjectUserLevelsesBalanceData
                .Levels[jPlayerProfile.CurrentLevelData.CurrentLevel].Experience;
        }

        private void OnHideSelectedItem()
        {
            jSignalOnNeedToHideTooltip.Dispatch();
        }

        private void OnSelectSignal(TooltipData tooltipData)
        {
            switch (tooltipData.Type)
            {
                case TooltipType.ProductItems:
                    tooltipData.OnGoToClickAction = (Hide);
                    break;
            }

            jSignalOnNeedToShowTooltip.Dispatch(tooltipData);
        }

        public override void Hide()
        {
            jSignalOnNeedToHideTooltip.Dispatch();

            base.Hide();
        }
    }
}