﻿using System;
using Engine.UI;
using GameLogic.Taxes.Service;
using NanoLib.Core.Services.Sound;
using NanoReality.Engine.UI.Common;
using NanoReality.Game.UI.BalancePanel;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using UnityEngine;
using UnityEngine.UI;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.UI.Components;
using TMPro;

namespace NanoReality.Game.Ui.Hud
{
    public class HudTooltipPopulation : UiTooltipPanelView
    {
        [SerializeField] private TextMeshProUGUI _taxesToCollect;
        [SerializeField] private Button _collectButton;
        [SerializeField] private LoadingSpinner _loadingSpinner;
        
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public BalancePanelContext PanelContext { get; set; }
        [Inject] public ICityTaxesService jCityTaxesService { get; set; }
        [Inject] public IGameCamera jGameCamera { get; set; }
        [Inject] public AddRewardItemSignal jAddRewardItemSignal { get; set; }
        [Inject] public ISoundManager jSoundManager { get; set; }

        public sealed override void Show()
        {
            base.Show();

            _loadingSpinner.StartSpin();
            jCityTaxesService.LoadNewTaxes(() =>
            {
                _loadingSpinner.StopSpin();
                UpdatedButtonAndCoinsLabel();
            });
            
            float populationFill = PanelContext.TotalPopulation / PanelContext.MaxPopulation;

            if (populationFill < 0.5f)
            {
                jSoundManager.Play(SfxSoundTypes.HappinessBarLess50, SoundChannel.SoundFX);
            }
            else if (populationFill > 0.7f)
            {
                jSoundManager.Play(SfxSoundTypes.HappinessBarMore50, SoundChannel.SoundFX);
            }

            _collectButton.interactable = false;
            _taxesToCollect.text = string.Empty;


            jCityTaxesService.TaxesChanged += OnCurrentCoinsChanged;
        }

        public override void Hide()
        {
            base.Hide();

            jCityTaxesService.TaxesChanged -= OnCurrentCoinsChanged;
        }
        

        /// <summary>
        /// Сбор налогов по кнопке
        /// </summary>
        public void OnCollectButtonClick()
        {
            if (jCityTaxesService.AnyTaxesAccrued)
            {
                jCityTaxesService.TaxesCollected += OnTaxesCountCollected;
                jCityTaxesService.Collect();
                _taxesToCollect.text = jCityTaxesService.CityTaxes.CurrentCoins.ToString();
                UpdatedButtonAndCoinsLabel();
            }
        }

        private void OnTaxesCountCollected(int coins)
        {
            jCityTaxesService.TaxesCollected -= OnTaxesCountCollected;
            var startPos = jGameCamera.ScreenToWorldPoint(_taxesToCollect.gameObject.transform.position);
            jAddRewardItemSignal.Dispatch(new AddItemSettings(FlyDestinationType.SoftCoins, startPos, coins.ToString()));
            jCityTaxesService.TaxesChanged += OnCurrentCoinsChanged;
        }

        private void OnCurrentCoinsChanged()
        {
            UpdatedButtonAndCoinsLabel();
        }

        private void UpdatedButtonAndCoinsLabel()
        {
            _taxesToCollect.text = jCityTaxesService.CityTaxes.CurrentCoins.ToString();
            _collectButton.interactable = jCityTaxesService.AnyTaxesAccrued;
        }
    }
}