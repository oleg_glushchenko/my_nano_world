﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Класс для смены значения текстового поля, в зависимости от значения слайдера
/// </summary>
[ExecuteInEditMode]
public class SliderChangeValue : MonoBehaviour
{
    /// <summary>
    /// Ссылка на слайдер
    /// </summary>
    public Slider Slider;

    /// <summary>
    /// Текстовое поле, указывающее прогресс в единицах, от нуля до <see cref="MaxCount"/>
    /// </summary>
    public Text UnitsTextField;

    /// <summary>
    /// Максимальное количество единиц
    /// </summary>
    public int MaxCount;

    public int MinCount;

    /// <summary>
    /// Текстовое поле, указывающее прогресс в процентах, от нуля до 100
    /// </summary>
    public Text PercentsTextField;

    private int _cachedPercentageSliderValue = -1; // -1 назначается для того что бы в первом 
    private int _cachedUnitsSliderValue = -1;      // апдейте значения целевых строк обновились

    public void Update()
    {
        if (Slider != null)
        {
            if (UnitsTextField != null)
            {
                var unitsSliderValue = Mathf.RoundToInt((MaxCount - MinCount)*Slider.normalizedValue);
                if (_cachedUnitsSliderValue != unitsSliderValue)
                {
                    _cachedUnitsSliderValue = unitsSliderValue;
                    UnitsTextField.text = (MinCount + _cachedUnitsSliderValue).ToString();
                }
            }

            if (PercentsTextField != null)
            {
                var percentageSliderValue = (int)(Slider.normalizedValue * 100f);
                if (_cachedPercentageSliderValue != percentageSliderValue)
                {
                    _cachedPercentageSliderValue = percentageSliderValue;
                    PercentsTextField.text = string.Format("{0}%", _cachedPercentageSliderValue);
                }
            }
        }
    }
}