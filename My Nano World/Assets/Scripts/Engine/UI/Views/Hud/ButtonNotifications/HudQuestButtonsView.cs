﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.UI.Views.Hud;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Engine.UI.Views.Hud.ButtonNotifications;
using NanoReality.Engine.UI.Extensions.AchievementPanel;
using NanoReality.Engine.UI.Extensions.QuestPanel;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.GameLogic.Achievements;
using NanoReality.GameLogic.Achievements.impl;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using strange.extensions.mediation.impl;

namespace NanoReality.Engine.UI.Extensions.Hud
{
    /// <summary>
    /// View of left Hud panel.
    /// </summary>
    public class HudQuestButtonsView : View
    {
        private int _currentStep;
        private List<IQuest> _sortedQuests;

        [SerializeField] private List<QuestButtonView> _questsButtonViews;
        
        [SerializeField] private HudNotificationButton _achievemntNotification;

        [SerializeField] private Button _achievmentsButton;
        [SerializeField] private QuestView _questView;
        [SerializeField] private Button _moveUpButton;
        [SerializeField] private Button _moveDownButton;
        [SerializeField] private Transform _background;
        
        #region Injects

        [Inject] public IUserQuestData jUserQuestData { get; set; }
        [Inject] public IUserAchievementsData jUserAchievementData { get; set; }
        [Inject] public IHintTutorial jHintTutorial { get; set; }
        [Inject] public IHardTutorial jHardTutorial { get; set; }
        [Inject] public SignalOnQuestUnlock jSignalOnQuestUnlock { get; set; }
        [Inject] public SignalOnQuestsLoaded jQuestsLoadedSignal { get; set; }
        [Inject] public SignalOnQuestAchive jSignalOnQuestAchive { get; set; }
        [Inject] public SignalOnQuestComplete jSignalOnQuestComplete { get; set; }
        [Inject] public SignalOnQuestRequirmentItemClick jSignalOnQuestRequirmentItemClick { get; set; }
        [Inject] public SignalOnQuestsShowedByUser jSignalOnQuestsShowedByUser { get; set; }
        [Inject] public RoadConstructSetActiveSignal jRoadConstructSetActiveSignal { get; set; }
        [Inject] public PlayerOpenedPanelSignal jPlayerOpenedPanelSignal { get; set; }
        [Inject] public SignalOnUserAchievementUnlocked jSignalOnUserAchievementUnlocked { get; set; }
        [Inject] public SignalOnRewardClaimed jSignalOnRewardClaimed { get; set; }
        [Inject] public ShowWindowSignal jShowWindowSignal { get;set; }
        [Inject] public SignalOnShownPanel jUiPanelOpenedSignal { get; set; }
        #endregion
        
        public QuestView QuestView => _questView;
        
        private void OnEnable()
        {
            _achievmentsButton.onClick.AddListener(OnAchievementsClicked); ;
            
            if(jUserQuestData == null) return;
            
            UpdateQuestList();
        }

        private void OnDisable()
        {
            _questView.CloseView();
            _achievmentsButton.onClick.RemoveListener(OnAchievementsClicked);
        }

        [PostConstruct]
        public void PostConstruct()
        {
            jSignalOnUserAchievementUnlocked.AddListener(UpdateQuestList);
            jSignalOnQuestUnlock.AddListener(OnQuestStateChange);
            jSignalOnQuestAchive.AddListener(OnQuestStateChange);
            jSignalOnQuestComplete.AddListener(OnQuestStateChange);
            jSignalOnQuestRequirmentItemClick.AddListener(OnQuestRequirmentClick);
            jSignalOnQuestsShowedByUser.AddListener(OnSignalOnQuestsShowedByUser);
            jQuestsLoadedSignal.AddListener(UpdateQuestList);
            jSignalOnRewardClaimed.AddListener(UpdateAchievementsOnClaimReward);
            _moveUpButton.onClick.AddListener(UpButtonClick);
            _moveDownButton.onClick.AddListener(DownButtonClick);
            jUiPanelOpenedSignal.AddListener(HideQuestWindow);

            InitQuestButtons();
        }

        [Deconstruct]
        public void Deconstruct()
        {
            jSignalOnUserAchievementUnlocked.RemoveListener(UpdateQuestList);
            jSignalOnQuestUnlock.RemoveListener(OnQuestStateChange);
            jSignalOnQuestAchive.RemoveListener(OnQuestStateChange);
            jSignalOnQuestComplete.RemoveListener(OnQuestStateChange);
            jSignalOnQuestRequirmentItemClick.RemoveListener(OnQuestRequirmentClick);
            jSignalOnQuestsShowedByUser.RemoveListener(OnSignalOnQuestsShowedByUser);
            jQuestsLoadedSignal.RemoveListener(UpdateQuestList);
            jSignalOnRewardClaimed.RemoveListener(UpdateAchievementsOnClaimReward);
            jUiPanelOpenedSignal.RemoveListener(HideQuestWindow);
            
            DisposeQuestButtons();
        }

        private void InitQuestButtons()
        {
            UpdateQuestList();
            foreach (QuestButtonView view in _questsButtonViews)
            {
                view.ShowQuestPanelClicked += OnQuestButtonClick;
            }
        }

        private void DisposeQuestButtons()
        {
            foreach (QuestButtonView view in _questsButtonViews)
            {
                view.ShowQuestPanelClicked -= OnQuestButtonClick;
            }
        }

        private void OnQuestStateChange(IQuest _)
        {
            UpdateQuestList();
        }

        private void OnQuestRequirmentClick(QuestConditionIcon _)
        {
            UpdateQuestList();
        }

        private void OnSignalOnQuestsShowedByUser(BusinessSectorsTypes _)
        {
            UpdateQuestList();
        }

        public void UpdateQuestList()
        {
            _sortedQuests = GetQuests(UserQuestStates.Achived, UserQuestStates.Avaliable, UserQuestStates.InProgress)
                .OrderByDescending(q => q.QuestState == UserQuestStates.Achived)
                .ThenByDescending(q => ((IUserLevelAchievedCondition) q.UnlockConditions.First()).TargetUserLevel)
                .ThenByDescending(q => q.AwardActions.First().AwardType == AwardActionTypes.Experience)
                .ThenBy(q => q.AwardActions.First().AwardType).ToList();

            _currentStep = 0;
            UpdateQuestButtons();
        }

        private void UpdateQuestButtons()
        {
            _questView.SetActive(false);
            DisableUnusedButtons();
            int questIndex = _currentStep;

            if (_sortedQuests.Count == 0) return;

            foreach (QuestButtonView view in _questsButtonViews)
            {
                if (!view.gameObject.activeInHierarchy)
                {
                    continue;
                }

                IQuest quest = _sortedQuests[questIndex];
                view.UpdateButton(quest);
                questIndex++;
            }

            UpdateAchievements();
        }

        private void DisableUnusedButtons()
        {
            bool upButtonState = true;
            bool middleButtonState = true;
            bool bottomButtonState = true;
            bool backgroundState = true;

            switch (_sortedQuests.Count)
            {
                case 3:
                    backgroundState = false;
                    break;
                case 2:
                    bottomButtonState = backgroundState = false;
                    break;
                case 1:
                    upButtonState = bottomButtonState = backgroundState = false;
                    break;
                case 0:
                    upButtonState = middleButtonState = bottomButtonState = backgroundState = false;
                    break;
            }

            _questsButtonViews[0].gameObject.SetActive(upButtonState);
            _questsButtonViews[1].gameObject.SetActive(middleButtonState);
            _questsButtonViews[2].gameObject.SetActive(bottomButtonState);
            _background.gameObject.SetActive(backgroundState);
        }

        private void OnQuestButtonClick(QuestButtonView questButton)
        {
            ChangeClaimState(false);
            _questView.SetInfo(questButton.CurrentQuest, _questsButtonViews.IndexOf(questButton));
            _questView.OnClosedView += () => ChangeClaimState(true);
        }

        private void ChangeClaimState(bool state)
        {
            for (int i = 0; i < _questsButtonViews.Count; i++)
            {
                QuestButtonView view = _questsButtonViews[i];
                
                if(!view.gameObject.activeInHierarchy) continue;
                
                view.ChangeClaimState(state);
            }
        }

        private void OnAchievementsClicked()
        {
            jShowWindowSignal.Dispatch(typeof(AchievementPanelView), null);
            jRoadConstructSetActiveSignal.Dispatch(false);
            jPlayerOpenedPanelSignal.Dispatch();
        }

        private void UpdateAchievementsOnClaimReward(Achievement achievement, Transform transform)
        {
            UpdateAchievements();
        }
        
        private void UpdateAchievements()
        {
            int unclaimedCount = jUserAchievementData.EarnedAchievementsNumber;
            _achievemntNotification.Value = unclaimedCount;
            _achievemntNotification.gameObject.SetActive(unclaimedCount > 0);
        }

        private List<IQuest> GetQuests(params UserQuestStates[] states)
        {
            return jUserQuestData.UserQuestsData.FindAll(item => ((IList) states).Contains(item.QuestState));
        }

        private void DownButtonClick()
        {
            UpdateCurrentStep(1);
        }

        private void UpButtonClick()
        {
            UpdateCurrentStep(-1);
        }

        private void UpdateCurrentStep(int value)
        {
            int questIndex = _currentStep + value;

            if (questIndex + 2 >= _sortedQuests.Count || questIndex < 0)
            {
                return;
            }

            _currentStep = questIndex;
            UpdateQuestButtons();
        }

        public QuestButtonView GetQuestButton(int id)
        {
            return _questsButtonViews.FirstOrDefault(q => q.CurrentQuest != null && q.CurrentQuest.QuestId.Value == id);
        }
        
        private void HideQuestWindow(UIPanelView _)
        {
            if(!jHardTutorial.IsTutorialCompleted || jHintTutorial.IsHintTutorialActive) return;
            _questView.SetActive(false);
        }
    }
}
