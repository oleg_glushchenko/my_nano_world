﻿using System.Linq;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Services;
using NanoReality.StrangeIoC;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.UI.Views.Hud.ButtonNotifications
{
    public class QuestButtonNotificationView : AViewMediator
    {
        [SerializeField] private HudNotificationButton _cityNotification;
        [SerializeField] private HudNotificationButton _agroNotification;
        [SerializeField] private HudNotificationButton _industryNotification;

        [SerializeField] private Image _cityGiftBoxImage;
        [SerializeField] private Image _agroGiftBoxImage;
        [SerializeField] private Image _industryGiftBoxImage;

        [SerializeField] private GameObject _cityGiftBox;
        [SerializeField] private GameObject _agroGiftBox;
        [SerializeField] private GameObject _industryGiftBox;

        [Inject]
        public IUserQuestData jUserQuestData { get; set; }

        [Inject]
        public IIconProvider jIconProvider { get; set; }

        protected override void PreRegister()
        {
        }

        protected override void OnRegister()
        {
        }

        protected override void OnRemove()
        {
        }

        public virtual void UpdateNotifications()
        {
            UpdateNotification(BusinessSectorsTypes.Town, _cityNotification, _cityGiftBoxImage, _cityGiftBox);
            UpdateNotification(BusinessSectorsTypes.Farm, _agroNotification, _agroGiftBoxImage, _agroGiftBox);
            UpdateNotification(BusinessSectorsTypes.HeavyIndustry, _industryNotification, _industryGiftBoxImage,
                _industryGiftBox);
        }

        private void UpdateNotification(BusinessSectorsTypes sectorType, HudNotificationButton notificationButton,
            Image giftBoxImage, GameObject parentObject)
        {
            notificationButton.Value = jUserQuestData.GetActiveQuestsCount(sectorType);

            if (GetQuest(sectorType, UserQuestStates.Achived) != null)
            {
                var giftBoxType = jUserQuestData.FirstAchievedQuestGiftBoxType(sectorType);
                
                var giftBoxSprite = jIconProvider.GetRewardBoxSprite(giftBoxType);

                giftBoxImage.sprite = giftBoxSprite;

                parentObject.SetActive(true);
            }
            else
            {
                parentObject.SetActive(false);
            }
        }

        protected IQuest GetQuest(BusinessSectorsTypes sectorType, params UserQuestStates[] states)
        {
            return jUserQuestData.UserQuestsData.Find(item =>
                item.BusinessSector == sectorType
                && states.Contains(item.QuestState));
        }
    }
}