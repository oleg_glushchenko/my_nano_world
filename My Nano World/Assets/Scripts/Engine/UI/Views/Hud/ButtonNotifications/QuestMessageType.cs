﻿namespace NanoReality.Engine.UI.Extensions.Hud
{
    public enum QuestMessageType
    {
        NewQuestAvaliable,
        NeedGetReward
    }
}