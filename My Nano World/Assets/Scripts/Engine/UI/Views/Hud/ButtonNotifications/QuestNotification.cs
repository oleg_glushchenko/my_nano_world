﻿using System.Linq;
using Assets.NanoLib.UI.Core.Signals;
using Assets.Scripts.Engine.UI.Views.Hud;
using NanoReality.GameLogic.Quests;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace NanoReality.Engine.UI.Extensions.Hud
{
    /// <summary>
    /// Обновляет счетчик доступных квестов в хаде
    /// </summary>
    public class QuestNotification:View
    {
        /// <summary>
        /// Счетчик квестов
        /// </summary>
        [SerializeField]
        private HudNotificationButton _questNotification;

        [Inject]
        public SignalOnQuestsLoaded jSignalOnQuestsLoaded { get; set; }

        [Inject]
        public SignalOnQuestComplete jSignalOnQuestComplete { get; set; }

        [Inject]
        public SignalOnQuestAchive jSignalOnQuestAchive { get; set; }

        [Inject]
        public IUserQuestData jUserQuestData { get; set; }

        protected override void Start()
        {
            base.Start();
            jSignalOnQuestsLoaded.AddListener(OnQuestLoaded);
            jSignalOnQuestComplete.AddListener(OnQuestCompleted);
            jSignalOnQuestAchive.AddListener(OnQuestAchived);
        }

        private void OnEnable()
        {
            UpdateQuestsCount();
        }

        private void OnQuestAchived(IQuest quest)
        {
            UpdateQuestsCount();
        }

        private void OnQuestCompleted(IQuest quest)
        {
            UpdateQuestsCount();
        }

        private void OnQuestLoaded()
        {
            UpdateQuestsCount();
        }

        private void UpdateQuestsCount()
        {
            var count = jUserQuestData.UserQuestsData.Where(q => q.QuestState == UserQuestStates.Achived).ToList().Count;
            _questNotification.Value = count;
        }
    }
}