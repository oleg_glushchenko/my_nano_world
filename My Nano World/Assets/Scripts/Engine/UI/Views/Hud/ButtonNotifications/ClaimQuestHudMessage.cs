using System;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Extensions.Hud
{
    public sealed class ClaimQuestHudMessage : View
    {
        public event Action ClaimClicked;
        
        [SerializeField] private Button _claimButton;

        public Button ClaimButton => _claimButton;
        
        [PostConstruct]
        public void PostConstruct()
        {
            _claimButton.onClick.AddListener(OnClaimClicked);
        }

        [Deconstruct]
        public void Deconstruct()
        {
            _claimButton.onClick.RemoveListener(OnClaimClicked);
        }

        private void OnClaimClicked()
        {
            ClaimClicked?.Invoke();
        }
    }
}