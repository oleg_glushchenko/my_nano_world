﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using NanoReality.Engine.UI.Extensions.Hud;
using NanoReality.Engine.UI.Views.QuestEvents;
using NanoReality.Game.Tutorial;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Services;
using NanoReality.StrangeIoC;
using UnityEngine;
using UnityEngine.UI;

namespace Engine.UI.Views.Hud.ButtonNotifications
{
     public class QuestButtonView : AView
     { 
          public event Action<QuestButtonView> ShowQuestPanelClicked;

          private IQuest _currentQuest;

          [SerializeField] private QuestImageIcon QuestImageIcon;
          [SerializeField] private ClaimQuestHudMessage ClaimQuestMessage;
          [SerializeField] private Image Background;
          [SerializeField] private Image SubBackground;
          [SerializeField] private Image SectorImage;
          [SerializeField] private Button OpenQuestButton;
          [SerializeField] private List<SectorSprite> SectorSprites = new List<SectorSprite>();
          
          [Inject] public IIconProvider jIconProvider { get; set; }
          [Inject] public IHardTutorial jHardTutorial { get; set; }

          public IQuest CurrentQuest => _currentQuest;
          public ClaimQuestHudMessage ClaimMessage => ClaimQuestMessage;
          public Button OpenQuest => OpenQuestButton;
          
          private void OnEnable()
          {
               OpenQuestButton.onClick.AddListener(OnQuestButtonClicked);
               ClaimQuestMessage.ClaimButton.onClick.AddListener(CollectReward);
               
               if (_currentQuest == null)
                    return;
               
               ChangeClaimState(true);
          }

          private void OnDisable()
          {
               OpenQuestButton.onClick.RemoveListener(OnQuestButtonClicked);
               ClaimQuestMessage.ClaimButton.onClick.RemoveListener(CollectReward);
          }

          public void UpdateButton(IQuest quest)
          {
               UpdateView(quest.BusinessSector);
               jIconProvider.SetQuestImageIcon(QuestImageIcon, quest);
               SectorImage.sprite = jIconProvider.GetSprite(quest.AwardActions.First());
               _currentQuest = quest;
               ChangeClaimState(true);
          }

          public void ChangeClaimState(bool state)
          {
               SetActiveClaimMessage(state && _currentQuest.QuestState == UserQuestStates.Achived);
          }

          private void UpdateView(BusinessSectorsTypes sectorsType)
          {
               if (_currentQuest != null && sectorsType == _currentQuest.BusinessSector) return;

               SectorSprite sectorSprite = GetSectorSprite(sectorsType);
               Background.sprite = sectorSprite.Sprite;
               SubBackground.sprite = sectorSprite.SubSprite;
          }

          private SectorSprite GetSectorSprite(BusinessSectorsTypes sectorsTypes)
          {
               sectorsTypes = sectorsTypes == BusinessSectorsTypes.Global ? BusinessSectorsTypes.Town : sectorsTypes;
               return SectorSprites.Find(s => s.SectorsTypes == sectorsTypes);
          }

          private void OnQuestButtonClicked()
          {
               ShowQuestPanelClicked?.Invoke(this);
          }

          public void SetActiveClaimMessage(bool isActive)
          {
               ClaimQuestMessage.gameObject.SetActive(isActive && jHardTutorial.IsTutorialCompleted);
          }

          public void CollectReward()
          {
               ChangeClaimState(false);
               _currentQuest.CollectAward();
          }

          [Serializable]
          public class SectorSprite
          {
               public BusinessSectorsTypes SectorsTypes;
               public Sprite Sprite;
               public Sprite SubSprite;
          }
     }
}
