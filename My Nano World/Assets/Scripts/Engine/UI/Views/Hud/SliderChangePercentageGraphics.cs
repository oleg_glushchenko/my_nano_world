﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Класс для спрайта к слайдеру, и значений, при которых необходимо отображать этот спрайт
/// </summary>
[Serializable]
public class SliderFill
{
    /// <summary>
    /// Спрайт запонителя слайдера
    /// </summary>
    public Sprite Fill;

    /// <summary>
    /// Минимальное значение, для которого в слайдере будет отображаться этот спрайт (в целочисленных процентах от 0 до 100)
    /// </summary>
    public int MinValueToShowInPercents;

    /// <summary>
    /// Максимальное значение, для которого в слайдере будет отображаться этот спрайт (в целочисленных процентах от 0 до 100)
    /// </summary>
    public int MaxValueToShowInPercents;
};

/// <summary>
/// Класс для смены спрайтов заливки слайдера, в зависимости от значения слайдера
/// </summary>
[ExecuteInEditMode]
public class SliderChangePercentageGraphics : MonoBehaviour
{
    /// <summary>
    /// Список всех вариантов заливки
    /// </summary>
    public List<SliderFill> SliderFills;

    /// <summary>
    /// Ссылка на слайдер
    /// </summary>
    public Slider Slider;

    /// <summary>
    /// Закешированное изображение, которому будет назначаться спрайт
    /// </summary>
    private Image _cachedImage;

    /// <summary>
    /// Вызывается при включении этого объекта
    /// </summary>
    public void OnEnable()
    {
        _cachedImage = GetComponent<Image>();
    }

    private int _cachedPercentageSliderValue;

    /// <summary>
    /// Вызывается каждый фрейм
    /// </summary>
    public void Update()
    {
        if (SliderFills != null && Slider != null && _cachedImage != null)
        {
            var percentageSliderValue = (int) (Slider.value*100f);
            if (_cachedPercentageSliderValue != percentageSliderValue)
            {
                _cachedPercentageSliderValue = percentageSliderValue;

                foreach (var fill in SliderFills)
                {
                    if (_cachedPercentageSliderValue <= fill.MaxValueToShowInPercents &&
                        _cachedPercentageSliderValue >= fill.MinValueToShowInPercents)
                    {
                        _cachedImage.sprite = fill.Fill;
                        return;
                    }
                }
            }
        }
    }
}