﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsLanguageDropdown : MonoBehaviour
{
    [SerializeField]
    private Dropdown _langDropdown;

    private List<string> _languageCodes; 
    
    private void Start()
    {
        PopulateDropdown();
        _langDropdown.onValueChanged.AddListener(OnLanguageChanged);
    }

    private void PopulateDropdown()
    {
        _langDropdown.ClearOptions();
        _languageCodes = new List<string>();

        var langOptionsList = new List<Dropdown.OptionData>();

        foreach (var lang in LocalizationMLS.Instance.LanguagesContainer.AvailableLanguages)
        {
	        var langOptionData = new Dropdown.OptionData {text = lang.NativeName};

	        langOptionsList.Add(langOptionData);
            _languageCodes.Add(lang.TwoLetterISOLanguageName);
        }

        _langDropdown.AddOptions(langOptionsList);

        _langDropdown.value = _languageCodes.IndexOf(LocalizationMLS.Instance.Language);
    }

    private void OnLanguageChanged(int langIndex)
    {
        LocalizationMLS.Instance.ChangeLanguage(_languageCodes[langIndex]);
    }
}
