﻿using strange.extensions.signal.impl;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Views.SettingsPanel
{
    public class SettingsCheckmarkView : MonoBehaviour
    {
        public Signal<SettingsCheckmarkView, bool> OnCheckmarkStateChangedSignal = new Signal<SettingsCheckmarkView, bool>();

        [SerializeField] private Sprite CheckedImage;

        [SerializeField] private Sprite UncheckedImage;

        [SerializeField] private Image Background;

        [SerializeField] private ScrollRect CheckmarkScroll;

        /// <summary>
        /// Скорость автоматического возврата маркера в крайнее положение 
        /// </summary>
        public float AutoScrollVelocity = 1000;

        private float _lastPosition;

        /// <summary>
        /// Установка маркера
        /// </summary>
        /// <param name="isOn">Включен ли маркер</param>
        public void SetCheckmark(bool isOn)
        {
            CheckmarkScroll.normalizedPosition = new Vector2(isOn ? 0 : 1, 0);
            Background.sprite = isOn ? CheckedImage : UncheckedImage;
        }

        /// <summary>
        /// Вызывается для проверки положения маркера и установки его в крайнее положение (on/off)
        /// </summary>
        /// <param name="position"></param>
        public void CheckedMarkMoved(Vector2 position)
        {
            if (IsCheckmarkStopped(CheckmarkScroll.normalizedPosition.x))
            {
                if (position.x > 0 && position.x < 0.5f)
                {
                    CheckmarkScroll.velocity = new Vector2(AutoScrollVelocity, 0);
                }
                if (position.x < 1 && position.x >= 0.5f)
                {
                    CheckmarkScroll.velocity = new Vector2(-AutoScrollVelocity, 0);
                }
            }

            _lastPosition = position.x;

            // проверка на переключение состояния и сигнализации о переключении
            if (position.x >= 0.5f && Background.sprite !=  UncheckedImage)
            {
                Background.sprite = UncheckedImage;
                OnCheckmarkStateChangedSignal.Dispatch(this, false);
            }
            else if (position.x < 0.5f && Background.sprite !=  CheckedImage)
            {
                Background.sprite = CheckedImage;
                OnCheckmarkStateChangedSignal.Dispatch(this, true);
            }
        }

        /// <summary>
        /// Проверка на то, что маркер остановился
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        private bool IsCheckmarkStopped(float position)
        {
            return Mathf.Abs(_lastPosition - position) < 0.01;
        }

        private void Update()
        {
            // вынужденная проверка на остановку маркера, т.к. при перетаскивании мышью, не отрабатывает событие ScrollRect.OnValueChanged
            if (IsCheckmarkStopped(CheckmarkScroll.normalizedPosition.x))
            {
                CheckedMarkMoved(CheckmarkScroll.normalizedPosition);
            }
        }
    }
}