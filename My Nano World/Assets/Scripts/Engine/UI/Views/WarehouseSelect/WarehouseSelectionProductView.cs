using NanoReality.Game.UI;
using strange.extensions.pool.api;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public sealed class WarehouseSelectionProductView : MonoBehaviour, IPoolable
{
    [SerializeField] private Image _productIconImage;
    [SerializeField] private Image _selectedMarkImage;
    [SerializeField] private TextMeshProUGUI _productCountText;
    [SerializeField] private Button _selectButton;
    
    [SerializeField] private Image _backgroundImage;
    [SerializeField] private Sprite _notSelectedBackgroundSprite;
    [SerializeField] private Sprite _selectedBackgroundSprite;

    private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();

    public WarehouseSelectionProductModel Model { get; } = new WarehouseSelectionProductModel();
    public Button SelectButton => _selectButton;

    private void Start()
    {
        Model.Clicked.BindTo(_selectButton).AddTo(_compositeDisposable);
        Model.ProductSprite.Subscribe(sprite => _productIconImage.sprite = sprite).AddTo(_compositeDisposable);
        Model.ProductCount.Subscribe(count => _productCountText.text = count.ToString()).AddTo(_compositeDisposable);
        Model.Selected.Subscribe(OnSelectChanged).AddTo(_compositeDisposable);
    }

    private void OnDestroy()
    {
        _compositeDisposable.Dispose();
    }
    
    private void OnSelectChanged(bool isSelected)
    {
        _backgroundImage.sprite = isSelected ? _selectedBackgroundSprite : _notSelectedBackgroundSprite;
        _selectedMarkImage?.gameObject.SetActive(isSelected);
        _productCountText?.gameObject.SetActive(!isSelected);
    }

    #region IPoolable implementation

    public bool retain { get; }
        
    public void Restore() { }

    public void Retain()
    {
        gameObject.SetActive(true);
    }

    public void Release()
    {
        gameObject.SetActive(false);
        _productIconImage.sprite = null;
        _productIconImage.material = null;
        Model.Selected.SetValueAndForceNotify(false);
    }

    #endregion
    
}