﻿using Assets.NanoLib.UI.Core.Views;
using Assets.NanoLib.UtilitesEngine.CustomDebugging.Models.api;
using UnityEngine;

public class CloseGamePanelView : UIPanelView 
{
    [Inject]
    public IDebug jDebug { get; set; }

    public void OnOkButtonClick()
    {
        jDebug.Log("Game Close");
        Application.Quit();
    }

    public void OnCancelButtonClick()
    {
        Hide();
    }

    public override void Show()
    {
        //throw new System.NotImplementedException();
    }

    public override void Hide()
    {
        //throw new System.NotImplementedException();
    }
}
