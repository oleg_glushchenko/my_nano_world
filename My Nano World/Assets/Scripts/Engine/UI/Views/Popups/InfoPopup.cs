﻿using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Assets.Scripts.MultiLanguageSystem.Logic;
using I2.Loc;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.UI
{
    public sealed class InfoPopup : BasePopupView
    {
        [SerializeField] private TextMeshProUGUI _titleField;
        [SerializeField] private Localize _messageField;
        [SerializeField] private Button _confirmButton;
        [SerializeField] private TextMeshProUGUI _confirmButtonText;
    
        public override void Initialize(IPopupSettings settings)
        {
            base.Initialize(settings);
            
            _titleField.SetLocalizedText(settings.Title);
            _messageField.Term = settings.Message;
            _confirmButtonText.text = settings.ConfirmButtonText;
        }

        public void OnEnable()
        {
            _confirmButton.onClick.AddListener(OnUpdateClicked);
        }

        public void OnDisable()
        {
            _confirmButton.onClick.RemoveListener(OnUpdateClicked);
        }

        private void OnUpdateClicked()
        {
            OnOkButtonClick();
        }
    }
}