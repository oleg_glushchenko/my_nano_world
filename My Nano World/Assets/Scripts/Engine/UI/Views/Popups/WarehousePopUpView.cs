﻿using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Assets.Scripts.GameLogic.Utilites;
using Assets.Scripts.MultiLanguageSystem.Logic;
using I2.Loc;
using NanoLib.Core.Services.Sound;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.Engine.UI.Views.Popups
{
    public class WarehousePopUpView : BasePopupView
    {
        #region InspectorFields

        /// <summary>
        /// Текст заголовка
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI _titleText;

        /// <summary>
        /// Текст всплывающего окна
        /// </summary>
        [SerializeField]
        private Localize _messageText;

        /// <summary>
        /// Текст кнопки подтверждения
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI _confirmButtonText;

        /// <summary>
        /// Текст кнопки отмены
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI _cancelButtonText;

        /// <summary>
        /// Кнопока подтверждения
        /// </summary>
        [SerializeField]
        private GameObject _confirmButton;

        /// <summary>
        /// Кнопка отказа
        /// </summary>
        [SerializeField]
        private GameObject _cancelButton;

        #endregion

        #region Injects

        [Inject]
        public IMapObjectsData jMapObjectsData { get; set; }

        [Inject]
        public IGameManager jGameManager { get; set; }
        [Inject] public ISoundManager jSoundManager { get; set; }

        #endregion

        private bool _isNextWarehouseLvlAvailable;

        /// <summary>
        /// Инициализация
        /// </summary>
        /// <param name="settings">Настройки окна</param>
        public override void Initialize(IPopupSettings settings)
        {
            base.Initialize(settings);

            var s = (NotEnoughSpacePopupSettings) settings;
            
            jSoundManager.Play(SfxSoundTypes.AppWindow, SoundChannel.SoundFX);
            _titleText.SetLocalizedText(s.Title);
            _cancelButtonText.SetLocalizedText(s.CancelButtonText);

            _isNextWarehouseLvlAvailable = CheckWarehouseLvl();
            if (_isNextWarehouseLvlAvailable)
            {
                _confirmButtonText.SetLocalizedText(s.ConfirmButtonText);
                _messageText.Term = s.Message;
                _cancelButton.SetActive(true);
            }
            else
            {
                _confirmButtonText.SetLocalizedText(LocalizationMLS.Instance.GetText(LocalizationKeyConstants.SHARED_BTN_OK));
                _messageText.Term = "Localization/FULLWHPOPUP_MSG_ASK_INCREASE_WAREHOUSE_CAPACITY";
                _cancelButton.SetActive(false);
            }
        }
        
        private bool CheckWarehouseLvl()
        {
            var warehouse = jGameManager.FindMapObjectByType<IWarehouseMapObject>(MapObjectintTypes.Warehouse);
            
            //проверяем доступен ли следующий лвл склада
            var isNextLevelModelAvailable = jMapObjectsData.IsPresentBuildingIdAndLevel(warehouse.ObjectTypeID, warehouse.Level + 1);

            return isNextLevelModelAvailable;
        }
        
        public void OnConfirmClick()
        {
            if (_isNextWarehouseLvlAvailable) //если склад максимального уровня, отменяем переход в панельку
                OnOkButtonClick();
            else
                OnCancelButtonClick();
        }

        /// <summary>
        /// Клик по кнопке отмены
        /// </summary>
        public void OnCancelClick()
        {
            jSoundManager.Play(SfxSoundTypes.CloseWindow, SoundChannel.SoundFX);
            OnCancelButtonClick();
        }
    }
}