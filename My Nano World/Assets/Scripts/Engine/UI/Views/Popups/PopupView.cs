﻿using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Assets.Scripts.MultiLanguageSystem.Logic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.UI.Views.Popups
{
    public class PopupView : BasePopupView 
    {
        [SerializeField] protected TextMeshProUGUI HeaderText;
        [SerializeField] protected TextMeshProUGUI MessageText;
        [SerializeField] protected GameObject OkButton;
        [SerializeField] protected GameObject CancelButton;
        [SerializeField] protected TextMeshProUGUI OkButtonText;
        [SerializeField] protected TextMeshProUGUI CancelButtonText;
		[SerializeField] private Image ContentBgImage;
		[SerializeField] private Sprite PopupBgWhite;

        #region UIHandlers

        public void OnConfirmPressed()
        {
            OnOkButtonClick();
        }

        public void OnCancelPressed()
        {
            OnCancelButtonClick();
        }

        #endregion


        public override void Initialize(IPopupSettings settings)
        {
            HeaderText.SetLocalizedText(settings.Title);
            MessageText.SetLocalizedText(settings.Message);

            SetPopupButton(OkButton, OkButtonText, settings.ConfirmButtonText);
            SetPopupButton(CancelButton, CancelButtonText, settings.CancelButtonText);

            ContentBgImage.sprite = PopupBgWhite;
        }

        protected void SetPopupButton(GameObject button, TextMeshProUGUI buttonText, string text)
        {
            button.SetActive(text != null);
            buttonText.SetLocalizedText(text);
        }
    }
}