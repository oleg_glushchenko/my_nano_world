﻿using System;
using UnityEngine;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using I2.Loc;
using NanoLib.Core.Services.Sound;
using NanoReality.Core.Resources;
using NanoReality.GameLogic.Utilites;
using TMPro;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.UI.Views.Popups
{
	public class QualityConfirmPopupView : BasePopupView
	{
		[SerializeField] private TextMeshProUGUI _titleText;
		[SerializeField] private Localize _messageText;
        
		[SerializeField] private TextMeshProUGUI _confirmButtonText;
		[SerializeField] private TextMeshProUGUI _cancelButtonText;
		
		[SerializeField] protected Button _okButton;
		[SerializeField] protected Button _cancelButton;
		
		private QualityPopupSettings _settings;
		private bool _toggle;
		
		[Inject] public OnGraphicQualityPopupUpdateSignal jOnGraphicQualityPopupCancelSignal { get; set; }
		[Inject] public ISoundManager jSoundManager { get; set; }

		public override void Initialize(IPopupSettings settings)
		{
			base.Initialize(settings);
			_settings = (QualityPopupSettings) settings;
		}

		public override void Show(Action<PopupResult> callback)
		{
			base.Show(callback);
			jSoundManager.Play(SfxSoundTypes.AppWindow, SoundChannel.SoundFX);
			_titleText.text = LocalizationUtils.LocalizeL2("UICommon/CONFIRM_BUTTON");
			
			if (_settings.ChangeToLowQualityGraphic)
			{
				_messageText.Term = "UICommon/GRAPHIC_CONFIRM_MESSAGE";
				_confirmButtonText.text = LocalizationUtils.LocalizeL2("UICommon/QUALITY_CONFIRM_BUTTON_OK");
				_cancelButtonText.text =LocalizationUtils.LocalizeL2("UICommon/QUALITY_CONFIRM_BUTTON_CANCEL");
			}
			else
			{
				_messageText.Term ="UICommon/GRAPHIC_CONFIRM_MESSAGE_LOAD";
				_confirmButtonText.text = LocalizationUtils.LocalizeL2("UICommon/QUALITY_CONFIRM_BUTTON_OK_LOAD");
				_cancelButtonText.text = LocalizationUtils.LocalizeL2("UICommon/QUALITY_CONFIRM_BUTTON_CANCEL_LOAD");
			}
		}

		public void OnConfirmClick()
		{
			OnOkButtonClick();
			jSoundManager.Play(SfxSoundTypes.CloseWindow, SoundChannel.SoundFX);
		}
        
		public void OnCancelClick()
		{
			jSoundManager.Play(SfxSoundTypes.CloseWindow, SoundChannel.SoundFX);
			OnCancelButtonClick();
			jOnGraphicQualityPopupCancelSignal.Dispatch();
		}
	}
}
