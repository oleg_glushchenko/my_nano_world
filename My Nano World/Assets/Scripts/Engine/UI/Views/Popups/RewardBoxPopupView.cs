﻿using System;
using System.Collections;
using Assets.NanoLib.UI.Core.Components;
using Assets.NanoLib.UI.Core.Signals;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Engine.UI;
using NanoLib.Core.Services.Sound;
using NanoReality.Engine.UI.Views.LevelUpPanel;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Services;
using NanoReality.GameLogic.TheEvent;
using NanoReality.UI.Components;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.UI.Views.Popups
{
    public class RewardBoxPopupView : BasePopupView
    {
        #region Injects
        
        [Inject] public AddRewardItemSignal jAddRewardItemSignal { get;set; }
        [Inject] public IGameCamera jGameCamera { get; set; }
        [Inject] public PlayerOpenedPanelSignal JPlayerOpenedPanelSignal { get; set; }
        [Inject] public IIconProvider jIconProvider { get; set; }
        [Inject] public ISoundManager jSoundManager { get; set; }

        #endregion
        
        #region Layout
        
        [SerializeField]
        private ItemContainer _rewardItemsContainer;
        
        [SerializeField]
        private LevelUpRewardItem _rewardPrefab;
        
        [SerializeField]
        private ScrollRect _rewardsScroll;
        
        [SerializeField]
        private Animator _boxAnimator;

        [SerializeField]
        private float _autohideDelay = 3f;

        private int _giftBoxType;
        
        #endregion
        
        public override void Initialize(IPopupSettings settings)
        {
            base.Initialize(settings);

            if (!_rewardItemsContainer.IsInstantiateed)
            {
                _rewardItemsContainer.InstaniteContainer(_rewardPrefab, 0);
            }
            _rewardItemsContainer.ClearCurrentItems();
            
            _rewardsScroll.horizontalNormalizedPosition = 0;

            var rewardBoxPopupSettings = (RewardBoxPopupSettings)settings;
            _giftBoxType = rewardBoxPopupSettings.GiftBoxType;

            if (rewardBoxPopupSettings.IsEventReward)
            {
                foreach (var reward in rewardBoxPopupSettings.Awards)
                {
                    var rewardItem = (LevelUpRewardItem) _rewardItemsContainer.AddItem();
                    rewardItem.SetReward(((DisplayRewardInfo)reward).Sprite, reward);
                }
            }
            else
            {
                var awards = rewardBoxPopupSettings.Awards;
                if (awards != null)
                {
                    for (int i = 0; i < awards.Count; i++)
                    {
                        IAwardAction award = awards[i];
                        var rewardItem = (LevelUpRewardItem)_rewardItemsContainer.AddItem();

                        Sprite sprite = jIconProvider.GetSprite(award);
                        rewardItem.SetReward(sprite, award);
                    }
                }
            }
        }

        public override void Show(Action<PopupResult> callback)
        {
            base.Show(callback);
            JPlayerOpenedPanelSignal.Dispatch();
            PlayAnimation();
        }

        public override void Show()
        {
            base.Show();
            
            PlayAnimation();
        }
        
        private void PlayAnimation()
        {            
            jSoundManager.Play(SfxSoundTypes.GiftboxOpened, SoundChannel.SoundFX);
            _boxAnimator.SetTrigger("Start_" + _giftBoxType);            
            StartCoroutine(AutoHide());
        }

        private IEnumerator AutoHide()
        {
            yield return new WaitForSeconds(_autohideDelay);
            ShowRewardAnimations();
            OnOkButtonClick();
        }

        private void ShowRewardAnimations()
        {
            var rewards = _rewardItemsContainer.CurrentItems;
            foreach (var item in rewards)
            {
                var rewardITem = (LevelUpRewardItem) item;
                var pos = jGameCamera.ScreenToWorldPoint(rewardITem.Icon.gameObject.transform.position);
                var amount = rewardITem.Reward.AwardCount.ToString();
                switch (rewardITem.Reward.AwardType)
                {
                    case AwardActionTypes.Experience:
                        jAddRewardItemSignal.Dispatch(new AddItemSettings(FlyDestinationType.Experience, pos, amount));
                        break;
                    case AwardActionTypes.SoftCurrency:
                        jAddRewardItemSignal.Dispatch(new AddItemSettings(FlyDestinationType.SoftCoins, pos, amount));
                        break;
                    case AwardActionTypes.PremiumCurrency:
                        jAddRewardItemSignal.Dispatch(new AddItemSettings(FlyDestinationType.PremiumCoins, pos, amount));
                        break;
                    case AwardActionTypes.NanoGems:
                        jAddRewardItemSignal.Dispatch(new AddItemSettings(FlyDestinationType.NanoGems, pos, amount));
                        break;
                    case AwardActionTypes.GoldBars:
                        jAddRewardItemSignal.Dispatch(new AddItemSettings(FlyDestinationType.GoldCurrency, pos, amount));
                        break;
                    case AwardActionTypes.Lantern:
                        jAddRewardItemSignal.Dispatch(new AddItemSettings(FlyDestinationType.Lantern, pos, amount, rewardITem.Icon.sprite));
                        break;
                    default:
                        jAddRewardItemSignal.Dispatch(new AddItemSettings(FlyDestinationType.Warehouse, pos, amount, rewardITem.Icon.sprite));
                        break;
                }
            }

            _rewardItemsContainer.gameObject.SetActive(false);
        }
    }
}