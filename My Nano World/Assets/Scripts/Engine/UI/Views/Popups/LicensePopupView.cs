﻿using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Assets.Scripts.MultiLanguageSystem.Logic;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.Engine.UI.Views.Popups
{
    public class LicensePopupView : BasePopupView
    {
        [SerializeField] 
        protected TextMeshProUGUI _licenseLabel;

        public override void Initialize(IPopupSettings settings)
        {
            base.Initialize(settings);

            var licenseSettings = (LicensePopupSettings)settings;

            _licenseLabel.Localize(licenseSettings.LicenseKey);
        }
    }
}
