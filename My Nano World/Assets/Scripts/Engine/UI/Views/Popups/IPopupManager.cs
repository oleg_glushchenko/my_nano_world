﻿using System;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;

namespace Assets.Scripts.Engine.UI.Views.Popups
{
    public interface IPopupManager
    {
        void Show(IPopupSettings settings, Action<PopupResult> callback = null);
        void Show(IPopupSettings settings, Action onSuccessResult, Action onCancelResult = null);
        void ForceClosePopup(Type popupType);
        void ForceCloseAllActivePopups();
        bool IsPopupActive<TPopupType>() where TPopupType : BasePopupView;
        bool IsPopupActive(Type popupType);
        bool HasNoActivePopup();
        TPopupView GetActivePopupByType<TPopupView>() where TPopupView : BasePopupView;
        event Action AllPopupsClosed;
    }
}