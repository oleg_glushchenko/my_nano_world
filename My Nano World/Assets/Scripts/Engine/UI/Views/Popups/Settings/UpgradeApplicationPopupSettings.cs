﻿using System;
using Assets.Scripts.GameLogic.Utilites;
using NanoReality.Game.UI;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    public sealed class UpgradeApplicationPopupSettings : BasePopupSettings
    {
        public override Type PopupType => typeof(InfoPopup);
        
        public override string Title => LocalizationMLS.Instance.GetText(LocalizationKeyConstants.UPGRADEPOPUP_LABEL_UPDATE_AVAILABLE);
        
        public override string Message => "Localization/" + LocalizationKeyConstants.UPGRADEPOPUP_TEXT_DESCRIPTION;
        
        public override string ConfirmButtonText => LocalizationMLS.Instance.GetText(LocalizationKeyConstants.UPGRADEPOPUP_BTN_UPDATE);
    }
}
