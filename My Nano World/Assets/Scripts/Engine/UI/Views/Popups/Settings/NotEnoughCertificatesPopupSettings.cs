﻿using System;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    public class NotEnoughCertificatesPopupSettings : BasePopupSettings
    {
        public override Type PopupType
        {
            get { return typeof(NotEnoughCertificatesPopupView); }
        }
        
        public AdTypePanel AdType { get; private set; }
        public int Certificates { get; private set; }
        public int CertificateCost { get; private set; }

        public NotEnoughCertificatesPopupSettings(AdTypePanel adType, int certificates, int certificateCost)
        {
            AdType = adType;
            Certificates = certificates;
            CertificateCost = certificateCost;
        }
    }
}
