﻿using NanoLib.Core.Logging;
using System;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    public class UserReportPopupSettings : IPopupSettings
    {
        public string Title { get; set; }
        public string Message { get; set; }
        public string ConfirmButtonText
        {
            get
            {
                Logging.LogError("Not supported field");
                return string.Empty;
            }
        }

        public string CancelButtonText
        {
            get
            {
                Logging.LogError("Not supported field");
                return string.Empty;
            }
        }

        public Type PopupType => typeof(UserReportPopupView);
    }
}
