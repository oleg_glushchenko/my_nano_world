﻿using Assets.Scripts.GameLogic.Utilites;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    /// <summary>
    /// Настройки окна о ошибке биллинга
    /// </summary>
    class BillingErrorPopupSettings : BasePopupSettings
    {
        private bool _isSupportEnabled;

        public BillingErrorPopupSettings(bool isSupportEnbled)
        {
            _isSupportEnabled = isSupportEnbled;
        }

        /// <summary>
        /// Заголовок окна
        /// </summary>
        public override string Title
        {
            get { return LocalizationMLS.Instance.GetText(LocalizationKeyConstants.BILLINGERRORPOPUP_LABEL_BILLING_ERROR); }
        }

        /// <summary>
        /// Сообщение окна
        /// </summary>
        public override string Message
        {
            get { return LocalizationMLS.Instance.GetText(LocalizationKeyConstants.BILLINGERRORPOPUP_MSG_BILLING_ERROR); }
        }

        /// <summary>
        /// Текст кнопки подтверждения
        /// </summary>
        public override string ConfirmButtonText
        {
            get
            {
                return _isSupportEnabled ? 
                    LocalizationMLS.Instance.GetText(LocalizationKeyConstants.BILLINGERRORPOPUP_BTN_SUPPORT) :
                    LocalizationMLS.Instance.GetText(LocalizationKeyConstants.SHARED_BTN_OK);
            }
        }
    }
}
