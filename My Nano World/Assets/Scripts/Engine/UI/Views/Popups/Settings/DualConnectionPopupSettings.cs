﻿using Assets.Scripts.GameLogic.Utilites;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    /// <summary>
    /// Настройки окна о повторном подключении
    /// </summary>
    public class DualConnectionPopupSettings : BasePopupSettings
    {
        /// <summary>
        /// Заголовок окна
        /// </summary>
        public override string Title
        {
            get { return LocalizationMLS.Instance.GetText(LocalizationKeyConstants.DUALCONNPOPUP_LABEL_DUAL_CONNECTION); }
        }

        /// <summary>
        /// Сообщение окна
        /// </summary>
        public override string Message
        {
            get { return LocalizationMLS.Instance.GetText(LocalizationKeyConstants.DUALCONNPOPUP_MSG_DUAL_CONNECTION); }
        }

        /// <summary>
        /// Текст кнопки подтверждения
        /// </summary>
        public override string ConfirmButtonText
        {
            get { return LocalizationMLS.Instance.GetText(LocalizationKeyConstants.DUALCONNPOPUP_BTN_RELOAD_GAME); }
        }
    }
}