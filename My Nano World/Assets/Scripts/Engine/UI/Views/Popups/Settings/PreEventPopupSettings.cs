using System;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    public class PreEventPopupSettings : BasePopupSettings
    {
        public override Type PopupType => typeof(PreEventPopupView);

        public Action<string> TimerTickAction;
        public Action OnTimerEnds;
        public Action OnPopupHide;
        public int UnlockLevel;
        public bool LowLevel;
        public bool DisableTimer;
    }
}
