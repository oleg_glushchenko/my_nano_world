using System;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    public class PurchaseEventPopupSetting : BasePopupSettings
    {
        public float Price { get; set; }
        public override Type PopupType => typeof(PurchaseTheEventPopupView);

        public Action<string> TimerTickAction;
        public Action OnDisappearAnimationEnds;
        public Action OnBuyEventBuilding;
        public Action OnTimerEnds;
        public Action<bool> OnPurchase;
    }
}
