﻿using System;
using NanoReality.Engine.UI.Views.NotEnoughtResourcesPanel;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    public class NotEnoughResourcesPopupSettings : BasePopupSettings
    {
        public Price Price { get; set; }
        public bool BuyResourcesForLanterns { get; set; }
        public int LanternPrice { get; set; }
        public override Type PopupType => typeof(NotEnoughResourcesPopupView);
        
        public NotEnoughResourcesPopupSettings(Price price)
        {
            Price = price;
        }

        public NotEnoughResourcesPopupSettings(Price price, bool buyResourcesForLanterns, int cost)
        {
            Price = price;
            BuyResourcesForLanterns = buyResourcesForLanterns;
            LanternPrice = cost;
        }
    }
}
