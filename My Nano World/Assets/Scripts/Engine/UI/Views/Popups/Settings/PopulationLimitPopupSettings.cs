﻿using System;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    public class PopulationLimitPopupSettings : BasePopupSettings
    {
        public override Type PopupType
        {
            get { return typeof(PopulationLimitPopupView); }
        }
    }
}
