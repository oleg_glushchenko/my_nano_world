﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;

public class OrderDeskInfoPopupSettings : BasePopupSettings
{
    readonly string _title;
    private readonly List<Product> _toolboxItems;
    private readonly List<Product> _orderItems;
    private readonly BusinessSectorsTypes _sectorType;

    public override string Title
    {
        get { return _title; }
    }

    public BusinessSectorsTypes SectorType
    {
        get { return _sectorType; }
    }

    public List<Product> ToolboxItems
    {
        get { return _toolboxItems; }
    }

    public List<Product> OrderItems
    {
        get { return _orderItems; }
    }

    public OrderDeskInfoPopupSettings(string title, List<Product> toolboxItems, List<Product> orderItems, BusinessSectorsTypes sectorType)
    {
        _title = title;
        _toolboxItems = toolboxItems;
        _orderItems = orderItems;
        _sectorType = sectorType;
    }

    public override Type PopupType
    {
        get { return typeof(OrderDeskInfoPopup); }
    }
}
