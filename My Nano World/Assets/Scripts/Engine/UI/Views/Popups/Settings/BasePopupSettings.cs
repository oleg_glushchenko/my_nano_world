﻿using System;
using Assets.Scripts.GameLogic.Utilites;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    /// <summary>
    /// Базовые настройки окна сообщения
    /// </summary>
    public abstract class BasePopupSettings : IPopupSettings
    {
        /// <summary>
        /// Заголовок окна
        /// </summary>
        public virtual string Title { get; set; }

        /// <summary>
        /// Сообщение окна
        /// </summary>
        public virtual string Message { get; set; }

        /// <summary>
        /// Текст кнопки подтверждения
        /// </summary>
        public virtual string ConfirmButtonText { get; set; } = LocalizationMLS.Instance.GetText(LocalizationKeyConstants.SHARED_BTN_OK);


        /// <summary>
        /// Текст кнопки отмены
        /// </summary>
        public virtual string CancelButtonText { get; set; }

        /// <summary>
        /// Тип окна
        /// </summary>
        public virtual Type PopupType => typeof(PopupView);
    }
}