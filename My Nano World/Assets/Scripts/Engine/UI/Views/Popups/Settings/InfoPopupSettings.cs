﻿using System;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using NanoReality.Game.UI;

public sealed class InfoPopupSettings : BasePopupSettings
{
    public override Type PopupType => typeof(InfoPopup);
    
    public override string Title { get; set; }

    public override string Message { get; set; }

    public InfoPopupSettings(string title, string message)
    {
        Title = title;
        Message = message;
    }
}