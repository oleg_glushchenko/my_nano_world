﻿using System;
using Assets.Scripts.GameLogic.Utilites;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    public class ConfirmPopupSettings : BasePopupSettings
    {
        public override string ConfirmButtonText => LocalizationMLS.Instance.GetText(LocalizationKeyConstants.SHARED_BTN_YES);
        
        public override string CancelButtonText => LocalizationMLS.Instance.GetText(LocalizationKeyConstants.SHARED_BTN_NO);

        public override Type PopupType => typeof (ConfirmPopupView);
    }
}