﻿using System;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    public class AgreementPopupSettings : BasePopupSettings
    {
        public override Type PopupType
        {
            get { return typeof(AgreementPopupView); }
        }
    }
}
