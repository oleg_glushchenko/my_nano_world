﻿using Assets.Scripts.GameLogic.Utilites;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    public class UpgradeErrorPopupSettings : BasePopupSettings
    {

        /// <summary>
        /// Заголовок окна
        /// </summary>
        public override string Title
        {
            get { return LocalizationMLS.Instance.GetText(LocalizationKeyConstants.UPGRADE_LABEL_ERROR); }
        }

        /// <summary>
        /// Сообщение окна
        /// </summary>
        public override string Message
        {
            get { return LocalizationMLS.Instance.GetText(LocalizationKeyConstants.UPGRADE_MSG_ERROR); }
        }

        /// <summary>
        /// Текст кнопки подтверждения
        /// </summary>
        public override string ConfirmButtonText
        {
            get
            {
                return LocalizationMLS.Instance.GetText(LocalizationKeyConstants.SHARED_BTN_OK);
            }
        }

        /// <summary>
        /// Текст кнопки подтверждения
        /// </summary>
        public override string CancelButtonText
        {           
            get
            {
                return LocalizationMLS.Instance.GetText(LocalizationKeyConstants.SHARED_BTN_CANCEL);
            }
        }
    }
}
