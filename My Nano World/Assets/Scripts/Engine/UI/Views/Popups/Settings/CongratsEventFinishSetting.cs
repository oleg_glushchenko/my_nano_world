using System;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    public class CongratsEventFinishSetting : BasePopupSettings
    {
        public override string Message { get; set; }
        public override string Title { get; set; }
        public override Type PopupType => typeof(CongratsEventFinishPopupView);
        public Action OnClaim;
        public Action OnDisappearAnimationEnds;
        public CongratsEventFinishSetting(Action onClaim, Action onDisappearAnimationEnds )
        {
            OnClaim = onClaim;
            OnDisappearAnimationEnds = onDisappearAnimationEnds;
        }
    }
}

