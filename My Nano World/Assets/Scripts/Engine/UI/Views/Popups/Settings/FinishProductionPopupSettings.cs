﻿using System;
using Assets.Scripts.GameLogic.Utilites;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    /// <summary>
    /// Настройки окна завершения производства
    /// </summary>
    class FinishProductionPopupSettings : BasePopupSettings
    {
        /// <summary>
        /// Тип окна
        /// </summary>
        public override Type PopupType
        {
            get { return typeof (PurchaseByPremiumPopupView); }
        }

        /// <summary>
        /// Стоимость
        /// </summary>
        public int Price { get; private set; }
        
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="price">Стоимость</param>
        public FinishProductionPopupSettings(int price)
        {
            Price = price;
        }

	    public override string Message
	    {
		    get
		    {
			    return LocalizationMLS.Instance.GetText(LocalizationKeyConstants.UPGRADE_MSG_ERROR);
		    }
	    }
    }
}
