﻿using System;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    public class LicensePopupSettings : BasePopupSettings
    {
        public override Type PopupType
        {
            get { return typeof(LicensePopupView); }
        }
        
        public string LicenseKey { get; private set; }

        public LicensePopupSettings(string licenseKey)
        {
            LicenseKey = licenseKey;
        }
    }
}
