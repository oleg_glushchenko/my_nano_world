﻿using System;
namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
	public class QualityPopupSettings: BasePopupSettings
	{
		public override Type PopupType => typeof (QualityConfirmPopupView);
		public bool ChangeToLowQualityGraphic { get; set; }
	}
}