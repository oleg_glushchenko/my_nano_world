﻿using Assets.Scripts.GameLogic.Utilites;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    /// <summary>
    /// Настройки окна о недоступности биллинга
    /// </summary>
    class BillingNotSupportedPopupSettings : BasePopupSettings
    {
        /// <summary>
        /// Имя социальной сети
        /// </summary>
        private readonly string _socialName;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="socialName">Имя социальной сети</param>
        public BillingNotSupportedPopupSettings(string socialName)
        {
            _socialName = socialName;
        }

        /// <summary>
        /// Заголовок окна
        /// </summary>
        public override string Title
        {
            get { return LocalizationMLS.Instance.GetText(LocalizationKeyConstants.BILLINGERRORPOPUP_LABEL_BILLING_NOTSUPPORTED); }
        }

        /// <summary>
        /// Текст кнопки подтверждения
        /// </summary>
        public override string Message
        {
            get { return string.Format(LocalizationMLS.Instance.GetText(LocalizationKeyConstants.BILLINGERRORPOPUP_MSG_BILLING_NOTSUPPORTED), _socialName); }
        }
    }
}
