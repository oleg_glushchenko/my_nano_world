﻿using Assets.Scripts.GameLogic.Utilites;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    /// <summary>
    /// Настройки окна о потере интернета
    /// </summary>
    public class InternetFailedPopupSettings : BasePopupSettings
    {
        /// <summary>
        /// Заголовок окна
        /// </summary>
        public override string Title
        {
            get { return LocalizationMLS.Instance.GetText(LocalizationKeyConstants.NOCONNECTIONPOPUP_LABEL_NO_CONNECTION); }
        }

        /// <summary>
        /// Сообщение окна
        /// </summary>
        public override string Message
        {
            get { return LocalizationMLS.Instance.GetText(LocalizationKeyConstants.NOCONNECTIONPOPUP_MSG_NO_CONNECTION); }
        }
    }
}
