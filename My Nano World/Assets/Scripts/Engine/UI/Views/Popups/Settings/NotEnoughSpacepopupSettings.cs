﻿using System;
using Assets.Scripts.GameLogic.Utilites;

namespace Assets.Scripts.Engine.UI.Views.Popups.Settings
{
    public sealed class NotEnoughSpacePopupSettings : BasePopupSettings
    {
        public override string Title => LocalizationMLS.Instance.GetText(LocalizationKeyConstants.FULLWHPOPUP_LABEL_FULL_WAREHOUSE);

        public override string Message => "Localization/FULLWHPOPUP_MSG_ASK_INCREASE_WAREHOUSE_CAPACITY";

        public override string ConfirmButtonText => LocalizationMLS.Instance.GetText(LocalizationKeyConstants.SHARED_BTN_YES);

        public override string CancelButtonText => LocalizationMLS.Instance.GetText(LocalizationKeyConstants.SHARED_BTN_NO);

        public override Type PopupType => typeof (WarehousePopUpView);
    }
}