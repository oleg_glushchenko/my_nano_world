﻿using Assets.NanoLib.UI.Core.Components;
using Assets.NanoLib.UI.Core.Views;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.UI.Views.Popups
{
    public class UnlockAreaPopupView : UIPanelView
    {
        [SerializeField] private ItemContainer _container;
        [SerializeField] private UnlockAreaPanelItem _prefab;
        [SerializeField] private TextMeshProUGUI _costText;
        [SerializeField] private Button _buyButton;

        public ItemContainer Container => _container;

        public UnlockAreaPanelItem Prefab => _prefab;

        public TextMeshProUGUI CostText => _costText;
        
        public Button BuyButton => _buyButton;
    }
}