﻿using UnityEngine;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Assets.Scripts.MultiLanguageSystem.Logic;
using NanoLib.Core.Services.Sound;
using TMPro;

namespace Assets.Scripts.Engine.UI.Views.Popups
{
    public class ConfirmPopupView : BasePopupView
    {
        [SerializeField] private TextMeshProUGUI _titleText;
        [SerializeField] private TextMeshProUGUI _messageText;
        
        [SerializeField] private TextMeshProUGUI _confirmButtonText;
        [SerializeField] private TextMeshProUGUI _cancelButtonText;
        [Inject] public ISoundManager jSoundManager { get; set; }

        public override void Initialize(IPopupSettings settings)
        {
            base.Initialize(settings);
            jSoundManager.Play(SfxSoundTypes.AppWindow, SoundChannel.SoundFX);
            var s = (ConfirmPopupSettings) settings;

            _titleText.SetLocalizedText(s.Title);
            _messageText.SetLocalizedText(s.Message);
            _confirmButtonText.SetLocalizedText(s.ConfirmButtonText);
            _cancelButtonText.SetLocalizedText(s.CancelButtonText);
        }
        
        public void OnConfirmClick()
        {
            jSoundManager.Play(SfxSoundTypes.CloseWindow, SoundChannel.SoundFX);
            OnOkButtonClick();
        }
        
        public void OnCancelClick()
        {
            jSoundManager.Play(SfxSoundTypes.CloseWindow, SoundChannel.SoundFX);
            OnCancelButtonClick();
        }
    }
}