﻿using UnityEngine;
using UnityEngine.UI;
#pragma warning disable 649

namespace Assets.Scripts.Engine.UI.Views.Popups
{
    /// <summary>
    /// Окно оценивания игры
    /// </summary>
    public class RateUsPopupView : BasePopupView
    {
        /// <summary>
        /// Чекбокс 'не показывать снова'
        /// </summary>
        [SerializeField]
        private Toggle _laterToggle;


        /// <summary>
        /// Обработчик подтверждения оценивания
        /// </summary>
        public void OnRateClick()
        {
            OnOkButtonClick();
        }

        /// <summary>
        /// Обработчик отказа оценивания
        /// </summary>
        public void OnLaterClick()
        {
            PlayerPrefs.SetInt("is_show_rate_popup", _laterToggle.isOn ? 1 : 0);
            OnCancelButtonClick();
        }
    }
}