﻿using NanoReality.GameLogic.Configs;
using UnityEngine;
using UnityEngine.UI;
#if UNITY_IOS
using NanoLib.Core.Logging;
using Unity.Advertisement.IosSupport;
#endif

namespace Assets.Scripts.Engine.UI.Views.Popups
{
    public class AgreementPopupView : BasePopupView
    {
        [SerializeField] private Toggle _privacyPolicy;
        [SerializeField] private Toggle _termsOfUse;
        [SerializeField] private Toggle _collectingData;
        [SerializeField] private Button _privacyPolicyLink;
        [SerializeField] private Button _termsOfUseLink;
        [SerializeField] private Button _collectingDataPermissions;
        [SerializeField] private Button _confirmButton;

        [Inject] public LinksConfigsHolder jLinksHolder { get; set; }

        private bool _privacyPolicyAccepted;
        private bool _termsOfUseAccepted;
        private bool _collectingPopupWatched;
        private bool _collectingDataAccepted;

        private void OnEnable()
        {
            _privacyPolicy.onValueChanged.AddListener(AcceptPrivacyPolicy);
            _termsOfUse.onValueChanged.AddListener(AcceptTermsOfUse);
            _privacyPolicyLink.onClick.AddListener(OnPrivacyPolicyButtonClick);
            _termsOfUseLink.onClick.AddListener(OnTermsOfUseClick);
            _confirmButton.onClick.AddListener(OnOkButtonClick);

#if UNITY_IOS
            _collectingData.onValueChanged.AddListener(OnCollectingDataClick);
            _collectingDataPermissions.onClick.AddListener(OnCollectingDataClick);
#else
            _collectingData.gameObject.SetActive(false);
            _collectingPopupWatched = true;
            _collectingDataAccepted = true;
#endif
            UpdateView();
        }

        private void OnDisable()
        {
            _privacyPolicy.onValueChanged.RemoveListener(AcceptPrivacyPolicy);
            _termsOfUse.onValueChanged.RemoveListener(AcceptTermsOfUse);
            _privacyPolicyLink.onClick.RemoveListener(OnPrivacyPolicyButtonClick);
            _termsOfUseLink.onClick.RemoveListener(OnTermsOfUseClick);
            _confirmButton.onClick.RemoveListener(OnOkButtonClick);

#if UNITY_IOS
            _collectingData.onValueChanged.RemoveListener(OnCollectingDataClick);
            _collectingDataPermissions.onClick.RemoveListener(OnCollectingDataClick);
#endif
        }

        private void UpdateView()
        {
            _confirmButton.interactable = _privacyPolicyAccepted && _termsOfUseAccepted && _collectingPopupWatched;
        }

        private void AcceptPrivacyPolicy(bool value)
        {
            _privacyPolicyAccepted = value;

            UpdateView();
        }

        private void AcceptTermsOfUse(bool value)
        {
            _termsOfUseAccepted = value;

            UpdateView();
        }

        private void OnCollectingDataClick(bool value)
        {
            if (_collectingDataAccepted)
                _collectingDataAccepted = false;
            else
                OpenCollectingDataNativeWindow();

            UpdateView();
        }

        private void OnPrivacyPolicyButtonClick()
        {
            Application.OpenURL(jLinksHolder.PrivacyPolicyUrl);
        }

        private void OnTermsOfUseClick()
        {
            Application.OpenURL(jLinksHolder.TermsOfUseUrl);
        }

        private void OnCollectingDataClick()
        {
            OpenCollectingDataNativeWindow();
        }

        private void OpenCollectingDataNativeWindow()
        {
            _collectingPopupWatched = true;

#if UNITY_IOS
            var status = ATTrackingStatusBinding.GetAuthorizationTrackingStatus();
            $"App Tracking Transparency status: {status}".Log(LoggingChannel.Analytics);
            
            if(status == ATTrackingStatusBinding.AuthorizationTrackingStatus.NOT_DETERMINED)
            {
                "Open iOS Native Permissions window".Log(LoggingChannel.Analytics);
                ATTrackingStatusBinding.RequestAuthorizationTracking();
            }
#endif
        }

        private void OnApplicationFocus(bool hasFocus)
        {
#if UNITY_IOS
            if (hasFocus && _collectingPopupWatched)
                _collectingData.isOn = ATTrackingStatusBinding.GetAuthorizationTrackingStatus() == ATTrackingStatusBinding.AuthorizationTrackingStatus.AUTHORIZED;
#endif
        }
    }
}