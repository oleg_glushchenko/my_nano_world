using System;
using System.Collections;
using System.Globalization;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.UI.Views.Popups
{
    public class PurchaseTheEventPopupView : BasePopupView
    {
        [SerializeField] private Button _claimButton;
        [SerializeField] private Button _closeButton;
        [SerializeField] private TextMeshProUGUI _price;
        [SerializeField] private TextMeshProUGUI _timer;
        [SerializeField] private Animator _animator;
        [SerializeField] private GameObject _VFX;

        private Action _onDissapearAnimEnds;
        private Action _onBuyEventBuilding;
        private float _disappearAnimLength = 3f;
        private float _hideAnimationLength = 1f;
        private bool _pendingPurchase;

        public override void Initialize(IPopupSettings settings)
        {
            base.Initialize(settings);

            var popupSettings = settings as PurchaseEventPopupSetting;
            _price.text = popupSettings.Price.ToString(CultureInfo.InvariantCulture);
            popupSettings.TimerTickAction += OnTimerTick;
            _onDissapearAnimEnds = popupSettings.OnDisappearAnimationEnds;
            _onBuyEventBuilding = popupSettings.OnBuyEventBuilding;
            _animator.SetBool("Close", false);
            popupSettings.OnTimerEnds += Hide;
            popupSettings.OnPurchase += OnPurchase;
        }

        public void OnEnable()
        {
            _claimButton.onClick.AddListener(OnBuyClicked);
            _closeButton.onClick.AddListener(OnCloseClicked);
        }

        public void OnDisable()
        {
            _claimButton.onClick.RemoveListener(OnBuyClicked);
            _closeButton.onClick.RemoveListener(OnCloseClicked);
        }

        private void OnTimerTick(string time)
        {
            _timer.text = time;
        }

        private void OnBuyClicked()
        {
            if (_pendingPurchase) return;
            _pendingPurchase = true;
            _onBuyEventBuilding?.Invoke();
        }

        private void OnPurchase(bool success)
        {
            if (success)
            {
                GameObject.Instantiate(_VFX);
            }

            _animator.SetBool("Close", true);
            StartCoroutine(ActionsAfterDisappearAnimations(success));
        }

        private void OnCloseClicked()
        {
            _animator.SetBool("Close", true);
            Invoke("OnCancelButtonClick", _hideAnimationLength);
        }

        private IEnumerator ActionsAfterDisappearAnimations(bool success)
        {
            yield return new WaitForSeconds(_disappearAnimLength);
            if (success)
            {
                _onDissapearAnimEnds?.Invoke();
            }

            _pendingPurchase = false;
            OnOkButtonClick();
        }
    }
}