using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.UI.Views.Popups
{
    public class PreEventPopupView : BasePopupView
    {
        [SerializeField] private TextMeshProUGUI _timer;
        [SerializeField] private TextMeshProUGUI _levelNumber;
        [SerializeField] private Animator _animator;
        [SerializeField] private Button _closeButton;
        [SerializeField] private GameObject _levelRequirements; 
        [SerializeField] private GameObject _info; 
        [SerializeField] private GameObject _timerGameObject; 

        private PreEventPopupSettings _settings;
        private float _hideAnimationLength = 1f;

        public override void Initialize(IPopupSettings settings)
        {
            base.Initialize(settings);
            _settings = settings as PreEventPopupSettings;
            _settings.TimerTickAction += OnTimerTick;
            _settings.OnTimerEnds += Hide;

            if (_settings.LowLevel)
            {
                _levelNumber.text = _settings.UnlockLevel.ToString();
            }
            else
            {
                _levelRequirements.SetActive(false);
                _info.SetActive(true);
            }

            _timerGameObject.SetActive(!_settings.DisableTimer);
        }

        public override void Hide()
        {
            base.Hide();
            _settings.OnPopupHide.Invoke();
        }

        public void OnEnable()
        {
             _closeButton.onClick.AddListener(OnCloseClicked);
        }

        public void OnDisable()
        {
            _closeButton.onClick.RemoveListener(OnCloseClicked);
        }

        private void OnTimerTick(string time)
        {
            _timer.text = time;
        }

        private void OnCloseClicked()
        {
            _animator.SetBool("Close", true);
            Invoke("OnCancelButtonClick", _hideAnimationLength);
        }
    }
}