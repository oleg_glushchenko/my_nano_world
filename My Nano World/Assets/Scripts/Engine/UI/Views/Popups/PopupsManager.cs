﻿using UnityEngine;
using System;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using strange.extensions.mediation.impl;
using System.Collections.Generic;
using NanoLib.Core.Logging;
using UniRx;

namespace Assets.Scripts.Engine.UI.Views.Popups
{
    public class PopupsManager : View, IPopupManager
    {
        [SerializeField] private List<BasePopupView> _popupList;

        private readonly Dictionary<Type, BasePopupView> _popupPrefabs = new Dictionary<Type, BasePopupView>();
        private readonly ReactiveDictionary<Type, BasePopupView> _activePopups = new ReactiveDictionary<Type, BasePopupView>();
        private readonly Dictionary<Type, BasePopupView> _cachedPopups = new Dictionary<Type, BasePopupView>();
        public event Action AllPopupsClosed;

        [Inject] public PopupOpenedSignal jPopupOpenedSignal { get; set; }
        [Inject] public PopupClosedSignal jPopupClosedSignal { get; set; }

        protected override void Awake()
        {
            base.Awake();
            foreach (var popupView in _popupList)
            {
                if (popupView == null)
                {
                    "Popups list contains nullable prefabs links.".LogWarning(LoggingChannel.Ui);
                    continue;
                }

                var popupType = popupView.GetType();
                _popupPrefabs.Add(popupType, popupView);
            }
        }

        [PostConstruct]
        public void PostConstruct()
        {
            jPopupOpenedSignal.AddListener(OnPopupOpenedSignal);
            jPopupClosedSignal.AddListener(OnPopupClosedSignal);
            _activePopups.ObserveCountChanged().Subscribe(count =>
            {
                if (count == 0)
                {
                    AllPopupsClosed?.Invoke();
                }
            });
        }

        [Deconstruct]
        public void Deconstruct()
        {
            jPopupOpenedSignal.RemoveListener(OnPopupOpenedSignal);
            jPopupClosedSignal.RemoveListener(OnPopupClosedSignal);
        }

        public void Show(IPopupSettings settings, Action<PopupResult> callback = null)
        {
            var popup = GetPopupByType(settings.PopupType);
            popup.Initialize(settings);
            popup.RectTransform.SetAsLastSibling();
            popup.gameObject.SetActive(true);
            popup.Show(callback);
        }

        public void Show(IPopupSettings settings, Action onSuccessResult, Action onCancelResult)
        {
            Show(settings, popupResult =>
            {
                switch (popupResult)
                {
                    case PopupResult.Ok:
                        onSuccessResult?.Invoke();
                        break;

                    case PopupResult.Cancel:
                        onCancelResult?.Invoke();
                        break;
                }
            });
        }

        public void ForceClosePopup(Type popupType)
        {
            if (_activePopups.ContainsKey(popupType))
            {
                _activePopups[popupType].Hide();
            }
        }

        public void ForceCloseAllActivePopups()
        {
            var markForHidePopups = new List<BasePopupView>();
            foreach (var popup in _activePopups)
            {
                markForHidePopups.Add(popup.Value);
            }
            
            foreach (var popup in markForHidePopups)
            {
                popup.Hide();
            }
        }

        public bool IsPopupActive<TPopupType>() where TPopupType : BasePopupView
        {
            return IsPopupActive(typeof(TPopupType));
        }

        public bool IsPopupActive(Type popupType)
        {
            return _activePopups.ContainsKey(popupType);
        }

        public bool HasNoActivePopup()
        {
            return _activePopups.Count == 0;
        }

        private BasePopupView GetPopupByType(Type popupType)
        {
            if (!_popupPrefabs.TryGetValue(popupType, out var popupPrefab))
            {
                $"Can't open popup of type [{popupType.Name}]".LogError(LoggingChannel.Ui);
                return null;
            }

            if (_cachedPopups.TryGetValue(popupType, out var popupInstance))
            {
                _cachedPopups.Remove(popupType);
                return popupInstance;
            }

            return Instantiate(popupPrefab, transform);
        }

        public TPopupView GetActivePopupByType<TPopupView>() where TPopupView : BasePopupView
        {
            var popupType = typeof(TPopupView);
            if (!IsPopupActive(popupType))
            {
                $"Popup view with type [{popupType}] not found".LogError(LoggingChannel.Ui);
                return null;
            }

            return (TPopupView) _activePopups[popupType];
        }

        private void OnPopupClosedSignal(BasePopupView closePopupView)
        {
            closePopupView.gameObject.SetActive(false);

            var popupType = closePopupView.GetType();

            if (!_cachedPopups.ContainsKey(popupType))
                _cachedPopups.Add(popupType, closePopupView);

            if (_activePopups.ContainsKey(popupType))
                _activePopups.Remove(popupType);
            else
                "Can't remove closed popup. Its not found in active popups list.".LogError(LoggingChannel.Ui);
        }

        private void OnPopupOpenedSignal(BasePopupView openedPopupView)
        {
            if (_activePopups.ContainsKey(openedPopupView.GetType()))
            {
                $"Popup of type {openedPopupView.GetType().Name} already in active popups list.".LogError(LoggingChannel.Ui);
                return;
            }

            _activePopups.Add(openedPopupView.GetType(), openedPopupView);
            openedPopupView.gameObject.SetActive(true);
        }
    }
}