using System;
using System.Collections;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CongratsEventFinishPopupView : BasePopupView
{
    [SerializeField] private Button _claimButton;
    [SerializeField] private TextMeshProUGUI _title;
    [SerializeField] private TextMeshProUGUI _message;
    [SerializeField] private Animator _animator;
    [SerializeField] private GameObject _VFX;

    private Action _onClaim;
    private Action _onDissapearAnimEnds;
    private float _disappearAnimLength = 3f;
    private bool _claimed;

    public override void Initialize(IPopupSettings settings)
    {
        base.Initialize(settings);
        var popupSettings = settings as CongratsEventFinishSetting;
        _onClaim = popupSettings.OnClaim;
        _onDissapearAnimEnds = popupSettings.OnDisappearAnimationEnds;
        _message.text = popupSettings.Message;
    }

    public void OnEnable()
    {
        _claimButton.onClick.AddListener(OnClaimClicked);
    }

    public void OnDisable()
    {
        _claimButton.onClick.RemoveListener(OnClaimClicked);
    }

    private void OnClaimClicked()
    {
        if (_claimed) return;
        _claimed = true;
        _onClaim.Invoke();
        _animator.SetBool("Close", true);
        GameObject.Instantiate(_VFX);
        StartCoroutine(ActionsAfterAnimations());
    }

    private IEnumerator ActionsAfterAnimations()
    {
        yield return new WaitForSeconds(_disappearAnimLength);
        _onDissapearAnimEnds?.Invoke();
        OnOkButtonClick();
    }
}
