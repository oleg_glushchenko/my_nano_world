﻿using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using UnityEngine;
using UnityEngine.UI;
#pragma warning disable 649

namespace Assets.Scripts.Engine.UI.Views.Popups
{
    /// <summary>
    /// Окно подтверждения ускорения производства
    /// </summary>
    public class FinishProductionPopup : BasePopupView
    {
        /// <summary>
        /// Количество денег
        /// </summary>
        [SerializeField]
        private Text _countText;
        

        /// <summary>
        /// Инициализация
        /// </summary>
        /// <param name="settings">Настройки окна</param>
        public override void Initialize(IPopupSettings settings)
        {
            base.Initialize(settings);

            var s = (FinishProductionPopupSettings)settings;
            _countText.text = s.Price.ToString();
        }
    }
}