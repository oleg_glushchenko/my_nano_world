﻿using System;
using Assets.NanoLib.Utilities.Pulls;
using Assets.Scripts.Engine.UI.Views.Tooltips;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.UI.Views.Popups
{
    public class UnlockAreaPanelItem : GameObjectPullable
    {
        #region InspectorFields
        
        [SerializeField] private GameObject _readyItemCheckmark;
        [SerializeField] private LongPressEventTrigger _longPress;
        [SerializeField] private Image _backgroundImage;
        [SerializeField] private Image _innerBackgroundImage;
        [SerializeField] private Sprite _notEnoughBackground;
        [SerializeField] private Sprite _enoughBackground;
        [SerializeField] private Sprite _innerNotEnoughBackground;
        [SerializeField] private Sprite _innerEnoughBackground;
        [SerializeField] private Sprite _activeBackground;
        [SerializeField] private Sprite _activeInnerBackground;
        [SerializeField] private Image _productImage;
        [SerializeField] private TextMeshProUGUI _requirementsCounter;
        [SerializeField] private Image _requirementsCounterBack;
        [SerializeField] private Sprite _lessThanRequiredBack;
        [SerializeField] private Sprite _equalToRequiredBack;
        
        #endregion

        private UnlockAreaPanelItemModel _model;
        private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();

        public event Action<TooltipData> OnLongPress;
        
        public void Init(UnlockAreaPanelItemModel model)
        {
            _model = model;
            
            _productImage.sprite = model.Sprite;
            _requirementsCounter.text = $"{model.CurrentAmount}/{model.NeedAmount}";

            model.CurrentAmount.ObserveEveryValueChanged(rp => rp.Value)
                .Subscribe(OnCurrentAmountChanged)
                .AddTo(_compositeDisposable);

            DrawBackground();
        }

        public override void OnPush()
        {
            base.OnPush();

            OnLongPress = null;
            _compositeDisposable.Clear();
            _longPress.onLongPress.RemoveListener(OnPressCallback);
            _longPress.onPointerClick.RemoveListener(OnPressCallback);
        }

        public override void OnPop()
        {
            base.OnPop();
            
            _longPress.onLongPress.AddListener(OnPressCallback);
            _longPress.onPointerClick.AddListener(OnPressCallback);
        }

        private void OnCurrentAmountChanged(int value)
        {
            _requirementsCounter.text = $"{_model.CurrentAmount}/{_model.NeedAmount}";
            DrawBackground();
        }

        private void OnSelected()
        {
            _backgroundImage.sprite = _activeBackground;
            _innerBackgroundImage.sprite = _activeInnerBackground;
        }
        
        private void OnDeselected()
        {
            DrawBackground();
        }
        
        private void DrawBackground()
        {
            if(_model.CurrentAmount.Value >= _model.NeedAmount)
            {
                _backgroundImage.sprite = _enoughBackground;
                _innerBackgroundImage.sprite = _innerEnoughBackground;
                _requirementsCounterBack.sprite = _equalToRequiredBack;

                _requirementsCounter.gameObject.SetActive(true);
                _readyItemCheckmark.SetActive(true);
            }
            else
            {
                _backgroundImage.sprite = _notEnoughBackground;
                _innerBackgroundImage.sprite = _innerNotEnoughBackground;
                _requirementsCounterBack.sprite = _lessThanRequiredBack;
                
                _readyItemCheckmark.SetActive(false);
            }
        }

        private void OnPressCallback()
        {
            OnSelected();

            TooltipData data = new TooltipData
            {
                Type = TooltipType.ProductItems,
                ProductItem = _model.Product,
                OnTooltipHideAction = OnDeselected,
                ProductAmount = _model.NeedAmount,
                OnGoToClickAction = _model.Hide,
                ItemTransform = (RectTransform) transform
            };
                
            OnLongPress?.Invoke(data);
        }
    }

    public class UnlockAreaPanelItemModel
    {
        public int Id { get; set; }
        public Action Hide { get;  set; }

        public ReactiveProperty<int> CurrentAmount { get; set; }
        public int NeedAmount { get; set; }
        
        public Product Product { get; set; }
        public Sprite Sprite { get; set; }
    }
}