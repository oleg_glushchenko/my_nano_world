using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Assets.Scripts.MultiLanguageSystem.Logic;
using I2.Loc;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NetworkErrorPopup : BasePopupView
{
    [SerializeField] private TextMeshProUGUI _titleField;
    [SerializeField] private Localize _messageField;
    [SerializeField] private Button _reloadGameButton;
    [SerializeField] private Button _tryContinueButton;
    [SerializeField] private Button _closeButton;
    [SerializeField] private Button _sendUserReportButton;

    private NetworkErrorPopupSettings _settings;

    [Inject] public IPopupManager PopupManager { get; set; }

    bool IsVisibleTryContinueButton
    {
        get
        {
#if !DebugLog && !UNITY_EDITOR
            return false;
#else
            return _settings.TryResendRequestCallback != null;
#endif
        }
    }

    public override void Initialize(IPopupSettings settings)
    {
        base.Initialize(settings);
        _settings = (NetworkErrorPopupSettings) settings;
        _titleField.SetLocalizedText(settings.Title);
        _messageField.Term = settings.Message;
        _tryContinueButton.gameObject.SetActive(IsVisibleTryContinueButton);
    }

    private void OnEnable()
    {
        _reloadGameButton.onClick.AddListener(OnReloadClicked);
        _tryContinueButton.onClick.AddListener(OnTryContinueClicked);
        _closeButton.onClick.AddListener(OnReloadClicked);
        _sendUserReportButton.onClick.AddListener(OnSendUserReportClick);

    }

    private void OnDisable()
    {
        _reloadGameButton.onClick.RemoveListener(OnReloadClicked);
        _tryContinueButton.onClick.RemoveListener(OnTryContinueClicked);
        _closeButton.onClick.RemoveListener(OnReloadClicked);
        _sendUserReportButton.onClick.RemoveListener(OnSendUserReportClick);
    }

    private void OnSendUserReportClick() => PopupManager.Show(new UserReportPopupSettings());

    private void OnTryContinueClicked()
    {
        _settings.TryResendRequestCallback?.Invoke();
        Hide();
    }

    private void OnReloadClicked()
    {
        Hide();
        _settings.ReloadApplicationCallback?.Invoke();
    }
}
