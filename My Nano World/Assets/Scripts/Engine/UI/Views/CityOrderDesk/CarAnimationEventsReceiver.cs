﻿using NanoReality.GameLogic.Orders;
using strange.extensions.mediation.impl;

// ReSharper disable once CheckNamespace

namespace Assets.Scripts.Engine.UI.Views.CityOrderDeskPanel
{
    public class CarAnimationEventsReceiver : View
    {
        public SignalOnShowCarInTripElementsEvent SignalOnShowCarInTripElementsEvent =
            new SignalOnShowCarInTripElementsEvent();

        public SignalOnHideCarInTripElementsEvent SignalOnHideCarInTripElementsEvent =
            new SignalOnHideCarInTripElementsEvent();

        public SignalOnResultArrivedAnimationEndsEvent SignalOnResultArrivedAnimationEndsEvent =
            new SignalOnResultArrivedAnimationEndsEvent();

        public SignalOnGetNewOrderAnimationEndsEvent SignalOnGetNewOrderAnimationEndsEvent =
            new SignalOnGetNewOrderAnimationEndsEvent();

        /// <summary>
        /// Вызывается анимацией, включает элементы скипа ожидания, при отправке заказа
        /// </summary>
        public void ShowCarInTripElements()
        {
            SignalOnShowCarInTripElementsEvent.Dispatch();
        }

        /// <summary>
        /// Вызывается анимацией, выключает элементы скипа ожидания, при получении результата заказа
        /// </summary>
        public void HideCarInTripElements()
        {
            SignalOnHideCarInTripElementsEvent.Dispatch();
        }

        /// <summary>
        /// Вызывается анимацией, сообщает о том, что закончилась анимация приезда фуры с наградой
        /// </summary>
        public void OnResultArrivedAnimationEnds()
        {
            SignalOnResultArrivedAnimationEndsEvent.Dispatch();
        }

        /// <summary>
        /// Вызывается анимацией, сообщает о том, что анимация получения нового заказа закончилась
        /// </summary>
        public void OnGetNewOrderAnimationEnds()
        {
            SignalOnGetNewOrderAnimationEndsEvent.Dispatch();
        }
    }
}