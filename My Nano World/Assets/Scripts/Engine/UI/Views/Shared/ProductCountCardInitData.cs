﻿using JetBrains.Annotations;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using UnityEngine;

namespace Assets.Scripts.Engine.UI.Views.Trade.Items
{
    public struct ProductCountCardInitData
    {
        [CanBeNull] public Product ProductObject;
        [CanBeNull] public Sprite ProductSprite;
        [CanBeNull] public Sprite SpecialOrderSprite;
        public bool? ShowOnlySimpleTools; //default false
        public int? ProductsCount;
        public int? CurrentProductsCount;
        public int? NeedProductsCount;
        public bool IsCountNeed;
        public TooltipProductType? Type;
    }
}

