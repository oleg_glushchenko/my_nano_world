﻿using UnityEngine;

public class LoadingIconView : MonoBehaviour
{
    public float RotationSpeed = 5f;

    void Update()
    {
        if (isActiveAndEnabled)
        {
            gameObject.transform.Rotate(Vector3.forward, RotationSpeed);
        }
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
