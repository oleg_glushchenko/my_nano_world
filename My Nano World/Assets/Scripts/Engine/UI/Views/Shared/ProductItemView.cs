using Assets.Scripts.Engine.UI.Views.Tooltips;
using DG.Tweening;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.UI.Views.Trade.Items
{
    public class ProductItemView : MonoBehaviour
    {
        [SerializeField] private Image _productImage;
        [SerializeField] private TextMeshProUGUI _counterText;
        [SerializeField] private Image _counterBackgroundImage;
        [SerializeField] private Image _counterCheckMark;
        [SerializeField] private Sprite _counterDefaultSprite;
        [SerializeField] private Sprite _counterEnoughSprite;
        [SerializeField] private Sprite _counterNotEnoughSprite;
        [SerializeField] private Button _itemButton;

        public ProductItemModel Model { get; private set; } = new ProductItemModel();

        private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();
        
        private void Awake()
        {
            Model.IsActiveProperty.Subscribe(isActive => gameObject.SetActive(isActive)).AddTo(_compositeDisposable);
            Model.ProductSpriteProperty.Subscribe(OnProductSpriteChanged).AddTo(_compositeDisposable);
            Model.CurrentCountProperty.Subscribe(_ => UpdateCounter()).AddTo(_compositeDisposable);
            Model.RequiredCountProperty.Subscribe(_ => UpdateCounter()).AddTo(_compositeDisposable);
            Model.Clicked.BindTo(_itemButton).AddTo(_compositeDisposable);
            Model.Clicked.Subscribe(_ => { ShowGoToProductBuildingTooltip(); }).AddTo(_compositeDisposable);
        }

        private void OnDestroy()
        {
            _compositeDisposable.Dispose();
        }

        private void OnProductSpriteChanged(Sprite newSprite)
        {
            const float punchScale = 0.05f;
            const float duration = 0.25f;
            const int vibrato = 1;

            _productImage.sprite = newSprite;

            _productImage.transform.DORewind();
            _productImage.transform.DOPunchScale(punchScale * Vector2.one, duration, vibrato);
        }

        private void UpdateCounter()
        {
            Sprite counterSprite;
            string counterText;
            if (Model.RequiredCount == 0)
            {
                counterSprite = _counterDefaultSprite;
                counterText = Model.CurrentCount.ToString();
            }
            else
            {
                counterSprite = Model.CurrentCount >= Model.RequiredCount
                    ? _counterEnoughSprite
                    : _counterNotEnoughSprite;
                counterText = $"{Model.CurrentCount}/{Model.RequiredCount}";
            }

            _counterBackgroundImage.sprite = counterSprite;
            _counterText.text = counterText;

            if (Model.CurrentCount >= Model.RequiredCount)
            {
                _counterCheckMark.gameObject.SetActive(true);
                _counterText.gameObject.SetActive(true);
            }
            else
            {
                _counterCheckMark.gameObject.SetActive(false);
               
            }
        }
        
        private void ShowGoToProductBuildingTooltip()
        {
            TooltipData tooltipData = new TooltipData
            {
                ItemTransform = (RectTransform) _itemButton.transform,
                Type = TooltipType.ProductItems,
                ProductItem = new Product {ProductTypeID = Model.RequiredIdProperty.Value},
                ProductAmount = default
            };
            
            Model.ShowProductTooltip.Execute(tooltipData);
        }
    }

    public class ProductItemModel
    {
        public BoolReactiveProperty IsActiveProperty = new BoolReactiveProperty();
        public ReactiveProperty<Sprite> ProductSpriteProperty = new ReactiveProperty<Sprite>();
        public ReactiveProperty<int> CurrentCountProperty = new ReactiveProperty<int>();
        public ReactiveProperty<int> RequiredCountProperty = new ReactiveProperty<int>();
        public ReactiveProperty<int> RequiredIdProperty = new ReactiveProperty<int>();
        public ReactiveCommand Clicked = new ReactiveCommand();
        public ReactiveCommand<TooltipData> ShowProductTooltip = new ReactiveCommand<TooltipData>();

        public int CurrentCount => CurrentCountProperty.Value;
        
        public int RequiredCount => RequiredCountProperty.Value;
    }
}