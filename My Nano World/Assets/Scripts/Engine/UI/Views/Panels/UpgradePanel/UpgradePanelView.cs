﻿using System.Collections.Generic;
using Assets.NanoLib.UI.Core.Components;
using Assets.NanoLib.UI.Core.Views;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Assets.Scripts.Engine.UI.Views.Tooltips;
using Assets.Scripts.Engine.UI.Views.Trade.Items;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.api;
using Assets.Scripts.GameLogic.Utilites;
using Assets.Scripts.MultiLanguageSystem.Logic;
using NanoReality.Game.Attentions;
using Engine.UI;
using GameLogic.Taxes.Service;
using I2.Loc;
using NanoLib.Core.Logging;
using NanoReality.Engine.Utilities;
using NanoReality.Game.Services.BuildingsUnlockService;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Production;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Services;
using NanoReality.GameLogic.Utilites;
using NanoReality.UI.Components;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Views.Panels
{
    public class UpgradePanelView : UIPanelView
    {
        #region Injections

        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public IProductsData jProductsData { get; set; }
        [Inject] public IMapObjectsData jMapObjectsData { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public ICityTaxesService jCityTaxesService { get; set; }
        [Inject] public IIconProvider jIconProvider { get; set; }

        [Inject] public SignalOnNeedToShowTooltip jSignalOnNeedToShowTooltip { get; set; }
        [Inject] public SignalOnNeedToHideTooltip jSignalOnNeedToHideTooltip { get; set; }
        [Inject] public IBuildingsUnlockService jBuildingsUnlockService { get; set; }
        [Inject] public SignalOnProductsStateChanged jSignalOnProductsStateChanged { get; set; }
        [Inject] public AddRewardItemSignal jAddRewardItemSignal { get; set; }
        [Inject] public SwitchBuildingAttentionsSignal jSwitchBuildingAttentionsSignal { get; set; }

        #endregion

        #region Layout

        [SerializeField] private Image _buildingIconImage;

        [SerializeField] private Button _buttonUpgrade;
        [SerializeField] private Button _buttonUpgradeSoft;

        [SerializeField] private TextMeshProUGUI _upgradeSoftCost;
        [SerializeField] private ItemContainer _requirementsContainer;

        [SerializeField] private ProductCountCard _productCountCard;

        [SerializeField] private TextMeshProUGUI _upgradeTimeLabel;
        [SerializeField] private TextMeshProUGUI _buildingNameLabel;
        [SerializeField] private Localize _buildingDescriptionLabel;
        [SerializeField] private TextMeshProUGUI _buildingLevelLabel;

        [SerializeField] private GameObject _rewardLayout;

        [SerializeField] private UpgradeLongTapControl _rewardSpeed;
        [SerializeField] private TextMeshProUGUI _rewardProductionSpeedText;
        [SerializeField] private UpgradeLongTapControl _rewardSlot;
        [SerializeField] private TextMeshProUGUI _rewardSlotsCountText;

        [SerializeField] private UpgradeLongTapControl _rewardExperience;
        [SerializeField] private TextMeshProUGUI _rewardExpText;

        [SerializeField] private UpgradeLongTapControl _rewardIncreaseNanoStorage;

        [SerializeField] private TextMeshProUGUI _rewardIncreaseNanoStorageText;

        [SerializeField] private TextMeshProUGUI _upgradeUnlockLevelText;

        [SerializeField] private GameObject _unlockGameObjectContainer;
        [SerializeField] private GameObject _upgradeGameObjectContainer;

        [SerializeField] private UpgradeLongTapControl _rewardIncreaseDwellingHouses;
        [SerializeField] private TextMeshProUGUI _rewardIncreaseDwellingHousesText;

        [SerializeField] private UpgradeLongTapControl _rewardIncreaseDwellingHousesWithUpgrades;

        [SerializeField] private TextMeshProUGUI _rewardIncreaseDwellingHousesWithUpgradesText;

        [SerializeField] private GameObject _timeCounter;
        [SerializeField] private GameObject _requirementsView;

        #endregion

        #region Fields

        public Button ButtonUpgrade
        {
            get { return _buttonUpgrade; }
        }

        private IMapBuilding _buildingToUpgrade;

        private Price _productPrice;
        private Price _softPrice;

        private bool _isProductsEnough;

        #endregion

        #region Overrides of UIPanelView

        public override void Initialize(object parameters)
        {
            base.Initialize(parameters);

            if (!_requirementsContainer.IsInstantiateed)
                _requirementsContainer.InstaniteContainer(_productCountCard, 0);
            
            var building = (IMapBuilding)parameters;
            RefreshPanel(building);
        }

        public override void Show()
        {
            base.Show();

            // Hack! To avoid bug with multiple registrations with the same method
            jSignalOnProductsStateChanged.RemoveListener(OnProductsStateChanged);
            jSignalOnProductsStateChanged.AddListener(OnProductsStateChanged);
            _buttonUpgradeSoft.onClick.AddListener(OnUpgradeButtonClick);
            jSwitchBuildingAttentionsSignal.Dispatch(false);
        }

        /// <summary>
        /// Прячет панель
        /// </summary>
        public override void Hide()
        {
            jSwitchBuildingAttentionsSignal.Dispatch(true);
            jSignalOnProductsStateChanged.RemoveListener(OnProductsStateChanged);
            _buttonUpgradeSoft.onClick.RemoveListener(OnUpgradeButtonClick);
            base.Hide();
        }

        #endregion

        #region Methods

        private void OnProductsStateChanged(Dictionary<Id<Product>, int> products)
        {
            RefreshPanel(_buildingToUpgrade);
        }
        
        public void OnUpgradeButtonClick()
        {
            ;
            if (!jPlayerProfile.PlayerResources.IsEnoughtForBuy(_softPrice))
            {
                ShowPopup(new NotEnoughMoneyPopupSettings(PriceType.Soft,
                        _softPrice.SoftPrice - jPlayerProfile.PlayerResources.SoftCurrency), success =>
                    {
                        if (success == PopupResult.Ok)
                        {
                            Hide();
                        }
                    });
            }
            // если не хватает продуктов
            else if (!_isProductsEnough)
            {
                // открываем попап покупки продуктов
                jPopupManager.Show(new NotEnoughResourcesPopupSettings(_productPrice), success =>
                {
                    switch (success)
                    {
                        case PopupResult.Ok:
                            // и в случае успешной покупки, отмечаем, что теперь продуктов должно хватать, и рекурсивно
                            // запускаем эту функцию для повторной проверки на продукты и на софт валюту
                            _isProductsEnough = true;
                            OnUpgradeButtonClick();
                            break;
                        case PopupResult.Cancel:
                            Hide();
                            break;
                        case PopupResult.Hide:
                            break;
                    }
                });
            }
            // если всего хватает - запускаем апгрейд
            else
            {
                if (_buildingToUpgrade.MapObjectType == MapObjectintTypes.CityHall)
                {
                    CollectTaxes();
                }
                Upgrade();
                
                Hide();
            }
        }
        
        private void CollectTaxes()
        {
            if (jCityTaxesService.AnyTaxesAccrued)
            {
                jCityTaxesService.TaxesCollected += OnTaxesCountCollected;
                jCityTaxesService.Collect();
            }
        }
        
        /// <summary>
        /// По сбору налогов показываем анимацию
        /// </summary>
        private void OnTaxesCountCollected(int coins)
        {
            var view = jGameManager.GetMapObjectView(_buildingToUpgrade);
            var position = view != null ? view.WorldPosition : 
                new Vector3(_buildingToUpgrade.MapPosition.X, 0.0f, _buildingToUpgrade.MapPosition.Y);
            
            jAddRewardItemSignal.Dispatch(new AddItemSettings(global::Engine.UI.FlyDestinationType.SoftCoins, position, coins.ToString()));
            jCityTaxesService.TaxesCollected -= OnTaxesCountCollected;
        }

        private void Upgrade()
        {
            if (_buildingToUpgrade.SectorId == BusinessSectorsTypes.Global.GetBusinessSectorId())
            {
                jGameManager.CurrentUserCity.GlobalCityMap.UpgradeObject(_buildingToUpgrade.MapID);
            }
            else
            {
                jGameManager.GetMap(_buildingToUpgrade.SectorId).UpgradeObject(_buildingToUpgrade.MapID);
            }
        }
        
        public void RefreshPanel(IMapBuilding buildingToUpgrade)
        {
            _buildingToUpgrade = buildingToUpgrade;
            
            _requirementsContainer.ClearCurrentItems();

            var buildingId = _buildingToUpgrade.ObjectTypeID.Value;
            var nextBuildingLevel = buildingToUpgrade.Level + 1;
            var instanceIndex = _buildingToUpgrade.InstanceIndex;
            
            _buildingNameLabel.SetLocalizedText(_buildingToUpgrade.Name.ToUpper());
            _buildingDescriptionLabel.Term = $"Localization/OBJECT_BUILDING_DSCR_{buildingId}";
            
            _buildingLevelLabel.SetLocalizedText(string.Format(LocalizationMLS.Instance.GetText(LocalizationKeyConstants.UPGRADE_PANEL_BUILDING_LEVEL),
                nextBuildingLevel));

            _buildingIconImage.sprite = jIconProvider.GetBuildingSprite(_buildingToUpgrade.Id);                
            
            _buildingIconImage.gameObject.SetActive(true);                
            _timeCounter.SetActive(true);
            _buildingLevelLabel.gameObject.SetActive(true);

            if (!buildingToUpgrade.CanBeUpgraded)
            {
                return;
            }

            var nextLevelModel = (IMapBuilding) jMapObjectsData.GetByBuildingIdAndLevel(buildingId, nextBuildingLevel);
            if (nextLevelModel == null)
            {
                Logging.LogError(LoggingChannel.Core, "Next level model is missing, seems building is already on high level");
                return;
            }

            _buildingIconImage.sprite = jIconProvider.GetBuildingSprite(nextLevelModel.Id);

            var price = jBuildingsUnlockService.GetUpgradeBuildingPrice(buildingId, nextBuildingLevel, instanceIndex);
            _softPrice = price;

            if (price.ProductsPrice != null)
            {
                _productPrice = new Price
                {
                    PriceType = PriceType.Resources,
                    ProductsPrice = price.ProductsPrice
                };
                _isProductsEnough = jPlayerProfile.PlayerResources.IsEnoughtForBuy(_productPrice);
            }
            else
                _isProductsEnough = true;

            var constructionTime = jBuildingsUnlockService.GetBuildingConstructionTime(buildingId, nextBuildingLevel, instanceIndex);
            _upgradeTimeLabel.text = UiUtilities.GetFormattedTimeFromSeconds_H_M_S(constructionTime);

            SetRewardItemsForUpgrade(price);
            SetUpgradeButton(price);

            SetRewards(_buildingToUpgrade, nextLevelModel);
            CheckUpgradeLvl(_buildingToUpgrade);
        }

        private void SetUpgradeButton(Price price)
        {
            if (price.IsNeedSoft && price.SoftPrice != 0)
            {
                _buttonUpgradeSoft.gameObject.SetActive(true);
                _buttonUpgrade.gameObject.SetActive(false);

                _upgradeSoftCost.text = price.SoftPrice.ToString();
            }
            else
            {
                _buttonUpgradeSoft.gameObject.SetActive(false);
                _buttonUpgrade.gameObject.SetActive(true);
            }
        }
        
        private void SetRewardItemsForUpgrade(Price price)
        {
            if (price.IsNeedProducts && price.ProductsPrice != null)
            {
                foreach (var product in price.ProductsPrice)
                {
                    var count = jPlayerProfile.PlayerResources.GetProductCount(product.Key);
                    var rewardItem = (ProductCountCard) _requirementsContainer.AddItem();
                    var productData = jProductsData.GetProduct(product.Key);

                    ProductCountCardInitData productInitData = new ProductCountCardInitData()
                    {
                        ProductObject = productData,
                        ProductSprite = jIconProvider.GetProductSprite(productData.ProductTypeID),
                        SpecialOrderSprite = jIconProvider.GetSpecialOrderIcon(productData.SpecialOrder),
                        ShowOnlySimpleTools = false,//true
                        ProductsCount = null,
                        CurrentProductsCount = count,
                        NeedProductsCount = product.Value,
                        IsCountNeed = true,//true
                        Type = TooltipProductType.Product
                    };

                    rewardItem.Initialize(productInitData);
                    AddListenersForTooltips(rewardItem);
                }
            }
            
            // если для апгрейда необходима хард валюта
            if (price.IsNeedHard && price.HardPrice != 0)
            {
                var rewardItem = (ProductCountCard) _requirementsContainer.AddItem();

                ProductCountCardInitData productInitData = new ProductCountCardInitData()
                {
                    ProductObject = null,
                    ProductSprite = jIconProvider.GetCurrencySprite(PriceType.Hard),
                    SpecialOrderSprite = null,
                    ShowOnlySimpleTools = false,//false
                    ProductsCount = null,
                    CurrentProductsCount = jPlayerProfile.PlayerResources.HardCurrency,
                    NeedProductsCount = price.HardPrice,
                    IsCountNeed = true,//true
                    Type = TooltipProductType.Hard
                };

                rewardItem.Initialize(productInitData);
            }
            
            _requirementsView.SetActive(_requirementsContainer.Count > 0);
        }

        private void AddListenersForTooltips(ProductCountCard rewardItem)
        {
            rewardItem.SignalOnSelected.RemoveListener(OnTooltipSelectSignal);
            rewardItem.SignalOnSelected.AddListener(OnTooltipSelectSignal);

            rewardItem.SignalOnHideTooltip.RemoveListener(OnTooltipHideSignal);
            rewardItem.SignalOnHideTooltip.AddListener(OnTooltipHideSignal);
        }

        private void OnTooltipSelectSignal(TooltipData tooltipData)
        {
            switch(tooltipData.Type)
            {
                case TooltipType.ProductItems:
                    tooltipData.OnGoToClickAction = (Hide);
                    break;
            }
            jSignalOnNeedToShowTooltip.Dispatch(tooltipData);
        }

        private void OnTooltipHideSignal()
        {
            jSignalOnNeedToHideTooltip.Dispatch();
        }

        private void CheckUpgradeLvl(IMapBuilding buildingModel)
        {
            int playerLevelForUpgrade = jBuildingsUnlockService.GetPlayerLevelForUpgrade(buildingModel.ObjectTypeID, 
                buildingModel.InstanceIndex, buildingModel.Level);

            if (jPlayerProfile.CurrentLevelData.CurrentLevel >= playerLevelForUpgrade)
            {
                _unlockGameObjectContainer.SetActive(false);
                _upgradeGameObjectContainer.SetActive(true);

                string text =  string.Format(LocalizationUtils.Localize(LocalizationKeyConstants.BUILDINGSHOP_ITEM_LABEL_UNLOCKS_AT_LVL), playerLevelForUpgrade);
                _upgradeUnlockLevelText.SetLocalizedText(text);
            }
            else
            {
                _unlockGameObjectContainer.SetActive(true);
                _upgradeGameObjectContainer.SetActive(false);
            }
        }

        private void SetRewards(IMapBuilding currentBuildingModel, IMapBuilding nextLevelModel)
        {
            _rewardLayout.SetActive(false);
            _rewardSpeed.gameObject.SetActive(false);
            _rewardSlot.gameObject.SetActive(false);
            _rewardExperience.gameObject.SetActive(false);
            _rewardIncreaseNanoStorage.gameObject.SetActive(false);
            _rewardIncreaseDwellingHouses.gameObject.SetActive(false);
            _rewardIncreaseDwellingHousesWithUpgrades.gameObject.SetActive(false);

            var buildingId = _buildingToUpgrade.ObjectTypeID.Value;
            var nextBuildingLevel = currentBuildingModel.Level + 1;
            var instanceIndex = _buildingToUpgrade.InstanceIndex;

            // награда в виде экспы за апгрейд
            var experienceReward = jBuildingsUnlockService.GetExperienceReward(buildingId, nextBuildingLevel, instanceIndex);
            if (experienceReward > 0)
            {
                _rewardExperience.gameObject.SetActive(true);
                _rewardExpText.text = experienceReward.ToString();
            }

            #region Production Buildings Rewards

            // для продакшн зданий
            var currentLevelProduction = currentBuildingModel as IProductionBuilding;
            if (currentLevelProduction != null)
            {
                var nextLevelProduction = (IProductionBuilding)nextLevelModel;

                // если после лвлапа будет более быстрое производство
                if (currentLevelProduction.AvailableProducingGoods.Count > 0 &&
                    nextLevelProduction.AvailableProducingGoods.Count > 0)
                {
                    var currentTime = currentLevelProduction.AvailableProducingGoods[0].ProducingTime;
                    var nextTime = nextLevelProduction.AvailableProducingGoods[0].ProducingTime;

                    if (currentTime > nextTime)
                    {
                        var fasterProductionSpeedPercentage = 100 - nextTime * 100 / currentTime;

                        if (!_rewardLayout.activeInHierarchy)
                            _rewardLayout.SetActive(true);

                        _rewardSpeed.gameObject.SetActive(true);
                        _rewardProductionSpeedText.text = string.Format("{0}%",
                            Mathf.RoundToInt(fasterProductionSpeedPercentage));
                    }
                }

                // если после лвлапа будет увеличен максимум слотов
                if (nextLevelProduction.MaximalSlotsCount > currentLevelProduction.MaximalSlotsCount)
                {
                    var difference = nextLevelProduction.MaximalSlotsCount -
                                     currentLevelProduction.MaximalSlotsCount;

                    if (!_rewardLayout.activeInHierarchy)
                        _rewardLayout.SetActive(true);

                    _rewardSlot.gameObject.SetActive(true);
                    _rewardSlotsCountText.SetLocalizedText(string.Format(
                            LocalizationMLS.Instance.GetText(LocalizationKeyConstants.UPGRADE_REWARD_NEW_SLOT_LABEL),
                            difference));
                }
            }

            #endregion

            #region City Hall Rewards

            var currenLevelCityHall = currentBuildingModel as ICityHall;
            if (currenLevelCityHall != null && nextLevelModel is ICityHall)
            {
                var nextLevelCityHall = (ICityHall) nextLevelModel;

                if (!_rewardLayout.activeInHierarchy)
                    _rewardLayout.SetActive(true);

                var currentLimit = currenLevelCityHall.LimitOfPopulation;
                var nextLimit = nextLevelCityHall.LimitOfPopulation;
                var limitPercentIncrease = Mathf.CeilToInt((1f - Mathf.Sqrt(currentLimit * (1f / nextLimit))) * 100f);

                _rewardIncreaseNanoStorageText.text = string.Format("+{0}%", limitPercentIncrease);
                _rewardIncreaseNanoStorage.gameObject.SetActive(true);

                _rewardIncreaseDwellingHousesWithUpgradesText.text = string.Format("+{0}", nextLimit - currentLimit);
                _rewardIncreaseDwellingHousesWithUpgrades.gameObject.SetActive(true);
            }

            #endregion
        }

        #endregion
    }
}