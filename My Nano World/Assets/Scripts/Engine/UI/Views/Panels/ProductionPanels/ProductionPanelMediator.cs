﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.UI.Views.Hud;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Assets.Scripts.Engine.UI.Views.Warehouse;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.api;
using Assets.Scripts.GameLogic.Utilites;
using Assets.Scripts.MultiLanguageSystem.Logic;
using NanoLib.Core.Pool;
using NanoLib.Core.Services.Sound;
using NanoLib.Core.Timers;
using NanoReality.Engine.UI.Extensions;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Controllers.Factories;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Production;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Services;
using NanoReality.GameLogic.Upgrade;
using NanoReality.UI.Components;
using strange.extensions.injector.api;
using UnityEngine;

namespace Engine.UI.Views.Panels.ProductionPanels
{
    public abstract class ProductionPanelMediator<TBuildingType, TView> : BuildingPanelMediator<TBuildingType, TView> 
        where TBuildingType : class, IProductionBuilding  where TView : ProductionPanelView
    {
        protected DraggableProductItem CurrentDraggableProductItem;
        private IProducingItemData _selectedProducingData;
        private List<IMapObject> _productionBuildings;
        private ProduceSlotView _currentPreviewSlot;
        private int _currentMineIndex;
        private bool _isInitialized;

        [Inject] public IInjectionBinder InjectionBinder { get; set; }
        [Inject] public IProductsData jProductsData { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IUnityPool<ProduceSlotView> jProduceSlotsPool { get; set; }
        [Inject] public ISkipIntervalsData jSkipIntervalsData { get; set; }
        [Inject] public ApplyFilterToReadyProducts jApplyFilterToReadyProducts { get; set; }
        [Inject] public StartSpentProductsAnimationSignal jStartSpentProductsAnimationSignal { get; set; }
        [Inject] public AddRewardItemSignal jAddRewardItemSignal { get; set; }
        [Inject] public IIconProvider jIconProvider { get; set; }
        [Inject] public ServerTimeTickSignal jServerTimeTickSignal { get; set; }
        [Inject] public OnHudBottomPanelIsVisibleSignal jOnHudBottomPanelIsVisibleSignal { get; set; }
        [Inject] public OnHudButtonsIsClickableSignal jOnHudButtonsIsClickableSignal { get; set; }
        
        private IList<IProducingItemData> UnlockAvailableProducts => CurrentModel.AvailableProducingGoods.FindAll(p =>
            jProductsData.GetProduct(p.OutcomingProducts).UnlockLevel <= jPlayerProfile.CurrentLevelData.CurrentLevel);
        
        private IList<IProducingItemData> LockAvailableProducts => CurrentModel.AvailableProducingGoods.FindAll(p =>
            jProductsData.GetProduct(p.OutcomingProducts).UnlockLevel > jPlayerProfile.CurrentLevelData.CurrentLevel);
        
        private IList<IProduceSlot> LockedSlots => CurrentModel.CurrentProduceSlots.Where(c => !c.IsUnlocked).ToList();
        
        protected override void Show(object param)
        {
            base.Show(param);
            jSoundManager.Play(SfxSoundTypes.RefineryFactory, SoundChannel.SoundFX);
            Init();
            _productionBuildings = jGameManager.FindAllMapObjectsOnMapByType(CurrentModel.SectorId.ToBusinessSectorType(), CurrentModel.MapObjectType);
            _currentMineIndex = _productionBuildings.IndexOf(CurrentModel);
            
            View.HeaderPanel.Initialize(CurrentModel, null, OnPreviousMinePressed, OnNextMinePressed);
            View.UpgradeButton.Set(CurrentModel, OnUpgradeBuildingClick);

            UpdatePanel();
            jApplyFilterToReadyProducts.Dispatch(CurrentModel.MapID, true);
            SetGrayscaleMode(true);
            
            CurrentModel.OnConfirmFinishProduction += OnConfirmFinishProduction;
            CurrentModel.OnFinishProduction += OnConfirmFinishProduction;
            
            jOnHudBottomPanelIsVisibleSignal.Dispatch(false);
            jOnHudButtonsIsClickableSignal.Dispatch(false);
        }

        protected override void OnHidden()
        {
            for (int i = 0; i < View.ProduceSlotViews.Count; i++)
            {
                RemoveProductionSlot(View.ProduceSlotViews[i]);
            }
            View.ProduceSlotViews.Clear();
            jSoundManager.Play(SfxSoundTypes.MetalMine, SoundChannel.SoundFX);
            CurrentModel.OnConfirmFinishProduction -= OnConfirmFinishProduction;
            CurrentModel.OnFinishProduction -= OnConfirmFinishProduction;

            jApplyFilterToReadyProducts.Dispatch(CurrentModel.MapID, false);
            SetGrayscaleMode(false);
            
            base.OnHidden();
            jOnHudBottomPanelIsVisibleSignal.Dispatch(true);
            jOnHudButtonsIsClickableSignal.Dispatch(true);
        }

        protected void Init()
        {
            if(_isInitialized)
                return;

            _isInitialized = true;
            
            View.DragReceiver.jSignalOnDragObjectPutted.AddListener(OnCenterProductDropped);
            View.DragReceiver.jSignalOnDragObjectEnter.AddListener(OnCenterProductEnter);
            View.DragReceiver.jSignalOnGameObjectExit.AddListener(OnCenterProductExit);
            
            View.AddNewSlotButton.onClick.AddListener(OnUnlockNewSlotClick);
            View.BuyNewSlotButton.onClick.AddListener(OnUnlockNewSlotClick);

            jProduceSlotsPool.InstanceProvider = new PrefabInstanceProvider(View.ProduceSlotPrefab);
            
            foreach (DraggableProductItem draggableProductItem in View.DraggableProductItems)
            {
                draggableProductItem.DragStarted += OnProductDragStarted;
                draggableProductItem.DragEnded += OnProductDragEnded;
                draggableProductItem.ShowRequirementsPanel += View.ShowRequirementsPanel;
                draggableProductItem.HideRequirementsPanel += View.HideRequirementsPanel;
                draggableProductItem.OnItemClickSignal.AddListener(OnProductClicked);

                InjectionBinder.injector.Inject(draggableProductItem);
            }
        }

        public override void OnRemove()
        {
            base.OnRemove();
            return; // view is deleted before OnRemove call when exiting the app
            
            View.DragReceiver.jSignalOnDragObjectPutted.RemoveListener(OnCenterProductDropped);
            View.DragReceiver.jSignalOnDragObjectEnter.RemoveListener(OnCenterProductEnter);
            View.DragReceiver.jSignalOnGameObjectExit.RemoveListener(OnCenterProductExit);
            
            View.AddNewSlotButton.onClick.RemoveListener(OnUnlockNewSlotClick);
            View.BuyNewSlotButton.onClick.RemoveListener(OnUnlockNewSlotClick);
            
            foreach (DraggableProductItem draggableProductItem in View.DraggableProductItems)
            {
                draggableProductItem.DragStarted -= OnProductDragStarted;
                draggableProductItem.DragEnded -= OnProductDragEnded;
                draggableProductItem.ShowRequirementsPanel -= View.ShowRequirementsPanel;
                draggableProductItem.HideRequirementsPanel -= View.HideRequirementsPanel;
                draggableProductItem.OnItemClickSignal.RemoveListener(OnProductClicked);
            }
        }

        protected abstract bool HasPlayerAllIncomingResources(IProducingItemData selectedProducingData);
        
        protected abstract void OnProduceSlotObjectDropped(ProduceSlotView slotView);

        protected abstract void OnProduceSlotDragEnter(ProduceSlotView obj);
        
        protected abstract void SetProductionSlots();

        protected virtual ProduceSlotView AddProductionSlot()
        {
            ProduceSlotView newSlotItem = jProduceSlotsPool.Pop();

            View.AddProductionSlot(newSlotItem);

            newSlotItem.DragObjectEnter += OnProduceSlotDragEnter;
            newSlotItem.DragObjectPutted += OnProduceSlotObjectDropped;

            newSlotItem.SignalOnSkip.AddListener(OnSkipSlotClicked);
            newSlotItem.SignalOnCollect.AddListener(OnSlotCollected);
            newSlotItem.transform.SetAsLastSibling();

            return newSlotItem;
        }

        protected virtual void RemoveProductionSlot(ProduceSlotView slotView)
        {
            slotView.DragObjectEnter -= OnProduceSlotDragEnter;
            slotView.DragObjectPutted -= OnProduceSlotObjectDropped;

            slotView.SignalOnSkip.RemoveListener(OnSkipSlotClicked);
            slotView.SignalOnCollect.RemoveListener(OnSlotCollected);
            
            jProduceSlotsPool.Push(slotView);
        }

        protected void HideSlotPreview()
        {
            _currentPreviewSlot?.HideProductPreview();
            _currentPreviewSlot = null;
            View.ShowProductionQueueFullMessage(false);
        }

        protected void ShowSlotPreview(IDraggableItem draggableItem)
        {
            if (CurrentModel.HasEmptySlot())
            {
                ProduceSlotView slotView = View.ProduceSlotViews.FirstOrDefault(slot =>
                    slot.CurrentSlotCondition == SlotCondition.Standby
                    && !slot.ProduceSlot.IsWaitingResponse);
                
                if (slotView == null)
                    return;

                HideSlotPreview();
                _currentPreviewSlot = slotView;
                _currentPreviewSlot.ShowProductPreview((draggableItem as DraggableProductItem).ItemImage.sprite);
            }
            else
            {
                View.ShowProductionQueueFullMessage(true);
            }
        }

        protected void TryProduceGood(IProduceSlot slot)
        {
            if (!HasPlayerAllIncomingResources(_selectedProducingData))
            {
                var price = new Price
                {
                    PriceType = PriceType.Resources,
                    ProductsPrice = _selectedProducingData.IncomingProducts
                };

                ShowPopup(new NotEnoughResourcesPopupSettings(price), () =>
                {
                        StartProduceGood();
                });
                return;
            }

            StartProduceGood();

            void StartProduceGood()
            {
                CurrentModel.StartProduceGood(_selectedProducingData, slot);
                AnimateConsumingResources(_selectedProducingData);
                SetProductionSlots();
            }
        }
        
        private void OnNextMinePressed()
        {
            SwitchAmongModels(true);
        }

        private void OnPreviousMinePressed()
        {
            SwitchAmongModels(false);
        }
        
        private void SwitchAmongModels(bool isNextBuilding)
        {
            if (_productionBuildings.Count < 2)
                return;
            
            if (isNextBuilding)
            {
                _currentMineIndex++;
                if (_currentMineIndex >= _productionBuildings.Count)
                    _currentMineIndex = 0;
            }
            else
            {
                _currentMineIndex--;
                if (_currentMineIndex < 0)
                    _currentMineIndex = _productionBuildings.Count - 1;
            }
            Hide();
            
            CurrentModel = (TBuildingType)_productionBuildings[_currentMineIndex];

            Show(CurrentModel);
        }

        private void OnUpgradeBuildingClick()
        {
            if (!CurrentModel.IsProduced())
            {
                ShowWindowSignal.Dispatch(typeof(UpgradeWithProductsPanelView), CurrentModel);
                return;
            }

            void OnUserSkipProductionAccepted(PopupResult r)
            {
                if (r == PopupResult.Ok)
                {
                    SkipAllProduction();
                }
            }

            void OnUpgradeErrorPopupResult(PopupResult r)
            {
                if (r != PopupResult.Ok) 
                    return;
                
                var totalPriceForSkipProduction = CalculateTotalSkipPrice();

                ShowPopup(new PurchaseByPremiumPopupSettings(totalPriceForSkipProduction), OnUserSkipProductionAccepted);
            }

            ShowPopup(new UpgradeErrorPopupSettings(), OnUpgradeErrorPopupResult);
        }
        
        private int CalculateTotalSkipPrice()
        {
            int totalPriceForSkipProduction = 0;
            foreach (var slot in CurrentModel.CurrentProduceSlots)
            {
                totalPriceForSkipProduction += jSkipIntervalsData.GetCurrencyForTime((int)slot.CurrentProducingTime);
            }

            return totalPriceForSkipProduction;
        }

        private void SkipAllProduction()
        {
            foreach (IProduceSlot slot in CurrentModel.CurrentProduceSlots)
            {
                if (slot.IsFinishedProduce)
                    continue;

                if (jPlayerProfile.PlayerResources.HardCurrency < jSkipIntervalsData.GetCurrencyForTime((int) slot.CurrentProducingTime))
                    return;

                CurrentModel.SkipSlot(slot, null); // todo add post actions
            }
        }
        
        private void OnUnlockNewSlotClick()
        {
            if (CurrentModel.AvailableSlotsCount >= CurrentModel.MaximalSlotsCount)
            {
                return;
            }
            
            if (!LockedSlots.Any()) return;
            
            IProduceSlot produceSlot = LockedSlots.First();
            int unlockPrice = produceSlot.UnlockPremiumPrice;
            int playerHardCurrency = jPlayerProfile.PlayerResources.HardCurrency;

            if (unlockPrice > playerHardCurrency)
            {
                var moneyPopupSettings = new NotEnoughMoneyPopupSettings(PriceType.Hard, 
                    unlockPrice - playerHardCurrency);
                ShowPopup(moneyPopupSettings);
                return;
            }

            if (unlockPrice == 0)
            {
                CurrentModel.UnlockSlot(produceSlot);
                UpdatePanel();
                return;
            }

            var popupSettings = new PurchaseByPremiumPopupSettings(unlockPrice, produceSlot.OnPayedOperationNotActual);
            ShowPopup(popupSettings, () =>
            {
                CurrentModel.UnlockSlot(produceSlot);
                UpdatePanel();
             });
        }
        
        private void SetAvailableProducts()
        {
            IList<IProducingItemData> unlockedProducts = UnlockAvailableProducts;
            int dragItemCounter = 0;

            List<DraggableProductItem> draggableProductViews = View.DraggableProductItems;
            foreach (IProducingItemData product in unlockedProducts)
            {
                draggableProductViews[dragItemCounter].SetProduct(product);
                dragItemCounter++;
            }

            // Show for user first locked product
            IProducingItemData firstLockedProduct = LockAvailableProducts.FirstOrDefault();
            if (firstLockedProduct != null)
            {
                draggableProductViews[dragItemCounter].SetProduct(firstLockedProduct, false);
                dragItemCounter++;
            }

            for (; dragItemCounter < draggableProductViews.Count; dragItemCounter++)
            {
                draggableProductViews[dragItemCounter].SetEmpty();
            }
            
            View.RequirementGoToProduct();
        }

        private void SetNewButtonSlot()
        {
            int newPrice = CurrentModel.CalculateSlotUnlockPrice();
            bool showAddNewSlotButton = newPrice > -1 ;

            View.AddNewSlotButton.gameObject.SetActive(showAddNewSlotButton);
            View.AddNewSlotButton.transform.SetAsLastSibling();
            View.SkipSlotPrice.SetLocalizedText(!jHardTutorial.IsTutorialCompleted || newPrice == 0
                ? LocalizationMLS.Instance.GetText(LocalizationKeyConstants.FREE_LABEL)
                : newPrice.ToString());
        }

        private void OnSlotCollected(ProduceSlotView slotView)
        {
            IProduceSlot produceSlot = slotView.ProduceSlot;
            IProducingItemData productToCollectData = produceSlot.ItemDataIsProducing;
            if (!jPlayerProfile.PlayerResources.IsCanPlaceToWarehouse(productToCollectData.Amount))
            {
                ShowPopup(new NotEnoughSpacePopupSettings(), () =>
                {
                    ShowWindowSignal.Dispatch(typeof(WarehouseView), null);
                });

                slotView.CollectButton.interactable = true;
                return;
            }

            Sprite productSprite = jIconProvider.GetProductSprite(produceSlot.ItemDataIsProducing.OutcomingProducts);
            Vector3 rewardStartPosition = jGameCamera.ScreenToWorldPoint(slotView.transform.position);

            
            CurrentModel.ShipProduct(produceSlot, null); // todo add update
            
            jAddRewardItemSignal.Dispatch(new AddItemSettings(FlyDestinationType.Warehouse, rewardStartPosition,
                productToCollectData.Amount, productSprite));
            UpdatePanel();
        }

        private void OnSkipSlotClicked(ProduceSlotView slotView)
        {
            IProduceSlot targetSlot = slotView.ProduceSlot;
            if (!jPlayerProfile.PlayerResources.IsCanPlaceToWarehouse(targetSlot.ItemDataIsProducing.Amount))
            {
                ShowPopup(new NotEnoughSpacePopupSettings(), () =>
                {
                    ShowWindowSignal.Dispatch(typeof(WarehouseView), null);
                });
                slotView.CollectButton.interactable = true;
                return;
            }

            int SkipPrice()
            {
                return jHardTutorial.IsTutorialCompleted
                    ? jSkipIntervalsData.GetCurrencyForTime((int) targetSlot.CurrentProducingTime) : 0;
            }

            int skipPrice = SkipPrice();

            if (skipPrice <= 0)
            {
                SkipSlot(slotView, skipPrice);
                return;
            }

            if (jPlayerProfile.PlayerResources.HardCurrency < skipPrice)
            {
                ShowPopup(new NotEnoughMoneyPopupSettings(PriceType.Hard, 
                    skipPrice - jPlayerProfile.PlayerResources.HardCurrency));
                return;
            }

            var popupSettings = new PurchaseByPremiumPopupSettings(skipPrice);
            
            void UpdatePrice(long _)
            {
                popupSettings?.InvokePriceChangeAction(SkipPrice());
            }
                       
            jServerTimeTickSignal.AddListener(UpdatePrice);
            
            ShowPopup(popupSettings, result =>
            {
                jServerTimeTickSignal.RemoveListener(UpdatePrice);
                                
                if (result == PopupResult.Ok)
                {
                    SkipSlot(slotView, SkipPrice());
                }
            });
        }

        private void SkipSlot(ProduceSlotView slotView, int skipPrice)
        {
            IProduceSlot produceSlot = slotView.ProduceSlot;
            IProducingItemData productToCollectData = produceSlot.ItemDataIsProducing;
            Sprite productSprite = jIconProvider.GetProductSprite(produceSlot.ItemDataIsProducing.OutcomingProducts);
            Vector3 rewardStartPosition = jGameCamera.ScreenToWorldPoint(slotView.transform.position);
            
            jAddRewardItemSignal.Dispatch(new AddItemSettings(FlyDestinationType.Warehouse, rewardStartPosition,
                productToCollectData.Amount, productSprite));
            CurrentModel.SkipSlot(slotView.ProduceSlot, null);
            UpdatePanel();
        }

        private void OnProductDragStarted(DraggableItem draggableItem)
        {
            View.SetDragReceiverActive(true);
            CurrentDraggableProductItem = (DraggableProductItem)draggableItem;
            _selectedProducingData = CurrentDraggableProductItem.ProducingItemData;
        }

        private void OnProductDragEnded(DraggableItem obj)
        {
            View.SetDragReceiverActive(false);
        }

        private void OnProductClicked(DraggableItem item) => jSoundManager.Play(SfxSoundTypes.ProductionStart, SoundChannel.SoundFX);

        private void OnCenterProductExit(IDraggableItem draggableItem, DragReceiver dragReceiver)
        {
            HideSlotPreview();
        }

        private void OnCenterProductEnter(IDraggableItem draggableItem, DragReceiver dragReceiver)
        {
            ShowSlotPreview(draggableItem);
        }

        private void OnCenterProductDropped(IDraggableItem draggableItem, DragReceiver dragReceiver)
        {
            HideSlotPreview();

            if (CurrentModel.HasEmptySlot())
            {
                IProduceSlot firstIdleSlot = CurrentModel.CurrentProduceSlots.
                    First(s => s.ItemDataIsProducing == null);;
                TryProduceGood(firstIdleSlot);
            }
        }
        
        private void OnConfirmFinishProduction(IProduceSlot slot, IProducingItemData producingItemData)
        {
            UpdatePanel();
        }
        
        private void UpdatePanel()// todo make this as post process action
        {
            SetAvailableProducts();
            SetProductionSlots();
           View.SetAvailableProductsBg(UnlockAvailableProducts.Count);
            SetNewButtonSlot();
        }

        private void AnimateConsumingResources(IProducingItemData producingItemData)// todo invoke form building view code
        {
            MapObjectView view = jGameManager.GetMapView(CurrentModel.SectorId).GetMapObjectById(CurrentModel.MapID);
            jStartSpentProductsAnimationSignal.Dispatch(jGameCamera.WorldToScreenPoint(view.transform.position),
                producingItemData.IncomingProducts);
        }
    }
}
