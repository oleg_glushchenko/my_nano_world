﻿using Engine.UI.Views.Panels.ProductionPanels;

namespace NanoReality.Engine.UI.Views.Panels
{
    public class FactoryPanelView : ProductionPanelView
    {
        [Inject] public SignalOnNeedToHideTooltip jSignalOnNeedToHideTooltip { get; set; }

        public override void Hide()
        {
            base.Hide();
            jSignalOnNeedToHideTooltip.Dispatch();
        }
    }
}