﻿using Assets.NanoLib.UI.Core.Components;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.UI.Views.Trade.Items;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.api;
using Assets.Scripts.MultiLanguageSystem.Logic;
using NanoReality.Core.Resources;
using NanoReality.Engine.UI.Components;
using NanoReality.Game.Production;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Services;
using NanoReality.GameLogic.Utilites;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;
using UnityEngine.UI.Extensions;

namespace NanoReality.Engine.UI.Views.Panels
{
    public class RequirmentPanel : UIPanelView
    {
        #region InspectorFields
        /// <summary>
        /// Контейнер итемов
        /// </summary>
        [SerializeField]
        private ItemContainer _requirmentsContainer;

        /// <summary>
        /// префаб итема для контейнера
        /// </summary>
        [SerializeField]
        private ProductCountCard _itemPrefab;

        /// <summary>
        /// Время производства продукта
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI _productionTimeText;

        /// <summary>
        /// Уровень анлока продукта
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI _unlockLevelText;

        /// <summary>
        /// Имя продукта
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI _productNameText;

        /// <summary>
        /// Контейнер для времени производства
        /// </summary>
        [SerializeField]
        private GameObject _productionTimeElements;

        /// <summary>
        /// Контейнер для уровня анлока
        /// </summary>
        [SerializeField]
        private GameObject _unlockLevelElements;

        /// <summary>
        /// Контейнер требований (анлока, времени производства)
        /// </summary>
        [SerializeField]
        private GameObject _requirementsProductsPlane;

        /// <summary>
        /// Угол слева
        /// </summary>
        [SerializeField]
        private GameObject _leftCorner;

        /// <summary>
        /// Угол справа
        /// </summary>
        [SerializeField]
        private GameObject _rightCorner;

        /// <summary>
        /// Угол слева
        /// </summary>
        [SerializeField]
        private GameObject _topLeftCorner;

        /// <summary>
        /// Угол справа
        /// </summary>
        [SerializeField]
        private GameObject _topRightCorner;

        /// <summary>
        /// Сколько товара на складе
        /// </summary>
        [SerializeField]
        private TextGameObject _warehouseCounter;
        
        [SerializeField] private float _maxPositionY;

        [SerializeField]
        private float _minXPosition;

        [SerializeField]
        private float _maxXPosition;

        [SerializeField]
        private float _minPanelSize;

        [SerializeField]
        private float _borders;

        [SerializeField]
        private FlowLayoutGroup _flowLayout;
        
        private LayoutElement _prefabLayout;
        private LayoutElement _panelLayout;
        
        #endregion

        [Inject] public IResourcesManager jResourcesManager { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IProductsData jProductsData { get; set; }
        [Inject] public IIconProvider jIconProvider { get; set; }
        [Inject] public IProductionService jProductionService { get; set; }

        private GameObject _productPlane;
        private float _defaultHeight;
        private bool _isNeedToShowProducts;

        /// <summary>
        /// Половина нижнего (верхнего) уголка у выноски необходима для определения точной позиции требований
        /// </summary>
        public RectTransform CornerTransform;
        
        public bool IsNeedToShowProducts
        {
            get => _isNeedToShowProducts;
            // TODO: добавить расширение плашки
            private set => _isNeedToShowProducts = value;
        }

        protected override void Awake()
        {
            base.Awake();
            _prefabLayout = _itemPrefab.GetComponent<LayoutElement>();
            _panelLayout = _requirementsProductsPlane.GetComponent<LayoutElement>();
            _defaultHeight = _panelLayout.preferredHeight;

            _requirmentsContainer.InstaniteContainer(_itemPrefab, 0);
        }
        
        public override void Initialize(object parameters)
        {
            base.Initialize(parameters);
            InitRequirements((DraggableProductItem)parameters);
        }
        
        public override void Show()
        {
            Debug.LogError("Show RequirmentPanel");
            base.Show();
            gameObject.SetActive(true);
        }

        public override void Hide()
        {
            base.Hide();
            gameObject.SetActive(false);
        }
        
        private void InitRequirements(DraggableProductItem draggableProductItem)
        {
            IProducingItemData producingItemData = draggableProductItem.ProducingItemData;
            GameObject productPlane = draggableProductItem.BackGroundHighLight;
            bool showRequirementsOnLeft = draggableProductItem.ShowRequirementsOnLeft;

            _requirmentsContainer.ClearCurrentItems();

            Product balanceProduct = jProductsData.GetProduct(producingItemData.OutcomingProducts);

            _productionTimeText.text = UiUtilities.GetFormattedTimeFromSeconds_M_S(balanceProduct.SectorID == 5
                ? producingItemData.ProducingTime
                : jProductionService.GetProductionTimeMultiplier(producingItemData));

            if (_productNameText != null)
                _productNameText.SetLocalizedText(balanceProduct.ProductName.ToUpper());

            _unlockLevelText.text =  string.Format(LocalizationUtils.LocalizeL2("Localization/TOOLTIP_UNLOCK_LABEL"), balanceProduct.UnlockLevel + 1);
            _warehouseCounter.Text.text = jPlayerProfile.PlayerResources.GetProductCount(balanceProduct.ProductTypeID).ToString();

            var thisProductIsUnlocked = balanceProduct.UnlockLevel <= jPlayerProfile.CurrentLevelData.CurrentLevel;
            _productionTimeElements.SetActive(thisProductIsUnlocked);
            _unlockLevelElements.SetActive(!thisProductIsUnlocked);

            if (producingItemData.IncomingProducts == null || producingItemData.IncomingProducts.Count == 0 || !thisProductIsUnlocked)
            {
                IsNeedToShowProducts = false;
                _requirementsProductsPlane.SetActive(false);
            }
            else
            {
                IsNeedToShowProducts = true;
                _requirementsProductsPlane.SetActive(true);

                foreach (var incomingProduct in producingItemData.IncomingProducts)
                {
                    var newItem = _requirmentsContainer.AddItem() as ProductCountCard;
                    if (newItem == null)
                    {
                        Debug.LogError(
                            "Не удалось проинстанировать ProductCountCard объект, возможно он не назначен панели в инспекторе");
                        return;
                    }
                    var productData = jProductsData.GetProduct(incomingProduct.Key);

                    ProductCountCardInitData productInitData = new ProductCountCardInitData()
                    {
                        ProductObject = productData,
                        ProductSprite = jResourcesManager.GetProductSprite(incomingProduct.Key.Value),
                        SpecialOrderSprite = jIconProvider.GetSpecialOrderIcon(productData.SpecialOrder),
                        ShowOnlySimpleTools = false,//true
                        ProductsCount = null,
                        CurrentProductsCount = jPlayerProfile.PlayerResources.GetProductCount(incomingProduct.Key),
                        NeedProductsCount = incomingProduct.Value,
                        IsCountNeed = true,//true
                        Type = TooltipProductType.Product
                    };

                    newItem.Initialize(productInitData);
                }
            }

            _productPlane = productPlane;

            InitLayout(showRequirementsOnLeft);
        }

        private void InitLayout(bool showRequirementsOnLeft)
        {
            TrimPanelSize();
            SetOffsets(showRequirementsOnLeft);
        }

        private void TrimPanelSize()
        { 
            if (_requirmentsContainer.CurrentItems.Count > 5)
                _panelLayout.preferredHeight = _defaultHeight + _prefabLayout.preferredHeight + _flowLayout.SpacingY;
            else
                _panelLayout.preferredHeight = _defaultHeight + _flowLayout.SpacingY;

            var rectTransform = (RectTransform) transform;
            var sizeDelta = rectTransform.sizeDelta;

            if (_requirmentsContainer.CurrentItems.Count > 5)
            {
                sizeDelta.x =
                    (_prefabLayout.preferredWidth + _flowLayout.SpacingX)*5
                    + _borders;
            }
            else
            {
                sizeDelta.x =
                    (_prefabLayout.preferredWidth + _flowLayout.SpacingX) * _requirmentsContainer.CurrentItems.Count
                    + _borders;
            }

            if (sizeDelta.x < _minPanelSize)
                sizeDelta.x = _minPanelSize;

            rectTransform.sizeDelta = sizeDelta;
        }

        private void SetOffsets(bool isLeft)
        {
            var newPosition = transform.position;
            newPosition.x = _productPlane.transform.position.x;
            newPosition.y = _productPlane.transform.position.y;
            transform.position = newPosition;

            var rectTransform = (RectTransform) transform;
            var anchoredPosition = rectTransform.anchoredPosition;

            var panelOffset = rectTransform.sizeDelta.x * 0.5f +
                              ((RectTransform) CornerTransform.transform).sizeDelta.x * 3;

            if ((anchoredPosition.x + panelOffset + rectTransform.sizeDelta.x * 0.5f) > _maxXPosition)
            {
                isLeft = true;
            }
            else if ((anchoredPosition.x - panelOffset - rectTransform.sizeDelta.x * 0.5f) < _minXPosition)
            {
                isLeft = false;
            }
            
            if (isLeft)
                anchoredPosition.x -= panelOffset;
            else
                anchoredPosition.x += panelOffset;
            
            SetCornersActive(isLeft);

            if (_requirmentsContainer.CurrentItems.Count > 5 && anchoredPosition.y > _maxPositionY)
            {
                anchoredPosition.y = _maxPositionY;

                _leftCorner.SetActive(false);
                _rightCorner.SetActive(false);
            }
            else
            {
                _topLeftCorner.SetActive(false);
                _topRightCorner.SetActive(false);
            }

            rectTransform.anchoredPosition = anchoredPosition;
        }
        
        private void SetCornersActive(bool showRequirementsOnLeft)
        {
            _leftCorner.SetActive(!showRequirementsOnLeft);
            _topLeftCorner.SetActive(!showRequirementsOnLeft);

            _rightCorner.SetActive(showRequirementsOnLeft);
            _topRightCorner.SetActive(showRequirementsOnLeft);
        }
    }
}