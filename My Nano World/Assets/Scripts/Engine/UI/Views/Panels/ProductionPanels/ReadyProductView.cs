﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using DG.Tweening;
using NanoLib.Core;
using NanoReality.Engine.UI.Components;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using NanoReality.StrangeIoC;
using strange.extensions.pool.api;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Views.Panels
{
    public sealed class ReadyProductView : AView, IPoolable, IPointerClickHandler
    {
        [SerializeField] private MapObjectLooker _mapObjectLooker;
        [SerializeField] private Image _itemIconImage;
        [SerializeField] private TextMeshProUGUI _itemsCountText;
        [SerializeField] private float _flyAnimationDuration;
        [SerializeField] private float _itemFlyOffsetY;
        
        private Tween _moveTweener;
        private MapObjectView _targetView;
        private readonly HashSet<IProduceSlot> _produceSlots = new HashSet<IProduceSlot>();
        private IntValue _productOutComeId;

        public event Action<IntValue, IProduceSlot, MapObjectView> OnProduceSlotClick;
        public event Action<IntValue> OnBecameEmpty;

        public void OnPointerClick(PointerEventData eventData)
        {
            var produceSlot = _produceSlots.OrderBy(c => c.CurrentProducingTime).FirstOrDefault();

            if (produceSlot != null)
            {
                OnProduceSlotClick?.Invoke(_productOutComeId, produceSlot, _targetView);
            }
        }

        public void SwitchVisibleState(bool isVisible)
        {
            _itemIconImage.gameObject.SetActive(isVisible);
        }
        
        public void AddProduceSlot(IProduceSlot produceSlot)
        {
            _produceSlots.Add(produceSlot);
            SetCount(true);
        }
        
        public void AddFirstProduceSlot(MapObjectView targetView, IProduceSlot produceSlot, Vector3 position)
        {
            _productOutComeId = produceSlot.ItemDataIsProducing.OutcomingProducts;
            _targetView = targetView;
            
            _mapObjectLooker.SetTarget(_targetView);
            _mapObjectLooker.Pivot = MapObjectLooker.PivotModes.Random;

            _produceSlots.Add(produceSlot);

            SetCount(false);

            SetPosition(position);
        }

        public void PlayAnimation()
        {
            var iconRectTransform = _itemIconImage.rectTransform;
            var anchoredPosition = iconRectTransform.anchoredPosition;
            var startPosition = anchoredPosition;
            var moveToPositionY = anchoredPosition.y + _itemFlyOffsetY;

            _moveTweener = iconRectTransform
                .DOAnchorPosY(moveToPositionY, _flyAnimationDuration)
                .SetLoops(-1, LoopType.Yoyo)
                .OnKill(() => { iconRectTransform.anchoredPosition = startPosition; })
                .Play();
        }

        public void Remove(IProduceSlot produceSlot)
        {
            _produceSlots.Remove(produceSlot);

            if (_produceSlots.Count == 0)
            {
                OnBecameEmpty?.Invoke(_productOutComeId);
            }
            else
            {
                SetCount(_produceSlots.Count > 1);
            }
        }

        public void SetTexture(Sprite sprite)
        {
            _itemIconImage.sprite = sprite;
        }

        private void SetCount(bool needShowCount)
        {
            _itemsCountText.text = $"x{_produceSlots.Count.ToString()}";
            _itemsCountText.gameObject.SetActive(needShowCount);
        }

        private void SetPosition(Vector3 position)
        {
            transform.position = position;
            _mapObjectLooker.RandomedOffset = position;
        }

        #region IPoolable

        public bool retain { get; private set; }

        public void Restore()
        {
            _produceSlots.Clear();
            if (_moveTweener != null)
            {
                _moveTweener.Kill(true);
                _moveTweener = null;
            }
        }

        public void Retain()
        {
            gameObject.SetActive(true);

            Show();
        }

        public void Release()
        {
            gameObject.SetActive(false);
            _mapObjectLooker.SetTarget(null);

            _moveTweener?.Kill();

            Hide();
        }

        #endregion
    }
}