﻿using System.Collections.Generic;
using System.Linq;
using Engine.UI.Views.Panels.ProductionPanels;
using NanoLib.Core.Logging;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;

namespace NanoReality.Engine.UI.Views.Panels
{
    public class FactoryPanelMediator : ProductionPanelMediator<IFactoryObject, FactoryPanelView>
    {
        protected IList<IProduceSlot> UnlockedSlots => CurrentModel.CurrentProduceSlots.Where(c => c.IsUnlocked)
            .OrderBy(c => c.ItemDataIsProducing == null)
            .ThenBy(c => !c.IsFinishedProduce).ThenBy(c => c.CurrentProducingTime).ToList();

        protected override bool HasPlayerAllIncomingResources(IProducingItemData selectedProducingData)
        {
            foreach (var product in selectedProducingData.IncomingProducts)
            {
                if (jPlayerProfile.PlayerResources.GetProductCount(product.Key) < product.Value)
                {
                    return false;
                }
            }

            return true;
        }

        protected override void OnProduceSlotDragEnter(ProduceSlotView slotView)
        {
            ShowSlotPreview(CurrentDraggableProductItem);
        }

        protected override ProduceSlotView AddProductionSlot()
        {
            ProduceSlotView slotView = base.AddProductionSlot();
            slotView.DragObjectExit += DragObjectExit;
            return slotView;
        }

        protected override void RemoveProductionSlot(ProduceSlotView slotView)
        {
            slotView.DragObjectExit -= DragObjectExit;
            base.RemoveProductionSlot(slotView);
        }
        
        protected override void SetProductionSlots()
        {
            for (int i = 0; i < UnlockedSlots.Count; i++)
            {
                ProduceSlotView slotView;
                if (View.ProduceSlotViews.Count > i)
                {
                    slotView = View.ProduceSlotViews[i];
                }
                else
                {
                    slotView = AddProductionSlot();
                }

                IProduceSlot produceSlot = UnlockedSlots[i];
                
                if (produceSlot.IsFinishedProduce)
                {
                    slotView.IsFirstInSlotQueue = true;
                }
                else if (i > 0 && UnlockedSlots[i-1].IsFinishedProduce && produceSlot.CurrentProducingTime > 0)
                {
                    slotView.IsFirstInSlotQueue = true;
                }
                else
                if(i == 0 && produceSlot.CurrentProducingTime > 0)
                {
                    slotView.IsFirstInSlotQueue = true;
                }
                else
                {
                    slotView.IsFirstInSlotQueue = false;
                }

                slotView.SetSlot(produceSlot);
            }
        }

        protected override void OnProduceSlotObjectDropped(ProduceSlotView slotView)
        {
            if (CurrentModel.HasEmptySlot())
            {
                IProduceSlot firstIdleSlot = CurrentModel.CurrentProduceSlots.
                    First(s => s.ItemDataIsProducing == null);;
                
                TryProduceGood(firstIdleSlot);
            }
        }

        private void DragObjectExit(ProduceSlotView obj)
        {
            HideSlotPreview();
        }
    }
}
