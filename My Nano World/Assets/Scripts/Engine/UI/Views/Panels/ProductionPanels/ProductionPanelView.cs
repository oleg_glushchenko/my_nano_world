﻿using System.Collections.Generic;
using System.Linq;
using NanoReality.Engine.UI.UIAnimations;
using NanoReality.Engine.UI.Views;
using NanoReality.Engine.UI.Views.Panels;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Engine.UI.Views.Panels.ProductionPanels
{
    public abstract class ProductionPanelView : BuildingPanelView
    {
        [SerializeField] private DragReceiver _dragReceiver;
        [SerializeField] private Image _draggableFieldImage;
        
        [SerializeField] private List<DraggableProductItem> _draggableProductItems;
        [SerializeField] private RequirementPanelOptimized _requirementPanelOptimized;

        [SerializeField] private GameObject _productionQueueFullMessage;
        [SerializeField] private GameObject _rightArrow;
        [SerializeField] private ProduceSlotView _produceSlotPrefab;
        [SerializeField] private Transform _slotViewsParent;
        [SerializeField] private Button _buyNewSlotButton;
        [SerializeField] private Button _addNewSlotButton;
        [SerializeField] private BuildingUpgradeButtonView _upgradeButton;
        [SerializeField] private BuildingNameInfoPanelView _headerPanel;
        [SerializeField] private TextMeshProUGUI _skipSlotPrice;
        private int _productId;
        
        private readonly List<ProduceSlotView> _produceSlotViews = new List<ProduceSlotView>();
        
        public DragReceiver DragReceiver => _dragReceiver;
        public TextMeshProUGUI SkipSlotPrice => _skipSlotPrice;
        public List<DraggableProductItem> DraggableProductItems => _draggableProductItems;
        public List<ProduceSlotView> ProduceSlotViews => _produceSlotViews;
        public ProduceSlotView ProduceSlotPrefab => _produceSlotPrefab;
        public Button AddNewSlotButton => _addNewSlotButton;
        public Button BuyNewSlotButton => _buyNewSlotButton;
        public BuildingUpgradeButtonView UpgradeButton => _upgradeButton;
        public BuildingNameInfoPanelView HeaderPanel => _headerPanel;
        
        public ProduceSlotView FirstSlotView => _produceSlotViews
            .Where((x) => x.ProduceSlot.ItemDataIsProducing != null && !x.ProduceSlot.IsFinishedProduce)
            .OrderBy(x => x.ProduceSlot.CurrentProducingTime)
            .FirstOrDefault();

        public void SetDragReceiverActive(bool raycastTarget)
        {
            _draggableFieldImage.raycastTarget = raycastTarget;
        }

        public void ShowProductionQueueFullMessage(bool active)
        {
            _productionQueueFullMessage.SetActive(active);
        }

        public void SetAvailableProductsBg(int count)
        {
            _rightArrow.SetActive(count > 3);
        }

        public void AddProductionSlot(ProduceSlotView newSlotItem)
        {
            ProduceSlotViews.Add(newSlotItem);
            newSlotItem.transform.SetParent(_slotViewsParent, false);
            newSlotItem.name = newSlotItem.name + ProduceSlotViews.IndexOf(newSlotItem);
        }

        public void SetRequirementGoToProductId(int productId)
        {
            _productId = productId;
        }
        
        public void RequirementGoToProduct()
        {
            if(_productId == 0) return;
            var requirementProduct = _draggableProductItems.FirstOrDefault(item => item.ProducingItemData?.OutcomingProducts == _productId);
            
            if (requirementProduct == null)
            {
                return;
            }
            requirementProduct.ShowRequirementsWhenItCan();
           
            _productId = 0;
        }
        
        public void ShowRequirementsPanel(DraggableProductItem obj)
        {
            _requirementPanelOptimized.Show(obj);
        }
        
        public void HideRequirementsPanel(DraggableProductItem obj)
        {
            _requirementPanelOptimized.Hide();
        }
    }
}
