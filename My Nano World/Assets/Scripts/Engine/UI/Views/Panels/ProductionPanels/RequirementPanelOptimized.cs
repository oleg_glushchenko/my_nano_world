using NanoReality.Engine.UI.Views.Panels;
using UnityEngine;

namespace Engine.UI.Views.Panels.ProductionPanels
{
    [RequireComponent(typeof(Canvas))]
    public class RequirementPanelOptimized : MonoBehaviour
    {
        [SerializeField] private RequirmentPanel _requirmentPanel;
        [SerializeField] private Canvas _canvas;

        public void Show(object data)
        {
            _requirmentPanel.Initialize(data);
            _canvas.enabled = true;
        }

        public void Hide()
        {
            _canvas.enabled = false;
        }
    }
}