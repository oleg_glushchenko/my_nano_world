﻿using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using UnityEngine;

namespace NanoReality.Engine.UI.Views.Panels
{
    public sealed class ReadyProductItemModel
    {
        public HashSet<IProduceSlot> ItemSlots;
        public MapObjectView BuildingView;
        public Id<Product> ProductType;
        public Vector3 Position;

        public ReadyProductItemModel()
        {
            ItemSlots = new HashSet<IProduceSlot>();
        }

        public void Clear()
        {
            ItemSlots = new HashSet<IProduceSlot>();
            BuildingView = null;
            ProductType = null;
        }
    }
}