﻿using Assets.NanoLib.UI.Core;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using NanoReality.Engine.UI.Extensions.DailyBonusPanel;
using NanoReality.Game.Services.OrderDesk.SeaportOrderDesk;
using NanoReality.GameLogic.Orders;
using NanoReality.GameLogic.Quests;
using NanoReality.UI.Components;
using static Assets.Scripts.GameLogic.Utilites.LocalizationKeyConstants;

public class FxPopupPanelMediator : UIMediator<FxPopUpPanelView>
{
    [Inject] public SignalOnQuestAchive jSignalOnQuestAchive { get; set; }
    [Inject] public SignalSeaportOrderSent jSignalSeaportOrderSent { get; set; }
    [Inject] public SignalOnUserAchievementUnlocked jSignalOnUserAchievementUnlocked { get; set; }
    [Inject] public  SignalWarehouseCapacityUpgrade jSignalWarehouseCapacityUpgrade { get; set; }
    [Inject] public ShowWindowSignal jShowWindowSignal { get; set; }
    [Inject] public SignalOnShownPanel jSignalOnShownPanel { get; set; }
    [Inject] public DailyBonusContext jDailyBonusContext { get; set; }

    private string GetText(string param) => LocalizationMLS.Instance.GetText(param);
    
    private const float DefaultAnimationTime = 2f;

    public override void OnRegister()
    {
        base.OnRegister();
        jSignalOnQuestAchive.AddListener(ShowQuestFx);
        jSignalSeaportOrderSent.AddListener(OnSeaportOrderSent);
        jSignalOnUserAchievementUnlocked.AddListener(ShowAchievementFx);
        jSignalWarehouseCapacityUpgrade.AddListener(OnWarehouseCapacityUpgrade);
    }

    public override void OnRemove()
    {
        base.OnRemove();
        jSignalOnQuestAchive.RemoveListener(ShowQuestFx);
        jSignalSeaportOrderSent.RemoveListener(OnSeaportOrderSent);
        jSignalOnUserAchievementUnlocked.RemoveListener(ShowAchievementFx);
        jSignalWarehouseCapacityUpgrade.RemoveListener(OnWarehouseCapacityUpgrade);
    }

    private void OnWarehouseCapacityUpgrade()
    {
        jShowWindowSignal.Dispatch(typeof(FxPopUpPanelView),
            new FxPopUpPanelView.FxPopupData(GetText(FXPOPUP_MSG_WAREHOUSE_INCREASED), DefaultAnimationTime,
                FxPanelType.Warehouse));
    }
    
    private void OnSeaportOrderSent(ISeaportOrderSlot slot)
    {
        jShowWindowSignal.Dispatch(typeof(FxPopUpPanelView),
            new FxPopUpPanelView.FxPopupData(GetText(FXPOPUP_MSG_ORDERDESK_FINISHED), DefaultAnimationTime,
                FxPanelType.OrderDesk));
    }

    private void ShowAchievementFx()
    {
        jPopupManager.ForceCloseAllActivePopups();
        
        jShowWindowSignal.Dispatch(typeof(FxPopUpPanelView),
            new FxPopUpPanelView.FxPopupData(GetText(FXPOPUP_MSG_ACHIEVEMENT_EARNED), DefaultAnimationTime,
                FxPanelType.Achievement));
    }

    private void ShowQuestFx(IQuest quest)
    {
        var fxPopUpPanelView = new FxPopUpPanelView.FxPopupData(GetText(FXPOPUP_MSG_QUEST_FINISHED),
            DefaultAnimationTime, FxPanelType.Quest);

        if (jDailyBonusContext.ShouldPanelBeShown)
        {
            jSignalOnShownPanel.AddListener(OnShownPanelSignal);

            void OnShownPanelSignal(UIPanelView obj)
            {
                if (!(obj is DailyBonusView)) return;

                jSignalOnShownPanel.RemoveListener(OnShownPanelSignal);
                jShowWindowSignal.Dispatch(typeof(FxPopUpPanelView), fxPopUpPanelView);
            }
        }
        else
        {
            jShowWindowSignal.Dispatch(typeof(FxPopUpPanelView), fxPopUpPanelView);
        }
    }
}