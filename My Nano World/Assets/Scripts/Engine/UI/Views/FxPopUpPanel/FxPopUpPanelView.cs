﻿using UnityEngine;
using System.Collections;
using Assets.NanoLib.UI.Core.Views;
using NanoReality.Engine.UI.UIAnimations;
using strange.extensions.signal.impl;
using Assets.Scripts.MultiLanguageSystem.Logic;
using TMPro;

public enum FxPanelType
{
    Quest,
    OrderDesk,
    Achievement,
    Warehouse
}

public class StartFxPopUpAnimationSignal : Signal<string, float, FxPanelType> { }

public class FxPopUpPanelView : UIPanelView
{
    public struct FxPopupData
    {
        public string PopUpMessage;
        public float AnimationTime;
        public FxPanelType FxType;

        public FxPopupData(string popUpMessage, float animationTime, FxPanelType fxType)
        {
            PopUpMessage = popUpMessage;
            AnimationTime = animationTime;
            FxType = fxType;
        }
    }

    [SerializeField] private OldParticles.UiParticles _starsUiParticles;
    [SerializeField] private GameObjectRotateEffect _godRaysGameObjectRotateEffect;
    [SerializeField] private GameObjectScaleEffect _godRaysScaleEffect;
    [SerializeField] private GameObjectScaleEffect _backgroundScaleEffect;
    [SerializeField] private GameObjectScaleEffect _popUpTextScaleEffect;
    [SerializeField] private FadeInOutScale _popUpTextFadeInOutScale;
    [SerializeField] private TextMeshProUGUI _popUpOutlineText;
    [SerializeField] private TextMeshProUGUI _popUpMessageText;

    private float _defaultAnimationTime = 2f;
    public string PopupMessage { get; private set; }
    public float PopupTimeout { get; private set; }
    public FxPanelType FxType { get; private set; }
    
    public override void Initialize(object parameters)
    {
        base.Initialize(parameters);
        var data = (FxPopupData)parameters;
        PopupMessage = data.PopUpMessage;
        PopupTimeout = data.AnimationTime;
        FxType = data.FxType;
    }

    public override void Show()
    {
        base.Show();
        gameObject.SetActive(true);
        StartCoroutine(StartAnimation(PopupMessage));
        StartCoroutine(DisableAnimation(PopupTimeout));
    }

    private IEnumerator StartAnimation(string message)
    {
        _popUpMessageText.SetLocalizedText(message);
        _popUpOutlineText.SetLocalizedText(message);

        _popUpTextFadeInOutScale.enabled = false;
        _backgroundScaleEffect.Play();
        _popUpTextScaleEffect.Play();
        _godRaysScaleEffect.Play();
        _godRaysGameObjectRotateEffect.Play();

        yield return new WaitForSeconds(0.3f);

        _starsUiParticles.ParticleSystem.Play();
    }

    private IEnumerator DisableAnimation(float time)
    {
        float preEndTime = time - (time*0.1f);
        float endTime = time*0.1f;

        yield return new WaitForSeconds(preEndTime);

        _backgroundScaleEffect.PlayReverse();
        _godRaysScaleEffect.PlayReverse();
        _popUpTextFadeInOutScale.enabled = true;

        yield return new WaitForSeconds(endTime);

        _godRaysGameObjectRotateEffect.Stop();
        _starsUiParticles.ParticleSystem.Stop();

        base.Hide();
    }

    public override void Hide()
    {
        base.Hide();
        gameObject.SetActive(false);
        _godRaysGameObjectRotateEffect.Stop();
        _starsUiParticles.ParticleSystem.Stop();
    }
}