﻿using Assets.Scripts.Engine.UI.Views.Shared;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using UnityEngine;
using UnityEngine.UI;
using System;
using strange.extensions.signal.impl;
using TMPro;
using UnityEngine.EventSystems;

namespace Assets.Scripts.Engine.UI.Views.Warehouse
{
    public class WarehouseRequiredItemCard : ReqiredItemCard, IPointerUpHandler, IPointerClickHandler
    {
        [SerializeField] private GameObject _lessThanRequiredView;
        [SerializeField] private TextMeshProUGUI _costText;
        [SerializeField] private GameObject _equalToRequiredView;
        [SerializeField] private Image _specialOrderImage;
        
        [SerializeField] private Image _backgroundImage;
        [SerializeField] private Image _innerBackgroundImage;
        [SerializeField] private Sprite _normalBackground;
        [SerializeField] private Sprite _enoughBackground;
        [SerializeField] private Sprite _innerNormalBackground;
        [SerializeField] private Sprite _innerEnoughBackground;
        [SerializeField] private Sprite _activeBackground;
        [SerializeField] private Sprite _activeInnerBackground;
        
        private int _countDifference;
        private WarehouseRequiredItemModel _productData;
        public Signal<WarehouseRequiredItemCard> SignalOnPointerClick = new Signal<WarehouseRequiredItemCard>();
        public Signal SignalOnPointerUp = new Signal();
        public event Action<WarehouseRequiredItemCard> OnBuyItemClicked;

        public Product Product => _productData.Product;
        public WarehouseRequiredItemModel Model => _productData;
        public int ProductAmount => _productData.RequiredCount;

        private Action _helpCallback;
        private bool _isEnough;

        public void UpdateProduct(WarehouseRequiredItemModel productData, Action helpCallback)
        {
            var image = productData.ProductSprite;
            var icon = productData.SpecialIcon;
            base.UpdateProduct(productData.ActualCount, productData.RequiredCount, image);
            _productData = productData;
            _helpCallback = helpCallback;
            _isEnough = productData.MissingAmount <= 0;
            
            if (_specialOrderImage != null)
            {
                _specialOrderImage.sprite = icon;
                _specialOrderImage.enabled = icon != null;
            }

            SetItemBackground();
            SetPrice(productData.ItemCost);
        }

        private void SetItemBackground()
        {
            if (_isEnough)
            {
                _lessThanRequiredView.SetActive(false);
                _equalToRequiredView.SetActive(true);
                _backgroundImage.sprite = _enoughBackground;
                _innerBackgroundImage.sprite = _innerEnoughBackground;
            }
            else
            {
                _lessThanRequiredView.SetActive(true);
                _equalToRequiredView.SetActive(false);
                _backgroundImage.sprite = _normalBackground;
                _innerBackgroundImage.sprite = _innerNormalBackground;
            }
        }

        private void SetPrice(int price)
        {
            _costText.enabled = price > 0;
            _costText.text = price.ToString();
        }
        
        public void Select()
        {
            _backgroundImage.sprite = _activeBackground;
            _innerBackgroundImage.sprite = _activeInnerBackground;
        }

        public void Deselect()
        {
            SetItemBackground();
        }

        public void SetEnough()
        {
            _isEnough = true;
            
            _lessThanRequiredView.SetActive(false);
            _equalToRequiredView.SetActive(true);
            _backgroundImage.sprite = _enoughBackground;
            _innerBackgroundImage.sprite = _innerEnoughBackground;
            RequirementsCounterBack.sprite = EqualToRequiredBackgr;
        }

        public void OnBuyButtonClick()
        {
            OnBuyItemClicked?.Invoke(this);
        }

        public void OnAskFriendButtonClick()
        {
            _helpCallback?.Invoke();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            SignalOnPointerClick.Dispatch(this);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            SignalOnPointerUp.Dispatch();
        }

        public void Clear()
        {
            OnBuyItemClicked = delegate { };
        }
    }
    
    public class WarehouseRequiredItemModel
    {
        public int ID;
        public Product Product;
        
        public Sprite ProductSprite;
        public Sprite SpecialIcon;
        public int RequiredCount;
        public int ActualCount;
        public int MissingAmount;
        public int ItemCost;
    }
}