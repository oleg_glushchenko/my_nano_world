using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.GameManager.Models.api;

namespace Engine.UI.Views.Warehouse
{
    public class WarehousePanelContext
    {
        [Inject] public IGameManager jGameManager { get;set; }
        public IMapBuilding Warehouse => jGameManager.FindMapObjectByType<IWarehouseMapObject>(MapObjectintTypes.Warehouse);
    }
}