﻿using System.Linq;
using Assets.NanoLib.UI.Core.Components;
using Assets.Scripts.Engine.UI.Views.Trade.Items;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.UI.Views.Warehouse
{
    public class WarehouseTutorialAnimator : MonoBehaviour
    {
        private const string ProductAnimationTrigger = "productAnimation";

        [SerializeField] private ItemContainer _warehouseResourcesItemContainer;

        [SerializeField] private WarehouseAnimatedItem _animatedItem;

        [SerializeField] private Animator _animator;

        [SerializeField] private WarehouseView _view;

        [SerializeField] private int _targetProductID;        

        private ProductCountCard _item;
        
        private void OnEnable()
        {
            _animatedItem.gameObject.SetActive(false);
            _view.OnShowTutorialAnimation.AddListener(OnShowTutorialAnimation);
        }

        private void OnDisable()
        {
            _view.OnShowTutorialAnimation.RemoveListener(OnShowTutorialAnimation);
        }

        private void OnShowTutorialAnimation()
        {
            _item = FindTargetItem();
            if (_item != null)
            {
                _animatedItem.transform.localPosition = Vector3.zero;
                _animator.SetTrigger(ProductAnimationTrigger);
            }

            _animatedItem.gameObject.SetActive(_item != null);
        }

        public void HideTargetItem()
        {
            if (_item != null)
            {
                var itemCanvasGroup = GetOrCreateComponent<CanvasGroup>(_item);
                itemCanvasGroup.alpha = 0f;
            }
        }

        public void ShowTargetItem()
        {
            if (_item != null)
            {
                var itemCanvasGroup = GetOrCreateComponent<CanvasGroup>(_item);
                itemCanvasGroup.DOFade(1f, 0.2f);
            }
            _item = null;
        }

        public void StarFlying()
        {
            if (_item != null)
            {
                _animatedItem.FlyToTarget(_item.BackgroundImage.transform);
            }
        }        

        private ProductCountCard FindTargetItem()
        {
            return
                _warehouseResourcesItemContainer.CurrentItems.Cast<ProductCountCard>()
                    .FirstOrDefault(item => item.Product.ProductTypeID.Value == _targetProductID);
        }

        private T GetOrCreateComponent<T>(Component item) where T : Component
        {
            var component = item.GetComponent<T>();
            if (component == null)
            {
                component = item.gameObject.AddComponent<T>();
            }
            return component;
        }
    }
}