﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.UI.Core.Components;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.UI.Views.Trade.Items;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Engine.UI.Views.Tooltips;
using DG.Tweening;
using Engine.UI.Views.Warehouse;
using NanoLib.Core.Services.Sound;
using NanoReality.Core.Resources;
using NanoReality.Engine.UI.UIAnimations;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Services;
using NanoReality.Game.Tutorial;
using strange.extensions.signal.impl;
using TMPro;

namespace Assets.Scripts.Engine.UI.Views.Warehouse
{
    public class WarehouseView : UIPanelView
    {
        #region Injects

        [Inject] public IProductsData jProductsData { get; set; }

        [Inject] public virtual IPlayerProfile jPlayerProfile { get; set; }

        [Inject] public SignalOnNeedToShowTooltip jSignalOnNeedToShowTooltip { get; set; }

        [Inject] public SignalOnNeedToHideTooltip jSignalOnNeedToHideTooltip { get; set; }

        [Inject] public IResourcesManager jResourcesManager { get; set; }

        [Inject] public IHardTutorial jHardTutorial { get; set; }

        [Inject] public IIconProvider jIconProvider { get; set; }
        [Inject] public ISoundManager jSoundManager { get; set; }

        #endregion

        #region InspectorFields

        [SerializeField] private WarehouseUpgrade _warehouseUpgrade;

        /// <summary>
        /// Контейнер итемов склада
        /// </summary>
        [SerializeField] private ItemContainer _warehouseResourcesItemContainer;

        /// <summary>
        /// Слайдер заполненности склада
        /// </summary>
        [SerializeField] private HorizontalProgressBar _capacitySlider;

        /// <summary>
        /// Итем склада (продукт)
        /// </summary>
        [SerializeField] private ProductCountCard _resourceItemPrefab;

        /// <summary>
        /// Отображение вместимости в складе (сколько есть/максимум)
        /// </summary>
        [SerializeField] private TextMeshProUGUI _warehouseCapthity;

        /// <summary>
        /// Цвет для отображения текущего количества продуктов в слайдере (меньше 35% заполненности)
        /// </summary>
        [SerializeField] private Color _greenCapathityColor;

        /// <summary>
        /// Цвет для отображения текущего количества продуктов в слайдере (между 35% - 66% заполненности)
        /// </summary>
        [SerializeField] private Color _orangeCapathityColor;

        /// <summary>
        /// Цвет для отображения текущего количества продуктов в слайдере (больше 67% заполненности)
        /// </summary>
        [SerializeField] private Color _redCapathityColor;

        /// <summary>
        /// Максимально число итемов, после которого включаем скроллинг
        /// </summary>
        [SerializeField] private int _maxScrollCapacity;

        /// <summary>
        /// 
        /// </summary>
        [SerializeField] private ScrollRect _warehouseScrollRect;

        [SerializeField] private Button _closeButton;

        #endregion

        private event Action OnShowEffectEnded;

        private Sequence _sequenceAnim;

        public Signal OnShowTutorialAnimation = new Signal();

        public Button UpgradeButton => _warehouseUpgrade.UpgradeButton;
        public GameObject UpgradeButtonTransform => _warehouseUpgrade.UpgradeButtonTransform;
        
        public WarehouseUpgrade WarehouseUpgrade => _warehouseUpgrade;

        public Button CloseButton => _closeButton;
        public override void Initialize(object parameters)
        {
            base.Initialize(parameters);
            UpdatePanel(false);
            switch (parameters)
            {
                case bool warehouse:
                    InitByTask();
                    break;
            }
        }

        protected override void ShowEffectEnded(ISpecialEffect specialEffect)
        {            
            base.ShowEffectEnded(specialEffect);
            jSoundManager.Play(SfxSoundTypes.WarehouseOpen, SoundChannel.SoundFX);

            if (!jHardTutorial.IsTutorialCompleted)
            {
                OnShowTutorialAnimation.Dispatch();
            }
            OnShowEffectEnded?.Invoke();
        }

        protected override void HideEffectEnded(ISpecialEffect specialEffect)
        {
            base.HideEffectEnded(specialEffect);
            ClearInventory();
            _sequenceAnim?.Complete();
            _sequenceAnim?.Kill();
        }

        private void SetCurrentCapacityValueColor()
        {
            if (_capacitySlider.FillAmount <= 0.51f)
            {
				_warehouseCapthity.text = string.Format("<color=#" + ColorUtility.ToHtmlStringRGBA(_greenCapathityColor) + ">" + jPlayerProfile.PlayerResources.ProductsCount + "</color>" + "/" + jPlayerProfile.PlayerResources.WarehouseCapacity);
                return;
            }

            if (_capacitySlider.FillAmount > 0.51f && _capacitySlider.FillAmount <= 0.75f)
            {
				_warehouseCapthity.text = string.Format("<color=#" + ColorUtility.ToHtmlStringRGBA(_orangeCapathityColor) + ">" + jPlayerProfile.PlayerResources.ProductsCount + "</color>" + "/" + jPlayerProfile.PlayerResources.WarehouseCapacity);
                return;
            }

            if (_capacitySlider.FillAmount >= 0.76f)
				_warehouseCapthity.text = string.Format("<color=#" + ColorUtility.ToHtmlStringRGBA(_redCapathityColor) + ">"+ jPlayerProfile.PlayerResources.ProductsCount + "</color>" + "/" + jPlayerProfile.PlayerResources.WarehouseCapacity);
        }

        private float RoundWarehouseValue(float value, int digits)
		{
			double mult = Math.Pow(10.0, digits);
			double result = Math.Truncate( mult * value ) / mult;
			return (float) result;
		}

        private void ClearInventory()
        {
            if (!_warehouseResourcesItemContainer.IsInstantiateed)
            {
                _warehouseResourcesItemContainer.InstaniteContainer(_resourceItemPrefab, 0);
            }

            _warehouseResourcesItemContainer.ClearCurrentItems();
        }

        public void UpdatePanel(bool isHideSubView = true)
        {
			float capacityValue = (float) jPlayerProfile.PlayerResources.ProductsCount/jPlayerProfile.PlayerResources.WarehouseCapacity;
            _capacitySlider.FillAmount = RoundWarehouseValue(capacityValue,2); 


            SetCurrentCapacityValueColor();

            ClearInventory();

            //формируем список продуктов доступных для игрока
            var productsForWarehouse = new Dictionary<Product, int>();
            for(var i = 0; i < jPlayerProfile.PlayerResources.GetProductsTypesCount(); i++)
            {
                var product = jPlayerProfile.PlayerResources.GetProductByIndex(i);
                if(product.Value == 0) continue;

                var productData = jProductsData.GetProduct(product.Key);
                productsForWarehouse.Add(productData, product.Value);
            }

            //сортируем продукты по их количеству
            productsForWarehouse = productsForWarehouse.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);

            //создаем итемы для панели и вешаем слушатели
            foreach(var good in productsForWarehouse)
            {
                var item = (ProductCountCard)_warehouseResourcesItemContainer.AddItem();
                var productData = jProductsData.GetProduct(good.Key.ProductTypeID);

                ProductCountCardInitData productInitData = new ProductCountCardInitData()
                {
                    ProductObject = productData,
                    ProductSprite = jResourcesManager.GetProductSprite(good.Key.ProductTypeID),
                    SpecialOrderSprite = jIconProvider.GetSpecialOrderIcon(productData.SpecialOrder),
                    ShowOnlySimpleTools = false,
                    ProductsCount = good.Value,
                    CurrentProductsCount = null,
                    NeedProductsCount = null,
                    IsCountNeed = true,
                    Type = TooltipProductType.Product
                };

                item.Initialize(productInitData);

                item.SignalOnSelected.RemoveListener(OnItemSelect);
                item.SignalOnSelected.AddListener(OnItemSelect);

                item.SignalOnHideTooltip.RemoveListener(OnItemHide);
                item.SignalOnHideTooltip.AddListener(OnItemHide);
            }

            _warehouseScrollRect.vertical = _warehouseResourcesItemContainer.CurrentItems.Count > _maxScrollCapacity;
        }


        private void OnItemHide()
        {
            jSignalOnNeedToHideTooltip.Dispatch();
        }

        private void OnItemSelect(TooltipData tooltipData)
        {
            switch(tooltipData.Type)
            {
                case TooltipType.ProductItems:
                    tooltipData.OnGoToClickAction = Hide;
                    break;
            }
            jSignalOnNeedToShowTooltip.Dispatch(tooltipData);
        }

        public void AddActionButtonEffect()
        {
            OnShowEffectEnded += () =>
            {
                PlayUpgradeButtonEffect(); 
                OnShowEffectEnded = null;
            };
        }
        private void PlayUpgradeButtonEffect()
        {
            _sequenceAnim = DOTween.Sequence();;

            const float sequenceDuration = 0.5f;
            var targetScale = new Vector2(1.2f, 0.85f);

            var scaleUp = UpgradeButtonTransform.transform.DOScale(targetScale, sequenceDuration).SetEase(Ease.InSine);
            var scaleDown = UpgradeButtonTransform.transform.DOScale(Vector2.one, sequenceDuration).SetEase(Ease.OutBack);
            
            _sequenceAnim.Append(scaleUp).Append(scaleDown).SetDelay(0.5f);
            _sequenceAnim.SetLoops(3, LoopType.Restart);
            
            _sequenceAnim.OnKill(() =>
            {
                _sequenceAnim = null;
            });
            _sequenceAnim.Play();
        }
        
        private void InitByTask()
        {
            AddActionButtonEffect();
        }
    }
}