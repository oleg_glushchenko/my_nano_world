﻿using NanoReality.Engine.UI.UIAnimations;
using UnityEngine;

namespace Assets.Scripts.Engine.UI.Views.Warehouse
{
    public class WarehouseAnimatedItem : MonoBehaviour
    {
        [SerializeField]
        private GameObjectFlyEffect _flyEffect;

        public void FlyToTarget(Transform target)
        {           
            _flyEffect.Destination = target;
            _flyEffect.Play();
        }
    }
}
