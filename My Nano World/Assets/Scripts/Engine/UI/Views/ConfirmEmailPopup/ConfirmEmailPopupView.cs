﻿using Assets.NanoLib.UI.Core.Views;
using Assets.NanoLib.Utilities.DataManager.Models.api;
using strange.extensions.signal.impl;
using UnityEngine;
using UnityEngine.UI;

public class ConfirmEmailPopupView : UIPanelView
{
    [SerializeField]
    protected InputField EmailInput;

    [Inject]
    public ILocalDataManager jLocalDataManager { get; set; }

    /// <summary>
    /// Сообщает о закрытии панели
    /// </summary>
    public Signal<bool, string> OnCloseSignal = new Signal<bool, string>();

    protected override void Start()
    {
        base.Start();
        EmailInput.text = PlayerPrefs.GetString("email");
    }
    
    public virtual void CloseButtonOkCallback()
    {
        PlayerPrefs.SetString("email", EmailInput.text);
        PlayerPrefs.Save();
        OnCloseSignal.Dispatch(true, EmailInput.text);
    }

    public virtual void CloseButtonCancelCallback()
    {
        OnCloseSignal.Dispatch(false, "");
        Hide();
    }
}
