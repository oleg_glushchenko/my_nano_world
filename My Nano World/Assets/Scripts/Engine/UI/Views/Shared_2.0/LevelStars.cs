﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// ReSharper disable once CheckNamespace

namespace NanoReality.Engine.UI.Views.Panels
{
    /// <summary>
    /// Класс списка звёзд, для отображения уровня здания
    /// </summary>
    public class LevelStars : MonoBehaviour
    {
        /// <summary>
        /// Список всех звёзд
        /// </summary>
        public List<Image> Stars;

        /// <summary>
        /// Звезда полученного уровня
        /// </summary>
        public Sprite StarEnabledSprite;

        /// <summary>
        /// Звузда неполученного уровня
        /// </summary>
        public Sprite StarDisabledSprite;

        /// <summary>
        /// Инициализирует звёзды, отбрасывает лишние, которые сверх-максимума, и включает\выключает в соответствии с
        /// уровнем
        /// </summary>
        /// <param name="currentLevel">Текущий уровень</param>
        /// <param name="maxLevel">Максимально возможный уровень</param>
        public void Init(int currentLevel, int maxLevel)
        {
            for (var i = 0; i < Stars.Count; i++)
            {
                Stars[i].gameObject.SetActive(i <= maxLevel);
                Stars[i].sprite = (i <= currentLevel) ? StarEnabledSprite : StarDisabledSprite;
            }
        }
    }
}