﻿using System;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Views.Shared.Filters
{
    [Serializable]
    public class Filter
    {
        public Toggle Toggle;
    }
}