﻿using Assets.NanoLib.Utilities;
using UnityEngine;
using UnityEngine.UI;


namespace NanoReality.Engine.UI.Views.Shared.Filters
{
    public class FilterToggleView : MonoBehaviour
    {
        public Image Icon;
		public GameObject FoneTab;
		public GameObject MainTab;

		public bool IsNeedSHowFone;


        private float Size
        {
            get
            {
                if (_size == 0)
                    _size = CacheIcon.GetCacheComponent<LayoutElement>().preferredWidth;
                return _size;
                
            }
        }

        private float _size;

        public CacheMonobehavior CacheIcon { get { return _cachedIcon ?? (_cachedIcon = new CacheMonobehavior(Icon)); } }
        private CacheMonobehavior _cachedIcon;

        void Awake()
        {
            var toggle = GetComponentInParent<Toggle>();
            if (toggle != null && toggle.isOn)
            {
                SetIconSize(true);
            }
        }

        public void OnStateChanged(bool state)
        {
            SetIconSize(state);
        }

        private void SetIconSize(bool selected)
        {
            if (selected)
            {
                CacheIcon.GetCacheComponent<LayoutElement>().preferredWidth = Size + Size * 0.1f;
                CacheIcon.GetCacheComponent<LayoutElement>().preferredHeight = Size + Size * 0.1f;
            }
            else
            {
                CacheIcon.GetCacheComponent<LayoutElement>().preferredWidth = Size;
                CacheIcon.GetCacheComponent<LayoutElement>().preferredHeight = Size;
            }
			if(IsNeedSHowFone){
				FoneTab.SetActive(!selected);
				MainTab.SetActive(selected);
			}
        }
    }
}