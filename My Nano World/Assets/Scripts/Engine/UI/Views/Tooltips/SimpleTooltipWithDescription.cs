﻿using Assets.Scripts.MultiLanguageSystem.Logic;
using NanoReality.GameLogic.Services;
using UnityEngine;
using TMPro;

namespace Assets.Scripts.Engine.UI.Views.Tooltips
{
    public class SimpleTooltipWithDescription : TooltipView
    {
        [Inject]
        public IProductService jProductService { get; set; }
        
        [SerializeField] 
        private TextMeshProUGUI _productName;

        [SerializeField]
        private TextMeshProUGUI _description;

        protected override void OnShow(TooltipData data)
        {
            var target = data.ItemTransform;
            var corners = new Vector3[4];
            target.GetWorldCorners(corners);
            var sizeY = Mathf.Abs(corners[0].y - corners[1].y);

            _productName.SetLocalizedText(jProductService.GetProductName(data.ProductItem.ProductTypeID));
            _description.SetLocalizedText(jProductService.GetProductDescription(data.ProductItem.ProductTypeID));

            var rt = (RectTransform)transform;
            var newPos = target.position;

            rt.position = newPos;
            newPos = rt.anchoredPosition3D;
            newPos.y += sizeY / 2;
            rt.anchoredPosition3D = newPos;
        }

        protected override void OnHide()
        {
            
        }
    }
}
