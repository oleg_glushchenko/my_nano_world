﻿using System;
using Assets.Scripts.Engine.UI.Views.Tooltips;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.Services;
using NanoReality.Game.Tutorial;
using strange.extensions.signal.impl;
using UnityEngine;
using NanoReality.StrangeIoC;
using strange.extensions.mediation.impl;

public class SignalOnNeedToHideTooltip : Signal { }
public class SignalOnNeedToShowTooltip : Signal<TooltipData> { }
public class SignalOnNeedToDisableTooltip : Signal<bool> { }

namespace Assets.Scripts.Engine.UI.Views.Tooltips
{
    public class TooltipsManager : View
    {
        #region Injects

        [Inject] public SignalOnNeedToHideTooltip jSignalOnNeedToHideTooltip { get; set; }
        [Inject] public SignalOnNeedToShowTooltip jSignalOnNeedToShowTooltip { get; set; }
        [Inject] public SignalOnNeedToDisableTooltip jSignalOnNeedToDisableTooltip { get; set; }

        [Inject] public IProductService jProductService { get; set; }
        [Inject] public IHardTutorial jHardTutorial { get; set; }

        #endregion

        #region InspectorFields

        [SerializeField]
        private SimpleTooltip _simpleTooltip;

        [SerializeField]
        private SpecialItemsTooltip _specialItemsTooltip;

        [SerializeField]
        private SimpleTooltipWithDescription _simpleTooltipWithDescription;

        #endregion

        public event Action<TooltipView> ShowTooltip;
        public event Action<TooltipView> HideTooltip;
        private bool _lockTooltipShow;

        [PostConstruct]
        public void PostConstruct()
        {
            jSignalOnNeedToShowTooltip.AddListener(OnShowTooltip);
            jSignalOnNeedToHideTooltip.AddListener(OnHideTooltip);
            jSignalOnNeedToDisableTooltip.AddListener(ChangeShowEnable);
        }

        [PostConstruct]
        public void Deconstruct()
        {
            jSignalOnNeedToShowTooltip.RemoveListener(OnShowTooltip);
            jSignalOnNeedToHideTooltip.RemoveListener(OnHideTooltip);
            jSignalOnNeedToDisableTooltip.RemoveListener(ChangeShowEnable);
        }

        private void OnShowTooltip(TooltipData tooltipData)
        {
            if (!jHardTutorial.IsTutorialCompleted || _lockTooltipShow)
            {
                tooltipData.OnTooltipHideAction?.Invoke();
                return;
            }

            TooltipView tooltipView;
            
            switch(tooltipData.Type)
            {
                case TooltipType.Simple:
                    tooltipView = _simpleTooltip;
                    break;
                case TooltipType.ProductItems:
                    if (tooltipData.ShowOnlySimpleTool || 
                        !jProductService.CanGoToBuildingByProduct(tooltipData.ProductItem.ProductTypeID))
                    {
                        tooltipView = _simpleTooltipWithDescription;
                    }
                    else
                    {
                        tooltipView = _specialItemsTooltip;
                    }
                    break;
                default:
                {
                    Debug.LogError("Can't show tooltip. " + tooltipData);
                    return;
                }
            }

            tooltipView.Show(tooltipData);
            ShowTooltip?.Invoke(tooltipView);
        }

        private void OnHideTooltip()
        {
            // TODO: need refactor in way, that we hide only tooltips that active, without check on visible.
            if (_simpleTooltip.IsVisible)
            {
                _simpleTooltip.Hide();
                HideTooltip?.Invoke(_simpleTooltip);
            }
            if (_simpleTooltipWithDescription.IsVisible)
            {
                _simpleTooltipWithDescription.Hide();
                HideTooltip?.Invoke(_simpleTooltipWithDescription);
            }
            if (_specialItemsTooltip.IsVisible)
            {
                _specialItemsTooltip.Hide();
                HideTooltip?.Invoke(_specialItemsTooltip);
            }
        }

        private void ChangeShowEnable(bool value)
        {
            _lockTooltipShow = value;
            OnHideTooltip();
        }
    }

    public enum TooltipType
    {
        Simple,
        ProductItems
    }

    public class TooltipData
    {
        /// <summary>
        /// ID итема (для тултипа продуктов)
        /// </summary>
        public Product ProductItem;

        /// <summary>
        /// Кол-во необходимого продукта (для тултипов по спец. продуктам)
        /// </summary>
        public int ProductAmount;

        /// <summary>
        /// Текст тултипа (для simple tooltip)
        /// </summary>
        public string Message;

        /// <summary>
        /// Используется в ивентах. Цена в лантернах для скипа продукта
        /// </summary>
        public int LanternCost;

        /// <summary>
        /// Для определения позиции тултипа
        /// </summary>
        public RectTransform ItemTransform { get; set; }

        /// <summary>
        /// Тип тултипа (e.g., simple, product)
        /// </summary>
        public TooltipType Type;

        /// <summary>
        /// Показивать только простой тултип даже если ето спец айтем
        /// </summary>
        public bool ShowOnlySimpleTool;

        public Action OnGoToClickAction;

        public Action OnBuyForLanterns;

        public Action OnBuyClickAction;

        public Action OnTooltipShowAction;
        
        public Action OnTooltipHideAction;
    }
}
