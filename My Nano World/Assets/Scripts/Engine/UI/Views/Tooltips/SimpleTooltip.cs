﻿using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.MultiLanguageSystem.Logic;
using NanoLib.UI.Core;
using UnityEngine;
using TMPro;

namespace Assets.Scripts.Engine.UI.Views.Tooltips
{
    public class SimpleTooltip : TooltipView
    {
        [Inject] public IUIManager jUiManager { get; set; }
        
        [SerializeField] 
        private TextMeshProUGUI _productName;

        private RectTransform _target;

        protected override void OnShow(TooltipData data)
        {
            _target = data.ItemTransform;
            _productName.SetLocalizedText(data.Message);
            
            UpdatePosition();
        }

        protected override void OnHide()
        {
            UpdatePosition();
        }

        private void LateUpdate()
        {
            UpdatePosition();
        }

        [ContextMenu("UpdatePosition")]
        private void UpdatePosition()
        {
            if (_target == null) return;
            
            var corners = new Vector3[4];
            _target.GetWorldCorners(corners);
            
            var sizeY = Mathf.Abs(corners[0].y - corners[1].y);

            var rt = (RectTransform) transform;
            var newPos = _target.position;

            rt.position = newPos;
            newPos = rt.anchoredPosition3D;
            newPos.y += sizeY/2;
            rt.anchoredPosition3D = newPos;

            rt.GetWorldCorners(corners);
            var canvasCorners = new Vector3[4];
            var overlayCanvasTransform = (RectTransform)jUiManager.OverlayCanvasTransform;
            overlayCanvasTransform.GetWorldCorners(canvasCorners);
            
            Rect canvasRect = new Rect(0f, 0f, canvasCorners[2].x, canvasCorners[2].y);
            var isCornerVisible = new bool[4];

            if (!canvasRect.Contains(rt.position))
            {
                var pos = rt.position;
                rt.position = new Vector3(
                    Mathf.Clamp(pos.x, canvasRect.xMin, canvasRect.xMax), 
                    Mathf.Clamp(pos.y, canvasRect.yMin, canvasRect.yMax), 0f);
                rt.GetWorldCorners(corners);
            }
            
            for (int i = 0; i < corners.Length; i++)
            {
                isCornerVisible[i] = canvasRect.Contains(corners[i]);
            }

            // bottom left and bottom right check
            if (!isCornerVisible[0] && !isCornerVisible[3])
            {
                rt.position += new Vector3(0f, canvasCorners[0].y - corners[0].y, 0f);
            }
            else
            // top left and top right check
            if (!isCornerVisible[1] && !isCornerVisible[2])
            {
                rt.position += new Vector3(0f, canvasCorners[1].y - corners[1].y, 0f);
            }
            
            // bottom left and top left check
            if (!isCornerVisible[0] && !isCornerVisible[1])
            {
                rt.position += new Vector3(canvasCorners[0].x - corners[0].x, 0f, 0f);
            }
            else
            // top right and bottom right check
            if (!isCornerVisible[2] && !isCornerVisible[3])
            {
                rt.position += new Vector3(canvasCorners[2].x - corners[2].x, 0f, 0f);
            }
        }
    }
}
