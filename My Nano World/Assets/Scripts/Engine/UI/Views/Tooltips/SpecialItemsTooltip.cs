﻿using System;
using System.Collections.Generic;
using System.Text;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Assets.Scripts.MultiLanguageSystem.Logic;
using Engine.UI;
using NanoLib.Core.Services.Sound;
using NanoReality.Engine.BuildingSystem.CityCamera;
using NanoReality.Engine.UI;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.OrderDesk;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using NanoReality.GameLogic.Services;
using NanoReality.UI.Components;
using UnityEngine.EventSystems;

namespace Assets.Scripts.Engine.UI.Views.Tooltips
{
    public class SpecialItemsTooltip : TooltipView
    {
        #region InspectorFields

        [SerializeField] private TextMeshProUGUI _tooltipTitle;
        [SerializeField] private TextMeshProUGUI _tooltipDescription;
        [SerializeField] private TextMeshProUGUI _hardCost;
        [SerializeField] private TextMeshProUGUI _lanternCost;

        [SerializeField] private Button _buyButton;
        [SerializeField] private Button _goToButton;
        [SerializeField] private Button _buyForLanternsButton;
        [SerializeField] private EventTrigger _overlayEventTrigger;

        [SerializeField] private GameObject _leftCorner;
        [SerializeField] private GameObject _rightCorner;

        [SerializeField] private TextMeshProUGUI _goToButtonTittle;

        #endregion

        private Price _price;
        private int _playerProductAmount;
        private EventTrigger.Entry _overlayPointerDownTrigger;
        private int _cost;
        private TooltipData _tooltipData;

        public Button GoToButton => _goToButton;
        public Button BuyForLanternsButton => _buyForLanternsButton;
        public EventTrigger OverlayHideTrigger => _overlayEventTrigger;
        
        [Inject] public IPopupManager jPopupManager { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IProductService jProductService { get; set; }
        [Inject] public ISoundManager jSoundManager { get; set; }
        [Inject] public BuyResoursesActionSignal jResoursesBuySignal { get; set; }

        #region Overrides of ATooltipView

        protected override void OnRegister()
        {
            base.OnRegister();
            
            _buyButton.onClick.AddListener(OnBuyButtonClick);
            _goToButton.onClick.AddListener(OnGoToButtonClick);
            _buyForLanternsButton?.onClick.AddListener(OnBuyForLanternsClick);
            
            _overlayPointerDownTrigger = new EventTrigger.Entry();
            _overlayPointerDownTrigger.eventID = EventTriggerType.PointerDown;
            _overlayPointerDownTrigger.callback.AddListener(eventData => { Hide(); });
            _overlayEventTrigger.triggers.Add(_overlayPointerDownTrigger);
        }

        protected override void OnRemove()
        {
            base.OnRemove();
            
            _buyButton.onClick.RemoveListener(OnBuyButtonClick);
            _goToButton.onClick.RemoveListener(OnGoToButtonClick);
            _buyForLanternsButton?.onClick.RemoveListener(OnBuyForLanternsClick);
            _overlayEventTrigger.triggers.Remove(_overlayPointerDownTrigger);
        }

        protected override void OnShow(TooltipData data)
        {
            _tooltipData = data;
            var productId = data.ProductItem.ProductTypeID;
            var amount = data.ProductAmount;

            jSoundManager.Play(SfxSoundTypes.AppWindow, SoundChannel.SoundFX);
            _tooltipTitle.SetLocalizedText(jProductService.GetProductName(productId));
            _hardCost.text = GetProductBuyoutCost().ToString();
            
            _price = new Price
            {
                PriceType = PriceType.Resources,
                ProductsPrice = new Dictionary<Id<Product>, int>
                {
                    { productId, amount }
                }
            };
            var isProductMissing = _playerProductAmount < data.ProductAmount;
            
            _buyButton.gameObject.SetActive(isProductMissing);

            _tooltipDescription.SetLocalizedText(jProductService.GetProductDescription(productId));
            _goToButtonTittle.SetLocalizedText(jProductService.GetProductGoToText(productId));

            _goToButton.gameObject.SetActive(jProductService.CanGoToBuildingByProduct(productId));

            bool enableLanternButton = data.LanternCost > 0;
            if (enableLanternButton)
            {
                _lanternCost.text = data.LanternCost.ToString();
            }

            _buyForLanternsButton?.gameObject.SetActive(enableLanternButton);

            SetTooltipPosition();
        }

        protected override void OnHide()
        {
            jSoundManager.Play(SfxSoundTypes.CloseWindow, SoundChannel.SoundFX);
        }

        #endregion

        private int GetProductBuyoutCost()
        {
            _playerProductAmount = jPlayerProfile.PlayerResources.GetProductCount(Data.ProductItem.ProductTypeID);
            var productDifference = Data.ProductAmount - _playerProductAmount;
            return Data.ProductItem.HardPrice * productDifference;
        }

        private void SetTooltipPosition()
        {
            var rt = (RectTransform)transform;           

            if(Data.ItemTransform.position.x < Screen.width * 0.5f)
            {
                _leftCorner.SetActive(true);
                _rightCorner.SetActive(false);

                rt.pivot = new Vector2(-0.2f, 0.5f);
            }
            else
            {
                _leftCorner.SetActive(false);
                _rightCorner.SetActive(true);

                rt.pivot = new Vector2(1.2f, 0.5f);
            }

            rt.position = Data.ItemTransform.position;
        }

        private void OnBuyButtonClick()
        {
            if (_price == null) return;

            jResoursesBuySignal.Dispatch(_price, result => { });

            Hide();
        }

        private void OnBuyForLanternsClick()
        {
            _tooltipData?.OnBuyForLanterns.Invoke();
            Hide();
        }

        private void OnGoToButtonClick()
        {
            var product = Data.ProductItem;
            var action = Data.OnGoToClickAction;
                
            action?.Invoke();
            Hide();

            if (product != null)
            {
                jProductService.GoToBuildingByProduct(product.ProductTypeID);
            }
        }
    }
}
