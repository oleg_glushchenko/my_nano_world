﻿using NanoReality.StrangeIoC;
using UnityEngine;

namespace Assets.Scripts.Engine.UI.Views.Tooltips
{
    public abstract class TooltipView : AViewMediator
    {
        protected TooltipData Data;
        
        public bool IsVisible { get; private set; }

        public void Show(TooltipData data)
        {
            Data = data;
            IsVisible = true;
            OnShow(Data);
            gameObject.SetActive(true);
            Data.OnTooltipShowAction?.Invoke();
        }

        public void Hide()
        {
            if (!IsVisible)
            {
                Debug.LogWarning("Not expected Hide tooltip call.");
                return;
            }

            IsVisible = false;
            gameObject.SetActive(false);
            Data.OnTooltipHideAction?.Invoke();
            Data = new TooltipData();
        }

        protected override void PreRegister()
        {
        }

        protected override void OnRegister()
        {
        }

        protected override void OnRemove()
        {
        }

        protected abstract void OnShow(TooltipData data);
        protected abstract void OnHide();
    }
}