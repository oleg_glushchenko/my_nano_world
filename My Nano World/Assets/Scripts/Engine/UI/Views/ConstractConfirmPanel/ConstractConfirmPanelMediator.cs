﻿using System.Runtime.InteropServices;
using NanoLib.Services.InputService;
using Assets.NanoLib.UI.Core.Signals;
using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem;
using NanoReality.Core.RoadConstructor;
using Assets.NanoLib.UI.Core;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.UI.Views.Hud;
using NanoReality.Game.UI.BuildingsShopPanel;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.GameManager.Models.api;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using UniRx;
using UnityEngine;

// ReSharper disable UnusedAutoPropertyAccessor.Global
using Assets.Scripts.Engine.BuildingSystem.MapObjects;

namespace NanoReality.Engine.UI.Views.ConstractConfirmPanel
{

    public class SignalOnCancelConstractInView : Signal { }

    public class SignalOnConfirmConstractInView : Signal { }
    
    public class SignalOnConfirmConstructSucceed : Signal { }
    public class SignalOnCancelConstruct : Signal { }

    public class SignalOnCancelConstractWithoutExit : Signal { }

    public class ConstractConfirmPanelMediator : UIMediator<ConstractConfirmPanelView>
    {
        [Inject] public SignalOnReleaseTouch jSignalOnReleaseTouch { get; set; }

        [Inject] public SignalOnCancelConstractWithoutExit jSignalOnCancelConstractWithoutExit { get; set; }

        [Inject] public IConstructionController jConstructionController { get; set; }
        [Inject] public SignalOnConstructControllerChangeState jSignalOnConstructControllerChangeState { get; set; }

        [Inject] public SignalOnCancelConstractInView jSignalOnCancelConstractInView { get; set; }

        [Inject] public SignalOnConfirmConstractInView jSignalOnConfirmConstractInView { get; set; }

        [Inject] public SignalOnConfirmConstructSucceed jSignalOnConfirmConstructSucceed { get; set; }

        [Inject] public BuildingDragBegin jBuildingDragBegin { get; set; }
        [Inject] public SignalOnCancelConstruct jSignalOnCancelConstruct { get; set; }

        [Inject] public IWorldSpaceCanvas jWorldSpaceCanvas { get; set; }
        [Inject] public PlayerOpenedPanelSignal jSignalPlayerOpenedPanel { get; set; }
        [Inject] public TileOfRoadEditedSignal JTileOfRoadEditedSignal { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public RoadConstructor jRoadConstructor { get; set; }
        [Inject] public ConstructConfirmPanelModel Model { get; set; }
        [Inject] public OnHudBottomPanelIsVisibleSignal jOnHudBottomPanelIsVisibleSignal { get; set; }
        [Inject] public OnHudButtonsIsClickableSignal jOnHudButtonsIsClickableSignal { get; set; }

        private bool _isShown;
        
        public override void OnRegister()
        {
            jSignalOnCancelConstractInView.AddListener(OnCancelBuild);
            jSignalOnConfirmConstractInView.AddListener(OnConfirmBuild);
            jSignalOnConstructControllerChangeState.AddListener(OnConstructionControllerStateChangedSignal);
            jSignalPlayerOpenedPanel.AddListener(CloseBuildingMode);
            JTileOfRoadEditedSignal.AddListener(OnTileDrawn);
        }

        public override void OnRemove()
        {
            base.OnRemove();
            jSignalOnCancelConstractInView.RemoveAllListeners();
            jSignalOnConfirmConstractInView.RemoveAllListeners();
            jSignalOnConstructControllerChangeState.RemoveAllListeners();
            jSignalPlayerOpenedPanel.RemoveAllListeners();
            JTileOfRoadEditedSignal.RemoveAllListeners();
        }

        private void CloseBuildingMode()
        {
            if (ViewVisible)
            {
                View.OnCancelClicked();
            }
        }
        
        //TODO rewrite this to correct jShowWindowSignal and remove this signal everywhere
        private void OnConstructionControllerStateChangedSignal(bool isEnabledConstruct)
        {
            if (isEnabledConstruct && !_isShown)
            {
                _isShown = true;
                Show(null);
            }
            else if(!isEnabledConstruct && _isShown)
            {
                _isShown = false;
                Hide();
            }
        }

        protected override void OnShown()
        {
            View.transform.SetParent(((WorldSpaceCanvas)jWorldSpaceCanvas).transform,false);
            
            if (jConstructionController.IsRoadBuildingMode)
                return;

            View.SetTarget(jConstructionController.SelectedObjectView);
            jBuildingDragBegin.AddListener(OnMove);
            jSignalOnReleaseTouch.AddListener(SetPosition);
            
            if (Input.GetMouseButton(0))
            {
                View.gameObject.SetActive(false);
            }
            else
            {
                SetPosition();
            }
            jOnHudBottomPanelIsVisibleSignal.Dispatch(false);
            jOnHudButtonsIsClickableSignal.Dispatch(false);
        }
        
        protected override void OnHidden()
        {
            jBuildingDragBegin.RemoveListener(OnMove);
            jSignalOnReleaseTouch.RemoveListener(SetPosition);
            jOnHudBottomPanelIsVisibleSignal.Dispatch(true);
            jOnHudButtonsIsClickableSignal.Dispatch(true);
            base.OnHidden();
        }

        private void OnTileDrawn()
        {
            // TODO: need correct handle view show in this case.
            if (View == null)
            {
                Show(null);
            }

            JTileOfRoadEditedSignal.RemoveListener(OnTileDrawn);
            View.gameObject.SetActive(false);

            Observable
                .EveryUpdate()
                .Where(_ => !Input.GetMouseButton(0))
                .FirstOrDefault()
                .Subscribe(_ =>
                {
                    JTileOfRoadEditedSignal.AddListener(OnTileDrawn);
                    if (!jRoadConstructor.HasSelection) return;
                    MapObjectView view = jConstructionController.SelectedObjectView != null
                        ? jConstructionController.SelectedObjectView
                        : jGameManager.GetMapView(jRoadConstructor.SelectedModel.SectorId)
                            .GetRoadElement(jRoadConstructor.LastPosition);

                    if (view == null) return;
                    var newPos = view.WorldPosition;
                    newPos.y += 2;
                    View.transform.position = newPos;
                    View.gameObject.SetActive(true);
                });
        }

        private void OnMove() => View.gameObject.SetActive(false);

        private void SetPosition()
        {
            if (jConstructionController.SelectedObjectView != null)
            {
                var newPos = jConstructionController.SelectedObjectView.WorldPosition;
                newPos.y += jConstructionController.SelectedObjectView.GridDimensions.y*2/3;

                View.transform.position = newPos;
                View.gameObject.SetActive(true);
                Model.isAcceptButtonEnabled.Value = jConstructionController.IsConstructionAvailable;
            }
        }

        private void OnConfirmBuild()
        {
            jConstructionController.ConfirmChangeObject();

            if (jConstructionController.IsRoadBuildingMode)//&&ViewVisible)
            {
                View.gameObject.SetActive(false);
                return;
            }
            Hide();
            //todo:remove one
            jSignalOnConfirmConstructSucceed.Dispatch();
        }

        private void OnCancelBuild()
        {
            if (!jConstructionController.IsRoadBuildingMode)
            {
                jConstructionController.CancelChangeObject();
                Hide();
                jSignalOnCancelConstruct.Dispatch();
            }
            else
            {
                jConstructionController.CancelChangeObject();
                Hide();
                jSignalOnCancelConstractWithoutExit.Dispatch();
            }
        }
    }
}

