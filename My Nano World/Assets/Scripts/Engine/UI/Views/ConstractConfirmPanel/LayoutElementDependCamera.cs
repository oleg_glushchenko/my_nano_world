﻿using UnityEngine;
using Assets.NanoLib.Utilities;
using NanoReality.Engine.BuildingSystem.CityCamera;
using strange.extensions.mediation.impl;
using UnityEngine.UI;

/// <summary>
/// Изменяет размер LaouytElemet-а в зависимости от текущего зума камеры
/// </summary>
[RequireComponent(typeof(LayoutElement))]
public class LayoutElementDependCamera : View
{
    [Inject]
    public IGameCameraModel jGameCameraModel { get; set; }

    /// <summary>
    /// Размер элемента при максимальном зуме
    /// </summary>
    public Vector2 MinSize;

    /// <summary>
    /// Размер элемента при минимальном зуме
    /// </summary>
    public Vector2 MaxSize;

    /// <summary>
    /// Для оптимизации
    /// </summary>
    public CacheMonobehavior CacheMonobehavior { get
    {
        return _cacheMonobehavior ?? (_cacheMonobehavior = new CacheMonobehavior(this));
    } }
    private CacheMonobehavior _cacheMonobehavior;

    protected void Update()
    {
        if (CacheMonobehavior.CachedGameObject.activeSelf && jGameCameraModel != null)
        {
            var layout = CacheMonobehavior.GetCacheComponent<LayoutElement>();

            var size = Vector2.Lerp(MinSize, MaxSize, jGameCameraModel.CurrentZoomPercent);
            layout.preferredWidth = size.x;
            layout.preferredHeight = size.y;
        }
    }
    
}
