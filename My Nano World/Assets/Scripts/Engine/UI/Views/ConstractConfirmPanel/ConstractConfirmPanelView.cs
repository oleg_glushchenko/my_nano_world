﻿using System;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Game.Tutorial;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Views.ConstractConfirmPanel
{
    public class ConstractConfirmPanelView : UIPanelView
    {
        [SerializeField] private Button _acceptButton;

        [SerializeField] private Button _cancelButton;

        private MapObjectView _constructedObjectView;

        [Inject] public SignalTutorialOnConfirmConstruct jSignalTutorialOnConfirmConstruct { get; set; }

        [Inject] public SignalOnCancelConstractInView jSignalOnCancelConstractInView { get; set; }

        [Inject] public SignalOnConfirmConstractInView jSignalOnConfirmConstractInView { get; set; }

        [Inject] public ConstructConfirmPanelModel Model { get; set; }

        public Button ConfirmButton => _acceptButton;
        public Button CancelButton => _cancelButton;
        
        private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();

        #region MonoBehaviour imlementation

        protected override void Start()
        {
            base.Start();

            Model.isAcceptButtonEnabled.Subscribe(x => _acceptButton.interactable = x).AddTo(_compositeDisposable);
            Model.isCancelButtonEnabled.Subscribe(x => _cancelButton.interactable = x).AddTo(_compositeDisposable);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            _compositeDisposable.Clear();
        }

        private void OnEnable()
        {
            _acceptButton.onClick.AddListener(OnAcceptClicked);
            _cancelButton.onClick.AddListener(OnCancelClicked);
        }

        private void OnDisable()
        {
            _acceptButton.onClick.RemoveListener(OnAcceptClicked);
            _cancelButton.onClick.RemoveListener(OnCancelClicked);
        }

        #endregion

        public void OnCancelClicked()
        {
            jSignalOnCancelConstractInView.Dispatch();
            Model.isAcceptButtonEnabled.Value = true;
        }

        public void OnAcceptClicked()
        {
            jSignalTutorialOnConfirmConstruct.Dispatch(_constructedObjectView);
            jSignalOnConfirmConstractInView.Dispatch();
        }

        //todo: replace with model values assign
        public override void SetButtonsAndTogglesEnabled(bool value)
        {
            _buttonsToEnableDisable.ForEach(b => b.enabled = value);
            _togglesToEnableDisable.ForEach(t => t.enabled = value);
        }

        public void SetTarget(MapObjectView target)
        {
            _constructedObjectView = target;
        }
    }

    public class ConstructConfirmPanelModel
    {
        public readonly ReactiveProperty<bool> isAcceptButtonEnabled = new ReactiveProperty<bool>(true);
        public readonly ReactiveProperty<bool> isCancelButtonEnabled = new ReactiveProperty<bool>(true);
    }
}