using System;
using Assets.NanoLib.UI.Core.Views;
using DG.Tweening;
using I2.Loc;
using NanoReality.UI.Components;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Views.TheEvent
{
    public class TheEventPanelView : UIPanelView
    {
        [SerializeField] private TabButton _dailyTabButton;
        [SerializeField] private TabButton _timelineTabButton;
        [SerializeField] private Button[] _closeButtons;
        [SerializeField] private Button _infoButton;
        [SerializeField] private Button _infoCloseButton;
        [SerializeField] private GameObject _infoPanel;
        [SerializeField] private TextMeshProUGUI _lanternsInfo;
        [SerializeField] private Button[] _nanoPassBuyButtons;
        [SerializeField] private TextMeshProUGUI[] _timeLeft;
        [SerializeField] private TextMeshProUGUI _lanterns;
        [SerializeField] private DailyTabView _dailyTabView;
        [SerializeField] private TimeLineTabView _timelineTabView;
        [SerializeField] private Animator _premiumPassBaner;
        [SerializeField] private GameObject _root;
        [SerializeField] private RectTransform _objects;

        private Sequence _tabSwitchAnimationSequence = null;
        private float _panelSwapBgOffset;
        private float _relativeSpeed= 7f;
        
        public DailyTabView DailyTab => _dailyTabView;
        public TimeLineTabView TimelineTab => _timelineTabView;
        public TabButton DailyTabButton => _dailyTabButton;
        public TabButton TimelineTabButton => _timelineTabButton;

        public Action OnCloseClick;
        public Action OnBuyNanoPassClick;
        public bool SwitchAnimInProgress;

        public void SetLanterns(int value)
        {
            _lanterns.text = value.ToString();
            _lanternsInfo.text = value.ToString();
        }

        public void SetTimerText(string text)
        {
            foreach (var textField in _timeLeft)
            {
                textField.text = text;
            }
        }

        public void DisableRoot()
        {
            _root.SetActive(false);
        }

        public override void Show()
        {
            base.Show();

            foreach (var button in _closeButtons)
            {
                button.onClick.AddListener(OnCloseButtonClick);
            }

            foreach (var button in _nanoPassBuyButtons)
            {
                button.onClick.AddListener(OnBuyNanoPassButtonClick);
            }

            _infoButton.onClick.AddListener(OpenInfoPanel);
            _infoCloseButton.onClick.AddListener(CloseInfoPanel);

            TimelineTab.ScrollRect.onValueChanged.AddListener(ScrollBackground);
            
            if (_timelineTabView.IsActive)
            {
                _premiumPassBaner.SetBool("Show", true);
            }
        }

        public override void Hide()
        {
            base.Hide();

            foreach (var button in _closeButtons)
            {
                button.onClick.RemoveListener(OnCloseButtonClick);
            }

            foreach (var button in _nanoPassBuyButtons)
            {
                button.onClick.RemoveListener(OnBuyNanoPassButtonClick);
            }

            TimelineTab.ScrollRect.onValueChanged.RemoveListener(ScrollBackground);
            
            _infoButton.onClick.RemoveListener(OpenInfoPanel);
            _infoCloseButton.onClick.RemoveListener(CloseInfoPanel);
        }

        public void DisableBuyNanoPassButtons()
        {
            foreach (var button in _nanoPassBuyButtons)
            {
                button.gameObject.SetActive(false);
            }
        }

        public void OnPanelsSwitchAnimation(bool switchToTimeline)
        {
            SwitchAnimInProgress = true;

            RectTransform timelineContent = _timelineTabView.ScrollRect.content;
            float screenWidth = _objects.rect.width;
            float panelOffset = screenWidth * 1.5f;
            float duration = 1f;

            if (_tabSwitchAnimationSequence == null)
            {
                _tabSwitchAnimationSequence = DOTween.Sequence();
                _timelineTabView.Tab.anchoredPosition = Vector2.left * (panelOffset + timelineContent.rect.width);
                _dailyTabView.Tab.anchoredPosition = Vector2.zero;
            }

            _tabSwitchAnimationSequence.Kill();

            var backgroundSpeedDumper = .001f;
            var backgroundScrollOffset = _panelSwapBgOffset+TimelineTab.ScrollRect.normalizedPosition.x*_relativeSpeed;
            
            if (switchToTimeline)
            {
                _premiumPassBaner.SetBool("Show", true);
                _tabSwitchAnimationSequence.Append(_dailyTabView.Tab.DOAnchorPos(Vector2.right * panelOffset, duration).OnComplete(OnAnimationComplete));
                _tabSwitchAnimationSequence.Append(_timelineTabView.Tab.DOAnchorPos(Vector2.zero, duration)
                    .SetEase(Ease.InOutBack));

                _timelineTabView.IsActive = true;
                _dailyTabView.IsActive = false;

                float transitionValue = panelOffset*backgroundSpeedDumper;
                DOTween.To(x => transitionValue = x, backgroundScrollOffset + panelOffset * backgroundSpeedDumper, 
                        backgroundScrollOffset, duration)
                    .SetEase(Ease.InOutBack)
                    .OnUpdate(() =>
                    {
                        Shader.SetGlobalFloat("_Scroll_Position", transitionValue);
                        _panelSwapBgOffset = transitionValue;
                    });
            }
            else
            {
                _premiumPassBaner.SetBool("Show", false);
                
                _tabSwitchAnimationSequence.Append(_timelineTabView.Tab.DOAnchorPos(Vector2.left * (panelOffset + timelineContent.rect.width), duration).OnComplete(OnAnimationComplete));
                _tabSwitchAnimationSequence.Append(_dailyTabView.Tab.DOAnchorPos(Vector2.zero, duration)
                    .SetEase(Ease.InOutBack).SetDelay(0f));
                _timelineTabView.IsActive = false;
                _dailyTabView.IsActive = true;


                float transitionValue = backgroundScrollOffset;
                DOTween.To(x => transitionValue = x, backgroundScrollOffset, 
                        backgroundScrollOffset + panelOffset * backgroundSpeedDumper, duration)
                    .SetEase(Ease.InOutBack).OnUpdate(() =>
                    {
                        Shader.SetGlobalFloat("_Scroll_Position", transitionValue);
                        _panelSwapBgOffset = transitionValue;
                    });
            }

            void OnAnimationComplete()
            {
                SwitchAnimInProgress = false;
            }
        }

        private void ScrollBackground(Vector2 value)
        {
            if(SwitchAnimInProgress) return;
            Shader.SetGlobalFloat("_Scroll_Position", _panelSwapBgOffset+value.x*_relativeSpeed); 
        }

        private void OpenInfoPanel()
        {
            _infoPanel.SetActive(true);
        }

        private void CloseInfoPanel()
        {
            var animator = _infoPanel.GetComponent<Animator>();

            if (animator != null)
            {
                animator.SetTrigger("Close");
                DOTween.Sequence().SetDelay(1f).OnComplete(() => { _infoPanel.SetActive(false); });
            }
            else
            {
                _infoPanel.SetActive(false);
            }
        }

        private void OnCloseButtonClick()
        {
            OnCloseClick?.Invoke();
        }

        private void OnBuyNanoPassButtonClick()
        {
            OnBuyNanoPassClick?.Invoke();
        }

        [Serializable]
        public class TabButton
        {
            public Button Button;
            public GameObject Notification;
            public GameObject NormalState;
            public GameObject SelectedState;
            public Image Normal;
            public Image Selected;
            public Action OnButtonClick;

            public void SetNormalState()
            {
                NormalState.SetActive(true);
                SelectedState.SetActive(false);
                Button.targetGraphic = Normal;
            }

            public void SetSelectedState()
            {
                SelectedState.SetActive(true);
                NormalState.SetActive(false);
                Button.targetGraphic = Selected;
            }

            public void SetNotification(bool enable)
            {
                Notification.SetActive(enable);
            }

            public void OnClick()
            {
                OnButtonClick?.Invoke();
            }

            public void AddButtonListener()
            {
                Button.onClick.AddListener(OnClick);
            }
        }

        [Serializable]
        public class DailyTabView
        {
            public RectTransform Tab;
            public ResizableProgressBar TimeLine;
            public ScrollRect ScrollRect;
            public TheEventRewardView[] CommonItems;
            public TheEventRewardView[] PremiumItems;
            public GameObject[] DayPoints;

            [HideInInspector]
            public bool IsActive = true;

            public void SetPassedDayPoint(int day)
            {
                DayPoints[day].SetActive(true);
            }

            public void SetActiveDayPoint(int day)
            {
                DayPoints[day].transform.GetChild(0).gameObject.SetActive(true);
            }

            public void SetTimeLineFill(float fill)
            {
                TimeLine.Value = fill;
            }
        }

        [Serializable]
        public class TimeLineTabView
        {
            public RectTransform Tab;
            public TextMeshProUGUI NanoPassDescription;
            public Localize NanoPassTerm;
            public TheEventStageItemView[] StageItems;
            public ScrollRect ScrollRect;

            [HideInInspector]
            public bool IsActive = false;

            public void SetScrollRectPosition(float value)
            {
                ScrollRect.horizontalNormalizedPosition = value;
            }

            public void SetNanoPassText(string term)
            {
                NanoPassTerm.Term = term;
            }
        }
    }
}