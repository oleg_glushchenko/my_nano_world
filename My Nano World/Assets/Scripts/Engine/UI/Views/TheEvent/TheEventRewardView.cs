using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Views.TheEvent
{
    public class TheEventRewardView : MonoBehaviour
    {
        [SerializeField] private GameObject _checkMark;
        [SerializeField] private GameObject _glow;
        [SerializeField] private Image _backgroundLocked;
        [SerializeField] private Image _backgroundUnlocked;
        [SerializeField] private Image _rewardImage;
        [SerializeField] private TextMeshProUGUI _amountLocked;
        [SerializeField] private TextMeshProUGUI _amountUnlocked;
        [SerializeField] private Button _claimButton;
        [SerializeField] private GameObject _lockedObjects;
        [SerializeField] private GameObject _unlockedObjects;

        public Action<TheEventRewardView> OnClaim;
        public Button ClaimButton => _claimButton;

        public void Unlock()
        {
            _lockedObjects.SetActive(false);
            _unlockedObjects.SetActive(true);
            _claimButton.targetGraphic = _backgroundUnlocked;
        }

        public void AddListenerClaimButton()
        {
            _claimButton.onClick.AddListener(OnClickClaim);
        }

        public void RemoveListenerClaimButton()
        {
            _claimButton.onClick.RemoveListener(OnClickClaim);
        }

        public void EnableCheckMark()
        {
            _checkMark.SetActive(true);
        }

        public void EnableGlow(bool enable)
        {
            _glow.SetActive(enable);
        }

        public void SetRewardImage(Sprite sprite)
        {
            _rewardImage.sprite = sprite;
        }

        public void SetRewardAmount(int amount)
        {
            _amountUnlocked.text = amount.ToString();
            _amountLocked.text = amount.ToString();
        }

        public void OnClickClaim()
        {
            OnClaim?.Invoke(this);
        }
    }
}
