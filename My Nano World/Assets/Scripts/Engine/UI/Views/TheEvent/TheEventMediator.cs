﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.UI.Core;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UtilitesEngine;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Assets.Scripts.GameLogic.Utilites;
using NanoReality.Core.Resources;
using NanoReality.Engine.Utilities;
using NanoReality.GameLogic.Bank;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.IAP;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.TheEvent;
using NanoReality.GameLogic.TheEvent.Data;
using NanoReality.GameLogic.Utilites;
using UnityEngine;

namespace NanoReality.Engine.UI.Views.TheEvent
{
    public class TheEventMediator : UIMediator<TheEventPanelView>
    {

        #region Injects

        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public IResourcesManager jResourcesManager { get; set; }
        [Inject] public ITimerManager jTimerManager { get; set; }
        [Inject] public IPlayerResources jPlayerResources { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IGameCamera jCamera { get; set; }
        [Inject] public IIapService jIapService { get; set; }
        [Inject] public ITheEventService jITheEventService { get; set; }
        [Inject] public BankContext jBankContext { get; set; }
        [Inject] public SignalOnLanternCurrencyStateChanged jSignalOnLanternCurrencyStateChanged { get; set; }
        [Inject] public HideWindowSignal jHideWindowSignal { get; set; }

        #endregion

        #region Private Variables
     
        private readonly Dictionary<TheEventRewardView, RewardInfo> _dailyRewardsMap = new Dictionary<TheEventRewardView, RewardInfo>();
        private readonly Dictionary<TheEventStageItemView, BuildingStageInfo> _stageMap = new Dictionary<TheEventStageItemView, BuildingStageInfo>();
        private ITimer _timer;
        private bool _dailyTabIsInitialized;
        private bool _buttonsIsInitialized;
        private bool _additionalDataIsInitialized;
        private bool _isPremiumUser;
        private bool _pendingRequest;
        private const float LootBoxAnimationLength = 3.5f;

        /// <summary>
        /// Positions of the timeline bar at daily tab should be set manually, depend on UI and the event length
        /// </summary>
        private readonly float[] _fillPositions = { 0, 0.1f, 0.23f, 0.4f, 0.54f, 0.7f, 0.85f, 1f };

        #endregion

        #region Show and Hide Methods

        protected override void OnShown()
        {
            base.OnShown();

            jSignalOnLanternCurrencyStateChanged.AddListener(OnChangeCurrency);
            View.SetLanterns(jPlayerResources.LanternCurrency);
            View.OnCloseClick += OnClickCloseTheEventPanel;
            View.OnBuyNanoPassClick += OnBuyNanoPassClick;
            SetAdditionalData();
            InitTimer(jITheEventService.EventDataBase.FinishDate);
            InitDailyTab();
            SetTimelineElements();
            InitTabButtons();
            SetNotificationDaily();
            SetNotificationTimeline();

            if (jPlayerProfile.IsPremiumUser)
            {
                OnNanoPassIsPurchased();
            }
            else
            {
                View.TimelineTab.SetNanoPassText(LocalizationUtils.GetUITerm(LocalizationKeyConstants.EVENTS_PANEL_NANOPASS_BEFORE_PURCHASE));
            }

            if (jITheEventService.OpenTimelineTab)
            {
                OnTimelineTabClick();
                jITheEventService.OpenTimelineTab = false;
            }

            if (View.TimelineTab.IsActive)
            {
                View.TimelineTab.SetScrollRectPosition(GetTimelineScrollPosition());
            }
        }

        protected override void OnHidden()
        {
            base.OnHidden();
            View.OnCloseClick -= OnClickCloseTheEventPanel;
            View.OnBuyNanoPassClick -= OnBuyNanoPassClick;
            jSignalOnLanternCurrencyStateChanged.RemoveListener(OnChangeCurrency);
            _timer?.CancelTimer();
        }

        #endregion

        #region The Event Panel Methods

        private void OnChangeCurrency(int _)
        {
            View.SetLanterns(jPlayerResources.LanternCurrency);
        }

        private void InitTimer(double finishTime)
        {
            var dateTime = DateTimeUtils.UnixStartTime.AddSeconds(finishTime).ToLocalTime();
            var secondsLeft = (dateTime - DateTime.Now).TotalSeconds;
            _timer = jTimerManager.StartServerTimer((float)secondsLeft, OnTimerFinish,
                elapsed =>
                {
                    View.SetTimerText(UiUtilities.GetFormattedTimeFromSecondsShorted((int)(secondsLeft - elapsed)));
                });
            View.SetTimerText(UiUtilities.GetFormattedTimeFromSecondsShorted((int)secondsLeft));
        }

        private void OnTimerFinish()
        {
            _timer?.CancelTimer();
            jUIManager.HideAll();
        }

        private void InitTabButtons()
        {
            if (_buttonsIsInitialized) return;

            View.DailyTabButton.AddButtonListener();
            View.TimelineTabButton.AddButtonListener();
            View.DailyTabButton.OnButtonClick += OnDailyTabClick;
            View.TimelineTabButton.OnButtonClick += OnTimelineTabClick;
            _buttonsIsInitialized = true;
        }

        private void SetNotificationDaily()
        {
            bool dailyNotificationIsEnabled = false;

            foreach (var entry in _dailyRewardsMap)
            {
                if (entry.Value.Day <= jITheEventService.EventDataBase.CurrentEventDay && !entry.Value.IsClaimed)
                {
                    if (entry.Value.IsPremium && !jPlayerProfile.IsPremiumUser)
                    {
                        continue;
                    }

                    View.DailyTabButton.SetNotification(true);
                    dailyNotificationIsEnabled = true;
                    break;
                }
            }

            if (!dailyNotificationIsEnabled)
            {
                View.DailyTabButton.SetNotification(false);
            }
        }

        private void SetNotificationTimeline()
        {
            bool timelineNotificationIsEnabled = false;

            foreach (var entry in _stageMap)
            {
                if (entry.Value.IsCompleted && !entry.Value.IsClaimed)
                {
                    View.TimelineTabButton.SetNotification(true);
                    timelineNotificationIsEnabled = true;
                    break;
                }
            }

            if (!timelineNotificationIsEnabled)
            {
                View.TimelineTabButton.SetNotification(false);
            }
        }

        private void InitDailyTab()
        {
            if (!_dailyTabIsInitialized)
            {
                _isPremiumUser = jPlayerProfile.IsPremiumUser;
                InitRewardsByDefault();
                SetDayPoints();
                View.DailyTab.SetTimeLineFill(_fillPositions[jITheEventService.EventDataBase.CurrentEventDay]);
                if (_isPremiumUser)
                {
                    UnlockPremiumRewards();
                }

                _dailyTabIsInitialized = true;
            }
            else
            {
                if (_isPremiumUser != jPlayerProfile.IsPremiumUser)
                {
                    UnlockPremiumRewards();
                }
            }
        }

        private void SetTimelineElements()
        {
            for (int day = 0; day < jITheEventService.EventDataBase.CurrentEventDay; day++)
            {
                TheEventStageItemView item = View.TimelineTab.StageItems[day];
                BuildingStageInfo stageData = jITheEventService.EventDataBase.BuildingStages[day];
                item.OnClaimClickAction -= OnClickClaimStageReward;
                item.OnGoToClickAction -= OnClickGoTo;

                if (stageData.IsClaimed)
                {
                    item.EnableNormalColor();
                    continue;
                }

                if (stageData.IsCompleted)
                {
                    item.Rise();
                    item.EnableClaimButton();
                    item.AddListenerClaimButton();
                    item.OnClaimClickAction += OnClickClaimStageReward;
                    item.EnableNormalColor();
                    break;
                }
                else
                {
                    item.Rise();
                    item.AddListenerGoToButton();
                    item.OnGoToClickAction += OnClickGoTo;
                    item.EnableNormalColor();
                    break;
                }
            }
        }

        private void SetDayPoints()
        {
            for (int day = 0; day < View.DailyTab.DayPoints.Length; day++)
            {
                if (day == jITheEventService.EventDataBase.CurrentEventDay)
                {
                    View.DailyTab.SetActiveDayPoint(day - 1);
                    break;
                }

                View.DailyTab.SetPassedDayPoint(day);
            }
        }

        private void InitRewardsByDefault()
        {
            bool glowCommonIsEnabled = false;

            for (int day = 0; day < jITheEventService.EventDataBase.DailyRewards.Length; day++)
            {
                TheEventRewardView commonView = View.DailyTab.CommonItems[day];
                TheEventRewardView premiumView = View.DailyTab.PremiumItems[day];

                commonView.SetRewardImage(
                    jResourcesManager.GetEventSprite(jITheEventService.EventDataBase.DailyRewards[day].CommonRewards.UiType));
                commonView.SetRewardAmount(jITheEventService.EventDataBase.DailyRewards[day].CommonRewards.Amount);

                premiumView.SetRewardImage(
                    jResourcesManager.GetEventSprite(jITheEventService.EventDataBase.DailyRewards[day].PremiumRewards.UiType));
                premiumView.SetRewardAmount(jITheEventService.EventDataBase.DailyRewards[day].PremiumRewards.Amount);

                if (day < jITheEventService.EventDataBase.CurrentEventDay)
                {
                    UnlockRewardView(commonView, jITheEventService.EventDataBase.DailyRewards[day].CommonRewards, ref glowCommonIsEnabled);
                }
            }
        }

        private void UnlockPremiumRewards()
        {
            bool glowPremiumIsEnabled = false;

            for (int day = 0; day < jITheEventService.EventDataBase.DailyRewards.Length; day++)
            {
                TheEventRewardView premiumView = View.DailyTab.PremiumItems[day];

                if (day < jITheEventService.EventDataBase.CurrentEventDay)
                {
                    UnlockRewardView(premiumView, jITheEventService.EventDataBase.DailyRewards[day].PremiumRewards, ref glowPremiumIsEnabled);
                }
            }
        }

        private void UnlockRewardView(TheEventRewardView view, RewardInfo info, ref bool glowIsEnabled)
        {
            view.Unlock();

            if (info.IsClaimed)
            {
                view.EnableCheckMark();
            }
            else if (!glowIsEnabled)
            {
                glowIsEnabled = true;
                SetActiveRewardView(view);
            }
        }

        private void SetActiveRewardView(TheEventRewardView rewardView)
        {
            rewardView.OnClaim += OnClickClaimDailyReward;
            rewardView.EnableGlow(true);
            rewardView.AddListenerClaimButton();
        }
      
        private IEnumerator OnClaimRewardFinalStage()
        {
            jServerCommunicator.FinalizeEvent(null);
            yield return new WaitForSeconds(LootBoxAnimationLength);
            jCamera.FocusOnMapObject(jITheEventService.GetEventBuildingView().MapObjectModel);

            jPopupManager.Show(new CongratsEventFinishSetting(jITheEventService.OnGettingEventRewardBuilding, jITheEventService.OnDisappearAnimEnds)
            {
                Message = LocalizationUtils.LocalizeUI(LocalizationKeyConstants.EVENTS_CONGRATS_BODY)
            });

            jHideWindowSignal.Dispatch(typeof(TheEventPanelView));
        }
       
        private void OnNanoPassIsPurchased()
        {
            View.DisableBuyNanoPassButtons();
            View.TimelineTab.SetNanoPassText(LocalizationUtils.GetUITerm(LocalizationKeyConstants.EVENTS_PANEL_NANOPASS_PURCHASED));
        }

        private void SetAdditionalData()
        {
            if (_additionalDataIsInitialized) return;

            for (int i = 0; i < jITheEventService.EventDataBase.DailyRewards.Length; i++)
            {
                jITheEventService.EventDataBase.DailyRewards[i].CommonRewards.Day = i + 1;
                jITheEventService.EventDataBase.DailyRewards[i].PremiumRewards.Day = i + 1;
                jITheEventService.EventDataBase.DailyRewards[i].CommonRewards.IsPremium = false;
                jITheEventService.EventDataBase.DailyRewards[i].PremiumRewards.IsPremium = true;
                jITheEventService.EventDataBase.BuildingStages[i].Day = i + 1;
                _dailyRewardsMap.Add(View.DailyTab.PremiumItems[i], jITheEventService.EventDataBase.DailyRewards[i].PremiumRewards);
                _dailyRewardsMap.Add(View.DailyTab.CommonItems[i], jITheEventService.EventDataBase.DailyRewards[i].CommonRewards);
                _stageMap.Add(View.TimelineTab.StageItems[i], jITheEventService.EventDataBase.BuildingStages[i]);
            }

            _additionalDataIsInitialized = true;
        }

        #endregion

        #region On Click Methods

        private void OnClickCloseTheEventPanel()
        {
            jHideWindowSignal.Dispatch(typeof(TheEventPanelView));
        }

        private void OnDailyTabClick()
        {
            if (View.DailyTab.IsActive) return;
            if (View.SwitchAnimInProgress) return;

            View.DailyTabButton.SetSelectedState();
            View.TimelineTabButton.SetNormalState();
            View.OnPanelsSwitchAnimation(false);
        }

        private void OnTimelineTabClick()
        {
            if (View.SwitchAnimInProgress) return;
            if (View.TimelineTab.IsActive)
            {
                View.TimelineTab.SetScrollRectPosition(GetTimelineScrollPosition());
                return;
            }

            View.TimelineTabButton.SetSelectedState();
            View.DailyTabButton.SetNormalState();
            View.OnPanelsSwitchAnimation(true);
            View.TimelineTab.SetScrollRectPosition(GetTimelineScrollPosition());
            SetTimelineElements();
        }

        private void OnClickGoTo()
        {
            var view = jITheEventService.GetEventBuildingView();
            jUIManager.HideAll();
            jCamera.FocusOnMapObject(view.MapObjectModel);
            view.OnMapObjectViewTap();
        }

        private void OnClickClaimDailyReward(TheEventRewardView view)
        {
            if (_pendingRequest)
            {
                return;
            }

            _pendingRequest = true;
            jServerCommunicator.ClaimDailyReward(_dailyRewardsMap[view].RewardId, () =>
            {
                _pendingRequest = false;

                jITheEventService.AddReward(_dailyRewardsMap[view].Rewards, _dailyRewardsMap[view].LootBoxType);
                view.EnableGlow(false);
                view.EnableCheckMark();
                view.OnClaim -= OnClickClaimDailyReward;
                _dailyRewardsMap[view].IsClaimed = true;
                view.RemoveListenerClaimButton();

                if (_dailyRewardsMap[view].Day < jITheEventService.EventDataBase.DailyRewards.Length)
                {
                    var nextReward = _dailyRewardsMap.First(entry =>
                        entry.Value.Day == _dailyRewardsMap[view].Day + 1 &&
                        entry.Value.IsPremium == _dailyRewardsMap[view].IsPremium);

                    if (jITheEventService.EventDataBase.CurrentEventDay >= nextReward.Value.Day)
                    {
                        SetActiveRewardView(nextReward.Key);
                    }
                }

                SetNotificationDaily();
            });
        }

        private void OnClickClaimStageReward(TheEventStageItemView view)
        {
            if (_pendingRequest)
            {
                return;
            }

            _pendingRequest = true;

            jServerCommunicator.ClaimStageReward(_stageMap[view].Day, (claimData) =>
            {
                _pendingRequest = false;
                jITheEventService.AddReward(claimData.Rewards, claimData.LootBoxType);
                _stageMap[view].IsClaimed = true;
                view.Collapse();
                SetTimelineElements();
                SetNotificationTimeline();

                if (_stageMap[view].Day == jITheEventService.EventDataBase.DailyRewards.Length)
                {
                    StartCoroutine(OnClaimRewardFinalStage());
                }
            });
        }

        private void OnBuyNanoPassClick()
        {
            IBankProduct nanoPass = jIapService.InAppShopOffers.Products.First(offer => offer.UiStyle == OfferStyle.NanoPass);
            OffersPurchaseProvider provider = new OffersPurchaseProvider();
            provider.Initialize(jBankContext);
            var settings = new NanoPassPopupSettings(nanoPass, provider)
            {
                OnSuccessfulPurchase = OnSuccessfulPurchaseNanoPass,
                EnableBackground = true
            };

            jPopupManager.Show(settings);
        }

        #endregion

        private void OnSuccessfulPurchaseNanoPass()
        {
            UnlockPremiumRewards();
            OnNanoPassIsPurchased();
        }

        private float GetTimelineScrollPosition()
        {
            float distance = 1f / View.TimelineTab.ScrollRect.content.childCount;
            int index;

            for (index = 0; index < jITheEventService.EventDataBase.BuildingStages.Length; index++)
            {
                if (!jITheEventService.EventDataBase.BuildingStages[index].IsCompleted)
                {
                    break;
                }
            }

            return distance * index;
        }
    }
}