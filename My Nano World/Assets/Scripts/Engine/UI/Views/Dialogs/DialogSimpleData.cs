using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;

namespace Assets.Scripts.Engine.UI.Views.Replics
{
    public class DialogSimpleData
    {
        public readonly BusinessSectorsTypes BusinessSectorType;
        public readonly string Text;
        public readonly DialogPosition Position;

        public DialogSimpleData(BusinessSectorsTypes businessSectorType, string text, DialogPosition position)
        {
            BusinessSectorType = businessSectorType;
            Text = text;
            Position = position;
        }
    }
}