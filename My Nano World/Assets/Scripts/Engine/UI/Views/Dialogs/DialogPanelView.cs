﻿using System;
using System.Collections.Generic;
using NanoLib.Services.InputService;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.UI.Views.Replics;
using Assets.Scripts.MultiLanguageSystem.Logic;
using I2.Loc;
using NanoLib.Core.Services.Sound;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.UI
{
    public class DialogPanelView : UIPanelView
    {
        private static readonly Vector3 OneInverseY = new Vector3(1, -1, 1);
        private static readonly Vector3 TextContainerDefault = new Vector3(270, -390, 0);
        private static readonly Vector3 BubbleCornerDefault = new Vector3(204, -165, 0);
        private static readonly Vector3 TextContainerSomeUp = new Vector3(270, -220, 0);
        private static readonly Vector3 BubbleCornerSomeUp = new Vector3(204, -30, 0);
        private static readonly Vector3 TextContainerUp = new Vector3(200, 40, 0);
        private static readonly Vector3 BubbleCornerUp = new Vector3(134, 30, 0);

        [SerializeField] private Image _darkness;
        [SerializeField] private Transform _transformContainer;
        [SerializeField] private Image _imageCharacter;
        [SerializeField] private RectTransform _transformImageCharacter;
        [SerializeField] private DialogBubble _bubble;
        [SerializeField] private Transform _transformBubbleCorner;
        [SerializeField] private Transform _transformTextContainer;
        [SerializeField] private Transform _transformText;
        [SerializeField] private Localize _text;
        [SerializeField] private List<DialogAnchor> _dialogAnchors;

        [SerializeField] private List<CharacterExpressionSpriteMouthPosition> _characterExpressionsSpritesMouthPositions;

        [SerializeField] private Button _dialogGoToButton;
        [SerializeField] private TextMeshProUGUI _dialogButtonText;
        [SerializeField] private Button _dialogRewardButton;
        [SerializeField] private Button _acceptButton;
        [SerializeField] private Button _declineButton;
        [SerializeField] private TextMeshProUGUI _acceptButtonText;
        [SerializeField] private TextMeshProUGUI _declineButtonText;
        [SerializeField] private GameObject _blockRaycastPanel;
        
        [SerializeField] private GameObject _tutorialContainer;
        [SerializeField] private GameObject _hintContainer;
        [SerializeField] private TextMeshProUGUI _textHint;
        [SerializeField] private TextMeshProUGUI _titleHint;
        [SerializeField] private TextMeshProUGUI _acceptButtonHintText;
        [SerializeField] private Button _acceptButtonHint;
        [SerializeField] private Button _closeButtonHint;

        [Inject] public HideWindowSignal jHideWindowSignal { get; set; }
        [Inject] public SignalOnBeginTouch jBeginTouchSignal { get; set; }
        [Inject] public ISoundManager jSoundManager { get; set; }
        

        private DialogData _currentDialogData;

        private Button DialogButton { get; set; }

        #region API

        public override void Show()
        {
            base.Show();

            jSoundManager.Play(SfxSoundTypes.AppWindow, SoundChannel.SoundFX);
            _acceptButton.onClick.AddListener(OnAcceptClicked);
            _declineButton.onClick.AddListener(OnDeclineClicked);

            AddHintListeners();
        }

        public override void Hide()
        {
            RemoveHintListeners();
                
            jBeginTouchSignal.RemoveListener(HidePanel);
            _acceptButton.onClick.RemoveListener(OnAcceptClicked);
            _declineButton.onClick.RemoveListener(OnDeclineClicked);
            
            _darkness.gameObject.SetActive(false);
            _darkness.raycastTarget = false;
            _text.Term = "";
            _transformContainer.gameObject.SetActive(false);
            _dialogButtonText.text = "";

            _dialogGoToButton.gameObject.SetActive(false);
            _acceptButton.gameObject.SetActive(false);
            _declineButton.gameObject.SetActive(false);

            _acceptButtonText.text = string.Empty;
            _declineButtonText.text = string.Empty;
            base.Hide();
        }

        private void HidePanel()
        {
            if (_bubble.IsTheEndOfText())
            {
                jHideWindowSignal.Dispatch(typeof(DialogPanelView));
            }
            else
            {
                _bubble.SkipPrintEffect();
            }
        }

        private void AddHintListeners()
        {
            _acceptButtonHint.onClick.AddListener(OnAcceptClicked);
            _closeButtonHint.onClick.AddListener(OnDeclineClicked);
        }

        private void RemoveHintListeners()
        {
            _acceptButtonHint.onClick.RemoveListener(OnAcceptClicked);
            _closeButtonHint.onClick.RemoveListener(OnDeclineClicked);
        }

        private void SaySomeText(DialogData data)
        {
            _currentDialogData = data;
            var settings = _characterExpressionsSpritesMouthPositions.Find(c =>
                c.Character == data.CharacterToShow && c.Expression == data.CharacterExpression);
            var replicAnchor = _dialogAnchors.Find(ra => ra.PositionType == data.Position);
            var isBubbleUp = data.Position == DialogPosition.LeftBubbleUp ||
                             data.Position == DialogPosition.RightBubbleUp;
            var isSomeUp = data.Position == DialogPosition.LeftSomeUp ||
                           data.Position == DialogPosition.RightSomeUp;

            _darkness.gameObject.SetActive(data.ShowFade);
            _transformContainer.SetParent(replicAnchor.Anchor, false);
            _bubble.transform.SetParent(settings.MouthPosition, false);

            _imageCharacter.gameObject.SetActive(data.CharacterExpression != CharacterExpression.Hide);
            _imageCharacter.sprite = settings.Sprite;

            _transformImageCharacter.sizeDelta = settings.Sprite.rect.size;
            _transformBubbleCorner.localScale = isBubbleUp ? OneInverseY : Vector3.one;
            _transformTextContainer.localPosition =
                isBubbleUp ? TextContainerUp : isSomeUp ? TextContainerSomeUp : TextContainerDefault;
            _transformBubbleCorner.localPosition =
                isBubbleUp ? BubbleCornerUp : isSomeUp ? BubbleCornerSomeUp : BubbleCornerDefault;
            _text.Term = data.TextKey;
            _transformText.localScale = replicAnchor.Anchor.localScale;
            _transformContainer.gameObject.SetActive(true);
            
            _bubble.Show(_text.GetMainTargetsText());

            bool showOneButton = data.ButtonEnabled && !data.ButtonIsReward;
            bool showAcceptButtons = data.ShowAcceptButtons;
            if (showOneButton)
            {
                _dialogGoToButton.gameObject.SetActive(true);
                _dialogRewardButton.gameObject.SetActive(data.ButtonEnabled && data.ButtonIsReward);
                DialogButton = data.ButtonIsReward ? _dialogRewardButton : _dialogGoToButton;

                _darkness.raycastTarget = true;
                if (data.ButtonEnabled && !data.ButtonIsReward)
                {
                    _dialogButtonText.SetLocalizedText(data.ButtonText);
                }

                _blockRaycastPanel.gameObject.SetActive(true);
            }
            else if (showAcceptButtons)
            {
                _acceptButton.gameObject.SetActive(true);
                _declineButton.gameObject.SetActive(true);
                _darkness.raycastTarget = true;

                _acceptButtonText.text = data.AcceptButtonText;
                _declineButtonText.text = data.DeclineButtonText;
                _blockRaycastPanel.gameObject.SetActive(true);
            }
            else
            {
                jBeginTouchSignal.AddListener(HidePanel);
                _blockRaycastPanel.gameObject.SetActive(false);
            }
        }

        private void ShowHintView(DialogData data)
        {
            if (!data.UseMessageBoxPopup)
            {
                _hintContainer.SetActive(false);
                _tutorialContainer.SetActive(true);
                return;
            }
            
            _hintContainer.SetActive(true);
            _tutorialContainer.SetActive(false);

            _textHint.SetLocalizedText(data.TextKey);
            _acceptButtonHintText.text =
                LocalizationManager.GetTranslation(ScriptLocalization.Tutorial.TUTORIAL_HINT_SHOW);
            _titleHint.text = LocalizationManager.GetTranslation(ScriptLocalization.Tutorial.TUTORIAL_HINT_TITLE);
        }

        #endregion

        public override void Initialize(object parameters)
        {
            base.Initialize(parameters);
            switch (parameters)
            {
                case DialogSimpleData simpleData:
                    ShowReplicaHandler(simpleData);
                    break;
                case DialogData data:
                    SaySomeText(data);
                    ShowHintView(data);
                    break;
            }
        }

        #region privates

        private void ShowReplicaHandler(DialogSimpleData simpleData)
        {
            if (simpleData.BusinessSectorType.IsSupported())
            {
                DialogCharacter character;
                if (simpleData.BusinessSectorType == BusinessSectorsTypes.Farm)
                {
                    character = DialogCharacter.Farmer;
                }
                else if (simpleData.BusinessSectorType == BusinessSectorsTypes.HeavyIndustry)
                {
                    character = DialogCharacter.Engineer;
                }
                else
                {
                    character = DialogCharacter.Girl;
                }

                SaySomeText(new DialogData(character, simpleData.Text, simpleData.Position));
            }
        }

        #endregion

        private void OnDeclineClicked()
        {
            _currentDialogData.AcceptResult.Invoke(false);
            jHideWindowSignal.Dispatch(typeof(DialogPanelView));
        }

        private void OnAcceptClicked()
        {
            _currentDialogData.AcceptResult.Invoke(true);
            jHideWindowSignal.Dispatch(typeof(DialogPanelView));
        }
    }

    [Serializable]
    public class DialogAnchor
    {
        public DialogPosition PositionType;
        public Transform Anchor;
    }

    [Serializable]
    public class CharacterExpressionSpriteMouthPosition
    {
        public DialogCharacter Character;
        public CharacterExpression Expression;
        public Sprite Sprite;
        public Transform MouthPosition;
    }

    public enum DialogPosition
    {
        Left,
        LeftSomeUp,
        LeftBubbleUp,
        Right,
        RightSomeUp,
        RightBubbleUp
    }

    public enum DialogCharacter
    {
        Engineer,
        Farmer,
        Girl
    }

    public enum CharacterExpression
    {
        Attention,
        Cool,
        Idle,
        LookAt,
        Hide
    }
}