﻿namespace NanoReality.GameLogic.Bank
{
	public enum BankTabType
	{
		Default,
		Offers,
		Soft,
		Hard,
		Gems
	}
}