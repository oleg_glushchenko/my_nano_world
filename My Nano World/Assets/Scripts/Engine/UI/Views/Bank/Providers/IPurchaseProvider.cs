﻿using System.Collections.Generic;

namespace NanoReality.GameLogic.Bank
{
	public interface IPurchaseProvider
	{
		void Initialize(BankContext context);
		void BuyProductClicked(IBankProduct product);
		IEnumerable<IBankProduct> GetProducts();
	}
}