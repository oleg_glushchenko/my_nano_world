using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.GameLogic.Utilites;
using NanoLib.Core.Logging;
using NanoLib.Core.Services.Sound;
using NanoReality.GameLogic.Constants;
using NanoReality.GameLogic.IAP;
using NanoReality.GameLogic.PlayerProfile.Models.Impl;
using NanoReality.GameLogic.Utilites;

namespace NanoReality.GameLogic.Bank
{
    public class OffersPurchaseProvider : IPurchaseProvider
    {
        private BankContext _context;
        private bool _isLocked;

        // Between server and client time could be different in a few milliseconds. Added delay to compensate it.
        private const int Delay = 20;
        private float _fxAnimLength = 2;

        public void Initialize(BankContext context)
        {
            _context = context;
        }
      
        public void BuyProductClicked(IBankProduct bankProduct)
        {
            if (_isLocked) return;
            _isLocked = true;

            if (bankProduct is InAppShopOffer product)
            {
                _context.jPurchaseFinishedSignal.AddListener(OnPurchaseComplete);
                _context.jIapService.BuyProduct(product.StoreId);
            }
            else
            {
                Logging.LogError(LoggingChannel.Core, "The product is not offer");
                _isLocked = false;
            }
        }

        public IEnumerable<IBankProduct> GetProducts()
        {
            if (_context.jIapService.InAppShopOffers != null)
            {
                var currentTime = ((DateTimeOffset) DateTime.Now).ToUnixTimeSeconds();
                return _context.jIapService.InAppShopOffers.Products
                    .Where(product => currentTime + Delay >= product.StartTime && (product.FinishTime == 0 || currentTime < product.FinishTime))
                    .OrderByDescending(product => product.Priority);
            }

            Logging.Log(LoggingChannel.Core, "Offers not ready");
            return null;
        }

        private void OnPurchaseComplete(IStoreProduct product, bool isSuccess)
        {
            if (isSuccess)
            {
                _context.jSoundManager.Play(SfxSoundTypes.BuyAnyBankItem, SoundChannel.SoundFX);
                var offer = (InAppShopOffer) product;

                _context.jPurchaseOfferFinishedSignal.Dispatch();
                _context.jNanoAnalytics.OnInAppPurchase(offer.StoreId, offer.StorePrice);

                for (int i = 0; i < offer.Reward.Length; i++)
                {
                    switch (offer.Reward[i].Type)
                    {
                        case RewardType.Soft:
                            _context.jPlayerProfile.PlayerResources.AddSoftCurrency(offer.Reward[i].Amount);
                            _context.jNanoAnalytics.OnPlayerCurrenciesIncrease(PlayerResources.CurencyType.Coins, offer.Reward[i].Amount, AwardSourcePlace.Purchase, product.StoreId);
                            break;

                        case RewardType.Hard:
                            _context.jPlayerProfile.PlayerResources.AddHardCurrency(offer.Reward[i].Amount);
                            _context.jNanoAnalytics.OnPlayerCurrenciesIncrease(PlayerResources.CurencyType.Bucks, offer.Reward[i].Amount, AwardSourcePlace.Purchase, product.StoreId);
                            break;

                        case RewardType.Products:
                            _context.jPlayerProfile.PlayerResources.AddProduct(offer.Reward[i].Id, offer.Reward[i].Amount);
                            break;

                        case RewardType.Gold:
                            _context.jPlayerProfile.PlayerResources.AddGoldCurrency(offer.Reward[i].Amount);
                            _context.jNanoAnalytics.OnPlayerCurrenciesIncrease(PlayerResources.CurencyType.Gold, offer.Reward[i].Amount, AwardSourcePlace.Purchase, product.StoreId);
                            break;

                        case RewardType.Lanterns:
                            _context.jPlayerProfile.PlayerResources.AddLanternCurrency(offer.Reward[i].Amount);
                            _context.jNanoAnalytics.OnPlayerCurrenciesIncrease(PlayerResources.CurencyType.Lanterns, offer.Reward[i].Amount, AwardSourcePlace.Purchase, product.StoreId);
                            break;

                        case RewardType.NanoPass:
                            _context.jPlayerProfile.IsPremiumUser = true;
                            _context.jNanoAnalytics.OnPurchaseNanoPass(product.StoreId);
                            break;
                    }
                }

                _context.jShowWindowSignal.Dispatch(typeof(FxPopUpPanelView), new FxPopUpPanelView.FxPopupData(
                    LocalizationUtils.LocalizeUI(LocalizationKeyConstants.PURCHASED_OFFER), _fxAnimLength, FxPanelType.Achievement));
            }
           
            _isLocked = false;
            _context.jPurchaseFinishedSignal.RemoveListener(OnPurchaseComplete);
            _context.jIapService.UpdateOffers();
        }
    }
}