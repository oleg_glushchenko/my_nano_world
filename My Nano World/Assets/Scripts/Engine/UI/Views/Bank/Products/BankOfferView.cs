using System;
using System.Collections.Generic;
using NanoReality.GameLogic.IAP;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.GameLogic.Bank
{
    public sealed class BankOfferView : BankProductBase
    {
        [SerializeField] private Image _icon;
        [SerializeField] private GameObject _discountGameObject;
        [SerializeField] private GameObject _oldPriceGameObject;
        [SerializeField] private GameObject _timerGameObject;
        [SerializeField] private TextMeshProUGUI _discountText;
        [SerializeField] private TextMeshProUGUI _name;
        [SerializeField] private TextMeshProUGUI _price;
        [SerializeField] private TextMeshProUGUI _oldPrice;
        [SerializeField] private TextMeshProUGUI _timerText;
        [SerializeField] private OfferRewardItemView[] _rewards;

        public InAppShopOffer Data => _data;
        public override event Action<IBankProduct> BuyBundleClick;
        private Dictionary<int, Sprite> _sprites;
        private InAppShopOffer _data;
     
        public override void Initialize(IBankProduct model, Sprite sprite)
        {
            _data = (InAppShopOffer) model;

            _name.text = Utilites.LocalizationUtils.Localize(_data.Name);

            _price.text = _data.Price + "$";

            if (!string.IsNullOrEmpty(_data.Discount))
            {
                _discountGameObject.SetActive(true);
                _discountText.text = _data.Discount;
            }

            if (!string.IsNullOrEmpty(_data.OldPrice))
            {
                _oldPriceGameObject.SetActive(true);
                _oldPrice.text = _data.OldPrice;
            }
            
            InitRewardsView();
        }

        public void SetImage(Sprite sprite)
        {
            _icon.sprite = sprite;
        }

        public void SetSprites(Dictionary<int, Sprite> sprites)
        {
            _sprites = sprites;
        }

        public void EnableTimer(string timerText)
        {
            _timerGameObject.SetActive(true);
            _timerText.text = timerText;
        }

        public void SetTimerText(string timerText)
        {
            _timerText.text = timerText;
        }

        private void InitRewardsView()
        {
            for (var i = 0; i < _data.Reward.Length; i++)
            {
                _rewards[i].InitProduct(_data.Reward[i].Amount, _sprites[i]);
            }
        }

        private void Reset()
        {
            _name.text = string.Empty;
            _price.text = string.Empty;
            _discountText.text = string.Empty;
            _oldPrice.text = string.Empty;
            _timerText.text = string.Empty;
            _icon.sprite = null;
            _sprites = null;
            _discountGameObject.SetActive(false);
            _oldPriceGameObject.SetActive(false);
            _timerGameObject.SetActive(false);
            
            foreach (var item in _rewards) 
                item.Reset();
        }

        public void OnPurchaseHandle() //Calls from Unity
        {
            BuyBundleClick?.Invoke(_data);
        }

        #region IPoolable

        public override void Restore()
        {
        }

        public override void Retain()
        {
            gameObject.SetActive(true);
        }

        public override void Release()
        {
            BuyBundleClick = delegate { };
            Reset();
            gameObject.SetActive(false);
        }

        public override bool retain { get; }

        #endregion
    }
}