﻿using System.Collections.Generic;
using NanoReality.GameLogic.Bank;
using NanoReality.GameLogic.StartApplicationSequence.Models.api;

namespace NanoReality.GameLogic.SoftCurrencyStore
{
    public interface ISoftCurrencyBundlesModel : IGameBalanceData
    {
        List<SoftCurrencyProduct> SoftCurrencyBundles { get; set; }
    }
}