﻿using System;
using System.Collections.Generic;
using Assets.NanoLib.Utilities.DataManager.Models.api;
using NanoReality.GameLogic.Bank;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.StartApplicationSequence.Models.api;
using Newtonsoft.Json;

namespace NanoReality.GameLogic.SoftCurrencyStore
{
    public class SoftCurrencyBundlesModel : ISoftCurrencyBundlesModel
    {
        [JsonProperty("SoftCurrencyBundles")]
        public List<SoftCurrencyProduct> SoftCurrencyBundles { get; set; }

        #region GameBalance
        
        public string DataHash { get; set; }
        
        public void LoadData(IServerCommunicator serverCommunicator, ILocalDataManager localDataManager, Action<IGameBalanceData> callback)
        {
            serverCommunicator.LoadSoftCurrencyBundles(data =>
            {
                SoftCurrencyBundles = data.SoftCurrencyBundles;
                DataHash = data.DataHash;
                callback(this);
            });
        }
        
        #endregion
    }
}
