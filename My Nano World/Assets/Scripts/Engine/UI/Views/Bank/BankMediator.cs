﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.UI.Core;
using Assets.NanoLib.UtilitesEngine;
using Assets.Scripts.Engine.UI.Views.Hud;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Assets.Scripts.GameLogic.Utilites;
using DG.Tweening;
using NanoLib.Core.Pool;
using NanoLib.Utilities;
using NanoReality.Core.Resources;
using NanoReality.Engine.Utilities;
using NanoReality.GameLogic.Common.Controllers;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.Constants;
using NanoReality.GameLogic.IAP;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Utilites;
using Sirenix.Utilities;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.GameLogic.Bank
{
    public class BankMediator : UIMediator<BankView>
    {
        #region Injects

        [Inject] public BankContext jBankContext { get; set; }
        [Inject] public OnHudBottomPanelIsVisibleSignal jOnHudBottomPanelIsVisibleSignal { get; set; }
        [Inject] public IPlayerProfile PlayerProfile { get; set; }
        [Inject] public SignalOnSoftCurrencyStateChanged jSignalOnSoftCurrencyStateChanged { get; set; }
        [Inject] public SignalOnHardCurrencyStateChanged jSignalOnHardCurrencyStateChanged { get; set; }
        [Inject] public OffersValidationController jOffersValidationController { get; set; }
        [Inject] public PurchaseOfferFinishedSignal jPurchaseOfferFinishedSignal { get; set; }
        [Inject] public IResourcesManager jResourcesManager { get; set; }
        [Inject] public ITimerManager jTimeManager { get; set; }
        [Inject] public DisableOffersButtonSignal jDisableOffersButtonSignal { get; set; }
        [Inject] public SignalLoadingSpinner jSignalLoadingSpinner { get; set; }
        [Inject] public SignalOffersUpdated jSignalOffersUpdated { get; set; }

        #endregion

        private bool _isOffersExist;
        private BankView.BankTab _offersTab;
        private IPurchaseProvider _offersProvider;
        
        private readonly Dictionary<BankOfferView, ITimer> _timers = new Dictionary<BankOfferView, ITimer>();
        private readonly Dictionary<BankTabType, IPurchaseProvider> _providers = new Dictionary<BankTabType, IPurchaseProvider>
        {
            {BankTabType.Soft, new SoftCurrencyPurchaseProvider()},
            {BankTabType.Hard, new PremiumCurrencyPurchaseProvider()},
            {BankTabType.Offers, new OffersPurchaseProvider()}
        };
        
        private const int GoldId = 532;
        
        protected override void Show(object param)
        {
            void OnOfferUpdate()
            {
                InitBank(param);
                View.Spinner.StopSpin();
                jSignalOffersUpdated.RemoveListener(OnOfferUpdate);
            }
            base.Show(param);

            if (jBankContext.jIapService.OffersUpdating)
            {
           
                View.Spinner.StartSpin();
                jSignalOffersUpdated.AddListener(OnOfferUpdate);

                return;
            }

            InitBank(param);
        }

        private void InitBank(object param)
        {
            View.BuyProductClicked += OnBuyProductClicked;
            jOnHudBottomPanelIsVisibleSignal.Dispatch(false);

            _providers.ForEach(provider =>
            {
                provider.Value.Initialize(jBankContext);
                SetTabsActive(provider.Key, provider.Value);
            });

            _isOffersExist = jOffersValidationController.HasValidOffers;
            SetActivePage((BankTabType)param);

            View.SoftCurrencyText.text = PlayerProfile.PlayerResources.SoftCurrency.ToString();
            View.HardCurrencyText.text = PlayerProfile.PlayerResources.HardCurrency.ToString();

            DOVirtual.Float(1, 0, 0.75f, (f) => View.ScrollBar.value = f);
            jSignalOnSoftCurrencyStateChanged.AddListener(UpdateSoftCurrency);
            jSignalOnHardCurrencyStateChanged.AddListener(UpdateHardCurrency);
            jPurchaseOfferFinishedSignal.AddListener(View.Hide);
            jSignalLoadingSpinner.AddListener(EnableLoadingSpinner);
        }

        protected override void OnHidden()
        {
            base.OnHidden();
            jOnHudBottomPanelIsVisibleSignal.Dispatch(true);
            View.BuyProductClicked -= OnBuyProductClicked;

            jSignalOnSoftCurrencyStateChanged.RemoveListener(UpdateSoftCurrency);
            jSignalOnHardCurrencyStateChanged.RemoveListener(UpdateHardCurrency);
            jPurchaseOfferFinishedSignal.RemoveListener(View.Hide);
            jSignalLoadingSpinner.RemoveListener(EnableLoadingSpinner);

            if (_timers.Any())
            {
                foreach (var timer in _timers)
                {
                    timer.Value?.Dispose();
                }

                _timers.Clear();
            }
        }

        private void SetTabsActive(BankTabType type, IPurchaseProvider provider)
        {
            View.TabsContent.offsetMin = new Vector2(0f, View.TabsContent.offsetMin.y);

            var bankTab = View.Tabs.First(tab => tab.type == type);
            var products = provider.GetProducts().ToList();

            if (type == BankTabType.Offers)
            {

                if (products.Any())
                {
                    _offersTab = bankTab;
                    _offersProvider = provider;
                    InitializeOffers(bankTab, products.Cast<InAppShopOffer>());
                }

                ResizeOffersTab();
            }
            else
            {
                bankTab.itemsPool.InstanceProvider = new PrefabInstanceProvider(bankTab.prefabs[0]);
                InitializeItems(bankTab, products);
            }
        }

        private void InitializeItems(BankView.BankTab bankTab, IEnumerable<IBankProduct> products)
        {
            var sprites = bankTab.sprites;
            int itemIndex = default;

            foreach (var product in products)
            {
                View.InitItem(bankTab, itemIndex, sprites, product);
                itemIndex++;
            }
        }

        private void InitializeOffers(BankView.BankTab tab, IEnumerable<InAppShopOffer> products)
        {
            foreach (var product in products)
            {
                var productView = View.GetView(tab, product.UiStyle);
                productView.transform.SetParent(tab.poolParent, false);
                productView.transform.SetSiblingIndex(0);

                bool isNanoPass = false;
                //Prepare sprites required to display rewards of current offer
                var sprites = new Dictionary<int, Sprite>();

                for (var i = 0; i < product.Reward.Length; i++)
                {
                    switch (product.Reward[i].Type)
                    {
                        case RewardType.Soft:
                            sprites.Add(i, tab.sprites[0]);
                            break;
                        case RewardType.Hard:
                            sprites.Add(i, tab.sprites[1]);
                            break;
                        case RewardType.Products:
                            sprites.Add(i, jResourcesManager.GetProductSprite(product.Reward[i].Id));
                            break;
                        case RewardType.Gold:
                            sprites.Add(i, jResourcesManager.GetProductSprite(GoldId));
                            break;
                        case RewardType.Lanterns:
                            sprites.Add(i, tab.sprites[2]);
                            break;
                        case RewardType.NanoPass:
                            sprites.Add(i, tab.sprites[3]);
                            isNanoPass = true;
                            break;
                    }
                }

                productView.SetSprites(sprites);
                productView.Initialize(product, null);
                NetworkUtils.DownloadOfferSprite(product.IconId, productView.SetImage);

                if (isNanoPass)
                {
                    productView.BuyBundleClick += OnExpandNanoPassClicked;
                }
                else
                {
                    productView.BuyBundleClick += View.OnBuyBundleClicked; //The subscription will be removed when the item releases to the pool
                }

                InitOfferTimer(productView, product.FinishTime);
                tab.items.Add(productView);
            }
        }

        private void EnableLoadingSpinner(bool enable)
        {
            if (enable)
            {
                View.Spinner.StartSpin();
            }
            else
            {
                View.Spinner.StopSpin();
            }
        }

        public void OnExpandNanoPassClicked(IBankProduct product)
        {
            var settings = new NanoPassPopupSettings(product, _offersProvider)
            {
                EnableBackground = false
            };

            jPopupManager.Show(settings);
        }

        private void OnOfferClick(IBankProduct _)
        {
            jDisableOffersButtonSignal.Dispatch();
        }

        private void InitOfferTimer(BankOfferView view, long finishTime)
        {
            if (finishTime != 0)
            {
                var dtDateTime = DateTimeUtils.UnixStartTime.AddSeconds(finishTime).ToLocalTime();
                var seconds = (dtDateTime - DateTime.Now).TotalSeconds;
                
                view.EnableTimer(UiUtilities.GetFormattedTimeFromSecondsShorted((int) seconds));
                
                var timer = jTimeManager.StartServerTimer((float) seconds, () =>
                {
                    view.Release();
                    ResizeOffersTab();
                }, elapsed => { view.SetTimerText(UiUtilities.GetFormattedTimeFromSecondsShorted((int) (seconds - elapsed))); });
                _timers.Add(view, timer);
            }
        }

        private void ResizeOffersTab()
        {
            if (jOffersValidationController.HasValidOffers == false || !_offersProvider.GetProducts().Any())
            {
                View.BankTabPanels.First(t => t.type == BankTabType.Offers).panelTransform.gameObject.SetActive(false);
            }
            else
            {
                SetHorizontalLayoutPanelSize(_offersTab, _offersProvider.GetProducts(), View.OffersTabPanel);
            }
        }

        private void SetHorizontalLayoutPanelSize(BankView.BankTab tab, IEnumerable<IBankProduct> products, RectTransform panel)
        {
            var itemWidth = tab.itemRectTransform.sizeDelta.x;
            var countItems = products.Count();
            var gridSpacing = tab.gridLayout.spacing;
            var width = countItems * itemWidth + gridSpacing * countItems;
            panel.sizeDelta = new Vector2(width, View.OffersTabPanel.rect.height);
            
            LayoutRebuilder.ForceRebuildLayoutImmediate(View.TabsContent);
        }

        private void SetActivePage(BankTabType tab)
        {
            SortTabs(_isOffersExist, tab);
        }

        public void SortTabs(bool isOffersExist, BankTabType type)
        {
            // Tabs sort order:
            // Case 1, store opened via Offers button :  Offers, NanoBucks , Coins
            // Case 2, store opened via NanoBucks button :  Offers, NanoBucks , Coins
            // Case 3, store opened via Coins button :  Offers, Coins, NanoBucks 

            void SetAsLastSibling(BankTabType bankType)
            {
                View.BankTabPanels.First(panel => panel.type == bankType).panelTransform.SetAsLastSibling();
            }

            switch (type)
            {
                case BankTabType.Hard:
                    SetAsLastSibling(BankTabType.Soft);
                    break;
                case BankTabType.Soft:
                    SetAsLastSibling(BankTabType.Hard);
                    break;
                case BankTabType.Offers:
                    SetAsLastSibling(BankTabType.Soft);
                    break;
            }
            
            EnableOfferTab(isOffersExist);
        }

        private void EnableOfferTab(bool isOffersExist)
        {
            var offerBankTabPanel = View.BankTabPanels.FirstOrDefault(t => t.type == BankTabType.Offers);
            offerBankTabPanel?.panelTransform.gameObject.SetActive(isOffersExist);
            offerBankTabPanel?.panelTransform.SetAsFirstSibling();
        }

        private void OnBuyProductClicked(IBankProduct product)
        {
            switch (product)
            {
                case SoftCurrencyProduct _:
                    _providers[BankTabType.Soft].BuyProductClicked(product);
                    break;
                case InAppShopProduct _:
                    _providers[BankTabType.Hard].BuyProductClicked(product);
                    break;
                case InAppShopOffer _:
                    _providers[BankTabType.Offers].BuyProductClicked(product);
                    break;
            }
        }

        private void UpdateSoftCurrency(int _)
        {
            View.SoftCurrencyText.text = PlayerProfile.PlayerResources.SoftCurrency.ToString();
        }

        private void UpdateHardCurrency(int _)
        {
            View.HardCurrencyText.text = PlayerProfile.PlayerResources.HardCurrency.ToString();
        }
    }
}