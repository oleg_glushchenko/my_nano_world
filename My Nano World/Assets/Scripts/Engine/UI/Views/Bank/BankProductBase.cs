using strange.extensions.pool.api;
using System;
using UnityEngine;

namespace NanoReality.GameLogic.Bank
{
    public abstract class BankProductBase : MonoBehaviour, IPoolable
    {
        public abstract void Initialize(IBankProduct model, Sprite sprite);

        public abstract event Action<IBankProduct> BuyBundleClick;
        public abstract bool retain { get; }
        public abstract void Release();
        public abstract void Restore();
        public abstract void Retain();
    }
}