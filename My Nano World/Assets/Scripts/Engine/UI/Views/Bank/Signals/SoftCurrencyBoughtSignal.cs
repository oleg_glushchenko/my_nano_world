﻿using strange.extensions.signal.impl;

namespace NanoReality.GameLogic.Bank
{
	public class SoftCurrencyBoughtSignal : Signal<IBankProduct>
	{
	}
}