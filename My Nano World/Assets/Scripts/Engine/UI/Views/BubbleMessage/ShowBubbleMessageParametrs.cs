﻿using System;
using strange.extensions.signal.impl;
using UnityEngine;

namespace Engine.UI.Views.BubbleMessage
{
    public struct ShowBubbleMessageParametrs
    {
        public BubbleMessageType MessageType;
        public string Message;
        public Transform Anchor;
        public Vector3 AnchorShift;
        public float Time;
        public Action ClickCallback;
        public Action HideCallback;

        public ShowBubbleMessageParametrs(BubbleMessageType messageType,
            string message, Transform anchor, Vector2 anchorShift = new Vector2(), 
            float time = 5f,
            Action clickCallback = null, Action hideCallback = null)
        {
            MessageType = messageType;
            Message = message;
            Anchor = anchor;
            AnchorShift = anchorShift;
            ClickCallback = clickCallback;
            HideCallback = hideCallback;
            Time = time;
        }
    }
}