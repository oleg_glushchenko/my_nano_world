﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using strange.extensions.signal.impl;

namespace Assets.Scripts.Engine.UI.Views.BubbleMessage
{
    public class HideBubbleMessageSignal : Signal<HideBubbleMessageParameters>
    {
    }
}
