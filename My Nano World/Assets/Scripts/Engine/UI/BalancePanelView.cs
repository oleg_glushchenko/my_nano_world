﻿using System;
using Assets.NanoLib.UI.Core.Views;
using Engine.UI.Components;
using NanoReality.Engine.UI.UIAnimations;
using NanoReality.Game.Ui.Hud;
using NanoReality.UI.Components;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Views.Panels
{
    public class BalancePanelView : UIPanelView
    {
        #region Inspector fields

        [Header("Transforms")] 
        [SerializeField] private RectTransform _softCurrency;
        [SerializeField] private RectTransform _levelPanel;
        [SerializeField] private RectTransform _happinessPanel;
        [SerializeField] private RectTransform _populationHappinessPanel;
        [SerializeField] private RectTransform _hardCurrency;
        [SerializeField] private RectTransform _warehousePanel;
        [SerializeField] private RectTransform _balancePanel;
        [SerializeField] private RectTransform _goldCurrency;

        [Header("Texts")] 
        [SerializeField] private TextCounterEffect _softCurrencyText;
        [SerializeField] private TextMeshProUGUI _userLevelCounter;
        [SerializeField] private TextCounterEffect _hardCurrencyText;
        [SerializeField] private TextCounterEffect _goldCurrencyText;
        [SerializeField] private TextMeshProUGUI _warehouseCounter;
        [SerializeField] private TextMeshProUGUI _populationCounter;
        [SerializeField] private TextMeshProUGUI _happinessCounter;

        [Header("Buttons")] 
        [SerializeField] private Button _warehouseTooltipButton;
        [SerializeField] private Button _addSoftCurrencyButton;
        [SerializeField] private Button _addHardCurrencyButton;
        [SerializeField] private Button _infoNanoGemsButton;
        [SerializeField] private Button _happinessTooltipButton;
        [SerializeField] private Button _profileTooltipButton;
        [SerializeField] private Button _foodSupplyTooltipButton;
        [SerializeField] private Button _overlayBalanceButton;
        [SerializeField] private Button _closeButton;
        [SerializeField] private UiTooltipPanelView _happinessTooltip;
        [SerializeField] private UiTooltipPanelView _populationTooltip;
        [SerializeField] private HudTooltipProfile _profileTooltip;
        [SerializeField] private HudFoodSupplyTooltip _foodSupplyTooltip;

        [Header("Sprites")] [SerializeField] private ProgressBar _experienceBar;
        [SerializeField] private ProgressBar _warehouseBar;
        [SerializeField] private SpriteChanger _happinessSprite;
        [SerializeField] private ProgressBar _foodSupplyProgressBar;

        // TODO: need remove this tooltip serialized fields ASAP. And calculate tooltip position from _tooltipButton position in Canvas Rect.
        [SerializeField] private RectTransform _profileTooltipPlaceholder;
        [SerializeField] private RectTransform _happinessTooltipPlaceholder;
        [SerializeField] private RectTransform _warehouseTooltipPlaceholder;
        [SerializeField] private RectTransform _foodSupplyTooltipPlaceholder;
        [SerializeField] private RectTransform _foodSupplyTransform;
        [SerializeField] private CanvasGroup _canvasGroup;

        public RectTransform ProfileTooltipPlaceholder => _profileTooltipPlaceholder;
        public RectTransform HappinessTooltipPlaceholder => _happinessTooltipPlaceholder;
        public RectTransform WarehouseTooltipPlaceholder => _warehouseTooltipPlaceholder;
        public RectTransform FoodSupplyTooltipPlaceholder => _foodSupplyTooltipPlaceholder;

        #endregion Inspector fields

        #region Properties
        
        public RectTransform BalancePanel => _balancePanel;
        public RectTransform SoftCurrency => _softCurrency;
        public RectTransform HardCurrency => _hardCurrency;
        public RectTransform GoldCurrency => _goldCurrency;
        public RectTransform FoodSupplyTransform => _foodSupplyTransform;
        public RectTransform WarehousePanel => _warehousePanel;
        public RectTransform LevelPanel => _levelPanel;
        public RectTransform HappinessPanel => _happinessPanel;
        public RectTransform PopulationHappinessPanel => _populationHappinessPanel;

        public UiTooltipPanelView HappinessTooltip => _happinessTooltip;
        public UiTooltipPanelView PopulationTooltip => _populationTooltip;
        public HudTooltipProfile ProfileTooltip => _profileTooltip;
        public HudFoodSupplyTooltip FoodSupplyTooltip => _foodSupplyTooltip;

        public Button LevelButton => _profileTooltipButton;
        public Button OverlayBalanceButton => _overlayBalanceButton;
        public Button WarehouseButton => _warehouseTooltipButton;
        public Button WarehouseTooltipButton => _warehouseTooltipButton;
        public Button AddSoftCurrencyButton => _addSoftCurrencyButton;
        public Button AddHardCurrencyButton => _addHardCurrencyButton;
        public Button InfoNanoGemsButton => _infoNanoGemsButton;
        public Button ProfileTooltipButton => _profileTooltipButton;
        public Button HappinessTooltipButton => _happinessTooltipButton;
        public Button FoodSupplyButton => _foodSupplyTooltipButton;
        
        public CanvasGroup CanvasGroup => _canvasGroup;

        private const float MaxHappiness = 100f;

        #endregion Properties
        
        private IDisposable _softCurrencyDisposable;
        private IDisposable _hardCurrencyDisposable;
        private IDisposable _goldCurrencyDisposable;
        private IDisposable _userLevelDisposable;
        private IDisposable _goldBarDisposable;
        private IDisposable _happinessValueDisposable;
        private IDisposable _foodSupplyClick;
        private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();
      
        public override void Initialize(object parameters)
        {
            base.Initialize(parameters);
            transform.SetAsFirstSibling();
        }

        public void Init(BalancePanelModel model)
        {
            _softCurrencyDisposable = model.SoftCurrency.Subscribe(coins => _softCurrencyText.text = coins);
            _hardCurrencyDisposable = model.HardCurrency.Subscribe(coins => _hardCurrencyText.text = coins);
            _goldCurrencyDisposable = model.GoldCurrency.Subscribe(goldAmount => _goldCurrencyText.text = goldAmount);
            _userLevelDisposable = model.UserLevel.Subscribe(level => _userLevelCounter.text = level.ToString());
            _goldBarDisposable = model.GoldBarAvailable.Subscribe(state => GoldCurrency.gameObject.SetActive(state));
            
            _happinessValueDisposable = model.HappinessValue.Subscribe(value =>
            {
                _happinessCounter.text = value + "%";
                _happinessSprite.Value = value / MaxHappiness;
            });

            model.WarehouseTextValue.Subscribe(value => _warehouseCounter.text = value).AddTo(_compositeDisposable);
            model.WarehouseProgressBar.Subscribe(value => _warehouseBar.Value = value).AddTo(_compositeDisposable);
            model.PopulationTextValue.Subscribe(value => _populationCounter.text = value).AddTo(_compositeDisposable);
            model.ExperienceСorrelation.Subscribe(value => _experienceBar.Value = value).AddTo(_compositeDisposable);
            model.FoodSupplyValue.Subscribe(value => _foodSupplyProgressBar.Value = value).AddTo(_compositeDisposable);

            model.SoftClick.BindTo(_addSoftCurrencyButton).AddTo(_compositeDisposable);
            model.HardClick.BindTo(_addHardCurrencyButton).AddTo(_compositeDisposable);
            model.InfoPanelClick.BindTo(_profileTooltipButton).AddTo(_compositeDisposable);
            model.WarehouseClick.BindTo(_warehouseTooltipButton).AddTo(_compositeDisposable);
           // model.HappyClick.BindTo(_happinessTooltipButton).AddTo(_compositeDisposable);
            model.FoodSupplyClick.BindTo(_foodSupplyTooltipButton).AddTo(_compositeDisposable);
            model.OverlayClick.BindTo(_overlayBalanceButton).AddTo(_compositeDisposable);
            model.CloseButton.BindTo(_closeButton).AddTo(_compositeDisposable);
        }

        public void UnsubscribeBalancePanel()
        {
            _softCurrencyDisposable.Dispose();
            _hardCurrencyDisposable.Dispose();
            _goldCurrencyDisposable.Dispose();
            _userLevelDisposable.Dispose();
            _goldBarDisposable.Dispose();
            _happinessValueDisposable.Dispose();
            _compositeDisposable.Clear();
        }
    }
}
