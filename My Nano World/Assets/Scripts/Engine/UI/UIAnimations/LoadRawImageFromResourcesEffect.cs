﻿using NanoReality.Engine.UI.UIAnimations;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Эффект анимации, загружающий изображение из ресурсов
/// </summary>
public class LoadRawImageFromResourcesEffect : BaseSpecialEffect
{
    #region Layout

#pragma warning disable 649

    /// <summary>
    /// Путь к текстуре
    /// </summary>
    [SerializeField] private string _texturePatch;

    /// <summary>
    /// Текстура, в которую будет помещено изображение, загруженное из ресурсов
    /// </summary>
    [SerializeField] private RawImage _textureRawImage;

#pragma warning restore 649

    #endregion

    #region Fields

    /// <summary>
    /// Текстура хранится в статике, так как её нужно позже выгружать. Она
    /// общая для всех
    /// </summary>
    private static Texture _loadedTexture;

    #endregion

    #region Methods

    public override void Play()
    {
        base.Play();

        UnloadTexture();

        if (!string.IsNullOrEmpty(_texturePatch))
        {
            _loadedTexture = Resources.Load(_texturePatch) as Texture;
            _textureRawImage.texture = _loadedTexture;
        }

        Stop();
    }

    private void OnDisable()
    {
        UnloadTexture();
    }

    private void UnloadTexture()
    {
        _textureRawImage.texture = null;

        if (_loadedTexture != null)
        {
            Resources.UnloadAsset(_loadedTexture);
            _loadedTexture = null;
        }
    }

    #endregion
}