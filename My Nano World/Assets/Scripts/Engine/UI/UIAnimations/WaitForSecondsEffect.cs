﻿using Assets.NanoLib.UtilitesEngine;
using UnityEngine;

#pragma warning disable 649

// ReSharper disable once CheckNamespace
namespace NanoReality.Engine.UI.UIAnimations
{
    /// <summary>
    /// Эффект скейла объекта
    /// </summary>
    public class WaitForSecondsEffect : BaseSpecialEffect
    {
        /// <summary>
        /// Время проигрывания анимации в секундах
        /// </summary>
        [SerializeField] private float _animationPlayTime = 0.3f;

        /// <summary>
        /// Текущее время проигрывания анимации
        /// </summary>
        private float _currTime;

        /// <summary>
        /// Флаг показывает, закончилось ли проигрывание текущего эффекта (нужно для старта проигрывания вложенных объектов)
        /// </summary>
        private bool _isEnd;

        public override void Play()
        {
            _isEnd = false;
            _isSubEffectsStarted = false;
            _currTime = 0;

            base.Play();
        }

        public override void PlayReverse()
        {
            _isEnd = false;
            _isSubEffectsStarted = false;
            _currTime = 0;

            base.PlayReverse();
        }

        public override void Stop()
        {
            if (SubEffectsPlayMode == SubEffectsPlayMode.WithParent)
            {
                base.Stop();
                return;
            }

            _isPlaying = false;
            effectStopped.Dispatch(this);
        }

        private bool _isSubEffectsStarted;

        // ReSharper disable once UnusedMember.Local
        private void Update()
        {
            if (!IsPlaying)
                return;

            _currTime += Time.deltaTime;

            if (_currTime >= _animationPlayTime)
            {
                _isEnd = true;
                Stop();
            }

            // если нужно после окончания основной анимации проиграть вложенные
            if (SubEffectsPlayMode != SubEffectsPlayMode.AfterParent)
                return;

            if (_isSubEffectsStarted || !_isEnd)
                return;

            _isSubEffectsStarted = true;

            foreach (var effect in subEffects)
                if (effect != null)
                    effect.Play();
        }
    }
}