﻿#pragma warning disable 649

namespace NanoReality.Engine.UI.UIAnimations
{
    public class UIPanelShowEffect : BaseSpecialEffect
    {
        public override void Play()
        {
            if (IsPlaying) return;

            base.Play();

            if (subEffects.Count == 0)
            {
                Stop();
            }
        }
    }
}