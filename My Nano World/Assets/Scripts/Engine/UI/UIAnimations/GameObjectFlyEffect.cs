﻿using System;
using Assets.NanoLib.UtilitesEngine;
using NanoReality.GameLogic.Utilities;
using UnityEngine;

#pragma warning disable 649


namespace NanoReality.Engine.UI.UIAnimations
{
    public class GameObjectFlyEffect : BaseSpecialEffect
    {
        public event Action Completed;

        public Transform Target;

        public Transform Destination;

        public AnimationCurve CurveX;
        public AnimationCurve CurveY;

        public bool UnclampedCurve;

        [SerializeField] protected float _speed;

        protected float _startTime;

        protected Vector3 StartPosition;
        protected Vector3 EndPosition;

        private bool _isCustomSpeedSet;
        private float _customSpeed;

        public float Speed
        {
            get { return _speed; }
        }

        public override void Play()
        {
            var rect = Destination.transform as RectTransform;
            var offset = Vector3.zero;

            if (rect != null)
            {
                if (rect.pivot.x < 0.5f)
                {
                    offset.x += rect.rect.width / 4f;
                }
                else if (rect.pivot.x > 0.5f)
                {
                    offset.x -= rect.rect.width / 4f;
                }
            }

            StartPosition = Target.position;
            EndPosition = Destination.position + offset;
            EndPosition.z = Target.position.z;
            _startTime = Time.time;

            base.Play();
        }

        protected virtual void Update()
        {
            if (!IsPlaying)
                return;

            float distCovered = (Time.time - _startTime + Time.deltaTime) *
                                (_isCustomSpeedSet ? _customSpeed : _speed);

            float x = CurveX.Evaluate(distCovered);
            float y = CurveY.Evaluate(distCovered);

            float posX;
            float posY;

            if (UnclampedCurve)
            {
                posX = Mathf.LerpUnclamped(StartPosition.x, EndPosition.x, x);
                posY = Mathf.LerpUnclamped(StartPosition.y, EndPosition.y, y);
            }
            else
            {
                posX = Mathf.Lerp(StartPosition.x, EndPosition.x, x);
                posY = Mathf.Lerp(StartPosition.y, EndPosition.y, y);
            }
            

            Target.position = new Vector3(posX, posY, Target.position.z);

            if (Vector3.Distance(Target.position, EndPosition) < 0.1f)
            {
                Stop();
                Completed.SafeRaise();
            }
        }

        public void SetCustomSpeedOnce(float customSpeed)
        {
            _isCustomSpeedSet = true;
            _customSpeed = customSpeed;
        }

        public override void Stop()
        {
            _isCustomSpeedSet = false;
            _customSpeed = Speed;

            base.Stop();
        }
    }
}