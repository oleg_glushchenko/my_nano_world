﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

#pragma warning disable 649

namespace NanoReality.Engine.UI.UIAnimations
{
    public class TextTwoValuesCounterEffect : MonoBehaviour
    {
        [SerializeField]
        protected TextCounterEffect ValueLabel;
        
        [SerializeField]
        protected TextMeshProUGUI Separator;
        
        [SerializeField]
        protected TextCounterEffect SecondValueLabel;
        

        public string Text
        {
            get
            {
                if (Separator != null)
                    return Separator.text;
                return "";
            }
            set
            {
                if (Separator != null)
                    Separator.text = value;
            }
        }

        public string Value
        {
            get
            {
                if (ValueLabel != null)
                    return ValueLabel.text.ToString();
                return "";
            }
            set
            {
                if (ValueLabel != null)
                    ValueLabel.text = int.Parse(value);
            }
        }

        public string SecondValue
        {
            get
            {
                if (SecondValueLabel != null)
                    return SecondValueLabel.text.ToString();
                return "";
            }
            set
            {
                if (SecondValueLabel != null)
                    SecondValueLabel.text = int.Parse(value);
            }
        }

        public void SetFirstValueColor(Color color)
        {
            ValueLabel._textComponent.color = color;
        }

        public void SetSecondValueColor(Color color)
        {
            SecondValueLabel._textComponent.color = color;
        }
    }
}
