﻿using System.Collections;
using System.Collections.Generic;
using NanoReality.Engine.UI.UIAnimations;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using DG.Tweening;

public class DelayFadeAlphaEffect : BaseSpecialEffect
{
    [SerializeField] private float _duration = 1; 
    [SerializeField] private float _delay = 0; 
    [SerializeField] private CanvasGroup _target;

    public override void Play()
    {
        _target.DOFade(1, _duration).ChangeStartValue(0).SetDelay(_delay);
        base.Play();
    }
    
    public override void PlayReverse()
    {
        _target.DOFade(0, _duration);
        base.PlayReverse();
    }

}
