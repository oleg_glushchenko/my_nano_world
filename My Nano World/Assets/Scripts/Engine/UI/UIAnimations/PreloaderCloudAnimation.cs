﻿using Assets.NanoLib.UI.Core.Views;
using UnityEngine;

#pragma warning disable 649

namespace NanoReality.Engine.UI.UIAnimations
{
    public class PreloaderCloudAnimation : BaseSpecialEffect
    {
        [SerializeField]
        private UIPanelView _preloaderPanel;

        private bool _isFirstLaunch = true;


        public override void Initialize()
        {
            base.Initialize();
            foreach (var effect in SubEffects)
            {
                effect.Initialize();
            }
        }

        public override void Play()
        {
            if (_isFirstLaunch)
            {
                _isFirstLaunch = false;
                Initialize();
                _isPlaying = true;
                Stop();
                return;
            }
            base.Play();
        }
    }
}
