﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.Utilities;
using Assets.NanoLib.Utilities.Pulls;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.UI.Components;
using UnityEngine;

namespace NanoReality.Engine.UI.UIAnimations
{
    /// <summary>
    /// Visual effect for worldSpace 
    /// </summary>
    public class ParticleSpecialEffect : BaseSpecialEffect, IPullableObject
    {
        [SerializeField] protected List<ParticleSystem> ParticleSystemList;

        [SerializeField] private Transform _particlesContainer;

        [SerializeField] private bool _bNeedShapeSetup;

        [SerializeField] private bool _bFreeObjOnComplete;

        protected ParticleSystem DefaultParticleSystem
        {
            get { return ParticleSystemList.FirstOrDefault(); }
        }

        private CacheMonobehavior _cacheMonobehavior;

        protected CacheMonobehavior CacheMonobehavior
        {
            get { return _cacheMonobehavior ?? (_cacheMonobehavior = new CacheMonobehavior(this)); }
        }

        private Coroutine _releaseCoroutine;

        protected virtual IEnumerator EReleaseCoroutine()
        {
            yield return new WaitForSeconds(ParticleSystemList.Max(ps => ps.main.duration));
            Stop();
        }

        public override void Play()
        {
            gameObject.SetActive(true);
            if (_isPlaying)
            {
                ParticleSystemList.ForEach(partSystem => partSystem.Stop());
            }

            ParticleSystemList.ForEach(partSystem => partSystem.Play());

            base.Play();

            if (_bFreeObjOnComplete)
            {
                _releaseCoroutine = StartCoroutine(EReleaseCoroutine());
            }
        }

        public override void Stop()
        {
            gameObject.SetActive(false);

            ParticleSystemList.ForEach(partSystem => partSystem.Stop());

            FreeObject();

            base.Stop();
        }

        public void SetTargetCustomRotation(Vector3 targetCustomRotation)
        {
            var looker = CacheMonobehavior.GetCacheComponent<MapObjectLooker>();
            looker.TargetCustomRotation = targetCustomRotation;
            looker.UpdateLookerPosition();
        }

        public void SetCustomScale(Vector3 targetCustomScale)
        {
            var looker = CacheMonobehavior.GetCacheComponent<MapObjectLooker>();
            looker.TargetCustomScale = targetCustomScale;
            looker.UpdateLookerPosition();
        }

        public virtual void SetTarget(MapObjectView target)
        {
            var looker = CacheMonobehavior.GetCacheComponent<MapObjectLooker>();
            looker.SetTarget(target);
            looker.UpdateLookerPosition();

            if (_bNeedShapeSetup)
            {
                SetCorrectShape(target.GridDimensions);
            }
        }

        //check size of the needed target and set correct size
        protected virtual void SetCorrectShape(Vector2 targetDimension)
        {
            if (targetDimension.x <= 1)
            {
                _particlesContainer.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            }
            else if (targetDimension.x <= 3)
            {
                _particlesContainer.localScale = new Vector3(0.9f, 0.9f, 0.9f);
            }
            else if (targetDimension.x <= 5)
            {
                _particlesContainer.localScale = new Vector3(1.2f, 1.2f, 1.2f);
            }
            else
            {
                _particlesContainer.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            }
        }


        #region IPullable

        public object Clone()
        {
            return Instantiate(this);
        }

        public IObjectsPull pullSource { get; set; }

        public virtual void OnPop()
        {
            gameObject.SetActive(true);

            enabled = true;
            var looker = CacheMonobehavior.GetCacheComponent<MapObjectLooker>();

            if (looker != null)
            {
                looker.enabled = true;
            }
        }

        public virtual void OnPush()
        {
            gameObject.SetActive(false);

            if (_releaseCoroutine != null)
            {
                StopCoroutine(_releaseCoroutine);
                _releaseCoroutine = null;
            }

            enabled = false;

            var looker = CacheMonobehavior.GetCacheComponent<MapObjectLooker>();
            if (looker != null)
            {
                looker.enabled = false;
                looker.SetTarget(null);
                looker.TargetCustomOffset = Vector3.zero;
                looker.TargetCustomRotation = Vector3.zero;
                looker.TargetCustomScale = Vector3.one;
            }
        }

        public void FreeObject()
        {
            gameObject.SetActive(false);

            if (pullSource != null)
                pullSource.PushInstance(this);
        }

        public void DestroyObject()
        {
            Destroy(this);
        }

        #endregion
    }
}