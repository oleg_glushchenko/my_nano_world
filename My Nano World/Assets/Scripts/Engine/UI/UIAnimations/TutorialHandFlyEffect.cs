﻿using UnityEngine;
using UnityEngine.UI;

#pragma warning disable 649


namespace NanoReality.Engine.UI.UIAnimations
{
    public class TutorialHandFlyEffect : MonoBehaviour
    {
        public Transform Target;
        public Transform Destination;
        public Image HandImage;

        protected void LateUpdate()
        {
            var posX = Destination.position.x;
            var posY = Destination.position.y;

            var tolerance = 0.1f;

            var offsetX = Screen.width * tolerance;
            var offsetY = Screen.height * tolerance;

            posX = Mathf.Clamp(posX, offsetX, Screen.width - offsetX);
            posY = Mathf.Clamp(posY, offsetY, Screen.height - offsetY);

            Target.position = new Vector3(posX, posY, Destination.position.z);

            var dir = Destination.position - transform.position;
//            var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg - 90f;
            var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg + 90f;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

            HandImage.enabled = Vector3.Distance(Target.position, Destination.position) > 200;
        }
    }
}