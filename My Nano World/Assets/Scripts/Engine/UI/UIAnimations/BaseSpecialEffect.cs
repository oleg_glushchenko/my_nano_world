﻿using UnityEngine;
using System.Collections.Generic;
using strange.extensions.signal.impl;

// ReSharper disable InconsistentNaming

namespace NanoReality.Engine.UI.UIAnimations
{
	public class BaseSpecialEffect : MonoBehaviour, ISpecialEffect
    {
        protected readonly Signal<ISpecialEffect> effectStarted = new Signal<ISpecialEffect>();
        protected readonly Signal<ISpecialEffect> effectStopped = new Signal<ISpecialEffect>();
        protected readonly Signal<ISpecialEffect> effectCanceled = new Signal<ISpecialEffect>();

        protected bool _isPlaying;

        [SerializeField] protected SpecialEffectMode _effectMode;
        
        [SerializeField] protected bool PlayOnStart;
        [SerializeField] protected bool CancelOnDisable = true;

        [SerializeField] protected SubEffectsPlayMode SubEffectsPlayMode;

        [SerializeField] protected List<BaseSpecialEffect> subEffects = new List<BaseSpecialEffect>();

        public Signal<ISpecialEffect> EffectStarted => effectStarted;

        public Signal<ISpecialEffect> EffectStopped => effectStopped;

        public Signal<ISpecialEffect> EffectCanceled => effectCanceled;

        public SpecialEffectMode EffectMode
        {
            get => _effectMode;
            set => _effectMode = value;
        }

        public bool IsPlaying => _isPlaying;

        public float TimeoutStart { get; set; }

        public List<BaseSpecialEffect> SubEffects => subEffects;

        protected virtual void OnEnable()
        {
            if (PlayOnStart)
            {
                Play();
            }
        }

        private void OnDisable()
        {
            if (CancelOnDisable)
            {
                Cancel();
            }
        }

        public virtual void Initialize()
        {
            
        }

        public virtual void Play()
        { 
            _isPlaying = true;
            effectStarted.Dispatch(this);

            if (SubEffectsPlayMode == SubEffectsPlayMode.WithParent)
            {
                AddListeners();
                for (int i = 0; i < subEffects.Count; i++)
                {
                    subEffects[i].Play(); 
                }
            }
            else if (SubEffectsPlayMode == SubEffectsPlayMode.AfterParent)
            {
                for (int index = 0; index < subEffects.Count; index++)
                {
                    if (subEffects[index] != null)
                    {
                        subEffects[index].Initialize();
                    }
                }
            }
        }

        public virtual void PlayReverse()
        {
            _isPlaying = true;
            effectStarted.Dispatch(this);

            if (SubEffectsPlayMode == SubEffectsPlayMode.WithParent)
            {
                AddListeners();
                for (int i = 0; i < subEffects.Count; i++)
                {
                    subEffects[i].PlayReverse();
                }
            }
        }

        public virtual void Stop()
        {
            if (!_isPlaying) return;
            
            _isPlaying = false;

            if (SubEffectsPlayMode == SubEffectsPlayMode.WithParent)
            {
                RemoveListeners();
                for (int i = 0; i < subEffects.Count; i++)
                {
                    subEffects[i].Stop();
                }

                effectStopped.Dispatch(this);
            }
            else if(SubEffectsPlayMode == SubEffectsPlayMode.AfterParent)
            {
                AddListeners();
                for (int i = 0; i < subEffects.Count; i++)
                {
                    subEffects[i].Play();
                }
            }
        }

        public virtual void Cancel()
        {
            if (!_isPlaying) return;
            
            RemoveListeners();
            _isPlaying = false;
            effectCanceled.Dispatch(this);
            for (int i = 0; i < subEffects.Count; i++)
            {
                subEffects[i].Cancel();
            }
        }

        protected void AddListeners()
        {
            for (int i = 0; i < subEffects.Count; i++)
            {
                subEffects[i].EffectStarted.AddListener(SubEffectsStarted);
                subEffects[i].EffectStopped.AddListener(SubEffectsStopped);
                subEffects[i].EffectCanceled.AddListener(SubEffectsCanceled);
            }
        }

        protected void RemoveListeners()
        {
            for (int i = 0; i < subEffects.Count; i++)
            {
                subEffects[i].EffectStarted.RemoveListener(SubEffectsStarted);
                subEffects[i].EffectStopped.RemoveListener(SubEffectsStopped);
                subEffects[i].EffectCanceled.RemoveListener(SubEffectsCanceled);
            }
        }

        protected virtual void SubEffectsStarted(ISpecialEffect specialEffect)
        {
            
        }

        protected virtual void SubEffectsStopped(ISpecialEffect specialEffect)
        {
            Stop();
        }

        protected virtual void SubEffectsCanceled(ISpecialEffect specialEffect)
        {
            Cancel();
        }
    }
}