﻿using System;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace OldParticles
{
    [RequireComponent(typeof(ParticleSystem))]
    [Obsolete("will be removed in future, use UiParticles plugin insted")]
    public class UiParticles : Graphic
    {


        [FormerlySerializedAs("m_ParticleSystem")]
        [SerializeField]
        private ParticleSystem m_ParticleSystem;


        public ParticleSystem ParticleSystem { get { return m_ParticleSystem; }
            set { if (SetPropertyUtility.SetClass(ref m_ParticleSystem, value)) SetAllDirty(); } }








        [FormerlySerializedAs("m_ParticleSystemRenderer")]
        [SerializeField]
        private ParticleSystemRenderer m_ParticleSystemRenderer;

        public ParticleSystemRenderer particleSystemRenderer
        {
            get { return m_ParticleSystemRenderer; }
            set { if (SetPropertyUtility.SetClass(ref m_ParticleSystemRenderer, value)) SetAllDirty(); }
        }


        [FormerlySerializedAs("m_particlesTextureCurve")]
        [SerializeField]
        private AnimationCurve m_particlesTextureCurve;

        public AnimationCurve ParticlesTextureCurve
        {
            get { return m_particlesTextureCurve; }
            set { if (SetPropertyUtility.SetClass(ref m_particlesTextureCurve, value)) SetAllDirty(); }
        }





        public override Texture mainTexture
        {
            get
            {
                if (material != null && material.mainTexture != null)
                {
                    return material.mainTexture;
                }
                return s_WhiteTexture;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            ParticleSystem = GetComponent<ParticleSystem>();
            particleSystemRenderer = GetComponent<ParticleSystemRenderer>();
        }


        protected override void OnPopulateMesh(VertexHelper toFill)
        {
            if (ParticleSystem == null)
            {
                base.OnPopulateMesh(toFill);
                return;
            }
            GenerateParticlesBillboards(toFill);
        }






        ParticleSystem.Particle[] m_Particles;

        void InitParticlesBuffer()
        {
            if (m_Particles == null || m_Particles.Length < ParticleSystem.maxParticles)
                m_Particles = new ParticleSystem.Particle[ParticleSystem.maxParticles];
        }

        void GenerateParticlesBillboards(VertexHelper vh)
        {
            InitParticlesBuffer();
            int numParticlesAlive = ParticleSystem.GetParticles(m_Particles);

            vh.Clear();



            for (int i = 0; i < numParticlesAlive; i++)
            {
                DrawParticleBillboard(m_Particles[i], vh);
            }
        }


        private void DrawParticleBillboard(ParticleSystem.Particle particle, VertexHelper vh)
        {
            var center = particle.position; // if local space just take this as offset from object anchor

            if (ParticleSystem.simulationSpace == ParticleSystemSimulationSpace.World)
            {
                center -= rectTransform.position;
            }

            var size = particle.GetCurrentSize(ParticleSystem);

            var leftTop = new Vector3(-size*0.5f, size*0.5f);
            var rightTop = new Vector3(size*0.5f, size*0.5f);
            var rightBottom =  new Vector3(size*0.5f, -size*0.5f);
            var leftBottom = new Vector3(-size*0.5f, -size*0.5f);

            var rotation = Quaternion.Euler(0, 0, particle.rotation);
            leftTop = rotation*leftTop+center;
            rightTop = rotation * rightTop+center;
            rightBottom = rotation * rightBottom+center;
            leftBottom = rotation * leftBottom+center;

            Color32 color32 = particle.GetCurrentColor(ParticleSystem);
            var i = vh.currentVertCount;

            Vector2[] uvs = new Vector2[4];

            if (!ParticleSystem.textureSheetAnimation.enabled)
            {
                uvs[0] = new Vector2(0f, 0f);
                uvs[1] = new Vector2(0f, 1f);
                uvs[2] = new Vector2(1f, 1f);
                uvs[3] = new Vector2(1f, 0f);
            }
            else
            {
                var textureAnimator = ParticleSystem.textureSheetAnimation;
                float timeAlive = particle.startLifetime - particle.remainingLifetime;


                var frameNum = ParticlesTextureCurve.Evaluate(timeAlive/particle.startLifetime);
                frameNum *= textureAnimator.numTilesY*textureAnimator.numTilesX;
                frameNum = Mathf.Clamp(Mathf.Round(frameNum), 0, 3);
                int x = 0;
                int y = 0;
                int currframe = 0;
                bool outCycle = false;
                for (; y < textureAnimator.numTilesY; y++)
                {
                    x = 0;
                    for (; x < textureAnimator.numTilesX; x++)
                    {

                        if (currframe >= frameNum)
                        {
                            outCycle = true;
                            break;
                        }
                        currframe++;
                    }
                    if (outCycle) break;
                }

                var xDelta = 1f/textureAnimator.numTilesX;
                var yDelta = 1f/textureAnimator.numTilesY;
                y = (textureAnimator.numTilesY - 1) - y;
                var sX = x*xDelta;
                var sY = y*yDelta;
                var eX = sX + xDelta;
                var eY = sY + yDelta;

                uvs[0] = new Vector2(sX, sY);
                uvs[1] = new Vector2(sX, eY);
                uvs[2] = new Vector2(eX, eY);
                uvs[3] = new Vector2(eX, sY);
            }

            vh.AddVert(leftBottom, color32, uvs[0]);
            vh.AddVert(leftTop, color32, uvs[1]);
            vh.AddVert(rightTop, color32, uvs[2]);
            vh.AddVert(rightBottom, color32, uvs[3]);

            vh.AddTriangle(i, i + 1, i + 2);
            vh.AddTriangle(i + 2, i + 3, i);
        }

        protected virtual void Update()
        {
            SetVerticesDirty();
            if (particleSystemRenderer != null) particleSystemRenderer.enabled = false;
        }
    }

}

