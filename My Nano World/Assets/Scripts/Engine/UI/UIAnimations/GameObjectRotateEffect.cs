﻿using Assets.NanoLib.UtilitesEngine;
using UnityEngine;
#pragma warning disable 649

namespace NanoReality.Engine.UI.UIAnimations
{
    public class GameObjectRotateEffect : BaseSpecialEffect
    {
        public float RotationSpeed;       
        public Vector3 Angle;
        public bool Cicle;

        
        void Update()
        {
            if (!IsPlaying) return;

            transform.RotateAround(transform.position, Angle, Time.deltaTime * RotationSpeed);

            if (!Cicle && Vector3.Distance(transform.rotation.eulerAngles, Angle) < 0.1f)
            {
                Stop();
            }
        }
    }
}