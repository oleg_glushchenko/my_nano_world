﻿using System.Collections.Generic;
using strange.extensions.signal.impl;

namespace NanoReality.Engine.UI.UIAnimations
{
    /// <summary>
    /// Интерфейс, определяющий поведение эффектов и анимации для UI
    /// </summary>
    public interface ISpecialEffect
    {
        /// <summary>
        /// Вызывается, при запуске эффекта
        /// </summary>
        Signal<ISpecialEffect> EffectStarted { get; }

        /// <summary>
        /// Вызывается, при остановке эффекта
        /// </summary>
        Signal<ISpecialEffect> EffectStopped { get; }

        /// <summary>
        /// Вызывается, при отмене эффекта
        /// </summary>
        Signal<ISpecialEffect> EffectCanceled { get; }


        /// <summary>
        /// Режим работы анимации
        /// </summary>
        SpecialEffectMode EffectMode { get; set; }

        /// <summary>
        /// Проигрывается ли в данный момент анимация
        /// </summary>
        bool IsPlaying { get; }

        /// <summary>
        /// Задержка перед запуском эффекта
        /// </summary>
        float TimeoutStart { get; set; }

        /// <summary>
        /// Список дополнительных эффектов
        /// </summary>
        List<BaseSpecialEffect> SubEffects { get; }

        /// <summary>
        /// Используется для предварительной настройки эффекта
        /// </summary>
        void Initialize();

        /// <summary>
        /// Запускает проигрывание анимации
        /// </summary>
        void Play();

        /// <summary>
        /// Проигрывание анимаци в обратном порядке
        /// </summary>
        void PlayReverse();

        /// <summary>
        /// Останавливает проигрывание анимации
        /// </summary>
        void Stop();

        /// <summary>
        /// Отменяет проигрывание анимаци
        /// </summary>
        void Cancel();
    }

    /// <summary>
    /// Режим работы анимации
    /// </summary>
    public enum SpecialEffectMode
    {
        /// <summary>
        /// Простое, единоразовое проигрывание
        /// </summary>
        Once,

        /// <summary>
        /// Единоразовое проигрывание в обратную сторону
        /// </summary>
        OnceReverse,

        /// <summary>
        /// Циклически повторяемое проигрывание
        /// </summary>
        Loop,

        /// <summary>
        /// Проигрывание с повторением
        /// </summary>
        Repeat,

        /// <summary>
        /// Проигрывание с реверсом (с возвратом в исходное положение)
        /// </summary>
        WithReverse
    }

    /// <summary>
    /// Режим проигрывания вложенных спецэффектов
    /// </summary>
    public enum SubEffectsPlayMode
    {
        /// <summary>
        /// Вложенные эффекты проигрываются одновременно с родительским
        /// </summary>
        WithParent,

        /// <summary>
        /// Вложенные эффекты проигрываются после проигрывания с родительского
        /// </summary>
        AfterParent
    }
}