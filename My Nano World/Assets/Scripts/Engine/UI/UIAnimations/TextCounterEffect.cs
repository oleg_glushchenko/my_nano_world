﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

// ReSharper disable InconsistentNaming
#pragma warning disable 649

namespace NanoReality.Engine.UI.UIAnimations
{
    public class TextCounterEffect: BaseSpecialEffect
    {
        public TextMeshProUGUI _textComponent;
        public bool IsPercent;


        [SerializeField]
        private float _sleep;
        

        private int _newValue;
        private int _oldValue;

        private bool _isInitialized;
        private int _step;

        
        public int text 
        {
            get { return _oldValue; }
            set
            {
                if (!gameObject.activeInHierarchy || !_isInitialized)
                {
                    _isInitialized = true;
                    _oldValue = value;
                    if(_textComponent != null)
                    _textComponent.text = GetStringValue();
                    else
                    {
                        Debug.LogError("Text Component is null " + name);
                    }
                    return;
                }

                _newValue = value;
                Play();
            }
        }


        #region Mono/override

        protected override void OnEnable()
        {
            _isInitialized = false;
            base.OnEnable();
        }

        public override void Play()
        {
            if(IsPlaying) Stop();
            base.Play();

            var offset = Mathf.Abs(_oldValue - _newValue);
            _step = (int) (offset*_sleep*2) + 1;

            StartCoroutine(SetText());
        }

        public override void Stop()
        {
            StopCoroutine(SetText());
            base.Stop();
        }

        #endregion


        private string GetStringValue()
        {
            var str = _oldValue + (IsPercent ? "%" : "");
            if (_oldValue > 999) str = str.Insert(str.Length - 3, " ");
            return str;
        }

        IEnumerator SetText()
        {
            var increase = _oldValue < _newValue;

            while (_oldValue != _newValue)
            {
                if (increase)
                {
                    _oldValue += _step;
                    if (_oldValue > _newValue) _oldValue = _newValue;
                }
                else
                {
                    _oldValue -=_step;
                    if (_oldValue < _newValue) _oldValue = _newValue;
                }
                if(_textComponent != null)
                    _textComponent.text = GetStringValue();
                else
                {
                    Debug.LogError("Text Component is null " + name);
                }
                yield return new WaitForSeconds(_sleep);
            }

            Stop();
        }
    }
}