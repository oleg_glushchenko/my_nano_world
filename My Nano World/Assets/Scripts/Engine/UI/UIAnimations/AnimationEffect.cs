﻿using UnityEngine;

namespace NanoReality.Engine.UI.UIAnimations
{
    /// <summary>
    /// Play Animation clip as effect
    /// </summary>
    public class AnimationEffect : BaseSpecialEffect
    {
        /// <summary>
        /// Target Animation component
        /// </summary>
        public Animation TargetAnimation;

        /// <summary>
        /// Target Animation clip
        /// </summary>
        public AnimationClip TargetClip;

        /// <summary>
        /// Control Animation clip that is played when animation stops
        /// </summary>
        public AnimationClip ControlClip;

        public override void Play()
        {
            TargetAnimation.clip = TargetClip;
            TargetAnimation.Play();
            base.Play();
        }

        public override void PlayReverse()
        {
            TargetAnimation.clip = TargetClip;
            TargetAnimation[TargetClip.name].time = TargetClip.length;
            TargetAnimation[TargetClip.name].speed = -1;
            TargetAnimation.Play();
            base.PlayReverse();
        }

        public override void Stop()
        {
            TargetAnimation[TargetClip.name].time = TargetClip.length;
            TargetAnimation[TargetClip.name].speed = 1;
            TargetAnimation.Play(PlayMode.StopAll);
            TargetAnimation.Stop();

            if (ControlClip != null)
            {
                TargetAnimation.clip = ControlClip;
                TargetAnimation.Play();
            }
            
            base.Stop();
        }

        /// <summary>
        /// Calls when animation ended (by animation event)
        /// </summary>
        public void OnAnimationEnd()
        {
            AnimationState animationState = TargetAnimation[TargetClip.name];
            if (animationState != null && animationState.speed > 0)
            {
                Stop();
            }            
        }

        /// <summary>
        /// Called when reverse animation ended (by animation event)
        /// </summary>
        public void OnReverseAnimationEnd()
        {            
            AnimationState animationState = TargetAnimation[TargetClip.name];
            if (animationState != null && animationState.speed < 0)
            {
                Stop();
            }
        }
    }
}
