﻿using Assets.NanoLib.UtilitesEngine;
using UnityEngine;

#pragma warning disable 649

// ReSharper disable once CheckNamespace
namespace NanoReality.Engine.UI.UIAnimations
{
    public enum ScaleMode
    {
        Both = 0,
        Horizontal = 1,
        Vertical = 2
    }

    /// <summary>
    /// Эффект скейла объекта
    /// </summary>
    public class GameObjectScaleEffect : BaseSpecialEffect
    {
        [SerializeField]
        private Transform _target;

        /// <summary>
        /// Время проигрывания анимации в секундах
        /// </summary>
        [SerializeField]
        private float _animationPlayTime=0.3f;

        [SerializeField]
        private ScaleMode _scaleMode = ScaleMode.Both;

        /// <summary>
        /// Текущее время проигрывания анимации
        /// </summary>
        private float _currTime;

        /// <summary>
        /// Кривая скейла (от 0 до 1 секунды)
        /// </summary>
        public AnimationCurve ScaleAnimationCurve = AnimationCurve.Linear(0,0.3f,1,1);

        /// <summary>
        /// Флаг показывает, закончилось ли проигрывание текущего эффекта (нужно для старта проигрывания вложенных объектов)
        /// </summary>
        private bool _isEnd;


        /// <summary>
        /// Базовый вектор
        /// </summary>
        public Vector3 CustomBaseVector = Vector3.one;

        private Vector3 _startingScale;

        public override void Play()
        {
            _startingScale = _target.localScale;

            _isReverse = false;
            _isSubEffectsStarted = false;

            _target.localScale = CustomBaseVector * ScaleAnimationCurve.Evaluate(0);

            switch (_scaleMode)
            {
                case ScaleMode.Horizontal:
                    _target.localScale = new Vector3(_target.localScale.x, _startingScale.y, _target.localScale.z);
                    break;

                case ScaleMode.Vertical:
                    _target.localScale = new Vector3(_startingScale.x, _target.localScale.y, _target.localScale.z);
                    break;
            }

            _currTime = 0;
            _isEnd = false;

            base.Play();
        }

        public override void PlayReverse()
        {
            _isReverse = true;
            _isEnd = false;
            _currTime = 0;
            _isSubEffectsStarted = false;

            _target.localScale = Vector3.one * ScaleAnimationCurve.Evaluate(1);

            switch (_scaleMode)
            {
                case ScaleMode.Horizontal:
                    _target.localScale = new Vector3(_target.localScale.x, _startingScale.y, _target.localScale.z);
                    break;

                case ScaleMode.Vertical:
                    _target.localScale = new Vector3(_startingScale.x, _target.localScale.y, _target.localScale.z);
                    break;
            }

            base.PlayReverse();
        }

        public override void Stop()
        {
            _target.localScale = CustomBaseVector * ScaleAnimationCurve.Evaluate(1);

            switch (_scaleMode)
            {
                case ScaleMode.Horizontal:
                    _target.localScale = new Vector3(_target.localScale.x, _startingScale.y, _target.localScale.z);
                    break;

                case ScaleMode.Vertical:
                    _target.localScale = new Vector3(_startingScale.x, _target.localScale.y, _target.localScale.z);
                    break;
            }

            if (SubEffectsPlayMode == SubEffectsPlayMode.WithParent)
            {
                base.Stop();
                return;
            }

            _isPlaying = false;
            effectStopped.Dispatch(this);
        }

        public override void Cancel()
        {
            _target.localScale = Vector3.one * ScaleAnimationCurve.Evaluate(1);

            switch (_scaleMode)
            {
                case ScaleMode.Horizontal:
                    _target.localScale = new Vector3(_target.localScale.x, _startingScale.y, _target.localScale.z);
                    break;

                case ScaleMode.Vertical:
                    _target.localScale = new Vector3(_startingScale.x, _target.localScale.y, _target.localScale.z);
                    break;
            }

            base.Cancel();
        }

        private bool _isReverse;
        private bool _isSubEffectsStarted;

        // ReSharper disable once UnusedMember.Local
        private void Update()
        {
            if (!IsPlaying) return;

            _target.localScale = CustomBaseVector * (!_isReverse ? ScaleAnimationCurve.Evaluate(_currTime / _animationPlayTime) : ScaleAnimationCurve.Evaluate((_animationPlayTime - _currTime) / _animationPlayTime));

            switch (_scaleMode)
            {
                case ScaleMode.Horizontal:
                    _target.localScale = new Vector3(_target.localScale.x, _startingScale.y, _target.localScale.z);
                    break;

                case ScaleMode.Vertical:
                    _target.localScale = new Vector3(_startingScale.x, _target.localScale.y, _target.localScale.z);
                    break;
            }

            _currTime += Time.deltaTime;;

            if (_currTime >= _animationPlayTime)
            {
                if (EffectMode == SpecialEffectMode.Loop)
                {
                    _currTime = 0;
                }
                else
                {
                    _isEnd = true;
                    Stop();
                }
                
            }

            // если нужно после окончания основной анимации проиграть вложенные
            if (SubEffectsPlayMode == SubEffectsPlayMode.AfterParent)
            {
                if (_isSubEffectsStarted || !_isEnd) return;
                _isSubEffectsStarted = true;
                for (int i = 0; i < subEffects.Count; i++)
                {
                    if (subEffects[i] != null)
                    {
                        subEffects[i].Play();
                    }
                }
            }
        }
    }
}