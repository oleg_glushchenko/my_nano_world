﻿using System.Collections.Generic;
using UnityEngine;
using NanoReality.Engine.UI.UIAnimations;

public class GameObjectsSetActiveStatesEffect : BaseSpecialEffect
{
    [SerializeField] private List<GameObject> _targetsToActivate;
    [SerializeField] private List<GameObject> _targetsToDeactivate;

    public override void Play()
    {
        base.Play();

        foreach (var target in _targetsToActivate)
            target.SetActive(true);

        foreach (var target in _targetsToDeactivate)
            target.SetActive(false);

        Stop();
    }
}