﻿using Assets.NanoLib.UtilitesEngine;
using UnityEngine;
using UnityEngine.UI;
#pragma warning disable 649

namespace NanoReality.Engine.UI.UIAnimations
{
    public class SpecialTransparentEffect : BaseSpecialEffect
    {
        [SerializeField] private Image _target;
        [SerializeField] private float _startOpacity;
        [SerializeField] private float _endOpacity;
        [SerializeField] private float _speed;

        private float _startTime;

        public override void Play()
        {
            base.Play();
            _startTime = Time.time;
        }

        void Update()
        {
            if (!IsPlaying) return;

            float distCovered = (Time.time - _startTime + Time.deltaTime) * _speed;
            //float fracJourney = distCovered / _scaleFactor;
            Color color = _target.material.color;
            color.a = Mathf.Lerp(_startOpacity, _endOpacity, distCovered);
            _target.material.color = color;
        }
    }
}