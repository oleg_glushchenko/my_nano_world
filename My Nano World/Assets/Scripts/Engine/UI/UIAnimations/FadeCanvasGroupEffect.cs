﻿using DG.Tweening;
using NanoReality.Engine.UI.UIAnimations;
using UnityEngine;

namespace Engine.UI.UIAnimations
{
    public sealed class FadeCanvasGroupEffect : BaseSpecialEffect
    {
        [SerializeField] private CanvasGroup _targetCanvasGroup;
        [SerializeField] private float _fadeDuration;
        private Tweener _fadeTween;

        public override void Play()
        {
            base.Play();
            _targetCanvasGroup.alpha = 0;
            _fadeTween?.Kill();
            _fadeTween = _targetCanvasGroup.DOFade(1, _fadeDuration).OnComplete(Stop);
        }

        public override void PlayReverse()
        {
            base.PlayReverse();
            _fadeTween?.Kill();
            _fadeTween = _targetCanvasGroup.DOFade(0, _fadeDuration).OnComplete(Stop);
        }

        public override void Stop()
        {
            base.Stop();
            _fadeTween?.Kill();
            _fadeTween = null;
        }

        public override void Cancel()
        {
            base.Cancel();
            _fadeTween?.Kill();
            _fadeTween = null;
        }
    }
}