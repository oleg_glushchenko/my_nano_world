﻿using Assets.NanoLib.UtilitesEngine;
using UnityEngine;
using UnityEngine.UI;

#pragma warning disable 649

namespace NanoReality.Engine.UI.UIAnimations
{
    public class ItemsContainerSpacingEffect : BaseSpecialEffect
    {
        [SerializeField]
        private HorizontalOrVerticalLayoutGroup _container;

        [SerializeField] private float _spacingOffset;
        [SerializeField] private float _speed;

        private float _startTime;
        private bool _isInitialized;

        /// <summary>
        /// дефолтное правильное значение интервала между итемами
        /// </summary>
        private float _initSpacing;

        /// <summary>
        /// интервал между итемами для начала анимации
        /// </summary>
        private float _startSpacing;

		protected virtual void Awake()
        {
            _initSpacing = _container.spacing;
        }

        public override void Initialize()
        {
            base.Initialize();

            _startSpacing = _initSpacing + _spacingOffset;
            _container.spacing = _startSpacing;

            _isInitialized = true;
        }

        public override void Play()
        {
            if (!_isInitialized)
            {
                Initialize();
            }
            _startTime = Time.time;
            base.Play();
        }

        private void Update()
        {
            if (!IsPlaying) return;

            var distCovered = (Time.time - _startTime + Time.deltaTime) * _speed;
            _container.spacing = Mathf.Lerp(_startSpacing, _initSpacing, distCovered);

            if (Mathf.Abs(_container.spacing - _initSpacing) < 0.1f)
            {
                _container.spacing = _initSpacing;
                _isInitialized = false;
                Stop();
            }
        }
    }
}