﻿using Assets.NanoLib.Utilities.Pulls;
using NanoReality.Engine.UI.Components;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using UnityEngine;
using UnityEngine.UI;

public class BuildingShopProductItem : GameObjectPullable
{
    private Product _product;
    
    #region Inspector Fields
    [SerializeField] private Image _productIcon;
    [SerializeField] private Image _backgroundImage;
    [SerializeField] private Sprite _normalBgSprite;
    [SerializeField] private Sprite _activeBgSprite;
    [SerializeField] private Image _backgroundInnerImage;
    [SerializeField] private Sprite _normalInnerBgSprite;
    [SerializeField] private Sprite _activeInnerBgSprite;
    [SerializeField] private Animation _requiermentsBounceAnimation;
    public RequiermentsCounter RequiermentsCounter;

    #endregion

    #region Methods
    public void SetProduct(Product product, Sprite icon)
    {
        if (product == null)
        {
            Debug.LogError("Product Missing" );
            return;
        }
        _product = product;
        
        if (icon == null)
        {
            Debug.LogError("Icon Sprite Missing: " + product.ProductTypeID.Value);
            return;
        }
        _productIcon.sprite = icon;
    }

    public override void OnPush()
    {
        _productIcon.sprite = null;
        _product = null;
        base.OnPush();
    }

    #endregion
}