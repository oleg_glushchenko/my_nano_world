﻿using UnityEngine;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.BuildingSystem.CityMap;
using NanoReality.GameLogic.Utilities;

namespace NanoReality.Engine.UI.Components
{
    public class ConstructMapObjectArrow : MapObjectLooker
    {
        [SerializeField] private Renderer _arrowRenderer;
        
        private CityMapView _currentCityMapView;
        private float _cameraDegrees;

        public void SetMapData(float cameraDegrees)
        {
            _cameraDegrees = cameraDegrees;
        }

        public void Show()
        {
            if (Target == null)
                return;
            
            _currentCityMapView = jGameManager.GetMapView(Target.MapObjectModel.SectorId);
            _arrowRenderer.enabled = true;
        }

        public void Hide()
        {
            SetTarget(null);
            _currentCityMapView = null;
            _arrowRenderer.enabled = false;
        }

        protected override void LateUpdate()
        {
            if (!Target) return;

            CityMapView mapView = _currentCityMapView;
            Vector2F mapCell = new Vector2F(mapView.transform.position.x, mapView.transform.position.z) + Target.WorldSpacePosition.ToVector2F();
            
            Vector2F dimensions = Target.WorldSpaceDimensions.ToVector2F();

            switch (Pivot)
            {
                case PivotModes.Bottom:
                    // do nothing we already in botton
                    break;

                case PivotModes.Center:

                    mapCell += dimensions/2;
                    break;

                case PivotModes.Top:
                    mapCell += dimensions;
                    break;

                case PivotModes.Left:
                    mapCell += new Vector2F(0, dimensions.Y);
                    break;

                case PivotModes.Right:
                    mapCell += new Vector2F(dimensions.X, 0);
                    break;

                case PivotModes.BottomLeft:
                    mapCell += new Vector2F(0, dimensions.Y / 2);
                    
                    break;

                case PivotModes.BottomRight:

                    mapCell += new Vector2F(dimensions.X / 2, 0);
                    break;

                case PivotModes.TopLeft:

                    mapCell += new Vector2F(dimensions.X / 2, dimensions.Y);
                    break;

                case PivotModes.TopRight:
                    mapCell += new Vector2F(dimensions.X, dimensions.Y / 2);                   
                    break;
            }

            var height = Target.Depth;

            var position = new Vector3();

            position.y = height;
            position.x = position.z = -1 * MapObjectGeometry.GetHeightOffset(height, _cameraDegrees);
            position += new Vector3(mapCell.X, 0, mapCell.Y);

            Transform lookerTransform = transform;
            lookerTransform.position = position;
            lookerTransform.rotation = Target.CacheMonobehavior.CachedTransform.rotation;
        }
    }
}