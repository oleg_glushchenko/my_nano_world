﻿using UnityEngine;

public class RectTransformPositioner : MonoBehaviour
{
    public RectTransform Target;
    public RectTransform OptimalPosition;


    void OnEnable()
    {
        if((float)Screen.width / Screen.height > 1.4f)
        {
            Target.anchoredPosition = new Vector2(OptimalPosition.anchoredPosition.x, OptimalPosition.anchoredPosition.y);
        }
    }
}
