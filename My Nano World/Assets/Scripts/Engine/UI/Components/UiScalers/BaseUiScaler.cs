﻿using System;
using UnityEngine;

namespace NanoReality.Engine.UI.Components.Scalers
{
    public class BaseUiScaler : MonoBehaviour
    {

        public float CurrentAspectRatio;
        public ScaleUpdateTypes UpdateType;
        public bool EvaluateIfRatioNotChanged;

        protected virtual void Awake()
        {
            if (UpdateType == ScaleUpdateTypes.Awake)
            {
                EvaluateRatio();
                enabled = false;
            }
            if (UpdateType == ScaleUpdateTypes.Update)
                EvaluateRatio();
        }


        protected virtual void Start()
        {
            if (UpdateType == ScaleUpdateTypes.Start)
            {
                EvaluateRatio();
                enabled = false;
            }
            if (UpdateType == ScaleUpdateTypes.Update)
                EvaluateRatio();
        }


        protected virtual void Update()
        {
            if (UpdateType == ScaleUpdateTypes.Update)
                EvaluateRatio();
        }

        /// <summary>
        /// считаем текущее соотношение сторон и обновляем UI если необходимо
        /// </summary>
        protected virtual void EvaluateRatio()
        {
            var newRatio =(float) Math.Round(((float) Screen.width)/Screen.height,2);
            var oldRatio = CurrentAspectRatio;
            CurrentAspectRatio = newRatio;
            if (EvaluateIfRatioNotChanged || Math.Abs(newRatio - oldRatio) > 0.01f)
                OnUiRationChanged();
        }

        /// <summary>
        /// метод применяемый при изменении соотношения сторон
        /// </summary>
        protected virtual void OnUiRationChanged()
        {
            
        }

        public enum ScaleUpdateTypes
        {
            Awake,
            Start,
            Update
        }
    }
}

