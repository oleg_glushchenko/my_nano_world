﻿using System;
using UnityEngine;

namespace NanoReality.Engine.UI.Components.Scalers
{
    [ExecuteInEditMode]
    public class AspectRatioScaler : MonoBehaviour
    {
        [SerializeField]
        private Params _from;
        [SerializeField]
        private Params _to;
        [SerializeField]
        private bool _onAwake = true;
        [SerializeField]
        private bool _onStart;
        [SerializeField]
        private bool _onUpdate;

        private Transform _transform;

        private void OnValidate()
        {
            _from.Width = Mathf.Clamp(_from.Width, 1, int.MaxValue);
            _from.Height = Mathf.Clamp(_from.Height, 1, int.MaxValue);
            
            _to.Width = Mathf.Clamp(_to.Width, 1, int.MaxValue);
            _to.Height = Mathf.Clamp(_to.Height, 1, int.MaxValue);
        }

        private void Awake()
        {
            _transform = GetComponent<Transform>();

            if (_onAwake)
            {
                CalculateScale();
            }
        }

        private void Start()
        {
            if (_onStart)
            {
                CalculateScale();
            }
        }

        private void Update()
        {
            if (_onUpdate)
            {
                CalculateScale();
            }
        }

        private void CalculateScale()
        {
            var aspect = Screen.width * 1f / Screen.height;
            aspect = Mathf.Clamp(aspect, _from.Aspect, _to.Aspect);
            var t = Mathf.Clamp01((aspect - _from.Aspect) / (_to.Aspect - _from.Aspect));
            var scale = Vector3.Lerp(_from.Scale, _to.Scale, t);
            var position = Vector3.Lerp(_from.Position, _to.Position, t);

            _transform.localScale = scale;
            _transform.localPosition = position;
        }

        [Serializable]
        public class Params
        {
            public int Width = 4;
            public int Height = 3;
            public Vector3 Scale = Vector3.one;
            public Vector3 Position = Vector3.zero;

            public float Aspect
            {
                get
                {
                    return Width * 1f / Height;
                }
            }
        }
    }
}
