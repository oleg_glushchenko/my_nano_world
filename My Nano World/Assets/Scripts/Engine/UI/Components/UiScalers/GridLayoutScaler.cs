﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Components.Scalers
{
    [RequireComponent(typeof (GridLayoutGroup))]
    public class GridLayoutScaler : TargetRatioUiScaler
    {

        private GridLayoutGroup _targetGroup;

		public RectOffset Padding;

        public Vector2 CellSize;
        public Vector2 Spacing;
		public int ConstraintCount;

        protected override void Awake()
        {
            _targetGroup = GetComponent<GridLayoutGroup>();
            base.Awake();
        }

        protected override void ApplyState()
        {
            base.ApplyState();
            _targetGroup.cellSize = CellSize;
            _targetGroup.spacing = Spacing;
			_targetGroup.constraintCount = ConstraintCount;
			_targetGroup.padding = Padding;
        }
    }
}
