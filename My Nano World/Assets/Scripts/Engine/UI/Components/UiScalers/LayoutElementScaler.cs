﻿using UnityEngine;
using System.Collections;
using NanoReality.Engine.UI.Components.Scalers;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Extensions.BuildingsShopPanel
{
    [RequireComponent(typeof(LayoutElement))]
    public class LayoutElementScaler : TargetRatioUiScaler
    {
        protected LayoutElement TargetLayoutElement;

        public float PrefferedWidth;
        public float PrefferedHeight;


        protected override void Awake()
        {
            TargetLayoutElement = GetComponent<LayoutElement>();
            base.Awake();
            
        }


        protected override void ApplyState()
        {
            base.ApplyState();
            TargetLayoutElement.preferredWidth = PrefferedWidth;
            TargetLayoutElement.preferredHeight = PrefferedHeight;
        }

    }
}
