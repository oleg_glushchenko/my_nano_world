﻿using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Components.Scalers
{
    [RequireComponent(typeof (Text))]
    public class FontScaler : TargetRatioUiScaler
    {
        private Text _targeText;

        public int TargetFontSize;

        protected override void Awake()

        {
            _targeText = GetComponent<Text>();
            base.Awake();

        }

        protected override void ApplyState()
        {
            base.ApplyState();
            _targeText.fontSize = TargetFontSize;

        }
    }
}
