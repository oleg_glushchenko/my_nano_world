﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace NanoReality.Engine.UI.Components.Scalers
{
    public class TargetRatioUiScaler : BaseUiScaler
    {
        public List<float> TargetRatios = new List<float>();
		public RatioTarget RatioTargetType = RatioTarget.Phone;

        /// <summary>
        /// список доступных соотношений сторон для телефонов и планшетов
        /// </summary>
        protected override void Awake()
        {



            // так как верстка была выполнена под 4:3, мы считаем планшетами только соотношения сторон 4:3
            if (RatioTargetType == RatioTarget.Tablet)
            {
				TargetRatios.Clear();	
				TargetRatios.Add(1.33f); 		
            }

            // все остальное - телефоны
            if (RatioTargetType == RatioTarget.Phone)
            {
                TargetRatios.Clear();
                TargetRatios.Add(1.5f);         //3:2	(1.5)	 		Galaxy Mini 2 и Galaxy Ace (480/320), iPhone 4 (960/640)
                TargetRatios.Add(1.66f);        //5:3	(1.(6)) 		Galaxy W, Galaxy Ace II, Galaxy s III mini, Galaxy S II (800/480), Lumia 920 и Nexus 4 (1280/768)
                TargetRatios.Add(1.78f);		//16:9	(1.775, 1.(7))	iPhone 5, iPhone 5s (1136х640), HTC One S (960f/540) , Xperia S и Galaxy Nexus и Galaxy S III и Galaxy Note II (1280/720)
                TargetRatios.Add(1.77f);
                TargetRatios.Add(1.6f);			//16:10	(1.6)			Meizu MX2, Galaxy Note (1280/800)
            }
            base.Awake();
        }

        protected override void OnUiRationChanged()
        {
            base.OnUiRationChanged();
            if(TargetRatios.Contains(CurrentAspectRatio))
                ApplyState();
        }

        protected virtual void ApplyState()
        {
        }

        public enum RatioTarget
        {
            Tablet,
            Phone
        }
    }
}