﻿using UnityEngine;
using System.Collections;

namespace NanoReality.Engine.UI.Components.Scalers
{
    [RequireComponent(typeof(RectTransform))]
    public class RectTransformUiScaler : TargetRatioUiScaler
    {
        protected RectTransform TargetRectTransform;

        public float Left;
        public float Righ;
        public float Top;
        public float Bottom;

        public Vector2 Size;
        public Vector3 LocalPosition;

        protected override void Awake()
        {
            TargetRectTransform = GetComponent<RectTransform>();
            base.Awake();
        }

        protected override void ApplyState()
        {
            base.ApplyState();
            if (Size != Vector2.zero)
            {
                TargetRectTransform.sizeDelta = Size;
				TargetRectTransform.anchoredPosition = LocalPosition;
            }
            else
            {

                TargetRectTransform.offsetMin = new Vector2(Left, Bottom);
                TargetRectTransform.offsetMax = new Vector2(-Righ, -Top);
            }
        }
    }
}
