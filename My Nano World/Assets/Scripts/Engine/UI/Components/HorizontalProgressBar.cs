﻿﻿using System;
 using UnityEngine;

/// <summary>
/// How to
/// Attach this component to progressBar background, and set fill Image as child of it
/// fill Image anchors used as paddings relative to background
/// Set LTRB in fillImage rectTransform to 0, pivot to (0,0.5)
/// </summary>

[ExecuteInEditMode]
[Obsolete("Use ProgressBar instead")]
public class HorizontalProgressBar : MonoBehaviour
{
	[SerializeField]
	protected RectTransform _background;

	[SerializeField]
	protected RectTransform _fillImage;

	[SerializeField]
	[Range(0f, 1f)]
	protected float _fillAmount;

    [SerializeField]
    protected float _minFillAmount = 0.03f; //костыль для слишком маленьких значений (плохо отображается в слайдере)

    public virtual float FillAmount
    {
        get { return _fillAmount; }
        set
        {
            _fillAmount = Mathf.Clamp01(value);
            _fillImage.gameObject.SetActive(_fillAmount > _minFillAmount);
            _fillImage.sizeDelta = new Vector2(_background.rect.width * (_fillImage.anchorMax.x - _fillImage.anchorMin.x) * (_fillAmount - 1f), 0f);
		}
    }

#if UNITY_EDITOR

	/// <summary>
	/// Used for preview in editor
	/// </summary>
	private void Update()
	{
		FillAmount = _fillAmount;
	}

#endif
}