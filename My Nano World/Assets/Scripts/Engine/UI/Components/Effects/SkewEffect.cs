﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Делает косые Image
/// </summary>
public class SkewEffect : BaseMeshEffect
{

    /// <summary>
    /// Офссет для верхних пикселей по горизонтали, и правых пикселей по вертикали
    /// </summary>
    [SerializeField]
    private Vector2 _vertexOffset;


    private Vector2 _max;
    private readonly List<int> _top = new List<int>();
    private readonly List<int> _right = new List<int>();

    public override void ModifyMesh(VertexHelper vh)
    {

        if (!IsActive())
        {
            return;
        }

        var currVertex = new List<UIVertex>();

        _max.x = float.MinValue;
        _max.y = float.MinValue;
        _top.Clear();
        _right.Clear();

        vh.GetUIVertexStream(currVertex);
        var count = currVertex.Count;
        if(count == 0)
            return;

        for (int i = 0; i < count; i++)
        {
            if (currVertex[i].position.x > _max.x)
                _max.x = currVertex[i].position.x;
            if (currVertex[i].position.y > _max.y)
                _max.y = currVertex[i].position.y;
        }

        for (int i = 0; i < count; i++)
        {
            if (currVertex[i].position.y == _max.y)
            {
                _top.Add(i);
            }
            if (currVertex[i].position.x == _max.x)
            {
                _right.Add(i);
            }
        }

        for (int i = 0; i < _top.Count; i++)
        {
            var v1 = currVertex[_top[i]];
            v1.position.x += _vertexOffset.x;
            currVertex[_top[i]] = v1;
        }

        for (int i = 0; i < _right.Count; i++)
        {
            var v1 = currVertex[_right[i]];
            v1.position.y += _vertexOffset.y;
            currVertex[_right[i]] = v1;
        }
        vh.Clear();
        vh.AddUIVertexTriangleStream(currVertex);

    }
}
