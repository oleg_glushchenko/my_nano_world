using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Engine.UI.Components
{
    [Obsolete("Use ProgressBar with ProgressBarIcon instead.")]
    public class DynamicHorizontalProgressBar : HorizontalProgressBar
    {
        [SerializeField]
        private ProgressBarStates _barStates;
	
        public override float FillAmount
        {
            get => _fillAmount;
            set
            {
                var rectTransform = _fillImage;
                _fillAmount = Mathf.Clamp01(value);
                _fillImage.gameObject.SetActive(_fillAmount > _minFillAmount);
                rectTransform.sizeDelta = new Vector2(_background.rect.width * (rectTransform.anchorMax.x - rectTransform.anchorMin.x) * (_fillAmount - 1f), 0f);
                if (_barStates != null)
                {
                    _fillImage.GetComponent<Image>().sprite = _barStates.GetSprite(_fillAmount);
                }
            }
        }
    }
}