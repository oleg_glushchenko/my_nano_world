using UnityEngine;
using UnityEngine.UI;

namespace Engine.UI.Components
{
    public class SpriteChanger : MonoBehaviour
    {
        [SerializeField]
        private Image _targetImage;
        [SerializeField]
        private ProgressBarStates _barStates;
        
        private float _value;
        public float Value
        {
            get => _value;
            set
            {
                _value = Mathf.Clamp01(value);
                if (_barStates != null)
                {
                    _targetImage.sprite = _barStates.GetSprite(_value);
                }
            }
        }
    }
}