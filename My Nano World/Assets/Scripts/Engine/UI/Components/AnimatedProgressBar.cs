﻿using System;
using DG.Tweening;
using NanoReality.GameLogic.Utilities;

namespace Engine.UI.Components
{
	[Obsolete("Use ProgressBar instead")]
	public class AnimatedProgressBar : HorizontalProgressBar 
	{
		
		public void FillAmountTo(float value, Action onComplete)
		{
			if (value > FillAmount)
			{
				DOTween.To(() => FillAmount, fill => FillAmount = fill, value, 1f)
					.OnComplete(onComplete.SafeRaise);
			}
			else
			{
				FillAmount = value;
				onComplete?.Invoke();
			}
			
		}
	}
}
