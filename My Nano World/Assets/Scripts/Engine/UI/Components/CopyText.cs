﻿using TMPro;
using UnityEngine;

namespace Engine.UI.Components
{
	/// <summary>
	/// Копирует текст с другого текстового поля
	/// </summary>
	[RequireComponent(typeof(TextMeshProUGUI))]
	public class CopyText : MonoBehaviour
	{
		[SerializeField]
		private TextMeshProUGUI _originalText;

		private TextMeshProUGUI _text;

		private void Start()
		{
			_text = GetComponent<TextMeshProUGUI>();
			_text.text = _originalText.text;
		}

		private void Update()
		{
			if (_text.text != _originalText.text)
				_text.text = _originalText.text;
		}
	}
}
