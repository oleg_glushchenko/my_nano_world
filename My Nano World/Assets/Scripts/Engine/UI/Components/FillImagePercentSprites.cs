﻿﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

[ExecuteInEditMode]
public class FillImagePercentSprites : MonoBehaviour {

    /// <summary>
    /// Список всех вариантов заливки
    /// </summary>
    public List<SliderFill> SliderFills;


	[SerializeField]
	private Image _image;

	[SerializeField]
	private HorizontalProgressBar _progressBar;

    /// <summary>
    /// Вызывается каждый фрейм - да ладно
    /// </summary>
    public void Update()
    {
		var percentageSliderValue = (int)(_progressBar.FillAmount * 100f);

	    foreach (SliderFill constraint in SliderFills)
	    {
		    if (percentageSliderValue <= constraint.MaxValueToShowInPercents &&
				percentageSliderValue >= constraint.MinValueToShowInPercents)
		    {
			    _image.sprite = constraint.Fill;
			    return;
		    }
	    }
    }
}
