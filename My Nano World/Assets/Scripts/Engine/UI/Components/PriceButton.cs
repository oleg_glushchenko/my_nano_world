﻿using Assets.Scripts.GameLogic.Utilites;
using Assets.Scripts.MultiLanguageSystem.Logic;
using strange.extensions.signal.impl;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Components
{
    [RequireComponent(typeof(Button))]
    public class PriceButton : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _priceLabel;

        [SerializeField] private bool _showFreeLabel = true;

        private Button _targetButton;
        private int _price;

        public Signal<PriceButton> SignalOnButtonClick = new Signal<PriceButton>();

        public bool ShowFreeLabel
        {
            get => _showFreeLabel;
            set
            {
                _showFreeLabel = value;
                UpdateLabel();
            }
        }

        public int Price
        {
            get => _price;
            set
            {
                _price = value;
                UpdateLabel();
            }
        }

        protected virtual void Awake()
        {
            _targetButton = GetComponent<Button>();
        }

        private void OnEnable()
        {
            _targetButton.onClick.AddListener(OnButtonClick);
        }

        private void OnDisable()
        {
            _targetButton.onClick.RemoveListener(OnButtonClick);
        }

        private void UpdateLabel()
        {
            string text = _price == 0 && _showFreeLabel
                ? LocalizationMLS.Instance.GetText(LocalizationKeyConstants.FREE_LABEL)
                : _price.ToString();

            if (_priceLabel != null)
            {
                _priceLabel.SetLocalizedText(text);
            }
        }

        public void OnButtonClick() => SignalOnButtonClick.Dispatch(this);
    }
}