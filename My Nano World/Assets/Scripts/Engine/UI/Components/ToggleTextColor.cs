﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Components
{
    [RequireComponent(typeof(Toggle))]
    public class ToggleTextColor : MonoBehaviour
    {
        private Toggle _toggle;
        public TextMeshProUGUI Text;
        
        public Color ActiveColor;
        public Color InactiveColor;

        protected virtual void Awake()
        {
            _toggle = GetComponent<Toggle>();
            _toggle.onValueChanged.AddListener(OnToggleValueChanged);
            OnToggleValueChanged(_toggle.isOn);
        }
        
        private void OnToggleValueChanged(bool isOn)
        {
            if (Text != null)
                Text.color = isOn ? ActiveColor : InactiveColor;
        }
    }
}