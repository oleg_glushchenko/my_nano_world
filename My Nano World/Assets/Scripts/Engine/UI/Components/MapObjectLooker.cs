﻿using UnityEngine;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.BuildingSystem.CityCamera;
using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.BuildingSystem.MapObjects.Components;
using NanoReality.GameLogic.GameManager.Models.api;
using strange.extensions.mediation.impl;

namespace NanoReality.Engine.UI.Components
{
	public class MapObjectLooker : View
	{
		[Inject] public IGameCameraModel jGameCameraModel { get; set; }
		[Inject] public IGameManager jGameManager { get; set; }
		
		[SerializeField] protected MapObjectView _target;
		[SerializeField] protected PivotModes _pivot;

		public Vector3 RandomedOffset = Vector3.zero;
		public Vector3 TargetCustomRotation = Vector3.zero;
		public Vector3 TargetCustomOffset = Vector3.zero;
		public Vector3 TargetCustomScale = Vector3.one;

		public bool AdaptToCameraZoom;
		public AnimationCurve AdaptingScaleCurve = new AnimationCurve(new Keyframe(0f, 1f), new Keyframe(1f, 0.85f));

		private ConstructionComponent _targetConstructionComponent;

		public MapObjectView Target => _target;

		public PivotModes Pivot
		{
			get => _pivot;
			set => _pivot = value;
		}
		
		public void SetTarget(MapObjectView value)
		{
			_target = value;
			_targetConstructionComponent = _target == null ? null : Target.ConstructionComponent;
		}

		protected virtual void LateUpdate()
		{
			UpdateLookerPosition();
		}

		public void UpdateLookerPosition()
		{
			if (!Target)
			{
				return;
			}

			var lookerTransform = transform;
			var targetTransform = _target.Transform;

			if (AdaptToCameraZoom)
			{
				var cameraZoomFactor = jGameCameraModel.CurrentZoom / jGameCameraModel.MinZoom * AdaptingScaleCurve.Evaluate(jGameCameraModel.CurrentZoomPercent);
				lookerTransform.localScale = TargetCustomScale * cameraZoomFactor;
			}
			else
			{
				lookerTransform.localScale = TargetCustomScale == Vector3.one ? targetTransform.localScale : TargetCustomScale;
			}

			var position = targetTransform.position;
			Bounds bounds;
			if (_targetConstructionComponent != null && _targetConstructionComponent.ConstructView)
			{
				bounds = _targetConstructionComponent.ConstructView.SpriteRenderer.sprite.bounds;
			}
			else
			{
				var target = Target as RoadMapObjectView;
				bounds = target != null ? target.MeshFilter.mesh.bounds : Target.SpriteRenderer.sprite.bounds;
			}

			var center = bounds.center;
			var extents = bounds.extents;
			var z = center.z - extents.z;

			var frontTopLeft = targetTransform.TransformPoint(new Vector3(center.x - extents.x, center.y + extents.y, z));
			var frontTopRight = targetTransform.TransformPoint(new Vector3(center.x + extents.x, center.y + extents.y, z));
			var frontBottomLeft = targetTransform.TransformPoint(new Vector3(center.x - extents.x, center.y - extents.y, z));
			var frontBottomRight = targetTransform.TransformPoint(new Vector3(center.x + extents.x, center.y - extents.y, z));

			switch (Pivot)
			{
				case PivotModes.Bottom:
					position = (frontBottomRight + frontBottomLeft) / 2;
					break;
				case PivotModes.Center:
					position = targetTransform.TransformPoint(bounds.center);
					break;
				case PivotModes.Top:
					position = (frontTopLeft + frontTopRight) / 2;
					break;
				case PivotModes.Left:
					position = (frontTopLeft + frontBottomLeft) / 2;
					break;
				case PivotModes.Right:
					position = (frontTopRight + frontBottomRight) / 2;
					break;
				case PivotModes.BottomLeft:
					position = frontBottomLeft;
					break;
				case PivotModes.BottomRight:
					position = frontBottomRight;
					break;
				case PivotModes.TopLeft:
					position = frontTopLeft;
					break;
				case PivotModes.TopRight:
					position = frontTopRight;
					break;
				case PivotModes.Random:
					position = (frontTopLeft + frontTopRight) / 2 + new Vector3(0f, 0f, 0f) + RandomedOffset;
					break;
				case PivotModes.TopSomeMore:
					position = (frontTopLeft + frontTopRight) / 2 + new Vector3(0f, 0.2f, 0f);
					break;
				case PivotModes.TopMiddleRight:
					position = (frontTopLeft + frontTopRight) / 2 + new Vector3(0.7f, 0.1f, 0f);
					break;
				case PivotModes.CenterRoad:
					position = targetTransform.TransformPoint(bounds.center);
					break;
			}

			lookerTransform.position = position + TargetCustomOffset;
			lookerTransform.rotation = TargetCustomRotation == Vector3.zero ? targetTransform.rotation : Quaternion.Euler(TargetCustomRotation);
		}

		public enum PivotModes
		{
			Center,
			Bottom,
			Top,
			TopSomeMore,
			TopMiddleRight,
			Left,
			Right,
			BottomLeft,
			BottomRight,
			TopLeft,
			TopRight,
			Random,
			CenterRoad
		}
	}
}