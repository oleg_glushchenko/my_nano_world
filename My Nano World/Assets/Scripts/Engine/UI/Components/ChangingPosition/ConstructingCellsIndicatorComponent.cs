﻿using UnityEngine;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.Settings.BuildSettings.Models.Api;
using NanoReality.StrangeIoC;

namespace NanoReality.UI.Components.ChangingPosition
{
    /// <summary>
    /// Компонент плашки-индикатора о возможности постройки в етом месте
    /// </summary>
    public class ConstructingCellsIndicatorComponent : AView, IMapObjectConstructingIndicatorComponent
    {
        public static IMapObjectConstructingIndicatorComponent Instance { get; private set; }

        [SerializeField] private Transform _quadTransform;
        [SerializeField] private MeshRenderer _quadRenderer;
        [SerializeField] private Color _areaFreeColor;
        [SerializeField] private Color _areNotFreeColor;

        [SerializeField] private Vector2Int _cellsOffset;

        private bool _isAreaFree;
        private MapObjectView _mapObjectView;

        [Inject] public ICityMapSettings jCityMapSettings { get; set; }
        
        #region IMapObjectConstructingIndicatorComponent implementation

        public void SetBuildingView(MapObjectView view)
        {
            _mapObjectView = view;
        }
        
        public void Show(bool isAreaFree)
        {
            base.Show();
            
            gameObject.SetActive(true);
            UpdateComponent(isAreaFree);
            
            // TODO: hotfix. workaround.
            Invoke(nameof(Delayed), 0.01f);
        }

        private void Delayed()
        {
            UpdateComponent(_isAreaFree);
        }

        public override void Hide()
        {
            base.Hide();
            
            gameObject.SetActive(false);
            
            _mapObjectView = null;
        }

        public void UpdateComponent(bool isAreaFree)
        {
            _isAreaFree = isAreaFree;
            _quadRenderer.material.color = isAreaFree ? _areaFreeColor : _areNotFreeColor;
            UpdatePositionAndScale();
        }
        
        #endregion
        
        protected override void Awake()
        {
            base.Awake();
            
            Instance = this;
            _quadRenderer.material = new Material(_quadRenderer.material);
            _mapObjectView = null;
        }

        protected override void Start()
        {
            base.Start();
            gameObject.SetActive(false);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if ((ConstructingCellsIndicatorComponent) Instance == this)
            {
                Instance = null;
            }
        }

        private void UpdatePositionAndScale()
        {
            Vector2 viewSize = ConvertToWorldPosition(_mapObjectView.GridDimensions);
            Vector3 panelOffset = ConvertToWorldPosition(_cellsOffset);

            Vector3 position = new Vector3();
            position.x = viewSize.x / 2;
            position.z = viewSize.y / 2;
            
            _quadRenderer.transform.localPosition = position;
            _quadRenderer.transform.localScale = new Vector3(viewSize.x, viewSize.y, 1f);
            
            _quadTransform.position = _mapObjectView.transform.position + panelOffset;
        }

        private Vector2 ConvertToWorldPosition(Vector2Int gridPosition)
        {
            return new Vector2(
                gridPosition.x * jCityMapSettings.GridCellScale,
                gridPosition.y * jCityMapSettings.GridCellScale);
        }
    }
}
