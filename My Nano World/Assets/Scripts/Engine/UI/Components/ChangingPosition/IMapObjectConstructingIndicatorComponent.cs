﻿using Assets.Scripts.Engine.BuildingSystem.MapObjects;

namespace NanoReality.UI.Components.ChangingPosition
{
    public interface IMapObjectConstructingIndicatorComponent
    {
        void SetBuildingView(MapObjectView view);
        
        void Show(bool isAreaFree);

        void Hide();

        void UpdateComponent(bool isAreaFree);
    }
}