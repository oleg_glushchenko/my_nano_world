﻿using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.BuildingSystem.CityMap;
using UnityEngine;

namespace NanoReality.Engine.UI.Components
{
    public class SubSectorLooker : MonoBehaviour
    {
        #region Inspector Fields

        [SerializeField]
        protected LockedSectorView _target;

        [SerializeField]
        protected MapObjectLooker.PivotModes _pivot;

        #endregion
        
        public Vector3 RandomedOffset = Vector3.zero;
        
        public MapObjectLooker.PivotModes Pivot
        {
            get { return _pivot; }
            set
            {
                _pivot = value;
                if (value == MapObjectLooker.PivotModes.Random)
                {
                    const float maxOffset = 1f;

                    RandomedOffset = new Vector3(
                        Random.Range(-maxOffset, maxOffset),
                        Random.Range(-maxOffset, maxOffset),
                        0);
                }
            }
        }

        protected void Start()
        {
            UpdatePosition();
        }

        public void SetTarget(LockedSectorView target)
        {
            _target = target;
        }

        private void UpdatePosition()
        {
            if (_target != null)
            {
                var position = _target.transform.position;
                var bounds = _target.Mesh.mesh.bounds;

                Vector3 v3Center = bounds.center;
                Vector3 v3Extents = bounds.extents;

                var v3FrontTopLeft = _target.transform.TransformPoint(new Vector3(v3Center.x - v3Extents.x, v3Center.y + v3Extents.y, v3Center.z - v3Extents.z));  // Front top left corner
                var v3FrontTopRight = _target.transform.TransformPoint(new Vector3(v3Center.x + v3Extents.x, v3Center.y + v3Extents.y, v3Center.z - v3Extents.z));  // Front top right corner
                var v3FrontBottomLeft = _target.transform.TransformPoint(new Vector3(v3Center.x - v3Extents.x, v3Center.y - v3Extents.y, v3Center.z - v3Extents.z));  // Front bottom left corner
                var v3FrontBottomRight = _target.transform.TransformPoint(new Vector3(v3Center.x + v3Extents.x, v3Center.y - v3Extents.y, v3Center.z - v3Extents.z));  // Front bottom right corner


                switch (Pivot)
                {
                    case MapObjectLooker.PivotModes.Bottom:

                        position = (v3FrontBottomRight + v3FrontBottomLeft) / 2;
                        break;

                    case MapObjectLooker.PivotModes.Center:

                        position = _target.transform.TransformPoint(bounds.center);
                        break;

                    case MapObjectLooker.PivotModes.Top:
                        position = (v3FrontTopLeft + v3FrontTopRight) / 2;
                        break;

                    case MapObjectLooker.PivotModes.Left:
                        position = (v3FrontTopLeft + v3FrontBottomLeft) / 2;
                        break;

                    case MapObjectLooker.PivotModes.Right:
                        position = (v3FrontTopRight + v3FrontBottomRight) / 2;
                        break;

                    case MapObjectLooker.PivotModes.BottomLeft:
                        position = v3FrontBottomLeft;
                        break;

                    case MapObjectLooker.PivotModes.BottomRight:
                        position = v3FrontBottomRight;
                        break;

                    case MapObjectLooker.PivotModes.TopLeft:
                        position = v3FrontTopLeft;
                        break;

                    case MapObjectLooker.PivotModes.TopRight:
                        position = v3FrontTopRight;
                        break;

                    case MapObjectLooker.PivotModes.Random:
                        position = ((v3FrontTopLeft + v3FrontTopRight) / 2) + new Vector3(0f, 0.5f, 0f) + RandomedOffset;
                        break;

                    case MapObjectLooker.PivotModes.TopSomeMore:
                        position = ((v3FrontTopLeft + v3FrontTopRight) / 2) + new Vector3(0f, 0.5f, 0f);
                        break;
                }

                Vector3 shiftToCenter = 0.5f * new Vector3(_target.WorldSpaceDimensions.x, 0, _target.WorldSpaceDimensions.y);
                Vector3 newPosition = new Vector3(position.x, position.y - 1.25f, position.z) + shiftToCenter;

                if (transform.position.sqrMagnitude - newPosition.sqrMagnitude > 0.001f)
                    transform.position = newPosition;
            }
        }
    }
}