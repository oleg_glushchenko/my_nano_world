using System;
using UnityEngine;

namespace Engine.UI.Components
{
    [ExecuteInEditMode]
    public abstract class ProgressBar : MonoBehaviour
    {
        public event Action<float> ProgressChanged;
        
        public abstract float Value { get; set; }

        protected void InvokeProgressChanged()
        {
            ProgressChanged?.Invoke(Value);
        }
    }
}