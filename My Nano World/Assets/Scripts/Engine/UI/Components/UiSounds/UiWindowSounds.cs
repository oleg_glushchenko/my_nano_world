﻿using UnityEngine;
using Assets.NanoLib.UI.Core.Views;
using NanoLib.Core.Services.Sound;
using strange.extensions.mediation.impl;

namespace NanoReality.Engine.UI.Components.Sound
{
    [RequireComponent(typeof(UIPanelView))]
    public class UiWindowSounds : View
    {
        [SerializeField] private SfxSoundTypes OpenSfx = SfxSoundTypes.AppWindow;
        [SerializeField] private SfxSoundTypes CloseSfx = SfxSoundTypes.CloseWindow;
        
        [Inject] public ISoundManager jSoundManager { get; set; }

        protected override void Start()
        {
            base.Start();
            var view = GetComponent<UIPanelView>();
            view.OnShown.AddListener(PlayShowClip);
            view.OnHide.AddListener(PlayHideClip);
        }

        private void PlayShowClip(UIPanelView sender)
        {
            jSoundManager.Play(OpenSfx, SoundChannel.SoundFX);
        }

        private void PlayHideClip(UIPanelView sender)
        {
            jSoundManager.Play(CloseSfx, SoundChannel.SoundFX);
        }
    }
}