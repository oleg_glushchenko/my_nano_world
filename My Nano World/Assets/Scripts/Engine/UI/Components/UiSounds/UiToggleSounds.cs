﻿using UnityEngine;
using NanoLib.Core.Services.Sound;
using strange.extensions.mediation.impl;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Components.Sound
{
    [RequireComponent(typeof(Toggle))]
    public class UiToggleSounds : View
    {
        [Inject] public ISoundManager jSoundManager { get; set; }

        protected override void Start()
        {
            base.Start();
            GetComponent<Toggle>().onValueChanged.AddListener(OnTargetToggleClick);
        }

        private void OnTargetToggleClick(bool _)
        {
            jSoundManager.Play(SfxSoundTypes.ClickButton, SoundChannel.SoundFX);
        }
    }
}