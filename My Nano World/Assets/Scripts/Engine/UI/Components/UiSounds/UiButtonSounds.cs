﻿using NanoLib.Core.Services.Sound;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Components.Sound
{
    [RequireComponent(typeof(Button))]
    public class UiButtonSounds : View
    {
        [Inject] public ISoundManager jSoundManager { get; set; }

        private Button _targetButton;

        protected override void Awake()
        {
            base.Awake();
            _targetButton = GetComponent<Button>();
            _targetButton.onClick.AddListener(OnButtonClicked);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            _targetButton.onClick.RemoveListener(OnButtonClicked);
        }

        private void OnButtonClicked()
        {
            jSoundManager.Play(SfxSoundTypes.IconTap, SoundChannel.SoundFX);
        }
    }
}