﻿using UnityEngine;
using Assets.NanoLib.Utilities;

namespace NanoReality.Engine.UI.Components
{
    public class NonPlayableBuildingLooker : MonoBehaviour
    {
        #region Inspector Fields
        [SerializeField]
        protected NonPlayableBuildingView _target;

        [SerializeField]
        protected MapObjectLooker.PivotModes _pivot;
        #endregion

        public NonPlayableBuildingView Target
        {
            get { return _target; }
            set { _target = value; }
        }

        public Vector3 RandomedOffset = Vector3.zero;


        public MapObjectLooker.PivotModes Pivot
        {
            get { return _pivot; }
            set
            {
                _pivot = value;
                if(value == MapObjectLooker.PivotModes.Random)
                {
                    const float maxOffset = 1f;

                    RandomedOffset = new Vector3(
                        Random.Range(-maxOffset, maxOffset),
                        Random.Range(-maxOffset, maxOffset),
                        0);
                }
            }
        }

        public CacheMonobehavior CacheMonobehavior { get; protected set; }

        protected virtual void Awake()
        {
            CacheMonobehavior = new CacheMonobehavior(this);
        }

        protected virtual void LateUpdate()
        {
            if(Target != null)
            {
                var position = Target.CacheMonobehavior.CachedTransform.position;
                var bounds = Target.Mesh.mesh.bounds;

                Vector3 v3Center = bounds.center;
                Vector3 v3Extents = bounds.extents;

                var v3FrontTopLeft = Target.CacheMonobehavior.CachedTransform.TransformPoint(new Vector3(v3Center.x - v3Extents.x, v3Center.y + v3Extents.y, v3Center.z - v3Extents.z));  // Front top left corner
                var v3FrontTopRight = Target.CacheMonobehavior.CachedTransform.TransformPoint(new Vector3(v3Center.x + v3Extents.x, v3Center.y + v3Extents.y, v3Center.z - v3Extents.z));  // Front top right corner
                var v3FrontBottomLeft = Target.CacheMonobehavior.CachedTransform.TransformPoint(new Vector3(v3Center.x - v3Extents.x, v3Center.y - v3Extents.y, v3Center.z - v3Extents.z));  // Front bottom left corner
                var v3FrontBottomRight = Target.CacheMonobehavior.CachedTransform.TransformPoint(new Vector3(v3Center.x + v3Extents.x, v3Center.y - v3Extents.y, v3Center.z - v3Extents.z));  // Front bottom right corner

                switch(Pivot)
                {
                    case MapObjectLooker.PivotModes.Bottom:

                        position = (v3FrontBottomRight + v3FrontBottomLeft) / 2;
                        break;

                    case MapObjectLooker.PivotModes.Center:

                        position = Target.CacheMonobehavior.CachedTransform.TransformPoint(bounds.center);
                        break;

                    case MapObjectLooker.PivotModes.Top:
                        position = (v3FrontTopLeft + v3FrontTopRight) / 2;
                        break;

                    case MapObjectLooker.PivotModes.Left:
                        position = (v3FrontTopLeft + v3FrontBottomLeft) / 2;
                        break;

                    case MapObjectLooker.PivotModes.Right:
                        position = (v3FrontTopRight + v3FrontBottomRight) / 2;
                        break;

                    case MapObjectLooker.PivotModes.BottomLeft:
                        position = v3FrontBottomLeft;
                        break;

                    case MapObjectLooker.PivotModes.BottomRight:
                        position = v3FrontBottomRight;
                        break;

                    case MapObjectLooker.PivotModes.TopLeft:
                        position = v3FrontTopLeft;
                        break;

                    case MapObjectLooker.PivotModes.TopRight:
                        position = v3FrontTopRight;
                        break;

                    case MapObjectLooker.PivotModes.Random:
                        position = ((v3FrontTopLeft + v3FrontTopRight) / 2) + new Vector3(0f, 0.5f, 0f) + RandomedOffset;
                        break;

                    case MapObjectLooker.PivotModes.TopSomeMore:
                        position = ((v3FrontTopLeft + v3FrontTopRight) / 2) + new Vector3(0f, 0.5f, 0f);
                        break;
                }

                CacheMonobehavior.CachedTransform.position = new Vector3(position.x, position.y - 1.25f, position.z);
                CacheMonobehavior.CachedTransform.rotation = Quaternion.Euler(new Vector3(0, 45, 0));// Target.CacheMonobehavior.CachedTransform.rotation;
            }
        }
    }
}