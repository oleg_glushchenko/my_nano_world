using System;

namespace Assets.NanoLib.UI.Core.Views {
    /// <summary>
    /// How new panel should close or not previous panels
    /// </summary>
    [Flags]
    public enum PanelOpenMode
    {
        /// <summary> do nothing </summary>
        None = 0,
        /// <summary> Close panel first (if opened) an open again</summary>
        Reopen = 1 << 0,
        /// <summary> do nothing </summary>
        Simple = 1 << 1,
        /// <summary> Close previous ignoring type </summary>
        Displace = 1 << 2,
        /// <summary> Close all previously opened panels </summary>
        Solo = 1 << 3
    }
}