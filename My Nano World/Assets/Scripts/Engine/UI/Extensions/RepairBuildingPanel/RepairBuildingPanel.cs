﻿using System.Collections.Generic;
using System.Linq;
using NanoLib.Services.InputService;
using Assets.NanoLib.UI.Core.Components;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.Analytics;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Assets.Scripts.Engine.UI.Views.Tooltips;
using Assets.Scripts.Engine.UI.Views.Trade.Items;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.api;
using Assets.Scripts.MultiLanguageSystem.Logic;
using NanoReality.Game.Services.BuildingsUnlockService;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.Services;
using NanoReality.GameLogic.Utilites;
using TMPro;
using UnityEngine.UI;
using TooltipType = Assets.Scripts.Engine.UI.Views.Tooltips.TooltipType;

namespace Engine.UI.Extensions.RepairBuildingPanel
{
    public class RepairBuildingPanel : BuildingPanelView
    {
        public ItemContainer RequirementsContainer;
        public ProductCountCard ProductCountCard;
        public TextMeshProUGUI BuildingTitle;
        public Button RepairButton;

        public TextValueObject WillBeAvailableText;
        public TextMeshProUGUI ComingSoonText;
        public TextMeshProUGUI AvailableText;
        
        private bool _needOffset = true;

        [Inject] public SignalOnBuildingRepaired jSignalOnBuildingRepaired { get; set; }
        [Inject] public SignalOnGroundTap jSignalOnGroundTap { get; set; }
        [Inject] public SignalOnNeedToShowTooltip jSignalOnNeedToShowTooltip { get; set; }
        [Inject] public SignalOnNeedToHideTooltip jSignalOnNeedToHideTooltip { get; set; }

        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IProductsData jProductsData { get; set; }
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public IBuildingsUnlockService jBuildingsUnlockService { get; set; }
        [Inject] public IIconProvider jIconProvider { get; set; }

        [Inject] public SignalOnProductsStateChanged jSignalOnProductsStateChanged { get; set; }
        [Inject] public SignalOnRepairBuildingAction jSignalOnRepairBuildingAction { get; set; }


        private List<IProductsToUnlockItem> _productsList = new List<IProductsToUnlockItem>();
        private MapObjectView TargetBuildingView { get; set; }
        
        public override void Show()
        {
            base.Show();
            jGameCamera.FocusOnMapObject(TargetBuilding);
            TargetBuildingView = GetTargetView();
        }

        public override void Hide()
        {
            RemoveListeners();
            base.Hide();
        }
        
        public override void Initialize(object parameters)
        {
            base.Initialize(parameters);
            jSignalOnGroundTap.AddListener(Hide);
            jSignalOnProductsStateChanged.AddListener(UpdateItems);
            BuildingTitle.SetLocalizedText(TargetBuilding.Name);

            ComingSoonText.gameObject.SetActive(false);

            int buildingUnlockLevel = jBuildingsUnlockService.GetBuildingUnlockLevel(TargetBuilding.ObjectTypeID);
            if (jPlayerProfile.CurrentLevelData.CurrentLevel < buildingUnlockLevel)
            {
                RequirementsContainer.gameObject.SetActive(false);
                WillBeAvailableText.gameObject.SetActive(true);
                AvailableText.text = string.Format(LocalizationUtils.LocalizeL2("Localization/SHARED_LABEL_WILL_BE_AVAILABLE_AT_LEVEL"), buildingUnlockLevel + 1);
                WillBeAvailableText.Value = "";
                WillBeAvailableText.Text = "";
                RepairButton.gameObject.SetActive(false);
                return;
            }

            WillBeAvailableText.gameObject.SetActive(false);
            RepairButton.gameObject.SetActive(true);
            FillItems(TargetBuilding.ProductsToUnlock?.Products);
        }

        public void FillItems(List<IProductsToUnlockItem> productsList)
        {
            _productsList = productsList;

            if (!RequirementsContainer.IsInstantiateed)
                RequirementsContainer.InstaniteContainer(ProductCountCard, 0);

            RequirementsContainer.ClearCurrentItems();

            if (_productsList == null || _productsList.Count == 0)
            {
                RequirementsContainer.gameObject.SetActive(false);
                return;
            }

            RequirementsContainer.gameObject.SetActive(true);
            foreach (var productToUnlock in _productsList)
            {
                var newCountCard = (ProductCountCard)RequirementsContainer.AddItem();
                Product product = jProductsData.GetProduct(productToUnlock.ProductID);

                var productInitData = new ProductCountCardInitData
                {
                    ProductObject = product,
                    ProductSprite = jIconProvider.GetProductSprite(productToUnlock.ProductID.Value),
                    SpecialOrderSprite = jIconProvider.GetSpecialOrderIcon(product.SpecialOrder),
                    ShowOnlySimpleTools = false, //true
                    ProductsCount = null,
                    CurrentProductsCount =
                        jPlayerProfile.PlayerResources.GetProductCount(productToUnlock.ProductID),
                    NeedProductsCount = productToUnlock.Count,
                    IsCountNeed = true, //true
                    Type = TooltipProductType.Product
                };

                newCountCard.Initialize(productInitData);

                newCountCard.SignalOnSelected.RemoveListener(OnSelectItemSignal);
                newCountCard.SignalOnSelected.AddListener(OnSelectItemSignal);

                newCountCard.SignalOnHideTooltip.RemoveListener(OnHideItemSignal);
                newCountCard.SignalOnHideTooltip.AddListener(OnHideItemSignal);
            }
        }

        private void UpdateItems(Dictionary<Id<Product>, int> a)
        {
            FillItems(_productsList);
        }

        private void OnHideItemSignal()
        {
            jSignalOnNeedToHideTooltip.Dispatch();
        }

        private void OnSelectItemSignal(TooltipData tooltipData)
        {
            switch (tooltipData.Type)
            {
                case TooltipType.ProductItems:
                    tooltipData.OnGoToClickAction = (Hide);
                    break;
            }

            jSignalOnNeedToShowTooltip.Dispatch(tooltipData);
        }

        public void OnRepairBuildingButtonClick()
        {
            // если есть требуемые продукты для ремонта здания
            if (_productsList != null && _productsList.Count > 0)
            {
                var productsPrice = _productsList.ToDictionary(product => product.ProductID, product => product.Count);

                var buyPrice = new Price
                {
                    SoftPrice = 0,
                    HardPrice = 0,
                    PriceType = PriceType.Resources,
                    ProductsPrice = productsPrice
                };

                var isEnough = jPlayerProfile.PlayerResources.IsEnoughtForBuy(buyPrice);

                if (isEnough)
                {
                    jPlayerProfile.PlayerResources.BuyItem(buyPrice);
                    DoRepair();
                }
                else
                {
                    jPopupManager.Show(new NotEnoughResourcesPopupSettings(buyPrice), result =>
                    {
                        if (result == PopupResult.Ok)
                            OnRepairBuildingButtonClick();
                    });
                }
            }
            // если требуемых продуктов для ремонта здания нет, то есть оно ремонтируется бесплатно
            else
            {
                DoRepair();
            }
        }

        private void DoRepair()
        {
            jServerCommunicator.RepairBuilding(TargetBuilding.MapID, OnServerConfirmedRepair);
            jSignalOnRepairBuildingAction.Dispatch(TargetBuilding);
            TargetBuilding.IsLocked = false;
            TargetBuilding.ProductsToUnlock = null;
            TargetBuildingView.Interactable = false;

            Hide();
        }

        private void OnServerConfirmedRepair()
        {
            jSignalOnBuildingRepaired.Dispatch(TargetBuilding.ObjectTypeID, TargetBuilding.MapID);
            TargetBuildingView.Interactable = true;
        }

        private void RemoveListeners()
        {
            jSignalOnGroundTap.RemoveListener(Hide);
            jSignalOnProductsStateChanged.RemoveListener(UpdateItems);
        }
    }
}
