﻿using System;
using Assets.NanoLib.Utilities.Pulls;
using NanoReality.Core.Resources;
using NanoReality.GameLogic.Quests;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Extensions.QuestPanel
{
    public class QuestAwardItem : GameObjectPullable
    {
        private IAwardAction _currentAwardAction;
        
        public Image RewardIcon;
        public TextMeshProUGUI RewardCount;

        public Sprite LevelUpSprite;
        public Sprite SoftCurrencySprite;
        public Sprite PremiumCurrencySprite;
        public Sprite ExperienceSprite;
        public Sprite NanoGemsSprite;

        public void UpdateItem(IAwardAction rewardAction, IResourcesManager jResourcesManager)
        {
            _currentAwardAction = rewardAction;
            SetRewardIcon(jResourcesManager);
            RewardCount.text = _currentAwardAction.AwardType != AwardActionTypes.GiveRandomProduct
                ? _currentAwardAction.AwardCount.ToString()
                : "?";
        }

        private void SetRewardIcon(IResourcesManager jResourcesManager)
        {
            switch (_currentAwardAction.AwardType)
            {
                case AwardActionTypes.LevelUp: RewardIcon.sprite = LevelUpSprite; break;
                case AwardActionTypes.SoftCurrency: RewardIcon.sprite = SoftCurrencySprite; break;
                case AwardActionTypes.PremiumCurrency: RewardIcon.sprite = PremiumCurrencySprite; break;
                case AwardActionTypes.Experience: RewardIcon.sprite = ExperienceSprite; break;
                case AwardActionTypes.NanoGems: RewardIcon.sprite = NanoGemsSprite; break;
                case AwardActionTypes.Resources:
                    RewardIcon.sprite = jResourcesManager.GetProductSprite(((GiveProductAward)_currentAwardAction).Product.Value);
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        
        public override void OnPush()
        {
            base.OnPush();
            _currentAwardAction = null;
        }
    }
}