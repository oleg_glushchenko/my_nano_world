﻿using Assets.NanoLib.Utilities.Pulls;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Extensions.QuestPanel
{
    public class QuestConditionDescription : GameObjectPullable
    {
        public Text Description;
    }
}