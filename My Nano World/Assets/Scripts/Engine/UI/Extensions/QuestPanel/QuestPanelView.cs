﻿using System.Collections;
using System.Linq;
using Assets.NanoLib.UI.Core.Components;
using Assets.NanoLib.UI.Core.Views;
using NanoReality.GameLogic.Quests;
using System.Collections.Generic;
using Assets.NanoLib.UI.Core.Signals;
using Assets.Scripts.Engine.UI.Views.Replics;
using Assets.Scripts.GameLogic.Utilites;
using Assets.Scripts.MultiLanguageSystem.Logic;
using NanoReality.Core.Resources;
using NanoReality.Engine.UI.UIAnimations;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Services;
using NanoReality.GameLogic.Utilites;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Extensions.QuestPanel
{    
    public class QuestPanelView : UIPanelView
    {
        #region InspectorFields

        /// <summary>
        /// Контейнер итемов квестов (public так как используется в туторе)
        /// </summary>
        public ItemContainer IconsItemContainer;

        /// <summary>
        /// Префаб итема квеста
        /// </summary>
        [SerializeField]
        private QuestIconItem _itemPrefab;

        /// <summary>
        /// Скролл квестов
        /// </summary>
        [SerializeField]
        private ScrollRect _scrollRect;

        /// <summary>
        /// Класс вкладок (для переключения между квестами связанными с разными секторами)
        /// </summary>
        [SerializeField]
        private QuestPanelBookmarks _questPanelBookmarks;

        /// <summary>
        /// Шапка панели квестов
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI _questPanelTitle;

        /// <summary>
        /// Подсказка игроку (на какому уровне откроется следующий квест)
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI _questPanelUnlockNotification;

        /// <summary>
        /// RectTransform для установки размеров панели
        /// </summary>
        [SerializeField]
        private RectTransform _questPanelRectTransform;

        /// <summary>
        /// Фон панели когда есть доступные квесты
        /// </summary>
        [SerializeField]
        private GameObject _questsAvailableGameObject;

        /// <summary>
        /// Фон панели когда нету доступных квестов
        /// </summary>
        [SerializeField]
        private GameObject _questsEmptyGameObject;

        /// <summary>
        /// Индекс для вычисления ширины панели
        /// </summary>
        [SerializeField]
        private float _panelWidthIndex = 3f;

        /// <summary>
        /// Индекс для вычисления высоты панели
        /// </summary>
        [SerializeField]
        private float _panelHeightIndex = 6f;

        #endregion

        [Inject] public IUserQuestData UserQuestData { get; set; }
        [Inject] public IResourcesManager jResourcesManager { get; set; }
        [Inject] public IIconProvider jIconProvider { get; set; }
        [Inject] public SignalOnUserBusinessSectorsUpdate jSignalOnUserBusinessSectorsUpdate { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IQuestBalanceData jQuestBalanceData { get; set; }
        [Inject] public ShowWindowSignal jShowWindowSignal { get;set; }

        //контейнеры для высоты и ширины панели с норм. размером (когда есть квесты)
        private Vector2 normalOffsetMin;
        private Vector2 normalOffsetMax;
        //контейнеры для высоты и ширины панели с мин. размером (когда нету квестов)
        private Vector2 minOffsetMin;
        private Vector2 maxOffsetMax;

        /// <summary>
        /// Костыль верстки. Прячем кнопку квестов в хаде пока открыто окно квестов
        /// </summary>
        public GameObject HudQuestButton;

        public override void Initialize(object parameters)
        {
            base.Initialize(parameters);
            HudQuestButton.SetActive(false);
            _scrollRect.vertical = false;
            _scrollRect.verticalNormalizedPosition = 1;
            SetInitialBookmarks();
            UpdateQuestNotificationCounts();
        }
        
        public void UpdatePanel()
        {
            if (!Visible) return;

            StartCoroutine(SetNormalizedPosition());

            //сортируем квесты по секторам
            var sectorQuests = UserQuestData.UserQuestsData.Where(
                    q => q.BusinessSector == _questPanelBookmarks.CurrentSelectedBookmark.BusinessSectorsType).ToList();

            //сортируем квесты по статусу
            var quests = sectorQuests.Where(
					q => q.QuestState == UserQuestStates.Achived ||
                    q.QuestState == UserQuestStates.InProgress)
                    .OrderBy((x) => x.QuestState != UserQuestStates.Achived).ToList();

            UpdateIcons(quests);
            UpdatePanelState(quests.Count);
            SetQuestTitle(_questPanelBookmarks.CurrentSelectedBookmark.QuestPanelTitle);

            UpdateQuestNotificationCounts();
        }

        /// <summary>
        /// Обновляем панель в зависимости от доступных квестов
        /// </summary>
        private void UpdatePanelState(int questAmount)
        {
            _questsEmptyGameObject.SetActive(questAmount == 0);
            _questsAvailableGameObject.SetActive(questAmount != 0);
            if(questAmount == 0)
            {
                _questPanelRectTransform.offsetMin = minOffsetMin;
                _questPanelRectTransform.offsetMax = maxOffsetMax;

                GetNextLevelQuestData();
            }
            else
            {
                _questPanelRectTransform.offsetMin = normalOffsetMin;
                _questPanelRectTransform.offsetMax = normalOffsetMax;
            }
        }

        /// <summary>
        /// Выводим подсказку с уровнем когда будут доступны следующие квесты
        /// </summary>
        private void GetNextLevelQuestData()
        {
            var nextLevelData = jQuestBalanceData.QuestLevelsData.Find(
                    n => n.QuestSectorType == _questPanelBookmarks.CurrentSelectedBookmark.BusinessSectorsType
                         && n.QuestLevel > jPlayerProfile.CurrentLevelData.CurrentLevel);
            var nextQuestLevel = nextLevelData.QuestLevel + 1;
            if (nextLevelData.QuestSectorType.IsSupported())
            {
                _questPanelUnlockNotification.SetLocalizedText(
                    LocalizationMLS.Instance.GetText(LocalizationKeyConstants.QUESTS_LABEL_UNLOCK_NEXT_LVL) +
                    nextQuestLevel + ".");
            }
            else
            {
                _questPanelUnlockNotification.SetLocalizedText(
                    LocalizationMLS.Instance.GetText(LocalizationKeyConstants.QUESTS_LABEL_NEW_QUEST_SOON));
            }
        }

        [PostConstruct]
        public void PostConstruct()
        {
            _questPanelBookmarks.SignalOnBookmarkChanged.AddListener(OnChangeQuestBookmark);
            jSignalOnUserBusinessSectorsUpdate.AddListener(OnUserBusinessSectorsUpdateSignal);

            GetPanelSizes();
        }

        /// <summary>
        /// Получаем размеры панельки (минимальные и нормальные)
        /// </summary>
        private void GetPanelSizes()
        {
            normalOffsetMin = _questPanelRectTransform.offsetMin;
            normalOffsetMax = _questPanelRectTransform.offsetMax;

            minOffsetMin = new Vector2(normalOffsetMin.x, normalOffsetMin.y * _panelHeightIndex);
            maxOffsetMax = new Vector2(normalOffsetMax.x * _panelWidthIndex, normalOffsetMax.y);
        }

        /// <summary>
        /// Переключение на нужную вкладку
        /// </summary>
        public void ActivateTab(BusinessSectorsTypes neededSectorType)
        {
            foreach (var filter in _questPanelBookmarks.Filters)
            {
                if (filter.BusinessSectorsType == neededSectorType)
                {
                    filter.QuestBookmarkItem.Toggle.isOn = true;
                    OnChangeQuestBookmark(_questPanelBookmarks);
                }
                else
                {
                    filter.QuestBookmarkItem.Toggle.isOn = false;
                }
            }
        }

        /// <summary>
        /// Проверяем табы и включаем/выключаем в зависимости от сектора
        /// </summary>
        private void UpdateBookmarksList(IEnumerable<BusinessSectorsTypes> sectorsList)
        {
            foreach (var availableSector in sectorsList)
            {
                _questPanelBookmarks.CheckBookmarkStatusBySectorId(availableSector);
            }
        }

        /// <summary>
        /// Открываем табу при открытии нового сектора
        /// </summary>
        private void OnUserBusinessSectorsUpdateSignal(IEnumerable<BusinessSectorsTypes> sectorsList)
        {
            foreach(var availableSector in sectorsList)
            {
                _questPanelBookmarks.CheckBookmarkStatusBySectorId(availableSector);
            }
        }

        private void SetInitialBookmarks()
        {
            _questPanelBookmarks.SelectSectorType(BusinessSectorsTypes.Town);
            UpdateBookmarksList(new[] {BusinessSectorsTypes.Town, BusinessSectorsTypes.Farm, BusinessSectorsTypes.HeavyIndustry});
        }

        /// <summary>
        /// Выставляем заголовок панели квестов в зависимости от региона
        /// </summary>
        private void SetQuestTitle(string titleConst)
        {
            _questPanelTitle.SetLocalizedText(LocalizationMLS.Instance.GetText(titleConst));
        }

        /// <summary>
        /// Обновляем счетчики на вкладках квест панели
        /// </summary>
        private void UpdateQuestNotificationCounts()
        {
            foreach(var filterItem in _questPanelBookmarks.Filters)
            {
                var count = UserQuestData.UserQuestsData.Where(q => q.QuestState == UserQuestStates.Achived &&
                q.BusinessSector == filterItem.BusinessSectorsType).ToList().Count;
                filterItem.Notifications.Value = count;
            }
        }

        /// <summary>
        /// Обновляем панельку при переключении между вкладками
        /// </summary>
        private void OnChangeQuestBookmark(QuestPanelBookmarks bookmark)
        {
            if (bookmark.CurrentSelectedBookmark.BusinessSectorsType == BusinessSectorsTypes.Farm)
                PlayerPrefs.SetInt("Farm_Quests_Opened_Once", 1);

            UpdatePanel();
        }

        void UpdateIcons(List<IQuest> quests)
        {
            InitItemsContainer();
            IconsItemContainer.ClearCurrentItems();

            foreach (var userQuest in quests)
            {
                var item = IconsItemContainer.AddItem() as QuestIconItem;
                if(item == null) continue;
                item.UpdateItem(userQuest, jResourcesManager, jIconProvider);
                item.OnClimeRewardSignal.AddListener(OnClimeReward);
            }
        }

        private void OnClimeReward(QuestIconItem item)
        {
			if (item.CurrentQuest.IsDialogShow)
			{
				ShowCharacterDialog(item);
			}

			item.CurrentQuest.CollectAward();
            
            item.UpdateItemBack();
			UpdatePanel();
        }

        private void ShowCharacterDialog(QuestIconItem item)
        {
            string dialogText = LocalizationUtils.GetQuestDialogText(item.CurrentQuest.QuestId.Value);

            jShowWindowSignal.Dispatch(typeof(DialogPanelView),
                new DialogSimpleData(item.CurrentQuest.BusinessSector, dialogText, DialogPosition.Left));
        }

        public override void Show()
        {

        }

        protected override void ShowEffectEnded(ISpecialEffect specialEffect)
        {
            base.ShowEffectEnded(specialEffect);

            StartCoroutine(SetNormalizedPosition());

            UpdatePanel();
        }

        IEnumerator SetNormalizedPosition()
        {
            yield return new WaitForEndOfFrame();

            if(_scrollRect != null)
            {
                _scrollRect.enabled = true;
                _scrollRect.vertical = true;
                _scrollRect.verticalNormalizedPosition = 1;
            }
        }

        public override void Hide()
        {
            base.Hide();
            HudQuestButton.SetActive(true);
        }

        private void InitItemsContainer()
        {
            if(!IconsItemContainer.IsInstantiateed)
                IconsItemContainer.InstaniteContainer(_itemPrefab, 0);
        }
    }
}