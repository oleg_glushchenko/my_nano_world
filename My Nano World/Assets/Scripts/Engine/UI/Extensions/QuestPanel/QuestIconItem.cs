﻿using Assets.NanoLib.UI.Core.Components;
using Assets.NanoLib.Utilities.Pulls;
using NanoReality.GameLogic.Quests;
using strange.extensions.signal.impl;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using NanoReality.Core.Resources;
using NanoReality.GameLogic.Services;
using NanoReality.GameLogic.Utilites;
using TMPro;

namespace NanoReality.Engine.UI.Extensions.QuestPanel
{
    public class QuestIconItem : GameObjectPullable
    {
        [SerializeField]
        private TextMeshProUGUI _questDescription;

        [SerializeField]
        private Animation _bounceAnimation;

        public ItemContainer QuestAwardsContainer;
        public QuestAwardItem AwardItemPrefab;

        public ItemContainer QuestRequirementsIconsContainer;
        public QuestConditionIcon RequirementItemIconPrefab;
        
        public IQuest CurrentQuest { get; private set; }
		public Transform ClimeButton;

        [SerializeField]
        List<GameObject> ObjectsForNotCompletedStatus = new List<GameObject>();
        [SerializeField]
        List<GameObject> ObjectsForCompletedStatus = new List<GameObject>();

        /// <summary>
        /// Слайдер выполнения квеста
        /// </summary>
        [SerializeField]
        private HorizontalProgressBar _questProgressBarSlider;

        /// <summary>
        /// Отображаем текущий прогресс квеста
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI _questProgressBarText;

        public Signal<QuestIconItem> OnClimeRewardSignal = new Signal<QuestIconItem>();

        void Awake()
        {
            QuestRequirementsIconsContainer.InstaniteContainer(RequirementItemIconPrefab, 0);
            QuestAwardsContainer.InstaniteContainer(AwardItemPrefab, 0);
        }

        public void UpdateItem(IQuest userQuest, IResourcesManager jResourcesManager, IIconProvider iconProvider)
        {
            CurrentQuest = userQuest;
            UpdateItemBack();
            _questDescription.text = LocalizationUtils.GetQuestDescription(CurrentQuest.QuestId.Value);
            CalculateProgress();
            UpdateRequirements(jResourcesManager, iconProvider);
            UpdateAwards(jResourcesManager);
        }

        public void PlayBounce()
        {
            _bounceAnimation.Play();
        }

        /// <summary>
        /// Высчитываем на заполненость слайдера и отображаем прогресс квеста
        /// </summary>
        private void CalculateProgress()
        {
            int currentQuestProgress = CurrentQuest.AchiveConditionsStates.Count(q => q.IsAchived);
            float fillValue = (float)currentQuestProgress / CurrentQuest.AchiveConditionsStates.Count;
            _questProgressBarSlider.FillAmount = fillValue;
            _questProgressBarText.text = string.Format(currentQuestProgress + "/" + CurrentQuest.AchiveConditionsStates.Count);
        }

        /// <summary>
        /// Обновляем фоны в соответствии со статусом квеста (выполнен, не выполнен)
        /// </summary>
        public void UpdateItemBack()
        {
            //Квест считаем завершенным, выставялет соответствующий фон для итемов
            if (CurrentQuest.QuestState == UserQuestStates.Achived)
            {
                for (int i = 0; i < ObjectsForCompletedStatus.Count; i++)
                    ObjectsForCompletedStatus[i].SetActive(true);
                for(int i = 0; i < ObjectsForNotCompletedStatus.Count; i++)
                    ObjectsForNotCompletedStatus[i].SetActive(false);
            }
            //Квест еще не завершен, выставялет соответствующий фон для итемов
            else
            {
                for(int i = 0; i < ObjectsForCompletedStatus.Count; i++)
                    ObjectsForCompletedStatus[i].SetActive(false);
                for(int i = 0; i < ObjectsForNotCompletedStatus.Count; i++)
                    ObjectsForNotCompletedStatus[i].SetActive(true);
            }
        }

        private void UpdateRequirements(IResourcesManager jResourcesManager, IIconProvider buildingsIconsManager)
        {
            QuestRequirementsIconsContainer.ClearCurrentItems();
            foreach (var requirement in CurrentQuest.AchiveConditionsStates)
            {
                var item = (QuestConditionIcon)QuestRequirementsIconsContainer.AddItem();
                item.UpdateItem(requirement, jResourcesManager, buildingsIconsManager);
            }
        }

        private void UpdateAwards(IResourcesManager jResourcesManager)
        {
            QuestAwardsContainer.ClearCurrentItems();
            foreach (var award in CurrentQuest.AwardActions)
            {
                var item = QuestAwardsContainer.AddItem() as QuestAwardItem;
                if (item != null) item.UpdateItem(award, jResourcesManager);
            }
        }

        public void OnClimeButtonClick()
        {
            OnClimeRewardSignal.Dispatch(this);
        }

        public override void OnPush()
        {
            base.OnPush();
            OnClimeRewardSignal = new Signal<QuestIconItem>();
        }
    }
}