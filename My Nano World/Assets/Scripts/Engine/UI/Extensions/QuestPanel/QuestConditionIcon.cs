﻿using Assets.NanoLib.UI.Core.Views;
using Assets.NanoLib.Utilities.Pulls;
using NanoReality.Core.Resources;
using NanoReality.GameLogic.Quests;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.Services;

namespace NanoReality.Engine.UI.Extensions.QuestPanel
{
    public class QuestConditionIcon : GameObjectPullable, IPointerClickHandler, IPointerDownHandler
    {
        [SerializeField] private Image RequirementIcon;
        [SerializeField] private GameObject _readyMark;
        [SerializeField] private TextMeshProUGUI ItemsCount;
        [SerializeField] private Image CounterBackground;
        [SerializeField] private Sprite CompleteCountSprite;
        [SerializeField] private Sprite NotCompleteCountSprite;
        [SerializeField] private Image RequiredItemBackground;
        [SerializeField] private Sprite RequiredItemCompleteSprite;
        [SerializeField] private Sprite RequiredItemNotCompleteSprite;
        [SerializeField] private Image RequiredItemInnerBackground;
        [SerializeField] private Sprite RequiredItemInnerCompleteSprite;
        [SerializeField] private Sprite RequiredItemInnerNotCompleteSprite;
        [SerializeField] private GameObject DefaultBg;
        [SerializeField] private GameObject SelectedBg;
        [SerializeField] private GameObject _upgradeIcon;


        [SerializeField] private GameObject _questCompletedIcon;
        [SerializeField] private GameObject _questNotCompletedIcon;


        [SerializeField] private Sprite ProductsTotalProducedQuestIcon;
        [SerializeField] private Sprite CertainBuildingTypeQuestIcon;
        [SerializeField] private Sprite SectorOpenedQuestIcon;
        [SerializeField] private Sprite CitiesOpenedQuestIcon;
        [SerializeField] private Sprite CoinsGainedQuestIcon;
        [SerializeField] private Sprite CollectTaxesQuestIcon;
        [SerializeField] private Sprite PremiumCoinsGainedQuestIcon;
        [SerializeField] private Sprite EducatePeoplesQuestIcon;
        [SerializeField] private Sprite SatisfactionQuestIcon;
        [SerializeField] private Sprite AuctionGetCoinsQuestIcon;
        [SerializeField] private Sprite AuctionSpendCoinsQuestIcon;
        [SerializeField] private Sprite PopulationAmountQuestIcon;
        [SerializeField] private Sprite QuestAchivedQuestIcon;
        [SerializeField] private Sprite ResearchingsQuestIcon;
        [SerializeField] private Sprite AuctionSaleQuestIcon;
        [SerializeField] private Sprite AuctionPurchaseQuestIcon;
        [SerializeField] private Sprite UserLevelQuestIcon;
        [SerializeField] private Sprite InviteFriendQuestIcon;
        [SerializeField] private Sprite UnlockSubSectorQuestIcon;
        [SerializeField] private Sprite OrderDeskQuestIcon;
        [SerializeField] private Sprite LoginSocialIcon;

        [SerializeField] private Sprite PowerStationIcon;
        [SerializeField] private Sprite WinBurgetIcon;
        [SerializeField] private Sprite RoadIcon;

        [SerializeField] private Material _grayScaleMaterial;

        public ICondition CurrentCondition;

        private bool _ignoreSelect;
        private bool _isSelected;
        private SignalOnQuestRequirmentItemClick _signalOnQuestRequirmentItemClick;

        /// <summary>
        /// Sets gray scale material to icon and image if true
        /// </summary>
        /// <param name="isLocked"></param>
        public void SetGrayScale(bool isLocked)
        {
            var material = isLocked ? _grayScaleMaterial : null;

            RequirementIcon.material = material;
        }
        
        public void UpdateItem(ICondition condition, IResourcesManager jResourcesManager, IIconProvider buildingsIconsManager)
        {
            CurrentCondition = condition;
            var r = CurrentCondition.GetConditionRequirement();
            SetRequirementData(r, jResourcesManager, buildingsIconsManager);
            _upgradeIcon.SetActive(IsUpgradeQuest());
        }

        private void SetRequirementData(Requirement r, IResourcesManager jResourcesManager, IIconProvider buildingsIconsManager)
        {
            switch (CurrentCondition.ConditionType)
            {
                case ConditionTypes.ProductProduced:
                    SetData(jResourcesManager.GetProductSprite(r.Id), r.CurrentCount, r.NeededCount, false);
                    break;

                case ConditionTypes.ProductsTotalProduced:
                    SetData(ProductsTotalProducedQuestIcon, r.CurrentCount, r.NeededCount);
                    break;

                case ConditionTypes.ProductsUniqueAmountProduced:
                    SetData(jResourcesManager.GetProductSprite(r.Id), r.CurrentCount, r.NeededCount);
                    break;

                case ConditionTypes.RepairBuilding:
                    SetData(GetBuildingIcon(buildingsIconsManager, new MapObjectId(r.Id, 0)), r.CurrentCount, r.NeededCount);
                    break;

                case ConditionTypes.HaveBuilding:
                case ConditionTypes.ConstructBuilding:
                case ConditionTypes.BuildingUnlocked:
                    SetData(GetBuildingIcon(buildingsIconsManager, new MapObjectId(r.Id, 0)), r.CurrentCount, r.NeededCount);
                    break;

                case ConditionTypes.CertainBuildingTypeBuilded:
                    SetUpgradeData(CertainBuildingTypeQuestIcon, r.CurrentCount, r.NeededCount);
                    break;

                case ConditionTypes.CitiesOpened:
                    SetData(CitiesOpenedQuestIcon, r.CurrentCount, r.NeededCount);
                    break;

                case ConditionTypes.CertainSectorOpened:
                case ConditionTypes.SectorsOpened:
                    SetData(SectorOpenedQuestIcon, r.CurrentCount, r.NeededCount);
                    break;

                case ConditionTypes.CoinsGained:
                case ConditionTypes.CollectSoftCoinsFromCommercialBuildings:
                    SetData(CoinsGainedQuestIcon, r.CurrentCount, r.NeededCount);
                    break;

                case ConditionTypes.PremiumCoinsGained:
                    SetData(PremiumCoinsGainedQuestIcon, r.CurrentCount, r.NeededCount);
                    break;

                case ConditionTypes.EducateSchoolPeoples:
                    SetData(EducatePeoplesQuestIcon, r.CurrentCount, r.NeededCount);
                    break;
                case ConditionTypes.SatisfactionLevel:
                    SetData(SatisfactionQuestIcon, r.CurrentCount, r.NeededCount);
                    break;

                case ConditionTypes.ReachPopulation:
                    SetData(PopulationAmountQuestIcon, r.CurrentCount, r.NeededCount);
                    break;

                case ConditionTypes.QuestAchived:
                case ConditionTypes.QuestsDoneCount:
                    SetData(QuestAchivedQuestIcon, r.CurrentCount, r.NeededCount);
                    break;

                case ConditionTypes.ResearchingsAmount:
                    SetData(ResearchingsQuestIcon, r.CurrentCount, r.NeededCount);
                    break;

                case ConditionTypes.AuctionPurchases:
                    SetData(AuctionSaleQuestIcon, r.CurrentCount, r.NeededCount, false);
                    break;
                case ConditionTypes.AuctionSales:
                    SetData(AuctionPurchaseQuestIcon, r.CurrentCount, r.NeededCount, false);
                    break;

                case ConditionTypes.SpendCoinsAuction:
                case ConditionTypes.GetCoinsFromAuction:
                    SetData(AuctionGetCoinsQuestIcon, r.CurrentCount, r.NeededCount);
                    break;

                case ConditionTypes.UserLevelAchived:
                    SetData(UserLevelQuestIcon, r.CurrentCount, r.NeededCount);
                    break;

                case ConditionTypes.InviteFriends:
                case ConditionTypes.AmountFriends:
                    SetData(InviteFriendQuestIcon, r.CurrentCount, r.NeededCount);
                    break;

                case ConditionTypes.UnlockSectorsForConstructions:
                    SetData(UnlockSubSectorQuestIcon, r.CurrentCount, r.NeededCount);
                    break;

                case ConditionTypes.OrderDeskOrders:
                case ConditionTypes.OrderDeskOrdersWithSectors:
                    SetData(OrderDeskQuestIcon, r.CurrentCount, r.NeededCount, false);
                    break;

                case ConditionTypes.CityRenamed:
                    SetData(OrderDeskQuestIcon, r.CurrentCount, r.NeededCount);
                    break;

                case ConditionTypes.CollectTaxes:
                    SetData(CollectTaxesQuestIcon, r.CurrentCount, r.NeededCount, false);
                    break;

                case ConditionTypes.LoginSocialNetwork:
                    SetData(LoginSocialIcon, r.CurrentCount, r.NeededCount, false);
                    break;

                case ConditionTypes.GlobalUpgradeEquipmentToTheNextClass:
                    break;
                case ConditionTypes.UpgradeEquipmentToTheNextClass:
                    break;
                case ConditionTypes.GlobalUpgradeEquipmentToTheNextLevel:
                    break;
                case ConditionTypes.UpgradeEquipmentToTheNextLevel:
                    break;
                case ConditionTypes.CertainQuestDone:
                    break;

                case ConditionTypes.UpgradePowerStation:
                    SetData(PowerStationIcon, r.CurrentCount, r.NeededCount,
                        false); // апгрейды приходят с нулевого значения
                    break;

                case ConditionTypes.WinBurgerGame:
                    SetData(WinBurgetIcon, r.CurrentCount, r.NeededCount, false);
                    break;

                case ConditionTypes.UseCasinoFreeSpin:
                    SetData(GetBuildingIcon(buildingsIconsManager, new MapObjectId(r.Id)), r.CurrentCount, r.NeededCount);
                    break;

                case ConditionTypes.ConnectBuildingToRoad:
                    SetData(RoadIcon, r.CurrentCount, r.NeededCount, false);
                    break;

                case ConditionTypes.CoverAmountBuildingsByService:
                case ConditionTypes.CoverBuildingsWithAreaOfEffect:
                    SetData(GetBuildingIcon(buildingsIconsManager, new MapObjectId(r.Id)), r.CurrentCount, r.NeededCount);
                    break;

                default:
                    SetData(null, r.CurrentCount, r.NeededCount, false);
                    break;
            }
        }

        private bool IsUpgradeQuest()
        {
            return
                (CurrentCondition.ConditionType == ConditionTypes.UpgradePowerStation) ||
                (CurrentCondition.ConditionType == ConditionTypes.HaveBuilding && CurrentCondition.Level != 0) ||
                CurrentCondition.ConditionType == ConditionTypes.CertainBuildingTypeBuilded;
        }

        private void SetData(Sprite image, int current, int needed, bool disableClickSignal = true)
        {
            _signalOnQuestRequirmentItemClick = disableClickSignal ? null : _signalOnQuestRequirmentItemClick;
            RequirementIcon.enabled = true;
            RequirementIcon.sprite = image;
            DisableBuildBack();
            SetData(current, needed, false);
        }

        private void SetUpgradeData(Sprite image, int current, int needed)
        {
            RequirementIcon.enabled = true;
            RequirementIcon.sprite = image;
            DisableBuildBack();
            SetData(current, needed, false);
        }

        private void SetData(int current, int needed, bool isBuild)
        {
            DisableBuildBack();
            if (current == needed)
            {
                CounterBackground.sprite = CompleteCountSprite;
                if (isBuild)
                {
                    _questCompletedIcon.SetActive(true);
                }

                _readyMark.SetActive(true);

                _signalOnQuestRequirmentItemClick = null;
            }
            else
            {
                CounterBackground.sprite = NotCompleteCountSprite;
                if (isBuild)
                {
                    _questNotCompletedIcon.SetActive(true);
                }

                _readyMark.SetActive(false);
            }

            ItemsCount.text = string.Format("{0}/{1}", current, needed);
        }

        private void DisableBuildBack()
        {
            _questCompletedIcon.SetActive(false);
            _questNotCompletedIcon.SetActive(false);
            _readyMark.SetActive(false);
        }

        private void OnTooltipPanelHide(UIPanelView uiPanelView)
        {
            uiPanelView.Hide();

            if (IsSelected)
            {
                IsSelected = false;
            }
        }

       
        private Sprite GetBuildingIcon(IIconProvider buildingsIconsManager, MapObjectId id)
        {            
            return buildingsIconsManager.GetBuildingSprite(id);
        }

        public override void OnPush()
        {
            base.OnPush();

            CurrentCondition = null;
            _signalOnQuestRequirmentItemClick = null;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (_signalOnQuestRequirmentItemClick == null)
                return;

            if (!_ignoreSelect)
            {
                IsSelected = !IsSelected;
            }
            _ignoreSelect = false;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (IsSelected)
            {
                IsSelected = false;
                _ignoreSelect = true;
            }
        }

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected != value)
                {
                    _isSelected = value;
                    _signalOnQuestRequirmentItemClick.Dispatch(this);
                }
            }
        }
    }
}