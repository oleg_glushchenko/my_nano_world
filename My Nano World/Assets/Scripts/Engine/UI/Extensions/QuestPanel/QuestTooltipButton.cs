﻿using UnityEngine;
using System.Collections;
using strange.extensions.signal.impl;
using UnityEngine.EventSystems;

namespace NanoReality.Engine.UI.Extensions.QuestPanel
{
    public class QuestTooltipButton : MonoBehaviour, IPointerDownHandler
    {
        public Signal SignalOnPointerDown = new Signal();

        public void OnPointerDown(PointerEventData eventData)
        {
            SignalOnPointerDown.Dispatch();
        }
    }
}