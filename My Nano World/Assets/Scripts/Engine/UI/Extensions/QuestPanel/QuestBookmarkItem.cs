﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Вкладка для панели квестов
/// </summary>
public class QuestBookmarkItem : MonoBehaviour
{
    #region InspectorFields
    /// <summary>
    /// Таба выбрана
    /// </summary>
    [SerializeField]
    private GameObject _activeTab;

    /// <summary>
    /// Таба не выбрана
    /// </summary>
    [SerializeField]
    private GameObject _notActiveTab;
    #endregion

    /// <summary>
    /// Toggle табы
    /// </summary>
    public Toggle Toggle;

    /// <summary>
    /// Обновляем статус для вкладки
    /// </summary>
    public void UpdateBookmarkState()
    {
        _activeTab.SetActive(Toggle.isOn);
        _notActiveTab.SetActive(!Toggle.isOn);
    }
}