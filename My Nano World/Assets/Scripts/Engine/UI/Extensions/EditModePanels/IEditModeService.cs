﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Game.Services;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
using NanoReality.GameLogic.Configs;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using UnityEngine;

namespace NanoReality.Engine.UI.Extensions.EditModePanels
{
    public interface IEditModeService : IGameService
    {
	    Dictionary<int, IMapObject> CachedMapObjectById { get; }
	    EditModeLayoutData LayoutData { get; }
	    bool IsEditModeEnable { get; }
	    public bool IsAOEEnable { get; }
	    bool IsEraseEnable { get; }
	    void GetLayouts(Action<List<EditModeLayout>> onComplete);
	    void GetLayoutData(int layoutId, Action updateStorage);
	    void ClearMap(Action onComplete);
	    void RemoveBuilding(MapObjectView mapObjectView);
	    void ApplyLayout(Action<bool> onComplete, int layoutId = -1);
	    void ChangeAOE(int mapId, int aoeType, Action<EditModeHappinessMap> onComplete);
	    void MoveBuilding(MMapObject mapObject, Action onComplete);
	    void RestoreBuilding(IMapObject mapObject, Vector2Int newPosition);
	    void AddRoad(List<Vector2Int> positions, string bss, Action<CreateRoadResponse> onComplete);
	    void RemoveRoad(List<Id_IMapObject> roads, string bss, Action<DeleteRoadResponse> onComplete);
	    void CancelEditMode();
    }
}
