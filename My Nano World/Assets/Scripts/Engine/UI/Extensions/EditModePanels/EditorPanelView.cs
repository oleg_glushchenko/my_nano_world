﻿using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.UI.Core.Components;
using Assets.NanoLib.UI.Core.Views;
using NanoReality.Core.UI;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

namespace NanoReality.Engine.UI.Extensions.EditModePanels
{
    public class EditorPanelView : UIPanelView
    {
        [SerializeField] private TextButton _eraseButton;
        [SerializeField] private Button _removeAllButton;
        [SerializeField] private Button _areaButton;
        [SerializeField] private Button _photoButton;
        [SerializeField] private Button _saveButton;
        [SerializeField] private Button _cancelButton;
        [SerializeField] private Button _returnButton;
        [SerializeField] private Button _buildRoadButton;
        [SerializeField] private Button _removeRoadButton;
        [SerializeField] private Button _filterByHeavyIndustryButton;
        [SerializeField] private Button _filterByTownButton;
        [SerializeField] private Button _filterByFarmButton;
        [SerializeField] private CanvasGroup _canvasGroup;
        [SerializeField] private EditModeHappinessPanel _editModeHappinessPanel;
        [SerializeField] public ScrollSnap _scrollSnap;
        [SerializeField] public StorageItem _buildingItemPrefab;
        [SerializeField] public GameObject _lock;

        public ItemContainer BuildingsContainer;
        public TextButton EraseButton => _eraseButton;
        public Button RemoveAllButton => _removeAllButton;
        public Button SaveButton => _saveButton;
        public Button CancelButton => _cancelButton;
        public Button FilterByHeavyIndustryButton => _filterByHeavyIndustryButton;
        public Button FilterByTownButton => _filterByTownButton;
        public Button FilterByFarmButton => _filterByFarmButton;
        public Button BuildRoadButton => _buildRoadButton;
        public Button RemoveRoadButton => _removeRoadButton;
        public Button ReturnButton => _returnButton;
        public Button AOEButton => _areaButton;
        public GameObject Lock => _lock;

        public EditModeHappinessPanel EditModeHappinessPanel => _editModeHappinessPanel;

        [PostConstruct]
        public void PostConstruct()
        {
            BuildingsContainer.InstaniteContainer(_buildingItemPrefab, 0);  
        }

        public override void Show()
        {
            base.Show();
            _photoButton.onClick.AddListener(ShowPhotoModeState);
            _returnButton.onClick.AddListener(HidePhotoModeState);
        }

        public override void Hide()
        {
            base.Hide();
            _photoButton.onClick.RemoveListener(ShowPhotoModeState);
            _returnButton.onClick.RemoveListener(HidePhotoModeState);
        }

        public void ClearCurrentItems()
        {
            BuildingsContainer.ClearCurrentItems();
            _filterByFarmButton.interactable = false;
            _filterByTownButton.interactable = false;
            _filterByHeavyIndustryButton.interactable = false;
        }

        public void ChangePhotoModeState(bool state, bool canBeClose = false)
        {
            _canvasGroup.alpha = state ? 0 : 1;
            _canvasGroup.blocksRaycasts = !state;
            ChangePhotoModeBackButtonState(canBeClose);
        }
        
        public void ChangePhotoModeBackButtonState(bool state)
        {
            _returnButton.gameObject.SetActive(state);
            _returnButton.interactable = state;
        }

        public StorageItem AddItem()
        {
            return BuildingsContainer.AddItem() as StorageItem;
        }

        public void UpdateScrolls()
        {
            _scrollSnap.UpdateListItemPositions();
            _scrollSnap.ChangePage(0);
        }
        
        private void ShowPhotoModeState() => ChangePhotoModeState(true, true);
        private void HidePhotoModeState() => ChangePhotoModeState(false, true);
    }
}