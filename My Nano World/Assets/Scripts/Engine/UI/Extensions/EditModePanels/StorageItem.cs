﻿using System;
using UnityEngine;
using Assets.NanoLib.Utilities.Pulls;
using Assets.Scripts.MultiLanguageSystem.Logic;
using NanoReality.Engine.UI.Extensions.BuildingsShopPanel;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.Services;
using strange.extensions.signal.impl;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

namespace NanoReality.Engine.UI.Extensions.EditModePanels
{
    public class StorageItem : GameObjectPullable,
        IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        [SerializeField] private BuildingItemTab _infoTab;
        [SerializeField] private Button _buyButton;
        [SerializeField] private float _dragWaitingTime = 0.5f;
        [SerializeField] private float _minDragLengthForBuying = 30;
        [Tooltip("Angle from Up to Right")]
        [SerializeField] [Range(0f, 90f)] 
        
        private float _dragAngleForBuying = 70f;
        private Sprite _currentBuildingSprite;
        private bool _ignoreSelect;
        private ScrollSnap _scrollSnap;
        private float _pointDownTime;
        
        public bool IsDragNow { get; private set; }
        public IMapObject CurrentMapObject { get; private set; }
        public bool IsLongTap => _pointDownTime > 0 && !IsDragNow && Time.timeSinceLevelLoad > _pointDownTime + _dragWaitingTime;

        public Signal<StorageItem> SignalOnBuyBuilding = new Signal<StorageItem>();
        public Signal<IMapObject> SignalOnGoToBuilding = new Signal<IMapObject>();
        public Signal<BuildingItem> SignalOnSelectChanged = new Signal<BuildingItem>();
        public Signal SignalOnPointerDown = new Signal();
        public Signal SignalOnPointerUp = new Signal();

        public void UpdateItem(IIconProvider iconProvider, ScrollSnap scroll, IMapObject currentMapObject )
        {
            _scrollSnap = scroll;
            CurrentMapObject = currentMapObject;
            _currentBuildingSprite = iconProvider.GetBuildingSprite(new MapObjectId(currentMapObject.Id.Id));
            
            if (_currentBuildingSprite != null)
            {
                _infoTab.BuildingImage.sprite = _currentBuildingSprite;
            }
            
            _infoTab.BuildingImage.material = null;
            _infoTab.BuildingTitle.text = currentMapObject.BalanceName;
            _buyButton.onClick.AddListener(OnClickBuyButton);
        }

        #region Pullable

        public override void OnPop()
        {
            base.OnPop();
            IsDragNow = false;
            _pointDownTime = 0f;
        }

        public override void OnPush()
        {
            SignalOnSelectChanged.RemoveAllListeners();
            SignalOnSelectChanged.RemoveAllOnceListeners();
            SignalOnBuyBuilding.RemoveAllListeners();
            SignalOnBuyBuilding.RemoveAllOnceListeners();

            SignalOnPointerDown.RemoveAllListeners();
            SignalOnPointerUp.RemoveAllListeners();
            _buyButton.onClick.RemoveAllListeners();

            CurrentMapObject = null;
            _currentBuildingSprite = null;
            _infoTab.BuildingImage.sprite = null;
            base.OnPush();
        }
        
        #endregion
        
        #region buy building signal dispatching 
 
        private void OnClickBuyButton()
        {
            DispatchBuyBuilding();
            DispatchGoToBuilding();
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            IsDragNow = true;
            _scrollSnap.OnBeginDrag(eventData);      
            _scrollSnap.ScrollRect.OnBeginDrag(eventData);
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (!IsDragNow)
            {
                return;
            }
            
            _scrollSnap.OnDrag(eventData);
            _scrollSnap.ScrollRect.OnDrag(eventData);
            
            var diff = eventData.position - eventData.pressPosition;
            var angle = Vector2.Angle(diff.normalized, Vector2.up);
            
            if (angle < _dragAngleForBuying && diff.magnitude > _minDragLengthForBuying)
            {
                DispatchBuyBuilding();
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            _scrollSnap.OnEndDrag(eventData);
            _scrollSnap.ScrollRect.OnEndDrag(eventData);
        }

        private void DispatchBuyBuilding()
        {
            SignalOnBuyBuilding.Dispatch(this);
            IsDragNow = false;
            _pointDownTime = 0f;
            OnPush();
        }

        private void DispatchGoToBuilding()
        {
            SignalOnGoToBuilding.Dispatch(this.CurrentMapObject);
        }
        
        #endregion
    }
}
