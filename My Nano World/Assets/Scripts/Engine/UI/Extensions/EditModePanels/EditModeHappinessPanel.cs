﻿
using Engine.UI.Components;
using strange.extensions.mediation.impl;
using TMPro;
using UnityEngine;

namespace NanoReality.Engine.UI.Extensions.EditModePanels
{
	public class EditModeHappinessPanel : View
	{
		[SerializeField] private TextMeshProUGUI _happinessCounter;
		[SerializeField] private SpriteChanger _happinessSprite;
		
		private const float MaxHappiness = 100f;

		public void UpdatePanel(int value)
		{
			_happinessCounter.text = value + "%";
			_happinessSprite.Value = value / MaxHappiness;
		}
	}
}