﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Extensions;
using GameLogic.BuildingSystem.MapObjects.Models.impl;
using NanoLib.Core.Logging;
using NanoReality.Engine.BuildingSystem.CityCamera;
using NanoReality.Game.Attentions;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Configs;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using strange.extensions.signal.impl;
using UnityEngine;

namespace NanoReality.Engine.UI.Extensions.EditModePanels
{
	public class SignalOnEditModeRemoveBuild : Signal { }
	public class SignalOnEditModeHappinessUpdate : Signal<int> { }
	public class SignalOnEditModeStorageUpdate : Signal<bool> { }
	public class SignalOnEditModeAOEStateChange : Signal<bool> { }

	public class EditModeService : IEditModeService
	{
		[Inject] public SignalOnEditModeStateChange jSignalOnEditModeStateChange { get; set; }
		[Inject] public SignalOnEditModeEraseStateChange jSignalOnEditModeEraseStateChange { get; set; }
		[Inject] public SignalOnEditModeRemoveBuild jSignalOnEditModeRemoveBuild { get; set; }
		[Inject] public SignalOnEditModeHappinessUpdate jSignalOnEditModeHappinessUpdate { get; set; }
		[Inject] public SignalOnEditModeStorageUpdate jSignalOnEditModeApplyStateChange { get; set; }
		[Inject] public SignalOnEditModeAOEStateChange jSignalOnEditModeAOEStateChange { get; set; }
		[Inject] public GetScreenshotSignal jGetScreenshotSignal { get; set; }
		[Inject] public IServerCommunicator jServerCommunicator { get; set; }
		[Inject] public IGameManager jGameManager { get; set; }
		[Inject] public IBuildingAttentionsController jBuildingAttentionsController { get; set; }
		[Inject] public IMapObjectsGenerator jMapObjectsGenerator { get; set; }

		public EditModeLayoutData LayoutData { get; private set; }
		public bool IsEditModeEnable { get; private set; }
		public bool IsEraseEnable { get; private set; }
		public bool IsAOEEnable { get; private set; }

		private Dictionary<BusinessSectorsTypes, List<IMapObject>> CachedMapObjectByBss { get; set; }
		public Dictionary<int, IMapObject> CachedMapObjectById { get; private set; }

		public void Init()
		{
			jSignalOnEditModeStateChange.AddListener(StateChange);
			jSignalOnEditModeEraseStateChange.AddListener(EraseStateChange);
			jSignalOnEditModeAOEStateChange.AddListener(SetAOEState);
		}

		public void Dispose()
		{
			CachedMapObjectByBss = null;
			jSignalOnEditModeStateChange.RemoveListener(StateChange);
			jSignalOnEditModeEraseStateChange.RemoveListener(EraseStateChange);
			jSignalOnEditModeAOEStateChange.RemoveListener(SetAOEState);
		}

		private void CacheCities(bool state)
		{
			if (!state)
			{
				CachedMapObjectByBss = null;
				CachedMapObjectById = null;
				return;
			}

			CachedMapObjectByBss = new Dictionary<BusinessSectorsTypes, List<IMapObject>>();
			CachedMapObjectById = new Dictionary<int, IMapObject>();

			var curCity = jGameManager.CurrentUserCity.CityMaps;
			foreach (var city in curCity)
			{
				var bss = city.BusinessSector;
				if (!CachedMapObjectByBss.ContainsKey(bss))
				{
					CachedMapObjectByBss.Add(bss, new List<IMapObject>());
				}

				foreach (var mapObject in city.MapObjects)
				{
					CachedMapObjectByBss[bss].Add(mapObject);
					CachedMapObjectById.Add(mapObject.MapID, mapObject);
				}
			}
		}

		private void StateChange(bool state)
		{
			IsEditModeEnable = state;
			CacheCities(state);
		}

		private void EraseStateChange(bool state)
		{
			IsEraseEnable = state;
		}

		private void SetAOEState(bool state)
		{
			IsAOEEnable = state;
		}

		public void MoveBuilding(MMapObject mapObject, Action onComplete)
		{
			void OnComplete(EditModeHappinessMap data)
			{
				mapObject.IsConnectedToGeneralRoad = data.BuildingData.ExitMainRoad;
				onComplete.Invoke();
				UpdateHappiness(data.Happiness);
			}

			var layoutId = LayoutData.MapBuildingData.First(b => b.BuildingId == mapObject.MapID).LayoutObjectId;

			if (layoutId == 0)
			{
				StringBuilder ids = new StringBuilder();
				foreach (var build in LayoutData.MapBuildingData)
				{
					ids.Append($" {build.BuildingId}({build.LayoutObjectId}) ");
				}
				$"We try to move {mapObject.MapID} but in Layout we have just: {ids}".LogError(LoggingChannel.Buildings);
			}
			jServerCommunicator.EditModeMoveBuilding(layoutId, mapObject.MapPosition.ToVector2Int(), OnComplete);
		}

		public void GetLayouts(Action<List<EditModeLayout>> onComplete)
		{
			jServerCommunicator.GetEditModeLayouts(onComplete);
		}

		public void GetLayoutData(int layoutId, Action updateStorage)
		{
			void OnComplete(EditModeLayoutData editModeLayoutData)
			{
				LayoutData = editModeLayoutData;
				jGameManager.ClearMapsView();
				RestoreMapObjects(() =>
				{
					updateStorage.Invoke();
					UpdateHappiness(editModeLayoutData.Happiness);
					UpdateApplyButton();
				});
			}

			jServerCommunicator.GetEditModeLayoutData(layoutId, OnComplete);
		}

		public void ClearMap(Action onComplete)
		{
			void OnComplete(EditModeLayoutData data)
			{
				LayoutData = data;
				jGameManager.ClearMapsView();
				RestoreMapObjects();
				onComplete.Invoke();
				UpdateHappiness(data.Happiness);
				UpdateApplyButton();
			}

			jServerCommunicator.EditModeClearMap(LayoutData.LayoutId, OnComplete);
		}

		private void RestoreMapObjects(Action callback = null)
		{
			void FinishLoad()
			{
				callback?.Invoke();
				jGameManager.UpdateRoads();
			}
			
			var dataCity = LayoutData.MapBuildingData;
			var curCity = jGameManager.CurrentUserCity.CityMaps;
			int cityCount = 0;
			
			foreach (var city in curCity)
			{
				AddLayoutRoadsToCity(city);
				List<IMapObject> removableMapObjects = new List<IMapObject>();
				foreach (var cityMapObject in city.MapObjects)
				{
					var dataBuilding = dataCity.FirstOrDefault(b => b.BuildingId == cityMapObject.MapID);

					if (dataBuilding == null)
					{
						// check for base road on each city
						if (!(cityMapObject is MRoad) || cityMapObject.Level != 200)
						{
							removableMapObjects.Add(cityMapObject);
						}

						continue;
					}

					cityMapObject.MapPosition = new Vector2F(dataBuilding.Position.x, dataBuilding.Position.y);

					if (cityMapObject is MRoad)
					{
						cityMapObject.MapID = dataBuilding.LayoutObjectId;
					}
					else
					{
						cityMapObject.IsConnectedToGeneralRoad = dataBuilding.ExitMainRoad;
					}

					if (cityMapObject is IAoeBuilding building)
					{
						building.AoeLayoutType = dataBuilding.AoeType;
					}
				}

				foreach (var removeMapObject in removableMapObjects)
				{
					city.MapObjects.Remove(removeMapObject);
				}

				city.RestoreMap(() =>
				{
					cityCount++;
					
					if (cityCount == curCity.Count)
					{
						FinishLoad();
					}
				});
			}
		}

		private void AddLayoutRoadsToCity(ICityMap city)
		{
			List<EditModeMapBuildingData> roadsData =
				LayoutData.MapBuildingData.FindAll(m => m.BuildingId == -1 && m.BusinessSectorsType == city.BusinessSector);

			foreach (var roadData in roadsData)
			{
				city.MapObjects.Add(GetRoadPrototype(roadData));
				roadData.BuildingId = roadData.LayoutObjectId;
			}
		}

		private IMapObject GetRoadPrototype(EditModeMapBuildingData roadData)
		{
			const int roadId = 31;
			var prototype = jMapObjectsGenerator.GetMapObjectModel(roadId, 0);

			prototype.OnInitOnMap(prototype);
			prototype.SectorId = roadData.BusinessSectorsType.GetBusinessSectorId();
			prototype.MapID = roadData.LayoutObjectId;
			prototype.MapObjectType = MapObjectintTypes.Road;
			prototype.MapPosition = new Vector2F(roadData.Position.x, roadData.Position.y);
			return prototype;
		}

		public void RestoreBuilding(IMapObject mapObject, Vector2Int newPosition)
		{
			void OnComplete(EditModeHappinessMap data)
			{
				mapObject.IsConnectedToGeneralRoad = data.BuildingData.ExitMainRoad;
				mapObject.MapPosition = new Vector2F(data.BuildingData.Position.x, data.BuildingData.Position.y);
				mapObject.CommitBuilding(mapObject.MapID);
				mapObject.IsVerificated = true;

				LayoutData.MapBuildingData.Add(data.BuildingData);
				RemoveBuildingFormStorage(data.BuildingData.BusinessSectorsType, data.BuildingData.BuildingId);
				UpdateHappiness(data.Happiness);
				UpdateApplyButton();
			}

			jServerCommunicator.EditModeRestoreBuilding(LayoutData.LayoutId, mapObject.MapID, newPosition, OnComplete);
		}

		private void RemoveBuildingFormStorage(BusinessSectorsTypes bss, int id)
		{
			var storageBuildingData = LayoutData.StorageBuildingData.First(m => m.BusinessSectorsType == bss);
			storageBuildingData.Buildings.Remove(id);
			if (storageBuildingData.Buildings.IsNullOrEmpty())
			{
				LayoutData.StorageBuildingData.Remove(storageBuildingData);
			}
		}

		public void AddRoad(List<Vector2Int> positions, string bss, Action<CreateRoadResponse> onComplete)
		{
			void OnComplete(EditModeCreateRoadResponse data)
			{
				UpdateHappiness(data.Happiness);
				onComplete.Invoke(data);
			}

			jServerCommunicator.EditModeAddRoad(LayoutData.LayoutId, bss, positions, OnComplete);
		}

		public void RemoveRoad(List<Id_IMapObject> roads, string bss, Action<DeleteRoadResponse> onComplete)
		{
			void OnComplete(EditModeDeleteRoadResponse data)
			{
				
				UpdateHappiness(data.Happiness);
				onComplete.Invoke(data);
			}

			jServerCommunicator.EditModeRemoveRoad(LayoutData.LayoutId, bss, roads, OnComplete);
		}

		public void RemoveBuilding(MapObjectView mapObjectView)
		{
			if (mapObjectView.MapObjectModel is MRoad && mapObjectView.MapObjectModel.Level == 200)
			{
				return;
			}

			var layoutId = LayoutData.MapBuildingData.First(b => b.BuildingId == mapObjectView.MapId).LayoutObjectId;

			void OnComplete(EditModeHappinessMap data)
			{
				LayoutData.StorageBuildingData = data.StorageBuildingData;
				jSignalOnEditModeRemoveBuild.Dispatch();
				jBuildingAttentionsController.RemoveBuildingStatesAttentionView(mapObjectView.MapId);

				jGameManager.GetMap(mapObjectView.MapObjectModel.SectorId).RemoveMapObject(mapObjectView.MapObjectModel);
				UpdateHappiness(data.Happiness);
				UpdateApplyButton();
			}

			jServerCommunicator.EditModeRemoveBuilding(layoutId, OnComplete);
		}

		public void ApplyLayout(Action<bool> onComplete, int layoutId = -1)
		{
			void OnComplete(bool result)
			{
				jGameManager.RestoreCityMapFromServer();
				onComplete.Invoke(result);
			}

			void Apply(string data)
			{
				int id = layoutId == -1 ? LayoutData.LayoutId : layoutId;
				jServerCommunicator.EditModeApplyLayout(id, data, OnComplete);
			}

			if (layoutId == -1)
			{
				jGetScreenshotSignal.Dispatch(Apply);
				return;
			}

			Apply("");
		}

		public void CancelEditMode()
		{
			void OnComplete(bool result)
			{
				$"Cancel edit mode {result}".LogWarning();
				jGameManager.RestoreCityMapFromServer();
			}

			void Cancel(string data)
			{
				jServerCommunicator.EditModeCancelLayout(LayoutData.LayoutId, data, OnComplete);
			}

			jGetScreenshotSignal.Dispatch(Cancel);
		}

		public void ChangeAOE(int mapId, int aoeType, Action<EditModeHappinessMap> onComplete)
		{
			void OnComplete(EditModeHappinessMap result)
			{
				onComplete.Invoke(result);
				UpdateHappiness(result.Happiness);
			}

			var layoutMapId = LayoutData.MapBuildingData.First(b => b.BuildingId == mapId).LayoutObjectId;
			jServerCommunicator.EditModeChangeAOE(layoutMapId, aoeType, OnComplete);
		}

		private void UpdateHappiness(int value)
		{
			jSignalOnEditModeHappinessUpdate.Dispatch(value);
		}

		private void UpdateApplyButton()
		{
			if (LayoutData.StorageBuildingData.Any(storageBuildingData => storageBuildingData.Buildings.Any(id => !(CachedMapObjectById[id] is AoeBuilding))))
			{
				jSignalOnEditModeApplyStateChange.Dispatch(false);
				return;
			}

			jSignalOnEditModeApplyStateChange.Dispatch(true);
		}
	}
}