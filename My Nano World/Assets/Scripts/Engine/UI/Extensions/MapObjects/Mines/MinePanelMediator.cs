﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.api.Mines;
using Engine.UI.Views.Panels.ProductionPanels;
using NanoReality.GameLogic.MinePanel;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using UnityEngine;

namespace Assets.Scripts.Engine.UI.Extensions.MapObjects.Mines
{
    public sealed class MinePanelMediator : ProductionPanelMediator<IMineObject, MinePanelView>
    {
        protected IList<IProduceSlot> UnlockedSlots => CurrentModel.CurrentProduceSlots.Where(c => c.IsUnlocked).ToList();
        
        protected override ProduceSlotView AddProductionSlot()
        {
            var slotView = base.AddProductionSlot();
            slotView.DragObjectExit += DragObjectExit;
            slotView.IsFirstInSlotQueue = true;
            return slotView;
        }
        
        protected override void RemoveProductionSlot(ProduceSlotView slotView)
        {
            slotView.DragObjectExit -= DragObjectExit;
            base.RemoveProductionSlot(slotView);
        }
        
        protected override void SetProductionSlots()
        {
            for (int i = 0; i < UnlockedSlots.Count; i++)
            {
                ProduceSlotView view = View.ProduceSlotViews.Count > i
                    ? View.ProduceSlotViews[i]
                    : AddProductionSlot();

                view.SetSlot(UnlockedSlots[i]);
            }
        }
        
        protected override bool HasPlayerAllIncomingResources(IProducingItemData selectedProducingData)
        {
            return true;
        }
        
        protected override void OnProduceSlotObjectDropped(ProduceSlotView slotView)
        {
            HideSlotPreview();
            
            if (slotView.CurrentSlotCondition == SlotCondition.MakingProduct ||
                slotView.CurrentSlotCondition == SlotCondition.Finished ||
                slotView.CurrentSlotCondition == SlotCondition.WaitingResponse)
            {
                return;
            }
            
            TryProduceGood(slotView.ProduceSlot);
        }

        protected override void OnProduceSlotDragEnter(ProduceSlotView slotView)
        {
            View.ShowProductionQueueFullMessage(!CurrentModel.HasEmptySlot());
            
            if (slotView.CurrentSlotCondition == SlotCondition.MakingProduct ||
                slotView.CurrentSlotCondition == SlotCondition.Finished ||
                slotView.CurrentSlotCondition == SlotCondition.WaitingResponse)
            {
                return;
            }
            
            TryProduceGood(slotView.ProduceSlot);
            
        }
        
        private void DragObjectExit(ProduceSlotView obj)
        {
            HideSlotPreview();
        }
    }
}