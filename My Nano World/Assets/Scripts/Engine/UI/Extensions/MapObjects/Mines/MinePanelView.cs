﻿using Assets.NanoLib.UI.Core.Signals;
using Engine.UI.Views.Panels.ProductionPanels;

namespace NanoReality.GameLogic.MinePanel
{
    public sealed class MinePanelView : ProductionPanelView
    {
        [Inject] public ShowWindowSignal ShowWindowSignal { get; set; }
        [Inject] public SignalOnNeedToHideTooltip jSignalOnNeedToHideTooltip { get; set; }

        private void OnDisable()
        {
            jSignalOnNeedToHideTooltip.Dispatch();
        }
    }
}