﻿using System;
using Assets.NanoLib.UI.Core.Components;
using Assets.NanoLib.UI.Core.Views;
using Assets.NanoLib.Utilities.Pulls;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.UI.Components;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class EntertainmentCoinItem : UIPanelView, IPullableObject, IPointerClickHandler
{
    private MapObjectView _mapObjectView;
    private ItemContainer _itemContainer;

    private Image _image;

    public GameObject ImageGameObject;
    private TweenerCore<Vector3, Vector3, VectorOptions> _moveTweener;

    private IGlobalMapObjectViewSettings _globalMapObjectViewSettings { get; set; }
    //private StartUIObjectAnimationSignal _startUIObjectAnimationSignal;

    public Image Image
    {
        get
        {
            if (_image == null)
                _image = ImageGameObject.GetComponent<Image>();

            return _image;
        }
    }

    public void Init(MapObjectView mapObjectView, ItemContainer itemContainer, IGlobalMapObjectViewSettings globalMapObjectViewSettings)
    {
        _globalMapObjectViewSettings = globalMapObjectViewSettings;
        //_startUIObjectAnimationSignal = startUIObjectAnimationSignal;

        _mapObjectView = mapObjectView;
        //((IEntertainmentBuilding)_mapObjectView.MapObjectModel).coinOverBuilding = this;
        _itemContainer = itemContainer;
        Invoke("sdf", 0.1f);
        if (_mapObjectView != null)
        {
            var looker = GetComponent<MapObjectLooker>();
            if (looker != null)
            {
                looker.SetTarget(_mapObjectView);
                looker.Pivot = MapObjectLooker.PivotModes.TopSomeMore;

                float delay = Random.Range(0f, 1f);
                DOVirtual.DelayedCall(delay, StartMoving);
            }
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        var view = _mapObjectView as EntertaimentMapObjectView;

        if (view != null)
        {
            //view.IsCoinAdded = false;
            //(view.MapObjectModel as IEntertainmentBuilding).ShipAward();
        }

        RemoveCoinOverBuilding();
    }

    public void RemoveCoinOverBuilding()
    {
        _itemContainer.RemoveItem(this);
        //_startUIObjectAnimationSignal.Dispatch(transform.position, FlyDestinationType.SoftCoins, Image.sprite, ((IEntertainmentBuilding)_mapObjectView.MapObjectModel).SoftCurrencyProfitPerCycle.ToString());
    }

    private void StartMoving()
    {
        var movePosition = new Vector3(0f, 0.17f, 0f);
        _moveTweener = transform.DOMove(movePosition, _globalMapObjectViewSettings.MotionReadyProductMoveUpDownTime)
            .SetLoops(-1, LoopType.Yoyo);
    }

    #region Pullable

    public object Clone()
    {
        return Instantiate(this);
    }

    public IObjectsPull pullSource { get; set; }

    public void OnPop()
    {
        enabled = true;
        GetComponent<MapObjectLooker>().enabled = true;
        Show();
    }

    public void OnPush()
    {
        enabled = false;
        var looker = GetComponent<MapObjectLooker>();
        looker.enabled = false;
        looker.SetTarget(null);

        _moveTweener?.Kill();

        Hide();
    }

    public void FreeObject()
    {
        pullSource?.PushInstance(this);
    }

    public void DestroyObject()
    {
        Destroy(this);
    }

    public override void Hide()
    {
        base.Hide();
        Image.enabled = false;
    }

    public override void Show()
    {
        base.Show();
        Image.enabled = true;
    }

    #endregion

}