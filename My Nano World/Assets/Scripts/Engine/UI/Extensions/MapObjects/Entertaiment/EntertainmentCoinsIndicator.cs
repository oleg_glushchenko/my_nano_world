﻿using Assets.NanoLib.UI.Core.Components;
using NanoReality.Engine.UI.Extensions.EditModePanels;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Controllers;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.Happiness;
using NanoReality.GameLogic.GameManager.Models.api;
using strange.extensions.mediation.impl;

public class EntertainmentCoinsIndicator : View
{
	public EntertainmentCoinItem EntertainmentCoinItem;

	public ItemContainer ItemContainer;

	[Inject] public IGameManager jGameManager { get; set; }
	[Inject] public IConstructionController jConstructionController { get; set; }
	[Inject] public SignalOnConstructControllerChangeState jSignalOnConstructControllerChangeState { get; set; }
	[Inject] public SignalEntertainmentBuildingNeedToShipMoney jSignalEntertainmentBuildingNeedToShipMoney { get; set; }
	[Inject] public StartCityMapInitializationSignal JStartCityMapInitializationSignal { get; set; }
	[Inject] public SignalOnEditModeStateChange jSignalOnEditModeStateChange { get; set; }
	[Inject] public IEditModeService jEditModeService { get; set; }

	protected override void Awake()
	{
		base.Awake();
		ItemContainer.InstaniteContainer(EntertainmentCoinItem, 0);
	}

	[PostConstruct]
	public void PostConstruct()
	{
		jSignalEntertainmentBuildingNeedToShipMoney.AddListener(OnNeedToShipMoney);
		JStartCityMapInitializationSignal.AddListener(ClearContainer);
		jSignalOnEditModeStateChange.AddListener(ConstructionModeChanged);

		jSignalOnConstructControllerChangeState.AddListener(ConstructionModeChanged);
	}

	private void ConstructionModeChanged(bool isConstructEnabled)
	{
		bool isNotActive = jEditModeService.IsEditModeEnable || isConstructEnabled;
		gameObject.SetActive(!isNotActive);
	}

	public void ClearContainer(ICityMap data)
	{
		ItemContainer.ClearCurrentItems();
	}

	public void Show()
	{
		//prevent attention showing while some overlay view enabled
		if (jGameManager != null && jConstructionController.IsBuildModeEnabled || jConstructionController.IsRoadBuildingMode)
			return;

		gameObject.SetActive(true);
	}

	public void Hide()
	{
		gameObject.SetActive(false);
	}

	private void OnNeedToShipMoney(Id_IMapObject mapObjectId)
	{
		var model = jGameManager.CurrentUserCity.GetMapObjectInCityByID(mapObjectId) as IBuildingProduceHappiness;

		// если не найдено мапобъекта, соответствующего данному индикатору, значит этот индикатор не с этой карты сектора, и его отображать не надо
		if (model != null)
		{
			if (!model.IsFunctional())
				return;

			//if(view.IsCoinAdded)
			//     return;

			var newCoinItem = ItemContainer.AddItem() as EntertainmentCoinItem;

			if (newCoinItem != null)
			{
				// view.IsCoinAdded = true;
				//newCoinItem.Init(mapObjectView, ItemContainer, jGlobalMapObjectViewSettings, jStartUIObjectAnimationSignal);
			}
		}
	}
}