﻿using Assets.Scripts.Engine.UI.Views.Hud;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Controllers.Agricultural;
using System.Collections.Generic;
using System.Linq;
using NanoLib.Core.Services.Sound;
using NanoReality.GameLogic.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Controllers.Factories;

namespace NanoReality.Engine.UI.Extensions.Agricultural
{
    public class AgroFieldPanelMediator : BuildingPanelMediator<IAgroFieldBuilding, AgroFieldPanelView>
    {
        private readonly LinkedList<IAgroFieldBuilding> _fieldList = new LinkedList<IAgroFieldBuilding>();
        private LinkedListNode<IAgroFieldBuilding> _currentField;
        private bool _isSwitched;
        
        [Inject] public OnChangeGrowingState jOnChangeGrowingState { get; set; }
        [Inject] public OnPlayerTooltipClickSignal jOnPlayerTooltipClickSignal { get; set; }
        [Inject] public OnHudBottomPanelIsVisibleSignal jOnHudBottomPanelIsVisibleSignal { get; set; }
        [Inject] public OnHudButtonsIsClickableSignal jOnHudButtonsIsClickableSignal { get; set; }
        [Inject] public ApplyFilterToReadyProducts jApplyFilterToReadyProducts { get; set; }

        protected override void OnShown()
        {
            base.OnShown();

            if (!_isSwitched) SetFieldsList();

            jOnHudBottomPanelIsVisibleSignal.Dispatch(false);
            jOnHudButtonsIsClickableSignal.Dispatch(false);
            
            View.OnSwitchField += Switch;
            jOnPlayerTooltipClickSignal.AddListener(Hide);
            jApplyFilterToReadyProducts.Dispatch(CurrentModel.MapID, true);
            jOnChangeGrowingState.AddListener(OnGrowingStateChanged);
            jSoundManager.Play(SfxSoundTypes.AppWindow, SoundChannel.SoundFX);
        }

        protected override void OnHidden()
        {
            base.OnHidden();

            jOnHudBottomPanelIsVisibleSignal.Dispatch(true);
            jOnHudButtonsIsClickableSignal.Dispatch(true);
            View.OnSwitchField -= Switch;
            jApplyFilterToReadyProducts.Dispatch(CurrentModel.MapID, false);
            jOnChangeGrowingState.RemoveListener(OnGrowingStateChanged);
            jOnPlayerTooltipClickSignal.RemoveListener(Hide);
            jSoundManager.Play(SfxSoundTypes.CloseWindow, SoundChannel.SoundFX);
        }

        private void OnGrowingStateChanged(IAgroFieldBuilding agroField)
        {
            if (ViewVisible)
            {
                var f = CurrentModel;
                if (!f.IsHarvesting && f.CurrentState == AgriculturalFieldState.Completed)
                {
                    View.UpdatePanel();
                }
            }
        }

        private void SetFieldsList()
        {
            _fieldList.Clear();
            var map = View.jGameManager.GetMap(CurrentModel.SectorId);
            foreach (var field in map.MapObjects.Where(f => f is IAgroFieldBuilding).Cast<IAgroFieldBuilding>())
            {
                if (CurrentModel == field)
                {
                    _fieldList.AddFirst(field);
                }
                else
                {
                    _fieldList.AddLast(field);
                }
            }

            _currentField = _fieldList.First;
        }

        private void Switch(bool forward)
        {
            if (forward)
            {
                _currentField = _currentField.Next ?? _fieldList.First;
            }
            else
            {
                _currentField = _currentField.Previous ?? _fieldList.Last;
            }

            CurrentModel = _currentField.Value;
            _isSwitched = true;

            Show(CurrentModel);
            _isSwitched = false;
            FocusOnBuilding();
        }
    }
}