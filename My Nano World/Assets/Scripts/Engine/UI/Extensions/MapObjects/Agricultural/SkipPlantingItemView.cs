﻿using strange.extensions.signal.impl;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Extensions.Agricultural
{
    public class SkipPlantingItemView : MonoBehaviour
    {
        public Signal OnBoostButtonClickSignal = new Signal();
        public Button SkipButton;
        public Text CostText;

        public void SetProduct(int boostCost)
        {
            CostText.text = boostCost.ToString();
        }

        public void OnBoostButtonClick()
        {
            OnBoostButtonClickSignal.Dispatch();
        }
    }
}