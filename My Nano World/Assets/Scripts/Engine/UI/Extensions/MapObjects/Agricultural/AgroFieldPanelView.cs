﻿using System;
using System.Collections.Generic;
using NanoLib.Services.InputService;
using Assets.NanoLib.Utilities;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.api;
using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Controllers.Agricultural;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using UnityEngine;
using System.Linq;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using UnityEngine.UI;
using Assets.Scripts.GameLogic.Utilites;
using NanoReality.Engine.UI.Views.BuildingsShopPanel;
using Assets.NanoLib.UI.Core.Signals;
using Assets.Scripts.Engine.UI.Views.Tooltips;
using Assets.Scripts.Engine.UI.Views.Warehouse;
using Assets.Scripts.MultiLanguageSystem.Logic;
using DG.Tweening;
using Engine.UI.Components;
using NanoLib.Core.Services.Sound;
using NanoLib.Core.Timers;
using NanoLib.UI.Core;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.MapObjects;
using TMPro;

namespace NanoReality.Engine.UI.Extensions.Agricultural
{
    public enum AgriculturalMode
    {
        Planting,
        Growing,
        Harvesting
    }

    public class AgroFieldPanelView : BuildingPanelView<AgroFieldBuilding>
    {
        #region Inspector fields

        [SerializeField] private GameObject _rightArrow;

        [SerializeField] private TextMeshProUGUI _headerText;
        [SerializeField] private RectTransform _headerPanel;

        [SerializeField] private DraggableProductItem _cropItem;
        [SerializeField] private GameObject _cropPanel;
        
        [SerializeField] private GameObject _productionPanel;
        [SerializeField] private GameObject _fertilizePanel;

        [SerializeField] private Button _nextFieldArrow;
        [SerializeField] private Button _previousFieldArrow;
        
        [SerializeField] private ProductTooltip _tooltip;
        
        [SerializeField] private ProgressBar _progressBar;
        [SerializeField] private TextMeshProUGUI _timerText;
        
        [SerializeField] private TextMeshProUGUI _skipPrice;
        [SerializeField] private Button _skipButton;
        [SerializeField] private Button _overlayButton;

        [SerializeField] private List<DraggableProductItem> _draggablePlantItems;
        [SerializeField] private CanvasGroup _panelCanvasGroup;

        #endregion

        #region Injected fields

        [Inject] public IProductsData jProductsData { get; set; }
        [Inject] public ISkipIntervalsData jIntervalsData { get; set; }
        [Inject] public IPlayerExperience jPlayerExperience { get; set; }
        [Inject] public IPlayerResources jPlayerResources { get; set; }
        [Inject] public ISoundManager jSoundManager { get; set; }

        [Inject] public ServerTimeTickSignal jServerTimeTickSignal { get; set; }
        [Inject] public SignalOnNeedToShowTooltip jSignalOnNeedToShowTooltip { get; set; }
        [Inject] public SignalOnNeedToHideTooltip jSignalOnNeedToHideTooltip { get; set; }
        [Inject] public ShowWindowSignal ShowWindowSignal { get; set; }
        [Inject] public OnItemDragStartedSignal OnItemDragStartedSignal { get; set; }
        [Inject] public OnItemDragEndedSignal OnItemDragEndedSignal { get; set; }
        [Inject] public SignalOnSkipPriceChanged jSignalOnSkipPriceChanged { get; set; }
        
        [Inject] public IHardTutorial jHardTutorial { get; set; }
        [Inject] public IHintTutorial jHintTutorial { get; set; }

        #endregion

        public List<DraggableProductItem> DraggableProductItems => _draggablePlantItems;
        public DraggableProductItem CropItem => _cropItem;
        public GameObject СropPanel => _cropPanel;
        
        private IAgroFieldBuilding _fieldModel;
        private AgriculturalMode _currentMode;
        private bool _isNotEnough;

        private static Id_IMapObject currentFieldId;

        public Button SkipButton => _skipButton;

        public event Action<bool> OnSwitchField;
        private Tweener _panelFadeTween;
        private IEnumerable<IProducingItemData> _availablePlantsDatas;
        private IEnumerable<IProducingItemData> _lockedPlantsDatas;
        private int _buildingsLayerMask;
        private int _groundLayerMask;
        private bool _needUpdateConfirmPopup;

        public IAgroFieldBuilding FieldModel => _fieldModel;

        public override void Initialize(object parameters)
        {
            base.Initialize(parameters);
            
            _fieldModel = parameters as IAgroFieldBuilding;
            _lockedPlantsDatas = GetNextAvailablePlantsQuery();
            _availablePlantsDatas = GetAvailablePlantsQuery();
            
            _currentMode = GetCurrentMode(_fieldModel);
            _isNotEnough = false;

            currentFieldId = _fieldModel.MapID;
            UpdatePanel();
        }

        public override void Show()
        {
            base.Show();
            AddListeners();
            _buildingsLayerMask = LayerMask.NameToLayer("Buildings");
            _groundLayerMask = LayerMask.NameToLayer("Ground");
        }
        
        private void OnEnable()
        {
            _overlayButton.onClick.AddListener(OnOverlayClicked);
        }
        
        private void OnDisable()
        {
            _overlayButton.onClick.RemoveListener(OnOverlayClicked);
        }
        
        private void OnOverlayClicked()
        {
            // TODO: workaround. disable overlay clicks for tutorial.
            if (!jHardTutorial.IsTutorialCompleted || jHintTutorial.IsHintTutorialActive )
            {
                return;
            }
            
            Hide();
        }

        public override void Hide()
        {
            base.Hide();
            RemoveListeners();
            _cropItem.CancelDrag();
            
            foreach (DraggableProductItem productItem in _draggablePlantItems)
            {
                productItem.CancelDrag();
            }
            
            _productionPanel.SetActive(false);
            _cropPanel.SetActive(false);
            currentFieldId = null;
            _panelCanvasGroup.alpha = 0;
            _panelFadeTween?.Kill();
        }

        private void AddListeners()
        {
            jServerTimeTickSignal.AddListener(OnServerTimeTick);
            
            int draggableItemIndex = 0;
            int availablePlantsCount = _availablePlantsDatas.Count();
            int lockedPlantsCount = _lockedPlantsDatas.Count();

            for (; draggableItemIndex < availablePlantsCount; draggableItemIndex++)
            {
                DraggableProductItem availableForPlant = _draggablePlantItems[draggableItemIndex];
                availableForPlant.OnDragSignal.AddListener(OnAgriculturalFieldOverCheck);
                availableForPlant.DragStarted += OnDragStartedCallback;
                availableForPlant.DragEnded += OnDragEndedCallback;
                availableForPlant.OnHoldedSignal.AddListener(ShowToolTip);
                availableForPlant.OnHoldedCancelSignal.AddListener(HideToolTip);
                availableForPlant.OnItemClickSignal.AddListener(OnProductClicked);
            }

            var totalPlantsCount = availablePlantsCount + lockedPlantsCount;
            for (; draggableItemIndex < totalPlantsCount; draggableItemIndex++)
            {
                DraggableProductItem lockedForPlant = _draggablePlantItems[draggableItemIndex];
                lockedForPlant.OnHoldedSignal.AddListener(ShowToolTip);
                lockedForPlant.OnHoldedCancelSignal.AddListener(HideToolTip);
            }
        }
        
        private void SetCropItem()
        {
            _cropItem.IsAvailable = true;
            _cropItem.OnDragSignal.AddListener(OnAgriculturalFieldOverCheck);
            _cropItem.DragStarted += OnDragStartedCallback;
            _cropItem.DragEnded += OnDragEndedCallback;
            _cropItem.OnHoldedSignal.AddListener(OnHoldedCrop);
            _cropItem.OnHoldedCancelSignal.AddListener(OnUnholdedCrop);
            _cropItem.OnItemClickSignal.AddListener(OnProductClicked);
        }

        private void RemoveListeners()
        {
            jServerTimeTickSignal.RemoveListener(OnServerTimeTick);
            
            _nextFieldArrow.onClick.RemoveListener(SwitchOnNextField);
            _previousFieldArrow.onClick.RemoveListener(SwitchOnPreviousField);

            foreach (var draggableProductItem in _draggablePlantItems)
            {
                draggableProductItem.OnDragSignal.RemoveListener(OnAgriculturalFieldOverCheck);
                draggableProductItem.DragStarted -= OnDragStartedCallback;
                draggableProductItem.DragEnded -= OnDragEndedCallback;
                draggableProductItem.OnHoldedSignal.RemoveListener(ShowToolTip);
                draggableProductItem.OnHoldedCancelSignal.RemoveListener(HideToolTip);
                draggableProductItem.OnItemClickSignal.RemoveListener(OnProductClicked);
            }

            _cropItem.OnDragSignal.RemoveListener(OnAgriculturalFieldOverCheck);
            _cropItem.DragStarted -= OnDragStartedCallback;
            _cropItem.DragEnded -= OnDragEndedCallback;
            _cropItem.OnHoldedSignal.RemoveListener(OnHoldedCrop);
            _cropItem.OnHoldedCancelSignal.RemoveListener(OnUnholdedCrop);
            _cropItem.OnItemClickSignal.RemoveListener(OnProductClicked);
        }
        
        private void OnProductClicked(DraggableItem item) => jSoundManager.Play(SfxSoundTypes.ProductionStart, SoundChannel.SoundFX);

        private void OnHoldedCrop(DraggableItem draggableItem)
        {
            jSignalOnNeedToShowTooltip.Dispatch(new TooltipData
            {
                Type = TooltipType.Simple,
                Message = LocalizationMLS.Instance.GetText(LocalizationKeyConstants.CROP_TOOLTIP_TEXT),
                ItemTransform = draggableItem.ItemImage.rectTransform
            });
        }

        private void OnUnholdedCrop(DraggableItem draggableItem)
        {
            jSignalOnNeedToHideTooltip.Dispatch();
        }
        
        private void NotEnoughResources()
        {
            _isNotEnough = true;
            Hide();
        }
        
        private void UpdateProductItems()
        {
            var currentFreeSlotIndex = 0;

            foreach (IProducingItemData plant in GetAvailablePlantsQuery())
            {
                DraggableProductItem availableForPlant = _draggablePlantItems[currentFreeSlotIndex];
                availableForPlant.SetProduct(plant, true, true);
                availableForPlant.Show();
                currentFreeSlotIndex++;
            }

            foreach (IProducingItemData plant in GetNextAvailablePlantsQuery())
            {
                DraggableProductItem lockedForPlant = _draggablePlantItems[currentFreeSlotIndex];
                lockedForPlant.SetProduct(plant, false);
                lockedForPlant.Show();
                currentFreeSlotIndex++;
            }

            for (int i = currentFreeSlotIndex; i < _draggablePlantItems.Count; i++)
            {
                _draggablePlantItems[i].Hide();
            }

            _rightArrow.SetActive(currentFreeSlotIndex > 3);
        }
        
        public void SkipProduction()
        {
            IPlayerResources playerResources = jPlayerResources;

            int price = jIntervalsData.GetCurrencyForTime(_fieldModel.GrowingTimeLeft);
            if (price <= 0)
            {
                SkipProductionTime(price);
                return;
            }
            
            if (playerResources.HardCurrency < price)
            {
                ShowPopup(new NotEnoughMoneyPopupSettings(PriceType.Hard,
                    price - playerResources.HardCurrency), result =>
                {
                    if (result == PopupResult.Ok)
                    {
                        Hide();
                    }
                });
            }
            else
            {
                var popupSettings = new PurchaseByPremiumPopupSettings(price);
                
                void UpdatePrice(long _)
                {
                    price = jIntervalsData.GetCurrencyForTime(_fieldModel.GrowingTimeLeft);
                    popupSettings?.InvokePriceChangeAction(price);
                }
                
                jServerTimeTickSignal.AddListener(UpdatePrice);
                
                ShowPopup(popupSettings, result =>
                {
                    jServerTimeTickSignal.RemoveListener(UpdatePrice);
                    if (result == PopupResult.Ok)
                    {
                        SkipProductionTime(price);
                    }
                });
            }
        }

        private void OnDragStartedCallback(DraggableItem draggableItem)
        {
            OnItemDragStartedSignal.Dispatch();
        }

        private void OnDragEndedCallback(DraggableItem draggableItem)
        {
            OnItemDragEndedSignal.Dispatch();
            if (_panelCanvasGroup.alpha < 1)
            {
                Hide();
            }
        }

        private void SwitchOnNextField() => OnSwitchField?.Invoke(true);

        private void SwitchOnPreviousField() => OnSwitchField?.Invoke(false);
        
        public void SetToolTipGoToProductId(int productId)
        {
            var requirementProduct = _draggablePlantItems.FirstOrDefault(item => item.ProducingItemData?.OutcomingProducts == productId);
            
            if (requirementProduct == null)
            {
                return;
            }

            if (_productionPanel.gameObject.activeSelf == false)
            {
                return;
            }
            ShowToolTip(requirementProduct);
        }
        
        private void ShowToolTip(DraggableItem item)
        {
            var productItem = (DraggableProductItem)item;
            Product product = jProductsData.GetProduct(productItem.ProducingItemData.OutcomingProducts);
            _tooltip.ShowProduct(product, (int) productItem.ProducingItemData.ProducingTime, item.transform as RectTransform);
            if (product.UnlockLevel > jPlayerExperience.CurrentLevel)
            {
                _tooltip.ShowUnlockInfo(product.UnlockLevel);
            }
            _tooltip.Show();
            productItem.BackGroundHighLight.SetActive(true);

            item.DragEnded -= OnDragEndedCallback;
            item.CancelDrag();
            item.DragEnded += OnDragEndedCallback;
            jSoundManager.Play(SfxSoundTypes.AppWindow, SoundChannel.SoundFX);
        }

        private void HideToolTip(DraggableItem item)
        {
            var i = (DraggableProductItem)item;
            i.BackGroundHighLight.SetActive(false);
            if (_tooltip.Visible)
            {
                _tooltip.Hide();
            }
        }

        public void UpdatePanel()
        {
            SetTitle();
            _currentMode = GetCurrentMode(_fieldModel);

            switch (_fieldModel.CurrentState)
            {
                case AgriculturalFieldState.Empty:
                    _productionPanel.SetActive(true);
                    _cropPanel.SetActive(false);
                    _fertilizePanel.SetActive(false);
                    _progressBar.gameObject.SetActive(false);
                    UpdateProductItems();
                    break;
                case AgriculturalFieldState.Completed:
                    _productionPanel.SetActive(false);
                    _fertilizePanel.SetActive(false);
                    _cropPanel.SetActive(true);
                    _progressBar.gameObject.SetActive(false);
                    SetCropItem();
                    break;
                default:
                    _productionPanel.SetActive(false);
                    _cropPanel.SetActive(false);
                    _fertilizePanel.SetActive(true);
                    _progressBar.gameObject.SetActive(true);
                    UpdateProgressBar();
                    break;
            }
        }

        private void UpdateProgressBar()
        {
            _timerText.text = UiUtilities.GetFormattedTimeFromSeconds_M_S(_fieldModel.GrowingTimeLeft);
            _progressBar.Value = Mathf.Clamp01(1 - _fieldModel.GrowingTimeLeft / _fieldModel.CurrentGrowingPlant.ProducingTime);
                
            int cost = jIntervalsData.GetCurrencyForTime(_fieldModel.GrowingTimeLeft);

            _skipPrice.SetLocalizedText(cost == 0
                ? LocalizationMLS.Instance.GetText(LocalizationKeyConstants.FREE_LABEL)
                : cost.ToString());
        }

        private void OnServerTimeTick(long _)
        {
            if (_fieldModel?.CurrentGrowingPlant != null && _fieldModel.GrowingTimeLeft > 0)
            {
                UpdateProgressBar();
            }
        }

        private void OnAgriculturalFieldOverCheck(Vector3 position, IProducingItemData productToGrow)
        {
            if (_isNotEnough) return;
            Vector3 cameraPos = position;
            RaycastHit hit = InputTools.RaycastScene(cameraPos, 1 << _buildingsLayerMask);

            if (hit.collider == null) return;

            var pressedObject = hit.collider.gameObject;
            
            if (pressedObject.layer == _groundLayerMask)
            {
                return;
            }

            var agriculturalFieldView = pressedObject.GetComponent<AgroFieldBuildingView>();
            if (agriculturalFieldView == null)
            {
                return;
            }

            if (currentFieldId != null && currentFieldId != agriculturalFieldView.MapObjectModel.MapID)
            {
                return;
            }
            
            currentFieldId = null;

            var fieldModel = (IAgroFieldBuilding)agriculturalFieldView.MapObjectModel;

            AgriculturalMode agrofieldState = GetCurrentMode(fieldModel);
            if (agrofieldState == AgriculturalMode.Planting && _currentMode == AgriculturalMode.Planting)
            {
                TryGrowPlant(fieldModel, productToGrow);
            }
            else if (agrofieldState == AgriculturalMode.Harvesting && _currentMode == AgriculturalMode.Harvesting)
            {
                TryHarvestPlant(agriculturalFieldView);
            }
        }

        private void TryGrowPlant(IAgroFieldBuilding agroFieldBuildingView, IProducingItemData productToGrow)
        {
            KeyValuePair<Id<Product>, int> productKeyValuePair = productToGrow.IncomingProducts.First();
            int playerProductsCount = jPlayerResources.GetProductCount(productKeyValuePair.Key);

            if (playerProductsCount < productKeyValuePair.Value)
            {
                var price = new Price
                {
                    PriceType = PriceType.Resources,
                    ProductsPrice = new Dictionary<Id<Product>, int>
                    {
                        {productKeyValuePair.Key, productKeyValuePair.Value}
                    }
                };

                ShowPopup(new NotEnoughResourcesPopupSettings(price), result =>
                {
                    if (result == PopupResult.Ok)
                    {
                        agroFieldBuildingView.StartGrowing(productToGrow);
                    }
                });
                NotEnoughResources();
                return;
            }

            agroFieldBuildingView.StartGrowing(productToGrow);
            PlayPanelFadeTween(0, 0.1f);
        }

        private void TryHarvestPlant(AgroFieldBuildingView agroFieldBuildingView)
        {
            var agriculturalFieldModel = (AgroFieldBuilding)agroFieldBuildingView.MapObjectModel;

            if (!jPlayerResources.IsCanPlaceToWarehouse(agriculturalFieldModel.CurrentGrowingPlant.Amount))
            {
                ShowPopup(new NotEnoughSpacePopupSettings(), result =>
                {
                    if (result == PopupResult.Ok)
                    {
                        ShowWindowSignal.Dispatch(typeof(WarehouseView), null);
                    }
                });

                NotEnoughResources();

                return;
            }

            agriculturalFieldModel.StartHarvesting();
            PlayPanelFadeTween(0, 0.1f);
        }
        
        private void SkipProductionTime(int hardCost)
        {
            _fieldModel.SkipGrowingTimer(hardCost);
            UpdatePanel();
            // TODO: need add play VFX.
        }
        
        private AgriculturalMode GetCurrentMode(IAgroFieldBuilding field)
        {
            return field.CurrentState == AgriculturalFieldState.Empty
                ? AgriculturalMode.Planting
                : field.CurrentState == AgriculturalFieldState.Completed
                    ? AgriculturalMode.Harvesting
                    : AgriculturalMode.Growing;
        }
        
        private IEnumerable<IProducingItemData> GetAvailablePlantsQuery()
        {
            return  _fieldModel.AvailableAgroProducts.FindAll( data =>
                jProductsData.GetProduct(data.OutcomingProducts).
                    UnlockLevel <= jPlayerExperience.CurrentLevel);
        }
        
        private IEnumerable<IProducingItemData> GetNextAvailablePlantsQuery()
        {
            IProductsData productsData = jProductsData;
            IProducingItemData firstUnlockedPlant = _fieldModel.AvailableAgroProducts
                .FirstOrDefault(plant => productsData.GetProduct(plant.OutcomingProducts).UnlockLevel >
                                         jPlayerExperience.CurrentLevel);

            if (firstUnlockedPlant == null)
            {
                return Enumerable.Empty<IProducingItemData>();
            }

            Product plantProduct = productsData.GetProduct(firstUnlockedPlant.OutcomingProducts);

            if (plantProduct == null)
            {
                return Enumerable.Empty<IProducingItemData>(); 
            }

            return _fieldModel.AvailableAgroProducts
                .Where(plant => productsData.GetProduct(plant.OutcomingProducts).UnlockLevel == plantProduct.UnlockLevel);
        }
        
        private void SetTitle()
        {
            if (_fieldModel.CurrentGrowingPlant == null)
            {
                _headerText.SetLocalizedText(LocalizationMLS.Instance.GetText(LocalizationKeyConstants.AGRO_EMPTY));
            }
            else
            {
                _headerText.SetLocalizedText(
                    jProductsData.GetProduct(_fieldModel.CurrentGrowingPlant.OutcomingProducts).ProductName.ToUpper()
                    + " " + LocalizationMLS.Instance.GetText(LocalizationKeyConstants.AGRO_FULL));
            }
            
            LayoutRebuilder.ForceRebuildLayoutImmediate(_headerPanel);
        }

        private void PlayPanelFadeTween(float alphaValue, float duration)
        {
            _panelFadeTween?.Kill();
           _panelFadeTween = _panelCanvasGroup.DOFade(alphaValue, duration);
        }
    }
}
