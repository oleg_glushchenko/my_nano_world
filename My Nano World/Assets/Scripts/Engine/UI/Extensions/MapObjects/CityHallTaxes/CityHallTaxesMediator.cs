﻿using Assets.NanoLib.UI.Core.Signals;
using Engine.UI;
using GameLogic.Taxes.Service;
using NanoLib.Core.Services.Sound;
using NanoLib.UI.Core;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.GameLogic.BuildingSystem.Happiness;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.UI.Components;

namespace NanoReality.Engine.UI.Extensions.MapObjects.CityHallTaxes
{
    public class CityHallTaxesMediator : BuildingPanelMediator<ICityHall, CityHallTaxesView>
    {
        [Inject] public AddRewardItemSignal jAddRewardItemSignal { get; set; }
        [Inject] public IHappinessData jHappinessData { get; set; }
        [Inject] public ShowWindowSignal jShowWindowSignal { get; set; }
        [Inject] public IUIManager jUiManager { get; set; }
        [Inject] public SignalOnNeedToHideTooltip jSignalOnNeedToHideTooltip { get; set; }
        [Inject] public ICityTaxesService jCityTaxesService { get; set; }

        public override void OnRemove()
        {
            base.OnRemove();
            jCityTaxesService.TaxesChanged -= OnCurrentCoinsChanged;
        }

        protected override void OnShown()
        {
            base.OnShown();
            jCityTaxesService.LoadNewTaxes(() =>
            {
                View.LoadTaxesSpinner.StopSpin();
                jCityTaxesService.TaxesChanged += OnCurrentCoinsChanged;
                UpdateCityHallLabelsData();
            });

            View.CollectButton.onClick.AddListener(OnCollectTaxes);
            View.UpdateHappinessData(jHappinessData);
            View.BuildingUpgradeButton.Set(CurrentModel, OnUpgradeButtonClick);
            
            View.LoadTaxesSpinner.StartSpin();
            View.CollectButton.interactable = false;
            View.TaxesToCollect.text = string.Empty;
        }
        
        protected override void OnHidden()
        {
            base.OnHidden();
            jCityTaxesService.TaxesChanged -= OnCurrentCoinsChanged;
        }

        private void OnCurrentCoinsChanged()
        {
            UpdateCityHallLabelsData();
        }

        public void OnUpgradeButtonClick()
        {
            jShowWindowSignal.Dispatch(typeof(UpgradePanelView), CurrentModel);
            UpgradePanelView upgradePanelView = jUiManager.GetView<UpgradePanelView>();
            upgradePanelView.RefreshPanel(CurrentModel);
            jSignalOnNeedToHideTooltip.Dispatch();
            View.Hide();
        }

        private void OnCollectTaxes()
        {
            if (jCityTaxesService.AnyTaxesAccrued)
            {
                jSoundManager.Play(SfxSoundTypes.CollectTaxes, SoundChannel.SoundFX);
                jCityTaxesService.TaxesCollected += OnTaxesCollected;
                jCityTaxesService.Collect();
            }
        }

        private void OnTaxesCollected(int coins)
        {
            var position = Env.MainCamera.ScreenToWorldPoint(View.TaxesToCollectPosition);
            jAddRewardItemSignal.Dispatch(new AddItemSettings(FlyDestinationType.SoftCoins, position,
                coins.ToString()));
            jCityTaxesService.TaxesCollected -= OnTaxesCollected;
        }

        private void UpdateCityHallLabelsData()
        {
            var cityTaxes = jCityTaxesService.CityTaxes;

            View.TaxPerHour.text = cityTaxes.PerHour.ToString();
            View.MaxTaxes.text = UiUtilities.GetFormattedTimeFromSecondsShort(cityTaxes.MaxCityHall);
            View.TaxesToCollect.text = cityTaxes.CurrentCoins.ToString();
            View.CollectButton.interactable = jCityTaxesService.AnyTaxesAccrued;
            View.PopulationCount.text = CurrentModel.TotalPopulation.ToString();
        }
    }
}
