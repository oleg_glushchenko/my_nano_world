﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Extensions.MapObjects.CityHallTaxes
{
    public class HappyColumn : MonoBehaviour
    {
        #region InspectorFields

        /// <summary>
        /// Отображаем прцоент налогов
        /// </summary>
        [SerializeField] private TextMeshProUGUI _columnText;

        /// <summary>
        /// Маркер для стоблца (смайл)
        /// </summary>
        [SerializeField] private Image _valueMarker;

        /// <summary>
        /// Минимальное значение для столбца
        /// </summary>
        [SerializeField] private int _minHappinessValue;

        /// <summary>
        /// Максимальное значение для столбца
        /// </summary>
        [SerializeField] private int _maxHappinessValue;

        /// <summary>
        /// Размер шрифта (для текущего уровня счастья)
        /// </summary>
        [SerializeField] private int _taxesSelectedFontSize;

        /// <summary>
        /// Размер шрифта (default)
        /// </summary>
        [SerializeField] private int _taxesDeselectedFontSize;

        /// <summary>
        /// Цвет текста для налогов (для текущего уровня счастья)
        /// </summary>
        [SerializeField] private Color _taxesSelectedTextColor;

        /// <summary>
        /// Цвет текста для налогов (по умолчанию)
        /// </summary>
        [SerializeField] private Color _taxesDeselectedTextColor;
        #endregion

        private bool isBoundColumn;

        /// <summary>
        /// Обновляем столбец
        /// </summary>
        public void SetColumn(int satisfactionPercent)
        {
            if (satisfactionPercent >= _minHappinessValue && satisfactionPercent <= _maxHappinessValue)
            {
                isBoundColumn = false;
                Select(satisfactionPercent);
            }
            else
            {
                if (!isBoundColumn)
                    Deselect();
            }
        }

        /// <summary>
        /// Выставляем значения для минимального столбца
        /// </summary>
        public void SetMinBound(int minBound)
        {
            isBoundColumn = true;
            _columnText.text = minBound + "%";
            BoundSelection();
        }

        /// <summary>
        /// Выставляем значения для максимального столбца
        /// </summary>
        public void SetMaxBound(int maxBound)
        {
            isBoundColumn = true;
            _columnText.text = maxBound + "%";
            BoundSelection();
        }

        private void BoundSelection()
        {
            _columnText.enabled = true;
            _valueMarker.enabled = false;
            _columnText.fontSize = _taxesDeselectedFontSize;
            _columnText.color = _taxesDeselectedTextColor;
        }

        private void Select(int percent)
        {
            _columnText.text = percent + "%";
            _columnText.enabled = true;
            _valueMarker.enabled = true;
            _columnText.fontSize = _taxesSelectedFontSize;
            _columnText.color = _taxesSelectedTextColor;
        }

        private void Deselect()
        {
            _columnText.enabled = false;
            _valueMarker.enabled = false;
            _columnText.fontSize = _taxesDeselectedFontSize;
            _columnText.color = _taxesDeselectedTextColor;
        }
    }
}