﻿using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.BuildingSystem.MapObjects.Components;
using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
using UnityEngine;

namespace NanoReality.Game.UI
{
    /// <summary>
    /// Generic type of BuildingPanelView for easy working with building model.
    /// </summary>
    /// <typeparam name="TBuildingModel">Type of building model.</typeparam>
    public abstract class BuildingPanelView<TBuildingModel> : BuildingPanelView where TBuildingModel : MMapObject
    {
        private TBuildingModel _targetModel;

        protected new TBuildingModel TargetBuilding => _targetModel ?? (_targetModel = (TBuildingModel) base.TargetBuilding);
        
        public override void Initialize(object targetBuilding)
        {
            base.Initialize(targetBuilding);
            _targetModel = (TBuildingModel)targetBuilding;
        }
    }
}

/// <summary>
/// Base class for panels with target building highlight.
/// </summary>
public abstract class BuildingPanelView : UIPanelView
{
    [Inject] public IGameManager jGameManager { get; set; }
    [Inject] public IGameCamera jGameCamera { get; set; }

    protected IMapBuilding TargetBuilding;
    private Material _defaultBuildingMaterial;

    public override void Initialize(object targetBuilding)
    {
        base.Initialize(targetBuilding);
        TargetBuilding = (IMapBuilding)targetBuilding;
    }
    
    protected MapObjectView GetTargetView()
    {
        return jGameManager.GetMapObjectView(TargetBuilding);
    }
}
