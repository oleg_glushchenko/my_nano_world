﻿using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Engine.UI;
using NanoReality.GameLogic.Achievements.impl;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.Orders;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Services;
using NanoReality.UI.Components;
using UnityEngine;

public class ExperienceIndicator : UIPanelView 
{
	[Inject] public AddRewardItemSignal jAddRewardItemSignal { get; set; }
	[Inject] public IGameManager jGameManager { get; set; }
	[Inject] public SignalOnMapObjectBuildFinished jSignalOnMapObjectBuildFinished { get; set; }
	[Inject] public SignalOnRewardClaimed jSignalOnRewardClaimed { get; set; }
	[Inject] public IBuildingService jBuildingService { get; set; }

    [PostConstruct]
	public void PostConstruct()
	{
		jSignalOnMapObjectBuildFinished.AddListener(OnBuildingComplete);
		jSignalOnRewardClaimed.AddListener(OnRewardClaimed);
	}

	[Deconstruct]
	public void Deconstruct()
	{
		jSignalOnMapObjectBuildFinished.RemoveListener(OnBuildingComplete);
		jSignalOnRewardClaimed.RemoveListener(OnRewardClaimed);
	}

	private void OnRewardClaimed(Achievement achievement, Transform button)
	{
		OnAwardCollected(button, achievement.AwardActions);
	}


	private void OnAwardCollected(Transform pTransform, List<IAwardAction> awards)
	{
		if (pTransform == null)
		{
			return;
		}

		var position = Camera.main.ScreenToWorldPoint(pTransform.position);
        var coinsAward = awards.FirstOrDefault(x => x.AwardType == AwardActionTypes.SoftCurrency);
        if (coinsAward != null)
        {
	        jAddRewardItemSignal.Dispatch(new AddItemSettings(Engine.UI.FlyDestinationType.SoftCoins, position, coinsAward.AwardCount.ToString()));
        }

        var premiumAward = awards.FirstOrDefault(x => x.AwardType == AwardActionTypes.PremiumCurrency);
        if (premiumAward != null)
        {
	        jAddRewardItemSignal.Dispatch(new AddItemSettings(Engine.UI.FlyDestinationType.PremiumCoins, position, premiumAward.AwardCount.ToString()));
        }
               
        var expAward = awards.FirstOrDefault(x => x.AwardType == AwardActionTypes.Experience);
        int exp = expAward?.AwardCount ?? 0;
        if (exp > 0)
        {
	        jAddRewardItemSignal.Dispatch(new AddItemSettings(Engine.UI.FlyDestinationType.Experience, position, exp.ToString()));
        }
	}

    private void OnBuildingComplete(IMapObject mapObject)
	{
		if (mapObject.MapObjectType == MapObjectintTypes.Road)
			return;

		int experienceReward = jBuildingService.GetBuildingConstructionReward(mapObject.MapID);
		if (experienceReward > 0)
		{
			StartAnimation(mapObject, experienceReward);
		}
	}

	private void StartAnimation(IMapObject mapObject, int xpAmount)
	{
		MapObjectView mapObjectView = jGameManager.GetMapObjectView(mapObject);
		Vector3 defaultPosition = new Vector3(0, 300, 0);

		Vector3 startPosition = mapObjectView == null
			? defaultPosition
			: mapObjectView.CacheMonobehavior.CachedTransform.position;

		jAddRewardItemSignal.Dispatch(new AddItemSettings(FlyDestinationType.Experience, startPosition,
			xpAmount.ToString()));
	}
}
