﻿using System.Collections;
using System.Linq;
using NanoReality.Engine.UI.UIAnimations;
using UnityEngine;

namespace Assets.Scripts.Engine.UI.Extensions.TerrainFx
{
    public class TerrainEffect : ParticleSpecialEffect
    {
        public void SetPosition(Vector3 globalPoint)
        {
            CacheMonobehavior.CachedTransform.position = globalPoint;
        }

        protected override IEnumerator EReleaseCoroutine()
        {
            yield return new WaitForSeconds(ParticleSystemList.Max(ps => ps.main.duration + ps.main.startLifetime.Evaluate(1)));
            Stop();
        }
    }
}
