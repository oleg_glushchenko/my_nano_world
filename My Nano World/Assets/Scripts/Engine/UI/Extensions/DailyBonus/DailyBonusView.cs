﻿using System;
using Assets.NanoLib.UI.Core.Components;
using Assets.NanoLib.UI.Core.Views;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;


namespace NanoReality.Engine.UI.Extensions.DailyBonusPanel
{
    public class DailyBonusView : UIPanelView
    {
        private const float MaxScrollPos = 1.1f;
        private const float MinScrollPos = -0.1f;
        private const int ItemsPerRow = 5;

        #region InspectorFields

        [SerializeField] private ScrollRect _scrollRect;

        [SerializeField] private Button _collectButton;

        [SerializeField] private ItemContainer _dailyBonusesContainer;

        [SerializeField] private DailyBonusItem _dailyBonusItemPrefab;

        #endregion

        public Action onCollect;

        public DailyBonusItem GetItem(int day) => _dailyBonusesContainer.CurrentItems[day] as DailyBonusItem;

        public DailyBonusItem AddItem() => _dailyBonusesContainer.AddItem() as DailyBonusItem;

        public override void Hide()
        {
            _dailyBonusesContainer.ClearCurrentItems();
            base.Hide();
        }

        public override void Initialize(object parameters)
        {
            base.Initialize(parameters);

            if (!_dailyBonusesContainer.IsInstantiateed)
                _dailyBonusesContainer.InstaniteContainer(_dailyBonusItemPrefab, 0);
            _dailyBonusesContainer.ClearCurrentItems();
        }

        public void SetScrollPositionOnADay(int currentDay)
        {
            var row = (currentDay - 1) / ItemsPerRow;
            var normalizedPos = 1 - row * ItemsPerRow / (_scrollRect.content.childCount - ItemsPerRow);
            var scrollRectPos = Mathf.Lerp(MinScrollPos, MaxScrollPos, normalizedPos);
            _scrollRect.DOVerticalNormalizedPos(scrollRectPos, 0.5f);
        }

        public void SetButtonInteractable(bool isInteractable) => _collectButton.interactable = isInteractable;

        public void OnCollectClick()
        {
            SetButtonInteractable(false);
            onCollect();
        }
    }
}