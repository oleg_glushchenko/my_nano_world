﻿using System.Collections.Generic;
using Assets.Scripts.GameLogic.DailyBonus.Api;
using Assets.Scripts.GameLogic.DailyBonus.Impl;
using NanoReality.Core.Resources;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.Game.Tutorial;

namespace NanoReality.Engine.UI.Extensions.DailyBonusPanel
{
    public class DailyBonusContext
    {
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IHardTutorial jHardTutorial { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public IDailyBonusBalanceData jDailyBonusData { get; set; }
        [Inject] public IResourcesManager jResourcesManager { get; set; }
        
        public bool IsDailyBonusCollectedToday => jPlayerProfile.IsDailyBonusCollectedToday;

        public bool ShouldPanelBeShown => !jPlayerProfile.IsDailyBonusCollectedToday && jHardTutorial.IsTutorialCompleted;

        public List<DailyBonusData> GetDailyBonusData => jDailyBonusData.DailyBonusDatas;

        public int GetCurrentBonusDay => jPlayerProfile.DailyBonusDay;
        
        public bool NextDayAvailable => jDailyBonusData.GetDailyBonusDataByDay(jPlayerProfile.DailyBonusDay + 1) != null;

        public int CurrentCollectedDay => jPlayerProfile.DailyBonusDay + (IsDailyBonusCollectedToday ? 2 : 1);
    }
}