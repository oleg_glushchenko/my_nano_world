﻿using System;
using Assets.Scripts.GameLogic.Utilites;
using Assets.Scripts.MultiLanguageSystem.Logic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Extensions.DailyBonusPanel

{
    /// <summary>
    /// Состояние айтема дейли бонуса (собран, текущий, следующий день)
    /// </summary>
    [Serializable]
    public struct DailyBonusItemState
    {
        /// <summary>
        /// GO состояния
        /// </summary>
        [SerializeField]
        private GameObject _stateGo;

        /// <summary>
        /// Кол-во награды
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI _awardCounter;

        /// <summary>
        /// Иконка награды
        /// </summary>
        [SerializeField]
        private Image _awardIcon;

        /// <summary>
        /// Номер дня
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI _dayNum;

        /// <summary>
        /// Размер иконки для софт, прем, продуктов
        /// </summary>
        [SerializeField]
        private Vector2 _generalIconSize;

        /// <summary>
        /// Размер иконки для кристаллов лаборатории
        /// </summary>
        [SerializeField]
        private Vector2 _crystallIconSize;

        public void SetActive(bool activeSelft)
        {
            _stateGo.SetActive(activeSelft);
        }

        /// <summary>
        /// Выставляем размер в зависимости от итема
        /// </summary>
        private void SetIconSize(DailyBonusIconSize iconSize)
        {
            switch (iconSize)
            {
                case DailyBonusIconSize.CrystallIcon:
                    _awardIcon.transform.localScale = new Vector3(0.7f, 0.7f);
                    break;
                case DailyBonusIconSize.GeneralIcon:
                    _awardIcon.transform.localScale = new Vector3(1f, 1f);
                    break;
            }
        }

        public void UpdateState(Sprite awardIcon, int awardCount, int dayNum, DailyBonusIconSize iconSizeType)
        {
            SetActive(true);
            _dayNum.SetLocalizedText(LocalizationMLS.Instance.GetText(LocalizationKeyConstants.DAILYBONUS_ITEM_LABEL_DAY) +
                           string.Format(" {0}", dayNum));
            _awardCounter.text = awardCount.ToString();
            _awardIcon.sprite = awardIcon;
            SetIconSize(iconSizeType);
        }
    }

    public enum DailyBonusIconSize
    {
        CrystallIcon = 1,
        GeneralIcon = 2
    }
}