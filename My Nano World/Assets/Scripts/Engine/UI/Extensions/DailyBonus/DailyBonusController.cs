﻿using Engine.UI;
using NanoReality.GameLogic.Quests;
using strange.extensions.command.impl;
using UnityEngine;

namespace NanoReality.Engine.UI.Extensions.DailyBonusPanel
{
    public class DailyBonusController : Command
    {
        private readonly DailyBonusViewMediator _mediator;

        public DailyBonusController(DailyBonusViewMediator mediator)
        {
            _mediator = mediator;
        }

        public void OnCollectClick()
        {
            _mediator.Collect();

            MakeRewardAnimation();
        }

        private void MakeRewardAnimation()
        {
            FlyDestinationType type;
            Sprite icon = null;
            var dailyBonusData = _mediator.GetDailyBonusData[_mediator.GetCurrentDay];
            var startPos = Env.MainCamera.ScreenToWorldPoint(_mediator.GetCurrentItem.transform.position);
            
            switch (dailyBonusData.AwardActionDatas[0].AwardType)
            {
                case AwardActionTypes.Crystal:
                case AwardActionTypes.Resources:
                    type = FlyDestinationType.Warehouse;
                    icon = _mediator.GetCurrentItem.AwardIcon;
                    break;
                case AwardActionTypes.SoftCurrency:
                    type = FlyDestinationType.SoftCoins;
                    break;
                case AwardActionTypes.PremiumCurrency:
                    type = FlyDestinationType.PremiumCoins;
                    break;

                case AwardActionTypes.NanoGems:
                    _mediator.GiveGems(dailyBonusData.AwardActionDatas[0].AwardCount);
                    return;
                default:
                    return;
            }


            _mediator.StartAnimation(new AddItemSettings(type, startPos, dailyBonusData.AwardActionDatas[0].AwardCount.ToString(), icon));
        }

        public void InitializeItems(int currentCollectedDay)
        {
            for (var i = 0; i < _mediator.GetDailyBonusData.Count; i++)
            {
                var dailyBonusData = _mediator.GetDailyBonusData[i];
                var item = _mediator.AddItem();

                item.UpdateItem(dailyBonusData, i + 1, currentCollectedDay, _mediator.GetTextureContainer);
            }
        }
    }
}