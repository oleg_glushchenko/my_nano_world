﻿using Assets.NanoLib.Utilities.Pulls;
using Assets.Scripts.GameLogic.DailyBonus.Impl;
using NanoReality.Core.Resources;
using NanoReality.GameLogic.Quests;
using UnityEngine;

namespace NanoReality.Engine.UI.Extensions.DailyBonusPanel
{
    public class DailyBonusItem : GameObjectPullable
    {
        [SerializeField]
        private DailyBonusItemState _completDayDatState;

        [SerializeField]
        private DailyBonusItemState _currentDayState;

        [SerializeField]
        private DailyBonusItemState _nextDayState;
        
        [SerializeField]
        private Sprite _softBonusValueSprite;

        [SerializeField]
        private Sprite _hardBonusValueSprite;

        [SerializeField]
        private Sprite _gemsBonusValueSprite;

        public Sprite AwardIcon;

        public void UpdateItem(DailyBonusData bonusData, int itemDay, int currentDay, IResourcesManager productTextures)
        {
            DailyBonusItemState day = _completDayDatState;
            _completDayDatState.SetActive(false);
            _currentDayState.SetActive(false);
            _nextDayState.SetActive(false);

            if (itemDay == currentDay)
            {
                day = _currentDayState;
            }

            if (itemDay > currentDay)
            {
                day = _nextDayState;
            }

            var awardData = bonusData.AwardActionDatas[0];

            AwardIcon = null;
            int awardCount = bonusData.AwardActionDatas[0].AwardCount;
            DailyBonusIconSize iconSizeType = DailyBonusIconSize.GeneralIcon;

            switch (awardData.AwardType)
            {
                case AwardActionTypes.Resources:
                    var resourceAward = (GiveProductAward) bonusData.AwardActionDatas[0];
                    AwardIcon = productTextures.GetProductSprite(resourceAward.Product.Value);
                    break;
                case AwardActionTypes.SoftCurrency:
                    AwardIcon = _softBonusValueSprite;
                    break;
                case AwardActionTypes.PremiumCurrency:
                    AwardIcon = _hardBonusValueSprite;
                    break;
                case AwardActionTypes.NanoGems:
                    AwardIcon = _gemsBonusValueSprite;
                    break;
                case AwardActionTypes.Crystal:
                    Debug.LogWarning("Not implemented");
                    break;
            }

            day.UpdateState(AwardIcon, awardCount, itemDay, iconSizeType);
        }
    }
}