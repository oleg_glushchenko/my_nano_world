﻿using DG.Tweening;
using UnityEngine;

namespace Assets.Scripts.Engine.UI.Extensions.ProductionPanel
{
    [RequireComponent(typeof(DraggableItem))]
    public class DraggableProductItemAnimator : MonoBehaviour
    {
        private DraggableItem _productItem;

        [SerializeField] private float _scaleTweenDuration = 0.3f;

        [SerializeField] private Vector3 _scaleWhenDrag = new Vector3(1.25f, 1.25f, 1f);


        private Tweener _scaleupTween;
        private Sequence _sequence;

        protected bool IsDragPerformed;

        private void Awake()
        {
            // TODO: need move this field to serializable.
            _productItem = GetComponent<DraggableItem>();
        }

        private void OnEnable()
        {
            _productItem.DragStarted += OnDragStarted;
            _productItem.OnDragEndedSignal.AddListener(OnDragEnded);
            _productItem.OnItemClickSignal.AddListener(OnItemClick);
            _productItem.OnItemUpSignal.AddListener(OnItemUp);

            _productItem.transform.localScale = Vector3.one;
        }

        private void OnDisable()
        {
            _productItem.DragStarted -= OnDragStarted;
            _productItem.OnDragEndedSignal.RemoveListener(OnDragEnded);
            _productItem.OnItemClickSignal.RemoveListener(OnItemClick);
            _productItem.OnItemUpSignal.RemoveListener(OnItemUp);
        }

        private void OnDestroy()
        {
            _scaleupTween?.Kill();
            _sequence?.Kill();
            _productItem = null;
        }

        private void OnItemUp()
        {
            if (!IsDragPerformed)
            {
                PlayBounceAnimation();
            }
        }

        private void OnItemClick(DraggableItem item)
        {
            PlayPointerDownAnimation();
        }

        private void OnDragEnded()
        {
            if (IsDragPerformed)
            {
                PlayBounceAnimation();
                IsDragPerformed = false;
            }
        }

        private void OnDragStarted(DraggableItem draggableItem)
        {
            IsDragPerformed = true;
        }

        private void PlayPointerDownAnimation()
        {
            _sequence?.Complete();
            _scaleupTween = _productItem.transform.DOScale(_scaleWhenDrag, _scaleTweenDuration).SetEase(Ease.OutElastic);
        }

        private void PlayBounceAnimation()
        {
            const float sequenceDuration = 0.2f;
            var targetScale = new Vector2(1.2f, 0.85f);

            if (_scaleupTween != null)
            {
                _scaleupTween.Complete();
                _scaleupTween = null;
            }

            _productItem.transform.localScale = Vector3.one;

            _sequence?.Kill(true);
            _sequence = DOTween.Sequence();
            var scaleUp = _productItem.transform.DOScale(targetScale, sequenceDuration).SetEase(Ease.InSine);
            var scaleDown = _productItem.transform.DOScale(Vector2.one, sequenceDuration).SetEase(Ease.OutBack);
            _sequence.Append(scaleUp).Append(scaleDown);
            _sequence.Play();
        }
    }
}