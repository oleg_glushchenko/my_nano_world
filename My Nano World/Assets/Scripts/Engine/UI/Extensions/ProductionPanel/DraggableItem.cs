﻿using System;
using System.Collections.Generic;
using System.Linq;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using NanoLib.UI.Core;

public class DraggableItem : View, IDraggableItem
{
    public event Action<DraggableItem> DragStarted;
    public event Action<DraggableItem> DragEnded;

    public Signal<DraggableItem> OnItemClickSignal = new Signal<DraggableItem>();

    public Signal OnItemUpSignal = new Signal();
    public Signal OnDragEndedSignal = new Signal();

    public Signal<DraggableItem> OnHoldedSignal = new Signal<DraggableItem>();
    public Signal<DraggableItem> OnHoldedCancelSignal = new Signal<DraggableItem>();

    protected const float TooltipHoldTimeout = 0.2f;

    protected bool IsDragNow;
    protected Transform ParentTransform;

    private Image _itemImage;
    
    public Image ItemImage { get; private set; }

    [Inject] public IUIManager jUiManager { get; set; }

    protected override void Awake()
    {
        base.Awake();
        ParentTransform = transform.parent;
        ItemImage = GetComponent<Image>();
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        DragStarted = null;
        DragEnded = null;
    }
    
    public virtual void SetEmpty()
    {
        ItemImage.sprite = null;
        ItemImage.gameObject.SetActive(false);
    }
    
    public virtual void OnBeginDrag(PointerEventData eventData)
    {
        transform.SetParent(jUiManager.OverlayCanvasTransform);
        transform.SetAsLastSibling(); // для того что бы наш драг элемент всегда был на переднем плане

        DragStarted?.Invoke(this);
    }
    
    public virtual void OnDrag(PointerEventData eventData)
    {
        if (!IsDragNow)
            return;
        
        transform.position = eventData.position;
    }
    
    public virtual void OnEndDrag(PointerEventData eventData)
    {
        IsDragNow = false;

        if (ParentTransform != null)
        {
            transform.SetParent(ParentTransform);
            transform.position = ParentTransform.position;
        }
        else
        {
            transform.position = transform.parent.position;
        }

        var receiver = GetDragReceiver();
        if (receiver != null && eventData != null)
            receiver.PutObject(this);
        else if (receiver != null)
            receiver.OnDragObjectExit(this);

        OnDragEndedSignal.Dispatch();

        DragEnded?.Invoke(this);
    }

    public void CancelDrag()
    {
        OnEndDrag(null);
    }

    protected DragReceiver GetDragReceiver()
    {
        var eventData = new PointerEventData(EventSystem.current)
        {
            position = Input.mousePosition
        };

        var objectsHit = new List<RaycastResult>();

        EventSystem.current.RaycastAll(eventData, objectsHit);

        foreach (var raycastResult in objectsHit)
        {
            if (raycastResult.gameObject == null) continue;

            var result = raycastResult.gameObject.GetComponent<DragReceiver>();
            if (result != null)
                return result;
        }

        return null;
    }
    
    public virtual void OnPointerDown(PointerEventData eventData)
    {
        if (!IsDragNow)
            OnItemClickSignal.Dispatch(this);

        IsDragNow = true;
    }
    
    public virtual void OnPointerUp(PointerEventData eventData)
    {
        OnItemUpSignal.Dispatch();

        IsDragNow = false;
    }
    
    public virtual void Update()
    {

    }
    
    public void SetItem(Sprite texture, bool halfTransparent)
    {
        ItemImage.gameObject.SetActive(true);
        ItemImage.sprite = texture;
        ItemImage.color = new Color(1, 1, 1, halfTransparent ? 0.5f : 1f);
    }
}