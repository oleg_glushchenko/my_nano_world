﻿using Assets.NanoLib.UI.Core.Views;
using System;
using NanoLib.UI.Core;
using UnityEngine.EventSystems;

public class DraggableCropItem : DraggableProductItem
{
    private DateTime _startHoldTime = DateTime.MinValue;
    private bool _isHolded;

    public override void OnPointerDown(PointerEventData eventData)
    {
        base.OnPointerDown(eventData);

        if (_isHolded) return;
        _startHoldTime = DateTime.Now;
        _isHolded = true;
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        base.OnPointerUp(eventData);
        _startHoldTime = DateTime.MinValue;
        _isHolded = false;
        OnHoldedCancelSignal.Dispatch(this);
    }

    public override void OnDrag(PointerEventData eventData)
    {
        _startHoldTime = DateTime.MinValue;
        _isHolded = false;
        OnHoldedCancelSignal.Dispatch(this);

        if (ProducingItemData != null && !IsAvailable) return;

        transform.position = eventData.position;

        // если текущий продукт относится к сельхоз. культуре, проверяем на возможность засеять поле
        OnDragSignal.Dispatch(transform.position, ProducingItemData);              
    }

    public override void Update()
    {
        if(_isHolded && (DateTime.Now - _startHoldTime).TotalSeconds > TooltipHoldTimeout)
        {
            OnHoldedSignal.Dispatch(this);
            _isHolded = false;
        }
    }
}