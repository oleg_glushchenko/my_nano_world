﻿using UnityEngine;
using UnityEngine.UI;

namespace Engine.UI.Extensions
{
    public static class UIElementsExtensions
    {
        public static RectTransform GetRectTransform(this Button button)
        {
            return button.transform as RectTransform;;
        }
    }
}