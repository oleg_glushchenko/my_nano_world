using NanoLib.Services.InputService;
using Assets.NanoLib.UI.Core;
using Assets.Scripts.Engine.BuildingSystem.CityMap.Highlightings;
using Engine.UI.Views.Panels.ProductionPanels;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.Game.Tutorial;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using strange.extensions.signal.impl;
using UnityEngine;

namespace NanoReality.Engine.UI.Extensions
{
    public class SignalOnMapMovedUnderTouch : Signal { }

    public abstract class BuildingPanelMediator<TBuildingType, TView> : UIMediator<TView> where TBuildingType : IMapBuilding where TView : UIView
    {
        [Inject] public SignalOnMapMovedUnderTouch jSignalOnMapMovedUnderTouch { get; set; }

        [Inject] public SignalOnMapObjectViewTap jSignalOnMapObjectViewTap { get; set; }
        [Inject] public SignalOnGroundTap jSignalOnGroundTap { get; set; }
        [Inject] public SignalOnLockedSubSectorTap jSignalOnLockedSubSectorTap { get; set; }
        [Inject] public IHardTutorial jHardTutorial { get; set; }
        [Inject] public IGameCamera jGameCamera { get; set; }

        [Inject] public IGameManager jGameManager { get; set; }

        [Inject] public HighlightController jHighlightController { get; set; }

        protected TBuildingType CurrentModel { get; set; }

        protected override void Show(object param)
        {
            CurrentModel = (TBuildingType) param;
            base.Show(param);
            FocusOnBuilding();
        }

        protected void FocusOnBuilding()
        {
            if (jHardTutorial.IsTutorialCompleted == false) return;
            jGameCamera.FocusOnMapObject(CurrentModel);
            jSignalOnMapMovedUnderTouch.Dispatch();
        }

        protected override void OnShown()
        {
            jSignalOnGroundTap.AddListener(HideIfNotInBuildMode);
            jSignalOnLockedSubSectorTap.AddListener(Hide);
            base.OnShown();
        }

        protected override void OnHidden()
        {
            jSignalOnGroundTap.RemoveListener(HideIfNotInBuildMode);
            jSignalOnLockedSubSectorTap.RemoveListener(Hide);
            base.OnHidden();
        }

        private void Hide(CityLockedSector cityLockedSector) => HideIfNotInBuildMode();

        private void HideIfNotInBuildMode()
        {
            if (CurrentModel == null || jHardTutorial.IsTutorialCompleted == false) 
                return;
            
            var mapView = jGameManager.GetMapView(CurrentModel.SectorId);
            var mapObjectView = mapView.GetMapObjectById(CurrentModel.MapID);

            if (mapObjectView == null || mapObjectView.ConstructionController.IsBuildModeEnabled)
                return;
            
            Hide();
        }

        protected void SetGrayscaleMode(bool isActive)
        {
            var mapObjectView = jGameManager.GetMapObjectView(CurrentModel);
            if (isActive)
            {
                jHighlightController.SetGrayscale(mapObjectView);
            }
            else
            {
                if (CheckProductionPanelViewInActiveUi()) return;
                jHighlightController.DisableAoeMode();
                jHighlightController.EnableBuildingsAnimation(mapObjectView);
            }
        }

        private bool CheckProductionPanelViewInActiveUi()
        {
            foreach (var view in jUIManager.ActiveUi)
            {
                if(view.Value is ProductionPanelView) return true;
            }

            return false;
        }
    }
}