﻿using System.Collections.Generic;
using Assets.NanoLib.UI.Core;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Assets.Scripts.Engine.UI.Views.Tooltips;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.api;
using NanoReality.Core.Resources;
using NanoReality.Game.Services.SectorSevices;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using UniRx;

namespace Engine.UI.Extensions.UnlockAreaPanel
{
    public class UnlockAreaPanelMediator : UIMediator<UnlockAreaPopupView>
    {
        [Inject] public IResourcesManager jResourcesManager { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IProductsData jProductsData { get; set; }
        [Inject] public SignalOnNeedToShowTooltip jSignalOnNeedToShowTooltip { get; set; }
        [Inject] public IUnlockSectorService jUnlockSectorService { get; set; }
        [Inject] public SignalOnProductsStateChanged jSignalOnProductsStateChanged { get; set; }

        private readonly List<UnlockAreaPanelItemModel> _itemModels = new List<UnlockAreaPanelItemModel>();
        private CityLockedSector _cityLockedSector;
        private Price _price;

        protected override void Show(object param)
        {
            _cityLockedSector = (CityLockedSector) param;
            _price = _cityLockedSector.LockedSector.Price;
            
            base.Show(param);
        }

        protected override void OnShown()
        {
            base.OnShown();
            
            jSignalOnProductsStateChanged.AddListener(OnProductsStateChanged);
            View.BuyButton.onClick.AddListener(OnBuyButtonClick);

            if (!View.Container.IsInstantiateed)
            {
                View.Container.InstaniteContainer(View.Prefab, 0);
            }
        
            if (_price.IsNeedSoft)
            {
                View.CostText.text = _price.SoftPrice.ToString();
            }
        
            if (_price.IsNeedProducts)
            {
                AddProductItems();
            }
        }

        protected override void OnHidden()
        {
            base.OnHidden();
        
            jSignalOnProductsStateChanged.RemoveListener(OnProductsStateChanged);
            View.BuyButton.onClick.RemoveListener(OnBuyButtonClick);
            View.Container.ClearContainer();
            _itemModels.Clear();
        }
    
        private void AddProductItems()
        {
            foreach(KeyValuePair<Id<Product>, int> product in _price.ProductsPrice)
            {
                UnlockAreaPanelItemModel model = new UnlockAreaPanelItemModel
                {
                    Id = product.Key.Value,
                    Hide = View.Hide,
                    Product = jProductsData.GetProduct(product.Key),
                    Sprite = jResourcesManager.GetProductSprite(product.Key.Value),
                    NeedAmount = product.Value,
                    CurrentAmount = new ReactiveProperty<int>(jPlayerProfile.PlayerResources.GetProductCount(product.Key))
                };

                var item = (UnlockAreaPanelItem) View.Container.AddItem();
                item.Init(model);
                item.OnLongPress += OnLongItemPress;
                _itemModels.Add(model);
            }
        }

        private void OnLongItemPress(TooltipData data)
        {
            jSignalOnNeedToShowTooltip.Dispatch(data);
        }

        private void OnProductsStateChanged(Dictionary<Id<Product>, int> warehouseProducts)
        {
            foreach (var warehouseProduct in warehouseProducts)
            {
                var unlockAreaPanelItemModel = _itemModels.Find(model => model.Id == warehouseProduct.Key.Value);
                unlockAreaPanelItemModel?.CurrentAmount.SetValueAndForceNotify(warehouseProduct.Value);
            }
        }
    
        private void OnBuyButtonClick()
        {
            if (_price.IsNeedProducts)
            {
                if (!jPlayerProfile.PlayerResources.CheckProductsEnought(_price.ProductsPrice))
                {
                    jPopupManager.Show(new NotEnoughResourcesPopupSettings(_price));
                    return;
                }
            }

            if (_price.IsNeedSoft)
            {
                if (!jPlayerProfile.PlayerResources.IsEnoughtForBuyOnlyGold(_price))
                {
                    var needSoft = _price.SoftPrice - jPlayerProfile.PlayerResources.SoftCurrency;
                    jPopupManager.Show(new NotEnoughMoneyPopupSettings(PriceType.Soft, needSoft));
                    return;
                }
            }

            jUnlockSectorService.BuySector(_cityLockedSector);
            Hide();
        }
    }
}