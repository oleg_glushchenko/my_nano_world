﻿using Assets.NanoLib.UtilitesEngine;
using Assets.NanoLib.Utilities.Pulls;
using Assets.Scripts.GameLogic.Quests.Models.api.Extensions;
using Assets.Scripts.MultiLanguageSystem.Logic;
using NanoReality.GameLogic.Quests;
using strange.extensions.mediation.impl;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Extensions.QuestsBuildingPanel
{
    public class QuestConditionInfoView : View, IPullableObject
    {
        [SerializeField] private Image _checkerImage;
        [SerializeField] private Image _progressImage;
        [SerializeField] private TextMeshProUGUI _description;
        [SerializeField] private Animation _bounceAnimation;

        [Inject] public ITimerManager jTimerManager { get; set; }


        protected override void Awake()
        {
            if (autoRegisterWithContext && !registeredWithContext)
                bubbleToContext(this, true, true);
        }

        public void SetInfo(ICondition condition)
        {
            var progressCondition = condition as IConditionWithProgress;

            _progressImage.enabled = progressCondition != null && progressCondition.IsInProgress && !condition.IsAchived;
            _checkerImage.enabled = condition.IsAchived;            
            _description.SetLocalizedText(condition.Description);
        }

        public void PlayBounce(float delay)
        {
            jTimerManager.StartServerTimer(delay, PlayBounceAnimation);
        }

        public void PlayBounceAnimation()
        {
            _bounceAnimation.Play();
        }

        #region IPullable

        public IObjectsPull pullSource { get; set; }

        public void OnPop()
        {
            gameObject.SetActive(true);
        }

        public void OnPush()
        {
            gameObject.SetActive(false);
        }

        public void FreeObject()
        {
            if (pullSource != null)
                pullSource.PushInstance(this);
            else
            {
                DestroyObject();
            }
        }

        public void DestroyObject()
        {
            Destroy(gameObject);
        }

        public object Clone()
        {
            return Instantiate(this);
        }

        #endregion
    }
}