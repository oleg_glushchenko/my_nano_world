﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Engine.UI.Views.Hud.ButtonNotifications;
using NanoReality.Core.SectorLocks;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using strange.extensions.signal.impl;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel
{
    public class QuestsBuildingPanelTabs : QuestButtonNotificationView
    {
        [SerializeField] private Button _buttonCityQuest;

        [SerializeField] private RectTransform _cityTransform;

        [SerializeField] private Button _buttonAgroQuest;

        [SerializeField] private RectTransform _agroTransform;

        [SerializeField] private Button _buttonIndustrialQuest;

        [SerializeField] private RectTransform _industiralTransform;

        [SerializeField] private Vector3 _selectedTabScale;

        [Inject] public FarmState FarmState { get; set; }

        private readonly Signal<BusinessSectorsTypes> _changeTabSignal = new Signal<BusinessSectorsTypes>();

        public Signal<BusinessSectorsTypes> ChangeTabSignal => _changeTabSignal;

        private Dictionary<BusinessSectorsTypes, GameObject> _gameObjectQuestButtons;

        private IDisposable _farmStateViewSubscription;

        private BusinessSectorsTypes _sectorType;

        public BusinessSectorsTypes SectorType
        {
            set
            {
                _sectorType = value;
                UpdateNotifications();
            }
        }

 
        protected override void PreRegister()
        {
            base.PreRegister();
            _buttonCityQuest.onClick.AddListener(() => OnQuestButtonClick(BusinessSectorsTypes.Town));
            _buttonAgroQuest.onClick.AddListener(() => OnQuestButtonClick(BusinessSectorsTypes.Farm));
            _buttonIndustrialQuest.onClick.AddListener(() => OnQuestButtonClick(BusinessSectorsTypes.HeavyIndustry));
                        
            _gameObjectQuestButtons = new Dictionary<BusinessSectorsTypes, GameObject>();
            _gameObjectQuestButtons.Add(BusinessSectorsTypes.Farm, _agroTransform.gameObject);
            _gameObjectQuestButtons.Add(BusinessSectorsTypes.Town, _cityTransform.gameObject);
            _gameObjectQuestButtons.Add(BusinessSectorsTypes.HeavyIndustry, _industiralTransform.gameObject);
        }

        protected override void OnRegister()
        {
            base.OnRegister();
            UpdateNotifications();   
            _farmStateViewSubscription = FarmState.FarmLocked.Subscribe(value => { _agroTransform.gameObject.SetActive(!value); });
        }

        protected override void OnRemove()
        {
            base.OnRemove();
            _farmStateViewSubscription?.Dispose();
        }

        public override void UpdateNotifications()
        {
            UpdateTabStates();
            base.UpdateNotifications();
        }

        private void OnQuestButtonClick(BusinessSectorsTypes sectorType)
        {
            if (sectorType != _sectorType)
            {
                _sectorType = sectorType;
                _changeTabSignal.Dispatch(sectorType);
            }
        }

        private void UpdateTabStates()
        {
            _cityTransform.localScale = Vector3.one;
            _agroTransform.localScale = Vector3.one;
            _industiralTransform.localScale = Vector3.one;

            RectTransform currentTransoform = null;
            switch (_sectorType)
            {
                case BusinessSectorsTypes.Town:
                    currentTransoform = _cityTransform;
                    break;
                case BusinessSectorsTypes.Farm:
                    currentTransoform = _agroTransform;
                    break;
                case BusinessSectorsTypes.HeavyIndustry:
                    currentTransoform = _industiralTransform;
                    break;
            }

            if (currentTransoform != null)
            {
                currentTransoform.localScale = _selectedTabScale;
            }
        }
    }
}