﻿using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Production;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.GameManager.Models.api;

namespace Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks.Utils
{
    public class ProductProduceUtils
    {

        private static readonly List<BusinessSectorsTypes> AllSectors = new List<BusinessSectorsTypes>
        {
            BusinessSectorsTypes.Farm,
            BusinessSectorsTypes.HeavyIndustry,
            BusinessSectorsTypes.Town
        };

        private static readonly List<MapObjectintTypes> AllBuildingTypes = new List<MapObjectintTypes>
        {
            MapObjectintTypes.Factory,
            MapObjectintTypes.Mine,
            MapObjectintTypes.AgriculturalField
        };

        public static bool FindProductInProgress(IGameManager jGameManager, Id<Product> productId)
        {
            foreach (var sectorsTypes in AllSectors)
            {
                foreach (var mapObjectintType in AllBuildingTypes)
                {
                    var isProductInProgressFound = CheckIfProductInProgressExist(jGameManager, sectorsTypes,
                        mapObjectintType, productId);
                    if (isProductInProgressFound)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private static bool CheckIfProductInProgressExist(IGameManager jGameManager, BusinessSectorsTypes businessSector, MapObjectintTypes mapObjectintType, Id<Product> productId)
        {
            Id<IBusinessSector> sectorType = businessSector.GetBusinessSectorId();

            var allFactories = jGameManager.GetMap(sectorType)
                .FindAllMapObjects(mapObjectintType);
            if (allFactories != null)
            {
                if (mapObjectintType == MapObjectintTypes.AgriculturalField)
                {
                    var field = FindAgriculturalField(allFactories, productId);
                    if (field != null)
                    {
                        return field.GrowingTimeLeft > 0 && field.CurrentGrowingPlant != null;
                    }
                    return false;
                }

                var mapBuilding = FindFactory(allFactories, productId);
                if (mapBuilding != null)
                {
                    var produceSlotInProgress = mapBuilding.CurrentProduceSlots.FirstOrDefault(
                        item =>
                            item.ItemDataIsProducing != null && item.ItemDataIsProducing.OutcomingProducts == productId &&
                            item.CurrentProducingTime > 0);
                    return produceSlotInProgress != null;
                }
            }
            return false;
        }

        public static MapObjectView FindProductionBuildingView(IGameManager jGameManager, Id<Product> productId)
        {
            foreach (var sectorsTypes in AllSectors)
            {
                foreach (var mapObjectintType in AllBuildingTypes)
                {
                    var foundBuilding = FindProductionBuilding(jGameManager, sectorsTypes, mapObjectintType, productId);
                    if (foundBuilding != null)
                    {
                        var mapObjectView =
                            jGameManager.GetMapView(sectorsTypes.GetBusinessSectorId())
                                .GetMapObjectById(foundBuilding.MapID);

                        if (mapObjectView != null)
                        {
                            return mapObjectView;
                        }
                    }
                }
            }
            return null;
        }

        private static IMapBuilding FindProductionBuilding(IGameManager jGameManager, BusinessSectorsTypes businessSector, MapObjectintTypes mapObjectintType, Id<Product> productId)
        {
            Id<IBusinessSector> sectorType = businessSector.GetBusinessSectorId();

            var allFactories = jGameManager.GetMap(sectorType)
                .FindAllMapObjects(mapObjectintType);
            if (allFactories != null)
            {
                if (mapObjectintType == MapObjectintTypes.AgriculturalField)
                {
                    return FindAgriculturalField(allFactories, productId);
                }
                return FindFactory(allFactories, productId);
            }
            return null;
        }

        /// <summary>
        /// Search for agricultural field, priority is given to the fields with growing products.
        /// </summary>
        public static IAgroFieldBuilding FindAgriculturalFieldWithProductOrEmpty(IGameManager jGameManager, Id<IBusinessSector> sectorType, Id<Product> productId)
        {
            var fields = jGameManager
                .GetMap(sectorType)
                .FindAllMapObjects(MapObjectintTypes.AgriculturalField)
                .OfType<IAgroFieldBuilding>()
                .ToList();

            if (fields.Count == 0) 
                return null;

            var result = fields.Find(f => f.CurrentGrowingPlant?.OutcomingProducts == productId);
            if (result != null)
                return result;

            result = fields.Find(f => f.AvailableAgroProducts.Any(plant => plant.OutcomingProducts == productId) && f.CurrentGrowingPlant == null);
            return result ?? fields.First();
        }

        private static IAgroFieldBuilding FindAgriculturalField(List<IMapObject> allFactories, Id<Product> productId)
        {
            if (allFactories != null)
            {
                var fieldWithGrowingProduct = FindAgriculturalFieldWithGrowingProduct(allFactories, productId);
                if (fieldWithGrowingProduct != null)
                {
                    return fieldWithGrowingProduct;                    
                }
                return
                    allFactories.OfType<IAgroFieldBuilding>()
                        .FirstOrDefault(
                            productionBuilding =>
                                productionBuilding.AvailableAgroProducts.Any(
                                    avaliableProducingGood => avaliableProducingGood.OutcomingProducts == productId));
            }
            return null;
        }

        private static IAgroFieldBuilding FindAgriculturalFieldWithGrowingProduct(List<IMapObject> allFactories, Id<Product> productId)
        {
            if (allFactories != null)
            {
                return
                    allFactories.OfType<IAgroFieldBuilding>()
                        .FirstOrDefault(
                            productionBuilding =>
                                productionBuilding.CurrentGrowingPlant != null &&
                                productionBuilding.CurrentGrowingPlant.OutcomingProducts == productId);
            }
            return null;
        }

        private static IProductionBuilding FindFactory(List<IMapObject> allFactories, Id<Product> productId)
        {
            return allFactories.OfType<IProductionBuilding>()
                .FirstOrDefault(
                    productionBuilding =>
                        productionBuilding.AvailableProducingGoods.Any(
                            avaliableProducingGood => avaliableProducingGood.OutcomingProducts == productId));
        }
    }
}
