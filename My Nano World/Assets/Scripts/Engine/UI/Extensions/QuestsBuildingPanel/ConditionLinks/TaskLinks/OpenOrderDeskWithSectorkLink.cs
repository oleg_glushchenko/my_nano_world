﻿
using System.Linq;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks.TaskLinks;
using GameLogic.Quests.Models.impl.Extensions;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Quests;

namespace Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks
{
    public class OpenOrderDeskWithSectorkLink : OpenBuildingTaskLink, IOpenOrderDeskWithSectorkLink
    {        
        private const int SeaportId = 140;
        private const int OrderDeskFarmId = 139;
        public override bool Setup(ICondition condition)
        {
            if (condition is CompleteOrderDeskCondition completeOrder)
            {
                return  SetupBuilding(completeOrder.BusinessSector);
            }

            if (condition is ShuffleOrderTableCondition shuffleOrder)
            {
                return  SetupBuilding(shuffleOrder.BusinessSector);
            }
            
            if (condition is ReceiveProductFromOrderCondition receiveOrder)
            {
                return  SetupBuilding(receiveOrder.BusinessSector);
            }
            
            return false;           
        }

        private bool SetupBuilding(BusinessSectorsTypes sectorId)
        {
            switch (sectorId)
            {
                case BusinessSectorsTypes.Town:
                    TargetMapObjectView =
                        jGameManager.FindMapObjectView<MapObjectView>(MapObjectintTypes.CityOrderDesk);
                    return TargetMapObjectView != null;
                default:
                    return SetupMapObjectView(sectorId, MapObjectintTypes.OrderDesk);
            }
        }
        
        protected override bool SetupMapObjectView(BusinessSectorsTypes sectorId, MapObjectintTypes mapObjectType, BuildingSubCategories buildingSubCategories = BuildingSubCategories.None)
        {
            var targets = jGameManager.FindAllMapObjectsOnMapByType(BusinessSectorsTypes.Global, mapObjectType);
            int mapObjectID = sectorId == BusinessSectorsTypes.HeavyIndustry? SeaportId: OrderDeskFarmId;
            TargetMapObjectView = jGameManager.GetMapObjectView(targets.FirstOrDefault(t => t.Id.Id == mapObjectID));
            return TargetMapObjectView != null;
        }

    }
}
