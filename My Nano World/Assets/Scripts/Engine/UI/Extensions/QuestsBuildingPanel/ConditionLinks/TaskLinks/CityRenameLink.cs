﻿using Assets.NanoLib.UI.Core.Signals;
using NanoReality.Engine.UI.Views.SettingsPanel;
using NanoReality.Game.UI.SettingsPanel;
using strange.extensions.mediation.impl;

namespace Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks
{
    public class CityRenameLink : TaskLink, ICityRenameLink
    {
        [Inject]
        public ShowWindowSignal ShowWindowSignal { get; set; }

        public override void Execute() => ShowWindowSignal.Dispatch(typeof(SettingsPanelView), null);
    }
}