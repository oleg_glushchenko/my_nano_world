﻿using System.Linq;
using Assets.NanoLib.UI.Core.Signals;
using Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks.TaskLinks;
using Assets.Scripts.Engine.UI.Views.Warehouse;
using GameLogic.Quests.Models.impl.Extensions;
using NanoReality.Game.UI.BuildingsShopPanel;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Quests;

namespace Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks
{
    public class OpenBuildingsShopLink : OpenBuildingTaskLink, IOpenBuildingsShopLink
    {
        [Inject] public IMapObjectsData jMapObjectsData { get; set; }  
        [Inject] public ShowWindowSignal jShowWindowSignal { get;set; }
        
        protected IMapBuilding _building;

        public override bool Setup(ICondition condition)
        {
            if (condition is HaveBuildingCondition buildCondition)
            {
                return SetupBuilding(buildCondition.BuildingTypeId, condition.Level);
            }
            
            if (condition is BuildingHaveByIdCondition haveByIdCondition)
            {
                return SetupBuilding(haveByIdCondition.BuildingId, condition.Level, haveByIdCondition.RequiredNumber, haveByIdCondition.CurrentNumber);
            }
            
            if (condition is BuildingHaveByTypeCondition haveByTypeCondition)
            {
                return SetupBuilding(haveByTypeCondition.BuildingType, condition.Level, haveByTypeCondition.RequiredNumber, haveByTypeCondition.CurrentNumber);
            }
            
            if (condition is BuildingConstructByIdCondition constructByIdCondition)
            {
                return SetupBuilding(constructByIdCondition.BuildingId, constructByIdCondition.Level, constructByIdCondition.RequiredCount, constructByIdCondition.СurrentCount);
            }
            
            if (condition is BuildingConstructByTypeCondition constructByTypeCondition)
            {
                return SetupBuilding(constructByTypeCondition.BuildingType, constructByTypeCondition.Level, constructByTypeCondition.RequiredNumber, constructByTypeCondition.CurrentNumber);
            }
            
            if (condition is BuildingHaveWithSubCategoryCondition buildingHaveWithSubCategoryCondition)
            {
                return SetupBuilding(MapObjectintTypes.DwellingHouse, condition.Level, buildingHaveWithSubCategoryCondition.RequiredCount, buildingHaveWithSubCategoryCondition.CurrentNumber, buildingHaveWithSubCategoryCondition.BuildingSubCategory);
            }
            
            if (condition is ConstructBuildingWithSubCategoryCondition constructBuildingWithSubCategoryCondition)
            {
                return SetupBuilding(MapObjectintTypes.DwellingHouse, condition.Level,constructBuildingWithSubCategoryCondition.RequiredCount, constructBuildingWithSubCategoryCondition.RequiredCount, constructBuildingWithSubCategoryCondition.BuildingSubCategory);
            }

            return false;
        }

        protected virtual bool SetupBuilding(Id_IMapObjectType buildingTypeId)
        {
            return SetupBuilding(buildingTypeId, -1);
        }

        protected bool SetupBuilding(Id_IMapObjectType buildingTypeId, int level)
        {
            var correctedLevel = level == -1 ? 0 : level;

            if (!jMapObjectsData.IsPresentBuildingIdAndLevel(buildingTypeId, correctedLevel)) return false;
            
            _building = jMapObjectsData.GetByBuildingIdAndLevel(buildingTypeId, correctedLevel) as IMapBuilding;
            if (_building == null) return false;
            
            if (_building is IWarehouseMapObject)
            {
                jShowWindowSignal.Dispatch(typeof(WarehouseView), null);
                return false;
            }

            var businessSectorId = _building.SectorIds.FirstOrDefault();
            var buildings = jGameManager.FindAllMapObjectsOnMapByType(
                businessSectorId, buildingTypeId);
            if (buildings == null)
            {
                return true;
            }
            
            buildings.Sort(CompareBuildingsByLevelDesc);

            for (int i = 0; i < buildings.Count; i++)
            {
                var building = buildings[i];
                if (building.Level == level && !building.IsConstructed || 
                    building.Level < level)
                {
                    TargetMapObjectView = jGameManager.GetMapObjectView(building);
                    jCamera.FocusOnMapObject(TargetMapObjectView.MapObjectModel);
                    break;
                }
            }
            
            return true;
        }

        private bool SetupBuilding(Id_IMapObjectType buildingTypeId, int level, int amount, int currentCount)
        {
            var correctedLevel = level == -1 ? 0 : level;

            if (!jMapObjectsData.IsPresentBuildingIdAndLevel(buildingTypeId, correctedLevel)) return false;

            _building = jMapObjectsData.GetByBuildingIdAndLevel(buildingTypeId, correctedLevel) as IMapBuilding;
            if (_building == null) return false;

            if (_building is IWarehouseMapObject)
            {
                jShowWindowSignal.Dispatch(typeof(WarehouseView), true);
                return false;
            }

            var businessSectorId = _building.SectorIds.FirstOrDefault();
            var buildings = jGameManager.FindAllMapObjectsOnMapByType(
                businessSectorId, buildingTypeId);
            if (buildings == null)
            {
                return true;
            }

            buildings.Sort(CompareBuildingsByLevelDesc);

            var buildingToUpgrade = buildings.FindAll(x => x.Level == level && !x.IsConstructed || x.Level < level && ((IMapBuilding)x).CanBeUpgraded);

            if (buildingToUpgrade.Any())
            {
                if (buildingToUpgrade.Count < amount - currentCount)
                {
                    return true;
                }

                TargetMapObjectView = jGameManager.GetMapObjectView(buildingToUpgrade.Last());
                jCamera.FocusOnMapObject(TargetMapObjectView.MapObjectModel);
                return true;
            }

            return true;
        }

        private bool SetupBuilding(MapObjectintTypes buildingType, int level, int amount, int currentCount,
            BuildingSubCategories buildingSubCategories = BuildingSubCategories.None)
        {
            bool checkSubCategory = buildingSubCategories != BuildingSubCategories.None;

            if (buildingType == MapObjectintTypes.Warehouse)
            {
                jShowWindowSignal.Dispatch(typeof(WarehouseView), null);
                return false;
            }

            _building = checkSubCategory
                ? jMapObjectsData.GetByBuildingTypeAndSubCategory(buildingType, buildingSubCategories) as IMapBuilding
                : jMapObjectsData.GetByBuildingType(buildingType) as IMapBuilding;

            if (_building == null) return false;

            var businessSectorId = _building.SectorIds.FirstOrDefault();
            var buildings = jGameManager.FindAllMapObjectsOnMapByType(
                businessSectorId, buildingType);
            if (buildings == null)
            {
                switch (buildingSubCategories)
                {
                    case BuildingSubCategories.Common:
                    case BuildingSubCategories.None:
                        return true;
                    default:
                        return false;
                }
            }

            var buildingsToUpgrade = buildings
                .Where(x => (!checkSubCategory || x.SubCategory == buildingSubCategories) && (x.Level == level && !x.IsConstructed || x.Level < level && ((IMapBuilding)x).CanBeUpgraded))
                .OrderBy(b => b.Level).ToList();

            if (buildingsToUpgrade.Any())
            {
                if (buildingsToUpgrade.Count() < amount - currentCount)
                {
                    return true;
                }

                TargetMapObjectView = jGameManager.GetMapObjectView(buildingsToUpgrade.Last());
                jCamera.FocusOnMapObject(TargetMapObjectView.MapObjectModel);
                return true;
            }

            return true;
        }

        public override void Execute()
        {
            if (TargetMapObjectView != null)
            {
                base.Execute();
                TargetMapObjectView = null;
            }
            else
            {
                jShowWindowSignal.Dispatch(typeof(BuildingsShopPanelView), _building);
            }
            
        }
        
        private static int CompareBuildingsByLevelDesc(IMapObject x, IMapObject y)
        {
            return y.Level.CompareTo(x.Level);
        }
    }
}
