﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks.TaskLinks;
using NanoReality.Game.Services.BuildingsUnlockService;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Quests;

namespace Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks
{
    public class OpenOrderDeskLink : OpenBuildingTaskLink, IOpenOrderDeskLink
    {
        private static readonly List<BusinessSectorsTypes> AllSectors = new List<BusinessSectorsTypes>
        {
            BusinessSectorsTypes.Town,
            BusinessSectorsTypes.HeavyIndustry,
            BusinessSectorsTypes.Farm
        };

        [Inject] public IBuildingsUnlockService jBuildingsUnlockService { get; set; }

        [Inject]
        public IPlayerProfile jPlayerProfile { get; set; }

        public override bool Setup(ICondition condition)
        {            
            var allSectoresCopy = AllSectors.ToList();
            var rnd = new Random();
            var randomSectors = allSectoresCopy.OrderBy(item => rnd.Next());

            foreach (var sectorType in randomSectors)
            {
                var mapObjectType = sectorType == BusinessSectorsTypes.Town
                    ? MapObjectintTypes.CityOrderDesk
                    : MapObjectintTypes.OrderDesk;

                var orderDeskBuilding = jGameManager.FindMapObjectOnMapByType(sectorType, mapObjectType);
                var unlockLevel = jBuildingsUnlockService.GetBuildingUnlockLevel(orderDeskBuilding.ObjectTypeID);
                var isBuildingActive = jPlayerProfile.CurrentLevelData.CurrentLevel >= unlockLevel;
                if (isBuildingActive)
                {
                    return SetupMapObjectView(sectorType, mapObjectType);                    
                }                
            }
            return false;
        }
    }
}
