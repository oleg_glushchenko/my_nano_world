﻿using Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks.Utils;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.api;
using NanoLib.Core;
using NanoReality.GameLogic.Quests;

namespace Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks.TaskLinks
{
    public class OpenProductProduceLink : OpenBuildingsShopLink, IOpenProductProduceLink
    {
        [Inject] public IProductsData jProductsData { get; set; }
        
        public override bool Setup(ICondition condition)
        {            
            var produceCondition = condition as ProductsProducedCondition;
            if (produceCondition != null)
            {
                return SetupProduct(produceCondition.ProductType);
            }
            return false;
        }

        private bool SetupProduct(IntValue productId)
        {
            TargetMapObjectView = ProductProduceUtils.FindProductionBuildingView(jGameManager, productId);

            if (TargetMapObjectView != null)
            {
                jCamera.FocusOnMapObject(TargetMapObjectView.MapObjectModel);
                return true;
            }

            var product = jProductsData.GetProduct(productId);
                
            return product != null && SetupBuilding(product.BuildingTypeId, 0);
        }
    }
}
