﻿using System.Collections.Generic;
using System.Linq;
using NanoLib.Services.InputService;
using NanoReality.Engine.BuildingSystem.CityCamera;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.Quests;
using UnityEngine;

namespace Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks.TaskLinks
{
    public class GoToLockedSectorLink : TaskLink, IGoToLockedSectorLink
    {
        private static readonly List<BusinessSectorsTypes> AllSectors = new List<BusinessSectorsTypes>
        {
            BusinessSectorsTypes.Town,
            BusinessSectorsTypes.HeavyIndustry,
            BusinessSectorsTypes.Farm
        };

        private CityLockedSector _cityLockedSector;
        private ILockedSector LockedSector => _cityLockedSector.LockedSector;

        [Inject] public IGameCamera jGameCamera { get; set; }
        [Inject] public IGameCameraModel jGameCameraModel { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public SignalOnLockedSubSectorTap jSignalOnLockedSubSectorTap { get; set; }

        public override bool Setup(ICondition condition)
        {
            _cityLockedSector = FindSubSectorView();
            return _cityLockedSector != null;
        }

        private CityLockedSector FindSubSectorView()
        {
            foreach (var businessSectorsTypes in AllSectors)
            {
                var sectorId = businessSectorsTypes.GetBusinessSectorId();
                var map = jGameManager.GetMap(sectorId);
                if (map.SubLockedMap.LockedSectors.Count > 0)
                {
                    var subSector = map.SubLockedMap.LockedSectors.FirstOrDefault(item => item.LockedSector.Unlockable);
                    if (subSector != null)
                    {
                        return subSector;
                    }
                }
            }            

            return null;
        }

        public override void Execute()
        {
            if (_cityLockedSector == null) return;
            
            var mapView = jGameManager.GetMapView(LockedSector.BusinessSectorId);
                
            var sectorXPos = LockedSector.LeftBottomGridPos.x * 2 - 10;
            var sectorYPos = LockedSector.LeftBottomGridPos.y * 2;
            var sectorSize = (Vector2) LockedSector.GridDimensions * mapView.jCityMapSettings.GridCellScale;
            var centerGridPos = new Vector2(sectorXPos, sectorYPos) + sectorSize * 0.5f;
            var targetCameraPos = centerGridPos.ToVector2F() + mapView.GetMapViewOriginPosition();

            jGameCamera.ZoomTo(targetCameraPos, CameraSwipeMode.Fly, jGameCameraModel.CurrentZoom, -1f, true);

            jSignalOnLockedSubSectorTap.Dispatch(_cityLockedSector);

            _cityLockedSector = null;
        }
    }
}