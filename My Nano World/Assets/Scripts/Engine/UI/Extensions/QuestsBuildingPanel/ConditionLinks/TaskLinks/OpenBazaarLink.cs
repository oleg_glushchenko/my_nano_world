﻿using System.Linq;
using Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks;
using GameLogic.Quests.Models.impl.Extensions;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Quests;

namespace Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks.TaskLinks
{
    public class OpenBazaarLink : OpenBuildingsShopLink, IOpenBazaarLink
    {
        public override bool Setup(ICondition condition)
        {
            if (condition is BazaarPurchaseProductCondition)
            {
                return  SetupMapObjectView(BusinessSectorsTypes.Global, MapObjectintTypes.TradingShop);
            }

            if (condition is BazaarPurchaseLootBoxCondition)
            {
                return  SetupMapObjectView(BusinessSectorsTypes.Global, MapObjectintTypes.TradingShop);
            }
            
            if (condition is BazaarSpendGoldBarCondition)
            {
                return  SetupMapObjectView(BusinessSectorsTypes.Global, MapObjectintTypes.TradingShop);
            }
            
            return false;           
        }

        protected override bool SetupMapObjectView(BusinessSectorsTypes sectorId, MapObjectintTypes mapObjectType, BuildingSubCategories buildingSubCategories = BuildingSubCategories.None)
        {
            var targets = jGameManager.FindAllMapObjectsOnMapByType(BusinessSectorsTypes.Global, mapObjectType);
            TargetMapObjectView = jGameManager.GetMapObjectView(targets.FirstOrDefault());
            jCamera.FocusOnMapObject(TargetMapObjectView.MapObjectModel);
            return TargetMapObjectView != null;
        }
    }
}
