﻿using NanoLib.Services.InputService;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.GameManager.Models.api;

namespace Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks.TaskLinks
{
    public class OpenBuildingTaskLink : TaskLink
    {
        protected MapObjectView TargetMapObjectView;

        [Inject] public SignalOnMapObjectViewTap jSignalOnMapObjectViewTap { get; set; }

        [Inject] public IGameManager jGameManager { get; set; }
        
        [Inject] public IGameCamera jCamera { get; set; }
        

        public override void Execute()
        {
            if (TargetMapObjectView != null)
            {
                jCamera.FocusOnMapObject(TargetMapObjectView.MapObjectModel);
                jSignalOnMapObjectViewTap.Dispatch(TargetMapObjectView);
            }
        }

        protected virtual bool SetupMapObjectView(BusinessSectorsTypes sectorId, MapObjectintTypes mapObjectType, BuildingSubCategories buildingSubCategories = BuildingSubCategories.None)
        {
            TargetMapObjectView = jGameManager.GetMapView(sectorId).GetMapObjectViewByType(mapObjectType);
            if (TargetMapObjectView != null)
            {
                jCamera.FocusOnMapObject(TargetMapObjectView.MapObjectModel);
            }
            
            return TargetMapObjectView != null;
        }
    }
}
