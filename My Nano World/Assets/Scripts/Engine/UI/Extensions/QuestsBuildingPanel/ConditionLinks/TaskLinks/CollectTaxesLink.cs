﻿using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Quests;

namespace Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks.TaskLinks
{
    public class CollectTaxesLink : OpenBuildingTaskLink, ICollectTaxesLink
    {
        public override bool Setup(ICondition condition)
        {
            var cityHall = jGameManager.FindMapObjectByType<IMapObject>(MapObjectintTypes.CityHall);
            if (cityHall != null)
            {
                TargetMapObjectView = jGameManager.GetMapObjectView(cityHall);
                jCamera.FocusOnMapObject(TargetMapObjectView.MapObjectModel);
            }                        
            return TargetMapObjectView != null;
        }        
    }
}
