﻿using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Quests;

namespace Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks.TaskLinks
{
    public class AuctionCoinsLink : OpenBuildingsShopLink, IAuctionCoinsLink
    {
        public override bool Setup(ICondition condition)
        {
            return SetupTradingBuilding(condition is IAuctionSpendCoinsCondition);
        }
        
        private bool SetupTradingBuilding(bool isSpendCoins)
        {
            var mapObjectType = isSpendCoins ? MapObjectintTypes.TradingShop : MapObjectintTypes.TradingMarket;

            var mapObject = jMapObjectsData.GetByBuildingType(mapObjectType);
            if (mapObject != null)
            {
                return SetupBuilding(mapObject.ObjectTypeID);
            }
            return false;
        }
    }
}
