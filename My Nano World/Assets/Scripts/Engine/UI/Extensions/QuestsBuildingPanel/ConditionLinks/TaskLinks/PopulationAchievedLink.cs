﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks;
using NanoReality.Engine.UI.Views.BuildingsShopPanel;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Services;

namespace Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks.TaskLinks
{
    public class PopulationAchievedLink : OpenBuildingsShopLink, IPopulationAchievedLink
    {
        [Inject] public IBuildingService jBuildingService { get; set; }
        [Inject] public IPlayerExperience jPlayerExperience { get; set; }
        
        public override bool Setup(ICondition condition)
        {
            if (condition is PopulationAchivedCondition)
            {
                List<IMapObject> dwellings = jGameManager.FindAllMapObjectsOnMapByType(BusinessSectorsTypes.Town, MapObjectintTypes.DwellingHouse);
                IMapObject outOfRoadDwelling = dwellings.FirstOrDefault(m => !m.IsConnectedToGeneralRoad);

                if (outOfRoadDwelling != null)
                {
                    TargetMapObjectView = jGameManager.GetMapObjectView(outOfRoadDwelling);
                    return true;
                }

                BuildingsShopItemData[] dwellingsData = FindAvailableToBuildDwelling();
                if (dwellingsData.Any())
                {
                    return SetupBuilding(dwellingsData[0].MapObjectData.ObjectTypeID);
                }

                foreach (IMapObject dwelling in dwellings)
                {
                    IMapBuilding instanceModel = (IMapBuilding)dwelling;
                    IMapBuilding nextLevelBalanceModel = jMapObjectsData.GetNextBuildingLevelData(instanceModel);

                    if (nextLevelBalanceModel == null || !instanceModel.IsUpgradable || instanceModel.IsUpgradingNow) continue;

                    TargetMapObjectView = jGameManager.GetMapObjectView(dwelling);
                    return true;
                }
            }

            return false;
        }

        private BuildingsShopItemData[] FindAvailableToBuildDwelling()
        {
            List<IMapObject> dwellings = jMapObjectsData.GetAllMapObjectsByBuildingType(MapObjectintTypes.DwellingHouse);

            return dwellings.Select(d => jBuildingService.GetBuildingShopItemData(d))
                .Where(data => 
                    data.BuildingState == BuildingState.CanBeBuild &&
                    data.UnlockLevel <= jPlayerExperience.CurrentLevel &&
                    data.MapObjectData.Level == 0 &&
                    data.LimitBuildingCount > 0)
                .OrderBy(c => c.UnlockLevel)
                .ToArray();
        }
    }
}
