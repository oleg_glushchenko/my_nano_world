﻿using Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks.TaskLinks;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Quests;

namespace Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks
{
    public class OpenAuctionLink : OpenBuildingTaskLink, IOpenAuctionLink
    {
        public override bool Setup(ICondition condition)
        {
            var mapObjectType = condition is AuctionPurchaseCondition
                ? MapObjectintTypes.TradingShop
                : MapObjectintTypes.TradingMarket;

            return SetupMapObjectView(BusinessSectorsTypes.Town, mapObjectType);
        }
    }
}