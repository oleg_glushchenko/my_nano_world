﻿using NanoReality.GameLogic.Quests;

namespace Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks
{
    public interface ITaskLink
    {
        bool Setup(ICondition condition);

        void Execute();
    }
}