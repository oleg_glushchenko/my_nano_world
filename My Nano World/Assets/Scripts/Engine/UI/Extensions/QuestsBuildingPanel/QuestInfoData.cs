﻿using NanoReality.GameLogic.Quests;

namespace Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel
{
    public class QuestInfoData
    {
        public IQuest Quest;
        public bool CanCollectReward = true;

    }
}
