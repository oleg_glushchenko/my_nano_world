﻿using System;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.MultiLanguageSystem.Logic;
using NanoLib.UI.Core;
using NanoReality;
using NanoReality.Engine.UI;
using NanoReality.Engine.UI.Views;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using TMPro;
using UnityEngine;
using Object = System.Object;

public class EnergyPanelView : BuildingPanelView
{
    [Inject] public IUIManager jUiManager { get; set; }
    
    [SerializeField] private BuildingUpgradeButtonView _upgradeButton;
    public BuildingUpgradeButtonView UpgradeButton => _upgradeButton;
   
    public TextMeshProUGUI BuildingNameText;
    private IPowerPlantBuilding _powerObject;
    private MapObjectView _targetView;

    private Camera _mainCamera;
    
    private RectTransform _canvasRect;

    public override void Initialize(object powerObject)
    {
        base.Initialize(powerObject);
        _canvasRect = jUiManager.OverlayCanvasTransform;
        _powerObject = (IPowerPlantBuilding)powerObject;
        BuildingNameText.SetLocalizedText(_powerObject.Name.ToUpper());
        _targetView = GetTargetView();
        _mainCamera = Camera.main;
        UpdatePosition();
    }

    public void OnExitPressed() => Hide();
    
    private void UpdatePosition()
    {
        var bounds = _targetView.SpriteRenderer.bounds;
            
        var position = _targetView.Transform.position;
        position.y += 1.6f * bounds.size.y;

        var viewportPosition = _mainCamera.WorldToViewportPoint(position);
        var anchoredPosition = GetScreenPos(viewportPosition, _canvasRect);

        if (RectTransform.anchoredPosition == anchoredPosition)
        {
            return;
        }
            
        RectTransform.anchoredPosition = anchoredPosition;
    }

    private void LateUpdate()
    {
        if (Visible && _powerObject != null)
        {
            UpdatePosition();
        }
    }
    
    private Vector2 GetScreenPos(Vector3 viewPortPos, RectTransform canvasRect)
    {
        return new Vector2(
            ((viewPortPos.x * canvasRect.sizeDelta.x) - (_canvasRect.sizeDelta.x * 0.5f)),
            ((viewPortPos.y * canvasRect.sizeDelta.y)) - (_canvasRect.sizeDelta.y * 0.5f));
    }
}