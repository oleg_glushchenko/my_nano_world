﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace NanoReality.Engine.UI.Views.BuildingsShopPanel
{
    public class PagesContainerView : MonoBehaviour
    {
        public PageIconView pageIconPrefab;
        public Sprite selectedPage;
        public Sprite deselectedPage;

        List<PageIconView> pageIcons = new List<PageIconView>();
        private int _currecntItemsCount;
        private int _currenSelectedIndex;

        public void UpdatePages(int itemsCount, int visibleItems)
        {
            var count = itemsCount / visibleItems + 1;
            _currecntItemsCount = count;

            for (int i = 0; i < pageIcons.Count; i++)
            {
                pageIcons[i].icon.sprite = deselectedPage;
                pageIcons[i].gameObject.SetActive(i < count);
            }

            if (pageIcons.Count < count)
            {
                var size = count - pageIcons.Count;
                for (int i = 0; i < size; i++)
                {
                    var page = Instantiate(pageIconPrefab);
                    page.gameObject.transform.SetParent(gameObject.transform);
                    page.transform.localScale = Vector3.one;
                    pageIcons.Add(page);
                }
            }

            pageIcons[0].icon.sprite = selectedPage;
            _currenSelectedIndex = 0;
        }

        /// <summary>
        /// Используется для установки текущей страницы списка
        /// </summary>
        /// <param name="offset">Смещение по списку (от 0 до 1)</param>
        public void SetCurrentPage(float offset)
        {
            if (_currecntItemsCount == 0) return;

            // вычисляем индекс страницы по текущему смещению скролла
            int index = (int)(_currecntItemsCount * offset);

            // проверяем выход за диапазон контейнера страниц
            if (index < 0) index = 0;
            if (index >= _currecntItemsCount) index = _currecntItemsCount - 1;
            
            // если список не на первом или последнем элементе, сразу переключаемся на следующую страницу
            if(index == 0 && offset > 0.01f && _currecntItemsCount > 1) index = 1;
            if (index == _currecntItemsCount - 1 && offset < 1.0f && _currecntItemsCount > 1) index = _currecntItemsCount - 2;

            // если мы на той же странице, выходим
            if (index == _currenSelectedIndex) return;

            // устанавливаем текущую страницу
            pageIcons[_currenSelectedIndex].icon.sprite = deselectedPage;
            pageIcons[index].icon.sprite = selectedPage;
            _currenSelectedIndex = index;
        }
    }
}