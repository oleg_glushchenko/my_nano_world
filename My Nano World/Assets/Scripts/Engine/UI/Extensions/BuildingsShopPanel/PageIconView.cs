﻿using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Views.BuildingsShopPanel
{
    public class PageIconView : MonoBehaviour
    {
        public Image icon;
    }
}