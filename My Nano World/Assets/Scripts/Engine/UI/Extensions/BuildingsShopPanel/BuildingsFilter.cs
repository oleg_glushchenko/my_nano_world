﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.UI.Views.Hud;

using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Views.BuildingsShopPanel
{

    public class BuildingsFilter : MonoBehaviour
    {
        public CacheMonobehavior CacheMonobehavior => _cacheMonobehavior ?? (_cacheMonobehavior = new CacheMonobehavior(this));
        private CacheMonobehavior _cacheMonobehavior;

        public BuildingsCategories CurrentSelectedCategory =>
            Filters.FirstOrDefault(x => x.Toggle.isOn)?.BuildingsCategory ?? BuildingsCategories.None;
        
        public List<Filter> Filters;

        public event Action<BuildingsCategories> BuildingFilterChanged;
        
        protected void Start()
        {
            foreach (var filter in Filters)
            {
                filter.Toggle.onValueChanged.AddListener(_ =>
                {
                    if (filter.Toggle.isOn)
                    {
                        BuildingFilterChanged?.Invoke(filter.BuildingsCategory);
                    }
                });
            }
        }


        private void OnDestroy() => BuildingFilterChanged = null;

        [Serializable]
        public class Filter
        {
            public BuildingsCategories BuildingsCategory;

            public Toggle Toggle;
            public HudNotificationButton Notification;
        }
    }
}
