﻿using Assets.NanoLib.UI.Core.Views;
using Assets.NanoLib.Utilities;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.api;
using Assets.Scripts.GameLogic.Utilites;
using NanoReality.Engine.UI.Components;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using NanoLib.Services.InputService;
using Assets.Scripts.MultiLanguageSystem.Logic;
using NanoReality.GameLogic.Utilites;
using UnityEngine.EventSystems;

namespace NanoReality.Engine.UI.Views.BuildingsShopPanel
{
    public class ProductTooltip : UIPanelView, IPointerDownHandler
    {
        #region Injects

        [Inject]
        public IPlayerProfile jPlayerProfile { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Inject]
        public IProductsData jProductsData { get; set; }

        [Inject]
        public SignalOnBeginTouch jSignalOnBeginTouch { get; set; }

        #endregion

        #region InspectorFields

        /// <summary>
        /// Название товара
        /// </summary>
        [SerializeField] private TextMeshProUGUI _productName;

        /// <summary>
        /// Время производства товара
        /// </summary>
        [SerializeField] private TextGameObject _producteTime;

        /// <summary>
        /// Время производства товара
        /// </summary>
        [SerializeField] private TextGameObject _unlockLevel;

        /// <summary>
        /// Сколько товара на складе
        /// </summary>
        [SerializeField] private TextGameObject _warehouseCounter;

        #endregion

        /// <summary>
        /// Показывает товар
        /// </summary>
        public void ShowProduct(Product product, int time, RectTransform target)
        {
            if(_unlockLevel != null) _unlockLevel.gameObject.SetActive(false);
            _producteTime.gameObject.SetActive(true);
            _productName.SetLocalizedText(product.ProductName);
            _producteTime.Text.SetLocalizedText(UiUtilities.GetFormattedTimeFromSeconds_H_M_S(time));
            if (_warehouseCounter != null) //костыль, так как используется в других тултипах
                ShowWarehouseCounterInfo(jPlayerProfile.PlayerResources.GetProductCount(product.ProductTypeID));
            var rt = (RectTransform) transform;
            var newPos = target.position;
            rt.position = newPos;
            newPos = rt.anchoredPosition3D;
            newPos.y += target.sizeDelta.y * 0.2f;
            rt.anchoredPosition3D = newPos;
        }

        /// <summary>
        /// Показываем сколько итемов на складе
        /// </summary>
        private void ShowWarehouseCounterInfo(int count)
        {
            _warehouseCounter.gameObject.SetActive(true);
            _warehouseCounter.Text.text = count.ToString();
        }

        /// <summary>
        /// Показываем тултипа с уровнем для unlock
        /// </summary>
        /// <param name="level"></param>
        public void ShowUnlockInfo(int level)
        {
            if (_unlockLevel == null) return;
            _producteTime.gameObject.SetActive(false);
            _warehouseCounter.gameObject.SetActive(false);
            _unlockLevel.gameObject.SetActive(true);
            _unlockLevel.Text.text = string.Format(LocalizationUtils.LocalizeL2("Localization/TOOLTIP_UNLOCK_LABEL"), level + 1);
        }

        public override void Show()
        {
            base.Show();
            gameObject.SetActive(true);
            jSignalOnBeginTouch.AddListener(OnTouch);
          
        }

        public override void Hide()
        {
            base.Hide();
            gameObject.SetActive(false);
            jSignalOnBeginTouch.RemoveListener(OnTouch);
        }

        #region logic of hiding by touch
        private bool _ignoreTouch;

        private void OnTouch()
        {
            if (_ignoreTouch)
            {
                _ignoreTouch = false;
            }
            else
            {
                Hide();
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            _ignoreTouch = true;
        }
        #endregion
    }
}