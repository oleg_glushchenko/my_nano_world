﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Assets.NanoLib.UI.Core.Components;
using Assets.NanoLib.Utilities;
using NanoReality.Core.Resources;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using strange.extensions.mediation.impl;

namespace NanoReality.Engine.UI.Views.BuildingsShopPanel
{
    /// <summary>
    /// Контейнер товаров, отображает айтемы товаров
    /// </summary>
    public class ProductsContainer : View
    {
        [Inject] public IResourcesManager jResourcesManager { get; set; }
        
        [SerializeField] private BuildingShopProductItem _productPrefab;
        [SerializeField] private ItemContainer _productsContainer;
        
        private Dictionary<Product, int> _productTimes;
        private Dictionary<Id<Product>, int> _productsForBuild;

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void ShowProducts(Dictionary<Product, int> productsTimes)
        {
            _productTimes = productsTimes ?? throw new ArgumentNullException("productsTimes");

            InstantiateContainer();
            _productsContainer.ClearCurrentItems();

            foreach (var product in _productTimes)
            {
                var item = (BuildingShopProductItem) _productsContainer.AddItem();
                var icon = jResourcesManager.GetProductSprite(product.Key.ProductTypeID);
                item.SetProduct(product.Key, icon);
            }
        }

        private void InstantiateContainer()
        {
            if (_productsContainer.IsInstantiateed) return;
            _productsContainer.InstaniteContainer(_productPrefab, 0);
        }
    }
}