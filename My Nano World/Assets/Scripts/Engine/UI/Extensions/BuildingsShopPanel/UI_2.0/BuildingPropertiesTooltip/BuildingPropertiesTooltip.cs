﻿using System.Collections;
using System.Collections.Generic;
using NanoLib.Services.InputService;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.api;
using DG.Tweening;
using NanoReality.Engine.UI.Components;
using NanoReality.Engine.UI.Extensions.BuildingsShopPanel;
using NanoReality.Game.Services.BuildingsUnlockService;
using NanoReality.GameLogic.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Production;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.Services;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Views.BuildingsShopPanel
{

    public class BuildingPropertiesTooltip : UIPanelView, IPointerDownHandler
    {
        #region Inspector
        
        [SerializeField] private TextGameObject _population;
        [SerializeField] private TextGameObject _experience;
        [SerializeField] private TextGameObject _timeRequierments;
        [SerializeField] private TextGameObject _dimensions;
        [SerializeField] private ProductsContainer _productsContainer;
        [SerializeField] private ContentSizeFitter _sizeFilter;
        [SerializeField] private Button _closeButton;

        #endregion

        [Inject] public SignalOnBeginTouch jSignalOnBeginTouch { private get; set; }
        [Inject] public IBuildingsUnlockService jBuildingsUnlockService { get; set; }
        [Inject] public IBuildingService jBuildingService { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public IProductsData jProductsData { get; set; }

        public IMapBuilding MapObject { get; private set; }

        public bool CanHide { private get; set; } = true;

        private BuildingItem _targetItem = null;

        public override void Show()
        {
            base.Show();
            gameObject.SetActive(true);
            _closeButton.onClick.AddListener(OnTouch);
        }

        public override void Hide()
        {
            base.Hide();
            _closeButton.onClick.RemoveListener(OnTouch);
            gameObject.SetActive(false);
        }

        public void SetMapObject(IMapBuilding target, BuildingItem targetItem)
        {
            MapObject = target;
            _targetItem = targetItem;

            RefreshProperties();
            ShowProductsList();
            SetPositionImmediately();
            StartCoroutine(SetPosition());
            _sizeFilter.enabled = false;
            transform.localScale = Vector3.zero;
            transform.DOScale(Vector3.one, .1f).OnComplete(() =>
            {
                _sizeFilter.enabled = true;
                LayoutRebuilder.ForceRebuildLayoutImmediate(transform as RectTransform);
            });
        }

        private void ShowProductsList()
        {
            var productionBuilding = MapObject as IProductionBuilding;
            var agriculturalField = MapObject as IAgroFieldBuilding;
                
            if (productionBuilding == null && agriculturalField == null)
            {
                _productsContainer.Hide();
                return;
            }
            
            List<IProducingItemData> producingItems = productionBuilding == null 
                ? agriculturalField.AvailableAgroProducts 
                : productionBuilding.AvailableProducingGoods;

            var products = new Dictionary<Product, int>();
            
            foreach (IProducingItemData producingItem in producingItems)
            {
                products.Add(jProductsData.GetProduct(producingItem.OutcomingProducts), (int) producingItem.ProducingTime);
            }

            _productsContainer.ShowProducts(products);
            _productsContainer.Show();
        }
        
        private void RefreshProperties()
        {
            ShowPrimaryRequirements();
            ShowRewards();
        }

        protected override void Awake()
        {
            if (autoRegisterWithContext && !registeredWithContext)
                bubbleToContext(this, true, true);
            jSignalOnBeginTouch.AddListener(OnTouch);
        }

        #region Hide tooltip

        private bool _ignoreTouch;

        private void OnTouch()
        {
            if (_ignoreTouch)
            {
                _ignoreTouch = false;
                return;
            }
            if (Visible && CanHide)
                Hide();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            _ignoreTouch = true;
        }

        #endregion

        private void ShowPrimaryRequirements()
        {
            _dimensions.Text.text = $"{MapObject.Dimensions.X}X{MapObject.Dimensions.Y}";
            
            int constructionTime = GetConstructionTime();
            
            bool hideTimeSign = LocalizationMLS.Instance.Language == LocalizationMLS.ArabicLanguageCode;
            _timeRequierments.Text.text = hideTimeSign
                ? UiUtilities.GetFormattedTimeFromSecondsLetterless(constructionTime)
                : UiUtilities.GetFormattedTimeFromSeconds(constructionTime);

            bool timerEnabled = constructionTime > 0;

            _timeRequierments.gameObject.SetActive(timerEnabled);
        }

        private void ShowRewards()
        {
            int userExperience = GetUserExpForConstruct();
            _experience.Text.text = userExperience.ToString();
            _experience.gameObject.SetActive(userExperience > 0);
            
            bool isDwelling = MapObject.MapObjectType == MapObjectintTypes.DwellingHouse;
            
            if (isDwelling)
            {
                int buildingId = MapObject.ObjectTypeID;
                int buildingLevel = 1;
                var instancesCount = jGameManager.GetMapObjectsCountInCity(buildingLevel);
                var nextInstanceIndex = instancesCount + 1;
                int maxPopulation = jBuildingsUnlockService.GetPopulationForDwelling(buildingId, buildingLevel, nextInstanceIndex);
                
                _population.Text.text = maxPopulation.ToString();
            }
            
            _population.gameObject.SetActive(isDwelling);
        }

        private int GetUserExpForConstruct()
        {
            int buildingId = MapObject.ObjectTypeID;
            int buildingLevel = 0;
            int nextInstanceIndex = jGameManager.GetMapObjectsCountInCity(buildingId) + 1;

            bool isDwellingBuilding = MapObject.MapObjectType == MapObjectintTypes.DwellingHouse;
            if (isDwellingBuilding)
            {
                // For dwellings, we display experience for second level, because on first level it is a building site.
                buildingLevel++;
            }

            return jBuildingsUnlockService.GetExperienceReward(buildingId, buildingLevel, nextInstanceIndex);
        }

        private IEnumerator SetPosition()
        {
            // костыль, т.к. лаяуты обновляются не сразу
            yield return new WaitForEndOfFrame();

            SetPositionImmediately();
        }

        private void SetPositionImmediately()
        {
            Vector3 currentPos = transform.position;
            currentPos.x = _targetItem.transform.position.x;
            transform.position = currentPos;
        }
        
        private int GetConstructionTime()
        {
            int targetId = MapObject.ObjectTypeID;
            int targetLevel = 0;

            if (MapObject.MapObjectType == MapObjectintTypes.DwellingHouse)
            {
                targetLevel = 1;
            }

            int targetInstancesCount = jGameManager.GetMapObjectsCountInCity(targetId);
            int instanceIndex = targetInstancesCount + 1;
            return jBuildingService.GetBuildingConstructionTime(targetId, targetLevel, instanceIndex);
        }
    }
}