﻿using System.Collections.Generic;
using NanoLib.Core.Services.Sound;
using Assets.NanoLib.Utilities;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.api;
using NanoReality.Game.Services.BuildingsUnlockService;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Impl;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Services;
using UnityEngine;

namespace NanoReality.Engine.UI.Views.LevelUpPanel
{
    public class LevelUpContext
    {
        [Inject] public IPlayerExperience jPlayerExperience { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IBuildingsUnlockService jBuildingsUnlockService { get; set; }
        [Inject] public IProductsData jProductsData { get; set; }
        [Inject] public IMapObjectsData jMapObjectsData { get; set; }
        [Inject] public ISoundManager jSoundManager { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public IIconProvider jIconProvider { get; set; }
        [Inject] public IUserLevelsData jUserLevelsData { get; set; }

        public List<IAwardAction> GetAwardsForCurrentLevel() => jUserLevelsData.Levels[jPlayerProfile.CurrentLevelData.CurrentLevel].awards;

        public List<Product> GetProducts => jProductsData.Products;

        public IMapObject GetMapObject(Id_IMapObjectType type, int level) => jMapObjectsData.GetByBuildingIdAndLevel(type, level);

        public Sprite GetBuildingSprite(MapObjectId id) => jIconProvider.GetBuildingSprite(id);

        public UserLevel GetCurrentLevelObjectUserBalance => jPlayerExperience.ObjectUserLevelsesBalanceData.Levels[jPlayerExperience.CurrentLevel];

        public Product GetProductData(Id<Product> productId)
        {
            return jProductsData.GetProduct(productId);
        }
        
        public bool HasAnyBuildingOfType(Id_IMapObjectType objectType)
        {
            return jGameManager.FindMapObjectByTypeId<IMapObject>(objectType) != null;
        }
    }
}