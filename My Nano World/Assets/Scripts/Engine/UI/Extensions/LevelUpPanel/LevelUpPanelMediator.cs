﻿using System;
using System.Linq;
using Assets.NanoLib.UI.Core;
using Assets.Scripts.Engine.Analytics;
using Assets.Scripts.Engine.Analytics.Extensions.CurrencyInfo;
using Assets.Scripts.Engine.UI.Views.Hud;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Assets.Scripts.Engine.UI.Views.Tooltips;
using NanoLib.Core.Logging;
using NanoLib.Core.Services.Sound;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Impl;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilites;
using UnityEngine;

namespace NanoReality.Engine.UI.Views.LevelUpPanel
{
    public class LevelUpPanelMediator : UIMediator<LevelUpPanelView>
    {
        #region Injects

        [Inject] public LevelUpContext Context { get; set; }
        [Inject] public SignalOnNeedToShowTooltip jSignalOnNeedToShowTooltip { get; set; }
        [Inject] public SignalOnNeedToHideTooltip jSignalOnNeedToHideTooltip { get; set; }
        [Inject] public OnHudBottomPanelIsVisibleSignal jOnHudBottomPanelIsVisibleSignal { get; set; }
        [Inject] public SignalOnActionWithCurrency jSignalOnActionWithCurrency { get; set; }


        #endregion

        #region Properties
        private int CurrentLevel => Context.jPlayerExperience.CurrentLevel;

        private UserLevel GetCurrentLevelObjectUserBalance => Context.GetCurrentLevelObjectUserBalance;
        
        #endregion Properties

        protected override void OnShown()
        {
            base.OnShown();
            View.RewardClaimed += RewardClaimed;
            View.RewardBoxClicked += OnRewardBoxClicked;
            View.UpdateLevelUpText(GetCurrentLevelupText());
            View.PlayRewardBoxAnimation(true);
            InitAwards();
            UpdateGiftBoxSprite();
            jOnHudBottomPanelIsVisibleSignal.Dispatch(false);
            HideTooltip();
            jPopupManager.ForceCloseAllActivePopups();
            Context.jSoundManager.Play(SfxSoundTypes.LevelReached, SoundChannel.SoundFX);
        }

        protected override void OnHidden()
        {
            jOnHudBottomPanelIsVisibleSignal.Dispatch(true);
            View.RewardClaimed -= RewardClaimed;
            View.RewardBoxClicked -= OnRewardBoxClicked;
            base.OnHidden();
        }

        #region Expression bodied methods

        private void ActionWithCurrency(CurrencyInfo info) => jSignalOnActionWithCurrency.Dispatch(info);

        private void HideTooltip() => jSignalOnNeedToHideTooltip.Dispatch();

        #endregion Expression bodied methods

        private void UpdateGiftBoxSprite()
        {
            View.UpdateSprite(Context.jIconProvider.GetRewardBoxSprite(GetCurrentLevelObjectUserBalance.GiftBoxType));
        }

        private void InitAwards()
        {
            var awardActions = Context.GetAwardsForCurrentLevel();
            foreach (var awardAction in awardActions.Where(awardAction => awardAction.AwardType == AwardActionTypes.Resources))
            {
                awardAction.Execute(AwardSourcePlace.LevelUp, awardAction.AwardId.ToString());
            }

            for (var i = 0; i < Context.GetProducts.Count; i++)
            {
                try
                {
                    ProcessProducts(i);
                }
                catch (Exception exception)
                {
                    exception.LogException();
                }
            }

            var unlockedBuildings = Context.jBuildingsUnlockService.GetNewUnlockedBuildings(Context.jPlayerExperience.CurrentLevel);
            if (unlockedBuildings == null) return;
            
            foreach (var buildingId in unlockedBuildings)
            {
                try
                {
                    ProcessUnlockedBuilding(buildingId);
                }
                catch (Exception exception)
                {
                    $"Not found balance data for BuildingId: {buildingId}".LogError();
                    exception.LogException();
                }
            }
        }

        private string GetCurrentLevelupText()
        {
            var currentLevel = (CurrentLevel + 1).ToString();
            return string.Format(LocalizationUtils.LocalizeL2("UICommon/LEVELUP_UNITE_LABEL"), "\r\n", currentLevel);
        }
        
        private void ProcessUnlockedBuilding(int unlockedBuildingId)
        {
            IMapObject balanceBuilding = Context.GetMapObject(unlockedBuildingId, 0);

            if (balanceBuilding == null)
            {
                $"Target building balance data is null. [{unlockedBuildingId}]".Log(LoggingChannel.Buildings);
                return;    
            }

            bool isBuildingPresent = Context.HasAnyBuildingOfType(balanceBuilding.ObjectTypeID);

            if (isBuildingPresent) return;

            var item = View.AddItem();
            item.SetBuilding(balanceBuilding, Context.GetBuildingSprite(new MapObjectId(unlockedBuildingId)));
            item.SignalOnPointerUp.AddListener(HideTooltip);
            item.OnLongPress.AddListener(OnUnlockItemLongPress);
        }

        private void ProcessProducts(int productId)
        {
            Product product = Context.GetProducts[productId];
            if (product == null)
            {
                throw new NullReferenceException($"Target products balance data is null. [{productId}]");
            }
            
            if (product.UnlockLevel != CurrentLevel) return;
            LevelUpUnlokedItem item = View.AddItem();

            //todo: should listeners also be removed?
            item.SetProduct(product.ProductTypeID);
            item.SignalOnPointerUp.AddListener(HideTooltip);
            item.OnLongPress.AddListener(OnUnlockItemLongPress);
        }

        private void OnUnlockItemLongPress(LevelUpUnlokedItem levelUpUnlockedItem)
        {
            var tooltipData = new TooltipData
            {
                ItemTransform = (RectTransform) levelUpUnlockedItem.transform,
                ShowOnlySimpleTool = true
            };

            if (levelUpUnlockedItem.TargetProduct == null)
            {
                var balanceBuilding = Context.GetMapObject(levelUpUnlockedItem.MapObjectType, 0);
                tooltipData.Type = TooltipType.Simple;
                tooltipData.Message = balanceBuilding.Name;
            }
            else
            {
                tooltipData.Type = TooltipType.ProductItems;
                tooltipData.ProductItem = Context.GetProductData(levelUpUnlockedItem.TargetProduct);
            }

            jSignalOnNeedToShowTooltip.Dispatch(tooltipData);
        }

        private void RewardClaimed()
        {
            // give award on window close
            var awards = GetCurrentLevelObjectUserBalance.awards;

            for (var i = 0; i < awards.Count; i++)
            {
                ProcessAwards(awards[i]);
            }
        }
        
        private void ProcessAwards(IAwardAction award)
        {
            if (award.AwardType == AwardActionTypes.Resources) return;

            award.Execute(AwardSourcePlace.LevelUp, award.AwardId.ToString());
            ActionWithCurrency(CurrencyInfo.GetInfoFromAwardType(award, CurrencyIncomesActions.LvlUp));
        }
        
        private void OnRewardBoxClicked()
        {
            var userLevel = GetCurrentLevelObjectUserBalance;
            Hide();
            ShowPopup(new RewardBoxPopupSettings(userLevel.awards, userLevel.GiftBoxType));
        }
    }
}
