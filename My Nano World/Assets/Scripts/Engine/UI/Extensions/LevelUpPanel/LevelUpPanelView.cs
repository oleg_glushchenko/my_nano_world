﻿using System;
using Assets.NanoLib.UI.Core.Components;
using Assets.NanoLib.UI.Core.Views;
using DG.Tweening;
using NanoReality.Engine.UI.UIAnimations;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

namespace NanoReality.Engine.UI.Views.LevelUpPanel
{
	public class LevelUpPanelView : UIPanelView
	{
		#region InspectorFields

		[SerializeField] private LevelUpUnlokedItem _unlokedItemPrefab;
		[SerializeField] private ItemContainer _unlockedItemsContainer;
		[SerializeField] private ScrollRect _unlockItemsScroll;

		[SerializeField] private ScrollSnap _scrollSnap;

		[SerializeField] private TextMeshProUGUI _congratulationsText;

		[SerializeField] private GameObject _panelGameObject;
		[SerializeField] private GameObjectScaleEffect _panelScaleEffect;

		[Header("Reward Box")] 
		[SerializeField] private Image _rewardBoxImage;
		[SerializeField] private Button _rewardBoxButton;

		[SerializeField] private float _rewardBoxTweenScale;
		[SerializeField] private float _rewardBoxTweenDuration;

		[SerializeField] private Button _bigRewardBoxButton;

		#endregion

		private bool _clickLocked = true;
		private Tweener _rewardBoxTween;

		public event Action RewardBoxClicked;
		public event Action RewardClaimed;

		public override void Initialize(object parameters)
		{
			base.Initialize(parameters);
			if (!_unlockedItemsContainer.IsInstantiateed)
			{
				_unlockedItemsContainer.InstaniteContainer(_unlokedItemPrefab, 0);
			}
			else
			{

			}
			_unlockItemsScroll.horizontalNormalizedPosition = 0;
			_scrollSnap.StopMovement();

			_panelGameObject.SetActive(true);
		}
		
		protected override void HideEffectEnded(ISpecialEffect specialEffect)
		{
			base.HideEffectEnded(specialEffect);
			_unlockedItemsContainer.ClearCurrentItems();
		}


		private void OnEnable()
		{
			_rewardBoxButton.onClick.AddListener(OnRewardBoxClicked);
			_bigRewardBoxButton.onClick.AddListener(OnRewardBoxClicked);
		}

		private void OnDisable()
		{
			_rewardBoxButton.onClick.RemoveListener(OnRewardBoxClicked);
			_bigRewardBoxButton.onClick.RemoveListener(OnRewardBoxClicked);
		}

		public LevelUpUnlokedItem AddItem()
		{
			return _unlockedItemsContainer.AddItem() as LevelUpUnlokedItem;
		}
		
		public void UpdateLevelUpText(string levelupText)
		{
			_congratulationsText.text = levelupText;
		}

		public void PlayRewardBoxAnimation(bool isActive)
		{
			KillRewardBoxAnimation();

			if (!isActive) return;

			_rewardBoxImage.transform.localScale = Vector3.one;
			Vector3 tweenScale = _rewardBoxTweenScale * Vector3.one;
			_rewardBoxTween = _rewardBoxImage.transform.DOScale(tweenScale, _rewardBoxTweenDuration)
				.SetEase(Ease.InOutCubic).SetLoops(-1, LoopType.Yoyo);
		}

		private void KillRewardBoxAnimation()
		{
			_rewardBoxTween?.Kill();
			_rewardBoxTween = null;
		}

		protected override void ShowEffectEnded(ISpecialEffect specialEffect)
		{
			base.ShowEffectEnded(specialEffect);
			_clickLocked = false;
			_unlockItemsScroll.horizontalNormalizedPosition = 0;
			_scrollSnap.RefreshButtons(0);
		}

		public override void Hide()
		{
			if (_clickLocked)
				return;

			KillRewardBoxAnimation();
			base.Hide();
		}

		public void UpdateSprite(Sprite sprite) => _rewardBoxImage.sprite = sprite;

		private void OnRewardBoxClicked()
		{
			if (_clickLocked)
			{
				return;
			}

			RewardBoxClicked?.Invoke();
			RewardClaimed?.Invoke();
			_panelScaleEffect.EffectStopped.AddOnce(effect => _panelGameObject.SetActive(false));
			_panelScaleEffect.PlayReverse();
		}
	}
}