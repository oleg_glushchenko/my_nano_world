﻿using UnityEngine;
using UnityEngine.UI;

using Assets.NanoLib.Utilities.Pulls;
using NanoReality.GameLogic.Quests;

public class LevelUpPanelItem : GameObjectPullable
{
    [SerializeField]
	RawImage ItemImage;
	[SerializeField]
	Text ItemName;
    [SerializeField] 
    private Text CountValue;
    [SerializeField] 
    private GameObject CounterObject;

	public void Show (string _name, int count, Texture2D _icon) {
        ItemName.text = _name;
        ItemImage.texture = _icon;
        CounterObject.SetActive(count > 0);
        CountValue.text = count.ToString();
	}

    public void Show(string _name, Texture2D _icon)
    {
        ItemName.text = _name;
        ItemImage.texture = _icon;
        CounterObject.SetActive(false);
    }
}
