﻿using Assets.NanoLib.Utilities.Pulls;
using NanoReality.GameLogic.Quests;
using strange.extensions.mediation.impl;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


namespace NanoReality.Engine.UI.Views.LevelUpPanel
{
    /// <summary>
    /// Айтем награды в окне левел апа
    /// </summary>
    public class LevelUpRewardItem : View, IPullableObject
    {
        /// <summary>
        /// Иконка товара
        /// </summary>
        [SerializeField]
        private Image _awardImage;

        /// <summary>
        /// Лейбла кол-ва товара
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI _awardCount;

        public Image Icon { get { return _awardImage; }}
        
        public IAwardAction Reward { get; private set; }
        
        /// <summary>
        /// Устанавливает товар в айтем
        /// </summary>
        /// <param name="awardSprite">спрайт награды</param>
        /// <param name="count">кол-во награды</param>
        public void SetReward(Sprite awardSprite, IAwardAction reward)
        {
            Reward = reward;
            _awardImage.sprite = awardSprite;
            _awardCount.text = reward.AwardCount.ToString();
        }

        #region IPullable
        public object Clone()
        {
           return Instantiate(this);
        }

        public IObjectsPull pullSource { get; set; }
        public void OnPop()
        {
           gameObject.SetActive(true);
        }

        public void OnPush()
        {
           gameObject.SetActive(false);
            _awardImage.sprite = null;
            _awardCount.text = null;
        }

        public void FreeObject()
        {
           if(pullSource != null)
               pullSource.PushInstance(this);
           else
           {
               DestroyObject();
           }
        }

        public void DestroyObject()
        {
            Destroy(gameObject);
        }
        #endregion

    }
}