﻿using Assets.Scripts.GameLogic.Utilites;
using Assets.Scripts.MultiLanguageSystem.Logic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Extensions.Authorize
{
    public class CityShortDataView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _cityName;
        [SerializeField] private TextMeshProUGUI _cityLevel;
        [SerializeField] private TextMeshProUGUI _cityPopulation;
        [SerializeField] private Image _cityImage;

        public void UpdateCity(string cityName, int level, int population, Sprite citySprite)
        {
            _cityName.text = cityName;
            _cityLevel.SetLocalizedText(LocalizationMLS.Instance.GetText(LocalizationKeyConstants.CHANGEACCOUNT_CITY_LEVEL) + " " + (level + 1).ToString());
            _cityPopulation.text = population.ToString();
            _cityImage.sprite = citySprite;
        }
    }
}