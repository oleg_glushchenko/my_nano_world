﻿using System;
using Assets.NanoLib.UI.Core.Views;
using UnityEngine;

namespace NanoReality.Engine.UI.Extensions.Authorize
{
    public class SelectWorldView : UIPanelView
    {
        [SerializeField] private CityShortDataView _currentCityShortData;
        [SerializeField] private CityShortDataView _anotherCityShortData;
        [SerializeField] private Sprite _poorCitySprite;
        [SerializeField] private Sprite _createCitySprite;

        public CityShortDataView CurrentCityShortData => _currentCityShortData;
        public CityShortDataView AnotherCityShortData => _anotherCityShortData;
        public Sprite PoorCitySprite => _poorCitySprite;
        public Sprite CreateCitySprite => _createCitySprite;
        public event Action CurrentWorldClick;
        public event Action AnotherWorldClick;

        public void OnCurrentWorldButtonClick()
        {
            CurrentWorldClick?.Invoke();
        }

        public void OnAnotherWorldButtonClick()
        {
            AnotherWorldClick?.Invoke();
        }
    }
}