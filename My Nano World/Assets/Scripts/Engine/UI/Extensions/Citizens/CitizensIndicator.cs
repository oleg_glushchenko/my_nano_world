﻿using Assets.NanoLib.UI.Core.Views;
using Engine.UI;
using NanoReality.Game.Services.BuildingsUnlockService;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.UI.Components;

public class CitizensIndicator : UIPanelView
{
	[Inject] public AddRewardItemSignal jAddRewardItemSignal { get; set; }
	[Inject] public IGameManager jGameManager { get; set; }
	[Inject] public SignalOnMapObjectBuildFinished jSignalOnMapObjectBuildFinished { get; set; }
	[Inject] public IBuildingsUnlockService jBuildingUnlockService { get; set; }


	[PostConstruct]
	public void PostConstruct()
	{
		jSignalOnMapObjectBuildFinished.AddListener(OnBuildingComplete);
	}

	private void OnBuildingComplete(IMapObject mapObject)
	{
		if (mapObject.MapObjectType == MapObjectintTypes.Road)
		{
			return;
		}

		var dwellingBuilding = mapObject as IDwellingHouseMapObject;
		if (dwellingBuilding == null || dwellingBuilding.MaxPopulation <= 0)
		{
			return;
		}

		var mapObjectView = jGameManager.GetMapView(mapObject.SectorId).GetMapObjectById(mapObject.MapID);
		if (mapObjectView == null)
		{
			return;
		}

		var populationIncrease = dwellingBuilding.MaxPopulation;
		if (dwellingBuilding.Level > 0)
		{
			int previousMax = jBuildingUnlockService.GetPopulationForDwelling(dwellingBuilding.ObjectTypeID, dwellingBuilding.Level - 1, dwellingBuilding.InstanceIndex);
			populationIncrease = dwellingBuilding.MaxPopulation - previousMax;
		}

		jAddRewardItemSignal.Dispatch(new AddItemSettings(FlyDestinationType.Citizens, mapObjectView.CacheMonobehavior.CachedTransform.position, populationIncrease.ToString()));
	}
}