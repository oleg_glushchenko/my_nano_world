﻿using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.MultiLanguageSystem.Logic;
using NanoLib.UI.Core;
using NanoReality.Engine.UI.Views;
using UnityEngine;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.Happiness;
using TMPro;

public class EntertaimentPanelView : BuildingPanelView
{
    public TextMeshProUGUI BuildingNameText;
    [SerializeField] private BuildingUpgradeButtonView _upgradeButton;

    public BuildingUpgradeButtonView UpgradeButton => _upgradeButton;
    private RectTransform _uiCanvasRect;
    private MapObjectView _mapObjectView;
    private Camera _mainCamera;
    
    [Inject] public IUIManager jUiManager { get; set; }

    public override void Initialize(object parameters)
    {
        base.Initialize(parameters);
        _uiCanvasRect = jUiManager.OverlayCanvasTransform;
        _mapObjectView = GetTargetView();
        _mainCamera = Camera.main;
    }

    public override void Show()
    {
        base.Show();
        BuildingNameText.SetLocalizedText(TargetBuilding.Name.ToUpper());
    }

    public override void Hide()
    {
        base.Hide();
        BuildingNameText.text = string.Empty;
    }

    public void OnExitPressed() => Hide();
    
    private void UpdatePosition()
    {
        var bounds = _mapObjectView.SpriteRenderer.bounds;
            
        var position = _mapObjectView.Transform.position;
        position.y += 1.7f * bounds.size.y;

        var viewportPosition = _mainCamera.WorldToViewportPoint(position);
        var anchoredPosition = GetScreenPos(viewportPosition, _uiCanvasRect);

        if (RectTransform.anchoredPosition == anchoredPosition)
        {
            return;
        }
            
        RectTransform.anchoredPosition = anchoredPosition;
    }
    
    private void LateUpdate()
    {
        if (Visible && _mapObjectView != null)
        {
            UpdatePosition();
        }
    }
    
    private Vector2 GetScreenPos(Vector3 viewPortPos, RectTransform canvasRect)
    {
        Vector2 canvasSizeDelta = canvasRect.sizeDelta;
        
        return new Vector2(
            (viewPortPos.x * canvasSizeDelta.x) - canvasSizeDelta.x * 0.5f,
            (viewPortPos.y * canvasSizeDelta.y) - canvasSizeDelta.y * 0.5f);
    }
}
