﻿using System;
using NanoReality.GameLogic.StartApplicationSequence.Controllers.Signals;
using UnityEngine;
using System.Collections;
using Assets.NanoLib.ServerCommunication.Models;
using Assets.NanoLib.UI.Core;
using Assets.NanoLib.Utilities;
using I2.Loc;
using NanoLib.Core.Services.Sound;
using NanoReality.Core.Resources;
using NanoReality.Engine.UI.Extensions.LoaderPanel;
using NanoReality.Game.Network.Commands;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.GameManager.Controllers;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Settings.GameSettings.Models.Api;
using NanoReality.GameLogic.StartApplicationSequence.Controllers.Commands;
using UniRx;
using Random = UnityEngine.Random;

namespace NanoReality.Game.UI
{
    public class LoaderPanelMediator : UIMediator<LoaderPanelView>
    {
        [Inject] public IGameSettings jGameSettings { get; set; }
        [Inject] public HidePreloaderAfterGameLaunchSignal jHidePreloaderAfterGameLaunchSignal { get; set; }
        [Inject] public SignalOnCityMapLoaded jSignalOnCityMapLoaded { get; set; }
        [Inject] public SignalOnRequestError jSignalOnRequestError { get; set; }
        [Inject] public GameDataLoadedSignal jGameBalanceLoadedSignal { get; set; }
        [Inject] public SignalPostLoadingAction jSignalPostLoadingAction { get; set; }
        [Inject] public AuthorizationStartedSignal jAuthorizationStartedSignal { get; set; }
        [Inject] public AuthorizationSuccessSignal jAuthorizationSuccessSignal { get; set; }
        [Inject] public BundleLoadedSignal jBundleLoadedSignal { get; set; }
        [Inject] public BundlesLoadingStartedSignal jBundlesLoadingStartedSignal { get; set; }
        [Inject] public BundlesLoadingFinishedSignal jBundlesLoadingFinishedSignal { get; set; }

        private float _currentProgress;
        private int _bundlesForLoading;
        private int _loadedBundles;
        private bool _isGameLoaded;
        private IDisposable _timerDisposable;
        private IDisposable _progressBarDisposable;
        
        private void OnAuthorizationStarted() => SetProgressValue(0.1f);
        private void OnAuthorizationSuccess(int _) => SetProgressValue(0.15f);
        private void OnGameConfigLoaded() => SetProgressValue(0.75f);
        private void OnCityMapLoaded(Id<IPlayerProfile> _, IUserCity __, ICityMap ___) => SetProgressValue(0.95f);

        protected override void Show(object param)
        {
            base.Show(param);
            
            jSoundManager.SetChannelActive(false, SoundChannel.SoundFX);
            
            var reload = param != null;
            
            View.StartCurrentCloudAnimation(reload);

            if (!reload)
            {
                SetProgressValue(0.05f);
                jSoundManager.Play(SfxSoundTypes.LoadingScreen, SoundChannel.Music, true);
                return;
            }
            
            var action = (Action) param;
            View.PreloaderCloudShowEnded += () => action.Invoke();
            
            ShowNextHint();
        }

        protected override void OnShown()
        {
            base.OnShown();
            
            jAuthorizationStartedSignal.AddListener(OnAuthorizationStarted);
            jAuthorizationSuccessSignal.AddListener(OnAuthorizationSuccess);
            jHidePreloaderAfterGameLaunchSignal.AddListener(OnGameLoaded);

            jGameBalanceLoadedSignal.AddListener(OnGameConfigLoaded);
            jSignalOnCityMapLoaded.AddListener(OnCityMapLoaded);
            jSignalOnRequestError.AddListener(OnRequestError);

            jBundlesLoadingStartedSignal.AddListener(OnBundlesLoadingStarted);
            jBundlesLoadingFinishedSignal.AddListener(OnBundlesLoadingFinished);
            jBundleLoadedSignal.AddListener(OnBundleLoaded);
            
        }
        
        #region Asset Bundles Loading Label
        
        private void OnBundlesLoadingStarted(int totalBundlesCount)
        {
            _timerDisposable?.Dispose();
            
            View.SetHintsText("LoaderHint/LOADER_HINT_WEAK_CONNECTION");

            var localeKey = $"Localization/{(jGameSettings.IsHighQualityGraphic ? "LOADER_LABEL_DL_ASSETS" : "LOADER_LABEL_UNPACKING")}";
            
            View.SetLoadingAssetsText(LocalizationManager.GetTranslation(localeKey));

            _loadedBundles = 0;
            _bundlesForLoading = totalBundlesCount;
            
            View.SetActiveLoadingAssets(true);
            View.SetLoadedAssetsText($"0/{totalBundlesCount}");

            AssetBundleManager.OnLoadingProgressUpdate += OnBundlesLoadingProgressUpdated;
        }

        private void OnBundlesLoadingProgressUpdated(ulong downloadedBytes, ulong totalBytes)
        {
            View.SetLoadedAssetsText($"{_loadedBundles}/{_bundlesForLoading} ({ConvertMegaBytes(downloadedBytes):F2}/{ConvertMegaBytes(totalBytes):F2} MB)");
        }

        private void OnBundleLoaded(int bundleIndex)
        {
            _loadedBundles = bundleIndex;
            View.SetLoadedAssetsText($"{bundleIndex}/{_bundlesForLoading}");
            
            var newProgress = _currentProgress + 0.04f;
            SetProgressValue(newProgress);
        }
        
        private void OnBundlesLoadingFinished(bool _)
        {
            AssetBundleManager.OnLoadingProgressUpdate -= OnBundlesLoadingProgressUpdated;
            
            SetProgressValue(0.75f);
            View.SetActiveLoadingAssets(false);
            ShowNextHint();
        }
        
        #endregion

        private void OnGameLoaded()
        {
            SetProgressValue(1);
            jHidePreloaderAfterGameLaunchSignal.RemoveListener(OnGameLoaded);
            jSoundManager.Stop(SfxSoundTypes.LoadingScreen, SoundChannel.Music);
            jSoundManager.SetChannelActive(jGameSettings.IsEffectsEnabled, SoundChannel.SoundFX);
            _isGameLoaded = true;
        }

        private void SetProgressValue(float progress)
        {
            _progressBarDisposable?.Dispose();
            _progressBarDisposable = Observable.FromCoroutine(() => IncreaseProgress(progress)).Subscribe();
        }

        private IEnumerator IncreaseProgress(float comingProgress)
        {
            while (_currentProgress < comingProgress)
            {
                _currentProgress += 0.15f * Time.deltaTime;
                _currentProgress = Mathf.Clamp01(_currentProgress);
                View.UpdateProgressbar(_currentProgress);
                yield return new WaitForEndOfFrame();
            }

            while (!_isGameLoaded)
            {
                yield return null;
            }

            View.PlayReverseCloudAnimation();
            yield return new WaitForSeconds(1f);

            Hide();
            jSignalPostLoadingAction.Dispatch();
        }

        private void ShowNextHint()
        {
            SetHint();
            _timerDisposable?.Dispose();
            _timerDisposable = Observable.Timer(TimeSpan.FromSeconds(8)).Repeat().Subscribe(_ => { SetHint(); });
        }

        private void SetHint()
        {
            var tip = Random.Range(1, 20);
            View.SetHintsText($"LoaderHint/LOADER_HINT_DEFAULT_{tip}");
        }

        private void OnRequestError(IErrorData _)
        {
            _progressBarDisposable?.Dispose();
        }

        private float ConvertMegaBytes(ulong bytes)
        {
            return bytes / 1024f / 1024f;
        }
        
        protected void OnDisable()
        {
            base.OnHidden();
            
            _currentProgress = 0;
            _timerDisposable?.Dispose();
            _progressBarDisposable?.Dispose();
            
            jBundlesLoadingFinishedSignal.RemoveListener(OnBundlesLoadingFinished);
            jGameBalanceLoadedSignal.RemoveListener(OnGameConfigLoaded);
            jSignalOnCityMapLoaded.RemoveListener(OnCityMapLoaded);
            jSignalOnRequestError.RemoveListener(OnRequestError);
            
            jBundlesLoadingStartedSignal.RemoveListener(OnBundlesLoadingStarted);
            jBundlesLoadingFinishedSignal.RemoveListener(OnBundlesLoadingFinished);
            jBundleLoadedSignal.RemoveListener(OnBundleLoaded);
            
            jSoundManager.Stop(SfxSoundTypes.LoadingScreen, SoundChannel.Music);
        }
    }
}