﻿using NanoReality.GameLogic.Achievements;
using strange.extensions.signal.impl;
using UnityEngine;
using System;
using Assets.Scripts.MultiLanguageSystem.Logic;
using Engine.UI.Components;
using NanoReality.Core;
using NanoReality.GameLogic.Quests;
using TMPro;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Extensions.AchievementPanel
{
	public class AchievementItemView : InfiniteItem
    {
		#region Inspector

		[SerializeField] private GameObject _questNotCompletedUI;
		[SerializeField] private GameObject _questCompletedUI;
		[SerializeField] private GameObject _questEarnedUI;
		[SerializeField] private TextMeshProUGUI _questTitle;
		[SerializeField] private TextMeshProUGUI _questDescription;
		[SerializeField] private TextMeshProUGUI _awardText;
		[SerializeField] private TextMeshProUGUI _awardTextClaimed;
		[SerializeField] private TextMeshProUGUI _progressDescr;
		[SerializeField] private Button _claimButton;
		[SerializeField] private FillImageProgressBar _progressBar;

		#endregion

		public IUserAchievement CurrentAchivement { get; private set; }

		public Action<int, Transform> OnClaimClick;

		void Start()
		{
			_claimButton.onClick.AddListener(OnClimeButtonClick);
		}

        public void UpdateState()
        {
	        _questNotCompletedUI.SetActive (!CurrentAchivement.IsEarned);
	        _questEarnedUI.SetActive (CurrentAchivement.IsEarned && !CurrentAchivement.IsCompleted());
			_questCompletedUI.SetActive(CurrentAchivement.IsCompleted());

			var achievement = CurrentAchivement.GetBalanceAchievementObject ();
			_questDescription.SetLocalizedText(achievement.Description);
			
			var req = achievement.GetConditionRequirement (0);
            var curr = Mathf.Min(req.CurrentCount, req.NeededCount); // if current count bigger than needed
			_questTitle.SetLocalizedText(achievement.Title);

			_progressDescr.text = $"{curr.ToString()}/{req.NeededCount.ToString()}";
			_progressBar.Value = (float) curr / req.NeededCount;
        }

        public void UpdateItem(IUserAchievement userQuest)
        {
            CurrentAchivement = userQuest;
            
			UpdateState();

			var balance = CurrentAchivement.GetBalanceAchievementObject ();

	        if (balance.AwardActions != null && balance.AwardActions.Count > 0)
	        {
		        _awardText.text = balance.AwardActions[0].AwardCount.ToString();
	        }
	        else
	        {
		        const string zero = "0";
		        _awardText.text = zero;
	        }

	        _awardTextClaimed.text = _awardText.text;
        }
		
        private void OnClimeButtonClick()
        {
	        OnClaimClick(Index, _claimButton.transform);
        }
    }
}