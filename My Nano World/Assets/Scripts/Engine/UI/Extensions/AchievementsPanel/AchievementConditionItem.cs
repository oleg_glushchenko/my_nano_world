﻿using Assets.NanoLib.Utilities.Pulls;
using NanoReality.GameLogic.Quests;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Extensions.AchievementPanel
{
    /// <summary>
    /// Achievement item prefab script
    /// </summary>
    public class AchievementConditionItem : GameObjectPullable
    {
        public Text Description;
        public Image CounterBackground;
        public Sprite CompleteCountSprite;
        public Sprite NotCompleteCountSprite;
        public Color CompleteColor;
        public Color NotCompleteColor;
        private string _caption;
        private Text _title;
        private ICondition _currentCondition;

        public void UpdateItem(Text title, string descript, ICondition condition)
        {
            _currentCondition = condition;
            _title = title;
            _caption = title.text;
            Description.text = descript;
            UpdateState();
        }

        public void UpdateState()
        {
            var r = _currentCondition.GetConditionRequirement();
            SetData(r.CurrentCount, r.NeededCount);
        }


        private void SetData(int current, int needed)
        {
            if (current >= needed)
            {
                _title.color = CompleteColor;
                current = needed;
            }
            else
            {
                _title.color = NotCompleteColor;
            }
            _title.text = _caption + " " + current + "/" + needed;
        }

        public override void OnPush()
        {
            base.OnPush();
            _currentCondition = null;
        }
    }

}

