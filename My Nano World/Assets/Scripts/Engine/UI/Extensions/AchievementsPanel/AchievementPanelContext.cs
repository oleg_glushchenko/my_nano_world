﻿using NanoReality.GameLogic.Achievements;

namespace NanoReality.Engine.UI.Extensions.AchievementPanel
{
    public class AchievementPanelContext
    {
        [Inject] public IUserAchievementsData UserAchievementData { get; set; }
    }
}