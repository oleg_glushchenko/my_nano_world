﻿using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.GameLogic.Utilites;
using Assets.Scripts.MultiLanguageSystem.Logic;
using NanoReality.Core;
using NanoReality.Engine.UI.UIAnimations;
using NanoReality.Game.Tutorial;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Engine.UI.Extensions.AchievementPanel
{

    public class AchievementPanelView : UIPanelView
    {
        #region Inspector       
        [SerializeField] private ScrollRect _scroll;
        [SerializeField] private TextMeshProUGUI _headerText;
        [SerializeField] private Button _closeButton;
        [SerializeField] private InfiniteScroll _infiniteScroll;

        #endregion
        
        public InfiniteScroll InfiniteScroll => _infiniteScroll;
        
        public override void Initialize(object parameters)
        {
            base.Initialize(parameters);
            UpdateOnShow();
            _infiniteScroll.gameObject.SetActive(false);
            _closeButton.onClick.RemoveAllListeners();
            _closeButton.onClick.AddListener(Hide);
        }

        private void UpdateOnShow()
        {
            _headerText.SetLocalizedText(LocalizationMLS.Instance.GetText(LocalizationKeyConstants.ACHIEVEMENTS_LABEL_ACHIEVEMENTS));
            _scroll.verticalNormalizedPosition = 1;
        }
        
        protected override void ShowEffectEnded(ISpecialEffect specialEffect)
        {
           base.ShowEffectEnded(specialEffect);
           _scroll.verticalNormalizedPosition = 1;
           _infiniteScroll.gameObject.SetActive(true);
           
        }
    }
}