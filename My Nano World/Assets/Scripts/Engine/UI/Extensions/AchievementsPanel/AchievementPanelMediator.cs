﻿using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.UI.Core;
using Assets.NanoLib.UI.Core.Signals;
using Assets.Scripts.Engine.Analytics;
using Assets.Scripts.Engine.Analytics.Model;
using NanoLib.Core.Services.Sound;
using NanoReality.GameLogic.Achievements;
using NanoReality.GameLogic.Achievements.impl;
using NanoReality.GameLogic.Quests;
using UnityEngine;

namespace NanoReality.Engine.UI.Extensions.AchievementPanel
{
    public class AchievementPanelMediator : UIMediator<AchievementPanelView>
    {
        [Inject] public AchievementPanelContext Context { get; set; }
        [Inject] public SignalOnUserAchievementUnlocked jSignalOnAchievementUnlock { get; set; }
        [Inject] public SignalOnAwardTaken jSignalOnAwardTaken { get; set; }
        [Inject] public AchievementCompletedSignal jAchievementCompletedSignal { get; set; }
        [Inject] public AchievementConditionUpdateSignal jAchievementConditionUpdateSignal { get; set; }
        [Inject] public SignalOnRewardClaimed jSignalOnRewardClaimed { get; set; }
        [Inject] public SignalOnAchievementCompleted jSignalOnAchievementCompleted { get; set; }

        public void RewardClaimed(Achievement achievement, Transform trans) =>
            jSignalOnRewardClaimed.Dispatch(achievement, trans);

        public void AchievementCompleted(AchievementInfo info) => jSignalOnAchievementCompleted.Dispatch(info);

        private List<IUserAchievement> _userAchievements = new List<IUserAchievement>();

        private void UpdateAchievements()
        {
            FilterUserAchievements();
            SortUserAchievement();
            View.InfiniteScroll.InitData(_userAchievements.Count);
            UpdateVisible();
        }
        
        protected override void OnShown()
        {
            base.OnShown();
            LocalizationMLS.Instance.OnChangedLanguage += UpdateAchievements;
            jSignalOnAchievementUnlock.AddListener(UpdateAchievements);
            jAchievementCompletedSignal.AddListener(UpdateAchievements);
            jAchievementConditionUpdateSignal.AddListener(UpdateAchievements);
            jSignalOnAwardTaken.AddListener(OnAwardTaken);

            FilterUserAchievements();
            SortUserAchievement();
            View.InfiniteScroll.OnFill += OnFillItem;
            View.InfiniteScroll.OnHeight += OnHeightItem;
            View.InfiniteScroll.InitData(_userAchievements.Count);
            SubscribeOnClaim();
        }

        private void SubscribeOnClaim()
        {
            foreach (var visibleView in View.InfiniteScroll.VisibleViews)
            {
                var view = (AchievementItemView) visibleView;
                view.OnClaimClick += OnClaimClickHandler;
            }
        }
        
        private void UnsubscribeOnClaim()
        {
            foreach (var visibleView in View.InfiniteScroll.VisibleViews)
            {
                var view = (AchievementItemView) visibleView;
                view.OnClaimClick = null;
            }
        }

        private void OnClaimClickHandler(int index, Transform transform)
        {
            var userAchievement = _userAchievements[index];
            var achievement = userAchievement.GetBalanceAchievementObject();
            
            AchievementCompleted(new AchievementInfo(
                achievement.Title, userAchievement.BalanceID.Value, achievement.AwardActions
            ));
            
            userAchievement.TakeAward(null, null);

            RewardClaimed(userAchievement.GetBalanceAchievementObject(), transform);
        }

        private void UpdateVisible()
        {
            foreach (var visibleView in View.InfiniteScroll.VisibleViews)
            {
                var view = (AchievementItemView) visibleView;
                view.UpdateItem(_userAchievements[visibleView.Index]);
            }
        }
        
        void OnFillItem (int index, object item) 
        {
            ((AchievementItemView)item).UpdateItem(_userAchievements[index]);
        }

        int OnHeightItem (int index) 
        {
            return 266;
        }

        protected override void OnHidden()
        {
            LocalizationMLS.Instance.OnChangedLanguage -= UpdateAchievements;
            jSignalOnAchievementUnlock.RemoveListener(UpdateAchievements);
            jAchievementCompletedSignal.RemoveListener(UpdateAchievements);
            jAchievementConditionUpdateSignal.RemoveListener(UpdateAchievements);
            jSignalOnAwardTaken.RemoveListener(OnAwardTaken);
            UnsubscribeOnClaim();
            View.InfiniteScroll.OnFill -= OnFillItem;
            View.InfiniteScroll.OnHeight -= OnHeightItem;
            base.OnHidden();
        }
        
        private void OnAwardTaken(IUserAchievement arg1, List<IAwardAction> arg2)
        {
            UpdateAchievements();
        }
        
        private void FilterUserAchievements()
        {
            _userAchievements.Clear();
            foreach (KeyValuePair<int,List<IUserAchievement>> entry in Context.UserAchievementData.UserAchievements)
            {
                var userAchievements = entry.Value
                    .OrderByDescending(a => a.IsEarned);

                int earned = 0;
                int completed = 0;
                
                foreach (var userAchievement in userAchievements)
                {
                    if (userAchievement.IsEarned)
                    {
                        earned++;

                        if (!userAchievement.IsCompleted())
                        {
                            _userAchievements.Add(userAchievement);
                        }
                        else
                        {
                            completed++;
                        }
                    }
                }

                if (completed == userAchievements.Count())
                {
                    var first = userAchievements.Reverse().FirstOrDefault();

                    if (first != null && first.IsCompleted())
                    {
                        _userAchievements.Add(first);
                    }
                } else
                if (earned == completed)
                {
                    var first = userAchievements.FirstOrDefault(a => !a.IsEarned);
                    if (first != null)
                    {
                        _userAchievements.Add(first);
                    }
                }
            }
        }
        
        private void SortUserAchievement()
        {
            _userAchievements = _userAchievements
                .OrderBy(a => a.IsCompleted())
                .ThenBy(a => !a.IsEarned)
                .ThenByDescending(a =>
                {
                    var req = a.GetBalanceAchievementObject().GetConditionRequirement (0);
                    float progress = (float) req.CurrentCount / req.NeededCount;
                    return progress;
                })
                .ThenBy(a =>a.GroupKey)
                .ToList();
        }
    }
}
