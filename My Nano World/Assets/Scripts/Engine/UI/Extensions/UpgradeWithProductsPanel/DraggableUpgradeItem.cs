﻿using System;
using System.Collections;
using Assets.Scripts.Engine.UI.Extensions.ProductionPanel;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.api;
using NanoReality.Core.Resources;
using NanoReality.GameLogic.Services;
using UniRx;

public enum DraggableUpgradeItemStatus
{
    Empty,
    ProductsNotEnought,
    ProductsEnought,
    Loaded,
}

public class DraggableUpgradeItem : HoverDraggableItem
{
    #region Inspector Fields

    [SerializeField] private GameObject _backgroundHighlight;

    [SerializeField] private TextMeshProUGUI _productCounterText;

    [SerializeField] private Image _productCounterBack;

    [SerializeField] private Sprite _productsEnoughCounterSprite;

    [SerializeField] private Sprite _productsNotEnoughCounterSprite;

    [SerializeField] private GameObject ReadyMark;

    [SerializeField] private GameObject _specialOrderIconHolder;

    [SerializeField] private Image _specialOrderImage;

    #endregion

    #region Injections

    [Inject] public IPlayerProfile jPlayerProfile { get; set; }
    [Inject] public IProductsData jProductsData { get; set; }
    [Inject] public IIconProvider jIconProvider { get; set; }
    [Inject] public IResourcesManager jResourcesManager { get; set; }
    
    #endregion

    public DraggableUpgradeItemStatus CurrentStatus { get; private set; }

    public IProductForUpgrade CurrentProductForUpgrade => _currentProductForUpgrade;

    private IProductForUpgrade _currentProductForUpgrade;
    
    private bool _isPointerDown;
    private float _pointerDownTime;
    private IDisposable _pointerHoldTimer;

    public bool AllowDrag = true;

    #region Methods

    public override void SetEmpty()
    {
        base.SetEmpty();
        _currentProductForUpgrade = null;

        CurrentStatus = DraggableUpgradeItemStatus.Empty;

        if (ParentTransform == null)
            transform.parent.gameObject.SetActive(false);
        else
            ParentTransform.gameObject.SetActive(false);
    }

    public void SetProduct(IProductForUpgrade currentProductForUpgrade)
    {
        _currentProductForUpgrade = currentProductForUpgrade;

        gameObject.SetActive(true);
        SetHighlightedBackground(false);

        if (ParentTransform == null)
            transform.parent.gameObject.SetActive(true);
        else
            ParentTransform.gameObject.SetActive(true);

        var currentProductsCount = jPlayerProfile.PlayerResources.GetProductCount(_currentProductForUpgrade.ProductID);
       
        if (_currentProductForUpgrade.IsLoaded)
        {
            CurrentStatus = DraggableUpgradeItemStatus.Loaded;
            ReadyMark.SetActive(true);
            _productCounterBack.gameObject.SetActive(false);
        }
        else
        {
            ReadyMark.SetActive(false);
            _productCounterBack.gameObject.SetActive(true);

            _productCounterText.text = string.Format("{0}/{1}",
                currentProductsCount,
                _currentProductForUpgrade.ProductsCount);


            var isProductsEnough = currentProductsCount >= _currentProductForUpgrade.ProductsCount;
            _productCounterBack.sprite = isProductsEnough ?
                _productsEnoughCounterSprite : _productsNotEnoughCounterSprite;

            CurrentStatus = isProductsEnough ? DraggableUpgradeItemStatus.ProductsEnought : DraggableUpgradeItemStatus.ProductsNotEnought;
        }

        SetItem(jResourcesManager.GetProductSprite(_currentProductForUpgrade.ProductID.Value), false);
        SetSpecialImage();
    }

    #endregion

    private void SetSpecialImage()
    {
        if (_specialOrderImage != null && _specialOrderIconHolder != null)
        {
            var product = jProductsData.GetProduct(_currentProductForUpgrade.ProductID);
            var specialIcon = jIconProvider.GetSpecialOrderIcon(product.SpecialOrder);
            _specialOrderImage.sprite = specialIcon;
            _specialOrderImage.enabled = specialIcon != null;
            _specialOrderIconHolder.SetActive(specialIcon != null);
        }        
    }

    private void SetHighlightedBackground(bool isHighlighted)
    {
        _backgroundHighlight.SetActive(isHighlighted);
    }

    #region Drag

    public override void OnPointerDown(PointerEventData eventData)
    {
        base.OnPointerDown(eventData);
        SetHighlightedBackground(true);
        _isPointerDown = true;
        _pointerDownTime = Time.time;
        _pointerHoldTimer = Observable.FromCoroutine(OnPointerHoldCheck).Subscribe();
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        base.OnPointerUp(eventData);
        SetHighlightedBackground(false);
        _isPointerDown = false;
        _pointerHoldTimer?.Dispose();
        _pointerDownTime = float.MinValue;
    }
    
    public override void OnBeginDrag(PointerEventData eventData)
    {
        if (!AllowDrag)
        {
            return;
        }
        
        if (IsProductsForbiddenToDrag())
        {
            return;
        }

        if (!IsDragNow)
        {
            return;
        }
        
        _pointerDownTime = float.MinValue;
        _pointerHoldTimer?.Dispose();
        OnHoldedCancelSignal?.Dispatch(this);

        SetHighlightedBackground(false);
        base.OnBeginDrag(eventData);        
    }
    
    public override void OnEndDrag(PointerEventData eventData)
    {
        if (IsProductsForbiddenToDrag())
            return;

        base.OnEndDrag(eventData);        
    }

    public override void OnDrag(PointerEventData eventData)
    {
        if (!AllowDrag)
        {
            return;
        }
        
        if (IsProductsForbiddenToDrag())
            return;

        base.OnDrag(eventData);
    }
    
    #endregion

    private IEnumerator OnPointerHoldCheck()
    {
        while (_isPointerDown)
        {
            float totalHoldTime = Time.time - _pointerDownTime;
            if(totalHoldTime < TooltipHoldTimeout)
            {
                yield return new WaitForEndOfFrame();
                continue;
            }
            
            OnHoldedSignal.Dispatch(this);
            _pointerHoldTimer?.Dispose();
            _pointerDownTime = float.MinValue;
            break;
        }
    }
    
    private bool IsProductsForbiddenToDrag()
    {
        if (CurrentStatus == DraggableUpgradeItemStatus.Empty)
            return true;

        if (CurrentStatus == DraggableUpgradeItemStatus.Loaded)
            return true;

        return false;
    }

}