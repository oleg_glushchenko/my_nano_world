﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Engine.UI.Views.Tooltips;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.GameLogic.Upgrade
{
    public class UpgradeWithProductsPanelView : BuildingPanelView
    {
        [SerializeField] private GameObject _rewardsLayout;
        [SerializeField] private TextMeshProUGUI _buildingNameText;
        [SerializeField] private TextMeshProUGUI _rewardCoveredBuildingsText;
        [SerializeField] private GameObject _lanternGO;
        [SerializeField] private GameObject _supplyInfoPanel;
        [SerializeField] private TextMeshProUGUI _lanternAmount;
        [SerializeField] private Text _upgradeProgressBarText;
        [SerializeField] private Image _icon;
        [SerializeField] private Image _dragReceiverImage;
        [SerializeField] private DragReceiver _dragReceiver;
        [SerializeField] private HorizontalProgressBar _upgradeProgressBar;
        [SerializeField] private UpgradeRewardIcons _upgradeRewardIcons;
        [SerializeField] private SpecialItemsTooltip _productGoToTooltip;
        [SerializeField] private List<DraggableUpgradeItem> _upgradeItems;
        
        public event Action<DraggableUpgradeItem> ItemReceived;

        public List<DraggableUpgradeItem> UpgradeItems => _upgradeItems;
        public DragReceiver DragReceiver => _dragReceiver;
        public SpecialItemsTooltip ProductGoToTooltip => _productGoToTooltip;
        public Image DragReceiverImage => _dragReceiverImage;
        public GameObject SupplyInfoPanel => _supplyInfoPanel;

        public string PanelTitle 
        {
            set => _buildingNameText.text = value;
        }
        
        public string NewPopulationCount 
        {
            set => _rewardCoveredBuildingsText.text = value;
        }

        public bool NeedToShowRewards
        {
            set => _rewardsLayout.SetActive(value);
        }

        public override void Show()
        {
            base.Show();
            
            _dragReceiver.jSignalOnDragObjectPutted.AddListener(ObjectReceived);
            _dragReceiver.jSignalOnDragObjectEnter.AddListener(OnDragProductEnter);
            _dragReceiver.jSignalOnGameObjectExit.AddListener(OnDragProductExit);
        }
        
        public override void Hide()
        {
            base.Hide();
            
            _dragReceiver.jSignalOnDragObjectPutted.RemoveListener(ObjectReceived);
            _dragReceiver.jSignalOnDragObjectEnter.RemoveListener(OnDragProductEnter);
            _dragReceiver.jSignalOnGameObjectExit.RemoveListener(OnDragProductExit);
        }

        public void SetProgressBarValue(float amount)
        {
            _upgradeProgressBar.FillAmount = amount;
            _upgradeProgressBarText.text = $"{Mathf.RoundToInt(amount * 100f)}%";
        }

        public void EnableLanternsCounter(bool enable)
        {
            _lanternGO.SetActive(enable);
        }

        public void SetLanternAmount(int amount)
        {
            _lanternAmount.text = amount.ToString();
        }
        
        public void SetIcon(Type type)
        {
            _icon.sprite = _upgradeRewardIcons.GetIcon(type);
        }

        private void OnDragProductExit(IDraggableItem item, DragReceiver receiver)
        {
            //Ignored
        }

        private void OnDragProductEnter(IDraggableItem item, DragReceiver receiver)
        {
            //Ignored
        }

        private void ObjectReceived(IDraggableItem receivedObject, DragReceiver sender)
        {
            var draggableUpgradeItem = receivedObject as DraggableUpgradeItem;
            if (draggableUpgradeItem != null)
                ItemReceived?.Invoke(draggableUpgradeItem);
        }
    }
}
