﻿using System;
using UnityEngine;
using Assets.NanoLib.Utilities.Pulls;
using TMPro;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;

public class FriendVisitItem : GameObjectPullable
{
    #region Inspector Fields

    [SerializeField]
    private Button _giftButton;

    [SerializeField]
    private GameObject _standartItemGameObject;

    [SerializeField]
    private GameObject _visitedItemGameObject;

    [SerializeField]
    private RawImage _avatarTextureStandart;

    [SerializeField]
    private RawImage _avatarTextureVisited;

    [SerializeField]
    private Text _userNameStandart;

    [SerializeField]
    private Text _userNameVisited;

    [SerializeField]
    private TextMeshProUGUI _userExp;

    [SerializeField]
    private Image _giftBg;

    [SerializeField]
    private Image _giftIcon;

    [SerializeField]
    private Material _grayMaterial;

    #endregion

    private Action<IUserProfile> _onProfileSelected;
    private Action<IUserProfile> _onProfileGift;

    private IUserProfile _cachedUser;

    public IUserProfile UserProfile
    {
        get { return _cachedUser; }
    }

    /// <summary>
    /// Обновляем данные по итему
    /// </summary>
    public void UpdateItem(IUserProfile user, Action<IUserProfile> onProfileSelected, Action<IUserProfile> onProfileGift, bool isGiftAvailable)
    {
        _standartItemGameObject.SetActive(true);
        _visitedItemGameObject.SetActive(false);

        SetGiftStatus(!isGiftAvailable);

        _avatarTextureStandart.texture = user.image;
        _userNameStandart.text = user.userName;
        _cachedUser = user;

        _onProfileSelected = onProfileSelected;
        _onProfileGift = onProfileGift;
    }

    /// <summary>
    /// Настраиваем итем игрока у которого в гостях
    /// </summary>
    public void SetVisitStatus(int friendLevel)
    {
        _visitedItemGameObject.SetActive(true);
        _standartItemGameObject.SetActive(false);

        _avatarTextureVisited.texture = _avatarTextureStandart.texture;
        _userNameVisited.text = _userNameStandart.text;

        _userExp.text = (friendLevel + 1).ToString();
    }

    /// <summary>
    /// Настраиваем кнопку "подарка" 
    /// </summary>
    private void SetGiftStatus(bool isGiftAvailable)
    {
        if(isGiftAvailable)
        {
            _giftBg.material = null;
            _giftIcon.material = null;

            _giftButton.interactable = true;
        }
        else
        {
            _giftBg.material = _grayMaterial;
            _giftIcon.material = _grayMaterial;

            _giftButton.interactable = false;
        }
    }

    /// <summary>
    /// Возвращает итем в пулл, чистим итем
    /// </summary>
    public override void OnPush()
    {
        base.OnPush();

        _avatarTextureStandart.texture = null;
        _avatarTextureVisited.texture = null;
        _cachedUser = null;
        _giftBg.material = null;
        _giftIcon.material = null;
    }

    /// <summary>
    /// Клик на "просмотр" города друга
    /// </summary>
    public void VisitFriendCity()
    {
        if(_onProfileSelected == null) return;

        _onProfileSelected(UserProfile);
    }

    /// <summary>
    /// Шлем подарок другу по клику
    /// </summary>
    public void OnGiftClick()
    {
        if(_onProfileGift != null)
        {
            _onProfileGift(UserProfile);
            _giftButton.interactable = false;
        }
    }
}