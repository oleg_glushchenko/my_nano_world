﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Класс обеспечивает работу индикатора текущей страницы
/// </summary>
public class FriendsPanelPagesIndicator : MonoBehaviour
{
    /// <summary>
    /// префаб индикатора страницы, для инстанциирования
    /// </summary>
    public GameObject PageButtonPrefab;

    /// <summary>
    /// Скроллинг
    /// </summary>
    public ScrollRect ScrollRect;

    /// <summary>
    /// Спрайт индикатора текущей страницы
    /// </summary>
    public Sprite ActivePageSprite;

    /// <summary>
    /// Спрайт индикатора неактивной страницы
    /// </summary>
    public Sprite InactivePageSprite;

    /// <summary>
    /// Список всех индикаторов страниц
    /// </summary>
    private readonly List<GameObject> _pageButtonsInstances = new List<GameObject>();

    /// <summary>
    /// Вызывается при старте работы панели
    /// </summary>
    // ReSharper disable once UnusedMember.Local
    private void Start()
    {
        // вешаем листнер
        ScrollRect.onValueChanged.AddListener(OnScrollValueChanged);

        // чистим\создаем кнопки
        RefreshButtons();
    }

    /// <summary>
    /// Обновление списка индикаторов страниц (вызывать после обновления элементов в скроллвью
    /// </summary>
    public void RefreshButtons()
    {
        // собираем все индикаторы, которые на данный момент существуют
        CollectCurrentPageButtons();

        // очищаем все индикаторы, если таковые имеются
        ClearAll();

        // создаем новые индикаторы
        CreatePageButtons();
    }

    /// <summary>
    /// Инстанциирование индикаторов страниц в соответствии с количеством элементов
    /// </summary>
    private void CreatePageButtons()
    {
        // количество индикаторов, которое необходимо создать. Т.к. в одном столбике 2 элемента - это количество делится на 2
        var countPagesToCreate = Mathf.RoundToInt(ScrollRect.content.childCount/2f);

        for (var i = 0; i < countPagesToCreate; i++)
        {
            var newPageButton = Instantiate(PageButtonPrefab);
            newPageButton.name = "PageItem " + (i + 1);
            newPageButton.transform.SetParent(gameObject.transform);
            newPageButton.transform.localScale = Vector3.one;
            _pageButtonsInstances.Add(newPageButton);
        }
    }

    /// <summary>
    /// Собирает в список все объекты, которые являются дочерными для текущего
    /// </summary>
    private void CollectCurrentPageButtons()
    {
        foreach (Transform childObject in transform)
            _pageButtonsInstances.Add(childObject.gameObject);
    }

    /// <summary>
    /// Очищает все инстансы индикаторов страниц, которые есть в списке.
    /// Если список ещё не заполнен, а элементы в чайлдах есть - необходимо
    /// сперва вызвать <see cref="CollectCurrentPageButtons"/>, а затем уже
    /// этот метод
    /// </summary>

    private void ClearAll()
    {
        foreach (var childObject in _pageButtonsInstances)
            Destroy(childObject);

        _pageButtonsInstances.Clear();
    }

    /// <summary>
    /// Отклик на событие изменения положения скролла
    /// </summary>
    /// <param name="value">положение скролла в двухмерном пространстве</param>
    private void OnScrollValueChanged(Vector2 value)
    {
        // если нет индикаторов страниц - нет смысла что либо считать
        if (_pageButtonsInstances.Count <= 0) return;

        // устанавливаем спрайт невыбранной страницы для всех индикаторов
        foreach (var pageButtonInstance in _pageButtonsInstances)
            pageButtonInstance.GetComponent<Image>().sprite = InactivePageSprite;

        // подсчёт необходимого для подсвечивания индикатора, на основе положения скролла и количества страниц
        var index =
            Mathf.RoundToInt(Mathf.Clamp(ScrollRect.horizontalNormalizedPosition, 0f, 1f)*_pageButtonsInstances.Count -
                             1);

        // если отскролить сильно влево - даже после клэмпа значение может быть отрицатильное (видимо клэмп не учитывает отрицательные числа)
        if (index == -1)
            index = 0;

        // установка спрайта активной страницы для необходимого индикатора
        _pageButtonsInstances[index].GetComponent<Image>().sprite = ActivePageSprite;
    }
}