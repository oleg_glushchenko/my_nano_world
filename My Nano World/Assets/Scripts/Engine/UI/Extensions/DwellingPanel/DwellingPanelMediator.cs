﻿using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using NanoLib.Core.Services.Sound;
using NanoReality.Engine.UI.Extensions;
using NanoReality.Game.Ui;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Upgrade;

public class DwellingPanelMediator : BuildingPanelMediator<IDwellingHouseMapObject, DwellingPanelView>
{
    protected override void Show(object param)
    {
        var building = param as IDwellingHouseMapObject;
        if (building?.Level < 1)
        {
            if (building.CanBeUpgraded)
            {
                ShowWindowSignal.Dispatch(typeof(UpgradeWithProductsPanelView), building);
            }
            else
            {
                jPopupManager.Show(new PopulationLimitPopupSettings());
            }
        }
        else
        {
            jSoundManager.Play(SfxSoundTypes.HospitalOpen, SoundChannel.SoundFX);
            base.Show(param);
        }
    }

    protected override void OnShown()
    {
        View.UpgradeButtonClick += OnUpgradeButtonClick;
        SetGrayscaleMode(true);
        base.OnShown();
    }

    protected override void OnHidden()
    {
        View.UpgradeButtonClick -= OnUpgradeButtonClick;
        SetGrayscaleMode(false);
        base.OnHidden();
    }
    
    private void OnUpgradeButtonClick(IMapBuilding building)
    {
        Hide();
        ShowWindowSignal.Dispatch(typeof(UpgradeWithProductsPanelView), building);
    }
}
