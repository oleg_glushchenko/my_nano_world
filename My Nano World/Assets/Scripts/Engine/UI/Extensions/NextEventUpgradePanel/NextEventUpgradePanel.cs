using Assets.NanoLib.UtilitesEngine;
using NanoLib.Services.InputService;
using TMPro;
using UnityEngine;

namespace Engine.UI.Extensions.NextEventUpgradePanel
{
    public class NextEventUpgradePanel : BuildingPanelView
    {
        [Inject] public SignalOnGroundTap jSignalOnGroundTap { get; set; }
        [Inject] public ITimerManager jTimerManager { get; set; }

        [SerializeField] private TextMeshProUGUI _time;

        private ITimer _timer;

        public override void Show()
        {
            base.Show();
            jGameCamera.FocusOnMapObject(TargetBuilding);
            IsAutoClosing = true;
        }

        public override void Hide()
        {
            jSignalOnGroundTap.RemoveListener(Hide);
            _timer?.CancelTimer();
            base.Hide();
        }

        public override void Initialize(object parameters)
        {
            base.Initialize(parameters);

            jSignalOnGroundTap.AddListener(Hide);
            IsAutoClosing = true;

            if (TargetBuilding is EventBuilding building)
            {
                _timer = jTimerManager.StartServerTimer(building.SecondsToNextEventDay, OnTimerEnds,
                    elapsed =>
                    {
                        _time.text = UiUtilities.GetFormattedTimeFromSecondsShorted((int) (building.SecondsToNextEventDay - elapsed));
                    });

                _time.text = UiUtilities.GetFormattedTimeFromSecondsShorted(((int)building.SecondsToNextEventDay));

                void OnTimerEnds()
                {
                    Hide();
                }
            }
        }
    }
}