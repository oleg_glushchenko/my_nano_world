﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Engine.UI.Extensions.SettingsPanel
{
    [Serializable]
    public class LanguageSpriteData
    {
        public string Language;
        public Sprite Sprite;
        public Sprite SubSprite;
    }
}
