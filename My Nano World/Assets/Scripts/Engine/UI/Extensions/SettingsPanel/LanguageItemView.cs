﻿using System.Collections.Generic;
using System.Globalization;
using Assets.NanoLib.Utilities.Pulls;
using NanoReality.GameLogic.Utilites;
using strange.extensions.signal.impl;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Engine.UI.Extensions.SettingsPanel
{
    [RequireComponent(typeof(LayoutElement))]
    public class LanguageItemView : GameObjectPullable
    {
        private Signal<string> _selectSignal = new Signal<string>();
        private string _langaugeId;

        [SerializeField] private Image _languageIcon;
        [SerializeField] private TextMeshProUGUI _languageName;
        [SerializeField] private Image _buttonImage;
        [SerializeField] private List<Sprite> _buttonsImage;

        private List<Sprite> _spriteStates;

        public Signal<string> SelectSignal
        {
            get { return _selectSignal; }
        }

        private LayoutElement _layoutElement;

        private bool _isSelected;        

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                ChangeState();
            }
        }

        private void ChangeState()
        {

            if (_spriteStates == null)
            {
                return;
            }

            _languageIcon.sprite = _isSelected ? _spriteStates[1] : _spriteStates[0];
            _buttonImage.sprite = _isSelected ? _buttonsImage[0] : _buttonsImage[1];
        }

        public void InitItem(string languageId, Sprite languageSprite, Sprite languageSubSprite)
        {
            _spriteStates = new List<Sprite>();
            _langaugeId = languageId;
            _spriteStates.Add(languageSprite);
            _spriteStates.Add(languageSubSprite);
            _languageIcon.sprite = languageSprite;
            _languageName.text = LocalizationUtils.GetLanguageText(languageId, languageId == "ar");
        }

        public void OnLanguageItemClick()
        {
            SelectSignal.Dispatch(_langaugeId);
        }        

        public override void OnPush()
        {
            IsSelected = false;
            SelectSignal.RemoveAllListeners();
            base.OnPush();
        }
    }
}
