﻿using strange.extensions.signal.impl;

namespace NanoReality.Engine.UI.Extensions.UISignals
{
    public class SignalOnNeedToRenameCity : Signal<string> { }

    public class OnCityNameChangedOnServerSignal : Signal { }
}
