using UnityEngine;

namespace Engine.UI
{
    [CreateAssetMenu(fileName = "CityCountersSprites", menuName = "NanoReality/UI/CityCountersSprites", order = 1)]
    public class CityCountersSprites : ScriptableObject
    {
        public Sprite PremiumCoinSprite;
        public Sprite SoftCoinSprite;
        public Sprite ExperienceSprite;
        public Sprite CitizenSprite;
        public Sprite GoldCurrencySprite;
    }
}