using UnityEngine;

namespace Engine.UI
{
    public class SpriteLoader
    {
        private ISpriteContainer _spriteContainer;

        private ISpriteContainer SpriteContainer
        {
            get
            {
                if (_spriteContainer != null) return _spriteContainer;
                lock (this)
                {
                    _spriteContainer = Resources.Load<SpriteContainer>("UI Data/SpriteContainer");
                }

                return _spriteContainer;
            }
        }

        public T GetSpriteData<T>() where T : Object
        {
            var data = SpriteContainer.GetContainer<T>();
            if (data != null) return data;
            var path = SpriteContainer.GetPath<T>();
            return Resources.Load<T>(path);

        }
    }
}