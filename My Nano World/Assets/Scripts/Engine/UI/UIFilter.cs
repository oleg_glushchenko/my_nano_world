using System;
using System.Collections.Generic;
using Assets.Scripts.Engine.UI.Views.Hud;
using NanoReality.Engine.UI.Extensions.EditModePanels;
using NanoReality.Engine.UI.Views.LevelUpPanel;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.Engine.UI.Views.TheEvent;
using NanoReality.Game.UI;
using strange.extensions.mediation.impl;

namespace Engine.UI
{
    public class UIFilter
    {
        public readonly Dictionary<Type, View> DefaultOnlyVisible = new Dictionary<Type, View>
        {
            {typeof(HudView), null},
            {typeof(BalancePanelView), null},
            {typeof(DialogPanelView), null},
            {typeof(EditorPanelView), null},
        };
        
        public readonly Dictionary<Type, View> StartupShow = new Dictionary<Type, View>
        {
            {typeof(HudView), null},
            {typeof(BalancePanelView), null}
        };
    }
}
