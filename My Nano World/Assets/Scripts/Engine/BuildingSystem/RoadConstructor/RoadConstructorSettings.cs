﻿using UnityEngine;

namespace NanoReality.Engine.BuildingSystem.MapObjects
{
    [CreateAssetMenu(menuName = "NanoReality/Settings/RoadConstructor")]
    public class RoadConstructorSettings : ScriptableObject
    {
        [Header("Snapping Options")] 
        [SerializeField] private bool _allowSnapping = false;

        [Tooltip("Error margin")]
        [SerializeField][Range(0f, 0.5f)] private float _snappingWeight = 0.3f;
        [SerializeField] private float _snappingInertiaCooldown = 1f;
        [SerializeField] private float _snappingMinRadius = 0.1f;
        [SerializeField] private float _snappingMaxValue = 10f;

        [Space]
        [Header("Cut the road - by length of the road tail")]
        [Tooltip("How long can the draft be until we want to create new brake point")]
        [SerializeField] private int _pathBendingByLength = 5;

        public bool AllowSnapping => _allowSnapping;
        public float SnappingWeight => _snappingWeight;
        public float SnappingMinRadius => _snappingMinRadius;
        public float SnappingMaxValue => _snappingMaxValue;
        public float SnappingInertiaCooldown => _snappingInertiaCooldown;
        public int PathBendingByLength => _pathBendingByLength;
    }
}