﻿using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.Utilites;
using UnityEngine;

namespace NanoLib.GeneratedTileMap
{
    /// <summary>
    /// Chunk of TileMap grid.
    /// </summary>
    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshRenderer))]
    public class Chunk : MonoBehaviour
    {
        public int DefaultTileX;
        public int DefaultTileY;

        private Mesh _chunkMesh;
        private Vector2[] _meshUvs;

        public Vector2 TilesInSpriteSheet { get; set; }
        
        private float TileSizeX
        {
            get { return 1.0f / TilesInSpriteSheet.x; }
        }

        private float TileSizeY
        {
            get { return 1.0f / TilesInSpriteSheet.y; }
        }

        /// <summary>
        /// Обновляет одну клетку сетки
        /// </summary>
        /// <param name="position"></param>
        /// <param name="tile"></param>
        /// <param name="gridWidth">кол-во клеток на чанк</param>
        public void UpdateGrid(Vector2 position, Vector2 tile, int gridWidth)
        {
            var i = (gridWidth * position.GetIntX() + position.GetIntY()) * 4;
            var tileX = tile.GetIntX();
            var tileY = tile.GetIntY();

            _meshUvs[i + 0] = new Vector2(tileX * TileSizeX, tileY * TileSizeY);
            _meshUvs[i + 1] = new Vector2((tileX + 1) * TileSizeX, tileY * TileSizeY);
            _meshUvs[i + 2] = new Vector2((tileX + 1) * TileSizeX, (tileY + 1) * TileSizeY);
            _meshUvs[i + 3] = new Vector2(tileX * TileSizeX, (tileY + 1) * TileSizeY);

            _chunkMesh.uv = _meshUvs;
        }

        /// <summary>
        /// Обновляет клетки в чанке
        /// </summary>
        /// <param name="points">список клеток для обновления</param>
        /// <param name="tileX">x координата клетки</param>
        /// <param name="tileY">y координата клетки</param>
        /// <param name="gridWidth">кол-во клеток на чанк</param>
        public void UpdateGrid(List<Vector2F> points, int tileX, int tileY, int gridWidth)
        {
            // HACK! NAN-5919 see grid texture, top right tile is fully transparent, this offset fixes neighbour pixel errors
            var offset = tileX == 1 && tileY == 1 ? 0.1f : 0f;

            for (var q = 0; q < points.Count; q++)
            {
                var i = (gridWidth * points[q].intX + points[q].intY) * 4;
                var x1 = tileX * TileSizeX + offset;
                var x2 = (tileX + 1) * TileSizeX - offset;

                var y1 = tileY * TileSizeY + offset;
                var y2 = (tileY + 1) * TileSizeY - offset;

                _meshUvs[i + 0] = new Vector2(x1, y1);
                _meshUvs[i + 1] = new Vector2(x2, y1);
                _meshUvs[i + 2] = new Vector2(x2, y2);
                _meshUvs[i + 3] = new Vector2(x1, y2);
            }

            _chunkMesh.uv = _meshUvs;
        }

        /// <summary>
        /// Обновляет клетки в чанке
        /// </summary>
        /// <param name="points">список клеток для обновления</param>
        /// <param name="tiles">uv координаты текстуры, для обновления</param>
        /// <param name="gridWidth">кол-во клеток на чанк</param>
        public void UpdateGrid(List<Vector2F> points, List<Vector2F> tiles, int gridWidth)
        {
            for (int q = 0; q < points.Count; q++)
            {
                var i = (gridWidth * points[q].intX + points[q].intY) * 4;
                _meshUvs[i + 0] = new Vector2(tiles[q].intX * TileSizeX, tiles[q].intY * TileSizeY);
                _meshUvs[i + 1] = new Vector2((tiles[q].intX + 1) * TileSizeX, tiles[q].intY * TileSizeY);
                _meshUvs[i + 2] = new Vector2((tiles[q].intX + 1) * TileSizeX, (tiles[q].intY + 1) * TileSizeY);
                _meshUvs[i + 3] = new Vector2(tiles[q].intX * TileSizeX, (tiles[q].intY + 1) * TileSizeY);
            }

            _chunkMesh.uv = _meshUvs;
        }

        /// <summary>
        /// Отмечает все клетки чанка как свободные
        /// </summary>
        public void ClearChank()
        {
            for (int i = 0; i < _meshUvs.Length; i += 4)
            {
                _meshUvs[i + 0] = new Vector2(0 * TileSizeX, 0 * TileSizeY);
                _meshUvs[i + 1] = new Vector2((0 + 1) * TileSizeX, 0 * TileSizeY);
                _meshUvs[i + 2] = new Vector2((0 + 1) * TileSizeX, (0 + 1) * TileSizeY);
                _meshUvs[i + 3] = new Vector2(0 * TileSizeX, (0 + 1) * TileSizeY);
            }

            _chunkMesh.uv = _meshUvs;
        }

        public void CreatePlane(float tileHeight, float tileWidth, int gridHeight, int gridWidth, Material material)
        {
            _chunkMesh = new Mesh();
            var mf = GetComponent<MeshFilter>();
            mf.GetComponent<Renderer>().material = material;
            mf.mesh = _chunkMesh;

            var vertices = new List<Vector3>();
            var triangles = new List<int>();
            var normals = new List<Vector3>();
            var uvs = new List<Vector2>();

            var index = 0;
            for (var x = 0; x < gridWidth; x++)
            {
                for (var y = 0; y < gridHeight; y++)
                {
                    AddVertices(tileHeight, tileWidth, y, x, vertices);
                    index = AddTriangles(index, triangles);
                    AddNormals(normals);
                    AddUvs(DefaultTileX, TileSizeY, TileSizeX, uvs, DefaultTileY);
                }
            }

            _meshUvs = uvs.ToArray();
            _chunkMesh.vertices = vertices.ToArray();
            _chunkMesh.normals = normals.ToArray();
            _chunkMesh.triangles = triangles.ToArray();
            _chunkMesh.uv = _meshUvs;
            _chunkMesh.RecalculateNormals();
        }

        private static void AddVertices(float tileHeight, float tileWidth, int y, int x, ICollection<Vector3> vertices)
        {
            var i = x * tileWidth;
            var i1 = y * tileHeight;
            vertices.Add(new Vector3(i, 0, i1));
            vertices.Add(new Vector3(i + tileWidth, 0, i1));
            vertices.Add(new Vector3(i + tileWidth, 0, i1 + tileHeight));
            vertices.Add(new Vector3(i, 0, i1 + tileHeight));
        }

        private static int AddTriangles(int index, ICollection<int> triangles)
        {
            triangles.Add(index + 2);
            triangles.Add(index + 1);
            triangles.Add(index);
            triangles.Add(index);
            triangles.Add(index + 3);
            triangles.Add(index + 2);
            index += 4;
            return index;
        }

        private static void AddNormals(ICollection<Vector3> normals)
        {
            normals.Add(Vector3.forward);
            normals.Add(Vector3.forward);
            normals.Add(Vector3.forward);
            normals.Add(Vector3.forward);
        }

        private static void AddUvs(int tileRow, float tileSizeY, float tileSizeX, ICollection<Vector2> uvs, int tileColumn)
        {
            if (tileRow != -1 && tileColumn != -1)
            {
                uvs.Add(new Vector2(tileColumn * tileSizeX, tileRow * tileSizeY));
                uvs.Add(new Vector2((tileColumn + 1) * tileSizeX, tileRow * tileSizeY));
                uvs.Add(new Vector2((tileColumn + 1) * tileSizeX, (tileRow + 1) * tileSizeY));
                uvs.Add(new Vector2(tileColumn * tileSizeX, (tileRow + 1) * tileSizeY));
            }
            else
            {
                uvs.Add(new Vector2(0.9f, 0.9f));
                uvs.Add(new Vector2(0.9f, 0.9f));
                uvs.Add(new Vector2(0.9f, 0.9f));
                uvs.Add(new Vector2(0.9f, 0.9f));
            }
        }
    }
}