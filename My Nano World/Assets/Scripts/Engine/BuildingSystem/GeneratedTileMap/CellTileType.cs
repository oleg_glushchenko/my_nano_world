﻿namespace NanoLib.GeneratedTileMap
{
    /// <summary>
    /// Defines state of tile cell in grid.
    /// </summary>
    public enum CellTileType
    {
        /// <summary>
        /// Клетка свободна, и не является местом для шахт
        /// </summary>
        Free,

        /// <summary>
        /// Клетка закрыта
        /// </summary>
        Closed,

        /// <summary>
        /// Клетка свободна и является местом для шахт
        /// </summary>
        MinePlace
    }
}