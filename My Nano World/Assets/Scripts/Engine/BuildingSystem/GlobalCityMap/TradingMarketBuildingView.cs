﻿using Assets.NanoLib.UI.Core.Signals;
using Assets.Scripts.Engine.UI.Views.Trade;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using Assets.Scripts.Engine.UI.Views.Trade.Items;

namespace NanoReality.Engine.BuildingSystem.GlobalCityMap
{
    public class TradingMarketBuildingView : AGlobalCityBuildingView
    {
        [Inject] public RoadConstructSetActiveSignal jRoadConstructSetActiveSignal { get; set; }

        [Inject] public PlayerOpenedPanelSignal JSignalPlayerOpenedPanel { get; set; }

        protected override void OnBuildingTap()
        {
            jRoadConstructSetActiveSignal.Dispatch(false);
            JSignalPlayerOpenedPanel.Dispatch();
        }
    }
}