using System.Collections.Generic;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using UnityEngine;

namespace NanoReality.Engine.BuildingSystem.GlobalCityMap
{
    public abstract class AGlobalCityBuildingView : AMapBuildingView
    {
        [SerializeField] private bool _isGlobal = true;
        [SerializeField] private bool _hasGraphics;

        protected bool IsGlobal
        {
            get => _isGlobal;
            set => _isGlobal = value;
        }

        #region Overrides of MapObjectView

        public bool HasGraphics => _hasGraphics;
        public override bool IsMovable => !IsGlobal;
        public override bool KeepConstructAnimation => MapBuildingModel != null && MapBuildingModel.IsLocked;

        protected override List<string> GetComponentsNames()
        {
            return new List<string>
            {
                "StaticAnimationsComponent",
                "ConstractAnimationsComponent",
                "ObjectSemiTransparentComponent",
                "ProgressBarComponent",
                "VFXComponent",
                "BuildingAttentionComponent",
                "ColorHighlightComponent",
            };
        }

        public override void InitializeView(IMapObject model)
        {
            if (model == null)
            {
                gameObject.SetActive(false);
                return;
            }

            base.InitializeView(model);

            ObjectTypeId = model.ObjectTypeID;
            ModelLevel = model.Level;
            GridPosition = new Vector2Int(model.MapPosition.intX, model.MapPosition.intY);

            if (KeepConstructAnimation)
            {
                ConstructionComponent.ShowConstractionAnimation();
            }

            ShowIndicators();
        }

        #endregion

        public void OnMapObjectBuildingRepaired()
        {
            if (KeepConstructAnimation || !MapBuildingModel.IsConstructed)
            {
                return;
            }
            
            VFXComponent.ShowUpgradeFxEffect();
            ConstructionComponent.HideConstractionAnimation();
        }

        protected override void OnUpgradeAttentionTap() => OnMapObjectViewTap();
    }
}