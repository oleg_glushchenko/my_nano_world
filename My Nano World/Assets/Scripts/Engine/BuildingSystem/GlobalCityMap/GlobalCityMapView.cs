﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NanoLib.Services.InputService;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.Analytics.Model.api;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.UI;
using NanoReality.Engine.UI.Extensions.EditModePanels;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.GlobalCityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.GlobalCityMap.Models;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.StrangeIoC;
using UniRx;
using UnityEngine;
using NanoReality.Game.Attentions;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Services;
using NanoReality.Isometry;

namespace NanoReality.Engine.BuildingSystem.GlobalCityMap
{
    public class GlobalCityMapView : AViewMediator
    {
        /// <summary>
        /// Helper class for configure position of global object on the map.
        /// Workaround decision while server not sending position of this buildings.
        /// </summary>
        [Serializable]
        private sealed class GlobalObjectInitData
        {
            public int buildingId;
            public Transform worldPosition;
        }

        [SerializeField]
        private List<GlobalObjectInitData> _globalObjectsInitDatas;

        private List<AGlobalCityBuildingView> _globalCityViews;

        private Id<IBusinessSector> _businessSectorId;
        private IGlobalCityMap Model { get; set; }

        private readonly Quaternion GlobalObjectsRotation = Quaternion.Euler(30, 45, 0);
        private IDisposable _initializeCityMapCoroutine;
        public float CurrentMaxDepth;
        public List<AGlobalCityBuildingView> Objects => _globalCityViews;
        #region Injects

        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public IMapObjectsGenerator jMapObjectsGenerator { get; set; }
        [Inject] public IConstructionController jConstructionController { private get; set; }

        [Inject] public SignalOnInitGlobalCityMap jSignalOnInitGlobalCityMap { get; set; }
        [Inject] public SignalOnMapObjectViewTap jSignalOnMapObjectViewTap { get; set; }
        [Inject] public SignalOnMapObjectUpgradedOnMap jSignalOnMapObjectUpgradedOnMap { get; set; }
        [Inject] public SignalOnMapObjectBuildFinished jSignalOnMapObjectBuildFinished { get; set; }
        [Inject] public SignalOnBuildingRepaired jSignalOnBuildingRepaired { get; set; }
        [Inject] public CityMapViewInitializedSignal jCityMapViewInitializedSignal { get; set; }
        [Inject] public IPlayerExperience jPlayerExperience { get; set; }
        [Inject] public IBuildingService jBuildingService { get; set; }
        [Inject] public UpdateWorldSpaceDepth jUpdateWorldSpaceDepth { get; set; }
        [Inject] public INanoAnalytics jNanoAnalytics { get; set; }
        [Inject] public IEditModeService jEditModeService { get; set; }

        #endregion

        #region Overrides of AViewMediator

        protected override void PreRegister()
        {
            _businessSectorId = BusinessSectorsTypes.Global.GetBusinessSectorId();
            _globalCityViews = new List<AGlobalCityBuildingView>();
        }

        protected override void OnRegister()
        {
            jGameManager.RegisterGlobalCityMapView(this);

            jSignalOnInitGlobalCityMap.AddListener(OnInitGlobalCityMap);
            jSignalOnMapObjectViewTap.AddListener(OnMapObjectViewTap);
            jSignalOnMapObjectUpgradedOnMap.AddListener(OnMapObjectUpgradedOnMap);
            jSignalOnBuildingRepaired.AddListener(OnBuildingRepaired);
        }

        protected override void OnRemove()
        {
            jSignalOnInitGlobalCityMap.RemoveListener(OnInitGlobalCityMap);
            jSignalOnMapObjectViewTap.RemoveListener(OnMapObjectViewTap);
            jSignalOnMapObjectUpgradedOnMap.RemoveListener(OnMapObjectUpgradedOnMap);
            jSignalOnBuildingRepaired.RemoveListener(OnBuildingRepaired);
        }

        #endregion

        private void OnInitGlobalCityMap(IGlobalCityMap map)
        {
            Model = map;

            _initializeCityMapCoroutine = Observable.FromCoroutine(InitializeCityMapAsync).Subscribe();
        }

        private IEnumerator InitializeCityMapAsync()
        {
            foreach (IMapObject mapObject in Model.MapObjects)
            {
                int id = mapObject.ObjectTypeID;
                GlobalObjectInitData globalObjectInitData =_globalObjectsInitDatas.FirstOrDefault(c => c.buildingId == id);

                if (globalObjectInitData == null)
                {
                    continue;
                }
                
                var view = jMapObjectsGenerator.GetMapObjectView(mapObject.Id);
                yield return new WaitForSeconds(0.05f);
                if (view != null)
                {
                    Transform viewTransform = view.transform;
                    viewTransform.SetParent(transform);

                    view.WorldSpacePosition = new Vector2(globalObjectInitData.worldPosition.position.x, globalObjectInitData.worldPosition.position.z); 
                    viewTransform.rotation = GlobalObjectsRotation;
                    view.InitializeView(mapObject);
                    _globalCityViews.Add(view as AGlobalCityBuildingView);
                }
                else
                {
                    Debug.LogError($"Can't find map object view. MapObject: {mapObject.Id}");
                }
            }

            jCityMapViewInitializedSignal.Dispatch(BusinessSectorsTypes.Global);

            SortDepthObjects();
        }

        public void SortDepthObjects()
        {
            CurrentMaxDepth = IsometryUtils.SortObjects(_globalCityViews);
            jUpdateWorldSpaceDepth.Dispatch();
        }


        private void OnMapObjectViewTap(MapObjectView view)
        {
            if (jConstructionController.IsBuildModeEnabled || 
                view == null || view.MapObjectModel.SectorId != _businessSectorId)
            {
                return;
            }

            if (jEditModeService.IsEditModeEnable && !(view.MapObjectModel is IAoeBuilding))
            {
                return;
            }

            view.OnMapObjectViewTap();
        }

        private void OnMapObjectUpgradedOnMap(IMapObject mapObject)
        {
            if (mapObject.SectorId != _businessSectorId)
            {
                return;
            }

            UpgradeMapObject(mapObject);
        }

        private void UpgradeMapObject(IMapObject mapObjectModel)
        {
            AGlobalCityBuildingView viewBeforeUpgrade = GetView(mapObjectModel.ObjectTypeID);
            if (viewBeforeUpgrade == null)
            {
                return;
            }
            
            Vector2 position = viewBeforeUpgrade.WorldSpacePosition;
            
            var newViewInstance = (AGlobalCityBuildingView) jMapObjectsGenerator.GetMapObjectView(mapObjectModel.Id);
            newViewInstance.Depth = viewBeforeUpgrade.Depth;

            _globalCityViews.Remove(viewBeforeUpgrade);
            viewBeforeUpgrade.DisposeView();

            AddMapObjectView(newViewInstance, mapObjectModel, position);
            
            if (newViewInstance.IgnoreDepthSort)
            {
                newViewInstance.SortObject();
            }
            else
            {
                SortDepthObjects();
            }
        }
        private void AddMapObjectView(AGlobalCityBuildingView mapObjectView, IMapObject mapObject, Vector2 position)
        {
            mapObjectView.InitializeView(mapObject);
            mapObjectView.Transform.SetParent(transform);
            mapObjectView.SetWorldSpacePosition(position);
            
            _globalCityViews.Add(mapObjectView);
        }
        
        private void OnBuildingRepaired(Id_IMapObjectType typeId, Id_IMapObject mapID)
        {
            AGlobalCityBuildingView view = GetView(typeId);

            int experienceReward = jBuildingService.GetBuildingConstructionReward(view.MapId);
            jPlayerExperience.AddExperience(experienceReward, true);
            jNanoAnalytics.OnExperienceUpdate(AwardSourcePlace.BuildingRepaired, view.MapId.Value.ToString(), 
                experienceReward);
            jSignalOnMapObjectBuildFinished.Dispatch(view.MapBuildingModel);
            view.OnMapObjectBuildingRepaired();
        }

        public AGlobalCityBuildingView GetView(Id_IMapObject mapId)
        {
            return _globalCityViews.FirstOrDefault(t => t.MapId == mapId);
        }

        public AGlobalCityBuildingView GetView(Id_IMapObjectType typeId)
        {
            return _globalCityViews.FirstOrDefault(t => t.ObjectTypeId == typeId);
        }

        public void Clear()
        {
            foreach (var view in _globalCityViews)
            {
                view.FreeObject();
            }

            _initializeCityMapCoroutine?.Dispose();
            _globalCityViews.Clear();
        }
    }
}
