﻿using System;
using Assets.Scripts.Engine.UI.Views.Hud;
using NanoReality.Game.Attentions;
using strange.extensions.mediation.impl;
using UniRx;
using UnityEngine;

namespace NanoReality.Engine.BuildingSystem.CityCamera
{
    public class CameraEffects : View
    {
        /// <summary>
        /// Grayscale effect shader.
        /// </summary>
        [SerializeField] private Shader _grayscalse;

        [SerializeField] private Camera _camera;

        [SerializeField] private float _screenSpaceXscale;
        [SerializeField] private float _screenSpaceYscale;
        [SerializeField] private float _vignetteMax;
        [SerializeField] private float _vignetteMin;
        [SerializeField] private float _vignettePow;

        [Inject] public OnHudBottomPanelIsVisibleSignal jOnHudBottomPanelIsVisibleSignal { get; set; }
        [Inject] public OnHudButtonsIsClickableSignal jOnHudButtonsIsClickableSignal { get; set; }
        [Inject] public IGameCameraModel jGameCameraModel { get; set; }
        [Inject] public SwitchBuildingAttentionsSignal jSwitchBuildingAttentionsSignal { get; set; }
        
        private IDisposable _grayscaleEffectDisposable;

        [PostConstruct]
        public void PostConstruct()
        {
            _grayscaleEffectDisposable = jGameCameraModel.GrayscaleActiveProperty.Subscribe(OnSetActiveCameraGrayscaleEffect);
            SetVignetteParams();
        }

        [Deconstruct]
        public void OnDeconstruct()
        {
            _grayscaleEffectDisposable.Dispose();
        }

        private void OnSetActiveCameraGrayscaleEffect(bool isActive)
        {
            if (isActive)
            {
                _camera.SetReplacementShader(_grayscalse, "RenderType");
            }
            else
            {
                _camera.ResetReplacementShader();
            }

            jSwitchBuildingAttentionsSignal.Dispatch(!isActive);
            jOnHudBottomPanelIsVisibleSignal.Dispatch(!isActive);
            jOnHudButtonsIsClickableSignal.Dispatch(!isActive);
        }

        void SetVignetteParams()
        {
            Shader.SetGlobalFloat("_ScreenSpaceXscale", _screenSpaceXscale);
            Shader.SetGlobalFloat("_ScreenSpaceYscale", _screenSpaceYscale);
            Shader.SetGlobalFloat("_VignetteMax", _vignetteMax);
            Shader.SetGlobalFloat("_VignetteMin", _vignetteMin);
            Shader.SetGlobalFloat("_VignettePow", _vignettePow);
        }

#if UNITY_EDITOR
        private void OnValidate()
        {
            SetVignetteParams();
        }
#endif
    }
}
