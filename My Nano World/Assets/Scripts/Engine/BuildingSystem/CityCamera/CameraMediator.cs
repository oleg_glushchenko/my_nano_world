﻿using System;
using NanoLib.Services.InputService;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.Utilities;
using Assets.NanoLib.Utilities.Serializers;
using NanoReality.GameLogic.BuildingSystem.CityCamera;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Controllers;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.Configs;
using Newtonsoft.Json;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace NanoReality.Engine.BuildingSystem.CityCamera
{
    public class CameraMediator : Mediator
    {
        [Inject] public CameraView View { get; set; }
        [Inject] public IGameCamera Camera { get; set; }
        [Inject] public GameCameraSettings jGameCameraSettings { get; set; }

        /// <summary>
        /// Wait for map initialized to set camera in default position
        /// </summary>
        [Inject] public StartCityMapInitializationSignal JStartCityMapInitializationSignal { get; set; }

        [Inject] public ZoomCameraSignal jSignalOnZoomCamera { get; set; }


        [Inject] public SignalOnCameraSwiped jSignalOnSwipeCamera { get; set; }
        [Inject] public InertiaCameraSignal jSignalOnInertiaCamera { get; set; }

        [Inject] public SignalOnManualSwipe jSignalOnManualSwipe { get; set; }

        [Inject] public SignalOnManualZoom jSignalOnManualZoom { get; set; }

        /// <summary>
        /// Move camera on some coords
        /// </summary>
        [Inject] public SignalOnAutoSwipeCamera jSignalOnAutoSwipeCamera { get; set; }

        /// <summary>
        /// Signal to zoom camera
        /// </summary>
        [Inject] public StartCameraAutoZoomSignal jStartCameraAutoZoomSignal { get; set; }

        [Inject] public SignalOnFireworksStart jSignalOnFireworksStart { get; set; }

        [Inject] public SignalEnableCamera jSignalEnableCamera { get; set; }

        [Inject] public SignalDisableCamera jSignalDisableCamera { get; set; }
        [Inject] public GetScreenshotSignal jGetScreenshotSignal { get; set; }  

        /// <summary>
        /// methods initiates immediately after injection.
        /// make links between View and Model (apply data from View to Model)
        ///  </summary>
        public override void OnRegister()
        {
            base.OnRegister();

            InitializeCamera();
            
            jSignalOnZoomCamera.AddListener(OnZoom);                //sign up for zoom event
            jSignalOnSwipeCamera.AddListener(OnSwipe);              //sign up for swipe event   
			jSignalOnInertiaCamera.AddListener(OnInertia);              //sign up for inertia event   
            jSignalOnAutoSwipeCamera.AddListener(OnAutoCameraSwipe);// sign up for auto move event
            jStartCameraAutoZoomSignal.AddListener(OnStartCameraAutoZoom);        // sign up for auto zoom event
            jSignalOnManualSwipe.AddListener(OnManualSwipe);
            jSignalOnManualZoom.AddListener(OnManualZoom);
            JStartCityMapInitializationSignal.AddListener(AlignToMap);
            jSignalOnFireworksStart.AddListener(StartFireworks);
            jSignalEnableCamera.AddListener(() => View.EnableCamera(true));
            jSignalDisableCamera.AddListener(() => View.EnableCamera(false));
            jGetScreenshotSignal.AddListener(GetScreenshot);
        }

        private void StartFireworks()
        {
            foreach (ParticleSystem fireWork in View.FireworksEffectsList)
            {
                fireWork.gameObject.SetActive(true);
                fireWork.Play(); 
            }
        }
        
        private void InitializeCamera()
        {
            Transform viewTransform = View.transform;
            var position = viewTransform.position.ToVector3F();
            var angleX = viewTransform.eulerAngles.x;
            
            Camera.Init(View.CameraBounds, position, angleX, View.Camera);
            View.OnPinch(jGameCameraSettings.DefaultZoom);
        }

        private void AlignToMap(ICityMap map)
        {
            Camera.AlignCamera(map, CameraAlignPosition.Center, CameraSwipeMode.Jump);
        }
       
        /// <summary>
        /// send swipe position to view at event
        ///  </summary>
        private void OnSwipe(Vector3F position)
        {
            View.StopAutoSwipe();
            View.OnSwipe(position.ToVector3());
        }

		private void OnInertia(Vector3F position)
		{
		    View.StopAutoSwipe();
			View.OnSwipe(position.ToVector3());
		}
        // send auto swipe to view at event
		private void OnAutoCameraSwipe(Vector3F position, CameraSwipeMode mode, float speed = -1, bool hardChangeDistance = false)
        {
			View.OnAutoSwipe(position.ToVector3(), mode, speed, hardChangeDistance);
        }        

        /// <summary>
        /// send zoom data into view at event
        ///  </summary>
        private void OnZoom(float currentZoom)
        {
            View.OnPinch(currentZoom);
        }

		private void OnStartCameraAutoZoom(float currentZoom, float speed = -1, bool hardChangeDistance = false)
        {
			View.OnAutoZoom(currentZoom, speed);
        }
        private void OnManualSwipe()
        {
            View.StopAutoSwipe();
        }

        private void OnManualZoom()
        {
            View.StopAutoSwipe();
            View.StopAutoZoom();
        }
        
        private void GetScreenshot(Action<string> callback)
        {
            var camera = View.ScreenshotCamera;
            camera.gameObject.SetActive(true);
            int width = camera.pixelWidth;
            int height = camera.pixelHeight;
            Texture2D texture = new Texture2D(width, height);
            RenderTexture targetTexture = RenderTexture.GetTemporary(width, height);
 
            camera.targetTexture = targetTexture;
            camera.Render();

            RenderTexture.active = targetTexture;
 
            Rect rect = new Rect(0, 0, width, height);
            texture.ReadPixels(rect, 0, 0);
            texture.Apply();

            byte[] bytesJpg;
            bytesJpg = texture.EncodeToJPG();
            string coords = JsonConvert.SerializeObject(bytesJpg);
          
            callback.Invoke(coords);
            camera.gameObject.SetActive(false);
        }
    }
}