﻿using System;
using UnityEngine;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using NanoReality.GameLogic.BuildingSystem.CityCamera;
using NanoReality.StrangeIoC;
using strange.extensions.signal.impl;
using UnityEngine.UI;

// ReSharper disable CompareOfFloatsByEqualityOperator

namespace NanoReality.Engine.BuildingSystem.CityCamera
{

    /// <summary>
    /// Игнорирует следующий фокус на центр города при смене сектора
    /// </summary>
    public class SignalCameraIgnoreStartZoomOnce : Signal {}

    /// <summary>
    /// Raised every frame while auto swiping is happening
    /// </summary>
    public class SignalOnCameraAutoSwipe : Signal { }

    /// <summary>
    /// Raised, after auto swipe ended
    /// </summary>
    public class SignalOnCameraAutoSwipeEnded : Signal { }

    /// <summary>
    /// Raised every frame while auto zooming is happening
    /// </summary>
    public class OnCameraAutoZoomedSignal : Signal { }

    /// <summary>
    /// Raised, after auto zoom ended
    /// </summary>
    public class SignalOnCameraAutoZoomEnded : Signal { }
    public class GetScreenshotSignal : Signal<Action<string>> { }

    public class CameraView : AViewMediator
    {
        [Inject] public IGameCameraModel jGameCameraModel { get; set; }
        [Inject] public SignalCameraIgnoreStartZoomOnce jSignalCameraIgnoreStartZoomOnce { get; set; }
        [Inject] public SignalOnCameraAutoSwipe jSignalOnCameraAutoSwipe { get; set; }
        [Inject] public SignalOnCameraAutoSwipeEnded jSignalOnCameraAutoSwipeEnded { get; set; }
        [Inject] public OnCameraAutoZoomedSignal jOnCameraAutoZoomedSignal { get; set; }
        [Inject] public SignalOnCameraAutoZoomEnded jSignalOnCameraAutoZoomEnded { get; set; }
        [Inject] public GameCameraSettings jGameCameraSettings { get; set; }
        
        public CameraBounds CameraBounds;
        public List<ParticleSystem> FireworksEffectsList;

        private float _customAutoZoomSpeed = -1;
        private Vector3 _cameraDestanation;         // auto swipe destanation coords
        private Camera _camera;   
        private bool _lockPinch;
        private float _cachedOrtographicSize;
        private float _customAutoSwipeSpeed = -1;
        private Tween _currentZoomRoutine;
        private Coroutine _currentSwipeRoutine;
        [SerializeField] private Camera _screenshotCamera;

        public Camera Camera => _camera;
        public bool AutoZoom { get; set; }
        public Camera ScreenshotCamera => _screenshotCamera;

        protected override void PreRegister()
        {
            _camera = GetComponent<Camera>();
        }

        protected override void OnRegister()
        {
            jSignalCameraIgnoreStartZoomOnce.AddListener(OnIngoreStartZoom);
        }

        protected override void OnRemove()
        {
            jSignalCameraIgnoreStartZoomOnce.RemoveListener(OnIngoreStartZoom);
            
            StopAutoSwipe();
            StopAutoZoom();
        }

        private bool _igonreStartZoomOnce;

        private void OnIngoreStartZoom()
        {
            _igonreStartZoomOnce = true;
        }

        /// <summary>
        /// apply swipe changes for camera(new position for camera during swipe)
        ///  </summary>
        public void OnSwipe(Vector3 position)
        {
            jGameCameraModel.PositionProperty.Value = position.ToVector3F();
            transform.position = position;
            CheckPositionThresholds();

            if (AutoZoom)
            {
                if (!_lockPinch)
                {
                    _cachedOrtographicSize = _camera.orthographicSize;
                    _lockPinch = true;
                }
                _camera.orthographicSize -= 0.01f;
                OnSwipe(position);
            }
        }

        /// <summary>
        /// apply zoom changes for camera
        ///  </summary>
        public void OnPinch(float cameraDistance)
        {
            if (_lockPinch)
            {
                Debug.LogError("Cant do OnPitch. Pitch is locked.");
                return;
            }
            _camera.orthographicSize = cameraDistance;
            CheckPositionThresholds();
        }

        public void OnAutoZoom(float currentZoom, float speed = -1)
        {
            if (currentZoom != _cachedOrtographicSize)
            {
                _cachedOrtographicSize = _camera.orthographicSize;
            }
            StopAutoZoom();
            _customAutoZoomSpeed = speed;
           
            float duration = Mathf.Abs(_camera.orthographicSize - currentZoom) / _customAutoZoomSpeed;

            _currentZoomRoutine = _camera.DOOrthoSize(currentZoom, duration)
                .OnStepComplete(jOnCameraAutoZoomedSignal.Dispatch)
                .OnComplete(()=>
                {
                    jSignalOnCameraAutoZoomEnded.Dispatch();
                    CheckPositionThresholds();
                });
        }
        
        private void CheckPositionThresholds()
        {
            var offset = GetGroundOffset();
            var position = new Vector3(transform.position.x + offset, 0, transform.position.z + offset);
            var clamped = CameraBounds.ClampByBorders(position, _camera.orthographicSize);
            transform.position = new Vector3(clamped.x - offset, transform.position.y, clamped.z - offset);
        }

        private float GetGroundOffset()
        {
            var length = transform.position.y / Mathf.Tan((transform.eulerAngles.x) * Mathf.Deg2Rad);
            var xzOffset = (length / Mathf.Cos(45 * Mathf.Deg2Rad)) / 2;
            return xzOffset;
        }

        /// <summary>
        /// Auto swipe camera to some position
        /// </summary>
        /// <param name="position">Destanation coords</param>
        /// <param name="mode">Camera swipe mode</param>
        /// <param name="speed"></param>
        /// <param name="hardChangeDestanation"></param>
        public void OnAutoSwipe(Vector3 position, CameraSwipeMode mode, float speed = -1, bool hardChangeDestanation = false)
        {
            if (_cameraDestanation != Vector3.zero && !hardChangeDestanation) return;

            if(mode == CameraSwipeMode.Jump)
            {
                OnSwipe(position);
                jSignalOnCameraAutoSwipeEnded.Dispatch();
                return;
            }

            _customAutoSwipeSpeed = speed;
            _cameraDestanation = position;
            if (_currentSwipeRoutine != null)
            {
                StopCoroutine(_currentSwipeRoutine);
            }
            _currentSwipeRoutine = StartCoroutine(SwipeCamera());
        }

        public void StopAutoSwipe()
        {
            if (_currentSwipeRoutine != null)
            {
                StopCoroutine(_currentSwipeRoutine);
                _currentSwipeRoutine = null;
                jGameCameraModel.PositionProperty.Value = transform.position.ToVector3F();
                _cameraDestanation = Vector3.zero;
            }

            if (_lockPinch)
            {
                _lockPinch = false;
                AutoZoom = false;
                OnAutoZoom(_cachedOrtographicSize);
            }
        }

        public void StopAutoZoom()
        {
            _currentZoomRoutine?.Kill();
        }

        // used for auto move camera into some position
        private IEnumerator SwipeCamera()
        {
            var distance = Vector3.Distance(transform.position , _cameraDestanation);
            while (distance > 0.001f )
            {
                var x = transform.position.x;
                var z = transform.position.z;

                var speed = Time.deltaTime * (_customAutoSwipeSpeed < 0 ? jGameCameraSettings.AutoSwipeSpeed : _customAutoSwipeSpeed);

                if(x != _cameraDestanation.x)
                {
                    x = Mathf.Lerp(x, _cameraDestanation.x, speed);
                }

                if (z != _cameraDestanation.z)
                {
                    z = Mathf.Lerp(z, _cameraDestanation.z, speed);
                }

                OnSwipe(new Vector3(x, transform.position.y, z));
                distance = Vector3.Distance(transform.position, _cameraDestanation);

                jSignalOnCameraAutoSwipe.Dispatch();
                yield return null;
            }
            _currentSwipeRoutine = null;
            OnSwipe(_cameraDestanation);
            _cameraDestanation = Vector3.zero;
            jSignalOnCameraAutoSwipeEnded.Dispatch();
        }

        public void EnableCamera(bool enable)
        {
            _camera.enabled = enable;
        }
    }
}
