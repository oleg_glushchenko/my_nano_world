﻿using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.Utilities;
using GameLogic.BuildingSystem.CityArea;
using NanoLib.Core.Logging;
using NanoLib.GeneratedTileMap;
using NanoReality.Engine.UI.Extensions.EditModePanels;
using NanoReality.GameLogic.BuildingSystem;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.Services;
using NanoReality.GameLogic.Settings.BuildSettings.Models.Api;
using NanoReality.GameLogic.StartApplicationSequence.Controllers.Signals;
using strange.extensions.mediation.impl;
using UniRx.Triggers;
using UnityEngine;

namespace NanoReality.Engine.BuildingSystem.Grid
{
    public sealed class GridDrawer : View
    {
        #region Injects

        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public ICitySectorService jCitySectorService { get; set; }
        [Inject] public ICityMapSettings jCityMapSettings { get; set; }
        [Inject] public SignalOnConstructControllerChangeState jSignalOnConstructControllerChangeState { get; set; }
        [Inject] public SignalOnFullClearMap jOnFullClearMapSignal { get; set; }
        [Inject] public FinishCityMapInitSignal jFinishCityMapInitSignal { get; set; }
        [Inject] public FinishApplicationInitSignal jFinishApplicationInitSignal { get; set; }
        [Inject] public StartCityMapInitializationSignal jStartCityMapInitializationSignal { get; set; }
        [Inject] public SignalOnSubSectorUnlockedVerified jSignalOnSubSectorUnlockedVerified { get; set; }
        [Inject] public BuildingConstructor jBuildingConstructor { get; set; }
        [Inject] public IEditModeService jEditModeService { get; set; }
        #endregion

        [Header("Pivot point position on the city grid.")]
        [SerializeField] private Vector2 _pivotGridPosition;

        [SerializeField] private BusinessSectorsTypes _sectorType;
        [SerializeField] private Material _gridMaterial;
        [SerializeField] private Vector2 _tilesPerChunk = new Vector2(20, 20);
        [SerializeField] private Vector2 _tileSize = new Vector2(1, 1);
        [SerializeField] private Vector2 _tilesInSpriteSheet = new Vector2(2, 2);

        private bool _isMapGridCreated;
        private TileMapMulti _tilemap;
        private ICityMap _cityMapModel;

        private const int AgroFieldID = 127;
        
        private bool AreWeInSelectedSector => jGameManager.CurrentSelectedObjectView?.MapObjectModel?.SectorId.Value == (int) _sectorType;
        private bool IsAgro => jGameManager.CurrentSelectedObjectView?.MapObjectModel?.ObjectTypeID.Value == AgroFieldID;
        
        [PostConstruct]
        public void PostConstruct()
        {
            jSignalOnConstructControllerChangeState.AddListener(OnGroundGridVisibilityChanged);
            jOnFullClearMapSignal.AddListener(ClearGrid);
            jFinishCityMapInitSignal.AddListener(RedrawGrid);
            jFinishApplicationInitSignal.AddListener(OnFinishApplicationInitSignal);
            jStartCityMapInitializationSignal.AddListener(OnStartCityMapInitialization);
            jSignalOnSubSectorUnlockedVerified.AddListener(OnSubSectorUnlocked);

            _tileSize *= jCityMapSettings.GridCellScale;
        }

        [Deconstruct]
        public void Deconstruct()
        {
            jSignalOnConstructControllerChangeState.RemoveListener(OnGroundGridVisibilityChanged);
            jOnFullClearMapSignal.RemoveListener(ClearGrid);
            jFinishCityMapInitSignal.RemoveListener(RedrawGrid);
            jFinishApplicationInitSignal.RemoveListener(OnFinishApplicationInitSignal);
            jSignalOnSubSectorUnlockedVerified.RemoveListener(OnSubSectorUnlocked);
        }

        private void OnStartCityMapInitialization(ICityMap iCityMap)
        {
            if (iCityMap.BusinessSector == _sectorType)
            {
                jStartCityMapInitializationSignal.RemoveListener(OnStartCityMapInitialization);
                _cityMapModel = iCityMap;
            }
        }

        private void OnMapCellsChanged(Vector2Int startPosition, Vector2Int endPosition, CellTileType cellTileType)
        {
            if (!_isMapGridCreated || _tilemap.TilesPerGridX != _cityMapModel.MapSize.Width || _tilemap.TilesPerGridY != _cityMapModel.MapSize.Height)
                CreateMapGrid();
            
            switch (cellTileType)
            {
                case CellTileType.Free:
                    _tilemap.UpdateGrid(startPosition, endPosition, 0, 0);
                    break;

                case CellTileType.Closed:
                    _tilemap.UpdateGrid(startPosition, endPosition, 0, 1);
                    break;

                case CellTileType.MinePlace:
                    _tilemap.UpdateGrid(startPosition, endPosition, 0, 0);
                    break;
            }
        }

        private void OnFinishApplicationInitSignal()
        {
            HideLockedSectorsGrid();
            BuildCityAreas();
        }
        
        private void OnSubSectorUnlocked(ILockedSector sector)
        {
            if (sector.BusinessSectorId.Value != (int) _sectorType)
            {
                return;
            }

            Vector2Int startPosition = sector.LeftBottomGridPos;
            Vector2Int endPosition = startPosition + sector.GridDimensions;
            OnMapCellsChanged(startPosition, endPosition, CellTileType.Free);
        }

        private void HideLockedSectorsGrid()
        {
            var sectors = jCitySectorService.LockedSectorsByCityType(_sectorType);

            if (sectors == null)
            {
                Logging.LogError("LockedSectors building error. Locked sectors list is null.");
                return;
            }

            if (_sectorType == BusinessSectorsTypes.Farm)
            {
                Vector2Int startPosition = Vector2Int.up * 4;
                var endPosition = new Vector2Int(23, 5);
                _tilemap.UpdateGrid(startPosition, endPosition, 1, 1);
            }

            foreach (CityLockedSector sector in sectors)
            {
                Vector2Int startPosition = sector.LockedSector.LeftBottomGridPos;
                Vector2Int endPosition = startPosition + sector.LockedSector.GridDimensions;

                _tilemap.UpdateGrid(startPosition, endPosition, 1, 1);
            }
        }

        private void BuildCityAreas()
        {
            var areas = jCitySectorService.SectorAreas(_sectorType);

            if (areas == null)
            {
                Logging.LogError("CityAreas building error. City areas list is null.");
                return;
            }

            foreach (ISectorArea area in areas.Where(c => c.IsDisabled))
            {
                Vector2Int startPosition = area.LeftBottomGridPos;
                Vector2Int endPosition = startPosition + area.GridDimensions;
                _tilemap.UpdateGrid(startPosition, endPosition, 0, 1);
            }
        }

        private void OnGroundGridVisibilityChanged(bool obj)
        {
            SwapGridsColors(obj);
            gameObject.SetActive(obj);
        }

        private void SwapGridsColors(bool enabled)
        {
            if (!enabled)
            {
                return;
            }

            Vector2Int startPosition = Vector2Int.zero;
            var endPosition = new Vector2Int(_tilemap.TilesPerGridX, _tilemap.TilesPerGridY);

            if (jBuildingConstructor.IsBuildModeEnabled)
            {
                // Building construction mode - need to show only active region white all the others red 
                if (AreWeInSelectedSector)
                {
                    if (IsAgro)
                    {
                        _tilemap.UpdateGrid(startPosition, endPosition, 0, 1);
                        DrawAgroGrid(0, 0);
                    }
                    else
                    {
                        _tilemap.UpdateGrid(startPosition, endPosition, 0, 0);
                        DrawAgroGrid(0, 1);
                    }
                }
                else
                {
                    _tilemap.UpdateGrid(startPosition, endPosition, 0, 1);
                }
            }
            else
            {
                // Road construction mode - need to hide all agro-sectors grid
                _tilemap.UpdateGrid(startPosition, endPosition, 0, 0);
                DrawAgroGrid(0, 1);
            }

            HideLockedSectorsGrid();
        }

        private void DrawAgroGrid(int xTile, int yTile)
        {
            if (_sectorType != BusinessSectorsTypes.Farm)
            {
                return;
            }

            Vector2Int startPosition = Vector2Int.zero;
            var endPosition = new Vector2Int(23, 4);
            _tilemap.UpdateGrid(startPosition, endPosition, xTile, yTile);
        }

        private void CreateMapGrid()
        {
            if (!_isMapGridCreated)
            {
                _isMapGridCreated = true;
                _tilemap = gameObject.AddComponent<TileMapMulti>();

                UpdateFields();
                _tilemap.BuildTileMap();

                gameObject.SetActive(false);
            }
            else
            {
                _tilemap.ClearChunks();
                UpdateFields();
                _tilemap.BuildTileMap();
            }
        }

        /// <summary>
        /// Обновляет все поля внутри сетки тайлов
        /// </summary>
        private void UpdateFields()
        {
            ICityGrid cityGrid = _cityMapModel.jCityGrid;
            
            _tilemap.TilesInChunk = _tilesPerChunk;
            _tilemap.TileSize = _tileSize;
            _tilemap.TilesInSpriteSheet = _tilesInSpriteSheet;
            _tilemap.GridMaterial = _gridMaterial;
            _tilemap.TilesPerGridX = cityGrid.Width;
            _tilemap.TilesPerGridY = cityGrid.Height;
        }

        /// <summary>
        /// Перерисовка сетки с обновлением сгенерированной меши
        /// </summary>
        /// <param name="map"></param>
        private void RedrawGrid(ICityMap map)
        {
            if (_cityMapModel == map)
            {
                CreateMapGrid();
                List<Vector2F> points = new List<Vector2F>();
                List<Vector2F> colors = new List<Vector2F>();
                for (int i = 0; i < _cityMapModel.jCityGrid.CityGridCells.Count; i++)
                {
                    for (int j = 0; j < _cityMapModel.jCityGrid.CityGridCells[i].Count; j++)
                    {
                        points.Add(new Vector2F(i,j));
                    
                        colors.Add(new Vector2F(0,0));
//                        if (CityMapModel.CityGrid[i][j].Value != null)
//                        {
//                            // _tilemap.UpdateGrid(i, j, 1, 0);
//                            colors.Add(new Vector2F(1,0));
//                        }
//                        else
//                        {
//                            colors.Add(new Vector2F(0, 0));
//                            // _tilemap.UpdateGrid(i, j, 0, 0);
//                        }
                    }
                }
                _tilemap.UpdateGrid(points, colors);
            }
        }

        public void ClearGrid(ICityMap map)
        {
            if (jEditModeService.IsEditModeEnable) return;
            
            if (_cityMapModel == map)
            {
                if (!_isMapGridCreated) return;

                _tilemap.ClearGrid();
            }
        }
    }
}
