using System;
using System.Collections.Generic;
using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.TrafficSystem.Road;
using NanoReality.GameLogic.Utilites;
using strange.extensions.signal.impl;
using UnityEngine;

namespace Assets.Scripts.Engine.BuildingSystem.CityMap
{
    /// <summary>
    /// Вспомогательный класс для удобной перерисовки дорожного полотна
    /// </summary>
    public class RoadViewGrid
    {
        /// <summary>
        /// Ширина карты
        /// </summary>
        public int Width { get; private set; }

        /// <summary>
        /// Высота карты
        /// </summary>
        public int Height { get; private set; }

        /// <summary>
        /// Матрица карты
        /// </summary>
        public RoadMapObjectView[,] RoadGrid;

        /// <summary>
        /// Dispatched when a road map object view was added
        /// </summary>
        public readonly Signal<RoadMapObjectView> OnRoadViewAddedSignal = new Signal<RoadMapObjectView>();

        /// <summary>
        /// Dispatched when a road map object view was removed
        /// </summary>
        public readonly Signal<RoadMapObjectView> OnRoadViewRemovedSignal = new Signal<RoadMapObjectView>();

        private readonly HashSet<RoadMapObjectView> _roadsCache;
        private readonly Dictionary<RoadTileShape, HashSet<RoadMapObjectView>> _roadsByShape;
        private readonly List<RoadMapObjectView> _lastUpdatedRoads;
        private readonly HashSet<RoadMapObjectView> _resetRoadViews;
        private readonly HashSet<RoadMapObjectView> _changedRoads;
        private bool _updateRoadTiles;
        private bool _isUnderConstruction;

        public IEnumerable<RoadMapObjectView> Roads { get { return _roadsCache; } }

        public IEnumerable<RoadMapObjectView> Crossroads
        {
            get
            {
                HashSet<RoadMapObjectView> crossroads;
                return _roadsByShape.TryGetValue(RoadTileShape.Crossroads, out crossroads) ? crossroads : null;
            }
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="width">ширина карты</param>
        /// <param name="height">высота карты</param>
        public RoadViewGrid(int width, int height)
        {
            Width = width;
            Height = height;
            RoadGrid = new RoadMapObjectView[Width, Height];

            _roadsCache = new HashSet<RoadMapObjectView>();
            _roadsByShape = new Dictionary<RoadTileShape, HashSet<RoadMapObjectView>>(new RoadTileShapeComparer())
            {
                {RoadTileShape.Straight, new HashSet<RoadMapObjectView>()},
                {RoadTileShape.Deadlock, new HashSet<RoadMapObjectView>()},
                {RoadTileShape.Turn, new HashSet<RoadMapObjectView>()},
                {RoadTileShape.Crossroads, new HashSet<RoadMapObjectView>()}
            };
            _lastUpdatedRoads = new List<RoadMapObjectView>(5);
            _resetRoadViews = new HashSet<RoadMapObjectView>();
            _changedRoads = new HashSet<RoadMapObjectView>();
            _updateRoadTiles = true;
            _isUnderConstruction = false;
        }

        /// <summary>
        /// Sets flag to update road tiles.
        /// If set to false, road tile views will not be updated upon map changes.
        /// </summary>
        /// <param name="update"></param>
        public void SetUpdateRoadTiles(bool update)
        {
            _updateRoadTiles = update;
        }

        /// <summary>
        /// Sets flag for road construction mode.
        /// If set to true, future changed road tiles will have only basic markings and no props.
        /// When set to false, tiles that have been changed during construction will be updated with markings and props. 
        /// </summary>
        /// <param name="isUnderConstruction"></param>
        public void SetUnderConstruction(bool isUnderConstruction)
        {
            _isUnderConstruction = isUnderConstruction;

            if (!isUnderConstruction)
            {
                UpdateChangedRoads();
            }
        }

        /// <summary>
        /// Добавление тайла дороги
        /// </summary>
        /// <param name="view">Вьюшка дороги</param>
        public void AddRoad(RoadMapObjectView view)
        {
            _roadsCache.Add(view);
            int x = view.GridPosition.x;
            int y = view.GridPosition.y;
		    
            RoadGrid[x, y] = view;
            UpdateRoadTiles(x, y);

        }

        /// <summary>
        /// Удаление тайла дороги
        /// </summary>
        /// <param name="view">Вьюшка дороги</param>
        public void RemoveRoad(RoadMapObjectView view)
        {
            _roadsCache.Remove(view);
		    
            int x = view.GridPosition.x;
            int y = view.GridPosition.y;
		    
            _roadsByShape[view.RoadTileType.ToShape()].Remove(view);
            RoadGrid[x, y] = null;
            UpdateRoadTiles(x, y);
            _changedRoads.Remove(view);

            OnRoadViewRemovedSignal.Dispatch(view);
        }

        /// <summary>
        /// Обновление дорожного полотна
        /// </summary>
        public void UpdateRoads()
        {
            UnityEngine.Profiling.Profiler.BeginSample("Update Roads");

            UpdateRoads(_roadsCache);

            _changedRoads.Clear();

            UnityEngine.Profiling.Profiler.EndSample();
        }

        /// <summary>
        /// Updates road views for given roads
        /// </summary>
        /// <param name="roads"></param>
        public void UpdateRoads(IEnumerable<RoadMapObjectView> roads)
        {
            foreach (var road in roads)
            {
                if (road != null)
                {
                    UpdateRoadTile(road);
                }
            }

            UpdateRoadViews();
        }

        /// <summary>
        /// Updates recently changed roads
        /// </summary>
        public void UpdateChangedRoads()
        {
            UnityEngine.Profiling.Profiler.BeginSample("Update Changed Roads");

            foreach (var road in _changedRoads)
            {
                if (road != null)
                {
                    road.Deselect();
                    UpdateRoadTile(road);
                }
            }

            _changedRoads.Clear();

            UpdateRoadViews();

            UnityEngine.Profiling.Profiler.EndSample();
        }

        /// <summary>
        /// Deselects all road tiles
        /// </summary>
        public void DeselectRoads()
        {
            DeselectRoads(_roadsCache);
        }

        /// <summary>
        /// Deselects given roads
        /// </summary>
        /// <param name="roads"></param>
        public void DeselectRoads(IEnumerable<RoadMapObjectView> roads)
        {
            foreach (var road in roads)
            {
                if (road != null)
                {
                    road.Deselect();
                }
            }
        }

        public void Clear()
        {
            RoadGrid = new RoadMapObjectView[Width, Height];
            _roadsCache.Clear();
            _roadsByShape[RoadTileShape.Crossroads].Clear();
            _roadsByShape[RoadTileShape.Deadlock].Clear();
            _roadsByShape[RoadTileShape.Straight].Clear();
            _roadsByShape[RoadTileShape.Turn].Clear();
            _lastUpdatedRoads.Clear();
            _resetRoadViews.Clear();
        }

        private void UpdateRoadTiles(int x, int y)
        {
            if (!_updateRoadTiles)
            {
                return;
            }

            _lastUpdatedRoads.Clear();

            var view = RoadGrid[x, y];

            MarkAndUpdate(view);

            if (x > 0)
            {
                var road = RoadGrid[x - 1, y];
                MarkAndUpdate(road);
            }

            if (y > 0)
            {
                var road = RoadGrid[x, y - 1];
                MarkAndUpdate(road);
            }

            if (x < Width - 1)
            {
                var road = RoadGrid[x + 1, y];
                MarkAndUpdate(road);
            }

            if (y < Height - 1)
            {
                var road = RoadGrid[x, y + 1];
                MarkAndUpdate(road);
            }

            UpdateRoadViews(_lastUpdatedRoads);
        }

        private void MarkAndUpdate(RoadMapObjectView road)
        {
            if (road == null)
            {
                return;
            }

            UpdateRoadTile(road);

            _lastUpdatedRoads.Add(road);

            if (_isUnderConstruction)
            {
                _changedRoads.Add(road);
            }
        }

        private void UpdateRoadTile(RoadMapObjectView view)
        {
            var pos = view.GridPosition;
            var x = pos.x;
            var y = pos.y;

            RoadMapObjectView[] adjacent = new RoadMapObjectView[4];

            if (x < Width - 1 && RoadGrid[x + 1, y] != null)
            {
                adjacent[(int)Directions.RightTop] = RoadGrid[x + 1, y];
            }

            if (y < Height - 1 && RoadGrid[x, y + 1] != null)
            {
                adjacent[(int)Directions.LeftTop] = RoadGrid[x, y + 1];
            }

            if (x > 0 && RoadGrid[x - 1, y] != null)
            {
                adjacent[(int)Directions.LeftBottom] = RoadGrid[x - 1, y];
            }

            if (y > 0 && RoadGrid[x, y - 1] != null)
            {
                adjacent[(int)Directions.RightBottom] = RoadGrid[x, y - 1];
            }

            _roadsByShape[view.RoadTileType.ToShape()].Remove(view);

            view.UpdateRoadTileType(adjacent);

            _roadsByShape[view.RoadTileType.ToShape()].Add(view);
        }

        private void UpdateRoadViews()
        {
            foreach (var road in _roadsByShape[RoadTileShape.Crossroads])
            {
                UpdateRoadView(road);
            }

            foreach (var road in _roadsByShape[RoadTileShape.Deadlock])
            {
                UpdateRoadView(road);
            }

            foreach (var road in _roadsByShape[RoadTileShape.Turn])
            {
                UpdateRoadView(road);
            }
        }

        private void UpdateRoadView(RoadMapObjectView view)
        {
            if (view == null || view.IsViewUpdated)
            {
                return;
            }

            view.UpdateRoadView(_isUnderConstruction);

            foreach (var pair in view.Adjacent)
            {
                UpdateRoadView(pair.Value);
            }
        }

        private void UpdateRoadViews(IEnumerable<RoadMapObjectView> views)
        {
            UnityEngine.Profiling.Profiler.BeginSample("Update Roads (recursive reset)");

            _resetRoadViews.Clear();
            foreach (var view in views)
            {
                ResetRoadView(view);
            }

            UpdateRoadViews();

            UnityEngine.Profiling.Profiler.EndSample();
        }

        private void ResetRoadView(RoadMapObjectView view)
        {
            if (_resetRoadViews.Contains(view))
            {
                return;
            }

            view.ResetView();
            _resetRoadViews.Add(view);

            if (_isUnderConstruction)
            {
                _changedRoads.Add(view);
            }
            
            if (view.RoadTileType.IsCrossroads() && !_lastUpdatedRoads.Contains(view))
            {
                return;
            }

            foreach (var pair in view.Adjacent)
            {
                ResetRoadView(pair.Value);
            }
        }

        public bool IsAreaFreeFromRoads(Vector2 pos, Vector2 size)
        {
            var startCell = pos;
            var lastCell = startCell + size;
            bool isPositionEmpty = true;

            if (!IsPositionCorrect(startCell.x, startCell.y) || !IsPositionCorrect(lastCell.x - 1, lastCell.y - 1))
                return false;

            for (float i = startCell.x; i < lastCell.x; i++)
            {
                for (float j = startCell.y; j < lastCell.y; j++)
                {

                    if (RoadGrid[(int)i, (int)j] != null)
                    {

                        isPositionEmpty = false;
                        break;

                    }
                }
            }
            return isPositionEmpty;
        }


        public bool IsPositionCorrect(float x, float y)
        {
            if (Width == 0 || Height == 0) throw new Exception("something is wrong with bounds of " + GetType().Name + "! " + Width + "," + Height);
            if (x < 0 || y < 0 || x >= Width || y >= Height) return false;
            return true;
        }
    }
}