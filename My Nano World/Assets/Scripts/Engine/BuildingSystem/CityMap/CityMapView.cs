﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.UtilitesEngine.CustomDebugging.Models.api;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Engine.BuildingSystem.CityMap;
using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.GameManager.Models.api;
using strange.extensions.mediation.impl;
using UnityEngine;
using NanoLib.Services.InputService;
using NanoReality.Engine.UI;
using NanoReality.Engine.UI.Extensions.EditModePanels;
using NanoReality.Game.Attentions;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.Settings.BuildSettings.Models.Api;
using NanoReality.Game.Tutorial;
using NanoReality.Isometry;

namespace Assets.Scripts.Engine.BuildingSystem.CityMap
{
	/// <summary>
	/// Вьюшка тайловой карты
	/// </summary>
	public class CityMapView : View
	{
        #region Injects
        
        [Inject] public IMapObjectsGenerator jMapObjectsGenerator { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public ICityMapSettings jCityMapSettings { get; set; }
        [Inject] public IConstructionController jConstructionController { get; set; }
        [Inject] public SignalOnMapObjectViewTap jSignalOnMapObjectViewTap { get; set; }
        [Inject] public SignalOnMapObjectChangedSelect jSignalOnMapObjectChangedSelect { get; set; }
        [Inject] public SignalRoadMapReselect jSignalRoadMapReselect { get; set; }
        [Inject] public SignalTutorialRoadCreate jSignalRoadCreate { get; set; }
        [Inject] public SignalTutorialRoadRemove jSignalRoadRemove { get; set; }
        [Inject] public UpdateWorldSpaceDepth jUpdateWorldSpaceDepth { get; set; }
        [Inject] public SignalOnMapObjectViewMoved jOnMapObjectMovedSignal { get; set; }
        [Inject] public IEditModeService jEditModeService { get; set; }
        [Inject] public IDebug Debug { get; set; }

        #endregion
        
        private RoadViewGrid _roadGrid;
        private Id<IBusinessSector> _sectorId;
        private bool _isAllInteractionsBlocked;
        
        /// <summary>
        /// Cached map object by id for better performance
        /// </summary>
        private readonly Dictionary<Id_IMapObject, MapObjectView> _mapObjectsById = new Dictionary<Id_IMapObject, MapObjectView>();

        /// <summary>
        /// Cached map object by position for better performance
        /// </summary>
        private readonly Dictionary<Vector2, MapObjectView> _mapObjectsByPosition = new Dictionary<Vector2, MapObjectView>();
        private readonly HashSet<MapObjectintTypes> InvisibleMapObjectTypes = new HashSet<MapObjectintTypes> { MapObjectintTypes.GroundBlock };

        public bool IsSortEnabled;
	    
	    public int MapHeight;
	    public int MapWidth;
	    
	    public float CurrentMaxDepth;
	    
	    public LockedSubSectorBorderGenerator SubSectorBorderGenerator;

	    [SerializeField] private BusinessSectorsTypes _businessSector;
	    [SerializeField] private List<MapObjectView> _mapObjects = new List<MapObjectView>();
	    [SerializeField] private Transform _sectorOrigin;
	    [SerializeField] private LockedSubMapView lockedSubMapView;

		public List<MapObjectView> Objects => _mapObjects;

		public LockedSubMapView LockedSubMapView => lockedSubMapView;
		public BusinessSectorsTypes BusinessSector => _businessSector;

		public RoadViewGrid RoadGrid => _roadGrid;

		public BusinessSectorsTypes SectorID => _businessSector;

		public ICityMap CityMapModel
		{
			get { return jGameManager.GetMap(SectorID.GetBusinessSectorId()); }
		}

		[PostConstruct]
		public void PostConstruct()
		{
			jSignalOnMapObjectChangedSelect.AddListener(CancelRoadSelection);
			jSignalOnMapObjectViewTap.AddListener(OnTouchMapObjectView);
		}
		
		[Deconstruct]
		public void Deconstruct()
		{
			jSignalOnMapObjectChangedSelect.RemoveListener(CancelRoadSelection);
			jSignalOnMapObjectViewTap.RemoveListener(OnTouchMapObjectView);
		}
		
	    public void Initialize(int height, int width)
	    {
	        MapHeight = height;
	        MapWidth = width;
	        _roadGrid = new RoadViewGrid(MapWidth, MapHeight);

	        lockedSubMapView.Clear();
	    }

	    private List<RoadMapObjectView> _currentSelectedRoads;

		private void OnTouchMapObjectView(MapObjectView view)
		{
			var isEditMode = jEditModeService.IsEditModeEnable;

			if (view == null || view.MapObjectModel.SectorId.ToBusinessSectorType() != SectorID)
			{
				return;
			}

			if (isEditMode && jEditModeService.IsEraseEnable)
			{
				jEditModeService.RemoveBuilding(view);
				return;
			}

			var road = view as RoadMapObjectView;
			if (road != null && jConstructionController.IsBuildModeEnabled)
			{
				SelectRoad(road);
			}
			else if (!isEditMode || (view.MapObjectModel is IAoeBuilding))
			{
				view.OnMapObjectViewTap();
			}
		}

		public Vector2F GetAverageBuildingsPosition()
		{
			var averagePosition = CityMapModel.GetAverageBuildingsPosition();
			if (averagePosition.Equals(new Vector2F(0,0)))
			{
				return _sectorOrigin.position.ToVector2F();
			}

			return averagePosition;
		}

		public Vector2F GetMapViewOriginPosition()
		{
			return new Vector2F(_sectorOrigin.position.x, _sectorOrigin.position.z);
		}

        private void SelectRoad(RoadMapObjectView road)
        {
            // запрещаем выделение дороги в режиме строительства
            if (jConstructionController.IsCreateRoad || !jGameManager.IsMapObjectSelectEnable || !jConstructionController.IsRoadBuildingMode) return;

            // тач по невыделенному участку дороги
            if (!road.SelectComponent.IsSelected)
            {
                // снимаем предыдущее выделение
                if (_currentSelectedRoads != null)
                {
	                jGameManager.DeselectRoads();

                    // сигнализируем о том, что происходит перевыделенние дороги
                    jSignalRoadMapReselect.Dispatch();
                }

                // проводим выделение
                _currentSelectedRoads = road.SelectRoadSegment();
            }
        }

		private void CancelRoadSelection(MapObjectView sender, bool isSelected)
		{
		    if(sender.MapObjectModel.SectorId != _sectorId) return;
			if (!isSelected)
			{
				_currentSelectedRoads = null;
			    jGameManager.DeselectRoads();
            }
        }

        [ContextMenu("UpdateRoads")]
		public void UpdateRoads()
		{
			_roadGrid.UpdateRoads();
	    }

        public void DeselectRoads()
		{
			_roadGrid.DeselectRoads();
	    }

	    public void DeselectRoads(IEnumerable<RoadMapObjectView> roadViews)
	    {
	        _roadGrid.DeselectRoads(roadViews);
	    }

        /// <summary>
        /// Очищает карту от вьюшек
        /// </summary>
        public void ClearMap()
		{
			foreach (var mapObjectView in _mapObjects)
			{
				mapObjectView.DisposeView();
			}

			_mapObjects.Clear();
            _mapObjectsById.Clear();

		    if (_roadGrid != null)
		    {
		        _roadGrid.Clear();
		    }

		    if (!jEditModeService.IsEditModeEnable)
		    {
			    lockedSubMapView.Clear();
		    }
		}

		public void SetMapObject(Vector2Int position, IMapObject mapObject, Action callback)
		{
			if (mapObject == null || InvisibleMapObjectTypes.Contains(mapObject.MapObjectType))
			{
				return;
			}

			MapObjectView obj = jMapObjectsGenerator.GetMapObjectView(mapObject.Id);
			if (obj == null)
			{
				Debug.LogWarning("Map Objects Generator not contains: " + mapObject.ObjectTypeID + " with level:" + mapObject.Level);
			}

			AddMapObjectView(obj, mapObject, position);
			callback?.Invoke();

			if (obj.MapObjectModel.MapObjectType != MapObjectintTypes.Road)
			{
				return;
			}

			_roadGrid.AddRoad((RoadMapObjectView) obj);
			jSignalRoadCreate.Dispatch((RoadMapObjectView) obj);
		}

        public void RemoveMapObject(Id_IMapObject mapId)
		{
			var mapObject = GetMapObjectById(mapId);

			if (mapObject == null) return;

			if (mapObject.MapObjectModel.MapObjectType == MapObjectintTypes.Road)
			{
				_roadGrid.RemoveRoad((RoadMapObjectView)mapObject);
                jSignalRoadRemove.Dispatch((RoadMapObjectView)mapObject);
			}

			RemoveMapObjectView(mapObject);
		}

		/// <summary>
		/// Апгрейдит вьюшку на карте
		/// </summary>
		/// <param name="mapObject">Новая модель здания (проапгрейдженная)</param>
		public void UpgradeMapObject(IMapObject mapObject)
		{
			var viewBeforeUpgrade = GetMapObjectById(mapObject.MapID);
			if (viewBeforeUpgrade == null)
				return;
			
			IMapObject mapObjectModel = mapObject;
			Int32 level = mapObjectModel.Level;
			Vector2F pos = mapObjectModel.MapPosition;
			var position = new Vector2Int((int)pos.X, (int)pos.Y);

            jMapObjectsGenerator.GetMapObjectView(mapObjectModel.Id, obj =>
            {
                obj.Depth = viewBeforeUpgrade.Depth;
		        RemoveMapObjectView(viewBeforeUpgrade);
		        AddMapObjectView(obj, mapObjectModel, position);
                //obj.SortObject();
               
                if (IsSortEnabled)
                {
                    if (obj.IgnoreDepthSort)
                    {
                        obj.SortObject();
                    }
                    else
                    {
                        SortDepthObjects();
                    }
                }
            });
		}

		/// <summary>
		/// Двигает объект по карте
		/// </summary>
		/// <param name="position">Позиция на карте</param>
		/// <param name="mapObject">Модель объекта который нужно подвинуть</param>
		public void MoveMapObject(Vector2Int position, IMapObject mapObject, Action callback = null)
		{
			MapObjectView objectView = GetMapObjectById(mapObject.MapID);
			SetObjectPosition(objectView, position);
			callback?.Invoke();
		    jOnMapObjectMovedSignal.Dispatch(objectView);
		}

		/// <summary>
		/// Вычисляет Y позицию здания и сортирует здание по высоте
		/// </summary>
		/// <param name="obj">здание</param>
		/// <param name="position">x-z позиция здания</param>
		private void SetObjectPosition(MapObjectView obj, Vector2Int position)
		{
			obj.SetGridPosition(position);

			if (IsSortEnabled)
		    {
		        if (obj.IgnoreDepthSort)
		        {
		            obj.SortObject();
		        }
		        else
		        {
		            SortDepthObjects();
		        }
		    }
		}

		public void SortDepthObjects()
		{
            CurrentMaxDepth = IsometryUtils.SortObjects(_mapObjects);
            
            jUpdateWorldSpaceDepth.Dispatch();
		}

		public void AddLockedSector(CityLockedSector cityLockedSector)
		{
			lockedSubMapView.Add(cityLockedSector);
			RedrawSubSectorsBorders(lockedSubMapView.Views);
		}

		public void RedrawSubSectorsBorders(IEnumerable<LockedSectorView> lockedSectorsView)
		{
			SubSectorBorderGenerator.CreateMesh(lockedSectorsView, 0f);
			SubSectorBorderGenerator.gameObject.SetActive (true);
		}

		/// <summary>
        /// Ищет вьюшку на карте
        /// </summary>
        /// <param name="type">тип искомого объекта</param>
        /// <returns>возвращает вьюшку если она есть на карте, иначе вернет null</returns>
        public MapObjectView GetMapObjectViewByType(MapObjectintTypes type)
        {
            var mapObjectView = _mapObjects.FirstOrDefault(
                obj => obj.MapObjectModel != null && obj.MapObjectModel.MapObjectType == type);
            return mapObjectView;
        }

        /// <summary>
        /// Ищет вьюшку на карте
        /// </summary>
        /// <param name="mapId">ай ди вьюшку которую нужно найти</param>
        /// <returns>возвращает вьюшку если она есть на карте, иначе вернет null</returns>
        public MapObjectView GetMapObjectById(Id_IMapObject mapId)
        {
            MapObjectView mapObjectView;
            if (_mapObjectsById.TryGetValue(mapId, out mapObjectView))
            {
                return mapObjectView;
            }
            return null;
        }

        /// <summary>
        /// Converts world position to city map position.
        /// </summary>
        /// <param name="worldPosition"></param>
        /// <returns></returns>
	    public Vector2F ConvertToGridPosOnMap(Vector3 worldPosition)
	    {
	        var offset = (worldPosition - transform.position) / jCityMapSettings.GridCellScale;
	        
            var result = new Vector2F((int)offset.x, (int)offset.z);
            return result;
        }
        
        public Vector2Int ConvertToGridPos(Vector3 worldPosition)
        {
	        var offset = (worldPosition - transform.position) / jCityMapSettings.GridCellScale;
	        
	        var result = new Vector2Int((int)offset.x, (int)offset.z);
	        return result;
        }

        /// <summary>
        /// Checks if world position belongs to city map.
        /// </summary>
        /// <param name="worldPosition"></param>
        /// <returns></returns>
	    public bool Contains(Vector3 worldPosition)
	    {
	        var pos = ConvertToGridPosOnMap(worldPosition);
	        int x = pos.intX;
	        int y = pos.intY;
	        if (x < 0 || y < 0 || x >= MapWidth || y >= MapHeight) return false;
	        return true;
        }

        public RoadMapObjectView GetRoadElement(Vector2F position)
        {
            var roadMatrix = RoadGrid.RoadGrid;
            if (position.intX >= 0 && position.intX < roadMatrix.GetLength(0) &&
                position.intY >= 0 && position.intY < roadMatrix.GetLength(1))
            {
                return roadMatrix[position.intX, position.intY];
            }
            Debug.LogWarning("The are not exist road in position " + position);
            return null;
        }

        private void AddMapObjectView(MapObjectView mapObjectView, IMapObject mapObject, Vector2Int position)
	    {
            // вьюшки зданий храним внутри объекта карты
            mapObjectView.CacheMonobehavior.CachedTransform.SetParent(transform);
	        // сигнализируем о добавлении объекта на карту после колбека для правильного апдейта дорог
	        mapObjectView.InitializeView(mapObject);
            SetObjectPosition(mapObjectView, position);

	        _mapObjects.Add(mapObjectView);
	        _mapObjectsByPosition[mapObjectView.GridPosition] = mapObjectView;
            _mapObjectsById[mapObject.MapID] = mapObjectView;

            // проверка на фективный id
            if (mapObject.MapID.Value < 0)
            {
                // обновляем кеш по id после получения ответа от сервера
                mapObject.EventOnCommitBuilding.AddOnce((oldId, obj) =>
                {
                    var view = GetMapObjectById(oldId);
                    // проверка на кеш объекта под старым фективным id
                    if (view != null && view.MapObjectModel == obj)
                    {
                        _mapObjectsById.Remove(oldId);
                    }

	                _mapObjectsById[obj.MapID] = mapObjectView;
	            });
	        }
        }

	    private void RemoveMapObjectView(MapObjectView mapObjectView)
	    {
		    mapObjectView.DisposeView();
	        _mapObjects.Remove(mapObjectView);
	        _mapObjectsById.Remove(mapObjectView.MapId);
	        _mapObjectsByPosition.Remove(mapObjectView.GridPosition);

            mapObjectView.FreeObject();
        }
	}
}
