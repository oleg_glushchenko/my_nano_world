﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoLib.Services.InputService;
using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.BuildingSystem.MapObjects.Components;
using NanoReality.Engine.UI.Extensions.EditModePanels;
using NanoReality.GameLogic.BuildingSystem;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.GameManager.Models.api;
using UnityEngine;

namespace Assets.Scripts.Engine.BuildingSystem.CityMap.Highlightings
{
    public class HighlightController
    {
        #region Signals

        [Inject] public SignalOnConstructControllerChangeState jSignalOnConstructControllerChangeState { get; set; }
        [Inject] public MapObjectMovedSignal jMapObjectMovedSignal { get; set; }
        [Inject] public SignalBuildFinishVerificated jSignalBuildFinishVerificated { get; set; }

        #endregion

        #region Objects Injects

        [Inject] public IGameCamera jGameCamera { get; set; }
        [Inject] public BuildingConstructor jBuildingConstructor { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public IGlobalMapObjectViewSettings jGlobalMapObjectViewSettings { get; set; }
        [Inject] public IEditModeService jEditModeService { get; set; }

        #endregion

        private readonly List<MapObjectintTypes> _aoeTypes = new List<MapObjectintTypes>
        {
            MapObjectintTypes.Hospital,
            MapObjectintTypes.Police,
            MapObjectintTypes.School,
            MapObjectintTypes.EntertainmentBuilding,
            MapObjectintTypes.FoodSupply,
            MapObjectintTypes.PowerPlant,
            MapObjectintTypes.PowerStation
        };

        private IAoeBuilding _selectedAoeSender;
        private Action removeHighlight;
        private IMapObject _selectedMapObject;

        private Material AoeEnabled => jGlobalMapObjectViewSettings.AoeMaterials.GetMaterial(_selectedAoeSender.MapObjectType);
        private Material AoeDisabled => jGlobalMapObjectViewSettings.AoeDisabledMaterial;
        private bool IsEditModeAOE => jEditModeService.IsEditModeEnable && jEditModeService.IsAOEEnable;

        [PostConstruct]
        public void PostConstruct()
        {
            jSignalOnConstructControllerChangeState.AddListener(OnConstructChangeState);
            jMapObjectMovedSignal.AddListener(MockOnMapObjectViewMoved);
            jSignalBuildFinishVerificated.AddListener(SomeBuildingJustFinishConstruction);
        }

        [Deconstruct]
        public void Deconstruct()
        {
            jSignalOnConstructControllerChangeState.RemoveListener(OnConstructChangeState);
            jMapObjectMovedSignal.RemoveListener(MockOnMapObjectViewMoved);
            jSignalBuildFinishVerificated.RemoveListener(SomeBuildingJustFinishConstruction);
        }

        private void SomeBuildingJustFinishConstruction(IMapObject mapObject)
        {
            if (_selectedAoeSender != null)
            {
                EnableAoe(mapObject);
            }
        }
        
        public void DisableAoeMode()
        {
            if (jBuildingConstructor.IsBuildModeEnabled || IsEditModeAOE) return;
            
            jGameCamera.SetGrayscaleActive(false);
            removeHighlight?.Invoke();
            
            if (_selectedAoeSender == null) return;
            _selectedAoeSender.AoeLayoutChanged -= OnAoeLayoutChanged;
            SetHighlightStateOnSenders(false);
            SetHighlightStateOnReceivers(false);
            _selectedMapObject = null;
            _selectedAoeSender = null;
        }

        private void OnAoeLayoutChanged(IAoeBuilding aoeBuilding)
        {
            SetHighlightStateOnReceivers(true);
        }

        private void SetHighlightStateOnSenders(bool value)
        {
            var cityMapView = jGameManager.GetMapView(_selectedAoeSender.SectorId);
            var senders = cityMapView.Objects.Where(nextMapObject => nextMapObject.MapObjectModel.MapObjectType == _selectedAoeSender.MapObjectType);
            foreach (var sender in senders)
            {
                if (value)
                {
                    if (!sender.MapObjectModel.IsVerificated) continue;
                    (sender as IAoeBuildingView)?.HighlighterComponent.SetHighlighting();
                }
                else
                {
                    if (IsEditModeAOE) return;
                    (sender as IAoeBuildingView)?.HighlighterComponent.RemoveHighlighting();
                }
            }
        }

        public void EnableAoe(IMapObject mapObject)
        {
            if (!(mapObject is IAoeBuilding selectedAoeBuilding)) return;
            if (_selectedMapObject != null &&_selectedMapObject != mapObject && !IsEditModeAOE)
            {
                SetHighlightStateOnSenders(false);
                SetHighlightStateOnReceivers(false);
            }

            _selectedMapObject = mapObject;
            _selectedAoeSender = selectedAoeBuilding;
            _selectedAoeSender.AoeLayoutChanged += OnAoeLayoutChanged;

            SetHighlightStateOnSenders(true);
            SetHighlightStateOnReceivers(true);
        }

        public void ForceChangeAoeState(bool state)
        {
            jGameCamera.SetGrayscaleActive(state);
            var cityMapView = jGameManager.CityMapViews;

            foreach (var cityMap in cityMapView.Values)
            {
                foreach (var mapObject in cityMap.Objects)
                {
                    if (state)
                    {
                        if (!mapObject.MapObjectModel.IsVerificated) continue;
                        (mapObject as IAoeBuildingView)?.HighlighterComponent.SetHighlighting();
                    }
                    else
                    {
                        (mapObject as IAoeBuildingView)?.HighlighterComponent.RemoveHighlighting();
                    }
                }
            }

            _selectedMapObject = null;
            _selectedAoeSender = null;
        }

        public void SetGrayscale(MapObjectView objectView)
        {
            jGameCamera.SetGrayscaleActive(true);
            ColorHighlightComponent colorHighlightComponent = objectView.GetCachedComponent<ColorHighlightComponent>();
            colorHighlightComponent.HighlightMaterial = jGlobalMapObjectViewSettings.SelectedBuildingMaterial;
            objectView.MapObjectModel.EventOnMapObjectBuildStarted.AddOnce((m) => removeHighlight = null); 
            colorHighlightComponent.SetHighlighting();
            removeHighlight = () => { colorHighlightComponent.RemoveHighlighting(); };
        }

        public void EnableBuildingsAnimation(MapObjectView objectView)
        {
            var component = objectView.GetCachedComponent<MapObjectStaticAnimations>();
            component.SwitchAnimationsRenders(true);
        }

        private void OnConstructChangeState(bool value)
        {
            var view = jBuildingConstructor.SelectedObjectView;
            if (view == null)
            {
                DisableAoeMode();
                return;
            }
            
            if (!value)
            {
                DisableAoeMode();
                return;
            }

            if (!HighlightAllowed(view.MapObjectModel)) return;

            if (!_aoeTypes.Contains(view.MapObjectModel.MapObjectType))
            {
                return;
            }

            SetGrayscale(view);
            EnableAoe(view.MapObjectModel);
        }

        private void MockOnMapObjectViewMoved(MapObjectView view)
        {
            if (_selectedMapObject != view.MapObjectModel) return;
            OnMapObjectViewMoved(view.MapObjectModel);
        }

        private bool HighlightAllowed(IMapObject view)
        {
            if (view != null)
            {
                if (!view.IsConstructed) return false;
                if (!view.IsVerificated) return false;
            }

            return true;
        }

        private void OnMapObjectViewMoved(IMapObject view)
        {
            if (!HighlightAllowed(view)) return;
            SetHighlightStateOnReceivers(true);
        }

        private void SetHighlightStateOnDifferentTypesOfAoeEmitters(bool value)
        {
            CityMapView cityMapView = jGameManager.GetMapView(_selectedAoeSender.SectorId);
            ICityMap mapSector = jGameManager.GetMap(_selectedAoeSender.SectorId);

            MapObjectintTypes unNeededType = _selectedAoeSender.MapObjectType;
            var differentTypesOfAoeEmitters = mapSector.MapObjects.Where(b =>
                b.MapObjectType != unNeededType && _aoeTypes.Contains(b.MapObjectType));

            foreach (IMapObject mapObject in differentTypesOfAoeEmitters)
            {
                MapObjectView mapObjectView = cityMapView.GetMapObjectById(mapObject.MapID);
                var colorComponent = mapObjectView.GetCachedComponent<ColorHighlightComponent>();

                if (!value)
                {
                    colorComponent.RemoveHighlighting();
                    EnableBuildingsAnimation(mapObjectView);
                    continue;
                }

                colorComponent.HighlightMaterial =
                    jGlobalMapObjectViewSettings.AoeMaterials.GetMaterial(mapObjectView.MapObjectModel.MapObjectType);
                colorComponent.SetHighlighting();
            }
        }

        private void SetHighlightStateOnReceivers(bool value)
        {
            if (IsEditModeAOE) return;

            SetHighlightStateOnDifferentTypesOfAoeEmitters(value);
            CityMapView cityMapView = jGameManager.GetMapView(_selectedAoeSender.SectorId);
            ICityMap mapSector = jGameManager.GetMap(_selectedAoeSender.SectorId);

            IEnumerable<MapObjectView> aoeBuildings = cityMapView.Objects.Where(b =>
                b.MapObjectModel.MapObjectType == _selectedAoeSender.MapObjectType);

            List<AoeInfoData> aoeInfo = GetCurrentAoeLayout(aoeBuildings);

            IEnumerable<IMapObject> receivers = mapSector.MapObjects.Where(b =>
                _selectedAoeSender.IsCoveringBuilding(b));

            foreach (IMapObject mapObject in receivers)
            {
                MapObjectView mapObjectView = cityMapView.GetMapObjectById(mapObject.MapID);
                var colorComponent = mapObjectView.GetCachedComponent<ColorHighlightComponent>();
                if (!value)
                {
                    colorComponent.RemoveHighlighting();
                    EnableBuildingsAnimation(mapObjectView);
                    continue;
                }

                colorComponent.HighlightMaterial = AoeUtilities.IsBuildingCovered(aoeInfo, mapObject)
                    ? AoeEnabled
                    : AoeDisabled;

                colorComponent.SetHighlighting();
            }
        }

        private List<AoeInfoData> GetCurrentAoeLayout(IEnumerable<MapObjectView> aoeBuildings)
        {
            var aoePivotToSize = new List<AoeInfoData>();
            foreach (var mapObjectView in aoeBuildings.Where(b => b.MapObjectModel.IsConstructed && b.MapObjectModel.IsConstructFinished))
            {
                var aoeModel = (IAoeBuilding) mapObjectView.MapObjectModel;
                Vector2 mapGridPosition = mapObjectView.GridPosition;
                AoeLayout aoeLayout = aoeModel.AoeLayoutType;
                Vector2 dimensions = aoeModel.Dimensions.ToVector2();
                Vector2 aoeAreaSize = aoeModel.AoeSize;

                Vector2 aoeAreaPivot = AoeUtilities.CalculateAoeAreaPivot(mapGridPosition, dimensions, aoeLayout, aoeAreaSize);
                Vector2 resultAoeAreaSize = AoeUtilities.ConvertAreaToRealSize(aoeLayout, aoeAreaSize);
                aoePivotToSize.Add(new AoeInfoData(aoeLayout, aoeAreaPivot, resultAoeAreaSize));
            }

            return aoePivotToSize;
        }
    }
}