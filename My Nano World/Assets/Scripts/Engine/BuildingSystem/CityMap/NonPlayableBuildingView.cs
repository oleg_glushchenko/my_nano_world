﻿using NanoLib.Services.InputService;
using Assets.NanoLib.Utilities;
using NanoReality.Engine.UI;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.Game.Tutorial;
using strange.extensions.mediation.impl;
using UnityEngine;


public class NonPlayableBuildingView : View
{
    #region Injects
    [Inject]
    public SignalOnNonPlayableBuildTap jSignalOnNonPlayableBuildTap { get; set; }

    [Inject]
    public IHardTutorial jHardTutorial { get; set; }

    [Inject]
    public IWorldSpaceCanvas jWorldSpaceCanvas { get; set; }

    [Inject]
    public IConstructionController jConstructionController { get; set; }

    #endregion

    public CacheMonobehavior CacheMonobehavior
    {
        get
        {
            return _cacheMonobehavior ?? (_cacheMonobehavior = new CacheMonobehavior(this));
        }
    }
    private CacheMonobehavior _cacheMonobehavior;

    public MeshFilter Mesh; //используем для правильной центровки (NonPlayableBuildingLooker)

    private bool _isPostConstructed;

    [PostConstruct]
    public void PostConstruct()
    {
        if(_isPostConstructed) return;
        _isPostConstructed = true;

        jSignalOnNonPlayableBuildTap.AddListener(OnNonPlayableBuildTap);
    }

    /// <summary>
    /// кликнули на "не игровой" обьект
    /// </summary>
    private void OnNonPlayableBuildTap(NonPlayableBuildingView view)
    {
        if(!jHardTutorial.IsTutorialCompleted) return;
        if(jConstructionController.IsCurrentlySelectObject || jConstructionController.IsRoadBuildingMode) return;
        if (view != this) return;

        var nonPlayableBuildUi = jWorldSpaceCanvas.GetNonPlayableBuildingBar();
        nonPlayableBuildUi.Init(this);
    }
}