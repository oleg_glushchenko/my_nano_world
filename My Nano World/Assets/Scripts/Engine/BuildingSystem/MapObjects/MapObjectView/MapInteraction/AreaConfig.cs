﻿using System;
using UnityEngine;

namespace NanoReality.Engine.BuildingSystem.MapObjects
{
    [Serializable]
    public class AreaConfig
    {        
        public Color AreaColor;
        public AreaType AreaType;
    }
}
