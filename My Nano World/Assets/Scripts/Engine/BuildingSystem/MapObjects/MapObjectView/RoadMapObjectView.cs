﻿using System;
using System.Collections.Generic;
using NanoLib.Services.InputService;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.UI;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.TrafficSystem.Road;
using NanoReality.GameLogic.Utilites;
using NanoReality.GameLogic.Utilities;
using NanoReality.Isometry;
using UnityEngine;

namespace NanoReality.Engine.BuildingSystem.MapObjects
{
    public class RoadMapObjectView : MapObjectView
    {
        public override bool IgnoreDepthSort => _ignoreDepthSorting;

        private bool _ignoreDepthSorting;

        private RoadTileState _roadTileState;

        /// <summary>
        /// Текущее состояние тайла дороги
        /// </summary>
        public RoadTileState RoadTileState
        {
            get => _roadTileState;
            set
            {
                _roadTileState = value;
                UpdateStateMaterial();
            }
        }

        public MeshFilter MeshFilter => _roadTileView.MeshFilter;

        public Material NormalMaterial;
		public Material NewMaterial;
		public Material DeleteMaterial;

        public RoadTileView RoadTileView => _roadTileView;

        [SerializeField]
        private RoadTileView _roadTileView;
        
        public readonly Dictionary<Directions, RoadMapObjectView> Adjacent = new Dictionary<Directions, RoadMapObjectView>(new DirectionsComparer());

        public int AdjacentCrossroadsCount
        {
            get
            {
                int count = 0;
                foreach (var pair in Adjacent)
                {
                    if (pair.Value.RoadTileType.IsCrossroads())
                    {
                        count++;
                    }
                }
                return count;
            }
        }

        public bool HasAdjacentCrossroads => AdjacentCrossroadsCount > 0;

        public bool HasAdjacentProps => false;

        public override bool IsMovable => false;

        public RoadTileType RoadTileType { get; private set; }

        public RoadMarkingsType RoadMarkingsType { get; private set; }

        public bool IsViewUpdated { get; private set; }

        [Inject] public IWorldSpaceCanvas WorldSpaceCanvas { get; set; }
        [Inject] public SignalOnMapObjectViewTap jSignalOnMapObjectViewTap { get; set; }
        [Inject] public TileOfRoadEditedSignal jTileOfRoadEdited { get; set; }
        
        #region Overrides of MapObjectView

        public override void InitializeView(IMapObject model)
        {
            _ignoreDepthSorting = true;

            base.InitializeView(model);
        }

        protected override void AddListeners()
        {
            base.AddListeners();

            jSignalOnMapObjectViewTap.AddListener(OnTouchMapObjectView);
        }

        protected override void RemoveListeners()
        {
            base.RemoveListeners();

            jSignalOnMapObjectViewTap.RemoveListener(OnTouchMapObjectView);
        }

        protected override void OnPostConstruct()
        {
            transform.localScale = Vector3.one * jCityMapSettings.RoadViewScale;
        }

        #endregion

        public override void OnMapObjectViewTap()
        {
            
        }

        private void OnTouchMapObjectView(MapObjectView view)
        {
            if (view == this)
            {
                var terrainRoadFx = WorldSpaceCanvas.GetTerrainRoadFxItem();
                if (terrainRoadFx != null)
                {

                    var bounds = MeshFilter.mesh.bounds;
                    var position = CacheMonobehavior.CachedTransform.TransformPoint(bounds.center + bounds.extents);
                    terrainRoadFx.SetPosition(position);                    
                    terrainRoadFx.Play();
                }
            }
        }

        protected override List<string> GetComponentsNames ()
        {
            var result = new List<string>()
            {
                "SelectComponent",
                "DragComponent",
                //"StaticAnimationsComponent",
                //"ConstractAnimationsComponent",
                //"ObjectSemiTransparentComponent",
                //"MotionArrowsComponent",
                //"ProgressBarComponent",
                "VFXComponent",
                //"BuildingAttentionComponent",
                //"ColorHighlightComponent"
            };
            return result;
        }

        private void SetStateMaterial(Material material)
        {
            _roadTileView.Renderer.material = material;
        }

        private void UpdateStateMaterial()
        {
            switch (RoadTileState)
            {
                case RoadTileState.New:
                case RoadTileState.Head:
                    SetStateMaterial(NewMaterial);
                    break;
                case RoadTileState.Normal:
                    SetStateMaterial(NormalMaterial);
                    break;
                case RoadTileState.Delete:
                    SetStateMaterial(DeleteMaterial);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        
        protected override void SetMapObjectGraphics()
        {         
			UpdateStateMaterial();
        }

        // флаг для фиксации первого выделенного тайла дороги. Используется для запуска рекурсивного выделения участка дороги
		private static bool firstSelectedTile;

        /// <summary>
        /// Используется для запуска выделения сегмента дороги
        /// </summary>
        public List<RoadMapObjectView> SelectRoadSegment()
        {
            var selection = new List<RoadMapObjectView>();
            SelectMapObject(selection);
            return selection;
        }
	     

        /// <summary>
        /// Выделяет тайл дороги
        /// </summary>
        /// <param name="selection">Список всех выделенных тайлов дороги</param>
        protected void SelectMapObject(List<RoadMapObjectView> selection)
        {
            // can't select road in non building mode
            /*if(!ConstructionController.IsRoadBuildingMode) return;*/
            
            jTileOfRoadEdited.Dispatch();
            // точку входа магистрали и мост нельзя выделить
            switch ((RoadLevelType)ModelLevel)
            {
                case RoadLevelType.EnterPoint:
                case RoadLevelType.Bridge:
                case RoadLevelType.HardRoad:
                case RoadLevelType.OrderDeskCrossExit:
                case RoadLevelType.OrderDeskExitToFakeMainRoad:
                    return;
                case RoadLevelType.Simple:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

			if (!SelectComponent.IsSelected)
            {
				
                // если это новое выделение участка дороги
                if (!firstSelectedTile)
                {
                    firstSelectedTile = true;
                    NextSelection(selection);   // запускаем рекурсивное выделение
                    return;
                }

				SelectComponent.ChangeSelect (true);
                selection.Add(this);

                if (ConstructionController.IsBuildModeEnabled)
                {
                    SetStateMaterial(DeleteMaterial);
                }
               
            }
        }

		// рекурсивное выделение участка дороги
		private void NextSelection(List<RoadMapObjectView> selection)
		{
			if (SelectComponent.IsSelected) return;

			SelectMapObject(selection); // выделяем тайл
            

			if (RoadTileType == RoadTileType.Intersection)
			{
				return;
			}
			switch ((RoadLevelType)VisualLevel)
			{
				case RoadLevelType.HardRoad:
				case RoadLevelType.OrderDeskCrossExit:
				case RoadLevelType.OrderDeskExitToFakeMainRoad:
					return;
			}
            
		    foreach (var tile in Adjacent)
            {
				// получаем следующий тайл дороги
                var road = tile.Value;

				// если текущий тайл не тупик
				if (RoadTileType != RoadTileType.Deadlock_LB &&
					RoadTileType != RoadTileType.Deadlock_RB &&
					RoadTileType != RoadTileType.Deadlock_LT &&
					RoadTileType != RoadTileType.Deadlock_RT)
				{   // и следующий тайл не тупик - прерываем выделение
					if (road.RoadTileType != RoadTileType &&
						road.RoadTileType != RoadTileType.Deadlock_LB &&
						road.RoadTileType != RoadTileType.Deadlock_RB &&
						road.RoadTileType != RoadTileType.Deadlock_LT &&
						road.RoadTileType != RoadTileType.Deadlock_RT)
						continue;
				} // если текущий тайл тупик, а следующий тайл это не тупик
				else if (road.RoadTileType != RoadTileType.Deadlock_LB &&
					road.RoadTileType != RoadTileType.Deadlock_RB &&
					road.RoadTileType != RoadTileType.Deadlock_LT &&
					road.RoadTileType != RoadTileType.Deadlock_RT)
				{ // и следующий тайл не является прямым участком дороги (т.е. является поворотом/перекрестком) - прерываем выделение
					if (road.RoadTileType != RoadTileType.Straight_LB_RT && road.RoadTileType != RoadTileType.Straight_LT_RB)
						continue;
				}
				road.NextSelection(selection); // продолжаем выделение
			}
		}
			
		
        public void Deselect()
        {
            RoadTileState = RoadTileState.Normal;
            firstSelectedTile = false;
			SelectComponent.ChangeSelect (false);
        }


        public void ShowRoadEnd(IWorldSpaceCanvas jWorldCanvas, bool isVisible, bool isHead, RoadTileState state)
        {
            var end = jWorldCanvas.GetEndRoadTile(isHead);

            if (isVisible)
            {
                end.SetTarget(this);
                end.gameObject.SetActive(true);
                RoadTileState = state == RoadTileState.Head ? RoadTileState.New : state;
            }
            else
            {
                end.gameObject.SetActive(false);
                RoadTileState = state;
            }
        }

        private RoadTileType GetRoadTileType()
        {
            switch ((RoadLevelType) ModelLevel)
            {
                case RoadLevelType.EnterPoint:
                case RoadLevelType.Bridge:
                    return RoadTileType.CityEnter;

                case RoadLevelType.OrderDeskCrossExit:
                    return RoadTileType.Tside_RT_LT_RB;

                case RoadLevelType.OrderDeskExitToFakeMainRoad:
                    return RoadTileType.Straight_LT_RB;
            }

            // если текущий участок дороги это точка входа магистрали в город или мост, то это всегда перекресток
            
            bool isLeftBottom = Adjacent.ContainsKey(Directions.LeftBottom);
            bool isLeftTop = Adjacent.ContainsKey(Directions.LeftTop);
            bool isRightBottom = Adjacent.ContainsKey(Directions.RightBottom);
            bool isRightTop = Adjacent.ContainsKey(Directions.RightTop);

            //******************************* STRAIGHT ********************************
            if ((isLeftBottom && isRightTop) && (!isLeftTop && !isRightBottom))
                return RoadTileType.Straight_LB_RT;

            if ((!isLeftBottom && !isRightTop) && (isLeftTop && isRightBottom))
                return RoadTileType.Straight_LT_RB;
            //*************************************************************************

            if (isLeftBottom && isLeftTop && isRightBottom && isRightTop)
                return RoadTileType.Intersection;

            //******************************* DEADLOCKS ********************************
            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            if (isLeftBottom && !isLeftTop && !isRightBottom && !isRightTop)
            {
                return RoadTileType.Deadlock_LB;
            }

            if (!isLeftBottom && isLeftTop && !isRightBottom && !isRightTop)
            {
                return RoadTileType.Deadlock_LT;
            }

            if (!isLeftBottom && !isLeftTop && isRightBottom && !isRightTop)
            {
                return RoadTileType.Deadlock_RB;
            }

            if (!isLeftBottom && !isLeftTop && !isRightBottom && isRightTop)
            {
                return RoadTileType.Deadlock_RT;
            }
            //******************************* DEADLOCKS ********************************

            //******************************* TURNS **********************************

            if (isLeftBottom && isLeftTop && !isRightBottom && !isRightTop)
            {
                return RoadTileType.Turn_LB_LT;
            }


            if (isLeftBottom && !isLeftTop && isRightBottom && !isRightTop)
            {
                return RoadTileType.Turn_LB_RB;
            }


            if (!isLeftBottom && isLeftTop && !isRightBottom && isRightTop)
            {
                return RoadTileType.Turn_LT_RT;
            }


            if (!isLeftBottom && !isLeftTop && isRightBottom && isRightTop)
            {
                return RoadTileType.Turn_RB_RT;
            }

            //******************************* TURNS **********************************


            //******************************* T-SIDE *********************************

            if (isLeftBottom && isLeftTop && !isRightBottom && isRightTop)
            {
                return RoadTileType.Tside_LT_LB_RT;
            }

            if (isLeftBottom && isLeftTop && isRightBottom && !isRightTop)
            {
                return RoadTileType.Tside_LB_LT_RB;
            }

            if (isLeftBottom && !isLeftTop && isRightBottom && isRightTop)
            {
                return RoadTileType.Tside_RB_LB_RT;
            }

            if (!isLeftBottom && isLeftTop && isRightBottom && isRightTop)
            {
                return RoadTileType.Tside_RT_LT_RB;
            }
            //******************************* T-SIDE *********************************

            return RoadTileType.Straight_LB_RT;
        }

        private void UpdateRoadTileAndPropsView()
        {
            _roadTileView.UpdateRoadTile(this);
            
            UpdateStateMaterial();
        }

        private void UpdateRoadTileView()
        {
            _roadTileView.UpdateRoadTile(this);

            UpdateStateMaterial();
        }

        public void UpdateRoadTileType(RoadMapObjectView[] adjacent)
        {
            Adjacent.Clear();
            
            for (int i = 0; i < adjacent.Length; i++)
            {
                if (adjacent[i] != null)
                {
                    var direction = (Directions) i;
                    Adjacent.Add(direction, adjacent[i]);
                }
            }

            RoadTileType = GetRoadTileType();

            ResetView();
        }

        public void ResetView()
        {
            Depth = 0f;

            _ignoreDepthSorting = true;

            RoadMarkingsType =
                RoadTileType.IsCrossroads()
                    ? RoadMarkingsType.Zebra
                    : RoadMarkingsType.YellowDashLine;

            IsViewUpdated = false;
        }

        public void UpdateRoadView(bool isUnderConstruction = false)
        {
            if (isUnderConstruction)
            {
                UpdateRoadTileView();
            }
            else
            {
                RoadMarkingsType = RoadMarkingsUtils.GetRoadTileMarkings(this);

                UpdateRoadTileAndPropsView();

                _ignoreDepthSorting = false;
            }

            IsViewUpdated = true;
        }

        public override void SortObject()
        {
			var height = 0f;
            var izoPosition = new Vector3(WorldSpacePosition.x, 0, WorldSpacePosition.y);
            var depthOffset = MapObjectGeometry.GetHeightOffset(height, jGameCameraModel.AngleDegrees);
            izoPosition.x -= depthOffset;
            izoPosition.z -= depthOffset;
            izoPosition.y = height;
            
            CacheMonobehavior.CachedTransform.eulerAngles = Vector3.zero;
            CacheMonobehavior.CachedTransform.localPosition = izoPosition;
        }

        protected override void OnSelectChanged(bool isActive)
        {
            // avoiding unnecessary sorting
            //base.OnSelectChanged(isActive);
        }
        
        protected override void UpdateIsometry()
        {
            IsoPosition = (Vector2)GridPosition;

            // set size z bigger to compensate for height of road props 
            IsoSize = ((Vector2)GridDimensions).ToVector3(GridDimensions.x * 1.5f);

            IsoBox = new IsoBox(IsoPosition, IsoSize);
            IsoHexagon = new IsoHexagon(IsoPosition, IsoSize);
        }

        public RoadFinishedEffect RoadFinishedEffect { get; set; }

		public void ShowRoadDust()
		{
			RoadFinishedEffect = WorldSpaceCanvas.GetRoadFinishedItem();

			if (RoadFinishedEffect != null)
			{
				RoadFinishedEffect.SetTarget(this);
				RoadFinishedEffect.Play();
			}
		}
        
    }

    public enum Directions
    {
        LeftTop,LeftBottom,RightTop,RightBottom
    }

    /// <summary>
    /// Special comparer for Directions enum. This will avoid all the casting when working with dictionaries.
    /// See: https://stackoverflow.com/questions/26280788/dictionary-enum-key-performance
    /// </summary>
    public struct DirectionsComparer : IEqualityComparer<Directions>
    {
        public bool Equals(Directions x, Directions y)
        {
            return x == y;
        }

        public int GetHashCode(Directions obj)
        {
            return (int)obj;
        }
    }

    /// <summary>
    /// Текущее состояние тайла дороги
    /// Используется для назначения материала при окраске дороги
    /// </summary>
    public enum RoadTileState
    {
        /// <summary>
        /// Обычное состояние (дорога уже полностью построена)
        /// </summary>
        Normal,

        /// <summary>
        /// Новый тайл дороги (дорога намечена для постройки)
        /// </summary>
        New,

        /// <summary>
        /// Тайл дороги отмечен для удаления
        /// </summary>
        Delete,

        /// <summary>
        /// Тайл дороги является последним добавленным (используется для подсветки концов ново создаваемой дороги)
        /// </summary>
        Head
    }
}
