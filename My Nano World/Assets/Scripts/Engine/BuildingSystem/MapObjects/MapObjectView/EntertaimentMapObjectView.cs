﻿using System.Collections.Generic;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace NanoReality.Engine.BuildingSystem.MapObjects
{
	public class EntertaimentMapObjectView : ProduceHappinessMapObjectView, INeedPowerMapObjectView
    {
	    public override MapObjectintTypes MapObjectIntType => MapObjectintTypes.EntertainmentBuilding;
	    
	    protected override List<string> GetComponentsNames()
	    {
		    var result = base.GetComponentsNames();
		    result.Add("DwellingObjectAnimationComponent");
		    return result;
	    }
    }
}
