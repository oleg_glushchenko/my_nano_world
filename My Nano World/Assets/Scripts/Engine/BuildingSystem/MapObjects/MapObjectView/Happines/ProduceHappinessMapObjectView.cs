﻿using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.Happiness;
using System.Collections.Generic;
using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.BuildingSystem.MapObjects.Components;
using NanoReality.Engine.BuildingSystem.MapObjects.PowerPlants;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using UnityEngine;

namespace Assets.Scripts.Engine.BuildingSystem.MapObjects
{
    public class ProduceHappinessMapObjectView : AMapBuildingView, IAoeBuildingView
    {
        [SerializeField] private AreaOfEffectView _happinessAoe;

        public AreaOfEffectView AoeView => _happinessAoe;

        private IBuildingProduceHappiness BuildingProduceHappinessModel => MapObjectModel as IBuildingProduceHappiness;
        public AoeHighlightComponent HighlighterComponent => GetCachedComponent<AoeHighlightComponent>();
        
        [Inject] public SignalOnMapObjectViewMoved jOnMapObjectMovedSignal { get; set; }

        protected override List<string> GetComponentsNames()
        {
            var result = base.GetComponentsNames();
            result.Add("AoeHighlightComponent");
            return result;
        }

        public override void InitializeView(IMapObject model)
        {
            base.InitializeView(model);
            
            IBuildingProduceHappiness buildingModel = BuildingProduceHappinessModel;
            
            _happinessAoe.InitializeArea(buildingModel);
            _happinessAoe.SetActiveArea(false, false);
        }

        protected override void AddListeners()
        {
            base.AddListeners();
            var pp = (IBuildingProduceHappiness)MapObjectModel;
            pp.AoeLayoutChanged += OnAoeLayoutChanged;
        }

        protected override void RemoveListeners()
        {
            base.RemoveListeners();
            var pp = (IBuildingProduceHappiness) MapObjectModel;
            pp.AoeLayoutChanged -= OnAoeLayoutChanged;
        }

        protected override void OnBuildingTap()
        {
            base.OnBuildingTap();
            jShowWindowSignal.Dispatch(typeof(EntertaimentPanelView), MapBuildingModel);
        }

        private void OnAoeLayoutChanged(IAoeBuilding buildingProduceHappiness)
        {
            _happinessAoe.ChangeLayout(buildingProduceHappiness.AoeLayoutType);
            jOnMapObjectMovedSignal.Dispatch(this);
        }
    }
}