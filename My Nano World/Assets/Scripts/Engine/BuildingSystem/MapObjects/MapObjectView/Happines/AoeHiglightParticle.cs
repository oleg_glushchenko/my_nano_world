﻿using Assets.NanoLib.Utilities.Pulls;
using NanoReality.Engine.UI.Components;

namespace Assets.Scripts.Engine.BuildingSystem.MapObjects.Components
{
    /// <summary>
    /// Эффект партиклов когда здание подсвечивается в AOE
    /// </summary>
    public class AoeHiglightParticle : GameObjectPullable
    {

        private MapObjectLooker _looker;
        private void Awake()
        {
            _looker = GetComponent<MapObjectLooker>();
        }


        public void SetLooker(MapObjectView target)
        {
            _looker.SetTarget(target);
        }

    }
}
