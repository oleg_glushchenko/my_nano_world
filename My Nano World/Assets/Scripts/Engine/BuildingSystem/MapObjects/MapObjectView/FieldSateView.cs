﻿using System;
using System.Collections.Generic;
using System.Linq;
using NanoReality.GameLogic.BuildingSystem.MapObjects;
using UnityEngine;

namespace NanoReality.Engine.BuildingSystem.MapObjects.AgroField
{
    public class FieldSateView : MonoBehaviour
    {
        [SerializeField] private AgriculturalFieldState _state;
        [SerializeField] private List<AgroFieldState> _newCropsRenderers;

        public AgriculturalFieldState State => _state;

        private void Awake()
        {
            if (_newCropsRenderers == null && _newCropsRenderers.Any(c => c == null))
            {
                Debug.LogError("Renderers is null. Try to find in children");
                _newCropsRenderers.Clear();
                _newCropsRenderers.AddRange(gameObject.GetComponentsInChildren<AgroFieldState>());
            }
        }

        public void ShowState(int productId)
        {
            for (int i = 0; i < _newCropsRenderers.Count; i++)
            {
                _newCropsRenderers[i].Renderer.enabled = _newCropsRenderers[i].ProductId == productId;
            }
        }

        public void Hide()
        {
            SetRenderersActive(false);
        }

        private void SetRenderersActive(bool isActive)
        {
            if (_newCropsRenderers == null)
            {
                Debug.LogError("Crop Renderers is null");
                return;
            }

            for (int i = 0; i < _newCropsRenderers.Count; i++)
            {
                var renderer = _newCropsRenderers[i].Renderer;
                if (renderer == null)
                {
                    Debug.LogError("renderer is null index " + i);
                    continue;
                }
                renderer.enabled = isActive;
            }
        }

        [Serializable]
        public class ProductsCropsObjects
        {
            public int ProductId;
            public Renderer ProductFieldRenderer;
        }
    }
}