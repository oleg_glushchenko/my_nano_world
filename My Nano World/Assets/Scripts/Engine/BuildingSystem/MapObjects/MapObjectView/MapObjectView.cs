using System;
using System.Collections.Generic;
using System.Linq;
using NanoLib.Core.Services.Sound;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UtilitesEngine;
using Assets.NanoLib.UtilitesEngine.CustomDebugging.Models.api;
using Assets.NanoLib.Utilities;
using Assets.NanoLib.Utilities.Pulls;
using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.Utilities;
using strange.extensions.mediation.impl;
using UnityEngine;
using Assets.Scripts.Engine.BuildingSystem.MapObjects.Components;
using DG.Tweening;
using NanoReality.Core.Resources;
using NanoReality.Engine.BuildingSystem.CityCamera;
using NanoReality.Engine.BuildingSystem.MapObjects.Components;
using NanoReality.Engine.UI;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.Game.Tutorial;
using NanoReality.GameLogic.Utilites;
using NanoReality.Isometry;
using strange.extensions.injector.api;
using NanoReality.GameLogic.StartApplicationSequence.Controllers.Signals;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.Settings.BuildSettings.Models.Api;
using UnityEngine.Serialization;

namespace Assets.Scripts.Engine.BuildingSystem.MapObjects
{
	public class MapObjectView : View, IPullableObject, IIsometryObject, IMapObjectView
    {
	    private const int MaxDepthOffsetWhenMoving = 10;

	    public bool Interactable { get; set; } = true;

	    #region InspectorFileds

	    /// <summary>
	    /// Id здания - уникально для каждого типа объекта
	    /// </summary>
	    [SerializeField] public Id_IMapObjectType ObjectTypeId;

	    /// <summary>
	    /// Уровень объекта
	    /// </summary>
	    [FormerlySerializedAs("VisualLevel")]
	    [FormerlySerializedAs("Level")] 
	    [SerializeField] private int _visualLevel;

	    [SerializeField] private Vector2Int _cityGridPosition;

	    [SerializeField] private Vector2 _worldSpacePosition;

	    [SerializeField] private SpriteRenderer _spriteRenderer;

	    [SerializeField] private List<SpriteRenderer> _animatedSprites;
	    [SerializeField] private List<ParticleSystemRenderer> _vfxParticles;
	    [SerializeField] private Animator _animator;
	    [SerializeField] private float _customScale = 1.0f;
	    
		public Vector2Int GridDimensions { get; protected set; }

	    /// <summary>
	    /// If true then height value is read from CustomHeaigh, otherwise object's height is considered Dimentions.X
	    /// </summary>
	    [Tooltip("If true, IsoSize gets CustomHeight otherwise Dimentions.x")]
	    public bool UseCustomHeight;
	    
	    [Range(0f, 10f)]
	    public float CustomHeight;

	    public float Depth;

		#endregion

		public CacheMonobehavior CacheMonobehavior => _cacheMonobehavior ?? (_cacheMonobehavior = new CacheMonobehavior(this));

		private CacheMonobehavior _cacheMonobehavior;
       
		#region IMapObjectView implementation

		public Transform Transform => transform;
        
		public SpriteRenderer SpriteRenderer
		{
			get => _spriteRenderer;
			set => _spriteRenderer = value;
		}


		public int VisualLevel
		{
			get => _visualLevel;
			set => _visualLevel = value;
		}

		public Id_IMapObject MapId => MapObjectModel?.MapID;
		public MapObjectId VisualId => new MapObjectId(ObjectTypeId, VisualLevel);

		#endregion
		
        #region IIsometryObject

        public Vector3 IsoPosition { get; protected set; }
        public Vector3 IsoSize { get; protected set; }
        public IsoBox IsoBox { get; protected set; }
        public IsoHexagon IsoHexagon { get; protected set; }

        public float IsoDepth
        {
            get => Depth;
            set => Depth = value;
        }

        public float CustomScale => _customScale;

        public bool IsVisited { get; set; }
        public List<IIsometryObject> ObjectsBehind { get; private set; }

        /// <summary>
        /// Должен ли объект игнорировать алогоритм сортировки глубины
        /// ignore depth sort.
        /// </summary>
        public virtual bool IgnoreDepthSort => _ignoreDepthSort;

        private bool _ignoreDepthSort;

        #endregion
        
        private int _modelLevel;

        public int ModelLevel
        {
	        get => _modelLevel;
	        protected set => _modelLevel = value;
        }

        /// <summary>
        /// Local transform position as a map child.
        /// </summary>
        public Vector2Int GridPosition
        {
	        get => _cityGridPosition;
	        set => _cityGridPosition = value;
        }

        /// <summary>
        /// Object position in CityGrid.
        /// </summary>
        public Vector2 WorldSpacePosition
        {
	        get => _worldSpacePosition;
	        set => _worldSpacePosition = value;
        }
        
        public Vector2 WorldSpaceDimensions => jCityMapSettings.GridCellScale * (Vector2)GridDimensions;

		public IMapObject MapObjectModel { get; protected set; }

		/// <summary>
		/// Is object movable by user
		/// </summary>
	    public virtual bool IsMovable => true;

		/// <summary>
        /// If true, keeps construct animation as visual after build finished
        /// </summary>
		public virtual bool KeepConstructAnimation => false;

		public MeshCollider MeshCollider => _meshCollider;

		public Vector3 WorldPosition => CacheMonobehavior.CachedTransform.position;

		public List<SpriteRenderer> AnimatedSprites => _animatedSprites;
		
		public List<ParticleSystemRenderer> VfxParticles => _vfxParticles;

		public Animator Animator => _animator;

		private Tweener _scaleTweener;

		#region Injects

		[Inject] public UserLevelUpSignal jUserLevelUpSignal { get; set; }
		[Inject] public IInjectionBinder Binder { get; set; }
		[Inject] public IConstructionController ConstructionController { get; set; }
		[Inject] public IGameCamera jGameCamera { get; set; }
		[Inject] public IGameCameraModel jGameCameraModel { get; set; }
		[Inject] public IGameManager jGameManager { get; set; }
		[Inject] public ISoundManager jSoundManager { get; set; }
		[Inject] public IHardTutorial jHardTutorial { get; set; }
		[Inject] public IWorldSpaceCanvas jWorldSpaceCanvas { get; set; }
		[Inject] public IConstructionController jConstructionController { get; set; }
		[Inject] public IPlayerProfile jPlayerProfile { get; set; }
		[Inject] public ICityMapSettings jCityMapSettings { get; set; }
		[Inject] public IResourcesManager jResourcesManager { get; set; }

		#region Signals


		[Inject] public PostLoadingActionsCompleteSignal jPostLoadingActionsCompleteSignal { get; set; }

	    [Inject]
	    public SignalOnBuildingRepaired jSignalOnBuildingRepaired { get; set; }

	    #endregion

        #endregion

        #region Components Properties
        
        public MapObjectSelect SelectComponent => GetCachedComponent<MapObjectSelect> ();
        
		public MapObjectDrag DragComponent => GetCachedComponent<MapObjectDrag> ();
        
		public MapObjectStaticAnimations StaticAnimationsComponent => GetCachedComponent<MapObjectStaticAnimations> ();

		public ConstructionComponent ConstructionComponent => GetCachedComponent<ConstructionComponent> ();
        
		public MapObjectSemiTransparentComponent SemiTransparentComponent => GetCachedComponent<MapObjectSemiTransparentComponent> ();
        
		public MapObjectProgressBarComponent ProgressBarComponent => GetCachedComponent<MapObjectProgressBarComponent> ();
        
		public MapObjectsVFXComponent VFXComponent => GetCachedComponent<MapObjectsVFXComponent> ();

		public  BuildingAttentionComponent AttentionComponent => GetCachedComponent<BuildingAttentionComponent> ();

		protected Dictionary<Type,IMapObjectViewComponent> Components = new Dictionary<Type,IMapObjectViewComponent>();
        
		#endregion

		#region Sounds to refactor
		
		//TODO deal with build/ugrade sounds
        private static string BuildingUpgradeSoundName => "building_upgrade_loop";
        private static string BuildingBuildSoundName => "building_construction";
		private MeshCollider _meshCollider;
		private MapObjectId _visualId;

		#endregion

        protected override void Awake()
        {
			if (autoRegisterWithContext && !registeredWithContext)
				bubbleToContext(this, true, true);
			InjectComponents ();
            ObjectsBehind = new List<IIsometryObject>();
            UpdateIsometry();

            foreach (SpriteRenderer childRenderer in GetComponentsInChildren<SpriteRenderer>(true))
            {
	            if (childRenderer != SpriteRenderer && childRenderer.gameObject.layer == LayerMask.NameToLayer("Buildings"))
	            {
		            _animatedSprites.Add(childRenderer);
	            }
            }
            
            foreach (ParticleSystemRenderer particleSystem in GetComponentsInChildren<ParticleSystemRenderer>(true))
            {
	            if (particleSystem.gameObject.layer == LayerMask.NameToLayer("Buildings"))
	            {
		            _vfxParticles.Add(particleSystem);
	            }
            }

            _animator = GetComponent<Animator>();
        }

        #region Components

        protected virtual List<string> GetComponentsNames()
		{
			var result = new List<string> () 
			{
				"SelectComponent",
				"DragComponent",
				"StaticAnimationsComponent",
				"ConstractAnimationsComponent",
				"ObjectSemiTransparentComponent",
				"MotionArrowsComponent",
                "ConstructPlaceIndicatorComponent",
				"ProgressBarComponent",
				"VFXComponent",
				"BuildingAttentionComponent",
			    "ColorHighlightComponent",
			    "DefaultLongTapComponent"
			};
			return result;
		}

		private void InjectComponents()
		{
			var componentsNames = GetComponentsNames ();
		    if (componentsNames == null)
		    {
		        return;
		    }

			foreach (var componentName in componentsNames)
			{
				var component = Binder.GetInstance<IMapObjectViewComponent> (componentName);

				var type = component.GetType();

				if (Components.ContainsKey(type))
				{
					Debug.LogError("MapObject: " + name + " already has component of type: " + componentName);
				}
				else
				{
					Components.Add (component.GetType(), component);
				}
			}
		}

		public T GetCachedComponent<T>() where T : class, IMapObjectViewComponent
		{
			Type targetType = typeof(T);
			if (!Components.TryGetValue(targetType, out IMapObjectViewComponent componentInstance))
			{
				//Debug.LogWarning($"Component of type [{targetType.Name}] not found in building [{GetType().Name}] MapId [{MapId}]", gameObject);
				return null;
			}

			return (T) componentInstance;
		}
		
		#endregion

        #region Pullable object
        
        public IObjectsPull pullSource { get; set; }
        public virtual object Clone()
        {
            return Instantiate(this);
        }
        
        public virtual void OnPop()
        {
	        gameObject.SetActive(true);
	        SetColliderActive(true);
	        Interactable = true;
        }

		public virtual void OnPush()
		{
			gameObject.SetActive(false);
			SetColliderActive(false);
			Interactable = false;
		}

        /// <summary>
		/// Calls when pull must be cleaned up
		/// </summary>
		public virtual void DestroyObject()
		{
			Destroy(gameObject);
		}

		#endregion

        protected virtual void AddListeners() { }

        protected virtual void RemoveListeners() { }
        
		#region Initalize/Dispose implementation

		[PostConstruct]
		public void PostConstruct()
		{
			OnPostConstruct();
		}
        
		protected virtual void OnPostConstruct()
		{
			
		}
		
		private void InitializeComponents()
		{
			foreach (var componentKeyValue in Components)
			{
				componentKeyValue.Value?.InitializeComponent(this);
			}
			
			OnComponentsInitialize();
		}

		private void DisposeComponents()
		{
			foreach (var componentKeyValuePair in Components)
			{
				componentKeyValuePair.Value?.DisposeComponent();
			}
			
			OnComponentsDispose();
		}
		
		public T GetViewComponent<T>() where T : IMapObjectViewComponent
		{
			var type = typeof(T);
			var component = Components.FirstOrDefault(c => c.Key == type).Value;
			return (T)component;
		}

		protected virtual void OnComponentsInitialize()
		{
			SelectComponent?.SignalOnSelectChanged.AddListener (OnSelectChanged);
		}

		protected virtual void OnComponentsDispose()
		{
			SelectComponent?.SignalOnSelectChanged.RemoveListener(OnSelectChanged);
		}

        #endregion

        /// <summary>
        /// Called when this object is tapped on map
        /// </summary>
        public virtual void OnMapObjectViewTap()
	    {
		    PlayScaleTap();
	    }

	    private void PlayScaleTap()
	    {
		    if (!jConstructionController.IsCurrentlySelectObject)
		    {
			    Vector3 punchScale = 0.1f * CacheMonobehavior.CachedTransform.localScale;

		        const float tweenDuration = 0.4f;
		        const int vibrato = 5;
		        const float elasticity = 0.5f;

		        _scaleTweener?.Kill(true);

                _scaleTweener = CacheMonobehavior.CachedTransform.DOPunchScale(punchScale, tweenDuration, vibrato, elasticity)
		            .OnComplete(StopScaleTap);
		    }
	    }

        private void StopScaleTap()
        {
	        _scaleTweener?.Kill(true);
        }

		#region position

		public void SetGridPosition(Vector2Int gridPosition)
		{
			GridPosition = gridPosition;
			SetWorldSpacePosition(CalculateWorldSpacePosition());
		}

		private Vector3 CalculateWorldSpacePosition()
		{
			return jCityMapSettings.GridCellScale * (Vector2)GridPosition;
		}
        
        public void SetWorldSpacePosition(Vector2 position)
        {
	        WorldSpacePosition = position;
            UpdateIsometry();
        }

        protected virtual void UpdateIsometry()
        {
            //convert to Vector3 with z = 0
            IsoPosition = (Vector2)GridPosition;

            IsoSize = ((Vector2)GridDimensions).ToVector3(UseCustomHeight ? CustomHeight : GridDimensions.x);
            
            IsoBox = new IsoBox(IsoPosition, IsoSize);
            IsoHexagon = new IsoHexagon(IsoPosition, IsoSize);
        }

        /// <summary>
        /// Сортировка здания по глубине
        /// </summary>
        public virtual void SortObject()
        {
            Vector3 izoPosition = new Vector3(WorldSpacePosition.x, 0, WorldSpacePosition.y);

            var rotation = new Vector3(jGameCameraModel.AngleDegrees, 45, 0);
            CacheMonobehavior.CachedTransform.rotation = Quaternion.Euler(rotation);
            CacheMonobehavior.CachedTransform.localPosition = izoPosition;

            var defaultBuildingsShift = 2f;
            var buildingIsoSizeShift = defaultBuildingsShift - ((Vector2)IsoSize).magnitude*0.5f;
            if (IsoSize.x == 0 || IsoSize.y == 0)
            {
	            buildingIsoSizeShift = 0f;
            }
            var selectionShift = _ignoreDepthSort ? 100f : buildingIsoSizeShift;
            CacheMonobehavior.CachedTransform.position = MapObjectGeometry.PlaceObjectInDepth(CacheMonobehavior.CachedTransform.position, selectionShift);
           
        }

        /// <summary>
        /// Коллбек изменения выделения здания в режиме конструктора
        /// </summary>
        protected virtual void OnSelectChanged(bool isActive)
        {
            var cityMapView = jGameManager.GetMapView(MapObjectModel.SectorId);
            if (isActive)
            {
                //ignore depth sort when object is selected
                _ignoreDepthSort = true;
                //draw object over all other objects on map
                Depth = cityMapView.CurrentMaxDepth + MaxDepthOffsetWhenMoving;
                SortObject();
            }
            else
            {
                //restore object depth
                _ignoreDepthSort = false;
                cityMapView.SortDepthObjects();
            }
		}
		#endregion


		#region Graphics

		/// <summary>
		/// Устанавливает спрайт, меш и материал здания 
		/// </summary>
		protected virtual void SetMapObjectGraphics()
		{
		}
		
        #endregion

        public void SetColliderActive(bool isEnabled)
        {
	        MeshCollider col = MeshCollider;
	        if (col != null)
	        {
		        col.enabled = isEnabled;
	        }
        }

		#region signal callbacks
		
		public virtual void InitializeView(IMapObject model)
		{
			MapObjectModel = model;
			ModelLevel = model.Level;
			GridDimensions = Vector2Int.RoundToInt(model.Dimensions.ToVector2());
            
			SetColliderMesh();
			SetColliderActive(true);
			
			InitializeComponents();
			AddListeners();
			SetMapObjectGraphics();
		}

		public virtual void DisposeView()
		{
			DisposeComponents();
			RemoveListeners();
			FreeObject();
		}
		
		public virtual void FreeObject()
		{
			AttentionComponent?.FreeView(true);
			pullSource.PushInstance (this);
		}

		#endregion

		public void SetColliderMesh()
		{
			if (SpriteRenderer != null && SpriteRenderer.sprite != null)
			{
				if (_meshCollider == null)
				{
					_meshCollider = gameObject.AddComponent<MeshCollider>();
				}
				_meshCollider.sharedMesh = jResourcesManager.GetBuildingSpriteMesh(VisualId);
			}
		}
		
		#if UNITY_EDITOR
	    
		void OnValidate()
		{
			transform.localScale = new Vector3(_customScale, _customScale, _customScale);
		}
		
		#endif
    }

	public interface IMapObjectView
    {
	    Transform Transform { get; }
	    
	    SpriteRenderer SpriteRenderer { get; }
	    
	    List<SpriteRenderer> AnimatedSprites { get; }
	    
	    List<ParticleSystemRenderer> VfxParticles { get; }
	    
	    Animator Animator { get; }

	    void InitializeView(IMapObject model);

	    void DisposeView();
    }
}
