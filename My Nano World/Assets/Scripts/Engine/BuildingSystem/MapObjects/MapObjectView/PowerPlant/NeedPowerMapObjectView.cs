﻿using Assets.Scripts.Engine.BuildingSystem.MapObjects.Components;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Engine.BuildingSystem.MapObjects
{
    public class NeedPowerMapObjectView : AMapBuildingView, INeedPowerMapObjectView
    {
        public Material NeedPowerHighlightingMaterial { get; protected set; }

        protected override List<string> GetComponentsNames()
        {
            var result = base.GetComponentsNames();
            result.Add("PowerNeedHighlightComponent");
            return result;
        }
    }
}