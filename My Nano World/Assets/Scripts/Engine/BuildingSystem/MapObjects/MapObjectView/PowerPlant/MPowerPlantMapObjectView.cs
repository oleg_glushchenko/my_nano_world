﻿using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.BuildingSystem.MapObjects.Components;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using System.Collections.Generic;
using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.BuildingSystem.MapObjects.Components;
using NanoReality.Engine.BuildingSystem.MapObjects.PowerPlants;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using UnityEngine;


public class MPowerPlantMapObjectView : AMapBuildingView, IAoeBuildingView
{
    [SerializeField]
    private AreaOfEffectView _aoe;
    
    public AreaOfEffectView AoeView => _aoe;

    public IPowerPlantBuilding PowerObjectModel => (IPowerPlantBuilding)MapObjectModel;

    public AoeHighlightComponent HighlighterComponent => GetCachedComponent<AoeHighlightComponent>();
    public override MapObjectintTypes MapObjectIntType => MapObjectintTypes.PowerPlant;

    [Inject] public SignalOnMapObjectViewMoved jOnMapObjectMovedSignal { get; set; }

    protected override List<string> GetComponentsNames()
    {
        var result = base.GetComponentsNames();
        result.Add("AoeHighlightComponent");
        return result;
    }
    
    protected override void AddListeners()
    {
        base.AddListeners();
        var pp = (IPowerPlantBuilding)MapObjectModel;
        pp.AoeLayoutChanged += OnAoeLayoutChanged;
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        var pp = (IPowerPlantBuilding) MapObjectModel;
        pp.AoeLayoutChanged -= OnAoeLayoutChanged;
    }

    protected override void OnBuildingTap()
    {
        base.OnBuildingTap();
        jShowWindowSignal.Dispatch(typeof(EnergyPanelView), MapBuildingModel);
    }

    public override void InitializeView(IMapObject model)
    {
        base.InitializeView(model);
        var powerPlant = (IPowerPlantBuilding) model;
        _aoe.InitializeArea(powerPlant);
        _aoe.SetActiveArea(false, false);
        _aoe.transform.localScale /= CustomScale;
    }

    private void OnAoeLayoutChanged(IAoeBuilding aoeBuilding)
    {
        _aoe.ChangeLayout(aoeBuilding.AoeLayoutType);
        jOnMapObjectMovedSignal.Dispatch(this);
    }
}
