﻿using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.Settings.BuildSettings.Models.Api;
using NanoReality.StrangeIoC;
using UnityEngine;

namespace NanoReality.Engine.BuildingSystem.MapObjects.PowerPlants
{
    public class AreaOfEffectView : AViewMediator
    {
        #region Inject

        [Inject] public ICityMapSettings jCityMapSettings { get; set; }

        #endregion

        private SpriteRenderer _borderSprite;
        private AoeLayout _currentLayout;

        private IAoeBuilding AoeBuildingModel { get; set; }

        private Vector2 AreaSize => AoeBuildingModel.AoeSize;

        /// <summary>
        /// Init AoE effect
        /// </summary>
        /// <param name="building">building related to effect</param>
        public void InitializeArea(IAoeBuilding building)
        {
            _borderSprite = GetComponentInChildren<SpriteRenderer>();
            AoeBuildingModel = building;

            UpdateSpriteRenderer();

            // Hack! revert the scale effect BUG NAN-5916 due to terrain resize and BuildSettings.TerrainBuildingViewScaleFactor
            transform.localScale = Vector3.one * (1f / (AoeBuildingModel.Dimensions.X * jCityMapSettings.CellSize));
        }

        /// <summary>
        /// Enable\disable effect
        /// </summary>
        /// <param name="isActive"></param>
        /// <param name="isShowBorder"></param>
        public void SetActiveArea(bool isActive, bool isShowBorder)
        {
            _currentLayout = AoeBuildingModel.AoeLayoutType;
            gameObject.SetActive(isActive);
            //_borderSprite.enabled = isShowBorder;
            if (!isActive) return;

            MoveAreaToLayout();
        }

        public void ChangeLayout(AoeLayout layout)
        {
            if (_currentLayout == layout)
            {
                return;
            }

            _currentLayout = layout;

            MoveAreaToLayout();
        }


        private void MoveAreaToLayout()
        {
            Transform cachedTransform = transform;
            Vector2 offsetWorldSpace = AoeUtilities.GetAoeOffsetPosition(_currentLayout, 
                                           AoeBuildingModel.Dimensions.ToVector2(), AreaSize);
            Vector3 worldSpacePivotOffset = jCityMapSettings.GridCellScale *
                                        new Vector3(offsetWorldSpace.x, 0, offsetWorldSpace.y);
            
            cachedTransform.position = cachedTransform.parent.position + worldSpacePivotOffset;
            
            UpdateSpriteRenderer();
        }
        
        private void UpdateSpriteRenderer()
        {
            Vector2 areaSizeNormal = AreaSize * jCityMapSettings.GridCellScale;
            Vector2 resultSize = areaSizeNormal;
            // We invert area size for this cases for show correct view.
            if (_currentLayout == AoeLayout.TopRight || _currentLayout == AoeLayout.BottomLeft)
            {
                resultSize = new Vector2(areaSizeNormal.y, areaSizeNormal.x);
            }
            
            _borderSprite.size = resultSize;
        }

        protected override void PreRegister() { }

        protected override void OnRegister() { }

        protected override void OnRemove() { }
    }
}