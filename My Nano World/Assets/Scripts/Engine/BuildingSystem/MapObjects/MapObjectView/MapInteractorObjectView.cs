﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Engine.UI.Extensions.TerrainFx;
using NanoLib.Core.Services.Sound;
using NanoReality.Engine.UI;
using NanoReality.GameLogic.Utilities;
using NanoReality.StrangeIoC;
using UnityEngine;

namespace NanoReality.Engine.BuildingSystem.MapObjects
{    
    public class MapInteractorObjectView : AViewMediator
    {
        [SerializeField] private Texture2D _texture;

        [SerializeField] private Transform _targetTransform;

        [SerializeField] [Range(0, 1)] private float _colorThreashold;
        
        [SerializeField] private float _fxDepthOffset=1.2f;

        public List<AreaConfig> _config;

        private Vector3 _globalPoint;
        private Vector2 _size;
        private TerrainEffect TerrainEffect { get; set; }

        [Inject] public IWorldSpaceCanvas jWorldSpaceCanvas { get; set; }
        [Inject] public ISoundManager jSoundManager { get; set; }

        protected override void PreRegister()
        {
            var boxCollider = _targetTransform.GetComponent<BoxCollider>();
            if (boxCollider != null)
            {
                _size = new Vector2(boxCollider.size.x, boxCollider.size.z);
            }
        }

        protected override void OnRegister()
        {
        }

        protected override void OnRemove()
        {
        }

        public void OnInteractorTap(Vector3 globalPoint)
        {
            
            _globalPoint = globalPoint;

            var localPoint = _targetTransform.InverseTransformPoint(globalPoint);

            var halfSizeX = _size.x * 0.5f;
            var halfSizeY = _size.y * 0.5f;

            localPoint.x += halfSizeX;
            localPoint.z += halfSizeY;

            var percentX = localPoint.x / _size.x;
            var percentY = localPoint.z / _size.y;

            int pixelX = (int)(_texture.width * percentX);
            int pixelY = (int)(_texture.height * percentY);

            Color pixel = _texture.GetPixel(pixelX, pixelY);
            var config = GetConfigByColor(pixel);
            
            if (config != null)
            {
                ShowTerrainEffect(config.AreaType);
            }
            
        }

        private AreaConfig GetConfigByColor(Color color)
        {
            return
                _config.FirstOrDefault(
                    areaConfig => CheckColorsIdentity(color, areaConfig.AreaColor, _colorThreashold));
        }

        private bool CheckColorsIdentity(Color colorA, Color colorB, float threashold)
        {            
            var result = Mathf.Abs(colorA.r - colorB.r) < threashold;
            result &= Mathf.Abs(colorA.g - colorB.g) < threashold;
            result &= Mathf.Abs(colorA.b - colorB.b) < threashold;
            result &= Mathf.Abs(colorA.a - colorB.a) < threashold;
            return result;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(_globalPoint, 0.05f);
        }

        public void ShowTerrainEffect(AreaType areaType)
        {
            switch (areaType)
            {
                case AreaType.Ground:
                    TerrainEffect = jWorldSpaceCanvas.GetTerrainGroundFxItem();
                    break;
                case AreaType.Dust:
                    TerrainEffect = jWorldSpaceCanvas.GetTerrainDustFxItem();
                    break;
                case AreaType.Road:
                    TerrainEffect = jWorldSpaceCanvas.GetTerrainRoadFxItem();
                    break;
                case AreaType.Water:
                    TerrainEffect = jWorldSpaceCanvas.GetTerrainWaterFxItem();
                    jSoundManager.Play(SfxSoundTypes.TapWater, SoundChannel.SoundFX);
                break;
                case AreaType.None:
                    return;
                default:
                    return;
            }

            if (TerrainEffect != null)
            {
                TerrainEffect.SetPosition(MapObjectGeometry.PlaceObjectInDepth(_globalPoint,_fxDepthOffset));
                TerrainEffect.Play();
            }
        }        

    }
}
