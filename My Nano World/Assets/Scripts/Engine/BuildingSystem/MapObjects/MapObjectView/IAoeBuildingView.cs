﻿using NanoReality.Engine.BuildingSystem.MapObjects.Components;
using NanoReality.Engine.BuildingSystem.MapObjects.PowerPlants;

namespace Assets.Scripts.Engine.BuildingSystem.MapObjects
{
    public interface IAoeBuildingView
    {
        AoeHighlightComponent HighlighterComponent { get; }
        
        AreaOfEffectView AoeView { get; }
    }
}