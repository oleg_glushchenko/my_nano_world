﻿using Engine.UI;
using GameLogic.Taxes.Service;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.Engine.BuildingSystem.GlobalCityMap;
using NanoReality.Engine.UI.Extensions.MapObjects.CityHallTaxes;
using NanoReality.Game.Attentions;
using NanoReality.GameLogic.Upgrade;
using NanoReality.UI.Components;
using UnityEngine;

namespace NanoReality.Engine.BuildingSystem.MapObjects
{
    public class CityHallMapObjectView : AGlobalCityBuildingView
    {
        #region Injects

        [Inject] public  AddRewardItemSignal jAddRewardItemSignal { get;set; }
        [Inject] public SignalOnTaxesAttentionTap jSignalOnTaxesAttentionTap { get; set; }
        
        [Inject] public ICityTaxesService jCityTaxesService { get; set; }

        #endregion

        private bool _isCollectedFromAttention;
        private Vector3 _attentionViewPosition;

        protected override void AddListeners()
        {
            base.AddListeners();
            
            jCityTaxesService.TaxesCollected += OnTaxesCountCollected;
            jCityTaxesService.TaxesChanged += OnCurrentCoinsChanged;
            jSignalOnTaxesAttentionTap.AddListener(OnAttentionClick);
        }

        protected override void RemoveListeners()
        {
            base.RemoveListeners();
            
            jCityTaxesService.TaxesCollected -= OnTaxesCountCollected;
            jCityTaxesService.TaxesChanged -= OnCurrentCoinsChanged;
            jSignalOnTaxesAttentionTap.RemoveListener(OnAttentionClick);
        }

        protected override void OnBuildingTap()
        {
            jShowWindowSignal.Dispatch(typeof(CityHallTaxesView), MapBuildingModel);
        }

        private void OnTaxesCountCollected(int coins)
        {
            jSignalOnTaxesCollection.Dispatch(coins);
            if (_isCollectedFromAttention)
            {
                _isCollectedFromAttention = false;
                jAddRewardItemSignal.Dispatch(new AddItemSettings( FlyDestinationType.SoftCoins, _attentionViewPosition, coins.ToString()));
            }
            
            ShowIndicators();
        }

        private void OnCurrentCoinsChanged()
        {
            ShowIndicators();
        }

        protected override void OnUpgradeAttentionTap()
        {
            jGameCamera.FocusOnMapObject(MapObjectModel);
            jShowWindowSignal.Dispatch(typeof(UpgradeWithProductsPanelView), MapObjectModel);
        }

        private void OnAttentionClick()
        {
            if(jCityTaxesService.AnyTaxesAccrued)
            {
                _isCollectedFromAttention = true;
                _attentionViewPosition = AttentionComponent.AttentionView.ActievementImage.gameObject.transform.position;
                jCityTaxesService.Collect();
            }
        }
    }
}
