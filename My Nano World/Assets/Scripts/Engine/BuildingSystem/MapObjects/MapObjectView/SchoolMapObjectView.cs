using System.Collections.Generic;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.BuildingSystem.MapObjects.Components;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using UnityEngine;

namespace NanoReality.Engine.BuildingSystem.MapObjects
{
    public class SchoolMapObjectView : ProduceHappinessMapObjectView
    {
        public override MapObjectintTypes MapObjectIntType => MapObjectintTypes.School;

    }
}