﻿using System.Collections.Generic;
using Assets.NanoLib.UI.Core.Signals;
using Assets.Scripts.Engine.UI.Views.Warehouse;
using NanoReality.Engine.BuildingSystem.GlobalCityMap;
using NanoReality.Engine.UI.Extensions.MapObjects.Warehouse;
using strange.extensions.mediation.impl;

namespace Assets.Scripts.Engine.BuildingSystem.MapObjects
{
    public class WarehouseBuildingView : AGlobalCityBuildingView
    {
        [Inject] public ShowWindowSignal ShowWindowSignal { get; set; }

        protected override List<string> GetComponentsNames()
        {
            var list = base.GetComponentsNames();
            list.Remove("UpgradeAbilityComponent");
            return list;
        }

        protected override void OnBuildingTap()
        {
            ShowWindowSignal.Dispatch(typeof(WarehouseView), MapBuildingModel);
        }

        protected override void OnUpgradeAttentionTap()
        {
            jGameCamera.FocusOnMapObject(MapObjectModel);
            
            ShowWindowSignal.Dispatch(typeof(WarehouseView), MapBuildingModel);
        }
    }
}
