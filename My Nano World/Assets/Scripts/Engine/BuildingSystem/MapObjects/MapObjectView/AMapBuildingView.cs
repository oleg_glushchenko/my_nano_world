﻿using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.Analytics;
using NanoReality.Engine.UI.Views.WorldSpaceUI;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.Common.Controllers.Signals;
using Assets.NanoLib.UI.Core.Signals;
using Engine.UI.Extensions.RepairBuildingPanel;
using NanoLib.Core.Services.Sound;
using NanoReality.Game.Attentions;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.Services;
using NanoReality.GameLogic.Upgrade;
using UnityEngine;

namespace Assets.Scripts.Engine.BuildingSystem.MapObjects
{
    public abstract class AMapBuildingView : MapObjectView
    {
        #region Signals

        [Inject] public SignalOnConstructControllerChangeState jSignalOnConstructControllerChangeState { get; set; }

        [Inject] public SignalOnBuildingUpgradeAttentionTap jSignalOnBuildingUpgradeAttentionTap { get; set; }

        [Inject] public SignalOnMovedMapObjectOnServer jSignalOnMovedMapObjectOnServer { get; set; }

        [Inject] public SignalBuildFinishVerificated jSignalBuildFinishVerificated { get; set; }

        [Inject] public SignalOnMapBuildingsUpgradeStarted jSignalOnMapBuildingsUpgradeStarted { get; set; }

        [Inject] public SignalOnMapBuildingUpgradeVerificated jSignalOnMapBuildingUpgradeVerificated { get; set; }

        [Inject] public MapBuildingVerifiedSignal jMapBuildingVerifiedSignal { get; set; }

        [Inject] public SignalOnProductsStateChanged jSignalOnProductsStateChanged { get; set; }

        [Inject] public SignalOnTaxesCollection jSignalOnTaxesCollection { get; set; }

        [Inject] public ShowWindowSignal jShowWindowSignal { get; set; }
        
        #endregion

        [Inject] public IBuildingService jBuildingService { get; set; }

        public IMapBuilding MapBuildingModel { get; private set; }
        
        public virtual MapObjectintTypes MapObjectIntType { get; }

        /// <summary>
        /// Sets target building model and initialize view state.
        /// </summary>
        /// <param name="model"></param>
        public override void InitializeView(IMapObject model)
        {
            base.InitializeView(model);

            MapBuildingModel = model as IMapBuilding;
            model.EventOnMapObjectBuildStarted.AddListener(OnMapObjectBuildStarted);
            transform.localScale = CustomScale * (jCityMapSettings.CellSize * MapBuildingModel.Dimensions.X) * Vector3.one;
        }

        public override void DisposeView()
        {
            base.DisposeView();
            MapBuildingModel.EventOnMapObjectBuildStarted.RemoveListener(OnMapObjectBuildStarted);
            MapBuildingModel = null;
        }

        protected override void AddListeners()
        {
            base.AddListeners();

            jSignalOnConstructControllerChangeState.AddListener(OnConstractControllerChangeStateSignal);
            jPostLoadingActionsCompleteSignal.AddListener(OnPreloaderCloudHideEnded);

            jSignalOnBuildingUpgradeAttentionTap.AddListener(OnMapBuildingUpgradeAttentionTap);

            jSignalOnMovedMapObjectOnServer.AddListener(OnMovedMapObjectOnServer);
            jSignalBuildFinishVerificated.AddListener(OnBuildFinishVerificated);
            jSignalOnMapBuildingsUpgradeStarted.AddListener(OnMapBuildingsUpgradeStarted);
            jSignalOnBuildingRepaired.AddListener(OnBuildingRepaired);
            jUserLevelUpSignal.AddListener(OnUserLevelUp);
            jSignalOnProductsStateChanged.AddListener(OnProductsStateChanged);
            jSignalOnTaxesCollection.AddListener(OnTaxesCollect);
        }

        protected override void RemoveListeners()
        {
            base.RemoveListeners();

            jSignalOnConstructControllerChangeState.RemoveListener(OnConstractControllerChangeStateSignal);
            jPostLoadingActionsCompleteSignal.RemoveListener(OnPreloaderCloudHideEnded);

            jSignalOnBuildingUpgradeAttentionTap.RemoveListener(OnMapBuildingUpgradeAttentionTap);

            jSignalOnMovedMapObjectOnServer.RemoveListener(OnMovedMapObjectOnServer);
            jSignalBuildFinishVerificated.RemoveListener(OnBuildFinishVerificated);
            jSignalOnMapBuildingsUpgradeStarted.RemoveListener(OnMapBuildingsUpgradeStarted);
            jSignalOnBuildingRepaired.RemoveListener(OnBuildingRepaired);
            jUserLevelUpSignal.RemoveListener(OnUserLevelUp);
            jSignalOnProductsStateChanged.RemoveListener(OnProductsStateChanged);
            jSignalOnTaxesCollection.RemoveListener(OnTaxesCollect);
        }

        public override void OnMapObjectViewTap()
        {
            if (!Interactable)
                return;

            base.OnMapObjectViewTap();

            if (!gameObject.activeInHierarchy || ConstructionController.IsBuildModeEnabled)
            {
                return;
            }

            // avoid iteraction when moving
            if (SelectComponent != null && SelectComponent.IsSelected)
            {
                return;
            }

            // avoid interactions when it is under consturction
            if (!MapBuildingModel.IsConstructed)
            {
                jSoundManager.Play(SfxSoundTypes.BuildingConstruction, SoundChannel.SoundFX);
                return;
            }

            if (MapBuildingModel.IsLocked ||
                MapBuildingModel.BuildingUnlockLevel > jPlayerProfile.CurrentLevelData.CurrentLevel)
            {
                OnLockedBuildingTap();
            }
            else if (MapBuildingModel.IsFunctional())
            {
                OnBuildingTap();
            }
        }

        protected virtual void ShowAttentionIfNeeded()
        {
            if (AttentionComponent == null || MapBuildingModel == null)
            {
                return;
            }

            AttentionComponent.UpdateAttentionView();
        }

        protected void ShowAttentionIfNeeded(IMapObject obj)
        {
            if (MapId != obj.MapID)
            {
                return;
            }

            ShowAttentionIfNeeded();
        }

        protected void OnLockedBuildingTap()
        {
            jShowWindowSignal.Dispatch(typeof(RepairBuildingPanel), MapBuildingModel);
        }

        protected virtual void OnBuildingTap()
        {

        }

        protected virtual void OnUpgradeAttentionTap()
        {
            jShowWindowSignal.Dispatch(typeof(UpgradeWithProductsPanelView), MapObjectModel);
        }

        protected virtual void OnBuildingAttentionTap(AttentionsTypes attentionType)
        {
            OnMapObjectViewTap();
        }

        protected void ShowIndicators()
        {
            ShowAttentionIfNeeded();
        }

        private void OnConstractControllerChangeStateSignal(bool state)
        {
            AttentionComponent?.SetViewVisible(!state);
        }

        private void OnPreloaderCloudHideEnded()
        {
            ShowIndicators();
        }

        private void OnBuildingAttentionTap(BuildingStatesAttentionView obj)
        {
            if (obj.CurrentTarget != this)
            {
                return;
            }

            OnBuildingAttentionTap(obj.CurrentAttentionsType);
        }

        private void OnMapBuildingUpgradeAttentionTap(IMapBuilding building)
        {
            if (MapId != building.MapID)
            {
                return;
            }

            OnUpgradeAttentionTap();
        }

        private void OnMovedMapObjectOnServer(IMapObject obj)
        {
            ShowIndicators();
        }

        private void OnMapObjectBuildStarted(IMapObject obj)
        {
            ShowIndicators();
        }

        private void OnBuildFinishVerificated(IMapObject obj)
        {
            ShowIndicators();
        }

        private void OnMapBuildingsUpgradeStarted(IMapBuilding obj)
        {
            ShowIndicators();
        }

        protected void OnBuildingRepaired(Id_IMapObjectType objectType, Id_IMapObject id)
        {
            ShowIndicators();
        }

        private void OnUserLevelUp()
        {
            ShowIndicators();
        }

        private void OnTaxesCollect(int count)
        {
            ShowIndicators();
        }

        private void OnProductsStateChanged(Dictionary<Id<Product>, int> products)
        {
            ShowIndicators();
        }

        private void OnApplicationFocus(bool hasFocus)
        {
            if (hasFocus)
            {
                // ShowIndicators();
            }
        }

        private void OnApplicationPause(bool pauseStatus)
        {
            if (!pauseStatus)
            {
                // ShowIndicators();
            }
        }
    }
}
