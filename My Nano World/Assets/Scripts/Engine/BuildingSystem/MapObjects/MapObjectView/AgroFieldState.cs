using UnityEngine;

namespace NanoReality.Engine.BuildingSystem.MapObjects.AgroField
{
    public sealed class AgroFieldState : MonoBehaviour
    {
        public Renderer Renderer;
        public int ProductId;
    }
}