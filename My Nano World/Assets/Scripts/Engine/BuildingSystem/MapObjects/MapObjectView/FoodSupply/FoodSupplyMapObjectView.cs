﻿using System.Collections.Generic;
using Engine.UI;
using NanoReality.Engine.BuildingSystem.GlobalCityMap;
using NanoReality.Game.Attentions;
using NanoReality.Game.CitizenGifts;
using NanoReality.Game.UI;
using NanoReality.UI.Components;
using UnityEngine;

public class FoodSupplyMapObjectView : AGlobalCityBuildingView
{
    [Inject] public ICityGiftsService jCityGiftsService { get; set; }
    [Inject] public SignalOnFoodSupplyAttentionTap jSignalOnFoodSupplyAttentionTap { get; set; }
    [Inject] public AddRewardItemSignal jAddRewardItemSignal { get; set; }
    
    private Vector3 _attentionViewPosition;
    
    public override bool IsMovable => false;
    
    protected override void AddListeners()
    {
        base.AddListeners();
            
        jCityGiftsService.GiftsUpdates += CheckAvailableCoinGift;
        jSignalOnFoodSupplyAttentionTap.AddListener(OnAttentionClick);
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
            
        jCityGiftsService.GiftsUpdates -= CheckAvailableCoinGift;
        jSignalOnFoodSupplyAttentionTap.RemoveListener(OnAttentionClick);
    }

    protected override void OnBuildingTap()
    {
        jShowWindowSignal.Dispatch(typeof(FoodSupplyPanelView), MapBuildingModel);
    }

    private void CheckAvailableCoinGift()
    {
        ShowIndicators();
    }

    private void OnAttentionClick()
    {
        var mapId = MapBuildingModel.MapID;
        if (!jCityGiftsService.HasAvailableGifts(mapId)) return;

        _attentionViewPosition = AttentionComponent.AttentionView.AttentionButton.gameObject.transform.position;

        var gifts = new List<CitizenGift>(jCityGiftsService.GetGifts(mapId));
        foreach (var foodSupplyGift in gifts)
        {
            var count = foodSupplyGift.Reward.Coins;
            if (count > 0)
            {
                jCityGiftsService.ClaimGift(mapId, foodSupplyGift.Id, () => OnCollect(count));
            }
        }
    }

    private void OnCollect(int coinsCount)
    {
        jAddRewardItemSignal.Dispatch(new AddItemSettings(FlyDestinationType.SoftCoins, _attentionViewPosition, coinsCount));
        jPlayerProfile.PlayerResources.AddSoftCurrency(coinsCount);

        ShowIndicators();
    }
}