﻿namespace Assets.Scripts.Engine.BuildingSystem.MapObjects.Components
{
	public interface IMapObjectViewComponent 
	{
		MapObjectView Target {get; }

		void InitializeComponent(MapObjectView target);
		
		void DisposeComponent();
	}
}