﻿using System;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.UI.Views.WorldSpaceUI;
using NanoReality.Engine.UI;
using NanoReality.Engine.UI.Extensions.EditModePanels;
using NanoReality.Game.Services.BuildingsUnlockService;
using NanoReality.GameLogic.BuildingSystem;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.UI.Components.ChangingPosition;
using UnityEngine;

namespace NanoReality.Engine.BuildingSystem.MapObjects.Components
{
    public class ConstructPlaceIndicatorComponent : ColorHighlightComponent
    {
        private MapObjectSelect TargetSelectComponent => Target.SelectComponent;

        private bool IsSelected => TargetSelectComponent != null && TargetSelectComponent.IsSelected;

        private IMapObjectConstructingIndicatorComponent VisualElement => ConstructingCellsIndicatorComponent.Instance;

        private PopulationInfoView PopulationInfoView { get; set; }
        
        #region Injects

        [Inject] public IConstructionController jConstructionController { get; set; }
        [Inject] public SignalMoveMapObjectView jSignalMoveMapObjectView { get; set; }
        [Inject] public IWorldSpaceCanvas jWorldSpaceCanvas { get; set; }
        [Inject] public BuildingConstructor jBuildingConstructor { get; set; }
        [Inject] public IBuildingsUnlockService jBuildingsUnlockService { get; set; }
        [Inject] public IEditModeService jEditModeService { get; set; }

        #endregion

        public override void InitializeComponent(MapObjectView target)
        {
            base.InitializeComponent (target);
            TargetSelectComponent.SignalOnSelectChanged.AddListener(OnMapObjectSelectChanged);
        }

        public override void DisposeComponent()
        {
            TargetSelectComponent.SignalOnSelectChanged.RemoveListener(OnMapObjectSelectChanged);
            VisualElement.Hide();
            base.DisposeComponent();
        }

        private void OnMapObjectSelectChanged(bool isSelected)
        {
            if (IsSelected)
            {
                AddVisualElements();
                if (jConstructionController.SelectedObjectView == null)
                {
                    UpdateFreeAreaColorEffect(true);
                }
                else
                {
                    UpdateFreeAreaColorEffect(jConstructionController.IsConstructionAvailable);
                }

                AddPopulationInfoIfNeeded();
                jSignalMoveMapObjectView.AddListener(OnMoveMapObjectSignal);
            }
            else
            {
                RemoveVisualElements();

                if (!jEditModeService.IsAOEEnable)
                {
                    RemoveHighlighting();
                    HighlightMaterial = null;
                }

                jSignalMoveMapObjectView.RemoveListener(OnMoveMapObjectSignal);

                RemovePopulationInfo();
            }
        }

        private void AddPopulationInfoIfNeeded()
        {
            if (!jBuildingConstructor.IsBuildNewBuilding)
            {
                return;
            }

            if (!(Target.MapObjectModel is IDwellingHouseMapObject dwellingHouse))
            {
                return;
            }

            var buildingId = dwellingHouse.ObjectTypeID;
            var currentLevel = dwellingHouse.Level;
            var instanceIndex = dwellingHouse.InstanceIndex;

            var nextLevel = currentLevel + 1;
            var nextLevelPopulation = jBuildingsUnlockService.GetPopulationForDwelling(buildingId, nextLevel, instanceIndex);

            if (PopulationInfoView == null)
            {
                PopulationInfoView = jWorldSpaceCanvas.GetPopulationInfoView();
            }

            PopulationInfoView.ShowIcon(Target, nextLevelPopulation);
        }

        private void RemovePopulationInfo()
        {
            if (PopulationInfoView != null)
            {
                PopulationInfoView.FreeObject();
                PopulationInfoView = null;
            }
        }

        private void OnMoveMapObjectSignal(Vector2Int movedPosition, IMapObject movedBuilding, Action callback = null)
        {
            UpdateFreeAreaColorEffect(jConstructionController.IsConstructionAvailable);
            VisualElement.UpdateComponent(jConstructionController.IsConstructionAvailable);
        }

        private void AddVisualElements()
        {
            VisualElement.SetBuildingView(Target);
            VisualElement.Show(jConstructionController.IsConstructionAvailable);
        }

        private void RemoveVisualElements()
        {
            VisualElement.Hide();
        }

        private void UpdateFreeAreaColorEffect(bool isAreaFree)
        {
            if (jEditModeService.IsAOEEnable) return;

            Material newHighlightMaterial = isAreaFree
                ? jGlobalMapObjectViewSettings.BuildingInFreeAreaMaterial
                : jGlobalMapObjectViewSettings.BuildingNotFreeAreaMaterial;

            if (newHighlightMaterial == HighlightMaterial)
            {
                return;
            }

            HighlightMaterial = newHighlightMaterial;
            SetHighlighting();
        }
    }
}