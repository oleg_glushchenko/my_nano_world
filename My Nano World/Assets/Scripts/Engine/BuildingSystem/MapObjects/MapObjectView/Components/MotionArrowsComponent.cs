﻿using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.BuildingSystem.MapObjects.Components;
using UnityEngine;
using Assets.Scripts.Engine.UI.Components.Arrows;
using DG.Tweening;
using NanoReality.Engine.BuildingSystem.CityCamera;


namespace NanoReality.Engine.BuildingSystem.MapObjects.Components
{
    public class MotionArrowsComponent : MapObjectViewComponent
    {
        #region Injects

        [Inject] public IGameCameraModel jGameCameraModel { get; set; }

        #endregion

        /// <summary>
        /// Кешиурем компонент селекта
        /// </summary>
        private MapObjectSelect _selectComponent;

        private Tweener _scaleTweener;

        public bool IsEnabled { get; private set; }

        public override void InitializeComponent(MapObjectView target)
        {
            base.InitializeComponent(target);
            if (Target.SelectComponent != null)
            {
                _selectComponent = Target.SelectComponent;
                _selectComponent.SignalOnSelectChanged.AddListener(OnMapObjectSelectChanged);
            }
        }

        public override void DisposeComponent()
        {
            if (_selectComponent != null)
            {
                _selectComponent.SignalOnSelectChanged.RemoveListener(OnMapObjectSelectChanged);
            }

            RemoveMotionArrows();
            base.DisposeComponent();
        }

        /// <summary>
        /// Удаляет стрелки перемещения со здания
        /// </summary>
        private void RemoveMotionArrows()
        {
            ArrowsComponent.Instance.RemoveArrows();
            //if (Target.HighlightComponent.IsInitilized)
            //    Target.HighlightComponent.RemoveHighlighting();
            IsEnabled = false;
        }

        /// <summary>
        /// Добавляет стрелки перемещения на здание
        /// </summary>
        private void AddMotionArrows()
        {
            ArrowsComponent.Instance.SetArrows(Target, jGameCameraModel.AngleDegrees);

            //Target.HighlightComponent.SetHighlighting();

            Vector3 targetScale = 0.1f * Target.CacheMonobehavior.CachedTransform.localScale;
            const float tweenDuration = 1.0f;
            const int vibrato = 5;
            const float elasticity = 0.0f;

            _scaleTweener = Target.gameObject.transform.DOPunchScale(targetScale, tweenDuration, vibrato, elasticity)
                .OnComplete(StopScale);
            IsEnabled = true;
        }


        private void OnMapObjectSelectChanged(bool isSelected)
        {
            if (isSelected)
            {
                AddMotionArrows();
            }
            else
            {
                RemoveMotionArrows();
            }
        }

        private void StopScale()
        {
            if (_scaleTweener != null)
                _scaleTweener.Kill(true);

            _scaleTweener = null;
        }
    }
}