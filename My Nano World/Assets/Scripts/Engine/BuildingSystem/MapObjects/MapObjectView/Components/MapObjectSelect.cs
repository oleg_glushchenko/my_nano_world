﻿using strange.extensions.signal.impl;

namespace NanoReality.Engine.BuildingSystem.MapObjects.Components
{
	/// <summary>
	/// Компонент для выделения объекта на карте
	/// </summary>
	public class MapObjectSelect : MapObjectViewComponent 
	{

		/// <summary>
		/// Выделен ли объект?
		/// </summary>
		public bool IsSelected 
		{
			get 
			{
				return _isSelected;
			}
			protected set
			{
				if (_isSelected != value) 
				{
					_isSelected = value;
					SignalOnSelectChanged.Dispatch (_isSelected);
				}
			}
		}

		/// <summary>
		/// Вызывается при изменении состояния выделения
		/// </summary>
		public Signal<bool> SignalOnSelectChanged = new  Signal<bool>();

		#region Injects
		
		[Inject]
		public SignalOnMapObjectChangedSelect jSignalOnMapObjectChangedSelect { get; set; }
		
		#endregion

		private bool _isSelected;
		
		/// <summary>
		/// Удаляет компонент с вьюшки
		/// </summary>
		public override void DisposeComponent ()
		{
			_isSelected = false;
			SignalOnSelectChanged.Dispatch (_isSelected);
			jSignalOnMapObjectChangedSelect.Dispatch (Target, _isSelected);
			SignalOnSelectChanged.RemoveAllListeners ();
			SignalOnSelectChanged.RemoveAllOnceListeners ();
			base.DisposeComponent();
		}

		/// <summary>
		/// Изменяет состояние выделения объекта
		/// </summary>
		/// <param name="state">If set to <c>true</c> state.</param>
		public virtual void ChangeSelect(bool state)
		{
			if (IsSelected != state) 
			{
				IsSelected = state;
				jSignalOnMapObjectChangedSelect.Dispatch (Target, IsSelected);
			}
		}

	}
}
