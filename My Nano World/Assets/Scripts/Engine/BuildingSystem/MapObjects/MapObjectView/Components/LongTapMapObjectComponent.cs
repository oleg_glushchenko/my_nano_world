using Assets.NanoLib.UI.Core.Signals;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoLib.Services.InputService;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using UnityEngine;

namespace NanoReality.Engine.BuildingSystem.MapObjects.Components
{
    public abstract class LongTapMapObjectComponent : MapObjectViewComponent
    {
        [Inject] public SignalOnLongTapMapObjectView jOnLongTapMapObjectViewSignal { get; set; }
		
        public override void InitializeComponent(MapObjectView target)
        {
            base.InitializeComponent(target);
            jOnLongTapMapObjectViewSignal.AddListener(OnLongTapMapObject);
        }
        
        public override void DisposeComponent()
        {
            base.DisposeComponent();
            jOnLongTapMapObjectViewSignal.RemoveListener(OnLongTapMapObject);
        }

        private void OnLongTapMapObject(int tapDuration, Vector3 raycastPosition, MapObjectView targetView)
        {
            if (targetView == Target && Target.MapObjectModel.IsVerificated)
            {
                OnTargetLongTapped();
            }
        }

        protected abstract void OnTargetLongTapped();
    }

    public sealed class DefaultLongTapComponent : LongTapMapObjectComponent
    {
        [Inject] public IConstructionController jConstructionConstroller { get; set; }
        
        protected override void OnTargetLongTapped()
        {
            jConstructionConstroller.ActivateBuildingMode(Target);
        }
    }

    public sealed class OpenPanelLongTapComponent : LongTapMapObjectComponent
    {
        [Inject] public ShowWindowSignal jShowWindowSignal { get; set; }
        
        protected override void OnTargetLongTapped()
        {
            
        }
    }
}