using System;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.Engine.UI.Views.WorldSpaceUI;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Game.Attentions;
using NanoLib.Core.Services.Sound;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
using NanoReality.GameLogic.Common.Controllers.Signals;

namespace NanoReality.Engine.BuildingSystem.MapObjects.Components
{
    public sealed class BuildingAttentionComponent : MapObjectViewComponent
    {
        private BuildingAttentionPolicy _attentionPolicy;
        private AttentionData _currentAttentionData;

        public BuildingStatesAttentionView AttentionView { get; private set; }
        public bool IsShown => AttentionView != null;
        
        public event Action<bool> SignalOnAttentionViewStateChanged;

        [Inject] public IBuildingAttentionsController jBuildingAttentionsController { get; set; }
        [Inject] public ISoundManager jSoundManager { get; set; }
        [Inject] public SignalOnBuildingElectricityChanged jSignalOnBuildingElectricityChanged { get; set; }
        [Inject] public SignalOnSubSectorUnlocked jSignalOnSubSectorUnlocked { get; set; }
        [Inject] public IConstructionController jConstructionController { get; set; }

        #region Methods

        public override void InitializeComponent(MapObjectView target)
        {
            base.InitializeComponent(target);

            _attentionPolicy = jBuildingAttentionsController.GetAttentionPolicy(target.MapObjectModel.GetType());
            _attentionPolicy.Initialize(TargetModel);

            TargetModel.EventOnBuildingRoadConnectionChanged.AddListener(OnRoadConnectionChanged);
            TargetModel.CitizenGiftChanged += UpdateView;

            AddListeners();

            UpdateAttentionView();
        }

        public override void DisposeComponent()
        {
            FreeView();
            AttentionView = null;
            TargetModel.EventOnBuildingRoadConnectionChanged.RemoveListener(OnRoadConnectionChanged);
            TargetModel.CitizenGiftChanged -= UpdateView;

            RemoveListeners();

            base.DisposeComponent();
        }

        private void AddListeners()
        {
            jSignalOnBuildingElectricityChanged.AddListener(UpdateStateIfNeed);
            jSignalOnSubSectorUnlocked.AddListener(OnSubSectorUnlocked);
        }

        private void RemoveListeners()
        {
            jSignalOnBuildingElectricityChanged.RemoveListener(UpdateStateIfNeed);
            jSignalOnSubSectorUnlocked.RemoveListener(OnSubSectorUnlocked);
        }

        #region Event litheners

        private void OnRoadConnectionChanged(IMapObject mapObject)
        {
            if (mapObject.Id == TargetModel.Id)
            {
                UpdateAttentionView();
            }
        }
        
        private void UpdateStateIfNeed(IBuildingWithElectricity mapObject)
        {
            if (mapObject == Target.MapObjectModel)
                UpdateAttentionView();
        }

        private void UpdateStateIfNeed(IBuildingWithElectricity mapObject, bool state)
        {
            UpdateStateIfNeed(mapObject);
        }

        private void UpdateView()
        {
            UpdateAttentionView();
        }

        private void OnSubSectorUnlocked(CityLockedSector cityLockedSector)
        {
            UpdateAttentionView();
        }

        #endregion

        public void UpdateAttentionView()
        {
            if (!IsAttentionCanBeShown())
            {
                FreeView();
                return;
            }

            _currentAttentionData = _attentionPolicy.CalculateAttention();

            if (_currentAttentionData.AttentionType != AttentionsTypes.None)
            {
                if (AttentionView == null)
                {
                    AttentionView = jBuildingAttentionsController.AddAttentionView(TargetModel.MapID);
                    jSoundManager.Play(SfxSoundTypes.NotificationIcon, SoundChannel.SoundFX);
                }
                
                AttentionView.UpdateState(_currentAttentionData, Target);
                SignalOnAttentionViewStateChanged?.Invoke(true);

                bool isVisible = !jConstructionController.IsBuildModeEnabled;
                SetViewVisible(isVisible);
            }
            else
            {
                FreeView();
            }
        }

        public void SetViewVisible(bool isVisible)
        {
            if (AttentionView == null)
            {
                return;
            }
            
            if (_currentAttentionData != null)
            {
                isVisible |= _currentAttentionData.AttentionType.HasFlag(AttentionsTypes.Road);
            }
                
            AttentionView.gameObject.SetActive(isVisible);
        }

        public void FreeView(bool force = false)
        {
            if (AttentionView != null)
            {
                jBuildingAttentionsController.RemoveBuildingStatesAttentionView(TargetModel.MapID);
                AttentionView = null;
                SignalOnAttentionViewStateChanged?.Invoke(false);
            }
        }

        private bool IsAttentionCanBeShown()
        {
            if (Target == null || Target.MapObjectModel == null) return false;
            if (!Target.MapObjectModel.IsConstructed) return false;
            if (Target.MapId.Value <= 0) return false;

            return true;
        }

        #endregion
    }
}

    