﻿using Assets.Scripts.Engine.BuildingSystem.MapObjects.Components;
using NanoReality.Engine.UI;

namespace NanoReality.Engine.BuildingSystem.MapObjects.Components
{
    public class HighlightHappinesComponent : ColorHighlightComponent
    {
        [Inject] public IWorldSpaceCanvas WorldSpaceCanvas { get; set; }

        private AoeHiglightParticle _particle;

        public override void SetHighlighting()
        {
            base.SetHighlighting();
            FreeParticle();
            _particle = WorldSpaceCanvas.GetHappinessParticle();
            _particle.SetLooker(Target);
        }

        public override void RemoveHighlighting()
        {
            base.RemoveHighlighting();
            FreeParticle();
        }
        
        public override void DisposeComponent()
        {
            base.DisposeComponent();
            FreeParticle();
        }

        private void FreeParticle()
        {
            if (_particle != null)
            {
                _particle.FreeObject();
                _particle = null;
            }
        }
    }
}