﻿using Assets.Scripts.Engine.BuildingSystem.MapObjects.Components;
using NanoReality.Engine.UI;

namespace NanoReality.Engine.BuildingSystem.MapObjects.Components
{
    public class PowerNeedHighlightComponent : ColorHighlightComponent
    {
        [Inject] public IWorldSpaceCanvas WorldSpaceCanvas { get; set; }

        private AoeHiglightParticle _particle;

        public override void SetHighlighting()
        {
            FreeParticle();
            _particle = WorldSpaceCanvas.GetEnergyParticle();
            _particle.SetLooker(Target);
            base.SetHighlighting();
        }

        private void FreeParticle()
        {
            _particle?.FreeObject();
            _particle = null;
        }

        public override void RemoveHighlighting()
        {
            FreeParticle();
            base.RemoveHighlighting();
        }
    }
}