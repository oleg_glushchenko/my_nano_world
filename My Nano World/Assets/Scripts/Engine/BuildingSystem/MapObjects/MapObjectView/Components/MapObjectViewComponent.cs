﻿using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.BuildingSystem.MapObjects.Components;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace NanoReality.Engine.BuildingSystem.MapObjects.Components
{
	public class MapObjectViewComponent : IMapObjectViewComponent
	{
		public MapObjectView Target { get; private set; }

		public IMapBuilding TargetModel { get; private set; }

		public virtual void InitializeComponent(MapObjectView target)
		{
			Target = target;
			TargetModel = (IMapBuilding)Target.MapObjectModel;
		}
		
		public virtual void DisposeComponent()
		{
			Target = null;
			TargetModel = null;
		}
	}
}