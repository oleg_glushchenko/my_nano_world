﻿using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.BuildingSystem.MapObjects;
using UnityEngine;

namespace NanoReality.Engine.BuildingSystem.MapObjects.Components
{
    public class ColorHighlightComponent : MapObjectViewComponent
    {
        public Material HighlightMaterial { get; set; }

        public Material IdleMaterial { get; set; }
        
        public bool IsHighlightActive { get; private set; }
        
        [Inject] public IGlobalMapObjectViewSettings jGlobalMapObjectViewSettings { get; set; }

        public override void InitializeComponent(MapObjectView view)
        {
            base.InitializeComponent(view);
            IdleMaterial = Target.SpriteRenderer.sharedMaterial;
        }

        public override void DisposeComponent()
        {
            RemoveHighlighting();
            base.DisposeComponent();
        }

        public virtual void SetHighlighting()
        {
            Target.StaticAnimationsComponent?.SetMaterialToAllSprites(HighlightMaterial);
            Target.StaticAnimationsComponent?.SwitchVfxParticles(false);

            Target.ConstructionComponent?.ConstructView?.SetMaterialToAllSprites(HighlightMaterial);
            Target.ConstructionComponent?.ConstructView?.SwitchVfxParticles(false);

            IsHighlightActive = true;
        }

        public virtual void RemoveHighlighting()
        {
            Target?.StaticAnimationsComponent?.SetMaterialToAllSprites(IdleMaterial);
            Target?.StaticAnimationsComponent?.SwitchVfxParticles(true);

            Target?.ConstructionComponent?.ConstructView?.SetMaterialToAllSprites(IdleMaterial);
            Target?.ConstructionComponent?.ConstructView?.SwitchVfxParticles(true);

            IsHighlightActive = false;
        }
    }
}
