﻿using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using UnityEngine;
using NanoLib.Services.InputService;
using NanoReality.Engine.BuildingSystem.CityCamera;
using NanoReality.Game.Services.BuildingServices;
using NanoReality.GameLogic.BuildingSystem.CityCamera;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.GameManager.Models.api;

namespace NanoReality.Engine.BuildingSystem.MapObjects.Components
{
	/// <summary>
	/// Компонент для перемещения объектов по карте
	/// </summary>
	public class MapObjectDrag : MapObjectViewComponent
	{
		#region Injects
		
		[Inject] public IConstructionController jConstructionController { get; set; }
		[Inject] public IGameManager jGameManager {get; set;}
		[Inject] public IGameCamera jGameCamera {get; set;}
		[Inject] public IMapObjectPlacementService jMapObjectPlacementService { get; set; }
		[Inject] public SignalDragObject jSignalDragObject { get; set; }
		[Inject] public MapObjectMovedSignal jMapObjectMovedSignal { get; set; }
		[Inject] public IInputController jInputController { get; set; }
		[Inject] public GameCameraSettings jGameCameraSettings { get; set; }
		[Inject] public IGameCameraModel jGameCameraModel { get; set; }
		
		#endregion
		
		public Vector2Int OldGridPosition { get; private set; }
		
		public Vector2Int CurrentGridPosition { get; private set; }
		
		private MapObjectSelect _selectComponent;
		private Vector3 _startInputPosition;
		private bool _startMoveCamera;
		
        private Id_IMapObject _lastDragObjectInSectorBounds;

		public override void InitializeComponent(MapObjectView target)
		{
			base.InitializeComponent(target);
			jSignalDragObject.AddListener(DragObject);
			_selectComponent = target.SelectComponent;
			_selectComponent.SignalOnSelectChanged.AddListener(OnSelectChanged);
		}

		public override void DisposeComponent()
		{
			base.DisposeComponent();
			jSignalDragObject.RemoveListener(DragObject);
			_selectComponent.SignalOnSelectChanged.RemoveListener(OnSelectChanged);
		}
		
		private void OnSelectChanged(bool isActivated)
		{
			if (!isActivated)
		    {
			    return;
		    }

			OldGridPosition = Target.GridPosition;
			CurrentGridPosition = Target.GridPosition;
		}

		private void DragObject(Vector3 position)
		{
			if (!Target.SelectComponent.IsSelected || !jConstructionController.IsBuildModeEnabled) return;

			jMapObjectPlacementService.SetContextData(TargetModel);
			
			Vector2Int gridPos = jGameManager.GetMapView(TargetModel.SectorId).ConvertToGridPos(position);

			OldGridPosition = CurrentGridPosition;
			CurrentGridPosition = jMapObjectPlacementService.FindEnablePosition(gridPos, OldGridPosition);
			jConstructionController.PlaceMapObject(CurrentGridPosition);

			if (OldGridPosition != CurrentGridPosition)
			{
				jMapObjectMovedSignal.Dispatch(Target);
			}

			UpdateCameraPosition();
		}
		
		private void UpdateCameraPosition()
		{
			if (IsInputPositionAtViewBorder())
			{
				if (_startMoveCamera)
				{
					_startMoveCamera = false;
					_startInputPosition = jInputController.CurrentFingerPosition;
					return;
				}
				
				MoveCamera();
			}
			else
			{
				_startMoveCamera = true;
			}
		}

		private void MoveCamera()
		{
			var delta = jInputController.CurrentFingerPosition - _startInputPosition;
			var convertedPosition = InputTools.ConvertDisplacement(delta);
			convertedPosition *= (jGameCameraSettings.SwipeObjectCoefficient * jGameCameraModel.CurrentZoom);
			jGameCamera.Swipe(convertedPosition.ToVector3F());
		}

		private bool IsInputPositionAtViewBorder()
		{
			var viewPortPoint = jGameCamera.ScreenToViewportPoint(jInputController.CurrentFingerPosition);

			if (viewPortPoint.x > jGameCameraSettings.MaxSwipeObjectBorder.x || viewPortPoint.x < jGameCameraSettings.MinSwipeObjectBorder.x)
			{
				return true;
			}

			if (viewPortPoint.y > jGameCameraSettings.MaxSwipeObjectBorder.y || viewPortPoint.y < jGameCameraSettings.MinSwipeObjectBorder.y)
			{
				return true;
			}

			return false;
		}
	}
}
