﻿using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Production;
using UnityEngine;

namespace NanoReality.Engine.BuildingSystem.MapObjects.Components
{
    public sealed class ProductionBuildingAnimationComponent : MapObjectViewComponent
    {
        private static readonly int WorkAnimationId = Animator.StringToHash("Work");
        private static readonly int IdleAnimationId = Animator.StringToHash("Idle");

        private IProductionBuilding _productionBuilding;

        public override void InitializeComponent(MapObjectView target)
        {
            base.InitializeComponent(target);

            _productionBuilding = (IProductionBuilding)target.MapObjectModel;
            _productionBuilding.StartedProduction += OnStartProduction;
            _productionBuilding.OnConfirmFinishProduction += OnConfirmFinishProduction;
            _productionBuilding.OnSlotShipped += OnSlotShipped;
            SetTrigger();
        }

        public override void DisposeComponent()
        {
            base.DisposeComponent();
            
            _productionBuilding.StartedProduction -= OnStartProduction;
            _productionBuilding.OnConfirmFinishProduction -= OnConfirmFinishProduction;
            _productionBuilding.OnSlotShipped -= OnSlotShipped;
        }

        private void OnSlotShipped(IProduceSlot produceSlot, IProducingItemData producingItemData)
        {
            if (!_productionBuilding.IsProduced())
            {
                Target.Animator.ResetTrigger(WorkAnimationId);
                Target.Animator.SetTrigger(IdleAnimationId);
            }
        }

        private void SetTrigger()
        {
            if (_productionBuilding.IsProduced())
            {
                Target.Animator.SetTrigger(WorkAnimationId);
            }
            else
            {
                Target.Animator.SetTrigger(IdleAnimationId);
            }
        }
        
        private void OnStartProduction(IProduceSlot produceSlot, IProducingItemData producingItemData)
        {
            Target.Animator.ResetTrigger(IdleAnimationId);
            Target.Animator.SetTrigger(WorkAnimationId);
        }
            
        private void OnConfirmFinishProduction(IProduceSlot produceSlot, IProducingItemData producingItemData)
        {
            if (!_productionBuilding.IsProduced())
            {
                Target.Animator.ResetTrigger(WorkAnimationId);
                Target.Animator.SetTrigger(IdleAnimationId);
            }
        }
    }
}
