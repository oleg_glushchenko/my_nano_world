﻿using System.Collections.Generic;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.BuildingSystem.MapObjects;
using UnityEngine;

namespace NanoReality.Engine.BuildingSystem.MapObjects.Components
{
    /// <summary>
    /// Component for managing static animations on an object
    /// </summary>
    public class MapObjectStaticAnimations : MapObjectViewComponent
    {
        /// <summary>
        /// Caching the select component
        /// </summary>
        private MapObjectSelect _selectComponent;

        /// <summary>
        /// Cached animation objects
        /// </summary>
        private List<SpriteRenderer> AnimatedSprites => Target.AnimatedSprites;

        /// <summary>
        /// Cached vfx objects
        /// </summary>
        private List<ParticleSystemRenderer> VfxParticles => Target.VfxParticles;

        private Animator MapObjectAnimator => Target.Animator;

        /// <summary>
        /// Are animations enabled
        /// </summary>
        private bool IsEnabled { get; set; }

        public override void InitializeComponent(MapObjectView target)
        {
            base.InitializeComponent(target);
            _selectComponent = Target.SelectComponent;
            _selectComponent?.SignalOnSelectChanged.AddListener(OnMapObjectSelectChanged);
        }

        public override void DisposeComponent()
        {
            _selectComponent?.SignalOnSelectChanged.RemoveListener(OnMapObjectSelectChanged);
            _selectComponent = null;
            base.DisposeComponent();
        }

        /// <summary>
        /// Turns on / off static animation on the building
        /// </summary>
        public void SwitchAnimationsRenders(bool isEnabled)
        {
            if (Target == null || Target.MapObjectModel == null)
            {
                Debug.LogError("Can't switch animations.");
                return;
            }
            
            if (Target is AgroFieldBuildingView)
            {
                return;
            }

            IsEnabled = Target.MapObjectModel.IsConstructed && isEnabled;

            SwitchSpriteAnimations(IsEnabled);
            SwitchVfxParticles(IsEnabled);
            ApplyBackgroundSpriteMaterialToChildren();
        }

        /// <summary>
        /// Sets given material to all building sprites
        /// </summary>
        public void SetMaterialToAllSprites(Material material)
        {
            if (Target == null)
            {
                return;
            }
            Target.SpriteRenderer.sharedMaterial = material;
            ApplyBackgroundSpriteMaterialToChildren();
        }

        /// <summary>
        /// Enable or disable buildings vfx
        /// </summary>
        public void SwitchVfxParticles(bool isEnabled)
        {
            if (Target == null)
            {
                return;
            }
            isEnabled = Target.MapObjectModel.IsConstructed && isEnabled;

            foreach (ParticleSystemRenderer particleSystemRenderer in VfxParticles)
            {
                particleSystemRenderer.enabled = isEnabled;
            }
        }

        private void SwitchSpriteAnimations(bool isEnabled)
        {
            IsEnabled = isEnabled;

            if (MapObjectAnimator != null)
            {
                MapObjectAnimator.enabled = isEnabled;
            }

            foreach (SpriteRenderer renderer in AnimatedSprites)
            {
                renderer.enabled = isEnabled;
            }
        }

        private void ApplyBackgroundSpriteMaterialToChildren()
        {
            Material material = Target.SpriteRenderer.sharedMaterial;

            if (!material)
            {
                return;
            }

            foreach (SpriteRenderer renderer in AnimatedSprites)
            {
                renderer.sharedMaterial = material;
            }
        }

        /// <summary>
        /// Building selection callback in construction mode
        /// </summary>
        private void OnMapObjectSelectChanged(bool isSelected)
        {
            //disable animations in move mode
            SwitchAnimationsRenders(!isSelected);
        }
    }
}