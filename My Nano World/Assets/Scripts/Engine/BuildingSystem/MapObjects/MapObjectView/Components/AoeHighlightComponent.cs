﻿using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.BuildingSystem.MapObjects.PowerPlants;

namespace NanoReality.Engine.BuildingSystem.MapObjects.Components
{
    public class AoeHighlightComponent : ColorHighlightComponent
    {
        private AreaOfEffectView _aoe;
    
        public override void InitializeComponent(MapObjectView view)
        {
            base.InitializeComponent(view);
            _aoe = ((IAoeBuildingView)Target).AoeView;
            HighlightMaterial = jGlobalMapObjectViewSettings.SelectedAoeBuildingIdleMaterial;
        }

        public override void DisposeComponent()
        {
            base.DisposeComponent();
            HighlightMaterial = null;
        }

        public override void SetHighlighting()
        {
            base.SetHighlighting();
            if (Target.MapObjectModel.IsConstructed)
            {
                _aoe.SetActiveArea(true, true);
            }
        }

        public override void RemoveHighlighting()
        {
            base.RemoveHighlighting();
            _aoe.SetActiveArea(false, false);
        }
    }
}