﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.Utilities.Pulls;
using Assets.Scripts.Engine.Utilities.Meshes;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace NanoReality.Engine.BuildingSystem.MapObjects.ConstractAnimations
{
    public class ConstractAnimationsContainer : View, IBuildingSitesContainer
    {
        private const int MinViewSize = 1;
        
        [SerializeField] private List<BuildingSiteView> _animationViewPrefabs;
        
        private readonly Dictionary<int, Mesh> _buildingSiteViewColliders = new Dictionary<Int32, Mesh>();
        private readonly Dictionary<int, IObjectsPull> _animationPulls = new Dictionary<int, IObjectsPull>();
        
        public List<BuildingSiteView> AnimationViewPrefabs
        {
            get => _animationViewPrefabs;
            set => _animationViewPrefabs = value;
        }

        protected override void Awake()
        {
            base.Awake();
            foreach (var prefab in AnimationViewPrefabs)
            {
                var pull = new MObjectsPull();
                pull.InstanitePull(prefab, 0);
                _animationPulls.Add(prefab.ViewSize, pull);
            }
        }
        
        public BuildingSiteView GetAnimationView(int viewSize)
        {
            for (var size = viewSize; size >= MinViewSize; size--)
            {
                if (_animationPulls.ContainsKey(size))
                {
                    return (BuildingSiteView) _animationPulls[size].GetInstance();
                }
            }
            return null;
        }

        public Mesh GetBuildingSiteMesh(int size)
        {
            if (!_buildingSiteViewColliders.TryGetValue(size, out Mesh mesh))
            {
                mesh = MeshUtils.GenerateMeshFromSprite(_animationViewPrefabs.First(c => c.ViewSize == size)
                    ?.SpriteRenderer.sprite);
                _buildingSiteViewColliders.Add(size, mesh);
            }

            return mesh;
        }
    }
}