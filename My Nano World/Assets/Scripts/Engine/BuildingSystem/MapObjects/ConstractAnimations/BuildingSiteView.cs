﻿using System.Collections.Generic;
using Assets.NanoLib.Utilities.Pulls;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace NanoReality.Engine.BuildingSystem.MapObjects.ConstractAnimations
{
    public class BuildingSiteView : View, IPullableObject
    {
        [SerializeField] private SpriteRenderer _spriteRenderer;
        [SerializeField] private float _customScale = 1.0f;

        private List<SpriteRenderer> _animatedSprites = new List<SpriteRenderer>();
        private List<ParticleSystemRenderer> _vfxParticles = new List<ParticleSystemRenderer>();

        private static readonly int WorkAnimationId = Animator.StringToHash("Work");
        private static readonly int IdleAnimationId = Animator.StringToHash("Idle");

        private Animator _animator;

        public int ViewSize;
        private Material IdleMaterial { get; set; }
        private MapObjectView Target { get; set; }

        public SpriteRenderer SpriteRenderer => _spriteRenderer;

        protected override void Awake()
        {
            base.Awake();
            IdleMaterial = SpriteRenderer.sharedMaterial;
            _animator = GetComponent<Animator>();

            foreach (SpriteRenderer childRenderer in GetComponentsInChildren<SpriteRenderer>(true))
            {
                if (childRenderer != SpriteRenderer && childRenderer.gameObject.layer == LayerMask.NameToLayer("Buildings"))
                {
                    _animatedSprites.Add(childRenderer);
                }
            }

            foreach (ParticleSystemRenderer particleSystem in GetComponentsInChildren<ParticleSystemRenderer>(true))
            {
                if (particleSystem.gameObject.layer == LayerMask.NameToLayer("Buildings"))
                {
                    _vfxParticles.Add(particleSystem);
                }
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            IdleMaterial = null;
        }

        public void SetTarget(MapObjectView target)
        {
            Target = target;
            transform.SetParent(null);
            transform.SetParent(Target.transform, true);
            transform.localScale = _customScale / target.CustomScale * Vector3.one;
            transform.localPosition = Vector3.zero;
            transform.localEulerAngles = Vector3.zero;

            _animator.SetTrigger(Target.MapObjectModel.CurrentConstructTime > 0 ? WorkAnimationId : IdleAnimationId);
        }

        /// <summary>
        /// Sets given material to all building sprites
        /// </summary>
        public void SetMaterialToAllSprites(Material material)
        {
            SpriteRenderer.sharedMaterial = material;
            ApplyBackgroundSpriteMaterialToChildren();
        }

        private void ApplyBackgroundSpriteMaterialToChildren()
        {
            Material material = SpriteRenderer.sharedMaterial;

            if (!material)
            {
                return;
            }

            foreach (SpriteRenderer renderer in _animatedSprites)
            {
                renderer.sharedMaterial = material;
            }
        }

        /// <summary>
        /// Enable or disable buildings vfx
        /// </summary>
        public void SwitchVfxParticles(bool isEnabled)
        {
            foreach (ParticleSystemRenderer particleSystemRenderer in _vfxParticles)
            {
                particleSystemRenderer.enabled = isEnabled;
            }
        }

        #region IPullable

        public object Clone()
        {
            return Instantiate(this);
        }

        public IObjectsPull pullSource { get; set; }

        public void OnPop()
        {
            gameObject.SetActive(true);
        }

        public void OnPush()
        {
            SetMaterialToAllSprites(IdleMaterial);
            gameObject.SetActive(false);
            transform.SetParent(null);
            Target = null;
        }

        public void FreeObject()
        {
            if (pullSource != null)
            {
                pullSource.PushInstance(this);
            }
            else
            {
                DestroyObject();
            }
        }

        public void DestroyObject()
        {
            Destroy(gameObject);
        }

        #endregion
    }
}