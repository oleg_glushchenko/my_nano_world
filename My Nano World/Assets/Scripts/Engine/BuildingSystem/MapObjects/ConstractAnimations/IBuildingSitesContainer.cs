﻿using System.Collections.Generic;
using UnityEngine;

namespace NanoReality.Engine.BuildingSystem.MapObjects.ConstractAnimations
{
    public interface IBuildingSitesContainer
    {
        List<BuildingSiteView> AnimationViewPrefabs { get; set; }

        BuildingSiteView GetAnimationView(int viewSize);

        Mesh GetBuildingSiteMesh(int size);
    }
}

