﻿using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using strange.extensions.signal.impl;

namespace NanoReality.Engine.BuildingSystem.MapObjects
{
    /// <summary>
    /// Вызывается, когда перевыбирается участок дороги
    /// </summary>
    public class SignalRoadMapReselect : Signal { }
    
    /// <summary>
    /// Вызывается когда изменяется состояние выделения объекта на карте
    /// </summary>
    public class SignalOnMapObjectChangedSelect : Signal<MapObjectView, bool> {}
    
    /// <summary>
    /// Сигнал вызывается когда какой-либо объект на карте был передвинут
    /// </summary>
    public class SignalOnMapObjectViewMoved : Signal<MapObjectView> {}
}
