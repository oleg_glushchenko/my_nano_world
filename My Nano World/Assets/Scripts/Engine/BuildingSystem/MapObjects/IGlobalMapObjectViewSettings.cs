﻿using System;
using Assets.Scripts.Engine.BuildingSystem.CityMap.Highlightings;
using UnityEngine;


namespace NanoReality.Engine.BuildingSystem.MapObjects
{
    public interface IGlobalMapObjectViewSettings
    {
        #region Slots size

        float SlotSizeSmall { get; set; }

        float SlotSizeBig { get; set; }

        #endregion

        #region Motion selection

        Color MotionSelectColor { get; set; }

        float MotionSelectTweenTime { get; set; }

        float MotionReadyProductMoveUpDownTime { get; set; }

        #endregion


        #region Semitransparent selection

        Shader DefaultShader { get; set; }

        Shader SemiTransparentShader { get; set; }

        Color SemiTransparentColor { get; set; }

        Shader BuildingHighlightingShader { get; set; }

        OutlineEffectColor BuildingNeedHappinessColor { get; set; }
        OutlineEffectColor BuildingProduceHappinessColor { get; set; }

        OutlineEffectColor BuildingNeedPowerColor { get; set; }
        OutlineEffectColor BuildingProducePowerColor { get; set; }
        OutlineEffectColor BuildingProductDragColor { get; set; }

        OutlineEffectColor BuildingNeedSupply { get; set; }
        OutlineEffectColor BuildingSupplyColor { get; set; }

        OutlineEffectColor ServiceConnectionsColor { get; set; }

        /// <summary>
        /// Цвет области покрытия электроенергии, у электростанций
        /// </summary>
        OutlineEffectColor ElectricityAreaColor { get; set; }

        /// <summary>
        /// Цвет области покртыия счастья у развлекательных зданий
        /// </summary>
        OutlineEffectColor HappinessAreaColor { get; set; }

        /// <summary>
        /// Цвет области действия общепита
        /// </summary>
        OutlineEffectColor SupplyAreaColor { get; set; }

        /// <summary>
        /// Цвет здания которое отключено от чего-либо (дорога, электричество, счастье)
        /// </summary>
        OutlineEffectColor BuildingDisabledColor { get; set; }

        Material BuildingNotFreeAreaMaterial { get; }

        Material BuildingInFreeAreaMaterial { get; }

        #endregion


        #region Defaul shader properties

        float MipMapBias { get; set; }

        #endregion


        Texture2D ReflectionTexture { get; }

        Color ColorReflect { get; }


        float ReflectColorWitdhTreshold { get; }

        Vector4 CloudSpeed { get; }

        float ReflectionPower { get; }

        float ReflectionUvDistoriton { get; }

        float CameraDisplacePower { get; }


        void SetGlobalShaderParams(float maxZoom, float minZoom);

        Shader AgroFieldsShader { get; }

        Material AoeDisabledMaterial { get; }

        Material SelectedBuildingMaterial { get; }
        
        Material BuildingSemiTransparentMaterial  { get; }
        
        Material SelectedBuildingIdleMaterial { get; }
        Material SelectedAoeBuildingIdleMaterial { get; }

        RoadConstructorSettings RoadConstructorSettings { get; }
        AoeMaterials AoeMaterials { get; }
    }

    [Serializable]
    public struct OutlineEffectColor
    {
        public Color MainColor;
        public Color OutlineColor;
    }
}