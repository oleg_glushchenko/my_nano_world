﻿using UnityEngine;
using NanoReality.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.BuildingSystem.CityMap.Highlightings;

[CreateAssetMenu(fileName = "GlobalMapObjectViewSettings", menuName = "NanoReality/Settings/GlobalMapObjectViewSettings", order = 1)]
public class GlobalMapObjectViewSettings : ScriptableObject, IGlobalMapObjectViewSettings
{
    [SerializeField] private Color _motionSelectColor;

    [SerializeField] private float _motionSelectTweenTime;
    [SerializeField] private float _motionReadyProductMoveUpDownTime;
    [SerializeField] private Shader _defaultShader;

    [SerializeField] private Shader _semiTransparentShader;
    [SerializeField] private Color _semiTransparentColor;

    [SerializeField] private Shader _buildingHighlightingShader;

    [SerializeField] private OutlineEffectColor _buildingNeedHappinessColor;
    [SerializeField] private OutlineEffectColor _buildingProduceHappinessColor;

    [SerializeField] private OutlineEffectColor _buildingNeedPowerColor;
    [SerializeField] private OutlineEffectColor _buildingProducePowerColor;
    [SerializeField] private OutlineEffectColor _buildingProductDragColor;

    [SerializeField] private OutlineEffectColor _buildingNeedSupplyColor;
    [SerializeField] private OutlineEffectColor _buildingSupplyColor;

    /// <summary>
    /// Материал области покрытия электроенергии, у электростанций
    /// </summary>
    [SerializeField] private OutlineEffectColor _electricityAreaColor;
    [SerializeField] private OutlineEffectColor _happinessAreaColor;
    [SerializeField] private OutlineEffectColor _supplyAreaColor;

    [SerializeField] private OutlineEffectColor _buildingDisabledColor;
    [SerializeField] private Material _buildingNotFreeAreaMaterial;
    [SerializeField] private Material _buildingSemiTransparentMaterial;
    [SerializeField] private Material _buildingInFreeAreaMaterial;

    [SerializeField] private OutlineEffectColor _serviceConnectionsColor;

    [SerializeField] private float _mipMapBias;
    [SerializeField] private Shader _agroFieldsShader;

    [SerializeField] private float _slotSizeSmall;
    [SerializeField] private float _slotSizeBig;

    [SerializeField] private Texture2D _reflectionTexture;
    [SerializeField] private Color _colorReflect;
    [SerializeField] private float _reflectColorWitdhTreshold;
    [SerializeField] private Vector4 _cloudSpeed;
    [SerializeField] private float _reflectionPower;
    [SerializeField] private float _reflectionUvDistoriton;
    [SerializeField] private float _cameraDisplacePower;

    [SerializeField] private Material _selectedBuildingMaterial;
    [SerializeField] private Material _selectedBuildingIdleMaterial;
    [SerializeField] private Material _selectedAoeBuildingIdleMaterial;
    [SerializeField] private Material _aoeDisabledMaterial;
    [SerializeField] private AoeMaterials _aoeMaterial;

    // Road Constructor Settings
    [SerializeField] private RoadConstructorSettings _roadConstructorSettings;

	public float SlotSizeSmall
    {
        get { return _slotSizeSmall; }
        set { _slotSizeSmall = value; }
    }

    public float SlotSizeBig
    {
        get { return _slotSizeBig; }
        set { _slotSizeBig = value; }
    }

    public Color MotionSelectColor
    {
        get { return _motionSelectColor; }
        set { _motionSelectColor = value; }
    }

    public float MotionSelectTweenTime
    {
        get { return _motionSelectTweenTime; }
        set { _motionSelectTweenTime = value; }
    }

    public float MotionReadyProductMoveUpDownTime
    {
        get { return _motionReadyProductMoveUpDownTime; }
        set { _motionReadyProductMoveUpDownTime = value; }
    }

    public Shader DefaultShader
    {
        get { return _defaultShader; }
        set { _defaultShader = value; }
    }

    public Shader SemiTransparentShader
    {
        get { return _semiTransparentShader; }
        set { _semiTransparentShader = value; }
    }

    public Color SemiTransparentColor
    {
        get { return _semiTransparentColor; }
        set { _semiTransparentColor = value; }
    }

    public Shader BuildingHighlightingShader
    {
        get { return _buildingHighlightingShader; }
        set { _buildingHighlightingShader = value; }
    }

    public OutlineEffectColor BuildingNeedHappinessColor
    {
        get { return _buildingNeedHappinessColor; }
        set { _buildingNeedHappinessColor = value; }
    }

    public OutlineEffectColor BuildingProduceHappinessColor
    {
        get { return _buildingProduceHappinessColor; }
        set { _buildingProduceHappinessColor = value; }
    }

    public OutlineEffectColor BuildingNeedPowerColor
    {
        get { return _buildingNeedPowerColor; }
        set { _buildingNeedPowerColor = value; }
    }

    public OutlineEffectColor BuildingProducePowerColor
    {
        get { return _buildingProducePowerColor; }
        set { _buildingProducePowerColor = value; }
    }

    public OutlineEffectColor BuildingNeedSupply
    {
        get { return _buildingNeedSupplyColor; }
        set { _buildingNeedSupplyColor = value; }
    }

    public OutlineEffectColor BuildingSupplyColor
    {
        get { return _buildingSupplyColor; }
        set { _buildingSupplyColor = value; }
    }

    public OutlineEffectColor BuildingProductDragColor
    {
        get { return _buildingProductDragColor; }
        set { _buildingProductDragColor = value; }
    }

    public OutlineEffectColor ElectricityAreaColor
    {
        get { return _electricityAreaColor; }
        set { _electricityAreaColor = value; }
    }

    public OutlineEffectColor HappinessAreaColor
    {
        get { return _happinessAreaColor; }
        set { _happinessAreaColor = value; }
    }

    public OutlineEffectColor SupplyAreaColor
    {
        get { return _supplyAreaColor; }
        set { _supplyAreaColor = value; }
    }

    public OutlineEffectColor BuildingDisabledColor
    {
        get { return _buildingDisabledColor; }
        set { _buildingDisabledColor = value; }
    }

    public OutlineEffectColor ServiceConnectionsColor
    {
        get { return _serviceConnectionsColor; }
        set { _serviceConnectionsColor = value; }
    }

    public Material BuildingSemiTransparentMaterial => _buildingSemiTransparentMaterial;
    
    public Material BuildingNotFreeAreaMaterial => _buildingNotFreeAreaMaterial;

    public Material BuildingInFreeAreaMaterial => _buildingInFreeAreaMaterial;

    #region Defaul shader properties

    public float MipMapBias
    {
        get { return _mipMapBias; }
        set { _mipMapBias = value; }
    }

    #endregion


    #region Reflection


    public Texture2D ReflectionTexture
    {
        get { return _reflectionTexture; }
    }

    public Color ColorReflect
    {
        get { return _colorReflect; }
    }


    public float ReflectColorWitdhTreshold
    {
        get { return _reflectColorWitdhTreshold; }
    }

    public Vector4 CloudSpeed
    {
        get { return _cloudSpeed; }
    }

    public float ReflectionPower
    {
        get { return _reflectionPower; }
    }

    public float ReflectionUvDistoriton
    {
        get { return _reflectionUvDistoriton; }
    }

    public float CameraDisplacePower
    {
        get { return _cameraDisplacePower; }
    }

    #endregion


    public void SetGlobalShaderParams(float maxZoom, float minZoom)
    {
        Shader.SetGlobalFloat("_MainTexBias", MipMapBias);
        Shader.SetGlobalFloat("_CameraMaxZoom", maxZoom);
        Shader.SetGlobalFloat("_CameraMinZoom", minZoom);
    }

    public Shader AgroFieldsShader
    {
        get { return _agroFieldsShader; }
    }

    public Material AoeDisabledMaterial => _aoeDisabledMaterial;
    public Material SelectedBuildingMaterial => _selectedBuildingMaterial;
    public Material SelectedBuildingIdleMaterial => _selectedBuildingIdleMaterial;
    public Material SelectedAoeBuildingIdleMaterial => _selectedAoeBuildingIdleMaterial;
    public AoeMaterials AoeMaterials => _aoeMaterial;
    public RoadConstructorSettings RoadConstructorSettings => _roadConstructorSettings;
}
    
