﻿using System;
using System.Collections.Generic;
using Assets.NanoLib.Utilities.Pulls;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using System.Linq;
using NanoReality.Core.Resources;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using strange.extensions.injector.api;
using UnityEngine;

namespace NanoReality.Engine.BuildingSystem.MapObjects
{
    public class UnityMapObjectsGenerator : IMapObjectsGenerator
    {
        #region Injects

        [Inject] public IInjectionBinder jInjectionBinder { get; set; }
        [Inject] public IResourcesManager jResourcesManager { get; set; }

        #endregion

        private MapObjectId _defaultMapObjectId = new MapObjectId(-1, 0);

        public List<KeyValuePair<Id_IMapObjectType, MapObjectView>> ObjectViewsPrototypes { get; set; }

        public Dictionary<int, List<IObjectPullWithLevel>> ObjectViewsPulls { get; set; }

        public Dictionary<MapObjectId, IMapObject> ObjectModelsPrototypes { get; set; }

        public List<KeyValuePair<Id_IMapObjectType, IObjectPullWithLevel>> ObjectModelsPulls { get; set; }

        [PostConstruct]
        public void PostConstruct()
        {
            ObjectViewsPulls = new Dictionary<int, List<IObjectPullWithLevel>>(300);
            ObjectViewsPrototypes = new List<KeyValuePair<Id_IMapObjectType, MapObjectView>>(300);

            ObjectModelsPulls = new List<KeyValuePair<Id_IMapObjectType, IObjectPullWithLevel>>(300);
            ObjectModelsPrototypes = new Dictionary<MapObjectId, IMapObject>(300);
        }

        [Deconstruct]
        public void Deconstruct()
        {
            ObjectViewsPulls.Clear();
            ObjectViewsPrototypes.Clear();
            ObjectModelsPulls.Clear();
            ObjectModelsPrototypes.Clear();
        }

        #region IMapObjectsGenerator implementation

        public int GetMaxLevel(IMapBuilding building)
        {
            var buildingType = building.MapObjectType;
            var buildingTypeID = building.ObjectTypeID;

            int maxLevel = 0;
            foreach (var prototype in ObjectModelsPrototypes)
            {
                var mapObj = prototype.Value;
                if (mapObj.MapObjectType == buildingType && mapObj.ObjectTypeID == buildingTypeID)
                {
                    int objLevel = mapObj.Level;
                    if (maxLevel < objLevel)
                    {
                        maxLevel = objLevel;
                    }
                }
            }

            return maxLevel;
        }

        #region Model methods

        /// <summary>
        /// Returns new instance of map objects
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        public IMapObject GetMapObjectModel(Id_IMapObjectType ID, int level)
        {
            KeyValuePair<Id_IMapObjectType, IObjectPullWithLevel> resultPair =
                ObjectModelsPulls.Find(e => e.Key == ID && e.Value.Level == level);

            IObjectPullWithLevel resultModel = resultPair.Value;
            if (resultModel == null)
            {
                Debug.LogError("MapObject with id:" + ID + " and level:" + level + " was not found!");
                return null;
            }

            return resultModel.GetInstance() as IMapObject;
        }

        public void AddObjectModel(IMapObject prototype)
        {
           var newPull = new MObjectsPull {Level = prototype.Level};
           newPull.InstanitePull(prototype);

            var valuePair = new KeyValuePair<Id_IMapObjectType, IObjectPullWithLevel>(prototype.ObjectTypeID, newPull);
            ObjectModelsPrototypes.Add(prototype.Id, prototype);
            ObjectModelsPulls.Add(valuePair);
        }

        public void RemoveObjectModel(IMapObject prototype)
        {
            ObjectModelsPrototypes.Remove(prototype.Id);
            ObjectModelsPulls.RemoveAll(e => e.Key == prototype.ObjectTypeID && e.Value.Level == prototype.Level);
        }

        #endregion

        #region View methods

        public MapObjectView GetMapObjectView(MapObjectId id)
        {
            MapObjectView viewInstance;
            
            MapObjectId visualId = CalculateBuildingVisualStyleLevel(id);
            IObjectPullWithLevel viewPool = GetViewPool(visualId);
            
            if (viewPool != null)
            {
                viewInstance = viewPool.GetInstance() as MapObjectView;
            }
            else if (CreateNewViewPool(visualId))
            {
                viewInstance = GetViewPool(visualId).GetInstance() as MapObjectView;
            }
            else
            {
                viewInstance = GetViewPool(new MapObjectId(0)).GetInstance() as MapObjectView;
                Debug.LogWarning($"Can't find MapObjectView for id: {id}");
            }

            jInjectionBinder.injector.Inject(viewInstance);
            return viewInstance;
        }

        public void GetMapObjectView(MapObjectId id, Action<MapObjectView> callback)
        {
            MapObjectView mapObjectView = GetMapObjectView(id);
            callback?.Invoke(mapObjectView);
        }
        
        #endregion

        public void Clear()
        {
            ObjectModelsPrototypes.Clear();

            foreach (var objectModelsPull in ObjectModelsPulls)
            {
                objectModelsPull.Value.ClearPull();
            }

            ObjectModelsPulls.Clear();
            ObjectViewsPrototypes.Clear();

            foreach (var valuePair in ObjectViewsPulls)
            {
                List<IObjectPullWithLevel> poolsByLevelList = valuePair.Value;
                foreach (IObjectPullWithLevel pull in poolsByLevelList)
                {
                    pull.ClearPull();
                }
            }

            ObjectViewsPulls.Clear();
        }

        #endregion


        private bool CreateNewViewPool(MapObjectId id, int preInstancesCount = 0)
        {
            try
            {
                MapObjectView viewPrefab = jResourcesManager.GetBuildingPrefab<MapObjectView>(id);

                if (viewPrefab == null)
                {
                    Debug.LogError($"Building prefab missing. Id: {id.Id} Level: {id.Level}");
                    return false;
                }

                var newViewsPool = jInjectionBinder.GetInstance<IObjectPullWithLevel>();
                newViewsPool.Level = id.Level;
                newViewsPool.InstanitePull(viewPrefab, preInstancesCount);

                var prototypePair = new KeyValuePair<Id_IMapObjectType, MapObjectView>(id.Id, viewPrefab);

                ObjectViewsPrototypes.Add(prototypePair);

                if (!ObjectViewsPulls.TryGetValue(id.Id, out List<IObjectPullWithLevel> poolsList))
                {
                    poolsList = new List<IObjectPullWithLevel>();
                    ObjectViewsPulls.Add(id.Id, poolsList);
                }

                poolsList.Add(newViewsPool);
            }
            catch (Exception e)
            {
                Debug.LogError(e);
                return false;
            }

            return true;
        }

        private IObjectPullWithLevel GetViewPool(MapObjectId id)
        {
            if (ObjectViewsPulls.TryGetValue(id.Id, out List<IObjectPullWithLevel> pullsList))
            {
                return pullsList.FirstOrDefault(c => c.Level == id.Level);
            }

            return null;
        }

        public MapObjectId CalculateBuildingVisualStyleLevel(MapObjectId id)
        {
            int level = id.Level;
            int visualLevel = 0;

            if (!ObjectModelsPrototypes.TryGetValue(id, out IMapObject building))
            {
                return default;
            }

            MapObjectintTypes objectType = building.MapObjectType;

            switch (objectType)
            {
                case MapObjectintTypes.None:
                {
                    Debug.LogError("Not valid map object type.");
                    break;
                }

                case MapObjectintTypes.CityHall:
                {
                    if (level >= 4)
                    {
                        visualLevel = 3;
                    }
                    else
                    {
                        visualLevel = level; 
                    }
                    break;
                }
                case MapObjectintTypes.DwellingHouse:
                {
                    visualLevel = level > 5 ? 5 : level;
                    break;
                }
                default:
                {
                    visualLevel = id.Level;
                    break;
                }
            }
            
            return new MapObjectId(id.Id, visualLevel);
        }
    }
}
    
