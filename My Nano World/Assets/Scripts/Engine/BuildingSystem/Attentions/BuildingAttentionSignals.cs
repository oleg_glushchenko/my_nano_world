using NanoReality.Engine.UI.Views.WorldSpaceUI;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using strange.extensions.signal.impl;

namespace NanoReality.Game.Attentions
{
    public sealed class SignalOnBuildingAttentionTap : Signal<BuildingStatesAttentionView> { }
    
    public sealed class SignalOnTaxesAttentionTap : Signal { }
    
    public sealed class SignalOnFoodSupplyAttentionTap : Signal { }

    public sealed class SignalOnDwellingHouseCoinAttentionTap: Signal<IDwellingHouseMapObject> { }

    public sealed class SignalOnBuildingUpgradeAttentionTap: Signal<IMapBuilding> { }

    public sealed class SignalOnQuestsBuildingQuestsAttentionTap: Signal<IQuestsBuilding> { }
}