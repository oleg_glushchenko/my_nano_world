﻿using System;
using NanoReality.Engine.UI.Views.WorldSpaceUI;

namespace NanoReality.Game.Attentions
{
	public interface IBuildingAttentionsController
	{
		BuildingStatesAttentionView AddAttentionView(int buildingId);
		BuildingStatesAttentionView GetAttentionView(int buildingId);
		void RemoveBuildingStatesAttentionView(int buildingId);
		void RemoveAllStatesAttentionView();
		void OrderBuildingStatesAttentionViews();
		void SetActiveAttentions(bool isActive);

		/// <summary>
		/// Returns new instance of attention policy for target building type.
		/// </summary>
		/// <param name="buildingType">Target IMapObject inherited type.</param>
		/// <returns></returns>
		BuildingAttentionPolicy GetAttentionPolicy(Type buildingType);
	}
}