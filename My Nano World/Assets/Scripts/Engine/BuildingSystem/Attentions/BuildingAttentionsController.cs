using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.api.CityOrderDesks;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.api.OrderDesks;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl.Mines;
using NanoLib.Core.Pool;
using NanoReality.Engine.BuildingSystem.CityCamera;
using NanoReality.Engine.UI;
using NanoReality.Engine.UI.Views.WorldSpaceUI;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.impl;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.impl.Factories;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.SupplyFood;
using strange.extensions.injector.api;
using strange.extensions.signal.impl;
using UniRx;
using UnityEngine;

namespace NanoReality.Game.Attentions
{
    /// <summary>
    /// Enable/disable building attentions views.
    /// 1st arg - IsActive.
    /// </summary>
    public sealed class SwitchBuildingAttentionsSignal : Signal<bool> {}
    
    /// <summary>
    /// Turn on/off interactability of building attentions
    /// </summary>
    public sealed class InteractBuildingAttentionsSignal : Signal<bool> {}
    
    public sealed class BuildingAttentionsController : IBuildingAttentionsController
    {
        [Inject] public IInjectionBinder jInjectionBinder { get; set; }
        [Inject] public IWorldSpaceCanvas jWorldSpaceCanvas { get; set; }
        [Inject] public IUnityPool<BuildingStatesAttentionView> jBuildingAttentionsPool { get; set; }
        [Inject] public UpdateWorldSpaceDepth jUpdateWorldSpaceDepth { get; set; }
        [Inject] public IGameCameraModel jGameCameraModel { get; set; }
        [Inject] public SwitchBuildingAttentionsSignal jSwitchBuildingAttentionsSignal { get; set; }
        [Inject] public InteractBuildingAttentionsSignal jInteractBuildingAttentionsSignal { get; set; }
        [Inject] public SignalOnConstructControllerChangeState jSignalOnConstructControllerChangeState { get; set; }

        private readonly Dictionary<Type, Type> _bindedAttentionPolicy = new Dictionary<Type, Type>()
        {
            { typeof(MDwellingHouseMapObject), typeof(DwellingBuildingAttentionPolicy) },
            { typeof(MCityHall), typeof(CityHallAttentionPolicy) },
            { typeof(FoodSupplyBuilding), typeof(FoodSupplyAttentionPolicy) },
            { typeof(TradingMarketBuilding), typeof(TradingMarketAttentionPolicy) },
            { typeof(MQuestsBuilding), typeof(QuestBuildingAttentionPolicy) },
            { typeof(CityOrderDeskMapBuilding), typeof(MetroAttentionPolicy) },
            { typeof(OrderDeskMapBuilding), typeof(SeaportAttentionPolicy) },
            { typeof(MineObject), typeof(MineAttentionPolicy) },
            { typeof(FactoryObject), typeof(FactoryAttentionPolicy) }
        };

        private readonly Type _defaultBuildingStrategyType = typeof(DefaultBuildingAttentionPolicy);

        private readonly Dictionary<int, BuildingStatesAttentionView> _activeAttentionViews;
        private bool _buildingAttentionAdded;
        private IDisposable _cameraEffectDisposable;

        public BuildingAttentionsController()
        {
            _activeAttentionViews = new Dictionary<int, BuildingStatesAttentionView>();
        }

        [PostConstruct]
        public void PostConstruct()
        {
            jUpdateWorldSpaceDepth.AddListener(OrderBuildingStatesAttentionViews);
            jSwitchBuildingAttentionsSignal.AddListener(SetActiveAttentions);
            jInteractBuildingAttentionsSignal.AddListener(OnInteractBuildingAttentionsSignal);
            jSignalOnConstructControllerChangeState.AddListener(OnReplaceModeEnabled);
            _cameraEffectDisposable = jGameCameraModel.GrayscaleActiveProperty.Subscribe(x => SetActiveAttentions(!x));
            jBuildingAttentionsPool.InstanceProvider = new PrefabInstanceProvider(jWorldSpaceCanvas.BuildingAttentionViewPrefab);

            BindAttentionPolicies();
        }

        [Deconstruct]
        public void Deconstruct()
        {
            jUpdateWorldSpaceDepth.RemoveListener(OrderBuildingStatesAttentionViews);
            jSwitchBuildingAttentionsSignal.RemoveListener(SetActiveAttentions);
            jInteractBuildingAttentionsSignal.RemoveListener(OnInteractBuildingAttentionsSignal);
            jSignalOnConstructControllerChangeState.RemoveListener(OnReplaceModeEnabled);
            _cameraEffectDisposable?.Dispose();
        }

        private void BindAttentionPolicies()
        {
            foreach (KeyValuePair<Type,Type> keyValuePair in _bindedAttentionPolicy)
            {
                Type policyType = keyValuePair.Value;
                jInjectionBinder.Bind(policyType).To(policyType);
            }

            jInjectionBinder.Bind(_defaultBuildingStrategyType).To(_defaultBuildingStrategyType);
        }

        #region Events

        private void OnInteractBuildingAttentionsSignal(bool isInteractable)
        {
            SetAttentionsInteractable(isInteractable);
        }

        #endregion

        public BuildingAttentionPolicy GetAttentionPolicy(Type buildingType)
        {
            if(!_bindedAttentionPolicy.TryGetValue(buildingType, out var strategyType))
            {
                strategyType = _defaultBuildingStrategyType;
            }

            var strategyInstance = (BuildingAttentionPolicy)jInjectionBinder.GetInstance(strategyType);
            return strategyInstance;
        }

        public void SetAttentionsInteractable(bool interactable)
        {
            foreach (var attention in _activeAttentionViews)
            {
                attention.Value.SetButtonInteractability(interactable);
            }
        }

        public BuildingStatesAttentionView AddAttentionView(int buildingId)
        {
            BuildingStatesAttentionView instance;
            if (_activeAttentionViews.ContainsKey(buildingId))
            {
                instance = _activeAttentionViews[buildingId];
            }
            else
            {
                instance = jBuildingAttentionsPool.Pop();
                instance.transform.SetParent(jWorldSpaceCanvas.BuildingAttentionsParent); 
                _activeAttentionViews.Add(buildingId, instance);
            }

            instance.SetButtonInteractability(true);

            _buildingAttentionAdded = true;
            
            return instance;
        }

        public BuildingStatesAttentionView GetAttentionView(int buildingId)
        {
            return _activeAttentionViews.LastOrDefault(i => i.Key == buildingId).Value;
        }

        public void RemoveBuildingStatesAttentionView(int buildingId)
        {
            BuildingStatesAttentionView attentionView;
            if (!_activeAttentionViews.TryGetValue(buildingId, out attentionView))
            {
                return;
            }
            jBuildingAttentionsPool.Push(attentionView);
            _activeAttentionViews.Remove(buildingId);
        }
        
        public void RemoveAllStatesAttentionView()
        {
            foreach (var view in _activeAttentionViews.Values)
            {
                jBuildingAttentionsPool.Push(view);
            }
            _activeAttentionViews.Clear();
        }
        
        public void SetActiveAttentions(bool isActive)
        {
            jWorldSpaceCanvas.BuildingAttentionsParent.gameObject.SetActive(isActive);
            jWorldSpaceCanvas.CitizensGiftsAttentionsParent.gameObject.SetActive(isActive);
        }

        private void OnReplaceModeEnabled(bool isEnabled)
        {
            SetActiveAttentions(!isEnabled);
        }

        private int BuildingStatesAttentionsComparison(BuildingStatesAttentionView a, BuildingStatesAttentionView b)
        {
            return Mathf.RoundToInt((a.CurrentTarget.IsoDepth - b.CurrentTarget.IsoDepth) * 100f); // * 100f is needed because IsoDepth can differ by less than 1
        }

        public void OrderBuildingStatesAttentionViews()
        {
            List<int> idsForRemove = _activeAttentionViews.Where(c => c.Value == null || c.Value.CurrentTarget == null).Select(c => c.Key).ToList();

            foreach (int buildingId in idsForRemove)
            {
                _activeAttentionViews.Remove(buildingId);
            }  
            
            var attentionsList2 = _activeAttentionViews.Select(c => c.Value).ToList();
            attentionsList2.Sort(BuildingStatesAttentionsComparison);
            
            for (var i = 0; i < attentionsList2.Count; i++)
            {
                attentionsList2[i].transform.SetSiblingIndex(i);
            }
        }
    }
}