using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl;

namespace NanoReality.Game.Attentions
{
    public sealed class DefaultBuildingAttentionPolicy : BuildingAttentionPolicy<MapBuilding>
    {
        public override AttentionData CalculateAttention()
        {
            AttentionData data = new AttentionData(AttentionsTypes.None);
            AddBasicAttention(data);
            AddUpgradeAttention(data);
            
            return data;
        }
    }
}