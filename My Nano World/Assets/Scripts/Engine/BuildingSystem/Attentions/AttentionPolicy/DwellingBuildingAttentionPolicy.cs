using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace NanoReality.Game.Attentions
{
    public sealed class DwellingBuildingAttentionPolicy : BuildingAttentionPolicy<IDwellingHouseMapObject>
    {
        public override AttentionData CalculateAttention()
        {
            AttentionData attentionData = new AttentionData(AttentionsTypes.None);
            AddBasicAttention(attentionData);
            AddUpgradeAttention(attentionData);

            return attentionData;
        }
    }
}