using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl;
using GameLogic.Taxes.Service;
using NanoLib.Core.Logging;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Impl;
using NanoReality.Game.Tutorial;
using NanoReality.GameLogic.Configs;
using NanoReality.GameLogic.Utilities;
using UnityEngine;

namespace NanoReality.Game.Attentions
{
    public sealed class CityHallAttentionPolicy : BuildingAttentionPolicy<MCityHall>
    {
        private bool _hasBeenShown;
        
        [Inject] public IHardTutorial jHardTutorial { get; set; }
        [Inject] public ICityTaxesService jCityTaxesService { get; set; }
        [Inject] public IGameConfigData jGameConfigData { get; set; }
        
        private CityTaxes CityTaxes => jCityTaxesService.CityTaxes;
        private float CollectedCoinsToShow => jGameConfigData.CollectedCoinsToShow;
        
        private bool NeedShowAttention
        {
            get
            {
                float collectedPercents =
                    Mathf.Clamp01(CityTaxes.CurrentCoins / (CityTaxes.MaxTaxes * CityTaxes.CoinsPerSecond));
                return collectedPercents >= CollectedCoinsToShow;
            }
        }

        public override AttentionData CalculateAttention()
        {
            AttentionData data = new AttentionData(AttentionsTypes.None);

            AddCityHallAttention(data);
            AddBasicAttention(data);

            if (!BuildingModel.IsLocked)
            {
                AddUpgradeAttention(data);
            }

            return data;
        }

        private void AddCityHallAttention(AttentionData data)
        {
            if (BuildingModel.IsLocked)
            {
                return;
            }
            
            if (!jHardTutorial.IsTutorialCompleted)
            {
                return;
            }

            if (CityTaxes == null)
            {
                Logging.LogError("By some reason CityTaxes is null.");
                return;
            }

            if (CityTaxes.CurrentCoins > 0 && _hasBeenShown)
            {
                FlagsHelper.Set(ref data.AttentionType, AttentionsTypes.Coin);
            }
            else if (NeedShowAttention)
            {
                FlagsHelper.Set(ref data.AttentionType, AttentionsTypes.Coin);
                _hasBeenShown = true;
            }
            else
            {
                _hasBeenShown = false;
            }
        }
    }
}
