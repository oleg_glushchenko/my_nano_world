using System.Linq;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.api.OrderDesks;
using NanoReality.Game.Services.OrderDesk.SeaportOrderDesk;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.Utilities;
using UnityEngine;

namespace NanoReality.Game.Attentions
{
    public sealed class SeaportAttentionPolicy : BuildingAttentionPolicy<OrderDeskMapBuilding>
    {
        [Inject] public ISeaportOrderDeskService jSeaportOrderDeskService { get; set; }
        
        public override AttentionData CalculateAttention()
        {
            AttentionData data = new AttentionData(AttentionsTypes.None);

            AddOrderDeskAttention(data);
            AddBasicAttention(data);
            AddUpgradeAttention(data);

            return data;
        }

        private void AddOrderDeskAttention(AttentionData data)
        {
            if (BuildingModel.OrderDeskSectorType != BusinessSectorsTypes.HeavyIndustry)
                return;
            
            foreach (var slot in jSeaportOrderDeskService.SeaportCollection)
            {
                if(CanBeSent(slot))
                {
                    FlagsHelper.Set(ref data.AttentionType, AttentionsTypes.OrderDesk);
                    return;
                }
            }
        }

        private bool CanBeSent(ISeaportOrderSlot slot)
        {
            if (slot.IsShuffling)
            {
                return false;
            }
            
            foreach (var orderItem in slot.ProductOrderItems)
            {
                int hasCount = jPlayerProfile.PlayerResources.GetProductCount(orderItem.ProductID);
                if (orderItem.Count > hasCount)
                    return false;
            }

            return true;
        }
    }
}