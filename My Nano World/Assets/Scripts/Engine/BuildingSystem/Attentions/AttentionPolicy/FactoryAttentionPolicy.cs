using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.impl.Factories;
using NanoReality.GameLogic.Utilities;

namespace NanoReality.Game.Attentions
{
    /// <summary>
    /// Now, using for Factory and Mine objects.
    /// </summary>
    public sealed class FactoryAttentionPolicy : BuildingAttentionPolicy<FactoryObject>
    {
        public override AttentionData CalculateAttention()
        {
            AttentionData data = new AttentionData(AttentionsTypes.None);

            AddFactoryAttention(data);
            AddBasicAttention(data);
            AddUpgradeAttention(data);

            return data;
        }

        private void AddFactoryAttention(AttentionData data)
        {
            // if we have some product in production queue, unset ReadyForUpgrade attention type.
            if (FlagsHelper.IsSet(data.AttentionType, AttentionsTypes.ReadyForUpgrade) && !BuildingModel.IsProductionEmpty())
            {
                FlagsHelper.Unset(ref data.AttentionType, AttentionsTypes.ReadyForUpgrade);
            }
        }
    }
}