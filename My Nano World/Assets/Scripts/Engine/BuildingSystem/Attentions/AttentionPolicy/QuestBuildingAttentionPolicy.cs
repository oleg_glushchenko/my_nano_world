using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl;
using NanoReality.GameLogic.Utilities;

namespace NanoReality.Game.Attentions
{
    public sealed class QuestBuildingAttentionPolicy : BuildingAttentionPolicy<MQuestsBuilding>
    {
        public override AttentionData CalculateAttention()
        {
            AttentionData data = new AttentionData(AttentionsTypes.None);

            AddQuestAttention(data);
            AddBasicAttention(data);
            AddUpgradeAttention(data);

            return data;
        }

        private void AddQuestAttention(AttentionData data)
        {
            if (BuildingModel.AchievedQuestsCount > 0)
            {
                FlagsHelper.Set(ref data.AttentionType, AttentionsTypes.QuestsAchieved);
                return;
            }

            if (BuildingModel.ActiveQuestsCount > 0)
            {
                FlagsHelper.Set(ref data.AttentionType, AttentionsTypes.QuestsAvailable);
            }
        }
    }
}