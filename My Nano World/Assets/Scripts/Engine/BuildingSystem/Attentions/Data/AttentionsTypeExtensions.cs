﻿using System;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.ServiceBuildings;

namespace NanoReality.Game.Attentions
{
    public static class AttentionsTypeExtensions
    {
        /// <summary>
        /// Returns only clean flag value of highest prioritized type.
        /// For instance, Road is higher than Electricity (the lower the value the higher priority).
        /// </summary>
        /// <param name="types"></param>
        /// <returns></returns>
        public static AttentionsTypes GetOnlyHighest(this AttentionsTypes types)
        {
            var checkTypeValues = Enum.GetValues(typeof(AttentionsTypes));
            foreach (AttentionsTypes value in checkTypeValues)
            {
                if (value != AttentionsTypes.None && (types & value) == value)
                {
                    return value;
                }
            }
            return AttentionsTypes.None;
        }
    }
}