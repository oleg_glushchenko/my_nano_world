﻿namespace NanoReality.Game.Attentions
{
	public sealed class AttentionData
	{
		public AttentionsTypes AttentionType;

	    public AttentionData(AttentionsTypes attentionType)
	    {
	        AttentionType = attentionType;
	    }

	    public override bool Equals(object obj)
	    {
	        AttentionData castedObj = obj as AttentionData;
            if (castedObj == null)
	            return false;

	        return AttentionType == castedObj.AttentionType;
	    }

	    public override int GetHashCode()
	    {
	        return (int) AttentionType;
	    }
	}
}