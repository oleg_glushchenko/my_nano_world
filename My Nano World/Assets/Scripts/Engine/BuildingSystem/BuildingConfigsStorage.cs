using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace NanoReality.Engine.BuildingSystem
{
    [CreateAssetMenu(fileName = "BuildingPrefabsStorage", menuName = "NanoReality/Buildings/BuildingPrefabStorage", order = 1)]
    public sealed class BuildingConfigsStorage : ScriptableObject, IBuildingConfigsStorage
    {
        [SerializeField] private string _configsFolderPath;
        [SerializeField] private List<BuildingConfigData> _buildingConfigs;

        private string SearchAssetsFilter => $"t:{typeof(BuildingConfig).Name}";
        
        #region IBuildingConfigsStorage implementation
        
        public string GetBuildingConfigPath(int buildingId)
        {
            string absolutePath = _buildingConfigs.FirstOrDefault(c => c.BuildingId == buildingId)?.ConfigPath;
            return absolutePath;
        }

        #endregion
        
        /// <summary>
        /// This method find all BuildingConfig assets in configs folder. And add it to building configs list.
        /// </summary>
        [ContextMenu("UpdateBuildingConfigsList")]
        public void UpdateBuildingConfigsList()
        {
            #if UNITY_EDITOR
            
            try
            {
                var newConfigsList = new List<BuildingConfigData>();
                
                string[] configsGuidList = UnityEditor.AssetDatabase.FindAssets(SearchAssetsFilter, new [] {_configsFolderPath});
                
                foreach (var configGuid in configsGuidList)
                {
                    string configAssetPath = UnityEditor.AssetDatabase.GUIDToAssetPath(configGuid);
                    var buildingConfig = UnityEditor.AssetDatabase.LoadAssetAtPath<BuildingConfig>(configAssetPath);
                
                    newConfigsList.Add(new BuildingConfigData(buildingConfig.BuildingId, configAssetPath));
                }

                foreach (BuildingConfigData newConfig in newConfigsList)
                {
                    if (!_buildingConfigs.Contains(newConfig))
                    {
                        Debug.Log($"Added new building. Id: [{newConfig.BuildingId}] Path: [{newConfig.ConfigPath}]");
                    }
                }
                
                foreach (BuildingConfigData oldConfig in _buildingConfigs)
                {
                    if (!newConfigsList.Contains(oldConfig))
                    {
                        Debug.Log($"Removed building config. Id: [{oldConfig.BuildingId}] Path: [{oldConfig.ConfigPath}]");
                    }
                }

                _buildingConfigs = newConfigsList;
                UnityEditor.EditorUtility.SetDirty(this);
                Debug.Log($"Success. Building configs list updated successful. Total configs count: {_buildingConfigs.Count}");
            }
            catch (Exception e)
            {
                Debug.LogError("Error. Building configs list didn't update. Exception: " + e);
            }
            
            #endif
        }
        
        /// <summary>
        /// This method find all BuildingConfig assets in configs folder and check theirs parameter for nullable.
        /// </summary>
        [ContextMenu("ValidateBuildingConfigs")]
        public void ValidateBuildingConfigs()
        {
#if UNITY_EDITOR
            
            try
            {
                string[] configsGuidList = UnityEditor.AssetDatabase.FindAssets(SearchAssetsFilter, new [] {_configsFolderPath});

                foreach (var configGuid in configsGuidList)
                {
                    string configAssetPath = UnityEditor.AssetDatabase.GUIDToAssetPath(configGuid);
                    var buildingConfig = UnityEditor.AssetDatabase.LoadAssetAtPath<BuildingConfig>(configAssetPath);

                    if (buildingConfig.ViewsList == null || buildingConfig.ViewsList.Count == 0)
                    {
                        Debug.LogError($"BuildingConfig Name:{buildingConfig.name} Id:{buildingConfig.BuildingId} has nullable or empty views list.", buildingConfig);
                        continue;
                    }

                    foreach (BuildingLevelViewData levelViewData in buildingConfig.ViewsList)
                    {
                        if (levelViewData.View == null)
                        {
                            Debug.LogError($"BuildingConfig Name:{buildingConfig.name} Id:{buildingConfig.BuildingId} has nullable view for level {levelViewData.Level}.", buildingConfig);
                        }

                        if (levelViewData.Sprite == null)
                        {
                            Debug.LogError($"BuildingConfig Name:{buildingConfig.name} Id:{buildingConfig.BuildingId} has null sprite value for level: {levelViewData.Level}", buildingConfig);
                        }
                    }
                }
                
                Debug.Log($"Building configs list validation check success. Total configs count: {_buildingConfigs.Count}");
            }
            catch (Exception e)
            {
                Debug.LogError("Error. Building configs list didn't update. Exception: " + e);
            }
            
#endif
        }
        
        /// <summary>
        /// Helper class for keeping path to building config.
        /// </summary>
        [Serializable]
        private class BuildingConfigData
        {
            [SerializeField] private int _buildingId;
            [SerializeField] private string _configPath;

            public int BuildingId => _buildingId;

            public string ConfigPath => _configPath;

            public BuildingConfigData()
            {
                
            }

            public BuildingConfigData(int buildingId, string configPath)
            {
                _buildingId = buildingId;
                _configPath = configPath;
            }
            
            protected bool Equals(BuildingConfigData other)
            {
                return _buildingId == other._buildingId && _configPath == other._configPath;
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType()) return false;
                return Equals((BuildingConfigData) obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    return (_buildingId * 397) ^ (_configPath != null ? _configPath.GetHashCode() : 0);
                }
            }
        }
    }

    /// <summary>
    /// Contains building configs paths from resources folder.
    /// </summary>
    public interface IBuildingConfigsStorage
    {
        /// <summary>
        /// Return path to building config in Resources folder.
        /// </summary>
        /// <param name="buildingId">Building id.</param>
        /// <returns>Path in Resources folder.</returns>
        string GetBuildingConfigPath(int buildingId);
    }


}