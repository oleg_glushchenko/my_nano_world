﻿using Assets.Scripts.GameLogic.Utilites;
using strange.extensions.signal.impl;
using UnityEngine;
using UnityEngine.EventSystems;

namespace NanoReality.Engine.Utilities
{
    /// <summary>
    /// Предназначен для определения типа улучшения и показа тултипа по лонг тапу
    /// </summary>
    public class UpgradeLongTapControl : MonoBehaviour, IPointerUpHandler
    {
        /// <summary>
        /// Вид улучшения данного итема (добавит слот, ускорит производство или откроет новый вид кристалов)
        /// </summary>
        [SerializeField]
        private UpgradeControlType _upgradeControlType;

        /// <summary>
        /// Используется для отображения в тултипе информации об улучшении
        /// </summary>
        public string ControlName { get; private set; }

        /// <summary>
        /// Long press component
        /// </summary>
        private LongPressEventTrigger _longPress;

        public Signal<UpgradeLongTapControl> SignalOnLongPress = new Signal<UpgradeLongTapControl>();
        public Signal SignalOnPointerUp = new Signal();

        /// <summary>
        /// Скрываем тултип по завершению лонг тапа
        /// </summary>
        /// <param name="eventData"></param>
        public void OnPointerUp(PointerEventData eventData)
        {
            SignalOnPointerUp.Dispatch();
        }

        private void InitControl()
        {
            switch (_upgradeControlType)
            {
                case UpgradeControlType.Crystal:
                    ControlName = LocalizationMLS.Instance.GetText(LocalizationKeyConstants.UPGRADEPANEL_TOOLTIP_CRYSTAL);
                    break;
                case UpgradeControlType.Slot:
                    ControlName = LocalizationMLS.Instance.GetText(LocalizationKeyConstants.UPGRADEPANEL_TOOLTIP_SLOT);
                    break;
                case UpgradeControlType.Speed:
                    ControlName = LocalizationMLS.Instance.GetText(LocalizationKeyConstants.UPGRADEPANEL_TOOLTIP_SPEED);
                    break;
                case UpgradeControlType.Experience:
                    ControlName = LocalizationMLS.Instance.GetText(LocalizationKeyConstants.UPGRADEPANEL_TOOLTIP_EXP);
                    break;

                case UpgradeControlType.IncreaseCityHallMoneyPool:
                    ControlName = LocalizationMLS.Instance.GetText(LocalizationKeyConstants.UPGRADEPANEL_TOOLTIP_INCREASE_MONEY_POOL);
                    break;
                    
                case UpgradeControlType.DwellingHousesMaxCount:
                    ControlName = "UPGRADEPANEL_TOOLTIP_INCREASE_DWELLING_HOUSES";//LocalizationMLS.Instance.GetText(LocalizationKeyConstants.UPGRADEPANEL_TOOLTIP_INCREASE_MONEY_POOL);
                    break;
            }
        }

        protected virtual void Awake()
        {
            _longPress = GetComponent<LongPressEventTrigger>();

            if (_longPress != null)
                _longPress.onLongPress.AddListener(OnLongPressCallback);

            InitControl();
        }

        private void OnLongPressCallback()
        {
            SignalOnLongPress.Dispatch(this);
        }

        public enum UpgradeControlType
        {
            Slot,
            Speed,
            Crystal,
            Experience,
            IncreaseCityHallMoneyPool,
            DwellingHousesMaxCount
        }
    }
}