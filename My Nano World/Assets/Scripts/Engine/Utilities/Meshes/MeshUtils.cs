﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Engine.Utilities.Meshes
{
    public static partial class MeshUtils
    {
        #region api

        public static Mesh CreateMeshFrom(MetaMesh metaMesh)
        {
            var mesh = new Mesh
            {
                vertices = metaMesh.Vertices.ToArray(),
                triangles = metaMesh.Triangles.ToArray(),
                colors = metaMesh.Colors.ToArray()
            };

            mesh.RecalculateBounds();
            mesh.RecalculateNormals();

            return mesh;
        }

        public static List<Vector3> GenerateArcVertices(Vector3 start, Vector3 end, Vector3 center, int numVertices)
        {
            return GenerateArcVerticesImpl(start, end, center, numVertices);
        }

        public static List<Vector3> GenerateArcVertices(float startAngle, float deltaAngle, float radius, Vector3 center, int numVertices)
        {
            return GenerateArcVerticesImpl(startAngle, deltaAngle, radius, center, numVertices);
        }

        public static List<Vector3> GenerateRoundedRectVertices(Vector2 size, Vector3 center, float radius, int numCornerVertices)
        {
            return GenerateRoundedRectVerticesImpl(size, center, radius, numCornerVertices);
        }

        public static void AddLineTo(MetaMesh metaMesh, Vector3 start, Vector3 end, float thick, Color color)
        {
            AddLineToImpl(metaMesh, start, end, thick, color);
        }
        
        public static void AddRoundedLineTo(MetaMesh metaMesh, Vector3 start, Vector3 end, float thick, Color color, int numCornerVertices)
        {
            AddRoundedLineToImpl(metaMesh, start, end, thick, color, numCornerVertices);
        }
        
        public static void AddDashedLineTo(MetaMesh metaMesh, Vector3 start, Vector3 end, float thick, float partLength, float partsSpace, Color color, bool partsRounded = false, int numPartCornerVertices = 7)
        {
            AddDashedLineToImpl(metaMesh, start, end, thick, partLength, partsSpace, color, partsRounded, numPartCornerVertices);
        }
        
        public static void AddDashedContourTo(MetaMesh metaMesh, List<Vector3> contourVertices, bool contourCompleted, float thick, float partLength, float partsSpace, Color color, bool partsRounded = false, int numPartCornerVertices = 7)
        {
            AddDashedContourToImpl(metaMesh, contourVertices, contourCompleted, thick, partLength, partsSpace, color, partsRounded, numPartCornerVertices);
        }

        public static Mesh GenerateDashedRectMesh(Vector3 center, Vector2 size, float thick, float partLength, float partsSpace, Color color, bool partsRounded = false, int numPartCornerVertices = 7)
        {
            return GenerateDashedRectMeshImpl(center, size, thick, partLength, partsSpace, color, partsRounded, numPartCornerVertices);
        }
        
        public static Mesh GenerateBorderMesh(ICollection<Vector3> outerVertices, ICollection<Vector3> innerVertices, Color borderColor, Color areaColor)
        {
            return GenerateBorderMeshImpl(outerVertices, innerVertices, borderColor, areaColor);
        }
        
        public static Mesh GenerateAreaMesh(ICollection<Vector3> vertices, Color areaColor)
        {
            return GenerateAreaMeshImpl(vertices, areaColor);
        }
        
        /// <summary>
        /// Angle in degrees (not radians) for vector at standart unit-circle math model
        /// </summary>
        /// <param name="vector"></param>
        /// <returns>Angle in degrees</returns>
        public static float AngleFromAxisX(Vector2 vector)
        {
            var angle = Vector2.Angle(Vector2.right, vector);
            return vector.y > 0 ? angle : 360 - angle;
        }

        public static Mesh GenerateMeshFromSprite(Sprite sprite)
        {
            if (!sprite)
            {
                Debug.LogError("No Sprite!");
                return null;
            }
            Vector2[] spriteVertices = sprite.vertices;
            ushort[] spriteTriangles = sprite.triangles;

            Vector3[] vertices = new Vector3[spriteVertices.Length];
            int[] triangles = new int[spriteTriangles.Length];

            for (var index = 0; index < spriteVertices.Length; index++)
            {
                vertices[index] = spriteVertices[index];
            }

            for (var index = 0; index < spriteTriangles.Length; index++)
            {
                triangles[index] = Convert.ToInt32(spriteTriangles[index]);
            }

            Mesh spriteMesh = new Mesh
            {
                name = sprite.name + " mesh", 
                vertices = vertices, 
                triangles = triangles, 
                uv = sprite.uv
            };

            spriteMesh.RecalculateBounds();
            spriteMesh.RecalculateNormals();
            spriteMesh.RecalculateTangents();

            return spriteMesh;
        }

        #endregion


        #region privates

        private static List<Vector3> GenerateArcVerticesImpl(Vector3 start, Vector3 end, Vector3 center, int numVertices)
        {
            var vertices = new List<Vector3>();

            var startDelta = start - center;
            var endDelta = end - center;
            var startRadius = startDelta.magnitude;
            var endRadius = endDelta.magnitude;
            var startAngle = AngleFromAxisX(startDelta);
            var endAngle = AngleFromAxisX(endDelta);

            for (var i = 0; i < numVertices; i++)
            {
                var lerpFactor = i * 1f / (numVertices - 1);
                var radius = Mathf.Lerp(startRadius, endRadius, lerpFactor);
                var angle = Mathf.LerpAngle(startAngle, endAngle, lerpFactor);
                var angleRad = angle * Mathf.Deg2Rad;
                var vertice = center + new Vector3(radius * Mathf.Cos(angleRad), radius * Mathf.Sin(angleRad));
                vertices.Add(vertice);
            }

            return vertices;
        }
        
        
        /// <summary>
        /// Generate arc vertices
        /// </summary>
        /// <param name="startAngle">start angle in degrees to X-ort, 0 to 360</param>
        /// <param name="deltaAngle">angle size of arc, arc direction is clockwise</param>
        private static List<Vector3> GenerateArcVerticesImpl(float startAngle, float deltaAngle, float radius, Vector3 center, int numVertices)
        {
            var vertices = new List<Vector3>();

            for (var i = 0; i < numVertices; i++)
            {
                var lerpFactor = i * 1f / (numVertices - 1);
                var angle = startAngle - deltaAngle * lerpFactor;
                var angleRad = angle * Mathf.Deg2Rad;
                var vertice = center + new Vector3(radius * Mathf.Cos(angleRad), radius * Mathf.Sin(angleRad));
                vertices.Add(vertice);
            }

            return vertices;
        }

        private static List<Vector3> GenerateRoundedRectVerticesImpl(Vector2 size, Vector3 center, float radius, int numCornerVertices)
        {
            var vertices = new List<Vector3>();

            vertices.AddRange(
                GenerateArcVerticesImpl(
                    new Vector3(size.x / 2 - radius, size.y / 2) + center,
                    new Vector3(size.x / 2, size.y / 2 - radius) + center,
                    new Vector3(size.x / 2 - radius, size.y / 2 - radius) + center,
                    numCornerVertices)
            );
            vertices.AddRange(
                GenerateArcVerticesImpl(
                    new Vector3(size.x / 2, -size.y / 2 + radius) + center,
                    new Vector3(size.x / 2 - radius, -size.y / 2) + center,
                    new Vector3(size.x / 2 - radius, -size.y / 2 + radius) + center,
                    numCornerVertices)
            );
            vertices.AddRange(
                GenerateArcVerticesImpl(
                    new Vector3(-size.x / 2 + radius, -size.y / 2) + center,
                    new Vector3(-size.x / 2, -size.y / 2 + radius) + center,
                    new Vector3(-size.x / 2 + radius, -size.y / 2 + radius) + center,
                    numCornerVertices)
            );
            vertices.AddRange(
                GenerateArcVerticesImpl(
                    new Vector3(-size.x / 2, size.y / 2 - radius) + center,
                    new Vector3(-size.x / 2 + radius, size.y / 2) + center,
                    new Vector3(-size.x / 2 + radius, size.y / 2 - radius) + center,
                    numCornerVertices)
            );

            return vertices;
        }
        
        private static void AddLineToImpl(MetaMesh metaMesh, Vector3 start, Vector3 end, float thick, Color color)
        {
            var direction = end - start;
            var normal = new Vector3(-direction.y, direction.x).normalized;
            var halfThick = normal * thick * 0.5f;
            var index0 = metaMesh.Add(start + halfThick, color);
            var index1 = metaMesh.Add(end + halfThick, color);
            var index2 = metaMesh.Add(end - halfThick, color);
            var index3 = metaMesh.Add(start - halfThick, color);
            metaMesh.AddTraingleFor(index0, index1, index2);
            metaMesh.AddTraingleFor(index2, index3, index0);
        }
        
        private static void AddRoundedLineToImpl(MetaMesh metaMesh, Vector3 start, Vector3 end, float thick, Color color, int numCornerVertices)
        {
            var direction = end - start;
            var directionNormalized = direction.normalized;
            var normal = new Vector3(-directionNormalized.y, directionNormalized.x);
            var halfThick = normal * thick * 0.5f;
            var solidStart = start + directionNormalized * thick * 0.5f;
            var solidEnd = end - directionNormalized * thick * 0.5f;
            
            AddLineToImpl(metaMesh, solidStart, solidEnd, thick, color);
            AddHalfCircleTo(metaMesh, solidStart - halfThick, solidStart, thick * 0.5f, numCornerVertices, color);
            AddHalfCircleTo(metaMesh, solidEnd + halfThick, solidEnd, thick * 0.5f, numCornerVertices, color);
        }

        private static void AddDashedLineToImpl(MetaMesh metaMesh, Vector3 start, Vector3 end, float thick, float partLength, float partsSpace, Color color, bool partsRounded, int numPartCornerVertices)
        {
            var direction = end - start;
            var directionNormalized = direction.normalized;
            var lineLength = direction.magnitude;
            var partLengthWithSpace = partLength + partsSpace;
            var numParts = (int) Mathf.Max((lineLength + partsSpace) / partLengthWithSpace, 1);
            var numSpaces = numParts - 1;
            var correctedPartLength = (lineLength - numSpaces * partsSpace) / numParts;
            var correctedPartLengthWithSpace = correctedPartLength + partsSpace;

            for (var i = 0; i < numParts; i++)
            {
                var partStart = start + i * correctedPartLengthWithSpace * directionNormalized;
                var partEnd = partStart + correctedPartLength * directionNormalized;
                AddSimpleOrRoundedLineTo(metaMesh, partStart, partEnd, thick, color, partsRounded, numPartCornerVertices);
            }
        }
                        
        private static void AddDashedContourToImpl(MetaMesh metaMesh, List<Vector3> contourVertices, bool contourCompleted, float thick, float partLength, float partsSpace, Color color, bool partsRounded = false, int numPartCornerVertices = 7)
        {
            var contourVerticesPositions = new List<float> {0};
            for (var i = 1; i < contourVertices.Count; i++)
            {
                var delta = contourVertices[i] - contourVertices[i - 1];
                contourVerticesPositions.Add(contourVerticesPositions[i - 1] + delta.magnitude);
            }

            var contourLength = contourVerticesPositions[contourVerticesPositions.Count - 1];
            if (contourCompleted) contourLength += (contourVertices[0] - contourVertices[contourVertices.Count - 1]).magnitude;
            
            var partLengthWithSpace = partLength + partsSpace;
            var conditionalLength = contourCompleted ? contourLength : contourLength + partsSpace;
            var numParts = (int) Mathf.Max(conditionalLength / partLengthWithSpace, 1);
            var numSpaces = contourCompleted ? numParts : numParts - 1;
            var correctedPartLength = (contourLength - numSpaces * partsSpace) / numParts;
            var correctedPartLengthWithSpace = correctedPartLength + partsSpace;

            for (var i = 0; i < numParts; i++)
            {
                var partStart = GetPositionOnContour(contourVertices, contourVerticesPositions, i * correctedPartLengthWithSpace);
                var partEnd = GetPositionOnContour(contourVertices, contourVerticesPositions, correctedPartLength + i * correctedPartLengthWithSpace);
                AddSimpleOrRoundedLineTo(metaMesh, partStart, partEnd, thick, color, partsRounded, numPartCornerVertices);
            }
        }

        private static Mesh GenerateDashedRectMeshImpl(Vector3 center, Vector2 size, float thick, float partLength, float partsSpace, Color color, bool partsRounded, int numPartCornerVertices)
        {
            var metaMesh = new MetaMesh();
            AddDashedLineToImpl(metaMesh, center + new Vector3(-size.x / 2, size.y / 2 + thick / 2), center + new Vector3(size.x / 2, size.y / 2 + thick / 2), thick, partLength, partsSpace, color, partsRounded, numPartCornerVertices);
            AddDashedLineToImpl(metaMesh, center + new Vector3(size.x / 2 + thick / 2, size.y / 2), center + new Vector3(size.x / 2 + thick / 2, -size.y / 2), thick, partLength, partsSpace, color, partsRounded, numPartCornerVertices);
            AddDashedLineToImpl(metaMesh, center + new Vector3(size.x / 2, -size.y / 2 - thick / 2), center + new Vector3(-size.x / 2, -size.y / 2 - thick / 2), thick, partLength, partsSpace, color, partsRounded, numPartCornerVertices);
            AddDashedLineToImpl(metaMesh, center + new Vector3(-size.x / 2 - thick / 2, -size.y / 2), center + new Vector3(-size.x / 2 - thick / 2, size.y / 2), thick, partLength, partsSpace, color, partsRounded, numPartCornerVertices);
            return CreateMeshFrom(metaMesh);
        }
        
        private static Mesh GenerateBorderMeshImpl(ICollection<Vector3> outerVertices, ICollection<Vector3> innerVertices, Color borderColor, Color areaColor)
        {
            var metaMesh = new MetaMesh();
            var outerIndexesBorder = metaMesh.Add(outerVertices, borderColor);
            var innerIndexesBorder = metaMesh.Add(innerVertices, borderColor);
            var innerIndexesArea = metaMesh.Add(innerVertices, areaColor);
            var centerIndex = metaMesh.Add(Vector3.zero, areaColor);
            metaMesh.AddTrinaglesFor(centerIndex, innerIndexesArea);
            metaMesh.AddTrinaglesFor(innerIndexesBorder, outerIndexesBorder);
            return CreateMeshFrom(metaMesh);
        }
        
        private static Mesh GenerateAreaMeshImpl(ICollection<Vector3> vertices, Color areaColor)
        {
            var metaMesh = new MetaMesh();
            var indexes = metaMesh.Add(vertices, areaColor);
            var centerIndex = metaMesh.Add(Vector3.zero, areaColor);
            metaMesh.AddTrinaglesFor(centerIndex, indexes);
            return CreateMeshFrom(metaMesh);
        }
        
        
        #region private utils
        
        private static void AddColors (ICollection<Color> colors, Color color, int count)
        {
            for (var i = 0; i < count; i++) colors.Add(color);
        }
        
        private static void AddHalfCircleTo(MetaMesh metaMesh, Vector3 start, Vector3 center, float radius, int numVertices, Color color)
        {
            var arcVertices = GenerateArcVerticesImpl(AngleFromAxisX(start - center), 180, radius, center, numVertices);
            var arcVerticesIndexes = metaMesh.Add(arcVertices, color);
            var solidStartIndex = metaMesh.Add(center, color);
            metaMesh.AddTrinaglesFor(solidStartIndex, arcVerticesIndexes, false);
        }

        private static void AddSimpleOrRoundedLineTo(MetaMesh metaMesh, Vector3 start, Vector3 end, float thick, Color color, bool rounded, int numCornerVertices)
        {
            if (rounded)
            {
                AddRoundedLineToImpl(metaMesh, start, end, thick, color, numCornerVertices);
            }
            else
            {
                AddLineToImpl(metaMesh, start, end, thick, color);
            }
        }

        private static Vector3 GetPositionOnContour(List<Vector3> contourVertices, List<float> contourVerticesPositions, float contourPosition)
        {
            var index = contourVerticesPositions.FindIndex(p => p >= contourPosition);
            var startIndex = index > 0 ? index - 1 : index == 0 ? 0 : contourVertices.Count - 1;
            var start = contourVertices[startIndex];
            var end = contourVertices[index >= 0 ? index : 0];
            var deltaPosition = contourPosition - contourVerticesPositions[startIndex];
            var directionNormalized = (end - start).normalized;
            return start + directionNormalized * deltaPosition;
        }
                
        #endregion

        #endregion
    }
}