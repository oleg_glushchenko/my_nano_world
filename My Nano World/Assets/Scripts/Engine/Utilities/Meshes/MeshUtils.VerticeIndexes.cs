﻿namespace Assets.Scripts.Engine.Utilities.Meshes
{
    public static partial class MeshUtils
    {
        public class VerticeIndexes
        {
            public int StartIndex { get; private set; }
            public int EndIndex { get; private set; }

            public VerticeIndexes(int startIndex, int endIndex)
            {
                StartIndex = startIndex;
                EndIndex = endIndex;
            }
        }
    }
}