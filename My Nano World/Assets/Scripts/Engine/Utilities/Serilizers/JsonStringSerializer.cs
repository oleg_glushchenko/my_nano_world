﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assets.NanoLib.Utilities.Serializers;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Assets.Scripts.GameLogic.Utilites;
using NanoLib.Core.Logging;
using NanoReality.GameLogic.StartApplicationSequence.Controllers.Signals;
using Newtonsoft.Json;

namespace NanoReality.Engine.Utilities
{
    /// <summary>
    /// Provides methods for converting between common language runtime types and JSON types.
    /// </summary>
    public class JsonStringSerializer : IStringSerializer
    {
        private readonly Queue<Action> _queue = new Queue<Action>();
        private readonly object _locker = new object();

        private readonly object _classLocker = new object();
        
        [Inject] public UpdateTickSignal UpdateTickSignal { get; set; }
        [Inject] public IPopupManager jPopupManager { get; set; }
        [Inject] public SignalReloadApplication jSignalReloadApplication { get; set; }
        
        [PostConstruct]
        public void Construct()
        {
            UpdateTickSignal.AddListener(DispatcherWorker);
        }

        private void DispatcherWorker()
        {
            lock (_locker)
            {
                if (_queue.Count > 0)
                {
                    try
                    {
                        _queue.Dequeue()();
                    }
                    catch (Exception e)
                    {
                        Logging.LogException(e);
                        Logging.LogError(LoggingChannel.Core,$"JsonStringSerializer Exception:\n{e}");

                        string title = $"{LocalizationMLS.Instance.GetText(LocalizationKeyConstants.ERRORRESTARTPOPUP_LABEL_ERROR)} {e.InnerException}";
                        string message = "Localization/" + LocalizationKeyConstants.SERVERERROR_TEXT_POPUP;
                            
                        IPopupSettings popupSettings = new NetworkErrorPopupSettings(title, message, null, OnReload);

                        void OnReload()
                        {
                            jSignalReloadApplication.Dispatch();
                        }

                        jPopupManager.Show(popupSettings);
                    }
                }
            }
        }
        
        public string Serialize(object value)
        {
            lock (_classLocker)
            {
                return JsonConvert.SerializeObject(value);
            }
        }
        
        public object Deserialize(string value, Type T, JsonSerializerSettings settings)
        {
            return JsonConvert.DeserializeObject(value, T, settings);
        }
        
        public object Deserialize(string value, Type type)
        {
            lock (_classLocker)
            {
                var deserializedObject = SerializationConverters.DeserializeObject(value, type);
                
                return deserializedObject;
            }
        }

        public TType Deserialize<TType>(string value)
        {
            return (TType) Deserialize(value, typeof(TType));
        }

        public void DeserializeAsync<TResult>(string value, Action<TResult> resultAction)
        {
            Task.Run(() =>
            {
                DeserializationWorker(value, resultAction);
            });
        }
        
        private void DeserializationWorker<TResult>(string value, Action<TResult> resultAction)
        {
            TResult q;
            try
            {
                lock (_classLocker)
                {
                    q = SerializationConverters.DeserializeObject<TResult>(value);
                }

                lock (_locker)
                {
                    _queue.Enqueue(() =>
                    {
                        resultAction?.Invoke(q);
                        
                    });
                }
            } 
            catch (Exception e)
            {
                lock (_locker)
                {
                    _queue.Enqueue(() => 
                        Logging.LogError(LoggingChannel.Core, 
                            $"JsonStringSerializer.DeserializationWorker : {value.Take(50)}. Error: {e}</Color>"));
                    resultAction?.Invoke(default);
                }
            }
        }
    }
}