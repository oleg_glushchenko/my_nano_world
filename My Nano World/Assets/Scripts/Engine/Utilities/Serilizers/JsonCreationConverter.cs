﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace NanoReality.Engine.Utilities.Serialization
{
    /// <summary>
    /// Provides methods for converting between common language runtime interfaces and JSON types.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class JsonCreationConverter<T> : JsonConverter
    {
        /// <summary>
        /// Create an instance of objectType, based properties in the JSON object
        /// </summary>
        /// <param name="objectType">type of object expected</param>
        /// <param name="jObject">
        /// contents of JSON object that will be deserialized
        /// </param>
        /// <returns></returns>
        protected abstract T Create(Type objectType, JObject jObject);

        public override bool CanConvert(Type objectType)
        {
            return typeof (T).IsAssignableFrom(objectType);
        }

        public override object ReadJson(JsonReader reader,
            Type objectType,
            object existingValue,
            JsonSerializer serializer)
        {
            
            
            JObject jObject;

            if(reader.TokenType!= JsonToken.Null)  
                jObject = JObject.Load(reader); // Load JObject from stream
            else
            {
                return null;
            }

            // Create target object based on JObject
            T target = Create(objectType, jObject);

            try
            {
                // Populate the object properties
                //serializer.Populate(jObject.CreateReader(), target);
                JsonConvert.PopulateObject(jObject.ToString(), target, SerializationConverters.Settings);
            }
            catch (Exception e)
            {
                throw new Exception($"Can't parse json for object [{objectType.Name} json:[{jObject}].\nException:{e}");
            }

            return target;
        } 

        public override void WriteJson(JsonWriter writer,
            object value,
            JsonSerializer serializer)
        {

            //base.WriteJson(writer,value,serializer);
            // serializer.Serialize(writer,value);
			throw new NotImplementedException ("WriteJson");
        }


        protected virtual bool IsContainsProperty(JObject obj, string prop)
        {
            return obj[prop] != null;
        }

    }


    public class StrangeIoCConvrter<T> : JsonCreationConverter<T>
    {
        protected override T Create(Type objectType, JObject jObject)
        {
            if (NanoRealityGameContext.InjectionBinder != null)
                return NanoRealityGameContext.InjectionBinder.GetInstance<T>();
            else
            {
                return default(T);
            }
        }
    }

}