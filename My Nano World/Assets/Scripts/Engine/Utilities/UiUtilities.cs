﻿using System;
using System.Text;
using Assets.NanoLib.Utilities;
using UnityEngine;
using UnityEngine.UI;

public static class UiUtilities
{
	/// <summary>
    /// Return string in format 00d 00h 00m 00s
    /// </summary>
    /// <param name="seconds"></param>
    /// <returns></returns>
    public static string GetFormattedTimeFromSeconds(int seconds)
	{
		var (daySign, hourSign, minSign, secSign) = TimeSigns();
		
		var timeSpan = TimeSpan.FromSeconds (seconds < 0 ? 0 : seconds);
		if (timeSpan.Days > 0)
			return $"{timeSpan.Days}{daySign} {timeSpan.Hours:D2}{hourSign} {timeSpan.Minutes:D2}{minSign} {timeSpan.Seconds:D2}{secSign}";
		if (timeSpan.Hours > 0)
			return $"{timeSpan.Hours:D2}{hourSign} {timeSpan.Minutes:D2}{minSign} {timeSpan.Seconds:D2}{secSign}";
		if (timeSpan.Minutes > 0)
			return $"{timeSpan.Minutes:D2}{minSign} {timeSpan.Seconds:D2}{secSign}";
		
		return $"{timeSpan.Seconds:D2}{secSign}";
	}

	public static string GetFormattedTimeFromSecondsShorted(int seconds)
	{
		var (daySign, hourSign, minSign, secSign) = TimeSigns();

		var timeSpan = TimeSpan.FromSeconds(seconds < 0 ? 0 : seconds);
		if (timeSpan.Days > 0)
			return $"{timeSpan.Days}{daySign} {timeSpan.Hours:D2}{hourSign}";
		if (timeSpan.Hours > 0)
			return $"{timeSpan.Hours:D2}{hourSign} {timeSpan.Minutes:D2}{minSign}";
		if (timeSpan.Minutes > 0)
			return $"{timeSpan.Minutes:D2}{minSign} {timeSpan.Seconds:D2}{secSign}";

		return $"{timeSpan.Seconds:D2}{secSign}";
	}

	public static string GetFormattedTimeFromSecondsLetterless(int seconds)
    {
        var timeSpan = TimeSpan.FromSeconds(seconds < 0 ? 0 : seconds);
        if (timeSpan.Days > 0)
            return $"{timeSpan.Days}:{timeSpan.Hours:D2}:{timeSpan.Minutes:D2}:{timeSpan.Seconds:D2}";
        if (timeSpan.Hours > 0)
            return $"{timeSpan.Hours:D2}:{timeSpan.Minutes:D2}:{timeSpan.Seconds:D2}";
        if (timeSpan.Minutes > 0)
            return $"{timeSpan.Minutes:D2}:{timeSpan.Seconds:D2}";

        return $"00:{timeSpan.Seconds:D2}";
    }

    public static string GetFormattedTimeFromSecondsShort(int seconds)
    {
	    var (daySign, hourSign, minSign, secSign) = TimeSigns();
	    var timeSpan = TimeSpan.FromSeconds (seconds < 0 ? 0 : seconds);
		var stringBuilder = new StringBuilder();

		if (timeSpan.Days > 0)
		{
			stringBuilder.AppendFormat("{0:D}{1}", timeSpan.Days, daySign);
		}
		
		if (timeSpan.Hours > 0)
		{
			stringBuilder.AppendFormat(" {0:D}{1}", timeSpan.Hours, hourSign);
		}
		
		if (timeSpan.Minutes > 0)
		{
			stringBuilder.AppendFormat(" {0:D}{1}", timeSpan.Minutes, minSign);
		}
		
		if (timeSpan.Seconds > 0)
		{
			stringBuilder.AppendFormat(" {0:D}{1}", timeSpan.Seconds, secSign);
		}

		if (seconds == 0)
		{
			return "0";
		}
		
		return stringBuilder.ToString().Trim(' ');
	}

    public static string GetFormattedTimeFromSeconds_H_M_S(double time)
    {
        var timeSpan = TimeSpan.FromSeconds(time < 0 ? 0 : time);
        return $"{(int) timeSpan.TotalHours:D2}:{timeSpan.Minutes:D2}:{timeSpan.Seconds:D2}";
    }

    /// <summary>
    /// Return time in format 2:23:57:49;
    /// </summary>
    /// <param name="time"></param>
    /// <returns></returns>
    public static string GetFormattedTimeFromSeconds_M_S(double time)
    {
        TimeSpan timeSpan = TimeSpan.FromSeconds(time < 0 ? 0 : time);

        if (timeSpan.Hours > 0)
        {
	        return $"{(int) timeSpan.TotalHours:D2}:{timeSpan.Minutes:D2}:{timeSpan.Seconds:D2}";
        }

        return $"{timeSpan.Minutes:D2}:{timeSpan.Seconds:D2}";
    }

    public static string GetFormattedTimeFromSeconds_H_M(double time)
    {
        time = Clamp(time);
        var timeSpan = TimeSpan.FromSeconds(time);
        return $"{timeSpan.Hours:D2}:{timeSpan.Minutes:D2}";
    }

    private static double Clamp(double time)
    {
        return time < 0.0 ? 0.0 : time;
    }
    
    private static (string daySign, string hourSign, string minSign, string secSign) TimeSigns()
    {
	    return (
		    daySign: "d",
		    hourSign: "h",
		    minSign: "m",
		    secSign: "s");
    }

    /// <summary>
    /// Scale building for UI, to fit on a ground plane
    /// </summary>
    /// <param name="buildingDimensions">building dimensions</param>
    /// <param name="buildingIconImage">building ui image</param>
    public static void ScaleUIBuilding(Vector2F buildingDimensions, Image buildingIconImage)
    {
	    if (!buildingIconImage)
	    {
		    return;
	    }

	    var spriteRectTransform = buildingIconImage.transform as RectTransform;
	    if (!spriteRectTransform)
	    {
		    return;
	    }

	    // scaleFactor is - relation of currant building image scale to default 
	    var scaleFactor = Mathf.Max(buildingDimensions.X, buildingDimensions.Y) / 3f;

	    // divide by 4 because - distance to building center is half of (isometric distortion) half of width
	    Vector3 pivotToBuildingCenter = Vector3.up * spriteRectTransform.rect.width / 4;
	    Vector3 buildingPositionShift = pivotToBuildingCenter - pivotToBuildingCenter * scaleFactor;

	    // some sprites have negative pivot point offset - sprite renderer handles it fine
	    // but in ui we should take care of it manually 
	    if (buildingIconImage.sprite)
	    {
		    var spriteNegativeOffsetY = -Mathf.Clamp(buildingIconImage.sprite.pivot.y, float.NegativeInfinity, 0);
		    var extraShift = spriteNegativeOffsetY * scaleFactor * (spriteRectTransform.rect.width / buildingIconImage.sprite.rect.width);
		    buildingPositionShift += Vector3.up * extraShift * scaleFactor;
	    }

	    spriteRectTransform.anchoredPosition = buildingPositionShift;
	    spriteRectTransform.localScale = Vector3.one * scaleFactor;
    }

}