﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Выключает объект при старте сцены (нужен для тествых спрайтов)
/// </summary>
public class TestSpriteDisabler : MonoBehaviour
{

    void Awake()
    {
        gameObject.SetActive(false);
    }
    
}
