using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Assets.Scripts.Engine.Utilities
{
    [ExecuteInEditMode]
    public class MapSlicePlacer : MonoBehaviour
    {
#if UNITY_EDITOR
        
        [SerializeField] private List<Sprite> _mapPeaces = new List<Sprite>();
        [SerializeField] private int _cols = 6;
        [SerializeField] private int _rows = 5;
        [SerializeField] private Material _material;
        [SerializeField] private float _cellScale = 2048;
        [SerializeField] private float _zShift = 0.001f;
        [SerializeField] private Vector3 _center = new Vector3();
        [SerializeField] private Transform _parent;
        private float _lastTileShift;

        private void ClearAll()
        {
            var mapPeaces = _parent.gameObject.GetComponentsInChildren<Transform>();

            foreach (Transform peace in mapPeaces)
            {
                if (peace == _parent)
                {
                    continue;
                }

                DestroyImmediate(peace.gameObject);
            }
        }

        [ContextMenu("Place Map Peaces")]
        [Button]
        public void PlaceMapPeaces()
        {
            ClearAll();
            var counter = 0;
            for (var i = 0; i < _cols; i++)
            {
                for (var j = 0; j < _rows; j++)
                {
                    counter++;

                    Sprite sprite = _mapPeaces.Find(e => e.name.Contains("_" + i + "x" + j));
                    if (!sprite)
                    {
                        Debug.LogError("No sprite for: " + i + "x" + j);
                        continue;
                    }

                    var x = _center.x + i * _cellScale;
                    var y = _center.y - j * _cellScale;
                    var z = _center.z + _zShift * (counter - 1);


                    _lastTileShift = .035f;
                    if (i == _cols - 1)
                    {
                        var cellSizeX = sprite.rect.width / sprite.pixelsPerUnit;
                        x = _center.x + (i - 1) * _cellScale + _cellScale / 2 + cellSizeX / 2 - _lastTileShift;
                    }

                    if (j == _rows - 1)
                    {
                        var cellSizeY = sprite.rect.height / sprite.pixelsPerUnit;
                        y = _center.y - (j - 1) * _cellScale - _cellScale / 2 - cellSizeY / 2 + _lastTileShift;
                    }

                    var position = new Vector3(x, y, z);


                    var newGameObject = new GameObject("MapPeace_" + i + "x" + j);

                    var spriteRenderer = newGameObject.AddComponent<SpriteRenderer>();

                    newGameObject.transform.parent = _parent;
                    newGameObject.transform.localPosition = position;

                    spriteRenderer.sprite = sprite;
                    if (_material)
                    {
                        spriteRenderer.material = _material;
                    }
                }
            }
        }
#endif
    }
}