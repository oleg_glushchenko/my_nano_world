﻿using System;

namespace NanoReality.Engine.Utilities
{
    public static class DateTimeUtils
    {
        /// <summary>
        /// Amount of seconds in one day (24 hours)
        /// </summary>
        public const int SecondsInDay = 60 * 60 * 24;

        /// <summary>
        /// Amount of seconds in one hour
        /// </summary>
        public const int SecondsInHour = 60 * 60;

        /// <summary>
        /// Amount of seconds in one minute
        /// </summary>
        public const int SecondsInMinute = 60;

        /// <summary>
        /// The Unix epoch start date and time
        /// </summary>
        public static readonly DateTime UnixStartTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        /// <summary>
        /// Converts unix timestamp to DateTime structure
        /// </summary>
        /// <param name="unixTimestamp">unix timestamp in seconds</param>
        /// <returns></returns>
        public static DateTime ConvertUnixTimestampToDateTime(long unixTimestamp)
        {
            return UnixStartTime.AddSeconds(unixTimestamp);
        }

        /// <summary>
        /// Returns unix timestamp in seconds
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static long GetUnixTimestamp(DateTime dateTime)
        {
            return (long)(dateTime - UnixStartTime).TotalSeconds;
        }

        /// <summary>
        /// Returns day time in seconds from unix timestamp
        /// </summary>
        /// <param name="unixTimestamp">unix timestamp in seconds</param>
        /// <returns></returns>
        public static long GetDayTimeInSecondsFromUnixTimestamp(long unixTimestamp)
        {
            var dateTime = ConvertUnixTimestampToDateTime(unixTimestamp);
            return dateTime.Hour * SecondsInHour + dateTime.Minute * SecondsInMinute + dateTime.Second;
        }

        public static double TotalSeconds(this DateTime date)
        {
            return date.Subtract(DateTime.MinValue).TotalSeconds;
        }

        public static DateTimeOffset FromUnixToDateTimeOffset(long unixTimestamp)
        {
            return DateTimeOffset.FromUnixTimeSeconds(unixTimestamp);
        }
    }
}
