﻿using UnityEngine;

/// <summary>
/// Скрипт, необходимый для каждого объекта окружения,
/// т.к. стандартные позиция и поворот из трансформа
/// для редактора не подходят
/// </summary>
public class PropScript : MonoBehaviour
{
    /// <summary>
    /// Позиция объекта на карте
    /// </summary>
    public Vector2 MapPosition;

    /// <summary>
    /// Вращение объекта по эйлеру
    /// </summary>
    public Vector3 EulerRotation = new Vector3(30, 45, 0);
}
