﻿public static class MathUtilities 
{
    /// <summary>
    /// Переводит значение в диапазон от 0 до 1
    /// </summary>
    /// <param name="currentValue"></param>
    /// <param name="maxValue"></param>
    /// <param name="minValue"></param>
    /// <returns></returns>
    public static float ConvertTo01Range(float currentValue, float maxValue, float minValue)
    {
        return (currentValue - minValue)/(maxValue - minValue);
    }
   
}
