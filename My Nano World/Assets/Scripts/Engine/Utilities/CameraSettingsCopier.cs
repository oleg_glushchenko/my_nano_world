﻿using UnityEngine;
using Assets.NanoLib.Utilities;

namespace NanoReality.Engine.UI
{
    /// <summary>
    /// Копирует состояния с одной камеры в другую
    /// </summary>
    [RequireComponent(typeof(Camera))]
    public class CameraSettingsCopier : MonoBehaviour
    {
        [SerializeField]
        private Camera _target;

        public Camera Target
        {
            get
            {
                return _target;
            }
            set
            {
                if(value!= _target)
                    TargetCacheMonobehavior = new CacheMonobehavior(value);
                _target = value; 
                
            }
        }

        public virtual CacheMonobehavior CacheMonobehavior { get; protected set; }

        protected virtual CacheMonobehavior TargetCacheMonobehavior { get; set; }

        protected virtual void Awake()
        {
            CacheMonobehavior = new CacheMonobehavior(this);
            if(Target != null)
                TargetCacheMonobehavior = new CacheMonobehavior(Target);
        }

        protected virtual void Update()
        {
            if (Target != null)
            {
                var targetCam = TargetCacheMonobehavior.GetCacheComponent<Camera>();
                var currCam = CacheMonobehavior.GetCacheComponent<Camera>();
                currCam.orthographic = Target.orthographic;
                currCam.orthographicSize = Target.orthographicSize;
                currCam.farClipPlane = targetCam.farClipPlane;
                currCam.nearClipPlane = targetCam.nearClipPlane;
                currCam.fieldOfView = targetCam.fieldOfView;
                currCam.enabled = targetCam.enabled;
            }
        }
        
        protected void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.green;
            Quaternion quaternion = new Quaternion(1, 1, 1, 1);
            quaternion = Quaternion.Euler(120, 45, 0);
            Gizmos.matrix = Matrix4x4.TRS(Env.MainCamera.transform.position, quaternion,
                Env.MainCamera.transform.lossyScale);
            Gizmos.DrawWireSphere(Vector3.zero, 2f);
        }
    }
}


