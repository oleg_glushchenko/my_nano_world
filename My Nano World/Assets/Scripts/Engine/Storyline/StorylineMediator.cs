﻿using NanoLib.AssetBundles;
using NanoReality.Game.Tutorial;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using UnityEngine;

namespace NanoReality.Engine.Storyline
{
	public class SignalStartPlayingStoryline : Signal {}
	
	public class SignalOnStorylineCompleted : Signal {}
	
	public class StorylineMediator : Mediator
	{
		[Inject] public SignalStartPlayingStoryline SignalStartPlayingStoryline { get; set; }
		//[Inject] public IResourcesManager jResourcesManager { get; set; }
		[Inject] public IPlayerProfile jPlayerProfile { get; set; }

		private StorylineView _view;
		
		public override void OnRegister()
		{
			base.OnRegister();
			// jAssetBundlesLoadedSignal.AddListener(LoadStoryline);
		}
		public override void OnRemove()
		{
			base.OnRemove();
			// jAssetBundlesLoadedSignal.RemoveListener(LoadStoryline);
		}

		private void OnStartPlaingCallback()
		{
//			_view.PlayStory();
		}
		
		private void LoadStoryline()
		{
//			if (jPlayerProfile.TutorialStep != StepType.Storyline)
//			{
//				jResourcesManager.UnloadBundle(AssetBundles.STORYLINE_BUNDLE_NAME);
//				return;
//			}
//			
//			var storyLineObj = Instantiate(jResourcesManager.GetStoryline(), transform.root, false);
//			storyLineObj.transform.localPosition = new Vector3(0,-1000,0);
//			storyLineObj.transform.localRotation = Quaternion.identity;
//			storyLineObj.transform.localScale = Vector3.one;
//			_view = storyLineObj.GetComponent<StorylineView>();
//			SignalStartPlayingStoryline.AddListener(OnStartPlaingCallback);
		}
	}
}
