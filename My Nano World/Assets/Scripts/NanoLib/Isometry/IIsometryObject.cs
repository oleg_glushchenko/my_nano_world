﻿using System.Collections.Generic;
using UnityEngine;

namespace NanoReality.Isometry
{
    public interface IIsometryObject
    {
        /// <summary>
        /// Position of the object on map with z for height
        /// </summary>
        Vector3 IsoPosition { get; }

        /// <summary>
        /// Size of the object with z for height
        /// </summary>
        Vector3 IsoSize { get; }

        /// <summary>
        /// Isometry box of the object based on position and size
        /// </summary>
        IsoBox IsoBox { get; }

        /// <summary>
        /// Isometry hexagon of the object based on position and size
        /// </summary>
        IsoHexagon IsoHexagon { get; }

        /// <summary>
        /// Isometric sorting depth.
        /// </summary>
        float IsoDepth { get; set; }

        /// <summary>
        /// Flag used by isometric depth sorting algorithm.
        /// </summary>
        bool IsVisited { get; set; }

        /// <summary>
        /// Objects that are behind of this object.
        /// </summary>
        List<IIsometryObject> ObjectsBehind { get; }

        /// <summary>
        /// Flag for ignoring depth sorting.
        /// </summary>
        bool IgnoreDepthSort { get; }

        /// <summary>
        /// Applies IsoDepth to object.
        /// </summary>
        void SortObject();
    }
}
