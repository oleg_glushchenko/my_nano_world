﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

namespace Assets.NanoLib.Utilities
{
    /// <summary>
    /// Представляет объект кешируемых данных о монобехе
    /// </summary>
    public class CacheMonobehavior
    {

        /// <summary>
        /// Кешированный трансформ
        /// </summary>
        public virtual Transform CachedTransform
        {
            get { return _cachedTransform ?? (_cachedTransform = _cachedMono.transform); }
        }

        /// <summary>
        /// Кешированный рендер
        /// </summary>
        public virtual Renderer CachedRender
        {
            get { return _cachedRenderer ?? (_cachedRenderer = _cachedMono.GetComponent<Renderer>()); }
        }

        /// <summary>
        /// Кешированный риджетбоди
        /// </summary>
        public virtual Rigidbody CachedRigidbody
        {
            get { return _cachedRigidbody ?? (_cachedRigidbody = _cachedMono.GetComponent<Rigidbody>()); }
        }

        /// <summary>
        /// Кешированный риджетбоди2D
        /// </summary>
        public virtual Rigidbody2D CachedRigidbody2D
        {
            get { return _cachedRigidbody2D ?? (_cachedRigidbody2D = _cachedMono.GetComponent<Rigidbody2D>()); }
        }

        /// <summary>
        /// Кешированный GameObject
        /// </summary>
        public virtual GameObject CachedGameObject
        {
            get { return _cachedGameObject ?? (_cachedGameObject = _cachedMono.gameObject); }

        }

        #region CachedData
        private Transform _cachedTransform;
        private Renderer _cachedRenderer;
        private Rigidbody _cachedRigidbody;
        private Rigidbody2D _cachedRigidbody2D;
        private readonly Dictionary<Type, Component> _cachedComponents;
        private readonly Component _cachedMono;
        private GameObject _cachedGameObject;

        #endregion

        public CacheMonobehavior([NotNull] Component mono)
        {
            if (mono == null) throw new ArgumentNullException("mono");
            _cachedMono = mono;
            _cachedComponents = new Dictionary<Type, Component>();
        }

        /// <summary>
        /// Добавляет компонент в словарь кешированных компонентов
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        protected virtual T AddCacheComponent<T>() where T : Component
        {
            if (_cachedComponents.ContainsKey(typeof(T)))
            {
                Debug.LogWarning(string.Format("You can't cached component {0} twice!", typeof(T)));
            }
            Component comp = _cachedMono.GetComponent<T>();
            if (comp == null)
            {
               return null;
            }
            _cachedComponents.Add(typeof(T), comp);
            return (T)comp;
        }

        /// <summary>
        /// Возращает ссылку на кешированный компонент
        /// </summary>
        /// <typeparam name="T">Тип компонента</typeparam>
        /// <returns>Закешированный компонент</returns>
        public virtual T GetCacheComponent<T>() where T : Component
        {
            if (_cachedComponents.ContainsKey(typeof(T)))
                return _cachedComponents[typeof(T)] as T;
            return AddCacheComponent<T>();
        }
    }
}
