﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using Assets.NanoLib.UtilitesEngine.CustomDebugging.Models.api;
using Assets.NanoLib.Utilities.DataManager.Models.api;
using NanoReality.Game.Data;
using UnityEngine;

namespace Assets.NanoLib.Utilities.DataManager.Models.impl
{
    public class LocalDataManager : ILocalDataManager
    {
        [Inject]
        public IDebug jDebug { get; set; }
        
        [Inject]
        public IBuildSettings jBuildSettings { get; set; }
        
        private RootObject rootObject = new RootObject();
        private string rootPath;
        private string rootName;
        private bool isLoaded;
     
        public void Init(string path, string name)
        {
            rootPath = path;
            rootName = name;
            isLoaded = false;
            
            LoadData();
        }

        public virtual void AddData(object data)
        {
            if(IsSerializable(data))
                rootObject.StorageObjects.Add(data);
            else
            {
                throw new Exception("The object is not serializable");
            }
        }

        public virtual bool isContains(object data)
        {
            return rootObject.StorageObjects.Contains(data);
        }

        public virtual void Remove(object data)
        {
            rootObject.StorageObjects.Remove(data);
        }

        public virtual T GetData<T>()
        {
            foreach (var storageObject in rootObject.StorageObjects)
            {
                if (storageObject.GetType() == typeof(T))
                    return (T)storageObject;
            }
            return default(T);
        }

        public virtual T[] GetDataArray<T>()
        {
            List<T> result = new List<T>();
            foreach (var storageObject in rootObject.StorageObjects)
            {
                if (storageObject is T)
                    result.Add( (T) storageObject);
            }
            return result.ToArray();
        }

        private static bool IsSerializable(object obj)
        {
            var t = obj.GetType();
            return  t.IsSerializable || obj is IXmlSerializable;
        }

        public virtual void SaveData()
        {
            FileStream aFileStream = null;
            try
            {
                jDebug.Log("Saving local data...", Color.magenta, true);

                if (string.IsNullOrEmpty(rootPath))
                {
                    jDebug.Log("File path is null or empty ", Color.magenta, true);
                    return;
                }

                if (string.IsNullOrEmpty(rootName))
                {
                    jDebug.Log("File name is null or empty ", Color.magenta, true);
                    return;
                }
                
                var aPath = Path.Combine(rootPath, rootName);

                if (rootObject == null)
                {
                    jDebug.Log("Local data is null. Nothing to save. ", Color.magenta, true);
                    return;
                }
            
                if (!Directory.Exists(rootPath))
                {
                    Directory.CreateDirectory(rootPath);
                }
                
                aFileStream = new FileStream(aPath, FileMode.Create);

                var aBinaryFormatter = new BinaryFormatter();
                aBinaryFormatter.Serialize(aFileStream, rootObject);
                
                aFileStream.Close();
                
                jDebug.Log("Local data saved successfully to " + aPath, Color.magenta, true);
#if UNITY_IOS
                UnityEngine.iOS.Device.SetNoBackupFlag(aPath);
#endif
            }
            catch (Exception anException)
            {
                jDebug.LogException(anException);
            }
            finally
            {
                if (aFileStream != null)
                {
                    try
                    {
                        aFileStream.Close();
                        aFileStream.Dispose();
                    }
                    catch (Exception anException)
                    {
                        jDebug.LogException(anException);
                    }
                }
            }
        }

        public virtual void LoadData()
        {
            var aPath = Path.Combine(rootPath, rootName);
            
            jDebug.Log("Loading local data from " + aPath, Color.magenta, true);

            if (isLoaded)
            {
                jDebug.LogWarning("Local data has already been loaded!", true);
                return;
            }
            
            FileStream aFileStream = null;
            try
            {
                var fileInfo = new FileInfo(aPath);
                if (fileInfo.Exists)
                {
                    var elapsedTime = DateTime.Now - fileInfo.LastWriteTime;
                    if (elapsedTime.TotalHours >= jBuildSettings.LocalDataValidityHours)
                    {
                        jDebug.Log("Local data is old! ElapsedTime: " + elapsedTime, Color.magenta, true);
                        ClearData();
                        return;
                    }
                    
                    aFileStream = new FileStream(aPath, FileMode.Open);
                    var aBinaryFormatter = new BinaryFormatter();
                    rootObject = (RootObject)aBinaryFormatter.Deserialize(aFileStream);
                    isLoaded = true;
                    aFileStream.Close();
                    
                    jDebug.Log("Local data loaded successfully!", Color.magenta, true);
                }
                else
                {
                    rootObject = new RootObject();
                    jDebug.Log("Local data file does not exist. Created new empty local data object!", Color.magenta, true);
                }
            }
            catch (Exception anException)
            {
#if UNITY_EDITOR
                jDebug.LogWarning(anException.Message, true);
#else
                jDebug.LogException(anException);
#endif
                if (aFileStream != null)
                {
                    aFileStream.Close();
                }
                
                ClearData();
            }
            finally
            {
                if (aFileStream != null)
                {
                    try
                    {
                        aFileStream.Close();
                    }
                    catch (Exception anException)
                    {
                        jDebug.LogException(anException);
                    }
                }
            }
        }

        public void ClearData()
        {
            jDebug.Log("Clearing local data...", Color.magenta, true);

            try
            {
                if (rootObject != null && rootObject.StorageObjects.Count <= 0)
                {
                    jDebug.Log("Local data is empty. No need to clear.", Color.magenta, true);
                    return;
                }

                if (string.IsNullOrEmpty(rootPath))
                {
                    jDebug.Log("File path is null or empty ", Color.magenta, true);
                    return;
                }

                if (string.IsNullOrEmpty(rootName))
                {
                    jDebug.Log("File name is null or empty ", Color.magenta, true);
                    return;
                }
                
                var aPath = Path.Combine(rootPath, rootName);

                if (File.Exists(aPath))
                {
                    jDebug.Log("Deleting local data file at: " + aPath, Color.magenta, true);
                    
                    File.Delete(aPath);
                    
                    jDebug.Log("Local data cleared successfully!", Color.magenta, true);
                }
                else
                {
                    jDebug.Log("Local data file does not exist at: " + aPath, Color.magenta, true);
                }
            }
            catch (Exception anException)
            {
                jDebug.LogError(anException.Message);
            }
            
            rootObject = new RootObject();
            
            jDebug.Log("Created new empty local data object!", Color.magenta, true);
        }

        [Serializable]
        private class RootObject
        {
            public readonly List<object> StorageObjects = new List<object>();
        }
    }
}
