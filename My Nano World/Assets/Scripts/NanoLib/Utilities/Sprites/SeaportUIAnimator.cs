﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SeaportUIAnimator : MonoBehaviour
{
	[SerializeField] private List<RectTransform> _objectsForAnimation = new List<RectTransform>();
	private bool _animationActive = false;
	private List<float> _animationStops = new List<float>() { 200f, 280f }; 

	void Update()
	{
		if (_objectsForAnimation.Count <= 0)
		{
			Debug.LogError("List of objectsForAnimation is Empty");
			return;
		}
		if (Input.GetButton("Fire1") && !_animationActive)
		{
			_animationActive = true;
			float stop = _animationStops[0];
			var animObject = _objectsForAnimation[Random.Range(0, _objectsForAnimation.Count)];
			foreach (var sto in _animationStops)
				if (sto != animObject.rect.height)
				{
					stop = sto;
					break;
				}

			animObject.DOSizeDelta(new Vector2(animObject.rect.width, stop), 2f)
					.SetEase(Ease.InElastic, .25f)
					.SetEase(Ease.OutBounce, .25f)
					.OnComplete(() => { _animationActive = false; });
		}

	}

}
