﻿using System.Collections.Generic;

namespace NanoLib.Utilities
{
	public static class CollectionUtilities
	{
		public static void AddRange<T>(this IList<T> list, IEnumerable<T> range)
		{
			foreach (var inRange in range) {
			
				list.Add (inRange);
			}
		}
	}
}

