﻿namespace Assets.NanoLib.ServerCommunication.Models
{
	public interface IErrorData
	{
		int HttpErrorCode { get; set; }
		string ErrorMessage { get; set; }
		string URL { get; set; }
		string Response { get; }
		RequestMethod Method { get; }
	}
}