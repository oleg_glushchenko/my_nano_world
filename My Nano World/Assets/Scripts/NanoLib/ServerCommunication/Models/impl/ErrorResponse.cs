﻿using System;

namespace NanoReality.GameLogic.ServerCommunication
{
	[Serializable]
	public class ErrorResponse
	{
		public int code;
		public int status;
		public string name;
		public string message;
		public string type;
	}
}