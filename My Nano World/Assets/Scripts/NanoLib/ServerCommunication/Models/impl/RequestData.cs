﻿using System;
using System.Collections.Generic;
using UnityEngine.Networking;

namespace Assets.NanoLib.ServerCommunication.Models
{
	public class RequestData
    {
	    public RequestMethod RequestMethod;
	    
        public Dictionary<string, string> Parameters;
        
		public Dictionary<string, LazyParameter<string>> LazyParametrs;
		
		public Func<string> UrlAdditionalParams;
		
		public string URL { get; private set; }
        
        public RequestCompleteDelegate SuccessCallback;
        
        public Action<IErrorData> RequestErrorCallback;
        
        public Dictionary<string, string> Headers;
        
        public string ServerAnswer;
        
        /// <summary>
        /// Server response status code.
        /// </summary>
        public int StatusCode;

        public UnityWebRequest WebRequest;

        /// <summary>
        /// How long request executed.
        /// </summary>
        public float Duration;
        
        public long UTC0 => _UTC0;
        private long _UTC0;
        
        public RequestData(RequestMethod requestMethod, Dictionary<string, string> parameters, string url,
            RequestCompleteDelegate successCallback, Action<IErrorData> requestErrorCallback , long utc,
            Dictionary<string, string> headers = null)
        {
            RequestMethod = requestMethod;
            Parameters = parameters;
			LazyParametrs = null;
			UrlAdditionalParams = null;
            URL = url;
            SuccessCallback = successCallback;
            RequestErrorCallback = requestErrorCallback;
            Headers = headers;

            ServerAnswer = "";
            StatusCode = 0;
            _UTC0 = utc;
        }

        public RequestData(string url, RequestMethod method, long utc, RequestCompleteDelegate successCallback, Action<IErrorData> requestErrorCallback, 
	        Dictionary<string, string> parameters = null, Dictionary<string, LazyParameter<string>> lazyParams = null,
	        Dictionary<string, string> headers = null, Func<string> urlAddParams = null)
        {
	        RequestMethod = method;
	        Parameters = parameters;
	        LazyParametrs = lazyParams;
	        UrlAdditionalParams = urlAddParams;
	        URL = url;
	        SuccessCallback = successCallback;
	        RequestErrorCallback = requestErrorCallback;
	        Headers = headers;

	        ServerAnswer = "";
	        StatusCode = 0;
	        _UTC0 = utc;
        }
        
		public RequestData(RequestMethod requestMethod, Dictionary<string, LazyParameter<string>> lazyParams, string url,
			RequestCompleteDelegate successCallback, Action<IErrorData> requestErrorCallback, long utc,
			Dictionary<string, string> headers = null, Func<string> urlAddParams = null)
		{
			RequestMethod = requestMethod;
			Parameters = null;
			LazyParametrs = lazyParams;
			UrlAdditionalParams = urlAddParams;
			URL = url;
			SuccessCallback = successCallback;
			RequestErrorCallback = requestErrorCallback;
			Headers = headers;

			ServerAnswer = "";
			StatusCode = 0;
		    _UTC0 = utc;
		}


		public string GetURL()
		{
			if (UrlAdditionalParams != null)
			{
				string q = UrlAdditionalParams.Invoke ();

				return URL + q;
			}
			return URL;
		}
		
		public bool Equals(RequestData other)
		{
			return RequestMethod == other.RequestMethod && Equals(Parameters, other.Parameters) &&
			       Equals(LazyParametrs, other.LazyParametrs) &&
			       Equals(UrlAdditionalParams, other.UrlAdditionalParams) && string.Equals(URL, other.URL) &&
			       Equals(SuccessCallback, other.SuccessCallback) &&
			       Equals(RequestErrorCallback, other.RequestErrorCallback) && Equals(Headers, other.Headers) &&
			       string.Equals(ServerAnswer, other.ServerAnswer) && StatusCode == other.StatusCode &&
			       _UTC0 == other._UTC0;
		}

		public override bool Equals(object obj)
		{
			return obj is RequestData other && Equals(other);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				var hashCode = (int) RequestMethod;
				hashCode = (hashCode * 397) ^ (Parameters != null ? Parameters.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (LazyParametrs != null ? LazyParametrs.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (UrlAdditionalParams != null ? UrlAdditionalParams.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (URL != null ? URL.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (SuccessCallback != null ? SuccessCallback.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (RequestErrorCallback != null ? RequestErrorCallback.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (Headers != null ? Headers.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (ServerAnswer != null ? ServerAnswer.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ StatusCode;
				hashCode = (hashCode * 397) ^ _UTC0.GetHashCode();
				return hashCode;
			}
		}
		
		public override string ToString()
		{
			return GetURL();
		}
    }

	public class LazyParameter<T>
	{
		private T _param;
		private Func<T> _lazyParam;
		
		public LazyParameter(T simpleParam)
		{
			_param = simpleParam;
		}
		
		public LazyParameter(Func<T> lazyParam)
		{
			_lazyParam = lazyParam;
		}
		
		public T GetValue()
		{
			return _lazyParam != null ? _lazyParam.Invoke () : _param;
		}

		public override string ToString()
		{
			return GetValue().ToString();
		}
	}
}