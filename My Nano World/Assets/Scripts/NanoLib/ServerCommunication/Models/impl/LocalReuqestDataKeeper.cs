﻿using System;
using System.Collections.Generic;

namespace Assets.NanoLib.ServerCommunication.Models
{
    /// <summary>
    /// Класс для кеширования данных
    /// </summary>
    [Serializable]
    public class LocalReuqestDataKeeper
    {
        public List<LocalRequestData> Items = new List<LocalRequestData>();

        public LocalRequestData GetLocalRequestData(string url)
        {
            return Items.Find(curr => curr.Url == url);
        }
    }
}