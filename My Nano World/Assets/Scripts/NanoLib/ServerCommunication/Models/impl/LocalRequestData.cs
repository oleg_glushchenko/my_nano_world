﻿using System;

namespace Assets.NanoLib.ServerCommunication.Models
{
    /// <summary>
    /// Структура с информацией об локальном кешированном запросе
    /// </summary>
    [Serializable]
    public class LocalRequestData
    {
        public string Url;
        public string Hash;
        public string PreloadedHash;
        public string Answer = "";
        public int AnswerSecretHash;
    }
}