﻿using System;
using NanoReality.GameLogic.GameManager.Models.api;
using strange.extensions.mediation.impl;
using UnityEngine;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using Assets.Scripts.Engine.BuildingSystem.CityMap;
using NanoReality.Engine.UI;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Controllers.Agricultural;
using NanoReality.Core.RoadConstructor;
using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Controllers;
using NanoReality.Engine.UI.Extensions;
using NanoReality.Game.Data;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.GameLogic.Common;

namespace NanoLib.Services.InputService
{
    /// <summary>
    /// Класс, определяющий объект и действие, которое произошло после действия пользователя
    /// </summary>
    public class InputLayerChecker : View
    {
        [Tooltip("Для отладки, Действие с которого началась операция ввода")]
        [SerializeField]
        private InputLayerType startedInputLayerType;

        [Tooltip("Для отладки, Текущее состояние ввода")]
        [SerializeField]
        private InputLayerType currentInputLayerType;
        
        [Tooltip("Для отладки, Текущее действие управления")]
        [SerializeField]
        private InputCurrentAction _inputCurrentAction;

        /// <summary>
        /// К какому виду объекта на данный момент прикоснулся игрок
        /// </summary>
        public InputLayerType CurrentInputLayerType
        {
            get => currentInputLayerType;
            private set => currentInputLayerType = value;
        }

        /// <summary>
        /// Действие с которого началась операция ввода
        /// </summary>
        public InputLayerType StartedInputLayerType
        { 
            get => startedInputLayerType;
            private set => startedInputLayerType = value;
        }

        /// <summary>
        /// Текущее действие управления
        /// </summary>
        public InputCurrentAction InputCurrentAction
        {
            get => _inputCurrentAction;
            private set => _inputCurrentAction = value;
        }
        
        public InputState CurrentInputState { get; set; }

        //---------------------------------------------------------
        
        /// <summary>
        /// Текущая позиция тача
        /// </summary>
        private Vector3 _currentTouchPosition;

        /// <summary>
        /// true в тех случаях, когда в данный момент происходит перетаскивание продукта. Необходимо для предотвращения тапа во время перетаскивания
        /// </summary>
        private bool _someItemDragged;

        private Action<DateTime, bool, bool> _checkForTouchAction;
        private Action<Vector3, bool> _checkForTouchDownAction;
        private Action _checkForTouchReleaseAction;
        private Action<float> _calculatePinchAction;
        private Action _calculatePinchEndAction;
        private Action<Vector3, Vector3, bool> _calcualteSwipeAction;
        private bool _longPushSignalSent;

        #region Inject

        [Inject] public IBuildSettings jBuildSettings { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public IConstructionController ConstructionController { get; set; }
        [Inject] public IWorldSpaceCanvas jWorldSpaceCanvas { get; set; }

        [Inject] public SignalOnLongTapMapObjectView jSignalOnLongTapMapObjectView { get; set; }
        [Inject] public SignalOnStartLongTapMapObjectView jSignalOnLongTapMapObjectViewStart { get; set; }
        [Inject] public SignalOnEndLongTapMapObjectView jSignalOnLongTapMapObjectViewEnd { get; set; }
        [Inject] public SignalOnMapObjectViewTap jSignalOnMapObjectViewTap { get; set; }
        [Inject] public SignalOnLockedSubSectorTap jSignalOnLockedSubSectorTap { get; set; }
        [Inject] public SignalOnNonPlayableBuildTap jSignalOnNonPlayableBuildTap { get; set; }


        [Inject] public SignalOnBeginTouch jSignalOnBeginTouch { get; set; }
        [Inject] public SignalOnReleaseTouch jSignalOnReleaseTouch { get; set; }
        [Inject] public SignalCameraSwipe jSignalCameraSwipe { get; set; }

        [Inject] public SignalDragObject jSignalDragObject { get; set; }
        [Inject] public BuildingDragBegin jBuildingDragBegin { get; set; }
        [Inject] public SignalOnSwipe jSignalOnSwipe { get; set; }
        [Inject] public SignalOnSwipeEnd jSignalOnSwipeEnd { get; set; }
        [Inject] public SignalOnTouchNotSwipeEnd jSignalOnTouchNotSwipeEnd { get; set; }
        [Inject] public SignalOnPinch jSignalOnPinch { get; set; }
        [Inject] public SignalOnPinchEnd jSignalOnPinchEnd { get; set; }
        [Inject] public SignalOnUiTap jSignalOnUiTap { get; set; }
        [Inject] public SignalOnGroundTap jSignalOnGroundTap { get; set; }
        [Inject] public SignalOnTerrainTap jSignalOnTerrainTap { get; set; }
        [Inject] public SignalOnManualSwipe SignalOnManualSwipe { get; set; }
        [Inject] public SignalOnManualZoom SignalOnManualZoom { get; set; }

        [Inject] public SignalOnMapMovedUnderTouch jSignalOnMapMovedUnderTouch { get; set; }
        [Inject] public OnItemDragStartedSignal OnItemDragStartedSignal { get; set; }
        [Inject] public OnItemDragEndedSignal OnItemDragEndedSignal { get; set; }
        [Inject] public SignalSimulateLongTap jSignalSimulateLongTap { get; set; }
        [Inject] public RoadConstructor jRoadConstructor{ get; set; }
        
        [Inject] public IInputController jInputController { get; set; }
        [Inject] public GameLoadingModel jGameLoadingModel { get; set; }
        

#if UNITY_EDITOR

        [Inject] public IHardTutorial jHardTutorial { get; set; }
        [Inject] public IHintTutorial jHintTutorial { get; set; }

#endif

        private int _buildingsLayer;
        private int _groundLayer;
        private int _groundBlockLayer;
        private int _lockedSectorLayer;
        private int _nonPlayableBuildingLayer;
        
        protected override void Awake()
        {
            base.Awake();
            CurrentInputState = InputState.CreateDefault();
            _buildingsLayer = LayerMask.NameToLayer("Buildings");
            _groundLayer = LayerMask.NameToLayer("Ground");
            _groundBlockLayer = LayerMask.NameToLayer("GroundBlock");
            _lockedSectorLayer = LayerMask.NameToLayer("LockedSubSector");
            _nonPlayableBuildingLayer = LayerMask.NameToLayer("NonPlayableBuild");
        }

        [PostConstruct]
        public void PostConstruct()
        {
            OnItemDragStartedSignal.AddListener(() => _someItemDragged = true);
            OnItemDragEndedSignal.AddListener(() => _someItemDragged = false);
            jSignalSimulateLongTap.AddListener(OnSimulateLongTouch);
            jSignalOnMapMovedUnderTouch.AddListener(() => { jInputController.OnMapMovedUnderTouch(); });
            jInputController.OnTapSignal.AddListener(Tap);
        }
        
        #endregion

        /// <summary>
        /// Инициализация
        /// </summary>
        protected override void Start()
        {
            base.Start();
            ClearAll();
        }

        protected virtual void Update()
        {
            if (jGameLoadingModel.LoadingState.Value != GameLoadingState.GameReady) return;
            
            if (StartedInputLayerType != InputLayerType.UI)
            {
                jInputController.CheckForTouch(CheckForTouch);
            }

            jInputController.CheckForTouchDown(CheckForTouchDown);
            jInputController.CheckForTouchRelease(CheckForTouchRelease);
            jInputController.CalculatePinch(CalculatePinch);
            jInputController.CalculatePitchEnd(CalculatePinchEnd);

            if (StartedInputLayerType != InputLayerType.UI && InputCurrentAction != InputCurrentAction.ZoomMap)
            {
                jInputController.CalculateSwipe(CalculateSwipe);
            }

#if UNITY_EDITOR

            if (Input.GetKeyUp(KeyCode.F9))
            {
                jHardTutorial.CompleteCurrentStep();
            }

            if (Input.GetKeyUp(KeyCode.F10))
            {
                jHardTutorial.SkipTutorial();
            }

            if (Input.GetKeyUp(KeyCode.F11))
            {
                jHintTutorial.SkipTutorial();
            }
#endif
        }

        /// <summary>
        /// Обработчик события тача в текущий момент
        /// </summary>
        /// <param name="touchStarTime">Начальное время нажатия</param>
        /// <param name="atLeastOneSwipeHappened">Было ли это первое касание</param>
        /// <param name="isPointerOverGameObject">Было ли нажатие на UI</param>
        private void CheckForTouch(DateTime touchStarTime, bool atLeastOneSwipeHappened, bool isPointerOverGameObject)
        {
            if (isPointerOverGameObject)
            {
                CurrentInputLayerType = InputLayerType.UI; // тач сейчас на элементе UI
            }
            else
            {
                // тач сейчас где-то на игровом объекте
                var hit = InputTools.RaycastScene(_currentTouchPosition);

                if (hit.collider == null)
                    return;

                var pressedObject = hit.collider.gameObject;

                if (pressedObject.layer == _buildingsLayer) // тач сейчас на здании
                {
                    var pressedObjectView = pressedObject.GetComponent<MapObjectView>();
                    if (pressedObjectView == null)
                    {
                        Debug.LogError($"InputController raycasted object in Buildings layer, but it hasn't MapObjectView. Name:{pressedObject.name}", pressedObject);
                        return;
                    }

                    CurrentInputLayerType = InputLayerType.Building;

                    if (touchStarTime == DateTime.MaxValue)
                    {
                        return;
                    }
                    var time = (int)(DateTime.Now - touchStarTime).TotalMilliseconds;

                    bool isCorrectLongTapEvent = pressedObjectView.IsMovable &&
                                            InputCurrentAction != InputCurrentAction.DragObject &&
                                            !ConstructionController.IsCreateRoad && !_someItemDragged &&
                                            !atLeastOneSwipeHappened;

                    if (isCorrectLongTapEvent && time >= jBuildSettings.LongTouchProgressDelay)
                    {
                        PushLongTapStartEvent(pressedObjectView);
                    }

                    if (atLeastOneSwipeHappened)
                    {
                        PushLongTapEndEvent();
                    }

                    if (isCorrectLongTapEvent && time >= jBuildSettings.LongTouchDuration)
                    {
                        InputCurrentAction = InputCurrentAction.DragObject;
                        PushLongTapEvent(time, _currentTouchPosition, pressedObjectView);
                    }
                }
                else if (pressedObject.layer == _groundLayer) // тач сейчас на плейне
                {
                    CurrentInputLayerType = InputLayerType.Ground;
                }
            }
        }

        /// <summary>
        /// Обработчик события начала тача
        /// </summary>
        /// <param name="touchPosition">Координаты нажатия</param>
        /// <param name="isPointerOverGameObject">Было ли нажатие на UI</param>
        private void CheckForTouchDown(Vector3 touchPosition, bool isPointerOverGameObject)
        {
            _currentTouchPosition = touchPosition;

            jSignalOnBeginTouch.Dispatch();

            // не позволяем пробиваться инпуту скозь UI
            if (isPointerOverGameObject)
            {
                StartedInputLayerType = InputLayerType.UI;
                CurrentInputLayerType = InputLayerType.UI;
                InputCurrentAction = InputCurrentAction.None;
            }
            else
            {
                var hit = InputTools.RaycastScene(_currentTouchPosition);

                // маусдаун мимо карты
                if (hit.collider == null) return;

                var pressedObject = hit.collider.gameObject;

                if (pressedObject.layer == _buildingsLayer) // тач сейчас на здании
                {
                    StartedInputLayerType = InputLayerType.Building;
                    CurrentInputLayerType = InputLayerType.Building;
                    InputCurrentAction = InputCurrentAction.ScrollMap;

                    if (!ConstructionController.IsCreateRoad)   // если в данный момент не создается дорога, можем выделить объект, иначе отменяем скролл
                    {
                        // если какой либо объект выбран
                        if (ConstructionController.SelectedModel != null)
                        {
                            var obj = pressedObject.GetComponent<MapObjectView>();
                            if (obj != null && obj.MapId == ConstructionController.SelectedModel.MapID)
                            {
                                jBuildingDragBegin.Dispatch();
                                InputCurrentAction = InputCurrentAction.DragObject;
                            }
                            // если же мы нажали не по тому объекту, который мы выбирали - это скролл карты
                            else
                            {
                                InputCurrentAction = InputCurrentAction.ScrollMap;
                            }
                        }
                    }
                    else
                    {
                        var map = jGameManager.GetMap(hit.point);
                        // если тач был возле дороги, за зданием - создаем дорогу
                        if (ConstructionController.SelectedModel != null && map != null && 
                            ConstructionController.CreateMapObject(ConstructionController.SelectedModel.ObjectTypeID, map.BusinessSector,
                                _currentTouchPosition.ToVector3F()))
                        {
                            // отменяем скролл при тапе на дорогу в режиме построки дороги
                            InputCurrentAction = InputCurrentAction.RoadConstruction;
                        }
                        else
                        {
                            InputCurrentAction = InputCurrentAction.ScrollMap;
                        }
                    }
                }
                else if (pressedObject.layer == _groundLayer || pressedObject.layer == _groundBlockLayer ||
                         pressedObject.layer == _lockedSectorLayer ||
                         pressedObject.layer == _nonPlayableBuildingLayer)
                {
                    var map = jGameManager.GetMap(hit.point);
                    if (ConstructionController.IsCreateRoad && ConstructionController.SelectedModel != null &&
                        map != null &&
                        ConstructionController.CreateMapObject(ConstructionController.SelectedModel.ObjectTypeID, map.BusinessSector,
                            _currentTouchPosition.ToVector3F()))
                    {
                        StartedInputLayerType = InputLayerType.Ground;
                        
                        InputCurrentAction = InputCurrentAction.RoadConstruction;
                    }
                    else
                    {
                        StartedInputLayerType = InputLayerType.Ground;
                        CurrentInputLayerType = InputLayerType.Ground;

                        InputCurrentAction = InputCurrentAction.ScrollMap;
                    }
                }
            }
        }

        /// <summary>
        /// Обработчик события окончания тача
        /// </summary>
        private void CheckForTouchRelease(bool isSwipe)
        {
            jSignalOnTouchNotSwipeEnd.Dispatch();
            // сбрасываем флаги, и сообщаем игре о том, что отпустили все тачи
            ClearAll();
            jSignalOnReleaseTouch.Dispatch();

            if (isSwipe)
            {
                jSignalOnSwipeEnd.Dispatch();
            }
        }

        /// <summary>
        /// Тап по объекту\карте\UI
        /// </summary>
        /// <param name="time">Продолжительность тапа в миллисекундах</param>
        /// <param name="selectedGameObject"></param>
        private void Tap(float time, GameObject selectedGameObject)
        {
            if (jGameLoadingModel.LoadingState.Value != GameLoadingState.GameReady) return;
            if (StartedInputLayerType == InputLayerType.None) return; //костыль на случай если игрок быстро и хаотично тапает по всему екрану

            // Если был запущен прогресс бар лонг тапа, и юзер отжал палец раньше времени вызова действия по завершению лонг тапа, отменяем прогресс бар лонг тапа  
            if (_longPushSignalSent)
            {
                PushLongTapEndEvent();
            }
            
            // тап по элементу UIjSignalOnMapObjectTap
            if (StartedInputLayerType == InputLayerType.UI)
            {
                jSignalOnUiTap.Dispatch(selectedGameObject);
                return;
            }

            RaycastHit hit = InputTools.RaycastScene(_currentTouchPosition);

            // тап вообще никуда не попал
            if (hit.collider == null)
                return;

            GameObject pressedObject = hit.collider.gameObject;

            // тап был произведен по зданию
            if (pressedObject.layer == _buildingsLayer) // тап по зданию
            {
                var pressedObjectView = pressedObject.GetComponent<MapObjectView>();

                if (time >= jBuildSettings.LongTouchDuration)
                {
                    PushLongTapEvent((int)time, _currentTouchPosition, pressedObjectView);
                }
                else
                {
                    if(time < 10) return; //костыль на случай когда игрок делает мультитап (NAN-2543)
                    {
                        PushTapEvent(pressedObjectView);
                    }
                }
            }

            // тап был произведен по плейну
            else if (pressedObject.layer == _groundLayer) // тап по плейну
            {
                if (CurrentInputState.IsAvailableType(InputActionType.Tap))
                {
                    var interactionMap = pressedObject.GetComponent<MapInteractorObjectView>();
                    if (interactionMap != null)
                    {
                        interactionMap.OnInteractorTap(hit.point);
                    }
                    jSignalOnGroundTap.Dispatch();
                    jSignalOnTerrainTap.Dispatch(hit);
                }
            }
            else if (pressedObject.layer == _lockedSectorLayer)
            {
                var s = pressedObject.transform.GetComponent<LockedSectorView>();
                if (s != null && CurrentInputState.IsAvailableType(InputActionType.Tap))
                {
                    jSignalOnLockedSubSectorTap.Dispatch (s.Model);
                }
                CurrentInputLayerType = InputLayerType.Ground;
            }
            else if (pressedObject.layer == _nonPlayableBuildingLayer)
            {
                var s = pressedObject.transform.GetComponent<NonPlayableBuildingView>();
                if(s != null)
                {
                    jSignalOnNonPlayableBuildTap.Dispatch(s);
                }
                CurrentInputLayerType = InputLayerType.Ground;
            }
        }

        /// <summary>
        /// Обработчик щипка
        /// </summary>
        /// <param name="pinch">Смещение "щипка"</param>
        private void CalculatePinch(float pinch)
        {
            if (CurrentInputState.IsAvailableType((InputActionType.Drag)))
            {
                SignalOnManualZoom.Dispatch();
                InputCurrentAction = InputCurrentAction.ZoomMap;
                jSignalOnPinch.Dispatch(pinch);
            }
        }

        private void CalculatePinchEnd()
        {
            if (InputCurrentAction == InputCurrentAction.ZoomMap)
            {
                InputCurrentAction = InputCurrentAction.None;
                jSignalOnPinchEnd.Dispatch();
            }
        }

        /// <summary>
        /// Обработка свайпа
        /// </summary>
        /// <param name="swipePosition">Начальные координаты свайпа</param>
        /// <param name="convertedPosition">Координаты смещения</param>
        /// <param name="atLeastOneSwipeHappened">Было ли это первое касание</param>
        private void CalculateSwipe(Vector3 swipePosition, Vector3 convertedPosition)
        {
            _currentTouchPosition = swipePosition;

            if (swipePosition != Vector3.zero)
            {
                switch (InputCurrentAction)
                {
                    case InputCurrentAction.ScrollMap:
                        PushSwipeCameraEvent(convertedPosition);
                        break;
                    case InputCurrentAction.DragObject:
                    {
                        var hit = InputTools.RaycastScene(_currentTouchPosition, 1 << _groundLayer);
                        if (hit.point != Vector3.zero)
                        {
                            PushDragEvent(hit.point);
                        }

                        break;
                    }
                    case InputCurrentAction.RoadConstruction:
                    {
                        jSignalOnSwipe.Dispatch(_currentTouchPosition.ToVector3F());

                        break;
                    }
                }
            }
        }

        private void PushTapEvent(MapObjectView pressedObjectView)
        {
            if(CurrentInputState.IsAvailableType((InputActionType.Tap)))
                jSignalOnMapObjectViewTap.Dispatch(pressedObjectView);
        }

        private void PushDragEvent(Vector3 dragPoint)
        {
            if (CurrentInputState.IsAvailableType((InputActionType.Drag)))
                jSignalDragObject.Dispatch(dragPoint);
        }

        private void PushLongTapStartEvent(MapObjectView target)
        {
            if (_longPushSignalSent == false)
            {
                _longPushSignalSent = true;
                jWorldSpaceCanvas.PopLongTapProgress();
                jSignalOnLongTapMapObjectViewStart.Dispatch(target);
            }
        } 

        private void PushLongTapEndEvent()
        {
            _longPushSignalSent = false;
            jSignalOnLongTapMapObjectViewEnd.Dispatch();
        }

        private void PushLongTapEvent(int time, Vector3 touchPosition, MapObjectView pressedObjectView)
        {
            if(CurrentInputState.IsAvailableType((InputActionType.LongTap)))
                jSignalOnLongTapMapObjectView.Dispatch(time, touchPosition, pressedObjectView);
        }
        
        private void PushSwipeCameraEvent(Vector3 swipePosition)
        {
            if (CurrentInputState.IsAvailableType((InputActionType.Drag)))
                jSignalCameraSwipe.Dispatch(swipePosition.ToVector3F());
        }

        private void PushManualSwipeEvent()
        {
            if(CurrentInputState.IsAvailableType((InputActionType.Drag)))
                SignalOnManualSwipe.Dispatch();
        }

        /// <summary>
        /// Очистка состояний
        /// </summary>
        private void ClearAll()
        {
            _currentTouchPosition = Vector3.zero;
            CurrentInputLayerType = InputLayerType.None;
            StartedInputLayerType = InputLayerType.None;
            InputCurrentAction = InputCurrentAction.None;
            _someItemDragged = false;
        }

        private void OnSimulateLongTouch(MapObjectView view, Vector3 position)
        {
            _currentTouchPosition = position;
            
            var touchStartTime = DateTime.UtcNow.AddSeconds(-jBuildSettings.LongTouchDuration * 2);

            StartedInputLayerType = InputLayerType.Building;
            CurrentInputLayerType = InputLayerType.Building;
            
            var time = (int)(DateTime.Now - touchStartTime).TotalMilliseconds;
            if (!_someItemDragged && time >= jBuildSettings.LongTouchDuration && !ConstructionController.IsCreateRoad)
            {
                jBuildingDragBegin.Dispatch();
                InputCurrentAction = InputCurrentAction.DragObject;
                PushLongTapEvent(time, _currentTouchPosition, view);
            }
        }
    }
}
