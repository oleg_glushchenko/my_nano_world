﻿using Assets.NanoLib.Utilities;
using strange.extensions.signal.impl;
using UnityEngine;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.BuildingSystem.CityMap;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;

namespace NanoLib.Services.InputService
{
    /// <summary>
    /// Сигнал для перемещения камеры
    /// Параметр: смещение свайпа
    /// </summary>
    public class SignalCameraSwipe : Signal<Vector3F> { }

    /// <summary>
    /// Сигнал для перемещения здания
    /// Параметр: текущие координаты
    /// </summary>
    public class SignalDragObject : Signal<Vector3> { }

    public class MapObjectMovedSignal : Signal<MapObjectView> { }

    /// <summary>
    /// Сигнал о свайпе
    /// Параметр: текущие координаты
    /// </summary>
    public class SignalOnSwipe : Signal<Vector3F> { }

    public class SignalOnInputEnable: Signal<bool>{}

	/// <summary>
	/// Вызывается при тапе на вьюшку объекта на карте
	/// </summary>
	public class SignalOnMapObjectViewTap : Signal<MapObjectView> { }

    /// <summary>
    /// Вызывется при тапе на вьюшку залоченного сектора на карте
    /// </summary>
    public class SignalOnLockedSubSectorTap : Signal<CityLockedSector> { }

    /// <summary>
    /// Вызывется при тапе на "не игровом" здании на карте
    /// </summary>
    public class SignalOnNonPlayableBuildTap : Signal<NonPlayableBuildingView>{}

    public class SignalOnBeginTouch : Signal { }

    public class SignalOnPinch : Signal<float> { }

    public class SignalOnPinchEnd : Signal { }

    public class BuildingDragBegin : Signal { }

    public class SignalOnTouchNotSwipeEnd : Signal { }
    public class SignalOnSwipeEnd : Signal { }

    public class SignalOnReleaseTouch : Signal { }

    /// <summary>
    /// Вызывается при длинном тапе на здание
    /// </summary>
    public class SignalOnLongTapMapObjectView : Signal<int, Vector3, MapObjectView> {}

    public class SignalOnStartLongTapMapObjectView : Signal<MapObjectView> { }

    public class SignalOnEndLongTapMapObjectView : Signal { }

    public class SignalOnGroundTap : Signal { }

    public class SignalOnTerrainTap : Signal<RaycastHit> { }

    public class SignalOnUiTap : Signal<object> { }

    public class SignalSimulateLongTap : Signal<MapObjectView, Vector3> { }
}