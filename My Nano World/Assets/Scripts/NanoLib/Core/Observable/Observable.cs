using System.Collections.Generic;
using NanoReality.UI.Components;
using UnityEngine;

namespace NanoLib.Core
{
    public abstract class Observable
    {
        private readonly List<IObserver> _listenersList = new List<IObserver>(3);
        private bool _isDirty;

        public void AddListener(IObserver listener)
        {
            if (_listenersList.Contains(listener))
            {
                Debug.LogError("Can't register listener. Its already exists in listeners list.");
                return;
            }
            
            _listenersList.Add(listener);
        }

        public void RemoveListener(IObserver listener)
        {
            bool result = _listenersList.Remove(listener);
            if (!result)
            {
                Debug.LogError("Listener not exists in listeners list.");
            }
        }

        public void SetChanged()
        {
            _isDirty = true;
        }

        public void NotifyListeners()
        {
            if (!_isDirty)
            {
                return;
            }
            
            foreach (IObserver listener in _listenersList)
            {
                listener.OnChanged();
            }
        }

        public void SetChangedAndNotify()
        {
            SetChanged();
            NotifyListeners();
        }
    }
}