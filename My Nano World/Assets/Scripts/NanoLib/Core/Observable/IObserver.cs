namespace NanoLib.Core
{
    public interface IObserver
    {
        void OnChanged();
    }
}