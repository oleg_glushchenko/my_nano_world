using System;
using System.Collections.Generic;

namespace NanoLib.Core.StateMachine
{
    public interface IState
    {
        Guid Id { get; }

        IStateMachine StateMachine { get; }

        List<IStateTransition> Transitions { get; }

        IState AddNewTransition(IStateMachineEvent trigger, IState destination);

        IState AddNewTransition(string triggerName, IState destination);

        void OnStateEnter();

        void OnStateExit();
    }
}