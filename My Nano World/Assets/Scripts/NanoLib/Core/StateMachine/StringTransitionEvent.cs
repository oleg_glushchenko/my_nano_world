﻿namespace NanoLib.Core.StateMachine
{
    public struct StringTransitionEvent : IStateMachineEvent
    {
        public string EventName { get; }
        public object EventIdentifier { get; }

        public StringTransitionEvent(string eventName)
        {
            EventIdentifier = eventName;
            EventName = eventName;
        }
    }
}