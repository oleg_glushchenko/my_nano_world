﻿using System;
using System.Collections.Generic;

namespace NanoLib.Core.StateMachine
{
    public class StateArgs
    {

    }

    public abstract class State : IState
    {
        private readonly List<IStateTransition> _stateTransitions = new List<IStateTransition>();
        protected readonly StateArgs StateArgs;

        #region IState implementation

        public IStateMachine StateMachine { get; }

        public Guid Id { get; }

        public List<IStateTransition> Transitions => _stateTransitions;
        
        public IState AddNewTransition(IStateMachineEvent trigger, IState destination)
        {
            var transition = new StateTransition(this, destination, trigger);
            _stateTransitions.Add(transition);

            return this;
        }
        
        public IState AddNewTransition(string triggerName, IState destination)
        {
            var transition = new StateTransition(this, destination, new StringTransitionEvent(triggerName));
            _stateTransitions.Add(transition);

            return this;
        }

        public abstract void OnStateEnter();

        public abstract void OnStateExit();

        #endregion

        protected State(IStateMachine parent, StateArgs args)
        {
            Id = Guid.NewGuid();
            StateArgs = args;
            StateMachine = parent;
        }

        protected void PushTransitionEvent(IStateMachineEvent machineEvent)
        {
            StateMachine.HandleEvent(machineEvent);
        }

        protected void PushTransitionEvent(string eventName)
        {
            StateMachine.HandleEvent(new StringTransitionEvent(eventName));
        }

        public override String ToString()
        {
            return $"State: {GetType()} Id: {Id.ToString()}";
        }
    }

    public abstract class State<TArgs> : State where TArgs : StateArgs
    {
        private TArgs _args;

        protected TArgs Args => _args ?? (_args = (TArgs)StateArgs);

        protected State(IStateMachine parent, StateArgs args) : base(parent, args)
        {
        }
    }
}