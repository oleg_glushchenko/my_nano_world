using System;

namespace NanoLib.Core.StateMachine
{
    public interface IStateMachineEvent
    {
        String EventName { get; }

        object EventIdentifier { get; }
    }
}