﻿using System;
using Newtonsoft.Json;
using UnityEngine;

namespace Assets.NanoLib.Utilities
{
    [Serializable]
    public struct Vector2F
    {

        public Vector2F(float X, float Y)
        {
            this.X = X;
            this.Y = Y;
        }

         [JsonProperty("x")]
        public float X;
         [JsonProperty("y")]
        public float Y;


         [JsonIgnore]
        public int intX
        {
            get { return this[0]; } 
            set { this[0]=value; } 
        }

         [JsonIgnore]
        public int intY
        {
            get { return this[1]; }
            set { this[1] = value; }
        }

         [JsonIgnore]
        public float magnitude {
            get { return (float) Math.Sqrt( X*X + Y*Y); }
        }

         [JsonIgnore]
        public float sqrMagnitude
        {
            get { return (X * X + Y * Y); }
        }
        [JsonIgnore]
        public Vector2F normalized
        {
            get
            {
                return new Vector2F(X/magnitude,Y/magnitude);
            }
        }


        public void Normalize()
        {
            this = this.normalized;
        }

        public override string ToString()
        {
            return X.ToString() + " " + Y.ToString();
        }

        #region static variables
        [JsonIgnore]
        public static Vector2F one
        {
            get
            {
                return  new Vector2F(1,1);
            }
        }
        [JsonIgnore]
        public static Vector2F right
        {
            get
            {
                return new Vector2F(1, 0);
            }
        }
        [JsonIgnore]
        public static Vector2F up
        {
            get
            {
                return new Vector2F(0, 1);
            }
        }
        [JsonIgnore]
        public static Vector2F left
        {
            get
            {
                return new Vector2F(-1, 0);
            }
        }
        [JsonIgnore]
        public static Vector2F down
        {
            get
            {
                return new Vector2F(0, -1);
            }
        }
        [JsonIgnore]
        public static Vector2F zero
        {
            get
            {
                return new Vector2F(0, 0);
            }
        }
        #endregion

         [JsonIgnore]
        public int this[int index]    
        {
            get
            {
                if (index == 0)
                {
                    return (int) X;
                }
                else if (index == 1)
                {
                    return (int) Y;
                }
                else
                {
                    throw new ArgumentException();
                }
            }
            set
            {
                if (index == 0)
                {
                    X=value;
                }
                else if (index == 1)
                {
                    Y=value;
                }
                else
                {
                    throw new ArgumentException();
                }
            }
        }


        public static Vector2F operator +(Vector2F v1, Vector2F v2)
        {
            return new Vector2F(v1.X+v2.X,v1.Y+v2.Y);
        }

        public static Vector2F operator -(Vector2F v1, Vector2F v2)
        {
            return new Vector2F(v1.X - v2.X, v1.Y - v2.Y);
        }

        public static Vector2F operator -(Vector2F v1)
        {
            return new Vector2F(-v1.X, -v1.Y);
        }

        public static bool operator ==(Vector2F v1, Vector2F v2)
        {
            return v1.X == v2.X && v1.Y == v2.Y;
        }

        public static bool operator !=(Vector2F v1, Vector2F v2)
        {
            return v1.X != v2.X || v1.Y != v2.Y;
        }

        public override bool Equals(object obj)
        {

            if (obj is Vector2F)
            {
                var vec = (Vector2F)obj;
                return intX == vec.intX && intY == vec.intY;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static Vector2F operator *(Vector2F v1, float d)
        {
            return new Vector2F(v1.X *d, v1.Y * d);
        }

        public static Vector2F operator *(float d, Vector2F v1)
        {
            return new Vector2F(v1.X * d, v1.Y * d);
        }
        
        public static Vector2F operator /(Vector2F v1, float d)
        {
            return new Vector2F(v1.X / d, v1.Y / d);
        }
        
        public static implicit operator Vector2F(Vector2Int vectorInt)
        {
            return new Vector2F { X = vectorInt.x, Y = vectorInt.y};
        }
    }
}
