using System;
using Newtonsoft.Json;

namespace NanoLib.Core
{
    public struct IntValue
    {
        [JsonProperty("value")]
        public int Value;

        public IntValue(int value)
        {
            Value = value;
        }
        
        public static implicit operator IntValue(int x)
        {
            return new IntValue() { Value = x };
        }
        
        public static implicit operator int(IntValue x)
        {
            return x.Value;
        }        
        
        public static implicit operator IntValue(long value)
        {
            return new IntValue(Convert.ToInt32(value));
        }
        
        public static bool operator ==(IntValue a, IntValue b)
        {
            return a.Value == b.Value;
        }

        public static bool operator !=(IntValue a, IntValue b)
        {
            return a.Value != b.Value;
        }

        public override string ToString()
        {
            return Value.ToString();
        }
    }
    
    /// <summary>
    /// TODO: workaround class.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public struct EnumValue<T> where T : Enum
    {
        [JsonProperty("value")]
        public T Value;

        public EnumValue(T value)
        {
            Value = value;
        }
        
        public static implicit operator EnumValue<T>(T x)
        {
            return new EnumValue<T>(x);
        }
    }
}