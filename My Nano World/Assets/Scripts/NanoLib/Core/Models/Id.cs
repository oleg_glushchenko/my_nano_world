﻿using System;
using NanoLib.Core;
using Newtonsoft.Json;
using UnityEngine;

namespace Assets.NanoLib.Utilities
{
    [Serializable]
    [JsonObject]
    public class Id<T> 
    {
        [SerializeField]
        [JsonProperty]
        private int value = -1;

        public Id(int i)
        {
            value = i;
        }

        public Id()
        {
            value = 0;
        }

        [JsonIgnore]
        public int Value
        {
            get { return value; }
        }

        public static bool operator ==(Id<T> a, Id<T> b)
        {
            if ( ((object)a) == null)
            {
                if (((object) b) == null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            if (((object)b) == null)
            {
                return false;
            } 
            return a.value == b.value;
        }

        public static bool operator !=(Id<T> a, Id<T> b)
        {
            if (((object)a) == null)
            {
                if (((object)b) == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            if (((object)b) == null)
            {
                return true;
            }
        
            return a.value != b.value;
        }


        public override bool Equals(object obj)
        {
            Id<T> id = obj as Id<T>;
            if (id != null)
                return id == this;
            else
                return false;
        }

        public override int GetHashCode()
        {
            return value.GetHashCode();
        }

        public override string ToString()
        {
            return value.ToString();
        }

        public static implicit operator Id<T>(Int64 x)
        {
            return new Id<T>((int) x);
        }
        
        public static implicit operator Id<T>(IntValue x)
        {
            return new Id<T>(x);
        }

        public static implicit operator Id<T>(string x)
        {
            int result;
            if (!int.TryParse(x, out result))
            {
                throw new Exception("failed to parse id from string '"+x+"'");
            };
            return new Id<T>(result);
        }
    }
}

