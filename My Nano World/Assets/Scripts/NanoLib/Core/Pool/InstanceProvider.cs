using System;
using strange.framework.api;

namespace NanoLib.Core.Pool
{
    public class InstanceProvider<TInstance> : IInstanceProvider
    {
        private readonly Func<TInstance> _createInstance;

        public InstanceProvider(Func<TInstance> createInstance)
        {
            _createInstance = createInstance;
        }

        public T GetInstance<T>()
        {
            object instance = _createInstance.Invoke();
            T retv = (T) instance;
            return retv;
        }

        public object GetInstance(Type key)
        {
            return _createInstance.Invoke();
        }
    }
}