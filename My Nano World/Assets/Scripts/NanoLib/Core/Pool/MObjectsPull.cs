﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.NanoLib.Utilities.Pulls
{
    [Obsolete("Use IUnityPool instead")]
    public class MObjectsPull : IObjectPullWithPeek
    {
        private Stack<IPullableObject> _instancesStack;
        private IPullableObject _objectPrototype;

        public bool IsInitialized { get; private set; }

        public int Level { get; set; }

        /// <summary>
        /// Instaniting pull with prototype pull object
        /// </summary>
        /// <param name="objectPrototype">Object which will used as prototype for cloning in pull</param>
        public void InstanitePull(IPullableObject objectPrototype)
        {
            InstanitePull(objectPrototype, 0);
        }

        public void InstanitePull(IPullableObject objectPrototype, int preinstancesCount)
        {
            _instancesStack = new Stack<IPullableObject>();
            _objectPrototype = objectPrototype;
            if (objectPrototype == null)
            {
                Debug.LogWarning("You try to initialize pull with nullable prototype object. Check your prefab links");
            }

            for (int i = 0; i < preinstancesCount; i++)
            {
                var copy = (IPullableObject)_objectPrototype.Clone();
                copy.pullSource = this;
                PushInstance(copy);
            }

            IsInitialized = true;
        }

        /// <summary>
        /// Returns a instance from pull just for peek, if pull is empty - create new instace
        /// </summary>
        public IPullableObject Peek()
        {
            return _objectPrototype;
        }

        /// <summary>
        /// Returns a instance from pull, if pull is empty - create new instace
        /// </summary>
        /// <returns></returns>
        public IPullableObject GetInstance()
        {
            IncreasePullIfNeed();
            IPullableObject instance = _instancesStack.Pop();
            instance.OnPop();
            return instance;
        }

        /// <summary>
        /// Returns a instance to pull
        /// </summary>
        /// <param name="instance"></param>
        public void PushInstance(IPullableObject instance)
        {
            instance.OnPush();
            if(_instancesStack.Contains(instance)) return; // avoid double instance in pull
            _instancesStack.Push(instance);
        }

        public void ClearPull()
        {
            if (_instancesStack != null)
            {
                foreach (var pullableObject in _instancesStack)
                {
                    pullableObject.DestroyObject();
                }

                _instancesStack.Clear();
            }
        }

        private void IncreasePullIfNeed()
        {
            if (_instancesStack.Count != 0)
            {
                return;
            }
            
            var instance = (IPullableObject)_objectPrototype.Clone();
            instance.pullSource = this;
            PushInstance(instance);
        }
    }
}