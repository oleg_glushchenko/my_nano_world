﻿using System;

namespace Assets.NanoLib.Utilities.Pulls
{
    /// <summary>
    /// Represent base pull interface
    /// </summary>
    [Obsolete("Use IUnityPool instead")]
    public interface IObjectsPull
    {
        /// <summary>
        /// if true - pull was instantiateed once
        /// </summary>
        bool IsInitialized { get; }

        /// <summary>
        /// Instaniting pull with prototype pull object
        /// </summary>
        /// <param name="objectPrototype">Object which will used as prototype for cloning in pull</param>
        void InstanitePull(IPullableObject objectPrototype);

        /// <summary>
        /// Instaniting pull with prototype pull object, creating preinstances
        /// </summary>
        /// <param name="objectPrototype">Object which will used as prototype for cloning in pull</param>
        /// <param name="preinstancesCount">number of preinstances which will be created</param>
        /// <param name="courutineStarter"></param>
        void InstanitePull(IPullableObject objectPrototype, int preinstancesCount);

        /// <summary>
        /// Returns a instance from pull, if pull is empty - create new instace
        /// </summary>
        /// <returns></returns>
        IPullableObject GetInstance();

        /// <summary>
        /// Returns a instance to pull
        /// </summary>
        /// <param name="instance"></param>
        void PushInstance(IPullableObject instance);

        /// <summary>
        /// Clear pull, destroy all objects in pull
        /// </summary>
        void ClearPull();
    }
}
