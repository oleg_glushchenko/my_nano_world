﻿using strange.framework.api;

namespace NanoLib.Core.Pool
{
    public interface IUnityPool<TPoolable>
    {
        IInstanceProvider InstanceProvider { get; set; }

        TPoolable Pop();

        void Push(TPoolable poolable);

        void Dispose();
    }
}