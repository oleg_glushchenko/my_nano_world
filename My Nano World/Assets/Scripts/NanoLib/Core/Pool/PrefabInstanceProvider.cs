﻿using System;
using strange.framework.api;
using UnityEngine;

namespace NanoLib.Core.Pool
{
    public sealed class PrefabInstanceProvider : IInstanceProvider
    {
        private readonly MonoBehaviour _itemSource;

        public PrefabInstanceProvider(MonoBehaviour itemSource)
        {
            _itemSource = itemSource;
        }

        public T GetInstance<T>()
        {
            return (T)GetInstance(typeof(T));
        }

        public object GetInstance(Type key)
        {
            return UnityEngine.Object.Instantiate(_itemSource);
        }
    }
}