﻿using System;

namespace Assets.NanoLib.Utilities.Pulls
{
    [Obsolete("Use IUnityPool instead")]
    public interface IObjectPullWithPeek : IObjectPullWithLevel
    {
        IPullableObject Peek();  
    }
}