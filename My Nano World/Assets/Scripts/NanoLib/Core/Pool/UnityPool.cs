﻿using strange.extensions.pool.api;
using strange.extensions.pool.impl;
using strange.framework.api;
using UnityEngine;

namespace NanoLib.Core.Pool
{
    public sealed class UnityPool<TPoolable> : Pool<TPoolable>, IUnityPool<TPoolable> where TPoolable : IPoolable
    {
        public IInstanceProvider InstanceProvider
        {
            get => instanceProvider;
            set => instanceProvider = value;
        }

        public UnityPool()
        {
            inflationType = PoolInflationType.INCREMENT;
        }

        public TPoolable Pop()
        {
            TPoolable instance = GetInstance();
            instance.Retain();

            return instance;
        }

        public void Push(TPoolable poolable)
        {
            poolable.Release();
            ReturnInstance(poolable);
        }

        public void Dispose()
        {
            foreach (object instance in instancesAvailable)
            {
                DisposeObject(instance);
            }
            
            foreach (object instance in instancesInUse)
            {
                DisposeObject(instance);
            }
            
            Clean();
            InstanceProvider = null;
        }

        private void DisposeObject(object target)
        {
            ((IPoolable)target).Release();
            // TODO: crunch for save destroy unity GO.
            var monoBehaviour = target as MonoBehaviour;
            if (monoBehaviour != null)
            {
                Object.Destroy(monoBehaviour);
            }
        }

        private new TPoolable GetInstance()
        {
            return base.GetInstance();
        }
    }
}