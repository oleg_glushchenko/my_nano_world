﻿using NanoLib.Core.Logging;
using strange.extensions.command.impl;

namespace NanoReality.StrangeIoC
{
    /// <summary>
    /// An abstract game command that logs execution actions
    /// </summary>
    public abstract class AGameCommand : Command
    {
        public override void Execute()
        {
            $"<b>{GetType().Name}</b> execute.".Log(LoggingChannel.Commands);
        }

        public override void Retain()
        {
            base.Retain();
            
            $"<b>{GetType().Name}</b> retained.".Log(LoggingChannel.Commands);
        }

        public override void Release()
        {
            base.Release();
            
            $"<b>{GetType().Name}</b> released.".Log(LoggingChannel.Commands);
        }
    }

    public abstract class CommandArgs
    {
    }
}