﻿using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

namespace NanoLib.Core.Logging.Settings
{
    [CreateAssetMenu(fileName = "LoggingSettings", menuName = "NanoReality/Settings/LoggingSettings", order = 10)]
    public class LoggingSettings : SerializedScriptableObject
    {
        [SerializeField] private Dictionary<LoggingChannel, Color> _channelsSettings = new Dictionary<LoggingChannel, Color>();
        [SerializeField] private LoggingChannel _activeChannels;

        public Dictionary<LoggingChannel, string> Channels
        {
            get
            {
                return _channelsSettings.ToDictionary(pair
                    => pair.Key, pair => ColorUtility.ToHtmlStringRGB(pair.Value));
            }
        }
        public LoggingChannel ActiveChannels => _activeChannels;
        
        [Button("Apply", ButtonSizes.Large, ButtonStyle.CompactBox)]
        private void Apply()
        {
            Logging.SetChannels(_activeChannels);

            $"Applied logs channels: {_activeChannels}".Log(LoggingChannel.Editor);
        }
    }
}