﻿using System;
using System.Diagnostics;
using Assets.NanoLib.UtilitesEngine.CustomDebugging.Models.api;
using NanoLib.Core.Logging;
using UnityEngine;
using Logging = NanoLib.Core.Logging.Logging;

namespace Assets.NanoLib.UtilitesEngine.CustomDebugging.Models.impl
{
    public class Debug : IDebug
    {
        public void Log(object obj, bool isTest = false)
        {
            Logging.Log(LoggingChannel.Debug, obj.ToString());
        }

        public void Log(object obj, Color color, bool isTest = false)
        {
            Logging.Log(LoggingChannel.Debug, obj.ToString());
        }

        public void LogError(object obj, bool isTest = false, StackFrame stackFrame = null)
        {
            Logging.LogError(LoggingChannel.Debug, obj.ToString());
        }

        public void LogWarning(object obj, bool isTest = false)
        {
            Logging.Log(LoggingChannel.Debug, obj.ToString());
        }

        public void LogException(Exception exception)
        {
            Logging.LogException(exception);
        }
    }
}