﻿using System;
using System.Diagnostics;
using UnityEngine;

namespace Assets.NanoLib.UtilitesEngine.CustomDebugging.Models.api
{
    public interface IDebug
    {
        void Log(object obj, bool isTest = false);

        void Log(object obj, Color color, bool isTest = false);

        void LogError(object obj, bool isTest = false, StackFrame stackFrame = null);

        void LogWarning(object obj, bool isTest = false);

        void LogException(Exception exception);
    }
}