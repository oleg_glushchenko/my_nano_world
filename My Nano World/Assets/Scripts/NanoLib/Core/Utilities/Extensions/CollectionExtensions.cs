﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Extensions
{
    public static class CollectionExtensions
    {
        public static void ForEach<T>(this IEnumerable<T> first, Action<T> action)
        {
            foreach (T t in first)
            {
                action(t);
            }
        }
        
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> value) => value == null || !value.Any();

        public static string GetValuesString<TKey,TValue>(this IDictionary<TKey, TValue> dictionary)
        {
            var stringBuilder = new StringBuilder();
            foreach (KeyValuePair<TKey, TValue> keyValuePair in dictionary)
            {
                stringBuilder.Append($"[{keyValuePair.Key}:{keyValuePair.Value}]");
            }

            return stringBuilder.ToString();
        }
    }
}