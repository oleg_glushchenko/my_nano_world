using System;

namespace NanoReality.Game.Tutorial
{
    public static class EnumExtensions
    {
        /// <summary>
        /// Gets next enum value.
        /// </summary>
        /// <param name="enumValue">Current value</param>
        /// <typeparam name="T">Type</typeparam>
        /// <returns></returns>
        /// <exception cref="ArgumentException">If generic type is not Enum.</exception>
        public static T NextValue<T>(this T enumValue) where T : struct
        {
            if (!typeof(T).IsEnum)
                throw new ArgumentException($"Argument {typeof(T).FullName} is not an Enum");

            T[] enumValues = (T[]) Enum.GetValues(enumValue.GetType());
            int j = Array.IndexOf(enumValues, enumValue) + 1;
            return (enumValues.Length == j) ? enumValues[0] : enumValues[j];            
        }
    }
}