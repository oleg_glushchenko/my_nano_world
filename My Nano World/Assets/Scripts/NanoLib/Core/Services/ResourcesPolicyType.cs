namespace NanoLib.AssetBundles
{
    public enum ResourcesPolicyType : byte
    {
        LocalResources = 0,
        LocalAssetBundles = 1,
        RemoteAssetBundles = 2
    }
}