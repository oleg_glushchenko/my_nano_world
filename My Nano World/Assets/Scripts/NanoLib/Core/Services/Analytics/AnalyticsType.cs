/// <summary>
/// предпологается 3 вида аналитики,  и случай кога события отправляются во все трекеры
/// </summary>
public enum AnalyticsType
{
    /// <summary>
    /// аналитика на нашем сервере
    /// </summary>
    Native,

    /// <summary>
    /// аналитика юнити
    /// </summary>
    Unity,

    /// <summary>
    /// аналитика http://www.gameanalytics.com
    /// </summary>
    GameAnalytics,

    /// <summary>
    /// https://hq.appsflyer.com/
    /// </summary>
    AppsFlyer,

    /// <summary>
    /// All trackers
    /// </summary>
    All
}