﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class LongPressEventTrigger : UIBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler, IPointerClickHandler
{
    [Tooltip("How long must pointer be down on this object to trigger a long press")]
    public float durationThreshold = 1.0f;

    public UnityEvent onLongPress = new UnityEvent();
    public UnityEvent onPointerUp = new UnityEvent();
    public UnityEvent onPointerDown = new UnityEvent();
    public UnityEvent onPointerClick = new UnityEvent();


    private bool isPointerDown = false;
    private bool longPressTriggered = false;
    private float timePressStarted;
    private bool _interactable = true;

    public bool Interactable
    {
        get => _interactable;
        set => _interactable = value;
    }


    private void Update()
    {
        if (!_interactable)
            return;
        if (isPointerDown && !longPressTriggered)
        {
            if (Time.time - timePressStarted > durationThreshold)
            {
                longPressTriggered = true;
                onLongPress.Invoke();
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (!_interactable)
            return;
        onPointerDown.Invoke();
        timePressStarted = Time.time;
        isPointerDown = true;
        longPressTriggered = false;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (!_interactable)
            return;
        isPointerDown = false;
        onPointerUp.Invoke();
    }


    public void OnPointerExit(PointerEventData eventData)
    {
        if (!_interactable)
            return;
        isPointerDown = false;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!_interactable)
        {
            return;
        }
        onPointerClick.Invoke();
    }
}