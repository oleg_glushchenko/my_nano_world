﻿using System;
using System.Collections.Generic;
using Assets.NanoLib.UtilitesEngine;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using UnityEngine;
using UnityEngine.EventSystems;

namespace NanoLib.Services.InputService
{
    /// <summary>
    /// Контроллер управления тачем
    /// </summary>
    public class TouchInputController : View, IInputController
    {
        /// <summary>
        /// Сигнал тапа (щелчка)
        /// float - время удержания тапа
        /// GameObject - текущий выбранный объект
        /// </summary>
        public Signal<float, GameObject> OnTapSignal { get; private set; }

        [SerializeField]
        private float _swipeTolerance = 0.05f;

        /// <summary>
        /// Текущая позиция тача
        /// </summary>
        private Vector3 _currentTouchPosition;
        
        /// <summary>
        /// Позиция последнего нажатия
        /// </summary>
        private Vector3 _startTouchPosition;

        /// <summary>
        /// Стартовое время нажатия (для определения режима выбора здания)
        /// </summary>
        private DateTime _startTouchTime;

        /// <summary>
        /// true в тех случаях, когда в данный момент происходит свайп. Необходимо для предотвращения тапа во время свайпа
        /// </summary>
        private bool _atLeastOneSwipeHappened;

        private bool _isPinchStarted = false;

        public Vector3 CurrentTouchPosition => _currentTouchPosition;

        public Vector3 CurrentFingerPosition => Input.mousePosition;

        private float PinchValue 
        {
            get
            {
                var result = 0f;
                // Только два тача могут означать пинч
                if (Input.touchCount != 2)
                {
                    return result;
                }

                // сбрасываем значение начала тача для избегания нежелательного тача
                _startTouchTime = DateTime.MaxValue;

                // TODO: когда один из пальцев мы отпустим, нужно сохранять инфу что мы делали пинч,
                // TODO: что бы затем не наткнуться на тап или свайп или драг, ведь палец будет уже один

                // Сохраняем оба тача
                var firstTouch = Input.GetTouch(0);
                var secondTouch = Input.GetTouch(1);

                if (EventSystemManager.IsPointerOverGameObject(firstTouch.fingerId) ||
                    EventSystemManager.IsPointerOverGameObject(secondTouch.fingerId))
                {
                    return result;
                }

                // Находим предыдущие позиции тачей
                var firstTouchPrevPos = firstTouch.position - firstTouch.deltaPosition;
                var secondTouchPrevPos = secondTouch.position - secondTouch.deltaPosition;

                // находим магнитуду векторов (дистанцию между ними, в общем)
                var prevTouchDeltaMag = (firstTouchPrevPos - secondTouchPrevPos).magnitude;
                var touchDeltaMag = (firstTouch.position - secondTouch.position).magnitude;

                // находим разницу
                result = (prevTouchDeltaMag - touchDeltaMag) * Time.deltaTime;
                return result;
            }
        }

        private bool IsHavePinch => !PinchValue.Equals(0);

        private EventSystem EventSystemManager => EventSystem.current;

        /// <summary>
        /// Инициализация
        /// </summary>
        protected override void Start()
        {
            base.Start();
            ClearAll();
        }

        /// <summary>
        /// Инициализация после инжектов
        /// </summary>
        [PostConstruct]
        public void PostConstruct()
        {
            OnTapSignal = new Signal<float, GameObject>();
        }
        
        /// <summary>
        /// Очистка состояний управления
        /// </summary>
        private void ClearAll()
        {
            _startTouchPosition = Vector3.zero;
            _currentTouchPosition = Vector3.zero;
            _atLeastOneSwipeHappened = false;
            _startTouchTime = DateTime.MaxValue;
        }

        /// <summary>
        /// Проверка события тача в текущий момент
        /// </summary>
        /// <param name="callback">Коллбек обработки (параметры: начальное время нажатия, было ли это первое касание, было ли нажатие на UI)</param>
        public void CheckForTouch(Action<DateTime, bool, bool> callback)
        {
            if (Input.touchCount != 1) return;

            bool isPointerOverGameObject = false;
            foreach (Touch touch in Input.touches)
                isPointerOverGameObject |= EventSystemManager.IsPointerOverGameObject(touch.fingerId);
            
            callback(_startTouchTime, _atLeastOneSwipeHappened, isPointerOverGameObject);
        }

        /// <summary>
        /// Проверка события начала тача
        /// </summary>
        /// <param name="callback">Коллбек обработки (параметры: координаты нажатия, было ли нажатие на UI)</param>
        public void CheckForTouchDown(Action<Vector3, bool> callback)
        {
            // было ли начато нажатие
            if (Input.touchCount != 1 ||
                    Input.GetTouch(0).phase != TouchPhase.Began) return;

			
            // запоминаем где начали нажимать
            _startTouchPosition = Input.GetTouch(0).position;
            _currentTouchPosition = _startTouchPosition;

            // запоминаем время нажатия
            _startTouchTime = DateTime.Now;

            _atLeastOneSwipeHappened = false;

            //при мнгочисленых тапах выдает false (не смотря на то что игрок тапал на UI) NAN-4349
            //callback(_startTouchPosition, EventSystemManager.IsPointerOverGameObject(UnityEngine.Input.GetTouch(0).fingerId));

            callback(_startTouchPosition, IsPointerOverUiObject());
        }

        /// <summary>
        /// Проверяем тапнул ли игрок на UI обьект
        /// </summary>
        /// <returns>true - был тап на UI</returns>
        private bool IsPointerOverUiObject()
        {
            var eventDataCurrentPosition = new PointerEventData(EventSystem.current)
            {
                position = new Vector2(Input.mousePosition.x, Input.mousePosition.y)
            };

            var results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            return results.Count > 0;
        }

        /// <summary>
        /// Проверка события окончания тача
        /// </summary>
        /// <param name="callback">Коллбек окончания нажатия</param>
        public void CheckForTouchRelease(Action<bool> callback)
        {
            if (Input.touchCount != 1 ||
                !(Input.GetTouch(0).phase == TouchPhase.Ended))
                return;
            
            bool isSwipe = _atLeastOneSwipeHappened;

            if (!isSwipe)
            {
                OnTapSignal.Dispatch((float)(DateTime.Now - _startTouchTime).TotalMilliseconds, EventSystemManager.currentSelectedGameObject);
            }

            ClearAll();

            callback.Invoke(isSwipe);
        }
      
        /// <summary>
        /// Просчитываем щипок
        /// </summary>
        /// <param name="callback">Коллбек обработки (параметр: смещение "щипка")</param>
        public void CalculatePinch(Action<float> callback)
        {
            if (IsHavePinch)
            {
                _isPinchStarted = true;
                callback(PinchValue);
            }
        }

        public void CalculatePitchEnd(Action callback)
        {
            if (_isPinchStarted && Input.touchCount < 2)
            {
                _isPinchStarted = false;
                callback();
            }
        }

        public void OnMapMovedUnderTouch()
        {
            _atLeastOneSwipeHappened = true;
        }
        
        public void CalculateSwipe(Action<Vector3, Vector3> callback)
        {
            // Свайп возможен только при одном таче, и если тач был начат не на UI элементе
            if ((Input.touchCount != 1) ||
                (Input.GetTouch(0).phase != TouchPhase.Moved &&
                 Input.GetTouch(0).phase != TouchPhase.Stationary))
            {
                return;
            }
            
            _currentTouchPosition = Input.GetTouch(0).position;
            
            var delta = InputTools.ConvertDisplacement(_startTouchPosition - _currentTouchPosition);

            if (!_atLeastOneSwipeHappened &&
                (Math.Abs(delta.x) > _swipeTolerance || Math.Abs(delta.z) > _swipeTolerance))
            {
                // мы уже знаем что произошел свайп, запоминаем эту информацию, что бы предотвратить тап в будущем
                _atLeastOneSwipeHappened = true;
                // сбрасываем значение долгого тача
                _startTouchTime = DateTime.Now;
            }

            // после вычисления разницы в следующий раз отталкиваться будем уже от текущего значения тача,
            // что бы не возвращаться каждый раз в "первое начало" тача
            _startTouchPosition = _currentTouchPosition;

            var convertedPosition = new Vector3(delta.x, delta.y, delta.z);
            callback(_startTouchPosition, convertedPosition);
        }
    }
}