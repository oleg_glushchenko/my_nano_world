﻿using System;
using strange.extensions.signal.impl;
using UnityEngine;

namespace NanoLib.Services.InputService
{
    [Flags] 
    public enum InputActionType : short
    {
        None = 0,
        Tap = 1,
        LongTap = 2,
        Drag = 4
    };
    
    public class InputState
    {
        public InputActionType CurrentState = InputActionType.None;

        public bool IsAvailableType(InputActionType request)
        {
            return CurrentState.HasFlag(request);
        }
        
        public override string ToString()
        {
            return $"InputState: Active types:{CurrentState}";
        }

        public static InputState CreateDefault()
        {
            return new InputState {CurrentState = InputActionType.Tap | InputActionType.LongTap | InputActionType.Drag};
        }
    }
    
    /// <summary>
    /// Интерфейс действий пользователя
    /// </summary>
    public interface IInputController
    {
        Vector3 CurrentTouchPosition { get; }

        Vector3 CurrentFingerPosition { get; }

        /// <summary>
        /// Сигнал тапа (щелчка)
        /// float - время удержания тапа
        /// GameObject - текущий выбранный объект
        /// </summary>
        Signal<float, GameObject> OnTapSignal { get; }

        /// <summary>
        /// Проверка события тача в текущий момент
        /// </summary>
        /// <param name="callback">Коллбек обработки (параметры: начальное время нажатия, было ли это первое касание, было ли нажатие на UI)</param>
        void CheckForTouch(Action<DateTime, bool, bool> callback);

        /// <summary>
        /// Проверка нажатия
        /// </summary>
        /// <param name="callback">Коллбек обработки (параметры: координаты нажатия, было ли нажатие на UI)</param>
        void CheckForTouchDown(Action<Vector3, bool> callback);

        /// <summary>
        /// Проверка окончания нажатия
        /// </summary>
        /// <param name="callback">Коллбек окончания нажатия</param>
        void CheckForTouchRelease(Action<bool> callback);

        /// <summary>
        /// Расчет "щипка" (прокрутки колесика) 
        /// </summary>
        /// <param name="callback">Коллбек обработки (параметр: смещение "щипка")</param>
        void CalculatePinch(Action<float> callback);

        /// <summary>
        /// Расчет окончания  "щипка" (прокрутки колесика) 
        /// </summary>
        void CalculatePitchEnd(Action callback);

        /// <summary>
        /// Расчет свайпа 
        /// </summary>
        /// <param name="callback">Коллбек обработки (параметры: начальные координаты свайпа, координаты смещения, было ли это первое касание)</param>
        void CalculateSwipe(Action<Vector3, Vector3> callback);

        void OnMapMovedUnderTouch();
    }

    /// <summary>
    /// Для статического вызова RayCastScene
    /// </summary>
    public static class InputTools
    {
        /// <summary>
        /// Пускает луч в позицию, указанную в touchPosition, и возвращает RaycastHit объект
        /// из которого затем можно получить информацию об объектах, встретившихся на пути луча
        /// </summary>
        /// <param name="touchPosition">позиция нажатия</param>
        /// <param name="layerMask">маска слоёв</param>
        /// <returns>Результат запуска луча с объектами, с которыми он столкнулся</returns>
        public static RaycastHit RaycastScene(Vector3 touchPosition, int layerMask = -1)
        {
            RaycastHit hit;
            var ray = Camera.main.ScreenPointToRay(new Vector3(touchPosition.x, touchPosition.y));

            return Physics.Raycast(ray, out hit, 1000f, layerMask) ? hit : new RaycastHit();
        }
        
        public static bool RaycastSceneOptimized(Vector3 touchPosition, out RaycastHit resultHit, int layerMask = -1)
        {
            var ray = Camera.main.ScreenPointToRay(new Vector3(touchPosition.x, touchPosition.y));

            return Physics.Raycast(ray, out resultHit, 1000f, layerMask);
        }

        public static bool SphereRaycastScene(Vector3 touchPosition, int layerMask = -1)
        {
            var ray = Camera.main.ScreenPointToRay(new Vector3(touchPosition.x, touchPosition.y));
            
            return Physics.SphereCast(ray, 2f, 1000f, layerMask);
        }
        
        public static Vector3 ConvertDisplacement(Vector3 delta)
        {
            delta.z = delta.y;
            delta.y = 0;
            delta.x /= Screen.width;
            delta.z /= Screen.height;
            var yOffset = delta.z / Mathf.Cos(45 * Mathf.Deg2Rad);
            var xOffset = delta.x / Mathf.Cos(45 * Mathf.Deg2Rad);
            delta.x = xOffset + yOffset;
            delta.z = -xOffset + yOffset;

            return delta;
        }
    }

    /// <summary>
    /// Текущее действие пользователя
    /// </summary>
    public enum InputCurrentAction
    {
        /// <summary>
        /// Нет действия
        /// </summary>
        None,

        /// <summary>
        /// Прокрутка карты
        /// </summary>
        ScrollMap,

        /// <summary>
        /// Перетаскивание объекта
        /// </summary>
        DragObject,

        /// <summary>
        /// Изменение масштаба
        /// </summary>
        ZoomMap,
        RoadConstruction,
    }

    /// <summary>
    /// Перечисление состояний: к чему прикоснулся игрок
    /// </summary>
    public enum InputLayerType
    {
        /// <summary>
        /// Пользователь ни к чему не прикоснулся
        /// </summary>
        None,

        /// <summary>
        /// Пользователь прикоснулся к земле
        /// </summary>
        Ground,

        /// <summary>
        /// Пользователь прикоснулся к UI
        /// </summary>
        UI,

        /// <summary>
        /// Пользователь прикоснулся к зданию
        /// </summary>
        Building
    }
}