using strange.extensions.signal.impl;

namespace NanoLib.Core.Timers
{
    public sealed class ServerTimeTickSignal : Signal<long> { }
}