﻿using System;

namespace Assets.NanoLib.UtilitesEngine
{
    public interface ITimer : IDisposable
    {
        event Action OnFinishedAction;
        event Action OnCancelTimerAction;

        float TimeoutDuration { get; }

        float TimeLeft { get; }

        void StartRealTimer(float durationTime, Action onFinishedAction = null, Action<float> onTickAction = null, float tickDuration = 1f);

        void CancelTimer(bool complete = false);
    }
}