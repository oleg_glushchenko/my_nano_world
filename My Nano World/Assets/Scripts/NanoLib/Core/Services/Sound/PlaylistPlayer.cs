﻿using NanoLib.SoundManager;

namespace NanoLib.Core.Services.Sound
{
    public sealed class PlaylistPlayer
    {
        private readonly SoundSettings _soundSettings;
        private readonly ISoundManager _soundManager;

        private SoundtrackPlaylist _currentPlaylist;
        private string _currentTrackName;
        private bool _shuffle;

        private const SoundChannel PlaylistChannel = SoundChannel.Music;

        public PlaylistPlayer(ISoundManager soundManager, SoundSettings soundSettings)
        {
            _soundSettings = soundSettings;
            _soundManager = soundManager;
        }
        
        public void Play(string playlistName, bool shuffle = false)
        {
            _shuffle = shuffle;
            Stop();

            _currentPlaylist = _soundSettings.SoundClipsStorage.GetPlayList(playlistName);;
            _currentTrackName = _shuffle ? _currentPlaylist.GetRandomNext : _currentPlaylist.GetFirst;
            PlayTrack(_currentTrackName);
        }
        
        public void Stop()
        {
            _soundManager.Stop(_currentTrackName, PlaylistChannel);
        }

        private void PlayTrack(string trackName)
        {
            _soundManager.Play(trackName, PlaylistChannel, finishedCallback:OnTrackFinished);
        }

        private void OnTrackFinished(string trackName)
        {
            var nextTrack = _shuffle ? _currentPlaylist.GetRandomNext : _currentPlaylist.GetNext;
            PlayTrack(nextTrack);
        }
    }
}