﻿using System;
using System.Collections.Generic;
using NanoLib.Core.Pool;
using NanoLib.Core.Services.Sound;
using NanoLib.SoundManager;
using UnityEngine;
using UnityEngine.Audio;
using Object = UnityEngine.Object;

namespace NanoLib.Core.Services.Sound
{
    public sealed class SoundManager : ISoundManager
    {
        [Inject] public SoundSettings jSoundSettings { get; set; }

        private readonly Dictionary<SoundChannel, List<SoundSource>> _activeSoundSources;
        private readonly IUnityPool<SoundSource> _soundSourcesPool;
        private PlaylistPlayer _playlistPlayer;
        private GameObject _soundSourcesContainer;

        public SoundManager()
        {
            int initialCollectionSize = 5;
            _soundSourcesPool = new UnityPool<SoundSource>();
            _activeSoundSources = new Dictionary<SoundChannel, List<SoundSource>>
            {
                { SoundChannel.None, new List<SoundSource>(initialCollectionSize) },
                { SoundChannel.Music, new List<SoundSource>(initialCollectionSize) },
                { SoundChannel.SoundFX, new List<SoundSource>(initialCollectionSize) },
            };
        }

        [PostConstruct]
        public void PostConstruct()
        {
            jSoundSettings.Init();
            _playlistPlayer = new PlaylistPlayer(this, jSoundSettings);
            _soundSourcesPool.InstanceProvider = new PrefabInstanceProvider(jSoundSettings.SoundSourcePrefab);
            _soundSourcesContainer = new GameObject("SoundSources");
        }

        [Deconstruct]
        public void Deconstruct()
        {
            foreach (KeyValuePair<SoundChannel,List<SoundSource>> keyValuePair in _activeSoundSources)
            {
                foreach (SoundSource activeSource in keyValuePair.Value)
                {
                    activeSource.Stop();
                }
            }

            _soundSourcesPool.Dispose();
            Object.Destroy(_soundSourcesContainer);
        }

        #region ISoundManager implementation

        private SoundSource Play(string audioClipName, SoundChannel channel = SoundChannel.None, bool isLooped = false, 
            bool playMuted = false, Action<string> finishedCallback = null)
        {
            if (string.IsNullOrEmpty(audioClipName))
            {
                Debug.LogWarning("[Sound] Audio clip name can't be null or empty.");
                return null;
            }

            try
            {
                AudioClip clip = jSoundSettings.SoundClipsStorage.GetClip(audioClipName);
                if (clip == null)
                {
                    Debug.LogWarning($"[Sound] Can't play sound clip [{audioClipName}]. It's null.");
                    return null;
                }

                AudioMixerGroup mixerGroup = jSoundSettings.GetAudioMixerGroup(channel);
                SoundSource soundSource = _soundSourcesPool.Pop();
                soundSource.Finished += OnSoundSourcePlayFinished;
                soundSource.Finished += (c) => { finishedCallback?.Invoke(audioClipName); };
                soundSource.SoundChannel = channel;
                soundSource.Mute = playMuted;
                soundSource.ClipName = audioClipName;
                soundSource.transform.SetParent(_soundSourcesContainer.transform);
                soundSource.Play(clip, mixerGroup, isLooped, jSoundSettings.GetFadeDuration(channel));
                
                if (!_activeSoundSources.TryGetValue(channel, out List<SoundSource> sourcesList))
                {
                    Debug.LogWarning($"[Sound] Sound channel {channel} not bound in active sources list.");
                    return null;
                }
                
                sourcesList.Add(soundSource);
                return soundSource;
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                return null;
            }
        }

        private static void SetChannelVolume(float currentVolume, bool isMuted, AudioMixer mixer, string channelName, out float savedVolume, float minValue, float maxVolume)
        {
            float result = Mathf.Lerp(minValue, maxVolume, Mathf.Clamp01(currentVolume));
            if (!isMuted)
            {
                mixer.SetFloat(channelName, result);
            }

            savedVolume = result;
        }
        
        public SoundSource Play(string audioClipName, SoundChannel channel = SoundChannel.None, bool isLooped = false, Action<string> finishedCallback = null)
        {
            return Play(audioClipName, channel, isLooped, jSoundSettings.GetChannelSoloState(channel), finishedCallback);
        }

        public SoundSource Play(SfxSoundTypes sfxClip, SoundChannel channel = SoundChannel.None, bool isLooped = false, Action<string> finishedCallback = null)
        {
            string audioClipName = Enum.GetName(typeof(SfxSoundTypes), sfxClip);
            return Play(audioClipName, channel, isLooped, jSoundSettings.GetChannelSoloState(channel), finishedCallback);
        }

        public SoundSource PlaySolo(SfxSoundTypes sfxClip, SoundChannel channel = SoundChannel.None, bool isLooped = false, Action<string> finishedCallback = null)
        {
            SetChannelSourcesMuted(channel, true);
            string audioClipName = Enum.GetName(typeof(SfxSoundTypes), sfxClip);
            return Play(audioClipName, channel, isLooped, false, finishedCallback);
        }

        public void StopSolo(SfxSoundTypes sfxClip, SoundChannel channel)
        {
            Stop(sfxClip, channel);
            SetChannelSourcesMuted(channel, false);
        }

        public void StopChannel(SoundChannel channel)
        {
            foreach (KeyValuePair<SoundChannel, List<SoundSource>> channelSourcePair in _activeSoundSources)
            {
                foreach (SoundSource activeSource in channelSourcePair.Value)
                {
                    activeSource.Stop();
                }
            }
        }

        public void Stop(string audioClipName, SoundChannel channel)
        {
            if (_activeSoundSources.TryGetValue(channel, out List<SoundSource> activeSourcesList))
            {
                List<SoundSource> targetSources = activeSourcesList.FindAll(c => c.ClipName == audioClipName);
                if (channel == SoundChannel.Music)
                {
                    foreach (SoundSource soundSource in targetSources)
                    {
                        soundSource.StopFaded();
                    }
                }
                else
                {
                    foreach (SoundSource soundSource in targetSources)
                    {
                        soundSource.Stop();
                    }
                }
            }
        }

        public void Stop(SfxSoundTypes sfxClip, SoundChannel channel = SoundChannel.None)
        {
            string audioClipName = Enum.GetName(typeof(SfxSoundTypes), sfxClip);
            Stop(audioClipName, channel);
        }

        public void SetChannelActive(bool isOn, SoundChannel channel)
        {
            jSoundSettings.SetChannelActive(channel, isOn);
        }

        public void PlayPlaylist(string playlistName, bool useRandom = false)
        {
            _playlistPlayer.Play(playlistName, useRandom);
        }

        public void StopPlaylist()
        {
            _playlistPlayer.Stop();
        }

        #endregion

        private void OnSoundSourcePlayFinished(SoundSource soundSource)
        {
            soundSource.Finished -= OnSoundSourcePlayFinished;
            _soundSourcesPool.Push(soundSource);
            if (_activeSoundSources.TryGetValue(soundSource.SoundChannel, out List<SoundSource> channelSources))
            {
                channelSources.Remove(soundSource);

            }
            else
            {
                Debug.LogWarning("[Sound] SoundSource isn't contains in active sources list.");
            }
        }

        private void SetChannelSourcesMuted(SoundChannel channel, bool isMute)
        {
            jSoundSettings.SetChannelSoloState(channel, isMute);
            if(!_activeSoundSources.TryGetValue(channel, out List<SoundSource> activeSourcesList))
            {
               Debug.LogWarning($"[Sound] Can't mute sound sources of channel: {channel}");
               return;
            }
            
            foreach (SoundSource activeSource in activeSourcesList)
            {
                activeSource.Mute = isMute;
            }
        }
    }
}