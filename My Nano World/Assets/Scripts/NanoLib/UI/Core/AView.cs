﻿using strange.extensions.mediation.impl;

namespace NanoReality.StrangeIoC
{
    public abstract class AView : View
    {
        public virtual void Show()
        {
            
        }

        public virtual void Hide()
        {
            
        }
    }
}
