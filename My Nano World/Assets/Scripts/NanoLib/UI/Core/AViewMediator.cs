﻿using strange.extensions.mediation.impl;

namespace NanoReality.StrangeIoC
{
    public abstract class AViewMediator : View
    {
        private bool _isRegistered;

        protected sealed override void Awake()
        {
            PreRegister();
            base.Awake();
            TryRegister();
        }

        protected sealed override void Start()
        {
            base.Start();
            TryRegister();
        }
        
        protected sealed override void OnDestroy()
        {
            OnRemove();
            base.OnDestroy();
        }

        /// <summary>
        /// Fires directly after creation and before injection
        /// </summary>
        protected abstract void PreRegister();

        /// <summary>
        /// Fires after all injections satisifed.
        /// Override and place your initialization code here.
        /// </summary>
        protected abstract void OnRegister();
        
        /// <summary>
        /// Fires on removal of Object.
        /// Override and place your cleanup code here.
        /// </summary>
        protected abstract void OnRemove();

        private void TryRegister()
        {
            if (registeredWithContext && !_isRegistered)
            {
                OnRegister();

                _isRegistered = true;
            }
        }
    }
}
