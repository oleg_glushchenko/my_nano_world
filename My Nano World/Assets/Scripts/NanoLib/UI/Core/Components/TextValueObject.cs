﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.NanoLib.UI.Core.Components
{
    public class TextValueObject : MonoBehaviour
    {

        [SerializeField]
        protected TextMeshProUGUI TextLabel;

        [SerializeField]
        protected TextMeshProUGUI ValueLabel;


        public virtual string Text
        {
            get
            {
                if (TextLabel != null)
                    return TextLabel.text;
                return "";
            }
            set
            {
                if (TextLabel != null)
                    TextLabel.text = value;
            }
        }

        public virtual string Value
        {
            get
            {
                if (ValueLabel != null)
                    return ValueLabel.text;
                return "";
            }
            set
            {
                if (ValueLabel != null)
                    ValueLabel.text = value;
            }
        }


    }

}

