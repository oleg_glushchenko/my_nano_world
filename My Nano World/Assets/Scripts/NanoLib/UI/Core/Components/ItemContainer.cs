﻿using System;
using System.Collections.Generic;
using Assets.NanoLib.Utilities.Pulls;
using UnityEngine;

namespace Assets.NanoLib.UI.Core.Components
{
    /// <summary>
    /// Represent base UI container for items
    /// </summary>
    public class ItemContainer : MonoBehaviour
    {
	    public Transform Container;
        
        public int Count => CurrentItems.Count;

	    protected virtual IObjectsPull ItemsPull { get; set; }
	    private bool _isInit;

        public List<IPullableObject> CurrentItems { get; } = new List<IPullableObject>();

        public bool IsInstantiateed { get; private set; }

        protected virtual void Awake() => Init();

        private void Init()
        {
            if (_isInit) 
                return;
            
            ItemsPull = new MObjectsPull();
            _isInit = true;
        }

        public void InstaniteContainer(IPullableObject prefab, int count)
        {
            if (!(prefab is Component))
            {
                throw new Exception("Prefab item must be Component!");
            }

            ClearContainer();
            Init();
            ItemsPull.InstanitePull(prefab);
            IsInstantiateed = true;

            for (int i = 0; i < count; i++)
            {
	            AddItem();
            }

	        foreach (var item in CurrentItems)
	        {
		        item.FreeObject();
	        }
        }

        public IPullableObject AddItem()
        {
            var item = ItemsPull.GetInstance();
            CurrentItems.Add(item);

            var itemGameObject = item as UnityEngine.Object as Component;

	        itemGameObject.transform.SetParent(Container);
	        itemGameObject.transform.SetAsLastSibling();
	        itemGameObject.transform.localScale = Vector3.one;

            return item;
        }

        public void RemoveItem(IPullableObject item)
        {
            if (CurrentItems.Contains(item))
            {
                item.FreeObject();
                CurrentItems.Remove(item);
            }
            else
            {
				Debug.LogWarning("Doesn't contains item+"+item);
            }
        }

        public void RemoveItem(int itemIndex)
        {
            if (itemIndex >= 0 && itemIndex < CurrentItems.Count)
            {
                var item = CurrentItems[itemIndex];
                item.FreeObject();
                CurrentItems.Remove(item);
            }
            else
            {
                Debug.LogError("Item indes is out of range");
            }
        }

        public void ClearContainer()
        {
            ItemsPull?.ClearPull();

            ClearCurrentItems();
        }

        public void ClearCurrentItems()
        {
            if (CurrentItems == null) return;

            foreach (var pullableObject in CurrentItems.ToArray())
                pullableObject.FreeObject();

            CurrentItems.Clear();
        }
    }
}
