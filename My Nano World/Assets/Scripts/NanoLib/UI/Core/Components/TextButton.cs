using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Core.UI
{
    public class TextButton : Button
    {
        [SerializeField] private TextMeshProUGUI _buttonText;

        public string Text
        {
            get => _buttonText.text;
            set => _buttonText.text = value;
        }
        
        public event Action Clicked;

        protected override void OnEnable()
        {
            base.OnEnable();
            onClick.AddListener(OnClicked);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            onClick.RemoveListener(OnClicked);
        }

        private void OnClicked()
        {
            Clicked?.Invoke();
        }
    }
}