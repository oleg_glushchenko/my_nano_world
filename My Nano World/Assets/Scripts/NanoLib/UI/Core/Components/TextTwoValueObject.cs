﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.NanoLib.UI.Core.Components
{
    public class TextTwoValueObject : TextValueObject
    {
        [SerializeField] protected Text SecondValueLabel;

        public void SetFirstValueColor(Color color)
        {
            ValueLabel.color = color;
        }

        public void SetSecondValueColor(Color color)
        {
            SecondValueLabel.color = color;
        }

        public void SetColor(Color color)
        {
            SecondValueLabel.color = color;
            ValueLabel.color = color;
            TextLabel.color = color;
        }

        public virtual string SecondValue
        {
            get
            {
                if (SecondValueLabel != null)
                    return SecondValueLabel.text;
                return "";
            }
            set
            {
                if (SecondValueLabel != null)
                    SecondValueLabel.text = value;
            }
        }
    }
}
