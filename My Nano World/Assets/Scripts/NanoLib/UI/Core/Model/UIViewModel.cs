namespace NanoLib.UI.Model
{
    public sealed class UIViewModel
    {
        public bool IsActive;

        public bool IsInteractable;

        public UIViewModel()
        {
            IsActive = true;
            IsInteractable = true;
        }
    }
}