using System;
using NanoLib.Core.Services.Sound;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using NanoLib.UI.Core;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Assets.NanoLib.UI.Core
{
    public class UIMediator<T> : Mediator, IUIMediator where T : UIView
    {
        [Inject] public IUIManager jUIManager { get; set; }
        [Inject] public ShowWindowSignal ShowWindowSignal { get; set; }
        [Inject] public HideWindowSignal HideWindowSignal { get; set; }
        [Inject] public WindowOpenedSignal WindowOpenedSignal { get; set; }
        [Inject] public WindowClosedSignal WindowClosedSignal { get; set; }
        [Inject] public SetWindowLastSiblingSignal jSetWindowLastSiblingSignal { get; set; }
        [Inject] public IPopupManager jPopupManager { get; set; }
        [Inject] public ISoundManager jSoundManager { get; set; }

        protected bool ViewVisible => _view != null;

        private T _view;

        protected T View
        {
            get
            {
                if (_view == null)
                {
                    Debug.LogError("View for " + this + " didn't exist, please call *Show(params)* method");
                    return null;
                }
                return _view;
            }
        }

        public override void OnRegister()
        {
            base.OnRegister();
            
            jSetWindowLastSiblingSignal.AddListener(OnSetWindowLastSibling);
        }

        public override void OnRemove()
        {
            base.OnRemove();
            
            jSetWindowLastSiblingSignal.RemoveListener(OnSetWindowLastSibling);
        }
        
        public void Hide(Type type)
        {
            Hide();
        }
        
        public void HandleShow(Type type, object param)
        {
            Show(param);
        }
        
        public Type GetViewType()
        {
            return typeof(T);
        }

        protected virtual void OnShown() { }
        
        protected virtual void OnHidden() { }

        protected virtual void Show(object param)
        {
            _view = jUIManager.GetView<T>();
            _view.Initialize(param);

            if (_view is UIPanelView view)
            {
                view.Show();
            }

            _view.NeedToHide = () =>
            {
                jUIManager.CloseView<T>();
                OnHidden();
                WindowClosedSignal.Dispatch(typeof(T));
            };

            OnShown();
            WindowOpenedSignal.Dispatch(typeof(T));
        }

        public void Hide()
        {
            if (!ViewVisible)
            {
                return;
            }

            if (_view is UIPanelView view)
            {
                view.Hide();
            }
            else
            {
                OnHidden();
                WindowClosedSignal.Dispatch(typeof(T));
                jUIManager.CloseView<T>();
            }
        }
        
        private void OnSetWindowLastSibling(Type windowType)
        {
            if (windowType != typeof(T)) return;
            
            jUIManager.SetViewLastSibling(windowType);
        }

        protected void ShowPopup(BasePopupSettings popupSettings, Action<PopupResult> callback = null)
        {
            jPopupManager.Show(popupSettings, callback);
        }
        
        protected void ShowPopup(BasePopupSettings popupSettings)
        {
            jPopupManager.Show(popupSettings, (Action<PopupResult>) null);
        }
        
        protected void ShowPopup(BasePopupSettings popupSettings, Action callback = null)
        {
            jPopupManager.Show(popupSettings, callback);
        }
    }
}