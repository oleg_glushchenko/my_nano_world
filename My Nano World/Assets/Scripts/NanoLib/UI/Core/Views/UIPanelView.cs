﻿using System;
using System.Collections.Generic;
using Assets.NanoLib.UI.Core.Signals;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using NanoLib.Core.Logging;
using NanoReality.Engine.UI.UIAnimations;
using strange.extensions.signal.impl;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.NanoLib.UI.Core.Views
{
    /// <summary>
    /// Base class for any UI Panel in game.
    /// </summary>
    public class UIPanelView : UIView
    {
        [SerializeField] protected List<Button> _buttonsToEnableDisable;
        [SerializeField] protected List<Toggle> _togglesToEnableDisable;
        
        // TODO: need SerializeField.
        public bool IsAutoClosing;
        
        public Signal<UIPanelView> OnShown { get; } = new Signal<UIPanelView>();
        
        public Signal<UIPanelView> OnHide { get; } = new Signal<UIPanelView>();
        
        public bool Visible { get; set; }

        [Inject] public SignalOnShownPanel jSignalOnShownPanel { get; set; }
        
        [Inject] public SignalOnHidePanel jSignalOnHidePanel { get; set; }
        [Inject] public IPopupManager jPopupManager { get; set; }

        protected ISpecialEffect SpecialShowEffect;
        protected ISpecialEffect SpecialHideEffect;
        
        protected override void Awake()
        {
            base.Awake();

            SpecialShowEffect = GetComponent<UIPanelShowEffect>();
            SpecialHideEffect = GetComponent<UIPanelHideEffect>();
        }
        
        public virtual void Show()
        {
            Logging.Log(LoggingChannel.Ui, $"Panel <b>{name}</b> Show", this);
            
            if (!registeredWithContext)
                base.Start();

            if (Visible)
            {
                Logging.LogError("PanelAlready visible.");
                return;
            }
            
            SpecialShowEffect?.EffectStopped.AddListener(ShowEffectEnded);
            SpecialShowEffect?.EffectCanceled.AddListener(ShowEffectEnded);
            
            SpecialHideEffect?.EffectStopped.AddListener(HideEffectEnded);
            SpecialHideEffect?.EffectCanceled.AddListener(HideEffectEnded);
                
            if (SpecialShowEffect == null)
            {
                ShowEffectEnded(null);
            }
            else
            {
                SpecialShowEffect.Play();
            }
        }
        
        public virtual void Hide()
        {
            Logging.Log(LoggingChannel.Ui, $"Panel <b>{name}</b> Hide", this);

            if (SpecialHideEffect == null)
            {
                HideEffectEnded(null);
            }
            else
            {
                SpecialHideEffect.Play();
            }
        }

        protected virtual void ShowEffectEnded(ISpecialEffect specialEffect)
        {
            Visible = true;
            OnShown.Dispatch(this);
            jSignalOnShownPanel?.Dispatch(this);
        }
        
        protected virtual void HideEffectEnded(ISpecialEffect specialEffect)
        {
            if (Visible)
            {
                Visible = false;

                SpecialShowEffect?.EffectStopped.RemoveListener(ShowEffectEnded);
                SpecialShowEffect?.EffectCanceled.RemoveListener(ShowEffectEnded);
            
                SpecialHideEffect?.EffectStopped.RemoveListener(HideEffectEnded);
                SpecialHideEffect?.EffectCanceled.RemoveListener(HideEffectEnded);

                NeedToHide?.Invoke();
                jSignalOnHidePanel?.Dispatch(this);
                OnHide.Dispatch(this);
            }
        }
        
        public virtual void SetButtonsAndTogglesEnabled(bool enabled)
        {
            _buttonsToEnableDisable.ForEach(b => b.enabled = enabled);
            _togglesToEnableDisable.ForEach(t => t.enabled = enabled);
        }

        public virtual void SetButtonsClickable(bool isClickable)
        {
            _buttonsToEnableDisable.ForEach(b => b.interactable = isClickable);
        }
        
        /// <summary>
        /// Use UIManager popup settings
        /// </summary>
        /// <param name="popupSettings"></param>
        /// <param name="callback"></param>
        protected void ShowPopup(BasePopupSettings popupSettings, Action<PopupResult> callback = null)
        {
            jPopupManager.Show(popupSettings, callback);
        }
    }

    public abstract class UiSubPanelView : UIView
    {
        
    }

    public abstract class UiSubPanelController : MonoBehaviour
    {
        public bool IsActive { get; private set; }
        
        public virtual void Show()
        {
            IsActive = true;
            gameObject.SetActive(true);
        }

        public virtual void Hide()
        {
            IsActive = false;
            gameObject.SetActive(false);
        }
    }
}