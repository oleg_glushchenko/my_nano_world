﻿using System;

namespace NanoLib.UI.Core
{
    public interface IUIMediator
    {
        Type GetViewType();

        void HandleShow(Type type, object param);

        void Hide(Type type);

        void Hide();
    }
}
