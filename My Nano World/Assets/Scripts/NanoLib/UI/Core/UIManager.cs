﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.UI.Core;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.UI.Views.Hud;
using Engine.UI;
using NanoReality.Core.Resources;
using NanoReality.Engine.UI.Views.LevelUpPanel;
using NanoReality.Game.UI.FlyingBalanceItemPanel;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.Engine.UI.Views.TheEvent;
using NanoReality.StrangeIoC;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace NanoLib.UI.Core
{
    public sealed class UIManager : AViewMediator, IUIManager
    {
        [SerializeField] private Transform _activeUIParent;
        [SerializeField] private Transform _cachedUIParent;
        [SerializeField] private Transform _hudPanelParent;
        [SerializeField] private Transform _balancePanelParent;
        [SerializeField] private Transform _theEventPanelParent;
        [SerializeField] private RectTransform _overlayCanvasTransform;
        [SerializeField] private FlyingBalanceItemPanel _flyingPanel;

        private readonly List<KeyValuePair<Type, View>> _activeUI = new List<KeyValuePair<Type, View>>();
        private readonly Dictionary<Type, View> _cachedUi = new Dictionary<Type, View>();
        private UIPrefabLinks _prefabLinks;
        private UIView _lastActive;

        public List<KeyValuePair<Type, View>> ActiveUi => _activeUI;
        public RectTransform OverlayCanvasTransform => _overlayCanvasTransform;
        public FlyingBalanceItemPanel FlyingPanel => _flyingPanel;
        private UIPrefabLinks PrefabLinks => _prefabLinks != null ? _prefabLinks : _prefabLinks = ScriptableObjectsManager.Get<UIPrefabLinks>();

        [Inject] public UIFilter jUIFilter { get; set; }

        public bool IsViewVisible(Type type) => _activeUI.Any(t => t.Key == type);

        public void CloseView<T>() where T : View
        {
            MoveToCache<T>();
        }

        public TViewType GetActiveView<TViewType>() where TViewType : View
        {
            Type viewType = typeof(TViewType);
            return IsViewVisible(viewType) ? (TViewType) ActiveUi.FirstOrDefault(t => t.Key == viewType).Value : null;
        }

        public TViewType GetView<TViewType>() where TViewType : View
        {
            var type = typeof(TViewType);
            View result = GetActiveView<TViewType>();

            if (result == null)
            {
                if (_cachedUi.ContainsKey(type))
                {
                    result = GetFromCache<TViewType>();
                }
                else
                {
                    Component prefab = PrefabLinks.GetGameObject(type).GetComponent(type);
                    Transform parent = SelectParent((View) prefab);

                    var viewInstance = (View) Instantiate(prefab, parent, false);

                    UseOpenRuleFor((UIView) viewInstance);
                    _activeUI.Add(new KeyValuePair<Type, View>(type, viewInstance));

                    result = viewInstance;
                }
            }
            
            SetLastActive((UIView) result);
            return result as TViewType;
        }

        public void LoadView(Type type)
        {
            Component prefab = PrefabLinks.GetGameObject(type).GetComponent(type);
            var viewInstance = (View) Instantiate(prefab, _cachedUIParent, false);
            _cachedUi.Add(type, viewInstance);
        }

        public void SetViewLastSibling<T>()
        {
            SetViewLastSibling(typeof(T));
        }

        public void SetViewLastSibling(Type viewType)
        {
            if(!IsViewVisible(viewType))
            {
                Debug.LogWarning("You try to SetLastSibling for view that not contains in active views list.");
                return;
            }
            
            _activeUI.FirstOrDefault(t => t.Key == viewType).Value.transform.SetAsLastSibling();
        }

        private T GetFromCache<T>() where T : View
        {
            var type = typeof(T);
            var view = _cachedUi[type];
            UseOpenRuleFor((UIView) view);
            _cachedUi.Remove(type);
            _activeUI.Add(new KeyValuePair<Type, View>(type, view));
            view.transform.SetParent(SelectParent(view));
            view.transform.localScale = Vector3.one;
            return view as T;
        }

        private void MoveToCache<T>() where T : View
        {
            var type = typeof(T);
            if (IsViewVisible(type))
            {
                var pair = _activeUI.FirstOrDefault(t => t.Key == type);
                var view = pair.Value;
                _activeUI.Remove(pair);
                _cachedUi.Add(type, view);
                view.transform.SetParent(_cachedUIParent);
                view.transform.localScale = Vector3.one;
            }
        }

        private void SetLastActive(UIView lastPanel)
        {
            if (jUIFilter != null && !jUIFilter.DefaultOnlyVisible.ContainsValue(lastPanel))
            {
                _lastActive = lastPanel;
            }
        }

        public bool IsNotDefaultViewVisible() 
        {
            return Except(_activeUI, jUIFilter?.DefaultOnlyVisible) != null;
        }
        
        public void HideAll(bool isHardClose = false)
        {
            Dictionary<Type, View> filtered = Except(_activeUI, jUIFilter?.DefaultOnlyVisible);
            if (filtered == null) return;

            KeyValuePair<Type, View>[] filteredArray = filtered.ToArray();
            for (int i = filteredArray.Length - 1; i >= 0; i--)
            {
                if (isHardClose)
                {
                    HardCloseView(filteredArray[i].Value);
                }
                else
                {
                    SoftCloseView(filteredArray[i].Value);
                }
            }
        }

        private Dictionary<Type, View> Except(List<KeyValuePair<Type, View>> source, Dictionary<Type, View> filter)
        {
            var filtered = new Dictionary<Type, View>();
            foreach (var pair in source)
            {
                if (!filter.ContainsKey(pair.Key))
                {
                    filtered.Add(pair.Key, pair.Value);
                }
            }

            return filtered.Count == 0 ? null : filtered;
        }

        private void UseOpenRuleFor(UIView newPanel)
        {
            if ((newPanel.OpenMode & PanelOpenMode.Solo) != 0)
            {
                HideAll();
            }

            if ((newPanel.OpenMode & PanelOpenMode.Displace) != 0)
            {
                SoftCloseView(_lastActive);
            }

            if ((newPanel.OpenMode & PanelOpenMode.Reopen) != 0)
            {
                SoftCloseView(newPanel);
            }
        }

        private void HardCloseView(View view)
        {
            UIPanelView uiPanelView = view as UIPanelView;
            if (uiPanelView == null || view as LevelUpPanelView != null) return;
            
            uiPanelView.Visible = true;
            uiPanelView.Hide();
        }

        private void SoftCloseView(View view)
        {
            //direct call to avoid (opened panels count * total panels count) calls number
            (view as UIPanelView)?.Hide();
        }

        public void HideLastActivePanel()
        {
            for (int i = _activeUI.Count - 1; i >= 0; i--)
            {
                UIPanelView panelView = _activeUI[i].Value as UIPanelView;

                string checkTypeView = panelView.GetType().ToString();

                if (panelView != null &&
                    panelView.IsAutoClosing &&
                    !jUIFilter.DefaultOnlyVisible.ContainsValue(panelView) &&
                    !checkTypeView.Equals("FxPopUpPanelView"))
                {
                    panelView.Hide();
                    return;
                }
            }
        }
        
        private Transform SelectParent(View view)
        {
            switch (view)
            {
                case BalancePanelView _:
                    return _balancePanelParent;

                case HudView _:
                    return _hudPanelParent;

                case TheEventPanelView _:
                    return _theEventPanelParent;

                default:
                    return _activeUIParent;
            }
        }

        protected override void PreRegister() { }
        protected override void OnRegister() { }

        protected override void OnRemove()
        {
            _prefabLinks.Unload();
            _prefabLinks = null;
            
            _activeUI.Clear();
            _cachedUi.Clear();
            _lastActive = null;
        }
    }
}