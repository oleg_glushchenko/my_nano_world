using System;
using Assets.NanoLib.UI.Core.Views;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Assets.NanoLib.UI.Core
{
    public class UIView : View
    {
        [HideInInspector]
        public Action NeedToHide;

        [SerializeField] protected PanelOpenMode _openMode = PanelOpenMode.Solo;

        public PanelOpenMode OpenMode => _openMode;

        private RectTransform _rectTransform;

        public RectTransform RectTransform
        {
            get
            {
                if (_rectTransform == null)
                {
                    _rectTransform = (RectTransform)transform;
                }

                return _rectTransform;
            }
        }

        private bool _initialized;
        
        public virtual void Initialize(object parameters)
        {
            if (!registeredWithContext)
                base.Start();
            if(_initialized) return;
            _initialized = true;
        }
    }
}