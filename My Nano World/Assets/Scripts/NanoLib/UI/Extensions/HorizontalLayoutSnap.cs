﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.nano_lib.UI.Extensions
{
    [ExecuteInEditMode]
    public class HorizontalLayoutSnap : MonoBehaviour
    {
        [SerializeField]
        private Vector2 _spacing;

        [SerializeField] 
        private Vector2 _padding;

        [SerializeField] 
        private Vector2 _cellSize;

        [SerializeField] 
        private int _defaultItemsPerPage = 6;

        private List<RectTransform> _items = new List<RectTransform>();

        private int _activeItemsCount;

        private Vector2 _containerPadding;

        private RectTransform _containerTransform;

        private Rect _viewportRect;
        
        private int _horizontalItemsCount;

        private int _verticalItemsCount;

        public Vector2 CellSize
        {
            get { return _cellSize; }
            set
            {
                _cellSize = value;
                CalcLayout();
            }
        }

        public int ItemsPerPage
        {
            get
            {
                int itemsPerPage = _horizontalItemsCount*_verticalItemsCount;
                return itemsPerPage == 0 ? _defaultItemsPerPage : itemsPerPage; 
            }
        }

        void Awake()
        {
            _activeItemsCount = 0;
            _containerTransform = transform.parent.GetComponent<RectTransform>();
        }

        void OnEnable()
        {
            CalcLayout();
        }

        private void CalcLayout()
        {
            Vector2 totalItemDimensions = new Vector2(_cellSize.x + _spacing.x, _cellSize.y + _spacing.y);

            _horizontalItemsCount = Mathf.FloorToInt((_viewportRect.width + _spacing.x) / (totalItemDimensions.x));
            _verticalItemsCount = Mathf.FloorToInt((_viewportRect.height + _spacing.y) / (totalItemDimensions.y));

            _containerPadding = new Vector2((_viewportRect.width - totalItemDimensions.x * _horizontalItemsCount) / 2f + _padding.x,
                (_viewportRect.height - totalItemDimensions.y * _verticalItemsCount) / 2f + _padding.y);
        }

        void LateUpdate()
        {
            Rect viewportRect = _containerTransform.rect;
            _viewportRect = viewportRect;
            CalcLayout();

            var activeChildsCount = 0;

            for (int i = 0; i < transform.childCount; i++)
            {
                if (transform.GetChild(i).gameObject.activeInHierarchy)
                    activeChildsCount++;
            }

            if (activeChildsCount != _activeItemsCount)
            {
                _items.Clear();
                _activeItemsCount = 0;

                for (int i = 0; i < transform.childCount; i++)
                {
                    var itemRect = transform.GetChild(i).GetComponent<RectTransform>();
                    if (itemRect.gameObject.activeInHierarchy)
                    {
                        _items.Insert(_activeItemsCount, itemRect);
                        _activeItemsCount ++;
                    }
                    else
                        _items.Add(itemRect);

                }
            }

            PlaceItems();
        }

        public void PlaceItems()
        {
            if(_items.Count == 0)
                return;
            
            foreach (RectTransform item in _items)
                item.pivot = Vector2.up;

            Vector2 currentPosition = new Vector2(_containerPadding.x, -_containerPadding.y);
            float nextPagePadding = _containerPadding.x;

            int pagesCount = Mathf.CeilToInt(_items.Count/(float)(_horizontalItemsCount*_verticalItemsCount));
            
            for (int page = 0; page < pagesCount; page++)
            {
                for (int i = 0; i < _verticalItemsCount; i++)
                {

                    currentPosition.x = nextPagePadding;

                    for (int j = 0; j < _horizontalItemsCount; j++)
                    {
                        int oneDimIndex = (page * (_verticalItemsCount * _horizontalItemsCount) + i * _horizontalItemsCount + j);
                        if (_items.Count > oneDimIndex)
                        {
                            _items[oneDimIndex].localPosition = currentPosition;
                            _items[oneDimIndex].sizeDelta = _cellSize;
                        }
                        else
                            return;

                        currentPosition.x += (_cellSize.x + _spacing.x);
                    }

                    currentPosition.y -= _cellSize.y + _spacing.y;
                }

                currentPosition.y = -_containerPadding.y;
                currentPosition.x += _containerPadding.x * 2f - _spacing.x;
                nextPagePadding = currentPosition.x;
            }
        }
    }
}
