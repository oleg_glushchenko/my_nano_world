﻿using Assets.NanoLib.UtilitesEngine;
using strange.extensions.signal.impl;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.nano_lib.UI.Extensions
{
    public class SignalOnDiscreteInputCahnged : Signal<DiscreteCountInput> { }

    /// <summary>
    /// Represent input for integer values that can change by + and - buttons
    /// </summary>
    public class DiscreteCountInput : MonoBehaviour
    {
        #region Inspector

        [SerializeField]
        private int _value;

        [SerializeField]
        private Button _incerementButton;

        [SerializeField]
        private Button _decrementButton;

        [SerializeField]
        private TextMeshProUGUI _textValue;

        public GameObject IncrementButton
        {
            get
            {
                if (_incerementButton != null)
                    return _incerementButton.gameObject;

                return null;
            }
        }

        /// <summary>
        /// How fast value changes when inc/dec button hovered
        /// </summary>
        public float _onHoverValueChangeDeltaTime = 133f;

        /// <summary>
        /// Increment delta value at the expiration of HoverValueChangeDeltaTime
        /// </summary>
        public int _valueChangeDeltaPerDeltaTime = 5;

        /// <summary>
        /// Divide HoverValueChangeDeltaTime at the expiration of it
        /// </summary>
        public float _deltaTimeMultiplier = 0.8f; 

        #endregion

        private bool _interactable;

        public bool Interactable
        {
            get { return _interactable; }
            set
            {
                _incerementButton.interactable = value;
                _decrementButton.interactable = value;
                _interactable = value;
            }
        }
            
        /// <summary>
        /// Current value of input
        /// </summary>
        public int Value
        {
            get { return _value; }
            set
            {
                _value = Mathf.Clamp(value, _minValue, _maxValue);
                _textValue.text = _value.ToString();
                UpdateButtonsStatus();
                _signalOnDiscreteInputCahnged.Dispatch(this);
            }
        }

        public int MinValue
        {
            get
            {
                return _minValue;
            }
            set
            {
                if (value > _maxValue)
                {
                    //Debug.LogError("Wrong min value it must be less than max value");
                    return;
                }
                _minValue = value;

                if (Value < _minValue)
                    Value = _minValue;
                UpdateButtonsStatus();
            }
        }

        public int MaxValue
        {
            get
            {
                return _maxValue;
            }
            set
            {
                if (value <= _minValue)
                {
                    //Debug.LogError("Wrong max value it must be greater than min value");
                    return;
                }
                _maxValue = value;

                if (Value > _maxValue)
                    Value = _maxValue;
                UpdateButtonsStatus();
            }
        }

        public bool IsMax
        {
            get { return _value == _maxValue; }
        }

        public bool IsMin
        {
            get { return _value == _minValue; }
        }

        public SignalOnDiscreteInputCahnged SignalOnDiscreteInputCahnged
        {
            get { return _signalOnDiscreteInputCahnged; }
            protected set { _signalOnDiscreteInputCahnged = value; }
        }
        
        public Signal<int> SignalOnDiscreteIncremented
        {
            get { return _signalOnDiscreteIncremented; }
            protected set { _signalOnDiscreteIncremented = value; }
        }

        private int _minValue;
        private int _maxValue;
        private int _delta;
        private bool _isHover;
        private bool _deltaTimeReached;
        private float _timeSinceHover;

        private int _tmpDelta;
        private float _tmpOnHoverValueChangeDeltaTime;

        private SignalOnDiscreteInputCahnged _signalOnDiscreteInputCahnged = new SignalOnDiscreteInputCahnged();
        private Signal<int> _signalOnDiscreteIncremented = new Signal<int>();

        void Awake()
        {
            _textValue.text = _value.ToString();

            HoverEventTrigger incrementHoverTrigger = _incerementButton.gameObject.AddComponent<HoverEventTrigger>();

            incrementHoverTrigger.SignalOnButtonHover.AddListener(() => Value = _value + _delta);
            incrementHoverTrigger.SignalOnButtonHoverStart.AddListener(OnSomeButtonHoverStart);
            incrementHoverTrigger.SignalOnButtonHoverEnd.AddListener(OnSomeButtonHoverEnd);

            HoverEventTrigger decrementEventTrigger = _decrementButton.gameObject.AddComponent<HoverEventTrigger>();

            decrementEventTrigger.SignalOnButtonHover.AddListener(() => Value = _value - _delta);
            decrementEventTrigger.SignalOnButtonHoverStart.AddListener(OnSomeButtonHoverStart);
            decrementEventTrigger.SignalOnButtonHoverEnd.AddListener(OnSomeButtonHoverEnd);
        }

        /// <summary>
        /// Initialize input
        /// </summary>
        /// <param name="minValue">minimum value of input</param>
        /// <param name="maxValue">maximim value of input</param>
        /// <param name="delta">how many add or substruct on +,- button click</param>
        /// <param name="value">default value of input</param>
        public void Init(int minValue, int maxValue, int delta, int value)
        {
            if (minValue > maxValue)
            {
                //Debug.LogError("Wrong max or min value");
                return;
            }
            
            
            _minValue = minValue;
            _maxValue = maxValue;
            _delta = delta;
            Value = Mathf.Clamp(value, _minValue, _maxValue);
            UpdateButtonsStatus();
        }

        void OnEnable()
        {
            _incerementButton.onClick.AddListener(IncrementCount);
            _decrementButton.onClick.AddListener(DecrementCount);
        }

        void OnDisable()
        {
            _incerementButton.onClick.RemoveListener(IncrementCount);
            _decrementButton.onClick.RemoveListener(DecrementCount); 
        }

        void Update()
        {
            if (_isHover)
            {
                _timeSinceHover += Time.deltaTime;
                if (_timeSinceHover >= _onHoverValueChangeDeltaTime)
                {
                    _timeSinceHover = 0;
                    _onHoverValueChangeDeltaTime *= _deltaTimeMultiplier;
                    _delta += _valueChangeDeltaPerDeltaTime;
                    _deltaTimeReached = true;
                    _signalOnDiscreteIncremented.Dispatch(_delta);
                }
            }
        }

        private void OnIncrementHover()
        {
            if (_deltaTimeReached)
            {
                _deltaTimeReached = false;
                IncrementCount();
            }
        }

        private void OnDecrementHover()
        {
            if (_deltaTimeReached)
            {
                _deltaTimeReached = false;
                DecrementCount();
            }
        }

        private void OnSomeButtonHoverStart()
        {
            if (!_isHover)
            {
                _isHover = true;
                _tmpOnHoverValueChangeDeltaTime = _onHoverValueChangeDeltaTime;
                _tmpDelta = _delta;
            }
        }

        private void OnSomeButtonHoverEnd()
        {
            _isHover = false;
            _timeSinceHover = 0;
            _deltaTimeReached = false;
            _onHoverValueChangeDeltaTime = _tmpOnHoverValueChangeDeltaTime;
            _delta = _tmpDelta;
        }

        private void IncrementCount()
        {
            //prevent value change from Button component
            if (!_isHover)
                Value = (_value + _delta);

            _signalOnDiscreteIncremented.Dispatch(_delta);
        }

        private void DecrementCount()
        {
            //prevent value change from Button component
            if (!_isHover)
                Value = (_value - _delta);

            _signalOnDiscreteIncremented.Dispatch(_delta);
        }

        private void UpdateButtonsStatus()
        {
            _incerementButton.interactable = _value != _maxValue;
            _decrementButton.interactable = _value != _minValue;
        }
    }
}
