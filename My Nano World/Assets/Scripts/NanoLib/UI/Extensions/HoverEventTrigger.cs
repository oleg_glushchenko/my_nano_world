﻿using Assets.NanoLib.UtilitesEngine;
using strange.extensions.signal.impl;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.nano_lib.UI.Extensions
{
    public class SignalOnButtonHover : Signal { }

    public class SignalOnButtonHoverStart : Signal { }

    public class SignalOnButtonHoverEnd : Signal { }

    [RequireComponent(typeof(Selectable))]
    public class HoverEventTrigger : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler,
        IPointerEnterHandler
    {
        public float HoverStartDelay = 0.67f;

        public SignalOnButtonHover SignalOnButtonHover = new SignalOnButtonHover();

        public SignalOnButtonHoverStart SignalOnButtonHoverStart = new SignalOnButtonHoverStart();

        public SignalOnButtonHoverEnd SignalOnButtonHoverEnd = new SignalOnButtonHoverEnd();

        private bool _isHover;
        private bool _hoverButExit;

        private float _timeSinceHover;

        private Selectable _selectable;

        void Awake()
        {
            _selectable = gameObject.GetComponent<Selectable>();
        }

        void Update()
        {
            _timeSinceHover += Time.deltaTime;
            if (_isHover && _selectable.interactable && _timeSinceHover > HoverStartDelay)
                SignalOnButtonHover.Dispatch();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            _isHover = true;
            _timeSinceHover = 0;
            SignalOnButtonHoverStart.Dispatch();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            _isHover = false;
            _hoverButExit = false;
            _timeSinceHover = 0;
            SignalOnButtonHoverEnd.Dispatch();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (_isHover)
            {
                _hoverButExit = true;
                _isHover = false;
            }
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (_hoverButExit)
            {
                _hoverButExit = false;
                _isHover = true;
            }
        }
    }
}
