﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Legacy/Transparent/Cutout Offset"
{
	Properties 
	{
		_Color ("Main Color", Color) = (1,1,1,1)
		_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
		_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
		_MainTexBias ("Mip Bias (-1 to 1)", float) = -1.65

		_FogTopColor ("Fog Top", Color) = (1,1,1,1)
		_FogBottomColor("Fog Bottom", Color) = (1,1,1,1)
		_FogColorIntensity("Fog Intensity", Float) = 1
		_FogRampTexture("Fog Ramp", 2D) = "white" {}

		_CameraMaxZoom("Camera Max Zoom", float) = 24
		_CameraMinZoom("Camera Min Zoom", float) = 6

	}
	SubShader 
	{
		Tags 
		{
			"Queue"="AlphaTest" 
			"IgnoreProjector"="True" 
			"RenderType"="TransparentCutout"

		}
		LOD 200
		Offset -1, -1
		Pass
		{
			ZWrite On
			ZTest Less

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			 #include "UnityCG.cginc"
	        
			uniform sampler2D _MainTex;
			uniform float4 _MainTex_ST;
			uniform fixed4 _Color;	
			uniform fixed _Cutoff;
			uniform float _MainTexBias;

			uniform fixed4 _FogTopColor;
			uniform fixed4 _FogBottomColor;
			uniform fixed _FogColorIntensity;
			uniform sampler2D _FogRampTexture;

			uniform fixed _CameraMaxZoom;
			uniform fixed _CameraMinZoom;

			struct v2f 
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			v2f vert(appdata_base i)
			{
				v2f o;
				o.uv = TRANSFORM_TEX (i.texcoord, _MainTex);
				o.pos = UnityObjectToClipPos(i.vertex);
				return o;
			}

			half4 frag(v2f i) : COLOR 
			{
				fixed4 c = tex2Dbias(_MainTex, half4(i.uv.x, i.uv.y,0.0, _MainTexBias)) * _Color; 
				//fixed4 ramp = tex2D(_FogRampTexture, half2(0., i.uv.y));
				//float zoom = unity_OrthoParams.x/ (_ScreenParams.x/_ScreenParams.y);
				//c.rgb  = lerp(c.rgb, lerp(_FogTopColor,_FogBottomColor,ramp.r), _FogColorIntensity*saturate((zoom-_CameraMinZoom)/(_CameraMaxZoom-_CameraMinZoom)));
				clip(c.a - _Cutoff);
				return c;
			}
			ENDCG
		}
	} 
}
