﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Legacy/Transparent/Building SkyReflect"
{
	Properties
	{
		_Color("Main Color", Color) = (1,1,1,1)
		_MainTex("Base (RGB) Trans (A)", 2D) = "white" {}
	_MainTexBias("Mip Bias (-1 to 1)", float) = -1.65

		_ReflectionTex("Reflection Texture RGBA", 2D) = "white" {}

	_Reflcolor("Color which reflect", Color) = (1,1,1,1)
		_ReflColTreshold("Reflect color width threshold", Float) = 0.5

		_CloudsSpeed("Clouds Speed", Vector) = (0.016, 0.006, 0, 0)
		//_ReflectionsPower("Reflections Power", Float) = 0.09
		_ReflDist("Reflections UV Distortion", Float) = 1.6
		_CamDispPow("Camera Displace Power", Float) = 0.01
	}
		SubShader
	{


		Tags{ "Queue" = "Transparent" }
		Cull back
		Blend SrcAlpha OneMinusSrcAlpha
		Lighting Off


		Pass
	{

		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#include "UnityCG.cginc"

		uniform sampler2D _MainTex;
	uniform float4 _MainTex_ST;
	uniform fixed4 _Color;
	uniform float _MainTexBias;
	sampler2D _ReflectionTex;
	fixed4 _ReflectionTex_ST;
	fixed4 _CloudsSpeed;
	//fixed _ReflectionsPower;
	fixed _ReflDist;
	fixed _CamDispPow;
	fixed4 _Reflcolor;
	fixed _ReflColTreshold;

	struct v2f
	{
		float4 pos : SV_POSITION;
		float2 uv : TEXCOORD0;

		fixed4 color : COLOR;
		//fixed4 scrPos : TEXCOORD0;
		fixed4 uvScreen : TEXCOORD1;
		//fixed2 uvMain : TEXCOORD2;
	};


	struct appdata_t {
		fixed4 vertex : POSITION;
		fixed4 color : COLOR;
		fixed2 texcoord : TEXCOORD0;
		fixed2 texcoord1 : TEXCOORD1;
	};

	v2f vert(appdata_t i)
	{
		v2f o;
		o.uv = TRANSFORM_TEX(i.texcoord, _MainTex);
		o.pos = UnityObjectToClipPos(i.vertex);


		fixed4 scrPos = ComputeScreenPos(o.pos);
		o.uvScreen.xy = (scrPos.xy);
		o.uvScreen.x = (o.uvScreen.x)*_ScreenParams.x / _ScreenParams.y;

		o.uvScreen.y += ((135.0 + (_WorldSpaceCameraPos.x + _WorldSpaceCameraPos.z) / 2.0) *_CamDispPow);
		o.uvScreen.x += (_WorldSpaceCameraPos.x - _WorldSpaceCameraPos.z) *_CamDispPow;

		o.uvScreen.zw = o.uvScreen.xy * _ReflectionTex_ST.xy + frac(_CloudsSpeed.xy *_Time.y);
		o.color = i.color;

		return o;
	}

	half4 frag(v2f i) : COLOR
	{
		fixed4 c = tex2Dbias(_MainTex, half4(i.uv.x, i.uv.y,0.0, _MainTexBias)) * _Color;

	fixed3 colDiff = saturate(_ReflColTreshold - abs(i.color.rgb - c.rgb));


	float reflectionMask = colDiff.r*colDiff.g*colDiff.b;

	float2 mainTexVUdisp = reflectionMask*_ReflDist;

	//float4 cloudTex = tex2D(_ReflectionTex, i.uvScreen.zw + mainTexVUdisp);
	float4 cloudTex = tex2Dbias(_ReflectionTex, half4(i.uvScreen.zw + mainTexVUdisp, 0.0, 6.0 - i.color.a*12.0));
	
	fixed4 refl;
	refl = cloudTex* reflectionMask * 10.0*i.color.a;
	refl.a = 0;



	return c + refl;
	}
		ENDCG
	}
	}
}
