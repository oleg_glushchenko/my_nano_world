// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'


Shader "CloudsCovering" {

Properties {
	_CloudTex ("Cloud texture RGBA", 2D) = "white" {}	
	_Mask ("Mask Texture (A)", 2D) = "white" {}
	_CloudsColor ("Clouds Color", Color) = (1,1,1,1)
	_CloudAlpha("Clouds Alpha", float) = 0.5
	_Multiplier("Color multiplier", float) = 1
	_ShadowColor ("Shadow Color", Color) = (1,1,1,1)	
	_ShadowMultiplier ("Shadow multiplier", float) = 1
	
	
	_CloudOffsetXY("Cloud Offset XY zoom zw", Vector) = (1.0,1.0,1.0,1.0)		
				
	_TimeSlide("Time Slide",Vector) = (0,0,0,0)	
	_MaskSlide("Mask Slide",Vector) = (0,0,0,0)
		
}

	
SubShader {
	
	
	Tags { "Queue"="Transparent+2" "IgnoreProjector"="True" "RenderType"="Transparent"}
	
	
	//Blend One One   // additive 
	//Blend OneMinusDstColor One   // Soft additive 
//	Blend One OneMinusSrcColor

	//Blend DstColor Zero                 // Multiplicative
	//Blend DstColor SrcColor             // 2x Multiplicative

	Cull Off Lighting Off ZWrite Off Fog { Color (0,0,0,0) }
	
	LOD 100
	
	CGINCLUDE	
	#include "UnityCG.cginc"
	
	sampler2D _CloudTex;
	sampler2D _Mask;	
	sampler2D _MaskDetail;

	float _Multiplier;
	float _ShadowMultiplier;

	float4 _CloudTex_ST;
	float4 _Mask_ST;
	fixed4 _CloudOffsetXY;
	fixed4 _ShadowColor;
	fixed _CloudAlpha;
	fixed4 _CloudsColor;
	float4 _TimeSlide;
	float4 _MaskSlide;
	
	struct v2f {
		float4	pos	: SV_POSITION;
		float4	uv		: TEXCOORD0;
		float4 uv2 : TEXCOORD1;
		float4 uv3 : TEXCOORD3;
		fixed4	color	: COLOR; 
		fixed4 colorShadows: TEXCOORD2;

	};

	
	v2f vert (appdata_full v)
	{
		v2f 		o;
				
		o.uv.xy		= v.texcoord.xy*_CloudTex_ST.xy + frac(_TimeSlide.xy*_Time.y);
		o.uv.zw		= v.texcoord.xy*_CloudTex_ST.zw + frac(_TimeSlide.zw*_Time.y);
		o.uv3.xy = v.texcoord.xy*_Mask_ST.xy + frac(_MaskSlide.xy*_Time.y);

		o.uv2.xy = o.uv.xy*_CloudOffsetXY.zw + _CloudOffsetXY.xy;
		o.uv2.zw = o.uv.zw*_CloudOffsetXY.zw + _CloudOffsetXY.xy;
		o.uv3.zw = o.uv3.xy*_CloudOffsetXY.zw + _CloudOffsetXY.xy;

		o.pos	= UnityObjectToClipPos(v.vertex); 

		o.color	=  _CloudsColor*_Multiplier;
		o.color.a = _CloudAlpha;
		o.colorShadows = _ShadowColor*_ShadowMultiplier;
		
		return o;
	}
	ENDCG

	Blend Zero SrcColor
	
	Blend SrcAlpha OneMinusSrcAlpha	
	
	Pass {
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#pragma fragmentoption ARB_precision_hint_fastest		
		fixed4 frag (v2f i) : COLOR
		{		
						
			fixed cloudMask = tex2D(_Mask, i.uv3.xy).a;		
			fixed shadowMask = tex2D(_Mask, i.uv3.zw).a;
					
			fixed4 colorClouds = (tex2D (_CloudTex, i.uv.xy) * tex2D (_CloudTex, i.uv.zw));//*0.5;						
			fixed4 shadow = (tex2D(_CloudTex, i.uv2.xy) * tex2D(_CloudTex, i.uv2.zw));
		
			colorClouds.rgb *= i.color.rgb;
			colorClouds.a *= cloudMask * i.color.a;
			fixed shadowAlpha = shadow.a*shadowMask*i.colorShadows.a;
			colorClouds.rgb = lerp(colorClouds.rgb, i.colorShadows.rgb, 1.0-colorClouds.a);
			colorClouds.a = max(colorClouds.a, shadowAlpha);
			//colorClouds.a += shadowAlpha;
			

			return colorClouds;
		}
		ENDCG 
	}
			
					
}


}

