// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'


Shader "CloudsCovering New" {

Properties {
	
	_Mask ("Mask Texture R-cloud, G-mask", 2D) = "white" {}
	_CloudsColor ("Clouds Color", Color) = (1,1,1,1)
	_ShadowColor("Shadow Color", Color) = (1,1,1,1)			

	_CloudAlpha("Clouds Alpha", float) = 0.5
	_Multiplier("Color multiplier", float) = 1			
	_CloudContrast("_Cloud Contrast", float) = 0.0

	_Covering("X-fill, Y-FillContrast, Z-EdgePower, W-EdgeThreshold", Vector) = (-1.17,3.0,4.12,0.31)
	
	_ShadowDisp("Shadow Offset XY zoom ZW", Vector) = (0.0,0.0,1.0,1.0)
				
	_TimeSlide("Time Slide",Vector) = (0,0,0,0)	
	_MaskSlide("Mask Slide",Vector) = (0,0,0,0)
		
}

	
SubShader {
	
	
	Tags { "Queue"="Transparent+20" "IgnoreProjector"="True" "RenderType"="Transparent"}
	
	
	//Blend One One   // additive 
	//Blend OneMinusDstColor One   // Soft additive 
//	Blend One OneMinusSrcColor

	//Blend DstColor Zero                 // Multiplicative
	//Blend DstColor SrcColor             // 2x Multiplicative
	//	Blend Zero SrcColor

	Blend SrcAlpha OneMinusSrcAlpha

	Cull Off Lighting Off ZWrite Off Fog { Color (0,0,0,0) }
	
	LOD 100
	
	CGINCLUDE	
	#include "UnityCG.cginc"
		
	sampler2D _Mask;	
	sampler2D _MaskDetail;

	fixed _Multiplier;
	

	fixed _CloudContrast;

	
	fixed4 _Covering;
		
	fixed4 _Mask_ST;
	fixed4 _ShadowDisp;
	fixed4 _ShadowColor;
	fixed _CloudAlpha;
	fixed4 _CloudsColor;
	fixed4 _TimeSlide;
	fixed4 _MaskSlide;
	
	struct v2f {
		float4	pos	: SV_POSITION;
		float4	uv		: TEXCOORD0;
		float4 uv2 : TEXCOORD1;
		fixed4	color	: COLOR; 

	};

	
	v2f vert (appdata_full v)
	{
		v2f 		o;
		o.pos = UnityObjectToClipPos(v.vertex);

		o.uv.xy = v.texcoord.xy*fixed2(_Mask_ST.z, _Mask_ST.z*_Mask_ST.y/ _Mask_ST.x) + frac(_TimeSlide.xy*_Time.x*_Mask_ST.z);
		o.uv.zw = v.texcoord.xy*fixed2(_Mask_ST.w, _Mask_ST.w*_Mask_ST.y / _Mask_ST.x) + frac(_TimeSlide.zw*_Time.x*_Mask_ST.w);
		
		o.uv2.xy = (v.texcoord.xy-0.5)*_Mask_ST.xy + frac(_MaskSlide.xy*_Time.x*_Mask_ST.xy);
		o.uv2.zw = o.uv2.xy*_ShadowDisp.zw + _ShadowDisp.xy;

		o.color	=  _CloudsColor*_Multiplier;
		o.color.a = _CloudAlpha;
				
		return o;
	}
	ENDCG

		Pass{
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#pragma fragmentoption ARB_precision_hint_fastest		
		fixed4 frag(v2f i) : COLOR
			{
				fixed tex = tex2D(_Mask, i.uv2.zw).r;
				tex = saturate(tex*_Covering.y + _Covering.x);
				fixed4 shadowMask = tex.r*_ShadowColor.a;
				shadowMask.rgb = _ShadowColor.rgb;
				return shadowMask;
			}
		ENDCG
		}
	
	Pass {
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#pragma fragmentoption ARB_precision_hint_fastest		
		fixed4 frag (v2f i) : COLOR
		{		
						
			fixed cloudMask = tex2D(_Mask, i.uv2.xy).r;
						
			cloudMask = saturate(cloudMask*_Covering.y + _Covering.x);

			fixed4 tex = (tex2D(_Mask, i.uv.xy).g * tex2D(_Mask, i.uv.zw).g);
					
			fixed4 colorClouds = (tex-_CloudContrast) / (1.0 - _CloudContrast)*i.color;
						
			colorClouds.a = cloudMask;
			colorClouds.a -= ((1.0 - abs(cloudMask - 0.5)*2.0))*(tex*_Covering.z - _Covering.w);
			colorClouds.a *= i.color.a;
			return colorClouds;
		}
		ENDCG 
	}
			
					
}


}

