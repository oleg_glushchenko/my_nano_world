// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'


Shader "CloudsCovering New Rain" {

Properties {
	
	_Mask ("Mask Texture R-cloud, G-mask", 2D) = "white" {}
	_CloudsColor("Clouds Color", Color) = (1,1,1,1)
	_ShadowColor("Shadow Color", Color) = (1,1,1,1)

	_CloudAlpha("Clouds Alpha", float) = 0.8
	_Multiplier("Color multiplier", float) = 1.71
	_CloudContrast("_Cloud Contrast", float) = 0.0

	_Covering("Covering X-fill, Y-FillContrast, Z-EdgePower, W-EdgeThreshold", Vector) = (-1.17,3.0,4.12,0.31)

	_RainFill("Rain Fill", float) = -1.4
	_RainTile("Rain Tile", Vector) = (20,20,35,35)
	_RainSpeed("Rain Speed", Vector) = (0.05, 0.2, 0.15,0.6)
	_Rain("Rain X-power, Y-phase offset, Z-amp, W-frequency", Vector) = (7.8,2.0,0.05,20)
	
	//_RainMaskOffset("Rain XY - offset, ZW - empty", Vector) = (-0.1,-0.2,0,0)

	_Offset("Offset Rain XY, Shadow ZW", Vector) = (0.1,0.04,-0.04,0.05)

	//_ShadowDisp("Shadow Offset XY zoom ZW", Vector) = (0.0,0.0,1.0,1.0)
				
	_TimeSlide("Time Slide",Vector) = (0,0,0,0)	
	_MaskSlide("Mask Slide zw - speed",Vector) = (0,0,0,0)
		
}

	
SubShader {
	
	
	Tags { "Queue"="Transparent+20" "IgnoreProjector"="True" "RenderType"="Transparent"}
	
	
	//Blend One One   // additive 
	//Blend OneMinusDstColor One   // Soft additive 
//	Blend One OneMinusSrcColor

	//Blend DstColor Zero                 // Multiplicative
	//Blend DstColor SrcColor             // 2x Multiplicative
	//	Blend Zero SrcColor

	Blend SrcAlpha OneMinusSrcAlpha

	Cull Back Lighting Off ZWrite Off Fog { Color (0,0,0,0) }
	
	LOD 100
	
	CGINCLUDE	
	#include "UnityCG.cginc"
		
	sampler2D _Mask;	
	sampler2D _MaskDetail;

	fixed _Multiplier;
	

	fixed _CloudContrast;

	
	fixed4 _Covering;
		
	fixed4 _Mask_ST;
	//fixed4 _ShadowDisp;
	fixed4 _ShadowColor;
	fixed _CloudAlpha;
	fixed _RainFill;
	fixed4 _CloudsColor;
	fixed4 _TimeSlide;
	fixed4 _MaskSlide;
	
	fixed4 _RainTile;
	fixed4 _RainSpeed;
	fixed4 _Rain;
	//fixed4 _RainMaskOffset;
	fixed4 _Offset;

	struct v2f {
		float4	pos	: SV_POSITION;
		float4	uv		: TEXCOORD0;
		float4 uv2 : TEXCOORD1;
		fixed4	color : COLOR;
		#if RAIN_ON 
			fixed4 uv3 : TEXCOORD2;
			fixed4 uv4 : TEXCOORD3;
		#endif
	};

	
	v2f vert (appdata_full v)
	{
		v2f 		o;
		o.pos = UnityObjectToClipPos(v.vertex);

		o.uv.xy = v.texcoord.xy*fixed2(_Mask_ST.z, _Mask_ST.z*_Mask_ST.y/ _Mask_ST.x) + frac(_TimeSlide.xy*_Mask_ST.z);
		o.uv.zw = v.texcoord.xy*fixed2(_Mask_ST.w, _Mask_ST.w*_Mask_ST.y / _Mask_ST.x) + frac(_TimeSlide.zw*_Mask_ST.w);
		
		o.uv2.xy = (v.texcoord.xy-0.5)*_Mask_ST.xy + frac(_MaskSlide.xy*_Mask_ST.xy);
		o.uv2.zw = o.uv2.xy + _Offset.zw;

		#if RAIN_ON 
			o.uv4.xy = o.uv2.xy + _Offset.xy;		
			o.uv3.xy = v.texcoord.xy*_RainTile.xy + frac(_RainSpeed.xy*_Time.x*_RainTile.xy);
			o.uv3.zw = v.texcoord.xy*_RainTile.zw + frac(_RainSpeed.zw*_Time.x*_RainTile.zw);
			//o.uv3.zw = v.texcoord.xy*_RainTile.zw + frac(_RainSpeed.zw*_Time.x*fixed2(-_RainTile.x, _RainTile.x));
			
		
			o.uv3.xz -= v.texcoord.y*30.0*_MaskSlide.z;		
			//o.uv3.xz -= v.texcoord.y*_MaskSlide.z;
		
			o.uv3.x += _Rain.z * sin(_Time.x*_Rain.w + (v.vertex.z)*_Rain.y);
			o.uv3.y += _Rain.z * sin(_Time.x*_Rain.w + (v.vertex.x)*_Rain.y);
			o.uv3.z += _Rain.z * sin(_Time.x*_Rain.w + (v.vertex.z+ v.vertex.x)*_Rain.y);
			o.uv3.w += _Rain.z * cos(_Time.x*_Rain.w + (v.vertex.y+ v.vertex.x)*_Rain.y);
		#endif

		o.color	=  _CloudsColor*_Multiplier;
		o.color.a = _CloudAlpha;
				
		return o;
	}
	ENDCG

		Pass{
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#pragma fragmentoption ARB_precision_hint_fastest		
		fixed4 frag(v2f i) : COLOR
			{
				fixed tex = tex2D(_Mask, i.uv2.zw).r;
				tex = saturate(tex*_Covering.y + _Covering.x);
				fixed4 shadowMask = tex.r*_ShadowColor.a;
				shadowMask.rgb = _ShadowColor.rgb;
				return shadowMask;
			}
		ENDCG
		}
	
	Pass {
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#pragma fragmentoption ARB_precision_hint_fastest		
		#pragma multi_compile __ RAIN_ON

		fixed4 frag (v2f i) : COLOR
		{

			fixed cloudMask = tex2D(_Mask, i.uv2.xy).r;

			cloudMask = saturate(cloudMask*_Covering.y + _Covering.x);

			fixed4 tex = (tex2D(_Mask, i.uv.xy).g * tex2D(_Mask, i.uv.zw).g);

			fixed4 colorClouds = (tex - _CloudContrast) / (1.0 - _CloudContrast)*i.color;

			colorClouds.a = cloudMask;
			colorClouds.a -= ((1.0 - abs(cloudMask - 0.5)*2.0))*(tex*_Covering.z - _Covering.w);
			colorClouds.a = saturate(colorClouds.a*i.color.a);
			#if RAIN_ON 
				fixed rain = tex2D(_Mask, i.uv3.xy).b * tex2D(_Mask, i.uv3.zw).b;
				fixed rainMask = tex2D(_Mask, i.uv4.xy).r;
				rainMask = saturate(rainMask*_Covering.y + _RainFill);

				fixed rainCol = rain* saturate(0.85 - colorClouds.a)*_Rain.x*saturate(rainMask*2.0 - 1.0);
				colorClouds.a = colorClouds.a + rainCol;
				colorClouds.rgb += (1.0 - colorClouds.rgb)*rainCol*2.0;
			#endif

			return colorClouds;
		}
		ENDCG 
	}
			
					
}


}

