#if !defined(UIOUTLINE_INCLUDED)
#define UIOUTLINE_INCLUDED 

#pragma target 2.0
#include "UnityCG.cginc"

sampler2D _MainTex; 
float _OutlineWidth;
fixed4 _ColorOutline;


struct appdata
{
	float4 vertex   : POSITION;
	float2 texcoord : TEXCOORD0;
};

struct v2f
{
	float4 vertex   : SV_POSITION; 
	float2 texcoord : TEXCOORD0;  
};

v2f vert(appdata v)
{
	v2f o;
	float s = 1.3;

#ifndef _grayscale
	v.vertex.z = 0.001;
#endif

	v.vertex.x += SHIFT.x*_OutlineWidth*s; // _OutlineWidth;
	v.vertex.y += SHIFT.y*_OutlineWidth*s; // _OutlineWidth;
		 
	o.vertex = UnityObjectToClipPos(v.vertex);
	o.texcoord = v.texcoord; 
	return o;
}

fixed4 frag(v2f i) : Color
{
	fixed4 tex1 = tex2D(_MainTex, i.texcoord);
	float a = pow(tex1.a , 4); 
	float4 c = _ColorOutline;
#ifdef _grayscale
	fixed4 grayscaled = Grayscale(c);
	c = lerp(c, grayscaled, _grayscale);
#endif

	return fixed4(c.rgb, a);
}





#endif