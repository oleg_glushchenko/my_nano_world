Shader "NanoReality/UI/AnimatedArcBackground"
{
    Properties
    {
        [PerRendererData]  _MainTex ("Texture", 2D) = "white" {}
        _BorderTex ("_BorderTex", 2D) = "white" {}
        
        _ClipInside ("_ClipInside", float) = 3.5
        _ClipOutside ("_ClipOutside", float) = 4.5
        _ClipTransitionWidth ("_ClipTransitionWidth", float) = .2
        
        _CloudsTex ("_CloudsTex", 2D) = "white" {} 
        
        
        _RadialScale ("_RadialScale", float) = 4.2
        _RadialLength ("_RadialLength", float) = 3.2
        
        _cloudSpeed ("_cloudSpeed", float) = .1
        
        _cloudPositionY ("_cloudPositionY", float) = .1
        
        _cloudWidth ("_cloudWidth", float) = .5
        _cloudHeight ("_cloudHeight", float) = .5
        
        
        _CloudsTex2 ("_CloudsTex2", 2D) = "white" {} 
        _cloudPositionY2 ("_cloudPositionY2", float) = .3
        _cloudSpeed2 ("_cloudSpeed2", float) = .2
        _cloudWidth2 ("_cloudWidth2", float) = .5        
        _cloudHeight2 ("_cloudHeight2", float) = .5
        
        
        _BackgroundColorA ("_BackgroundColorA", Color) = (0,0,0,0)
        _BackgroundColorB ("_BackgroundColorB", Color) = (0,0,0,0)
        
        _BottomColor ("_BottomColor", Color) = (0,0,0,0)
        _BottomColorHeight ("_BottomColorHeight", float) = .1
        
        _RadiusShadowColor ("_RadiusShadowColor", Color) = (0,0,0,0)
        _RadiusShadowColorWidth ("_RadiusShadowColorWidth", float) = .1
        
    }
    SubShader
    {
		Blend SrcAlpha OneMinusSrcAlpha 
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };
            
			float2 PolarCoordinates(float2 UV, float2 Center, float RadialScale, float LengthScale)
			{
			    float2 delta = UV - Center;
			    float radius = length(delta) * 2 * RadialScale;
			    float angle = atan2(delta.x, delta.y) * 1.0/6.28 * LengthScale;
			    return float2(radius, angle);
			}
			
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _BorderTex;
            sampler2D _CloudsTex;
            sampler2D _CloudsTex2;
            
            float _RadialScale;
            float _RadialLength;
            
            float _ClipInside;
            float _ClipOutside;
            float _ClipTransitionWidth;
            
            float _cloudSpeed;
            float _cloudSpeed2;
            
            float _cloudPositionY;
            float _cloudPositionY2;
            
            float _cloudHeight;
            float _cloudHeight2;
            
            float _cloudWidth;
            float _cloudWidth2;
            
            float4 _BackgroundColorA;
            float4 _BackgroundColorB;
            
            float4 _BottomColor;
            float4 _RadiusShadowColor;
            
            float _BottomColorHeight;
            float _RadiusShadowColorWidth;
             
            fixed4 frag (v2f i) : SV_Target
            {
                float2 scaledUv = float2(i.uv.x, i.uv.y*.5);
                
                float2 ruv = PolarCoordinates(scaledUv,float2(.5,0),_RadialScale,_RadialLength);
                
                fixed4 border = tex2D(_BorderTex, ruv);
                
                float l = length(ruv);
                
                float innerMask = smoothstep(_ClipInside+_ClipTransitionWidth, _ClipInside-_ClipTransitionWidth, l);
                float outerMask = smoothstep(_ClipOutside-_ClipTransitionWidth, _ClipOutside+_ClipTransitionWidth, l);
                
                fixed4 col = tex2D(_BorderTex, ruv);
                
                float4 clipped = clamp( col - innerMask, 0,1);
                clipped.a = clamp( clipped.a - outerMask, 0,1);
                
                fixed4 sky = lerp( _BackgroundColorB, _BackgroundColorA, i.uv.y ) * innerMask;
                                
                float2 suv1 = float2 (frac(i.uv.x*_cloudWidth + _Time.x*_cloudSpeed), i.uv.y*_cloudHeight -_cloudPositionY );
                float2 suv2 = float2 (frac(i.uv.x*_cloudWidth2 + _Time.x*_cloudSpeed2), i.uv.y*_cloudHeight2 -_cloudPositionY2 );
                
                fixed4 cloud1 = tex2D(_CloudsTex, suv1) * innerMask;
                fixed4 cloud2 = tex2D(_CloudsTex2, suv2) * innerMask;
                
                sky.rgb = lerp( sky.rgb , cloud1.rgb, cloud1.a); 
                sky.rgb = lerp( sky.rgb , cloud2.rgb, cloud2.a); 
                
                float shadow = smoothstep(_ClipInside+_RadiusShadowColorWidth, _ClipInside+_ClipTransitionWidth, l);                
                sky.rgb = lerp( sky.rgb , _RadiusShadowColor.rgb, shadow*_RadiusShadowColor.a); 
                                
                sky.rgb = lerp( sky.rgb , _BottomColor.rgb,  clamp(1-i.uv.y - _BottomColorHeight,0,1 ) * _BottomColor.a) * innerMask; 
                
                return   clipped+sky;
            }
            ENDCG
        }
    }
}
