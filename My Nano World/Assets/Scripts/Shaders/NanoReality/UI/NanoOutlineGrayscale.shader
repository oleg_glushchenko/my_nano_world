﻿Shader "NanoReality/UI/NanoOutlineGrayscale"  
{
	Properties
	{
		[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
		_Color("Tint", Color) = (1,1,1,1)
		_ColorOut("OutlineColor", Color) = (1,1,1,1)
		_UvOffset("UvOffsetValue", Range(0,0.1)) = 0.0016

		_StencilComp("Stencil Comparison", Float) = 8
		_Stencil("Stencil ID", Float) = 0
		_StencilOp("Stencil Operation", Float) = 0
		_StencilWriteMask("Stencil Write Mask", Float) = 255
		_StencilReadMask("Stencil Read Mask", Float) = 255

		_ColorMask("Color Mask", Float) = 15

		[Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip("Use Alpha Clip", Float) = 0
	}

		SubShader
		{
			Tags
			{
				"Queue" = "Transparent"
				"IgnoreProjector" = "True"
				"RenderType" = "Transparent"
				"PreviewType" = "Plane"
				"CanUseSpriteAtlas" = "True"
			}

			Stencil
			{
				Ref[_Stencil]
				Comp[_StencilComp]
				Pass[_StencilOp]
				ReadMask[_StencilReadMask]
				WriteMask[_StencilWriteMask]
			}

			Cull Off
			Lighting Off
			ZWrite Off
			ZTest[unity_GUIZTestMode]
			Blend SrcAlpha OneMinusSrcAlpha
			ColorMask[_ColorMask]

			Pass
			{
				Name "Default"
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma target 2.0

				#include "UnityCG.cginc"
				#include "UnityUI.cginc"

				#pragma multi_compile __ UNITY_UI_CLIP_RECT
				#pragma multi_compile __ UNITY_UI_ALPHACLIP

			float4 Grayscale(fixed4 c)
			{
				float4 color = c;
				color.rgb = dot(color.rgb, float3(0.3, 0.59, 0.11));
				color.rgb *= 1.9;
				color.rgb = (color.rgb - 0.5) * 0.5 + 0.5;
				float q = 0.9;
				float3 mask = float3(0.184, 0.6, 1);
				color.rgb = q * color.rgb + (1 - q) * mask;
				return fixed4(pow(color.rgb,1.5), color.a);
			}


				struct appdata_t
				{
					float4 vertex   : POSITION;
					float4 color    : COLOR;
					float2 uv : TEXCOORD0;
				};

				struct v2f
				{
					float4 vertex   : SV_POSITION;
					fixed4 color : COLOR;
					float2 uv  : TEXCOORD0;
					float4 worldPosition : TEXCOORD1;
				};

				uniform float _GrayscaleMode;

				sampler2D _MainTex;
				float4 _MainTex_ST;
				fixed4 _Color;
				fixed4 _ColorOut;
				fixed4 _TextureSampleAdd;
				float4 _ClipRect;
				float _UvOffset;

				v2f vert(appdata_t v)
				{
					v2f o;
					o.worldPosition = v.vertex;
					o.vertex = UnityObjectToClipPos(o.worldPosition);
					o.uv = TRANSFORM_TEX(v.uv, _MainTex);
					o.color = v.color * _Color;
					return o;
				}

				fixed4 frag(v2f i) : SV_Target
				{
					float2 NE = float2(i.uv.x + _UvOffset, i.uv.y + _UvOffset);
					float2 NW = float2(i.uv.x + _UvOffset, i.uv.y - _UvOffset);
					float2 SW = float2(i.uv.x - _UvOffset, i.uv.y - _UvOffset);
					float2 SE = float2(i.uv.x - _UvOffset, i.uv.y + _UvOffset);

					fixed outlineNE = tex2D(_MainTex, NE).a;
					fixed outlineNW = tex2D(_MainTex, NW).a;
					fixed outlineSW = tex2D(_MainTex, SW).a;
					fixed outlineSE = tex2D(_MainTex, SE).a;

					fixed4 color = tex2D(_MainTex, i.uv);
					fixed outline = (outlineNE + outlineNW + outlineSW + outlineSE) * (1 - color.a);
					fixed4 outlineColored = outline * _ColorOut;

					fixed4 result = fixed4(color + outlineColored);
					fixed mask = (1 - color.a);

					fixed4 o = outlineColored * mask + color * (1 - mask);
					fixed4 grayscaled = Grayscale(o);
					o = lerp(o, grayscaled, _GrayscaleMode);

					return o;
				}
			ENDCG
			}
		}
}