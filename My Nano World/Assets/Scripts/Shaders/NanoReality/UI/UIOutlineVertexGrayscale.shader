﻿Shader "NanoReality/UI/NanoUIOutlineGrayscale"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_OutlineWidth("Width", Float) = 1
		_ColorOutline("Outline Color", Color) = (0,0,0,1)
		[PerRendererData] _Color("Tint Color", Color) = (1,1,1,1)
    }
	SubShader
	{
		Tags
		{
			"Queue" = "Transparent"
			"IgnoreProjector" = "True"
			"RenderType" = "ReplacementBuildingOutline"
			"PreviewType" = "Plane"
			"CanUseSpriteAtlas" = "True"
		}
		Cull Off
		Lighting Off
		Blend SrcAlpha OneMinusSrcAlpha
		Zwrite Off
		Ztest Less

		CGINCLUDE
		float4 Grayscale(fixed4 c)
		{
			float4 color = c;
			color.rgb = dot(color.rgb, float3(0.3, 0.59, 0.11));
			color.rgb *= 1.9;
			color.rgb = (color.rgb - 0.5) * 0.5 + 0.5;
			float q = 0.9;
			float3 mask = float3(0.184, 0.6, 1);
			color.rgb = q * color.rgb + (1 - q) * mask;
			return fixed4(pow(color.rgb,1.5), color.a);
		}

		uniform float _GrayscaleMode;

		ENDCG

		Pass
		{  
			Name "Outline - L"
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#define _grayscale _GrayscaleMode
			#define SHIFT float2(1, 0)
			#include "SubShaders/UIOutline.cginc"

			ENDCG
		} 
		Pass
		{
			Name "Outline - R" 

			 CGPROGRAM
			 #pragma vertex vert
			 #pragma fragment frag
			#define _grayscale _GrayscaleMode
			 #define SHIFT float2(-1, 0)
			 #include "SubShaders/UIOutline.cginc"

			 ENDCG
		 }
		 Pass
		 {
			 Name "Outline - U" 

			  CGPROGRAM
			  #pragma vertex vert
			  #pragma fragment frag
			  #define _grayscale _GrayscaleMode
			  #define SHIFT float2(0, 1)
			  #include "SubShaders/UIOutline.cginc"

			  ENDCG
		  }
		  Pass
		  {
			  Name "Outline - D" 

			   CGPROGRAM
			   #pragma vertex vert
			   #pragma fragment frag
			   #define _grayscale _GrayscaleMode
			   #define SHIFT float2(0, -1)
			   #include "SubShaders/UIOutline.cginc"

			   ENDCG
		   }
		   Pass
		   {
			   Name "Default"
				 
			  CGPROGRAM
			  #pragma vertex vert
			  #pragma fragment frag  
			  #define _grayscale _GrayscaleMode
			   #include "SubShaders/UIIcon.cginc" 
			   ENDCG
		   }
	}
	FallBack "NanoReality/UI/NanoOutlineGrayscale"
}
