﻿#if !defined(CLOUDSANIMATION_INCLUDED)
#define CLOUDSANIMATION_INCLUDED

uniform float _TurnX=0;
uniform float _TurnY=-45;
uniform float _TurnZ=-30;            
uniform half _CloudUvX=0.008;
uniform half _CloudUvY=0.018;
uniform fixed4 _CloudsCol=fixed4(0,.14,1,.39);
uniform half _CloudsWiggleIntensity=1;
uniform half _CloudsWiggleScale=1;
uniform half _CloudsTwinkleIntensity=1;
 
uniform half _CloudsTwinkleScale=5;
uniform half _CloudsTwinkleSpeed=-0.02;
uniform half _OffsetProgress=0;

uniform sampler2D_half _CloudsTex ;

float2 CloudsUV(float4 vertex)
{	
    fixed4 worldPos = mul(unity_ObjectToWorld, vertex);    
    fixed4 turnedorldPos = RotateVertsAllAxisDeg_lp(worldPos, _TurnX, _TurnY, _TurnZ);
    float worldUvX = (turnedorldPos.x/ turnedorldPos.w * _CloudUvX);
    float worldUvY = (turnedorldPos.y/ turnedorldPos.w * _CloudUvY);    
    return float2(worldUvX, worldUvY);                
}

float CloudsAmount( float2 worldUv, float4 time )
{	
    half2 cloudsUVoffset = OffsetUVplussed_lp(worldUv, fixed2(1, 0), _OffsetProgress);
    fixed twinkle = tex2D(_CloudsTex, worldUv * _CloudsTwinkleScale).g;
    fixed twinklesin = sin( time * _CloudsTwinkleSpeed ) * twinkle;
    twinklesin += _CloudsTwinkleIntensity; 
    twinklesin /= _CloudsTwinkleIntensity;
    
    fixed wiggle = tex2D(_CloudsTex, worldUv * _CloudsWiggleScale).b * _CloudsWiggleIntensity;
    return tex2D(_CloudsTex, cloudsUVoffset + wiggle).r * _CloudsCol.a * twinklesin;                
}

float4 CloudsMix( float4 clouds, float4 color)
{
    clouds = clamp(clouds, 0, 1);
    fixed3 cloudsColored = clouds * color * _CloudsCol.rgb;
    fixed cloudsMask = 1 - clouds;                 
    return fixed4(color.rgb * cloudsMask + cloudsColored.rgb, color.a);
}

#endif