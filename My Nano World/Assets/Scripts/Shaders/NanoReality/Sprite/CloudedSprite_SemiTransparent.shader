﻿Shader "NanoReality/Sprite/Clouded_SemiTransparent"
{
	Properties
	{
		[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
		_Color("Tint", Color) = (1,1,1,.5)
				
		[MaterialToggle] PixelSnap("Pixel snap", Float) = 0
				
		[HideInInspector] _RendererColor("RendererColor", Color) = (1,1,1,1)
		[HideInInspector] _Flip("Flip", Vector) = (1,1,1,1)
		[PerRendererData] _AlphaTex("External Alpha", 2D) = "white" {}
		[PerRendererData] _EnableExternalAlpha("Enable External Alpha", Float) = 0 
	}

	SubShader
	{
		Tags
		{
			"Queue" = "Transparent"
			"IgnoreProjector" = "True"
			"RenderType"="ReplacementBuilding"
			"PreviewType" = "Plane"
			"CanUseSpriteAtlas" = "True"
		}

		Cull Off
		Lighting Off
		Blend SrcAlpha OneMinusSrcAlpha
		Zwrite Off
		Ztest Less

		GrabPass
		{
			"_BackgroundTexture"
		}
		Pass
		{       
            CGPROGRAM
            #pragma vertex SpriteVert
            #pragma fragment SpriteFrag
            #pragma target 2.0
            #pragma multi_compile_instancing
            #pragma multi_compile_local _ PIXELSNAP_ON
            #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
            
            #pragma multi_compile TRANSPARENCY_OFF TRANSPARENCY_ON
                    
            #ifndef UNITY_SPRITES_INCLUDED
            #define UNITY_SPRITES_INCLUDED
    
            #include "UnityCG.cginc"
            #include "../CGincludes/MerlinVertexTrix.cginc"
            #include "../CGincludes/MerlinUV.cginc"            
            #include "../CGincludes/CloudsAnimation.cginc"
    
            #ifdef UNITY_INSTANCING_ENABLED
    
            UNITY_INSTANCING_BUFFER_START(PerDrawSprite)
            // SpriteRenderer.Color while Non-Batched/Instanced.
            UNITY_DEFINE_INSTANCED_PROP(fixed4, unity_SpriteRendererColorArray)
            // this could be smaller but that's how bit each entry is regardless of type
            UNITY_DEFINE_INSTANCED_PROP(fixed2, unity_SpriteFlipArray)
            UNITY_INSTANCING_BUFFER_END(PerDrawSprite)
    
            #define _RendererColor  UNITY_ACCESS_INSTANCED_PROP(PerDrawSprite, unity_SpriteRendererColorArray)
            #define _Flip           UNITY_ACCESS_INSTANCED_PROP(PerDrawSprite, unity_SpriteFlipArray)
    
            #endif // instancing
    
            CBUFFER_START(UnityPerDrawSprite)
            #ifndef UNITY_INSTANCING_ENABLED
            fixed4 _RendererColor;
            fixed2 _Flip;
            #endif
            float _EnableExternalAlpha;
            CBUFFER_END
    
            // Material Color.
            fixed4 _Color;
    
            struct appdata_t
            {
                float4 vertex   : POSITION;
                float4 color    : COLOR;
                float2 texcoord : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };
    
            struct v2f
            {
                float4 vertex   : SV_POSITION;
                fixed4 color : COLOR;
                float2 texcoord : TEXCOORD0;
                half2 worldUv: TEXCOORD1;
				float4 time : TANGENT;
		        float4 grabPos : TEXCOORD2;
                UNITY_VERTEX_OUTPUT_STEREO
            };
    
            sampler2D _MainTex;
            sampler2D _AlphaTex;
            sampler2D _BackgroundTexture;
      
            const fixed one = 1;
            const fixed zero = 0;
                
            inline float4 UnityFlipSprite(in float3 pos, in fixed2 flip)
            {
                return float4(pos.xy * flip, pos.z, 1.0);
            }
    
            v2f SpriteVert(appdata_t v)
            {
            
                v2f o;
    
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
    
                o.vertex = UnityFlipSprite(v.vertex, _Flip);
                o.vertex = UnityObjectToClipPos(o.vertex);
                o.texcoord = v.texcoord;
                o.color = v.color * _Color * _RendererColor;
		        o.grabPos = ComputeGrabScreenPos(o.vertex);
                
                o.worldUv  = CloudsUV(v.vertex);
				o.time = (_Time % 200) / 200;
    
                #ifdef PIXELSNAP_ON
                o.vertex = UnityPixelSnap(o.vertex);
                #endif

                return o;
            }
    
            fixed4 SampleSpriteTexture(float2 uv)
            {
                fixed4 color = tex2D(_MainTex, uv);
                #if ETC1_EXTERNAL_ALPHA
                fixed4 alpha = tex2D(_AlphaTex, uv);
                color.a = lerp(color.a, alpha.r, _EnableExternalAlpha);
                #endif
                return color;
            } 
    
            fixed4 SpriteFrag(v2f i) : SV_Target
            {                
                fixed4 color = SampleSpriteTexture(i.texcoord) * i.color;
                half4 bgcolor = tex2Dproj(_BackgroundTexture,  i.grabPos);                   
                
                bgcolor *= (1 -  _Color.a*color.a);	
                color.rgb *= _Color.a*color.a; 
                color.rgb += bgcolor.rgb; 
                clip(color.a-.1);
                
                //Clouds 
                fixed4 result = CloudsMix( CloudsAmount( i.worldUv,  i.time * 200 ) , color);
                
                return result;
            }
    
            #endif // UNITY_SPRITES_INCLUDED
    
            ENDCG
		}
	}
}
