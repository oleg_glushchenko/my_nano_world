﻿Shader "NanoReality/Sprite/CloudedUpCity"
{
	Properties
	{
		[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
		_Color("Tint", Color) = (1,1,1,1)
		[MaterialToggle] PixelSnap("Pixel snap", Float) = 0
		[HideInInspector] _RendererColor("RendererColor", Color) = (1,1,1,1)
		[HideInInspector] _Flip("Flip", Vector) = (1,1,1,1)
		[PerRendererData] _AlphaTex("External Alpha", 2D) = "white" {}
		[PerRendererData] _EnableExternalAlpha("Enable External Alpha", Float) = 0
		[Space(20)] //TurnWorldspace
		_TurnX("TurnX", Float) = 0
        _TurnY("TurnY", Float) = -45
        _TurnZ("TurnZ", Float) = -35
		[Space(20)]
		_CloudUvX("CloudsUvXscale", Float) = 0
		_CloudUvY("CloudsUvYscale", Float) = 0
		_CloudsTex("CloudsTexture", 2D) = "white" {}
		_CloudsCol("CloudsColor", COLOR) = (1,1,1,1)
		_CloudsTwinkleIntensity("CloudsTwinkleIntensity", Float) = 0
		_CloudsTwinkleScale("CloudsTwinkleScale", Float) = 0
		_CloudsTwinkleSpeed("CloudsTwinkleSpeed", Float) = 0
		_CloudsWiggleScale("CloudsWiggleScale", Float) = 0
		_CloudsWiggleIntensity("CloudsWiggleIntensity", Float) = 0
		_CloudsSpeed ("CloudsSpeed", Float) = 0
	}

	SubShader
	{
		Tags
		{
			"Queue" = "Transparent"
			"IgnoreProjector" = "True"
			"RenderType" = "Transparent"
			"PreviewType" = "Plane"
			"CanUseSpriteAtlas" = "True"
		}

		Cull Off
		Lighting Off
		ZWrite On
		Ztest Less
		Blend One OneMinusSrcAlpha

		Pass
		{
		    Stencil 
                {
                    Ref 4
                    Comp Always
                    Pass Replace
                }
                
            CGPROGRAM
            #pragma vertex SpriteVert
            #pragma fragment SpriteFrag
            #pragma target 2.0
            #pragma multi_compile_instancing
            #pragma multi_compile_local _ PIXELSNAP_ON
            #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
                    
            #ifndef UNITY_SPRITES_INCLUDED
            #define UNITY_SPRITES_INCLUDED
    
            #include "UnityCG.cginc"
            #include "../CGincludes/MerlinVertexTrix.cginc"
    
            #ifdef UNITY_INSTANCING_ENABLED
    
            UNITY_INSTANCING_BUFFER_START(PerDrawSprite)
            // SpriteRenderer.Color while Non-Batched/Instanced.
            UNITY_DEFINE_INSTANCED_PROP(fixed4, unity_SpriteRendererColorArray)
            // this could be smaller but that's how bit each entry is regardless of type
            UNITY_DEFINE_INSTANCED_PROP(fixed2, unity_SpriteFlipArray)
            UNITY_INSTANCING_BUFFER_END(PerDrawSprite)
    
            #define _RendererColor  UNITY_ACCESS_INSTANCED_PROP(PerDrawSprite, unity_SpriteRendererColorArray)
            #define _Flip           UNITY_ACCESS_INSTANCED_PROP(PerDrawSprite, unity_SpriteFlipArray)
    
            #endif // instancing
    
            CBUFFER_START(UnityPerDrawSprite)
            #ifndef UNITY_INSTANCING_ENABLED
            fixed4 _RendererColor;
            fixed2 _Flip;
            #endif
            float _EnableExternalAlpha;
            CBUFFER_END
    
            // Material Color.
            fixed4 _Color;
    
            struct appdata_t
            {
                float4 vertex   : POSITION;
                float4 color    : COLOR;
                float2 texcoord : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };
    
            struct v2f
            {
                float4 vertex   : SV_POSITION;
                fixed4 color : COLOR;
                float2 texcoord : TEXCOORD0;
                half2 worldUv: TEXCOORD1;
                UNITY_VERTEX_OUTPUT_STEREO
            };
    
            sampler2D _MainTex;
            sampler2D _AlphaTex;
    
            sampler2D_half _CloudsTex;
            half4 _CloudsTex_ST;
            
            float _TurnX;
            float _TurnY;
            float _TurnZ;
            
            half _CloudUvX;
            half _CloudUvY;
            fixed4 _CloudsCol;
            half _CloudsWiggleIntensity;
            half _CloudsWiggleScale;
            half _CloudsTwinkleIntensity;
            half _CloudsTwinkleScale;
            half _CloudsTwinkleSpeed;
            half _CloudsSpeed;

            const fixed one = 1;
            const fixed zero = 0;
			    
            half2 OffsetCloudsUV(half2 uv, half offsetX, half offsetY, half speed)
            {
                half Xspeed = speed * offsetX;
                half Yspeed = speed * offsetY;

                half offsetedX = uv.x + ((_Time.y * Xspeed) % 2);
                half offsetedY = uv.y + ((_Time.y * Yspeed) % 2);

                return half2(offsetedX, offsetedY);
            }
                
            inline float4 UnityFlipSprite(in float3 pos, in fixed2 flip)
            {
                return float4(pos.xy * flip, pos.z, 1.0);
            }
    
            v2f SpriteVert(appdata_t v)
            {
                fixed4 turnedVets = RotateVertsAllAxisDeg_hp(v.vertex, _TurnX, _TurnY, _TurnZ);
                fixed4 worldPos = mul(unity_ObjectToWorld, normalize(turnedVets));
                float worldUvX = worldPos.x/ worldPos.w * _CloudUvX;
                float worldUvY = worldPos.y/ worldPos.w * _CloudUvY;
                
                v2f o;
    
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
    
                o.vertex = UnityFlipSprite(v.vertex, _Flip);
                o.vertex = UnityObjectToClipPos(o.vertex);
                o.texcoord = v.texcoord;
                o.worldUv  = float2(worldUvX, worldUvY);
                o.color = v.color * _Color * _RendererColor;
    
                #ifdef PIXELSNAP_ON
                o.vertex = UnityPixelSnap(o.vertex);
                #endif
    
                return o;
            }
    
            fixed4 SampleSpriteTexture(float2 uv)
            {
                fixed4 color = tex2D(_MainTex, uv);
    
                #if ETC1_EXTERNAL_ALPHA
                fixed4 alpha = tex2D(_AlphaTex, uv);
                color.a = lerp(color.a, alpha.r, _EnableExternalAlpha);
                #endif
    
                return color;
            }
    
            fixed4 SpriteFrag(v2f i) : SV_Target
            {
                
                fixed4 color = SampleSpriteTexture(i.texcoord) * i.color;
                color.rgb *= color.a;
                
                
                //Clouds
                half2 cloudsUVoffset = OffsetCloudsUV(i.worldUv, 1, 0, _CloudsSpeed);
                fixed twinkle = tex2D(_CloudsTex, i.worldUv * _CloudsTwinkleScale).g;
                fixed twinklesin = sin(_Time.y * _CloudsTwinkleSpeed) * twinkle;
                twinklesin += _CloudsTwinkleIntensity;
                twinklesin /= _CloudsTwinkleIntensity;
                fixed wiggle = tex2D(_CloudsTex, i.worldUv * _CloudsWiggleScale).b * _CloudsWiggleIntensity;
                fixed clouds = tex2D(_CloudsTex, cloudsUVoffset + wiggle).r * _CloudsCol.a * twinklesin;
                fixed3 cloudsColored = clouds * color * _CloudsCol.rgb;
                fixed cloudsMask = 1 - clouds;
                
                fixed4 result = fixed4(color.rgb * cloudsMask + cloudsColored, color.a);
                    
                return result;
            }
    
            #endif // UNITY_SPRITES_INCLUDED
    
            ENDCG
		}
	}
}
