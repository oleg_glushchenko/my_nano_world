﻿Shader "NanoReality/Sprite/DynamicAlpha"
{
    Properties
    {
        _MainTex ("Texture OutlineMask", 2D) = "white" {} //outlineMask
		_MainCol("Tint Color", COLOR) = (1,1,1,1)
		_MainIntensity("OutlineIntensity", Float) = 1
		[Space(10)]
		_AlphaIntensity("AlphaIntensity", Float) = 1
		[Space(10)]
		_Mask1("Mask1", 2D) = "white" {}
		_Mask1Col("Mask1 Color", COLOR) = (1,1,1,1)
		_Mask1params("Mask1 params", Vector) = (0,0,0,0) //AxisMaskXY, Speed, Something
		_Mask1Intensity("OutlineIntensity", Float) = 1
		[Space(10)]
		_Mask2("Mask2", 2D) = "white" {}
		_Mask2Col("Mask2 Color", COLOR) = (1,1,1,1)
		_Mask2params("Mask2 params", Vector) = (0,0,0,0) //AxisMaskXY, Speed, Something
		_Mask2Intensity("OutlineIntensity", Float) = 1

		[Toggle(USE_REAL_ALPHA)] _UseRealAlpha("Use Real Alpha", Float) = 0

[Enum(Off,0,On,1)]_ZWrite("ZWrite", Float) = 0
[Enum(UnityEngine.Rendering.CompareFunction)] _ZTest("ZTest", Float) = 4 //"LessEqual"
[Enum(UnityEngine.Rendering.CullMode)] _Culling("Culling", Float) = 0

    }
    SubShader
    {
		Tags { "Queue" = "Transparent" "RenderType" = "Transparent" }
		ZWrite[_ZWrite]
		ZTest[_ZTest]
		Cull[_Culling]
		
		//Blend OneMinusDstColor One //My doesn't take vertex alpha =(
		Blend SrcAlpha OneMinusSrcAlpha // Traditional transparency
		//Blend One OneMinusSrcAlpha // Premultiplied transparency
		//Blend One One // Additive
		//Blend OneMinusSrcAlpha One

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#pragma shader_feature USE_REAL_ALPHA 

            #include "UnityCG.cginc"
			#include "../../NanoReality/CGincludes/MerlinUV.cginc"

			sampler2D_half _MainTex;
			half4 _MainTex_ST;
			sampler2D_half _Mask1;
			half4 _Mask1_ST;
			sampler2D_half _Mask2;
			half4 _Mask2_ST;
			half _MainIntensity;
			half _Mask1Intensity;
			half _Mask2Intensity;
			half _AlphaIntensity;
			half4 _Mask1params;
			half4 _Mask2params;
			fixed4 _MainCol;
			fixed4 _Mask1Col;
			fixed4 _Mask2Col;


            struct vertInput
            {
                float4 vertex : POSITION;
                half2 uv : TEXCOORD0;
				float3 screenPos: TEXCOORD2;
				fixed4 color : COLOR;
            };

            struct v2f
            {
                half2 main_uv : TEXCOORD0;
				half2 mask1_uv : TEXCOORD1;
				half2 mask2_uv : TEXCOORD2;
                float4 vertex : SV_POSITION;
				fixed4 color : COLOR;
            };


            v2f vert (vertInput v)
            {
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);

				float2 screenUv = float2(o.vertex.xy/ o.vertex.w);

				half2 mask1_uv_offset = OffsetUVlinear_lp(screenUv, _Mask1params.xy, _Mask1params.z);
				half2 mask2_uv_offset = OffsetUVlinear_lp(screenUv, _Mask2params.xy, _Mask2params.z);

                
                o.main_uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.mask1_uv = TRANSFORM_TEX(mask1_uv_offset, _Mask1);
				o.mask2_uv = TRANSFORM_TEX(mask2_uv_offset, _Mask2);
				o.color = v.color;
                return o;
            }


            fixed4 frag (v2f i) : COLOR
            {
                fixed3 main = _MainIntensity * _MainCol.rgb;
				fixed3 mask1 = _Mask1Intensity * _Mask1Col.rgb;
				fixed3 mask2 = _Mask2Intensity * _Mask2Col.rgb;
				

#ifdef USE_REAL_ALPHA 
				fixed mainAlpha = tex2D(_MainTex, i.main_uv).a;
#else
				fixed mainAlpha = tex2D(_MainTex, i.main_uv).r;
#endif
				fixed mask1Alpha = tex2D(_Mask1, i.mask1_uv).r;
				fixed mask2Alpha = tex2D(_Mask2, i.mask2_uv).r;

				fixed alpha = mainAlpha * mask1Alpha * mask2Alpha * i.color.a * _AlphaIntensity;

                return fixed4(main * mask1 * mask2 * i.color.rgb, alpha);
            }
            ENDCG
        }
    }
}
