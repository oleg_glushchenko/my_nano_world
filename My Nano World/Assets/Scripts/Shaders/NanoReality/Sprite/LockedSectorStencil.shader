﻿Shader "NanoReality/Sprite/LockedSectorStencil"
{
    Properties
    {
        _MainTex ("Base (RGB)", 2D) = "white" {}
		_Color ("Main Color", Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags { "RenderType"="LockedSector" }
        LOD 100
        ZWrite off

        Pass
        {
//            Stencil 
//                {
//                    Ref 2
//                    Comp Greater
//                    Pass Zero
//                }
            Cull back
            Blend SrcAlpha OneMinusSrcAlpha
            Lighting Off
                
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 pos : SV_POSITION;
            };

            sampler2D _MainTex;
			float4 _MainTex_ST;
			fixed4 _Color;
			uniform float _MainTexBias;

            v2f vert (appdata v)
            {
                v2f o;
				o.uv = TRANSFORM_TEX (v.uv, _MainTex);
				o.pos = UnityObjectToClipPos(v.vertex);
				return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                return tex2D(_MainTex, i.uv) * _Color;
            }
            ENDCG
        }
    }
}
