﻿Shader "NanoReality/Environment/TreesNoAnimation"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {} 
    }
    SubShader
    {
        Tags { "RenderType"="ReplacementTransparent" "QUEUE" = "Transparent+5"}

		Blend SrcAlpha OneMinusSrcAlpha
			
        Pass
        {       
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			
            #include "UnityCG.cginc"
            #include "../CGincludes/MerlinVertexTrix.cginc"
            #include "../CGincludes/MerlinUV.cginc"
            #include "../CGincludes/CloudsAnimation.cginc"
  
            sampler2D_half _MainTex;
            fixed4 _MainTex_ST;
			sampler2D_half _WiggleTex;
			fixed4 _WiggleTex_ST;
			 

			const fixed one = 1;
			const fixed zero = 0;
			
			struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float4 color : COLOR;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float2 worldUv : TEXCOORD1;
				float4 time : TANGENT;
            };
			
            v2f vert (appdata v)
            {				 
                v2f o; 
				o.vertex = UnityObjectToClipPos(v.vertex);				 
                o.worldUv  = CloudsUV(v.vertex);
				o.time = (_Time % 200) / 200;
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 allVegetation = tex2D(_MainTex, i.uv);
                
                //Clouds 
                fixed4 result = CloudsMix( CloudsAmount( i.worldUv, i.time * 200) , allVegetation);
                
                return result;
            }
            ENDCG
        }
    }
	FallBack "Unlit/Transparent"
}
