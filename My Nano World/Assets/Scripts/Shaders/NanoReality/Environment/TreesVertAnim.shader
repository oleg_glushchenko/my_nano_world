﻿Shader "NanoReality/Environment/TreesVertAnim"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		[Space(10)]
		[Header(WIGGLE PARAMETERS)]
		_WiggleTex("WiggleMask", 2D) = "white" {}
		_WiggleSpeed("WiggleSpeed", Float) = 1
		_WiggleScale("WiggleScale", Float) = 1
		_WiggleIntensity("WiggleIntensity", Float) = 1
		[Space(10)]
		[Header(HORIZONTAL DISTORTION)]
		_WavelengthHor("WaveLengthHorizontal", FLoat) = 1
		_WaveIntensityHor("WaveIntensityHorizontal", FLoat) = 1
		_AmplitudeHor ("AmplitudeHorizontal", FLoat) = 1
		_SpeedHor ("SpeedHorizontal", Float) = 1
		[Space(10)]
		[Header(VERTICAL DISTORTION)]
		_WavelengthVert("WaveLengthVertical", FLoat) = 1
		_WaveIntensityVert("WaveIntensityVertical", FLoat) = 1
		_AmplitudeVert("AmplitudeVertical", FLoat) = 1
		_SpeedVert("SpeedVertical", Float) = 1 
    }
    SubShader
    {
        Tags { "RenderType"="ReplacementTransparent" "QUEUE" = "Transparent+5"}

		Blend SrcAlpha OneMinusSrcAlpha
			
        Pass
        {       
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			
            #include "UnityCG.cginc"
            #include "../CGincludes/MerlinVertexTrix.cginc"
            #include "../CGincludes/MerlinUV.cginc"
            #include "../CGincludes/CloudsAnimation.cginc"

            sampler2D_half _MainTex;
            fixed4 _MainTex_ST;
			sampler2D_half _WiggleTex;
			fixed4 _WiggleTex_ST;
			
			half _WiggleSpeed;
			half _WiggleIntensity;
			half _WiggleScale;
			half _WavelengthHor;
			half _WaveIntensityHor;
			half _AmplitudeHor;
			half _SpeedHor;
			half _WavelengthVert;
			half _WaveIntensityVert;
			half _AmplitudeVert;
			half _SpeedVert; 

			const fixed one = 1;
			const fixed zero = 0;
			
			struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float4 color : COLOR;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float2 worldUv : TEXCOORD1;
				float4 time : TANGENT;
            };
			
            v2f vert (appdata v)
            {				
				float2 uvOffseted = v.uv.xy * _WiggleScale  + (sin(_WiggleSpeed* _Time.y) );
				float wiggleTex = tex2Dlod(_WiggleTex, float4(uvOffseted, 0, 0));

				float waveHor = 2 * UNITY_PI / _WavelengthHor * _WaveIntensityHor;
				float wiggle = wiggleTex * _WiggleIntensity;
				float waveVert = 2 * UNITY_PI / _WavelengthVert * _WaveIntensityVert;
				
                v2f o;
				v.vertex.x += _AmplitudeHor * v.color.r * sin(waveHor + (v.vertex.x - _SpeedHor * _Time.y));
				v.vertex.x -= wiggle * v.color.b * _WiggleIntensity;
				v.vertex.y += _AmplitudeVert * v.color.b * sin(waveVert + (v.vertex.y - _SpeedVert * _Time.y));
				v.vertex.y -= wiggle * v.color.b * _WiggleIntensity;
				o.vertex = UnityObjectToClipPos(v.vertex);
				
                o.worldUv  = CloudsUV(v.vertex);

				o.time = (_Time % 200) / 200;
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 allVegetation = tex2D(_MainTex, i.uv);
                
                //Clouds 
                fixed4 result = CloudsMix( CloudsAmount( i.worldUv, i.time * 200) , allVegetation);
                
                return result; 
            }
            ENDCG
        }
    }
	FallBack "NanoReality/Environment/TreesNoAnimation"
}
