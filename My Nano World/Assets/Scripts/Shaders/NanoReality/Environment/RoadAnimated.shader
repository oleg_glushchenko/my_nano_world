﻿Shader "NanoReality/Environment/Road Animated"
{
	Properties
	{
		_Color("Main Color", Color) = (1,1,1,1)
		_Color1("Second Color", Color) = (1,1,1,1)
		_MainTex ("Texture", 2D) = "white" {}
		_Speed("Speed", float) = 1
		_MainTexBias("Mip Bias (-1 to 1)", float) = -1.65
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				half4 animColor : COLOR;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			half4 _Color;
			half4 _Color1;
			half _Speed;
			uniform fixed _MainTexBias;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				half blendTime = 4 / _Speed*(abs(fmod(_Time, _Speed) - _Speed*0.5) - _Speed *0.25);
				o.animColor = lerp(_Color, _Color1, blendTime);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2Dbias(_MainTex, half4(i.uv.x, i.uv.y,0.0, _MainTexBias))*i.animColor;
				return col;
			}
			ENDCG
		}
	}
}
