﻿Shader "NanoReality/Environment/WaterLite/WaterMain"
{
	//vertexcolor.r - suppress waves distortion 
	//vertexcolor.g - foam area
	//vertexcolor.b - river flow area
	//vertexcolor.a - suppress sinWaves area (or if no grabpass - alpha)
	Properties
	{
		_ColorShore("ShoreColor", Color) = (1,1,1,1)
		_ColorWater("_ColorWater", Color) = (1,1,1,1)
		[NoscaleOffset]
		_MainTex("Main Texture", 2D) = "white" {} //waves, foam, blins, distortion 

		_UnderTex("UnderwaterTexture", 2D) = "white" {}
		_foamTex("Foam Texture", 2D) = "white" {}
		_foamTex2("Foam Texture 2", 2D) = "white" {}

		_ScaleWiggle("WiggleScale", Float) = 1.0
		[Space(20)]
		_ColorWaves("WavesColor", Color) = (1,1,1,1)
		_ScaleWaves("WavesScale", Float) = 1.0
		_SpeedWaves("WavesSpeed", Float) = 1.0
		_DistrWaves("WavesDistortionFactor", Float) = 1.0

		[Space(20)]
		_ColorFoam("FoamColor", Color) = (1,1,1,1)
		_ScaleFoam("FoamScale", Float) = 1.0
		_ScaleRatioFoam("FoamRatioScale", Float) = 1.0
		_ScreueFoam("ScreueFoam", Float) = 1.0
		_SpeedFoam("FoamSpeed", Float) = 1.0
		_DistrFoam("FoamDistortionFactor", Float) = 1.0
		_secondaryFoam("_secondaryFoam", Float) = .1

		[Space(20)]
		_DistrRefrMin("RefractioStrengthMin", Float) = .02
		_DistrRefrMax("RefractioStrengthMax", Float) = .002
		_ScaleRefraction("RefractionScale", Float) = 1.0
		_SpeedRefr("RefractioSpeed", Float) = 1.0

		[Space(20)]

		_RiverColor("River Color", Color) = (1,1,1,1)
		_ScaleRiver("Scale River", Float) = .8
		_SpeedRiver("Speed River", Float) = .8
		_RiverDistortionImpact("River Distortion Impact", Float) = .5
		_ScaleRatioRiver("Scale Ratio River", Float) = .8

		[Space(20)]
		[KeywordEnum(First,Second,Third,Fourth,Fifth)] _T("_SinWaveMixType", Float) = 0

		_WaveFormA("Wave Form A", Float) = 3
		_WaveFormB("Wave Form B", Float) = 5
		_WaveFormC("Wave Form C", Float) = 3
		_WaveFormD("Wave Form D", Float) = 2
		_WaveFormE("Wave Form E", Float) = 2
		_WavesTime("Waves Time", Float) = 2

		[Space(10)]
		_SinWavesSpeed("_SinWavesSpeed", Float) = 1
		_SinWavesAddOverlay("_SinWavesAddOverlay", Float) = .2
		_SinWavesAddRefrDistortion("_SinWavesAddRefrDistortion", Float) = .4
		_SinWavesAddDistortion("_SinWavesAddDistortion", Float) = .05
		_SinWavesFoamAddOverlay("_SinWavesFoamAddOverlay", Float) = .05
		_SinWavesFoamAddColor("_SinWavesFoamAddColor", Float) = .3

		[Space(10)]
		_FoamWaveTextureScaleX("_FoamWaveTextureScaleX", Float) = 1
		_FoamWaveTextureScaleY("_FoamWaveTextureScaleY", Float) = 3

		[Space(10)]
		_WaveFormCutA("_WaveFormCutA", Float) = .3
		_WaveFormCutB("_WaveFormCutB", Float) = .7

		[Space(10)]
		_FoamWaveTextureShiftY("_FoamWaveTextureShiftY", Float) = 0.3
		_FoamWaveTextureShiftYY("_FoamWaveTextureShiftYY", Float) = 0


		[Space(10)]
		[Toggle(ANIMATE_SIN_WAVE_FOAM)]
		_AnimateSinWaveFoamY("Animate Sin Wave Foam Y", Float) = 0

		[Space(20)]
		_WorldUVToGrabPosition("_WorldUVToGrabPosition", Float) = .1

		[Space(20)]
		_TransparentBlendingAmount("Transparent Blending Amount", Float) = .8

		[Toggle(USE_TRANSPARENT_BLENDING)]
		_UseTransparentBlending("Use Transparent Blending", Float) = 0


		[Toggle(USE_GRAB_PASS_BACKGROUND)]
		_UseGrabPassBackground("Use Grab Pass Background", Float) = 0


		[Toggle(DEBUG_TIME)]
		_DebugTime("DEBUG TIME", Float) = 0

	}

	SubShader
	{
		Tags { "RenderType" = "WaterLite" "Queue" = "Transparent+100" }

		LOD 50
		ZWrite Off
		ZTest Always

		Blend SrcAlpha OneMinusSrcAlpha

		GrabPass
		{
			"_BackgroundTexture"
		}
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag 
			#include "UnityCG.cginc"
			#include "../CGincludes/MerlinVertexTrix.cginc"
			#include "../CGincludes/MerlinUV.cginc"
			#include "../CGincludes/CloudsAnimation.cginc"

			#pragma shader_feature _T_FIRST _T_SECOND  _T_THIRD _T_FOURTH

			#pragma shader_feature ANIMATE_SIN_WAVE_FOAM
			#pragma shader_feature USE_TRANSPARENT_BLENDING
			#pragma shader_feature USE_GRAB_PASS_BACKGROUND
			#pragma shader_feature DEBUG_TIME
			#pragma shader_feature FLIP_WAVE_Y            

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float2 uv2 : TEXCOORD1;
				float2 uv3 : TEXCOORD2;
				float2 uv4 : TEXCOORD3;
				fixed4 color : COLOR;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float2 uv2 : TEXCOORD1;
				float2 uv3 : TEXCOORD2;
				half2 uv4: TEXCOORD3;
				float4 grabPos : TEXCOORD4;
				half2 screenUv: TEXCOORD5;
				half2 worldUv: TEXCOORD6;
				float4 vertex : SV_POSITION;
				fixed4 color : COLOR;
				float4 time : TANGENT;
			};

			sampler2D_half _MainTex;
			sampler2D_half _UnderTex;
			sampler2D_half _foamTex;
			sampler2D_half _foamTex2;
			sampler2D _BackgroundTexture;

			float4 _MainTex_ST;
			float4 _UnderTex_ST;
			float _ScaleWiggle;
			fixed4 _ColorShore;
			fixed4 _ColorWater;

			fixed4 _ColorWaves;
			float _ScaleWaves;
			float _SpeedWaves;
			float _DistrWaves;

			fixed4 _ColorFoam;
			float _ScaleFoam;
			float _ScaleRatioFoam;
			float _ScreueFoam;
			float _SpeedFoam;
			float _DistrFoam;
			float _secondaryFoam;
			float _DistrRefrMin;
			float _DistrRefrMax;
			float _ScaleRefraction;
			float _SpeedRefr;
			float _ScaleRiver;
			float _SpeedRiver;
			float _ScaleRatioRiver;
			float4 _RiverColor;
			float _TransparentBlendingAmount;
			float _RiverDistortionImpact;


			float _WaveFormA;
			float _WaveFormB;
			float _WaveFormC;
			float _WaveFormD;
			float _WaveFormE;
			float _WavesTime;
			float _WaveFormCutA;
			float _WaveFormCutB;
			float _SinWavesSpeed;
			float _SinWavesAddOverlay;
			float _SinWavesAddRefrDistortion;
			float _SinWavesAddDistortion;
			float _SinWavesFoamAddOverlay;
			float _SinWavesFoamAddColor;

			uniform float _CameraSize;
			float _WorldUVToGrabPosition;

			float _FoamWaveTextureScaleX;
			float _FoamWaveTextureScaleY;

			float _FoamWaveTextureShiftYY;
			float _FoamWaveTextureShiftY;

			const fixed one = 1;
			const fixed zero = 0;

			float2 OffsetUvY(float2 originalUv, float offsetY, fixed4 distortion, float distortionFactor, float4 time)
			{
				float uvResultX = originalUv.x;
				float uvResultY = (originalUv.y + distortion / distortionFactor + time.w * offsetY) % 2;
				float2 result = float2(uvResultX, uvResultY);
				return result;
			}

			float2 OffsetYnoDistr(float2 originalUv, float speed, float4 time) {
				float uvResultX = originalUv.x;
				float uvResultY = (originalUv.y + time.w * speed) % 2;
				float2 result = float2(uvResultX, uvResultY);
				return result;
			}

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);

				o.uv = TRANSFORM_TEX(v.uv, _UnderTex);
				o.uv2 = TRANSFORM_TEX(v.uv2, _MainTex);
				o.uv3 = TRANSFORM_TEX(v.uv3, _MainTex);
				o.uv4 = TRANSFORM_TEX(v.uv4, _MainTex);

				o.color = v.color;

				o.worldUv = CloudsUV(v.vertex);
				o.time = (_Time % 200) / 200;

				o.grabPos = ComputeGrabScreenPos(o.vertex);
				o.screenUv = ComputeScreenPos(o.vertex);
				
				return o;
			}

			float3 CreateSinWaves(float2 foamUV, float time)
			{

				float sw1 = foamUV.y * _WaveFormB * _WaveFormA - time * 20;
				float sw3 = foamUV.x * _WaveFormA + time;

				float sinWave1 = sin(sw1);
				float sinWave3 = sin(sw3);

				float sinWave2 = sin(foamUV.y * _WaveFormC + time);
				float sinWave4 = sin(foamUV.x * _WaveFormD - time);


				float sinWaves = sinWave1 * sinWave3 * sinWave2 * sinWave4;

				float asinWave1 = fmod(abs(sw1), UNITY_PI) / UNITY_PI;
				float asinWave3 = smoothstep(0, 1, fmod(abs(sw3) , UNITY_PI) / UNITY_PI);

				#if FLIP_WAVE_Y
					float2 wavesCoordinates = float2(asinWave3 * _FoamWaveTextureScaleX,asinWave1 * _FoamWaveTextureScaleY);
				#else
					float2 wavesCoordinates = float2(asinWave3 * _FoamWaveTextureScaleX,1 - asinWave1 * _FoamWaveTextureScaleY);
				#endif

				wavesCoordinates.y = (wavesCoordinates.y) * (wavesCoordinates.y);

				return float3(wavesCoordinates.x, wavesCoordinates.y, sinWaves);
			}

			float2 ShoreLine(float2 foamUV,  float2 waveUV, float inColor, float distortion, float amount, float shoreAmount, float time)
			{

				float2 foamUvscaled = foamUV * _ScaleFoam * float2(_ScaleRatioFoam,1);
				float2 foamUvOffset = OffsetUvY(foamUvscaled, _SpeedFoam, distortion, _DistrFoam, time * 40);
				foamUvOffset += float2(0, -sin(foamUvOffset.x * _ScreueFoam));

				fixed foam = inColor;

				#if ANIMATE_SIN_WAVE_FOAM
					foam = (foam * foam) * tex2D(_MainTex,distortion * .02 + float2(waveUV.x * 2,waveUV.y * .4 - fmod(_Time.x * .6,1))).g * (amount)* _ColorFoam.a;
				#endif

				 foam += tex2D(_MainTex, foamUvOffset).g * _ColorFoam.a * shoreAmount;

				float2 foamUvOffset2 = OffsetUvY(foamUvscaled, _SpeedFoam, distortion, _DistrFoam, -time * 40 + .5);
				foamUvOffset2 += float2(.7,sin(foamUvOffset.x * _ScreueFoam));
				fixed foam2 = tex2D(_MainTex, foamUvOffset2).g * (shoreAmount)* _ColorFoam.a;



				return float2(foam, foam2);
			}


			float2 MixSinWavesFoamTextures(float2 uv, float2 uv2, float2 distrtion, float amount, float time)
			{
				float foamColor3 = ((tex2D(_foamTex2, uv * float2(1,.5) + distrtion + float2(-_Time.y * .1 - uv2.x * .5 ,_FoamWaveTextureShiftYY)).r - .5) * 2) * amount * amount;
				float foamColor1 = tex2D(_foamTex, uv + distrtion + float2(_Time.x ,_FoamWaveTextureShiftY)).r * amount;
				float foamColor2 = tex2D(_foamTex, float2(1 - uv.x, uv.y) + distrtion + float2(_Time.x, _FoamWaveTextureShiftY)).r * amount;


				#if(_T_FIRST)
					float foamColor = max(foamColor2,foamColor2) * ((foamColor3 + .5) * (foamColor3 + .5) * .25);
					return float2(foamColor3, foamColor);
				#endif

				#if(_T_SECOND)		
					return float2(foamColor1, foamColor2 * foamColor3);
				#endif

				#if(_T_THIRD)	
				 return float2(foamColor2, foamColor3 * foamColor3);
				#endif

				#if(_T_FOURTH)
					float foamColor = min(foamColor1 * foamColor2,foamColor3 * .5 + .5) + .3;
				 return float2(foamColor * foamColor, foamColor3 * foamColor3);
				#endif

				return float2(foamColor2 * foamColor2, foamColor3 * foamColor3);
			}


			fixed4 frag(v2f i) : SV_Target
			{
				float4 animMap = i.color;
				float4 time = i.time * 200;
				
				float wavesAmount = (1 - animMap.g);


				#if(DEBUG_TIME)
					float wavesTime = _WavesTime;
				#else
					float wavesTime = _Time.x * _SinWavesSpeed;
				#endif


				#ifdef USE_TRANSPARENT_BLENDING 

					#ifdef USE_GRAB_PASS_BACKGROUND

						float alpha = (1 - smoothstep(_TransparentBlendingAmount, 1, animMap.g));
					#else		                    
						float alpha = 1 - animMap.r;
					#endif 

				#else
					float alpha = 1;
				#endif

				// animated coordinates of the common sine field
				float wavesWave = sin(i.uv4.y * _WaveFormE + 21) / 2;
				float2 foamUV = float2 (i.uv4.x + wavesWave, i.uv4.y * 2);

				float3 tmp1 = CreateSinWaves(foamUV,wavesTime);

				// wave height at this point
				float sinWaves = tmp1.z;

				// coordinates for each wave
				float2 wavesCoordinates = tmp1.xy;

				float notShoreMask = smoothstep(.8, .95,  i.uv4.y);

				sinWaves = sinWaves + notShoreMask / 3;

				#if(USE_GRAB_PASS_BACKGROUND)
					float notRiver = clamp(0, 1, 1 - animMap.b) * (animMap.a);
				#else
					float notRiver = 0;
				#endif

				#if(USE_GRAB_PASS_BACKGROUND)	
					float suppressWaves = (1 - animMap.r);
				#else				
					float suppressWaves = 0;
				#endif

				// the tops of the sinusoidal field - where we draw the foam
				float foamAmount = smoothstep(_WaveFormCutA, _WaveFormCutB,  sinWaves) * notRiver;

				float distrAmount = smoothstep(.7,.1,animMap.g);


				float2 distrScaled = i.uv * _ScaleWiggle;
				float distortion = tex2D(_MainTex, distrScaled).b;

				// river
				float2 riverUvScaled = i.uv3 * _ScaleRiver * float2(_ScaleRatioRiver,1);
				float2 riverUvOffset = OffsetUvY(riverUvScaled, _SpeedRiver, distortion, _DistrFoam, time);

				// river distortion
				fixed river = tex2D(_MainTex, riverUvOffset).b * _RiverDistortionImpact * animMap.b;


				// distorted uv - cloud reflections
				float2 wavesUvscaled = lerp(i.screenUv ,i.worldUv,.75) * _ScaleWaves;

				// combine the distortion of the river, with the distortion of the sea - and we make a uv offset out of it

				float2 cloudsReflectionUvOffset = OffsetUvY(wavesUvscaled,	_SpeedWaves, lerp(distortion,river,animMap.b), _DistrWaves,_Time.x) - (sinWaves)* _SinWavesAddDistortion * notRiver;

				// main distortion, used for - bottom and spools on waves
				float2 refrSacaled = i.uv * _ScaleRefraction;
				float2 distrOffset = OffsetYnoDistr(refrSacaled, _SpeedRefr, time);
				fixed distrOfsetted = tex2D(_MainTex, distrOffset).b * distrAmount + (sinWaves)* _SinWavesAddRefrDistortion;

				float2 underOffsetProj = (distrOfsetted * 2 - 1) * lerp(_DistrRefrMin,_DistrRefrMax,_CameraSize) * distrOfsetted + river / 100;


				#if(USE_GRAB_PASS_BACKGROUND)
					// Bottom - take from grabpass
					float4 underGrabPos = i.grabPos + float4(underOffsetProj * suppressWaves,0,0);
					fixed4 underColor = tex2Dproj(_BackgroundTexture, underGrabPos);
				#else
					// Bottom - take from Texture
					float2 underGrabPos = i.uv + float2(underOffsetProj);
					fixed4 underColor = tex2D(_UnderTex, underGrabPos);
				#endif

				// apply illumination from the sinus field to the bottom, as well as their tops separately
				underColor += underColor * (sinWaves)* _SinWavesAddOverlay * notRiver + underColor * (foamAmount)* _SinWavesFoamAddOverlay;
				float2 foamColor = MixSinWavesFoamTextures(wavesCoordinates,i.uv4,distrOfsetted * .1,foamAmount ,wavesTime);

				// reflections from clouds		
				fixed cloudsReflections = tex2D(_MainTex, cloudsReflectionUvOffset).r * wavesAmount * _ColorWaves.a;
				fixed3 cloudsReflectionsColored = cloudsReflections * _ColorWaves;

				// surf at the shore line
				float2 foam = ShoreLine(i.uv2,  wavesCoordinates, foamColor.x, distortion, foamAmount, (1 - wavesAmount), wavesTime);

				// foam - surf + rollback + rolls on the waves

				fixed3 foamColored = foam.x * _ColorFoam + (foam.y * _ColorFoam) * _secondaryFoam + foamColor.y * _SinWavesFoamAddColor;

				// foam + cloud reflections
				fixed3 details = cloudsReflectionsColored + foamColored;
				fixed detailsMask = 1 - (cloudsReflections + foam.x);

				// minus details to avoid overexposure
				underColor = underColor * detailsMask;

				//if not grabpass we put clouds (for grabpass they are unneeded because they are in the background)
				#if(!USE_GRAB_PASS_BACKGROUND)					 
					   underColor = CloudsMix(CloudsAmount(i.worldUv, i.time * 200) , underColor);
				#endif

				// total
				fixed4 allWater = fixed4(underColor.rgb + details + river * _RiverColor.rgb , alpha);

				return allWater;
			}
		ENDCG
		}
	}
}
