Shader "NanoReality/Environment/WaterLite/FountainDynamicWater"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _DistortionTex ("Distortion Texture", 2D) = "white" {}
        
        _Color ("Color", COLOR) = (1,1,1,1)
        _AddColor ("Add Color", COLOR) = (1,1,1,1)
        
		_X1("X1", Float) = 0
		_Y1("Y1", Float) = 0
		_M1("M1", Float) = 1
		_A1("A1", Float) = 1
		_X2("X2", Float) = 0
		_Y2("Y2", Float) = 0
		_M2("M2", Float) = 1
		_A2("A2", Float) = 1
		
		_T("T", Range(0,1)) = 1
		
        [Toggle(_TIMER_MODE)]
		_TimerMode("Timer Mode", Range(0,1)) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100
       ZWrite OFF

        Pass
        {
            CGPROGRAM  
            #pragma vertex vert
            #pragma fragment frag

            #pragma shader_feature _TIMER_MODE

            #include "UnityCG.cginc"

            struct appdata
            {
                float3 vertex : POSITION;
                float4 color : COLOR;
                float4 uv : TEXCOORD0; 
            };

            struct v2f
            {
                float4 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float4 color : COLOR;
                float4 mainParam: TEXCOORD1;
                float4 masks: TEXCOORD2;
            };

            sampler2D _MainTex;
            sampler2D _DistortionTex;
            float4 _MainTex_ST;
            float4 _Color;
            float4 _AddColor;
            float _X1;
            float _X2;
            float _Y1;
            float _Y2;
            float _M1;
            float _M2;
            float _A1;
            float _A2;            
            float _T;
            
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv.xy = TRANSFORM_TEX(v.uv.xy, _MainTex);
                o.uv.z = v.uv.z;
                o.uv.w = v.uv.w;
                o.color = v.color;
                
                #ifdef _TIMER_MODE 
                     float t =  _T;
                #else
                    float t = v.uv.z;                    
                #endif
                
                o.mainParam = float4(
	                lerp(_X1,_X2,t),
					lerp(_Y1,_Y2,t),
					lerp(_A1,_A2,t),
					lerp(_M1,_M2,t)
                );
                
                o.masks = float4(
	                smoothstep(.5,.45,abs((v.uv.x-.5)))*smoothstep(.5,.45,abs((v.uv.y-.5))), 
	                v.uv.x-.5 , v.uv.y,
	                lerp(_M1,_M2,t)
                );               
                
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {                
                #ifdef _TIMER_MODE 
                     float t =  _T;
                #else
                    float t = i.uv.z;                    
                #endif
                
                float mix=0;
                               
				float o = smoothstep(.25,.05,mix);
				float oo = _Color;				
				float mask = 1 - length((i.uv.xy-.5)*2);				
				float2 uv = i.uv.xy + i.uv.w*100+ sin(i.uv.x*i.uv.y*t*5)*t*float2(.7,1);
				float m = (  sin(uv.x*6+i.uv.z)/4+sin(uv.y*7)/4+.5  );
				m += (  sin(uv.x*5+i.uv.z)/4+sin(uv.y*3)/4+.5 );
				float c = m*m * mask;
				clip(c-(.3+t));				
				return _AddColor+lerp( _Color*c  , i.color*c,i.color.a); 
            }
            ENDCG
        }
    }
}
