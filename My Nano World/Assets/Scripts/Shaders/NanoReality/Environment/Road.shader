﻿Shader "NanoReality/Environment/Road"
{
	Properties
	{
		_Color("Main Color", Color) = (1,1,1,1)
		_MainTex ("Texture", 2D) = "white" {}
		_MainTexBias("Mip Bias (-1 to 1)", float) = -1.65
	}
	SubShader
	{
		Tags { "RenderType"="ReplacementOpaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			
			#include "UnityCG.cginc"
			#include "../CGincludes/MerlinVertexTrix.cginc"
            #include "../CGincludes/MerlinUV.cginc"
            #include "../CGincludes/CloudsAnimation.cginc"
			
			sampler2D _MainTex;
			float4 _MainTex_ST;
			fixed4 _Color;
			uniform fixed _MainTexBias;
			
			const fixed one = 1;
			const fixed zero = 0;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				half2 worldUv: TEXCOORD3;
				float4 time : TANGENT;
			};

			

			v2f vert (appdata v)
			{				
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex); 
                o.worldUv  = CloudsUV(v.vertex);
				o.time = (_Time % 200) / 200;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2Dbias(_MainTex, half4(i.uv.x, i.uv.y,0.0, _MainTexBias))*_Color;
				
				//Clouds 
                fixed4 result = CloudsMix( CloudsAmount( i.worldUv, i.time * 200) , col); 
                return result; 
			}
			ENDCG
		}
	}
}
