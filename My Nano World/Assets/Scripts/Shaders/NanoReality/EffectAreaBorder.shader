﻿Shader "Custom/Transparent/AreaShaderBorder"
{
	Properties
    {
       _Color ("Main Color", Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags{ "Queue" = "Transparent-1" "RenderType"="ReplacementAreaBorder"}

        Blend SrcAlpha OneMinusSrcAlpha
        Lighting Off
        Cull Off
        ZTest Always
        ZWrite Off
        Cull back

        Pass
        {

            Offset -1, -1
            Blend SrcAlpha OneMinusSrcAlpha
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"


            fixed4 _Color;


            struct v2f
            {
                float4 pos : SV_POSITION;
                float4 color : COLOR;
            };

            v2f vert(appdata_full i)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(i.vertex);
                o.color = i.color;
                return o;
            }

            half4 frag(v2f i) : COLOR
            {
                float4 c = _Color*i.color;
                c.a *= (1 - saturate(sin(_Time.z)) * 0.4);
                return c;
            }
            ENDCG
        }
    }
}
