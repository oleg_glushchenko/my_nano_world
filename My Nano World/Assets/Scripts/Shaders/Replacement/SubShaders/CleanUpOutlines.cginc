#if !defined(CLEANUPOUTLINES_INCLUDED)
    #define CLEANUPOUTLINES_INCLUDED     
    
    #pragma target 2.0
    #include "UnityCG.cginc"
     
    float4 _Color;	
    fixed _Clipping;
    	
    sampler2D _BackgroundTexture;
            
    struct appdata
    {
        float4 vertex   : POSITION;
        float2 texcoord : TEXCOORD0;
    };
    
    struct v2f
    {
        float4 vertex   : SV_POSITION;
        float2 texcoord : TEXCOORD0; 
        fixed4 color : COLOR;
        float4 grabPos : TEXCOORD1; 
    };
    
        
    v2f SpriteVert(appdata v)
    {
        v2f o;
        o.vertex = UnityObjectToClipPos(v.vertex);
        o.texcoord = v.texcoord; 
        o.grabPos = ComputeGrabScreenPos(o.vertex);        
        return o;
    }
    
    float4 GrabTransparancy(float4 color, float4 GrabPos)
    {
        half4 bgcolor = tex2Dproj(_BackgroundTexture, GrabPos);
        fixed4 coloredTex = fixed4(color.rgb * (_Color.rgb * _Color.a), color.a);    
            	
		bgcolor *= (1 - _Color.a);
		coloredTex.rgb *= _Color.a;
		coloredTex.rgb += bgcolor.rgb;
		
        return coloredTex;
    }
    
    fixed4 SpriteFrag(v2f i) : Color
    { 
        half4 color = tex2D( _MainTex, i.texcoord );    
        float4 tinted = GrabTransparancy(color, i.grabPos);
        clip(tinted.a-_Clipping);    
        return tinted;
    }
#endif