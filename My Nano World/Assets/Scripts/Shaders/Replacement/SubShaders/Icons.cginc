#if !defined(ICONS_INCLUDED)
    #define ICONS_INCLUDED 
    
    
    #pragma target 2.0
    #include "UnityCG.cginc"
    
    struct appdata
    {
        float4 vertex : POSITION;
        float2 uv : TEXCOORD0;
    };
    
    struct v2f
    {
        float2 uv : TEXCOORD0;
        float4 vertex : SV_POSITION;
        float2 screenUv : TEXCOORD1;
    };
    
    v2f vert(appdata v)
    {
        v2f o;
        o.vertex = UnityObjectToClipPos(v.vertex);
        o.uv = TRANSFORM_TEX(v.uv, _MainTex);
        o.screenUv = GetScreenSpace(o.vertex);
        return o;
    }
    
    fixed4 frag(v2f i) : SV_Target
    {
        fixed4 col = tex2D(_MainTex, i.uv);
        return Grayscale(col, i.screenUv);
    } 
#endif
