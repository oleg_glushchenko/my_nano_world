#if !defined(COMMON_INCLUDED)
    #define COMMON_INCLUDED     
    
    sampler2D _MainTex;
    float4 _MainTex_ST;
    uniform float _ScreenSpaceXscale;
    uniform float _ScreenSpaceYscale;
    uniform float _VignetteMax;
    uniform float _VignetteMin;
    uniform float _VignettePow;    
    
    float2 GetScreenSpace(float4 vertpos)
    {
        float screenUvX = vertpos.x / vertpos.w * _ScreenSpaceXscale;
        float screenUvY = vertpos.y / vertpos.w * _ScreenSpaceYscale;    
        return float2(screenUvX, screenUvY);
    }
    
    float4 Grayscale(fixed4 c, float2 screenUv)
    {
        float distFromCenter = distance(screenUv.xy, float2(0, 0));
        float vignette = smoothstep(_VignetteMax, _VignetteMax - _VignetteMin, pow(distFromCenter, _VignettePow));
    
        float4 color = c;
        color.rgb = dot(color.rgb, float3(0.3, 0.59, 0.11));
        color.rgb *= 1.9;
        color.rgb = (color.rgb - 0.5) * 0.5 + 0.5;
    
        float q = 0.9;
        float3 mask = float3(0.184, 0.6, 1);
    
        color.rgb = q * color.rgb + (1 - q) * mask;
        fixed3 vignettedColor = saturate(color.rgb * vignette);
    
        return fixed4(vignettedColor.rgb, color.a);
    }
#endif