#if !defined(AOEPLANE_INCLUDED)
    #define AOEPLANE_INCLUDED 
    
    #pragma target 2.0
    #pragma multi_compile_instancing
    #pragma multi_compile_local _ PIXELSNAP_ON
    #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
    
    #ifndef UNITY_SPRITES_INCLUDED
    #define UNITY_SPRITES_INCLUDED
    
    #include "UnityCG.cginc"

	#ifdef UNITY_INSTANCING_ENABLED

        UNITY_INSTANCING_BUFFER_START(PerDrawSprite)
        // SpriteRenderer.Color while Non-Batched/Instanced.
        UNITY_DEFINE_INSTANCED_PROP(fixed4, unity_SpriteRendererColorArray)
        // this could be smaller but that's how bit each entry is regardless of type
        UNITY_DEFINE_INSTANCED_PROP(fixed2, unity_SpriteFlipArray)
        UNITY_INSTANCING_BUFFER_END(PerDrawSprite)
    
        #define _RendererColor  UNITY_ACCESS_INSTANCED_PROP(PerDrawSprite, unity_SpriteRendererColorArray)
        #define _Flip           UNITY_ACCESS_INSTANCED_PROP(PerDrawSprite, unity_SpriteFlipArray)
    
        #endif // instancing
    
        CBUFFER_START(UnityPerDrawSprite)
            #ifndef UNITY_INSTANCING_ENABLED
            fixed4 _RendererColor;
            fixed2 _Flip;
            #endif
            float _EnableExternalAlpha;
        CBUFFER_END
    
        // Material Color.
        fixed4 _Color;
    
        struct appdata_t
        {
            float4 vertex   : POSITION;
            float4 color    : COLOR;
            float2 texcoord : TEXCOORD0;
            UNITY_VERTEX_INPUT_INSTANCE_ID
        };
    
        struct v2f
        {
            float4 vertex   : SV_POSITION;
            fixed4 color : COLOR;
            float2 texcoord : TEXCOORD0;
            float4 time : TANGENT;
            UNITY_VERTEX_OUTPUT_STEREO
        };
    
        sampler2D _AlphaTex;
        half _PulseSpeed;
        fixed4 _ColorOut;
    
        inline float4 UnityFlipSprite(in float3 pos, in fixed2 flip)
        {
            return float4(pos.xy * flip, pos.z, 1.0);
        }
    
        v2f SpriteVert(appdata_t v)
        {
            v2f o;
            UNITY_SETUP_INSTANCE_ID(v);
            UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
    
            float4 vertex = UnityObjectToClipPos(float4(0.0, 0.0, 0.0, 1.0));
            float4 screenPos = ComputeScreenPos(vertex);
            o.vertex = UnityFlipSprite(v.vertex, _Flip);
            o.vertex = UnityObjectToClipPos(o.vertex);
            o.vertex.z -= screenPos.x / 1000;
            o.texcoord = v.texcoord;
            o.time = (_Time % 200) / 200;
            o.color = v.color * _Color * _RendererColor;
            #ifdef PIXELSNAP_ON
                o.vertex = UnityPixelSnap(o.vertex);
            #endif
            return o;
        }
    
        fixed4 SampleSpriteTexture(float2 uv)
        {
            fixed4 color = tex2D(_MainTex, uv);
    
            #if ETC1_EXTERNAL_ALPHA
                fixed4 alpha = tex2D(_AlphaTex, uv);
                color.a = lerp(color.a, alpha.r, _EnableExternalAlpha);
            #endif
            return color;
        }
    
        fixed4 SpriteFrag(v2f i) : SV_Target
        {
            fixed4 c = SampleSpriteTexture(i.texcoord) * i.color;
            float isBorder = step(c.x * c.y * c.z, .1) * c.a;
            float4 border = isBorder * _ColorOut;
            c *= step(.1,c.x * c.y * c.z); // cuting out black border
            c.rgb *= c.a;
            fixed4 pulsColor = fixed4(c.rgb * 0.1 + _Color.rgb * c.a, c.a);
            half oscilate = (sin(200 * i.time.y * _PulseSpeed) + 1) / 2;
            half4 oscilatedColor = lerp(c, pulsColor, oscilate) + border;
            return   oscilatedColor;
        }
    #endif // UNITY_SPRITES_INCLUDED
#endif