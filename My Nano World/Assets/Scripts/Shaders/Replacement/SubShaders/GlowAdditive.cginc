#if !defined(GLOWADDITIVE_INCLUDED)
    #define GLOWADDITIVE_INCLUDED 

	#pragma multi_compile_particles
	#include "UnityCG.cginc"

	fixed4 _TintColor;
	float _CutOutLightCore;

	struct appdata_t {
		float4 vertex : POSITION;
		fixed4 color : COLOR;
		float2 texcoord : TEXCOORD0;
	};

	struct v2f {
		float4 vertex : POSITION;
		fixed4 color : COLOR;
		float2 texcoord : TEXCOORD0;
        #ifdef SOFTPARTICLES_ON
        float4 projPos : TEXCOORD1;
        #endif
	};
	
	v2f vert(appdata_t v)
	{
		v2f o;
		o.vertex = UnityObjectToClipPos(v.vertex);
		
        #ifdef SOFTPARTICLES_ON
            o.projPos = ComputeScreenPos(o.vertex);
            COMPUTE_EYEDEPTH(o.projPos.z);
        #endif
        
		o.color = v.color;
		o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
		return o;
	}

	sampler2D _CameraDepthTexture;
	float _InvFade;

	fixed4 frag(v2f i) : COLOR
	{
		#ifdef SOFTPARTICLES_ON
            float sceneZ = LinearEyeDepth(UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos))));
            float partZ = i.projPos.z;
            float fade = saturate(_InvFade * (sceneZ - partZ));
            i.color.a *= fade;
		#endif

		fixed4 tex = tex2D(_MainTex, i.texcoord);
		return  tex.g * _TintColor * i.color * 3;
	}
#endif