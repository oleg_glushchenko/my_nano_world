#if !defined(MAP_INCLUDED)
    #define MAP_INCLUDED 

	#pragma target 2.0
	#include "UnityCG.cginc"

	#include "../../NanoReality/CGincludes/MerlinVertexTrix.cginc"
	#include "../../NanoReality/CGincludes/MerlinUV.cginc"
	#include "../../NanoReality/CGincludes/MerlinViewDirPos.cginc"
	#include "../../NanoReality/CGincludes/CloudsAnimation.cginc"

	sampler2D_half _BgTex;
	fixed4 _BgTex_ST;
	sampler2D_half _LayerMask;
	fixed4 _LayerMask_ST;
	half4 _CloudsTex_ST;

	half _Height;
	half _VertOffset;
	half _HorOffset;
	half _VertScale;
	half _HorScale;

	const fixed one = 1;
	const fixed zero = 0;
	
	struct appdata
	{
		half4 vertex    : POSITION;
		half3 normal    : NORMAL;
		half2 uv		: TEXCOORD0;
		half3 viewDir: TEXCOORD2;
	};

	struct v2f
	{
		half2 uv : TEXCOORD0;
		half2 uv2 : TEXCOORD1;
		half4 vertex : SV_POSITION;
		half3 viewDir: TEXCOORD2;
		half2 worldUv: TEXCOORD3;
		float4 time : TANGENT;
		float2 screenUv : TEXCOORD4;
	};

	v2f vert(appdata v)
	{
		fixed4 worldPos = mul(unity_ObjectToWorld, v.vertex);
		v2f o;
		o.vertex = UnityObjectToClipPos(v.vertex);
		o.viewDir = normalize(UnityWorldSpaceViewDir(worldPos.xyz));
		o.uv = TRANSFORM_TEX(v.uv, _MainTex);
		half2 changedUV2 = half2(v.uv.x * _HorScale + _HorOffset, v.uv.y * _VertScale + _VertOffset);
		half2 p = ParallaxMapping_mp(changedUV2, float2(0, 1), o.viewDir, _Height);
		o.uv2 = TRANSFORM_TEX(p, _BgTex);
		o.worldUv = CloudsUV(v.vertex);
		o.time = (_Time % 200) / 200;

		o.screenUv = GetScreenSpace(o.vertex);
		return o;
	}

	fixed4 frag(v2f i) : SV_Target
	{ 
		fixed4 foregroundTex = tex2D(_MainTex, i.uv);
		fixed3 foregroundColor = foregroundTex.rgb;
		fixed3 foregroundMask = tex2D(_LayerMask, i.uv);
		fixed3 foreground = foregroundColor * foregroundMask;

		fixed3 backgroundColor = tex2D(_BgTex, i.uv2).rgb;  
		fixed backgroundMask = 1 - foregroundMask;
		fixed3 background = backgroundColor * backgroundMask;
		 
		fixed4 result = fixed4(background + foreground, one);
		result.a = min(
				pow(clamp((i.uv.x) * 512 - .2, 0, 1), 2),
				pow(clamp((1 - i.uv.y) * 512 + .5, 0, 1), 4)
				); 
		return Grayscale(result, i.screenUv);
	}
#endif