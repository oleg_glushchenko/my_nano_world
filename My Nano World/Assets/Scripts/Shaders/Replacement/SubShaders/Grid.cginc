#if !defined(GRID_INCLUDED)
    #define GRID_INCLUDED 

	#pragma target 3.0
	#include "UnityCG.cginc"
	uniform fixed4 _Color;
	uniform float _MainTexBias;

	struct v2f
	{
		float4 pos : SV_POSITION;
		float2 uv : TEXCOORD0;
	};

	v2f vert(appdata_base i)
	{
		v2f o;
		o.uv = TRANSFORM_TEX(i.texcoord, _MainTex);
		o.pos = UnityObjectToClipPos(i.vertex);
		return o;
	}

	half4 frag(v2f i) : COLOR
	{
		fixed4 c = tex2Dbias(_MainTex, half4(i.uv.x, i.uv.y,0.0, _MainTexBias));
		return c * _Color;
	}

#endif