#if !defined(LOCKEDSECTOR_INCLUDED)
    #define LOCKEDSECTOR_INCLUDED 
    
    #include "UnityCG.cginc"
	struct appdata
	{
		float4 vertex : POSITION;
		float2 uv : TEXCOORD0;
	};

	struct v2f
	{
		float2 uv : TEXCOORD0;
		float4 pos : SV_POSITION;
	};

	fixed4 _Color;
	uniform float _MainTexBias;

	v2f vert(appdata v)
	{
		v2f o;
		o.uv = TRANSFORM_TEX(v.uv, _MainTex);
		o.pos = UnityObjectToClipPos(v.vertex);
		return o;
	}

	fixed4 frag(v2f i) : SV_Target
	{
		return tex2D(_MainTex, i.uv) * _Color;
	}

#endif