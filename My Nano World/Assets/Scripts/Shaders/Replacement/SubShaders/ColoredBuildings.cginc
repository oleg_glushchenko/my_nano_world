#if !defined(COLOREDBUILDINGS_INCLUDED)
    #define COLOREDBUILDINGS_INCLUDED    
    
    #pragma target 2.0    
    #include "UnityCG.cginc"
	#ifdef UNITY_INSTANCING_ENABLED

	UNITY_INSTANCING_BUFFER_START(PerDrawSprite)
	// SpriteRenderer.Color while Non-Batched/Instanced.
	UNITY_DEFINE_INSTANCED_PROP(fixed4, unity_SpriteRendererColorArray)
	// this could be smaller but that's how bit each entry is regardless of type
	UNITY_DEFINE_INSTANCED_PROP(fixed2, unity_SpriteFlipArray)
	UNITY_INSTANCING_BUFFER_END(PerDrawSprite)

	#define _RendererColor  UNITY_ACCESS_INSTANCED_PROP(PerDrawSprite, unity_SpriteRendererColorArray)
	#define _Flip           UNITY_ACCESS_INSTANCED_PROP(PerDrawSprite, unity_SpriteFlipArray)

	#endif // instancing

	CBUFFER_START(UnityPerDrawSprite)
        #ifndef UNITY_INSTANCING_ENABLED
        fixed4 _RendererColor;
        fixed2 _Flip;
        #endif
        float _EnableExternalAlpha;
	CBUFFER_END
	
	// Material Color.
	fixed4 _Color;
	fixed _Clipping;

	struct appdata_t
	{
		float4 vertex   : POSITION;
		float4 color    : COLOR;
		float2 texcoord : TEXCOORD0;
		UNITY_VERTEX_INPUT_INSTANCE_ID
	};

	struct v2f
	{
		float4 vertex   : SV_POSITION;
		fixed4 color : COLOR;
		float2 texcoord : TEXCOORD0;
		float4 grabPos : TEXCOORD1;
		UNITY_VERTEX_OUTPUT_STEREO
	};

	sampler2D _AlphaTex;
	fixed4 _ColorOut;

	inline float4 UnityFlipSprite(in float3 pos, in fixed2 flip)
	{
		return float4(pos.xy * flip, pos.z, 1.0);
	}

	v2f SpriteVert(appdata_t v)
	{
		v2f o;

		UNITY_SETUP_INSTANCE_ID(v);
		UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

		o.vertex = UnityFlipSprite(v.vertex, _Flip);
		o.vertex = UnityObjectToClipPos(o.vertex);
		o.texcoord = v.texcoord;
		o.grabPos = ComputeGrabScreenPos(o.vertex);
		o.color =  _Color * _RendererColor;

        #ifdef PIXELSNAP_ON
            o.vertex = UnityPixelSnap(o.vertex);
        #endif
        
		return o;
	}

	fixed4 SampleSpriteTexture(float2 uv)
	{
		fixed4 color = tex2D(_MainTex, uv);
		return color;
	}
	
	fixed4 SpriteFrag(v2f i) : SV_Target
	{
		fixed4 tex = tex2D(_MainTex, i.texcoord);
		fixed4 coloredTex = fixed4(tex.rgb, tex.a);
		tex.a = pow(tex.a, 12);
	    clip(tex.a-_Clipping);
		return fixed4( coloredTex.rgb, tex.a);
	}
#endif