#if !defined(GRAYSCALEBUILDINGS_INCLUDED)
    #define GRAYSCALEBUILDINGS_INCLUDED
    
    #include "UnityCG.cginc"
    
    uniform fixed4 _Color;
    uniform float _MainTexBias;
    
    struct v2f
    {
        float4 pos : SV_POSITION;
        float2 uv : TEXCOORD0;
        float2 screenUv : TEXCOORD1;
    };
    
    v2f vert(appdata_base i)
    {
        v2f o;
        o.uv = TRANSFORM_TEX(i.texcoord, _MainTex);
        o.pos = UnityObjectToClipPos(i.vertex);
        o.screenUv = GetScreenSpace(o.pos);
        return o;
    }
    
    half4 frag(v2f i) : COLOR
    {
        return  Grayscale(tex2Dbias(_MainTex, half4(i.uv.x, i.uv.y,0.0, _MainTexBias)) * _Color, i.screenUv);
    } 
#endif