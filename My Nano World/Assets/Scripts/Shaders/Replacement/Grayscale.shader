﻿Shader "Hidden/Replacement/Grayscale"
{
    CGINCLUDE
		#include "SubShaders/Common.cginc"
    ENDCG

	// OPAQUE - roads
	SubShader
	{
		Tags { "RenderType" = "ReplacementOpaque" }
		LOD 100
		Pass
		{
			CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
                #include "SubShaders/Opaque.cginc"
			ENDCG
		}
	}

    //LockedSector - shaded, interior
    SubShader
    {
        Tags { "RenderType"="LockedSector" }
        LOD 100
        ZWrite off
        Pass
        {
            Cull back
            Blend SrcAlpha OneMinusSrcAlpha
            Lighting Off
            CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
                #include "SubShaders/LockedSector.cginc"
            ENDCG
        }
    }

    // Transparent - palms and bushes
    SubShader
    {
        Tags { "RenderType"="ReplacementTransparent" "Queue"="Transparent" }
        Cull back
        Blend SrcAlpha OneMinusSrcAlpha
        Lighting Off
        LOD 100
		ZWrite Off
		ZTest Less
        Pass
        {
            CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
                #include "SubShaders/Transparent.cginc"
            ENDCG
        }
    }

	// GRAYSCALE BUILDING - all non-selected and inactive buildings
	SubShader
	{
		Tags{ "Queue" = "Transparent" "RenderType" = "ReplacementBuilding"}
		Cull Off
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
		Lighting Off
		Pass
		{
			CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
                #include "SubShaders/GrayscaleBuildings.cginc"
			ENDCG
		}
	}

    //BUILDING OUTLINE COLORED - Houses that may or may not fall under the AOE frame are yellow or red
    SubShader
	{
		Tags
		{
			"Queue" = "Transparent"
			"IgnoreProjector" = "True"
			"RenderType"="ReplacementBuildingOutline"
			"PreviewType" = "Plane"
			"CanUseSpriteAtlas" = "True"
		}

		Zwrite Off
		Cull Off
		Lighting Off
		Blend SrcAlpha OneMinusSrcAlpha 

		GrabPass
		{
			"_BackgroundTexture"
		}
		// this is outlines active House
		Pass
		{
		    CGPROGRAM
                #pragma vertex SpriteVert
                #pragma fragment SpriteFrag
                #define SHIFT float2(1,0)
                #include "SubShaders/Outline.cginc"
		    ENDCG
		}
		Pass
		{
		    CGPROGRAM
                #pragma vertex SpriteVert
                #pragma fragment SpriteFrag
                #pragma target 2.0
                #include "UnityCG.cginc"
                #define SHIFT float2(-1,0)
                #include "SubShaders/Outline.cginc"
		    ENDCG
		}
		Pass
		{
		    CGPROGRAM
                #pragma vertex SpriteVert
                #pragma fragment SpriteFrag
                #pragma target 2.0
                #include "UnityCG.cginc"
                #define SHIFT float2(0,1)
                #include "SubShaders/Outline.cginc"
		    ENDCG
		}
		Pass
		{
		    CGPROGRAM
                #pragma vertex SpriteVert
                #pragma fragment SpriteFrag
                #pragma target 2.0
                #include "UnityCG.cginc"
                #define SHIFT float2(0,-1)
                #include "SubShaders/Outline.cginc"
		    ENDCG
		} 
		// Render Main Texture
		Pass
		{
			CGPROGRAM
                #pragma vertex SpriteVert
                #pragma fragment SpriteFrag
                #pragma target 2.0
                #pragma multi_compile_instancing
                #pragma multi_compile_local _ PIXELSNAP_ON
                #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
                #ifndef UNITY_SPRITES_INCLUDED
                #define UNITY_SPRITES_INCLUDED
                #include "UnityCG.cginc"
                #include "SubShaders/ColoredBuildings.cginc"
                #endif // UNITY_SPRITES_INCLUDED
			ENDCG
		}
		// Clean up the outlines
		Pass
		{
		    Zwrite On
            Blend SrcAlpha OneMinusSrcAlpha
			CGPROGRAM
                #pragma vertex SpriteVert
                #pragma fragment SpriteFrag
                #pragma target 2.0
                #pragma multi_compile_instancing
                #pragma multi_compile_local _ PIXELSNAP_ON
                #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
                #ifndef UNITY_SPRITES_INCLUDED
                #define UNITY_SPRITES_INCLUDED
                #include "UnityCG.cginc"
                #include "SubShaders/CleanUpOutlines.cginc"
                #endif
			ENDCG
		}
	}

	//BuildingSelected - The house that is selected - but without a frame (an ordinary house, not a power plant, not a theater)
	SubShader
	{
		Tags
		{
			"Queue" = "Transparent"
			"IgnoreProjector" = "True"
			"RenderType"="ReplacementBuildingSelected"
			"PreviewType" = "Plane"
			"CanUseSpriteAtlas" = "True"
		}
		Zwrite On 
		Cull Off
		Lighting Off
		Blend SrcAlpha OneMinusSrcAlpha

		// no GrabPass because no transparency
		// this is outlines active House
		Pass
		{
		    CGPROGRAM
                #pragma vertex SpriteVert
                #pragma fragment SpriteFrag
                #define SHIFT float2(1,0)
                #include "SubShaders/Outline.cginc"
		    ENDCG
		}
		Pass
		{
		    CGPROGRAM
                #pragma vertex SpriteVert
                #pragma fragment SpriteFrag
                #pragma target 2.0
                #include "UnityCG.cginc"
                #define SHIFT float2(-1,0)
                #include "SubShaders/Outline.cginc"
		    ENDCG
		}
		Pass
		{
		    CGPROGRAM
                #pragma vertex SpriteVert
                #pragma fragment SpriteFrag
                #pragma target 2.0
                #include "UnityCG.cginc"
                #define SHIFT float2(0,1)
                #include "SubShaders/Outline.cginc"
		    ENDCG
		}
		Pass
		{
		    CGPROGRAM
                #pragma vertex SpriteVert
                #pragma fragment SpriteFrag
                #pragma target 2.0
                #include "UnityCG.cginc"
                #define SHIFT float2(0,-1)
                #include "SubShaders/Outline.cginc"
		    ENDCG
		}
		// Render Main Texture
		Pass
		{
			CGPROGRAM
                #pragma vertex SpriteVert
                #pragma fragment SpriteFrag
                #pragma target 2.0
                #pragma multi_compile_instancing
                #pragma multi_compile_local _ PIXELSNAP_ON
                #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
                #ifndef UNITY_SPRITES_INCLUDED
                #define UNITY_SPRITES_INCLUDED
                #include "UnityCG.cginc"
                #include "SubShaders/ColoredBuildings.cginc"
                #endif // UNITY_SPRITES_INCLUDED
			ENDCG
		}
		// Clean up the outlines
		Pass
		{
            Blend SrcAlpha OneMinusSrcAlpha
			CGPROGRAM
                #pragma vertex SpriteVert
                #pragma fragment SpriteFrag
                #pragma target 2.0
                #pragma multi_compile_instancing
                #pragma multi_compile_local _ PIXELSNAP_ON
                #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
                #ifndef UNITY_SPRITES_INCLUDED
                #define UNITY_SPRITES_INCLUDED
                #include "UnityCG.cginc"
                #include "SubShaders/CleanUpOutlines.cginc"
                #endif 
			ENDCG
		}
	}

	//AOEBuildingSelected - with yellow border - theater, power-plant - selected house with AoE frame
	SubShader
	{
		Tags
		{
			"Queue" = "Transparent+7"
			"IgnoreProjector" = "True"
			"RenderType"="ReplacementBuildingAoeSelected"
			"PreviewType" = "Plane"
			"CanUseSpriteAtlas" = "True"
		}

		Zwrite Off
		Cull Off
		Lighting Off
		Blend SrcAlpha OneMinusSrcAlpha 

		// no GrabPass because no transparency
		// this is outlines active House
		Pass
		{
		    CGPROGRAM
                #pragma vertex SpriteVert
                #pragma fragment SpriteFrag
                #define SHIFT float2(1,0)
                #include "SubShaders/Outline.cginc"
		    ENDCG
		}
		Pass
		{
		    CGPROGRAM
                #pragma vertex SpriteVert
                #pragma fragment SpriteFrag
                #pragma target 2.0
                #include "UnityCG.cginc"
                #define SHIFT float2(-1,0)
                #include "SubShaders/Outline.cginc"
		    ENDCG
		}
		Pass
		{
		    CGPROGRAM
                #pragma vertex SpriteVert
                #pragma fragment SpriteFrag
                #pragma target 2.0
                #include "UnityCG.cginc"
                #define SHIFT float2(0,1)
                #include "SubShaders/Outline.cginc"
		    ENDCG
		}
		Pass
		{
		    CGPROGRAM
                #pragma vertex SpriteVert
                #pragma fragment SpriteFrag
                #pragma target 2.0
                #include "UnityCG.cginc"
                #define SHIFT float2(0,-1)
                #include "SubShaders/Outline.cginc"
		    ENDCG
		}
		// Render Main Texture
		Pass
		{
			CGPROGRAM
                #pragma vertex SpriteVert
                #pragma fragment SpriteFrag
                #pragma target 2.0
                #pragma multi_compile_instancing
                #pragma multi_compile_local _ PIXELSNAP_ON
                #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
                #ifndef UNITY_SPRITES_INCLUDED
                #define UNITY_SPRITES_INCLUDED
                #include "UnityCG.cginc"
                #include "SubShaders/ColoredBuildings.cginc"
                #endif // UNITY_SPRITES_INCLUDED
                
			ENDCG
		}
		// Clean up the outlines
		Pass
		{
		    Zwrite On
            Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
                #pragma vertex SpriteVert
                #pragma fragment SpriteFrag
                #pragma target 2.0
                #pragma multi_compile_instancing
                #pragma multi_compile_local _ PIXELSNAP_ON
                #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
                #ifndef UNITY_SPRITES_INCLUDED
                #define UNITY_SPRITES_INCLUDED
                #include "UnityCG.cginc"
                #include "SubShaders/CleanUpOutlines.cginc"
                #endif
			ENDCG
		}
	}

    //GlowAdditive - these are glowing particles flying upward from buildings inside the AoE frame
    SubShader
    {
        Tags { "Queue"="Transparent+100" "IgnoreProjector"="True" "RenderType"="ReplacementGlowAdditive" }
        Blend SrcAlpha One
        Cull Off
        Lighting Off
        ZWrite Off
        Pass {
            CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
                #include "SubShaders/GlowAdditive.cginc"
            ENDCG
        }
    }

    //GlowAdditive - these are glowing rays from the house
    SubShader
    {
        Tags { "Queue"="Transparent+100" "IgnoreProjector"="True" "RenderType"="SelectionGlowAdditive" }
        Blend SrcAlpha One
        Cull Off
        Lighting Off
        ZWrite Off

        Pass {
            CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
                #include "SubShaders/GlowAdditive.cginc"
            ENDCG
        }
    }

    // GRID
    SubShader
    {
            Tags
            {
                "IgnoreProjector" = "True"
                "RenderType" = "ReplacedGrid"
                "Queue" = "Geometry+40"
            }
            LOD 200
    		Pass
    		{
    			Blend SrcAlpha OneMinusSrcAlpha
    			ZWrite Off
    			ZTest Less
    			Offset -2, -2
    			CGPROGRAM
    				#pragma vertex vert
    				#pragma fragment frag
					#include "SubShaders/Grid.cginc"
    			ENDCG
    		}
    }

    //Locked Sector Border - frames around locked sectors
    SubShader
    {
    	Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="ReplacedLockedSectorBorder"}
    	LOD 100

    	ZWrite Off
    	Blend SrcAlpha OneMinusSrcAlpha

    	Pass {
    		CGPROGRAM
    			#pragma vertex vert
    			#pragma fragment frag
				#include "SubShaders/Icons.cginc"
    		ENDCG
    	}
    }

	//MovingArrows - Arrows under the house while dragging
    SubShader
    {
    	Tags
    	{
    		"Queue"="Transparent+7"
    		"IgnoreProjector"="True"
    		"RenderType"="ReplacementMoveArrows"
    		"PreviewType"="Plane"
    		"CanUseSpriteAtlas"="True"
    	}

    	Cull Off
    	Lighting Off
    	ZWrite Off
    	Blend One OneMinusSrcAlpha

    	Pass
    	{
    		CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#include "SubShaders/MovingArrows.cginc"
    		ENDCG
    	}
    }

	//WaterLite
	SubShader
	{
		Tags { "RenderType" = "WaterLite" "Queue" = "Transparent+2" }
		 

		LOD 50
		ZWrite Off
		ZTest Always

		Blend SrcAlpha OneMinusSrcAlpha

		GrabPass
		{
			"_BackgroundTexture"
		}
		Pass
		{
			CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
			#pragma shader_feature _T_FIRST _T_SECOND  _T_THIRD _T_FOURTH

			#pragma shader_feature ANIMATE_SIN_WAVE_FOAM
			#pragma shader_feature USE_TRANSPARENT_BLENDING
			#pragma shader_feature USE_GRAB_PASS_BACKGROUND
			#pragma shader_feature DEBUG_TIME
			#pragma shader_feature FLIP_WAVE_Y            

                #include "SubShaders/WaterLite.cginc"
			ENDCG
		}
	}

	//Map
	SubShader
	{
		Tags { "RenderType" = "Map" "Queue" = "Geometry" }
		LOD 100
		Pass
		{
			CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
                #include "SubShaders/Map.cginc"
			ENDCG
		}
	}

	//WorldSpaceSprite - suddenly - these are locks on locked sectors
	SubShader
	{
        Tags {"Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "WorldSpaceSprite"}
        Cull Off
        Lighting Off
        ZWrite Off
        ZTest Always
        Blend SrcAlpha OneMinusSrcAlpha
        Pass {
            CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
                #include "SubShaders/Icons.cginc"
            ENDCG
        }
    }

	//Birds
	SubShader
    {
        Tags { "RenderType" = "ReplacementVertAnimParticles" }
        LOD 100
        Pass
        {
            CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
                #include "SubShaders/Birds.cginc"
            ENDCG
        }
    }

    //SpritePulsate - yellow AoE border
    SubShader
	{
		Tags
		{
			"Queue" = "Transparent+6"
			"IgnoreProjector" = "True"
			"RenderType" = "SpritePulsate"
			"PreviewType" = "Plane"
			"CanUseSpriteAtlas" = "True"
		}
		Cull Off
		Lighting Off
		ZWrite Off
		Ztest Less
		Blend One OneMinusSrcAlpha
		Pass
		{
		    CGPROGRAM
                #pragma vertex SpriteVert
                #pragma fragment SpriteFrag
                #include "SubShaders/AOEPlane.cginc"
			ENDCG
		}
	}
}
