﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/ProgressBar" {
	Properties {
		_Color ("Color", Color) = (1,1,1,0)
        _MainTex ("Base (RGB) Background", 2D) = "white" {} 
		_GradientTex ("Base (RGB) Gradient", 2D) = "white" {} 
		_Progress ("Progress (0 - 1)", Range(0,1)) = 0.5
	}

	SubShader {
		Tags { "Queue"="Overlay+1" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Lighting Off
        LOD 200

		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha 

        Pass{
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag

		#include "UnityCG.cginc"	

		uniform sampler2D _MainTex;
		uniform sampler2D _GradientTex;
		uniform float4 _MainTex_ST;
		uniform fixed _Progress;
		uniform float4 _Color;

		struct v2f 
		{
			float4 pos : SV_POSITION;
			float2 uv : TEXCOORD0;
		};

		 v2f vert(appdata_base i){
			v2f o;
			o.uv = TRANSFORM_TEX (i.texcoord, _MainTex);
			o.pos = UnityObjectToClipPos(i.vertex);
			return o;
		 }

		half4 frag(v2f i) : COLOR {
			half4 b = tex2D( _MainTex, i.uv);
			half4 f = tex2D( _GradientTex, i.uv);
	
			half4 color = b;
			if( i.uv.x < _Progress ){
				color = (1.0 - f.a)*color + f.a*f;
			}
	
			return color*_Color;
		 }
		ENDCG
		}
	}
}
