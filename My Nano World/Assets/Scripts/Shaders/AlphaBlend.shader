﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Transparent/Alpha Blend"
{
	Properties 
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Color ("Main Color", Color) = (1,1,1,1)
	}
	SubShader 
	{
		Tags{ "Queue" = "Transparent" }


		Pass
		{
		    Cull back
            Blend SrcAlpha OneMinusSrcAlpha
            Lighting Off

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

	        #include "UnityCG.cginc"
	        
			sampler2D _MainTex;
			float4 _MainTex_ST;
			fixed4 _Color;
			uniform float _MainTexBias;
			
			struct v2f 
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			v2f vert(appdata_base i)
			{
				v2f o;
				o.uv = TRANSFORM_TEX (i.texcoord, _MainTex);
				o.pos = UnityObjectToClipPos(i.vertex);
				return o;
			}

			half4 frag(v2f i) : COLOR 
			{
				return tex2D(_MainTex, i.uv) * _Color;
			}
			ENDCG
		}
	} 
}
