﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Transparent/Building Outline"
{
	Properties 
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Color ("Main Color", Color) = (1,1,1,1)
		_OutlineColor("Outline Color", Color) = (1,1,1,1)
		_OutlineSize("Outline size", Range(0,0.05)) = 0.035
		_OutlineOffset("Outline offset", Vector) = (0,-0.05,0.05,0)
	}
	SubShader 
	{
		Tags{ "Queue" = "Transparent" "RenderType"="ReplacementBuildingOutline"}



        Pass
        {
            Cull Back
            Blend SrcAlpha OneMinusSrcAlpha
            Lighting Off
            ZTest Always
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            sampler2D _MainTex;
            float4 _MainTex_ST;
            fixed4 _Color;
            uniform float _MainTexBias;

            float _OutlineSize;
            half4 _OutlineColor;
            half4 _OutlineOffset;

            struct v2f
            {
                float4 pos : SV_POSITION;
                float2 uv : TEXCOORD0;
            };

            v2f vert(appdata_base i)
            {
                v2f o;

                 //scaling code
                float4x4 scaleMat;
                scaleMat[0][0] = 1.0 + _OutlineSize;
                scaleMat[0][1] = 0.0;
                scaleMat[0][2] = 0.0;
                scaleMat[0][3] = 0.0;
                scaleMat[1][0] = 0.0;
                scaleMat[1][1] = 1.0 + _OutlineSize;
                scaleMat[1][2] = 0.0;
                scaleMat[1][3] = 0.0;
                scaleMat[2][0] = 0.0;
                scaleMat[2][1] = 0.0;
                scaleMat[2][2] = 1.0 + _OutlineSize;
                scaleMat[2][3] = 0.0;
                scaleMat[3][0] = 0.0;
                scaleMat[3][1] = 0.0;
                scaleMat[3][2] = 0.0;
                scaleMat[3][3] = 1.0;
                o.pos = UnityObjectToClipPos( mul (scaleMat, i.vertex+_OutlineOffset));

                o.uv = TRANSFORM_TEX (i.texcoord, _MainTex);
                return o;
            }

            half4 frag(v2f i) : COLOR
            {
                return _OutlineColor * tex2D(_MainTex, i.uv).a;
            }
            ENDCG
        }

		Pass
		{
		    Cull back
            Blend SrcAlpha OneMinusSrcAlpha
            Lighting Off
            Offset -2, -2

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

	        #include "UnityCG.cginc"
	        
			sampler2D _MainTex;
			float4 _MainTex_ST;
			fixed4 _Color;
			uniform float _MainTexBias;

			float _OutlineSize;
			half4 _OutlineColor;
			
			struct v2f 
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			v2f vert(appdata_base i)
			{
				v2f o;
				o.uv = TRANSFORM_TEX (i.texcoord, _MainTex);
				o.pos = UnityObjectToClipPos(i.vertex);
				return o;
			}

			half4 frag(v2f i) : COLOR 
			{


				float4 col = tex2D(_MainTex, i.uv);
                col.rgb = col.rgb *_Color.a + _Color.rgb*col.a*(1-_Color.a);
				return col;
			}
			ENDCG
		}
	}
}
