﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Transparent/Cutout Fog" 
{
	Properties 
	{
		_Color ("Main Color", Color) = (1,1,1,1)
		_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
		_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
		_MainTexBias ("Mip Bias (-1 to 1)", float) = -1.65
		_ColorTop ("Top", Color) = (1,1,1,1)
		_ColorBottom("Bottom", Color) = (1,1,1,1)
		_ColorMul("ColorMul", Float) = 1
		_FogRamp("FogRamp", 2D) = "white" {}
	}
	SubShader 
	{
		Tags 
		{
			"Queue"="AlphaTest" 
			"IgnoreProjector"="True" 
			"RenderType"="TransparentCutout"

		}
		LOD 200
	
		Pass
		{
			ZWrite On
			ZTest Less

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			 #include "UnityCG.cginc"
	        
			uniform sampler2D _MainTex;
			uniform float4 _MainTex_ST;
			uniform fixed4 _Color;	
			uniform fixed _Cutoff;
			uniform float _MainTexBias;
			uniform float4 _ColorTop;
			uniform float4 _ColorBottom;
			uniform float _ColorMul;
			uniform sampler2D _FogRamp;
			struct v2f 
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float4 modelPos : POSITION1;
			};

			v2f vert(appdata_base i)
			{
				v2f o;
				o.uv = TRANSFORM_TEX (i.texcoord, _MainTex);
				o.pos = UnityObjectToClipPos(i.vertex);
				o.modelPos = i.vertex;
				return o;
			}

			half4 frag(v2f i) : COLOR 
			{
				fixed4 c = tex2Dbias(_MainTex, half4(i.uv.x, i.uv.y,0.0, _MainTexBias)) * _Color; //tex2Dbias(_MainTex, half4(i.texcoord.x,i.texcoord.y,0.0,_MainTexBias)) * i.color;
				fixed4 ramp = tex2D(_FogRamp, half2(0., i.uv.y));
				float zoom = unity_OrthoParams.x/ (_ScreenParams.x/_ScreenParams.y);
				c.rgb  = lerp(c.rgb, lerp(_ColorTop,_ColorBottom,ramp.r), _ColorMul*saturate((zoom-6)/(24-6)));
				clip(c.a - _Cutoff);

				return c;
			}
			ENDCG
		}
	} 
}
