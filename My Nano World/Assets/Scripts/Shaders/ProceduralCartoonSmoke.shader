﻿Shader "Unlit/ProceduralCartoonSmoke"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Color ("Color", Color) = (1,1,1,1)
        _Blend ("Blend", Range(0,1)) = 1
        _Multiply ("Multiply", Range(0,10)) = 1
    }
    SubShader
    {
        Tags { "Queue"="Transparent" }
        LOD 100
		Blend SrcAlpha OneMinusSrcAlpha
		Zwrite Off 
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float4 color : COLOR;
                float2 particlesData : TEXCOORD1;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float4 color : COLOR;
                float2 particlesData : TEXCOORD1;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float4 _Color;
            float _Blend; 
            float _Multiply; 

			float random (in float2 st) {
			    return frac(sin(dot(st.xy,
			                         float2(12.9898,78.233)))
			                 * 43758.5453123);
			}
			
			float noise ( float2 st) {
			    float2 i = floor(st);
			    float2 f = frac(st);
			
			    // Four corners in 2D of a tile
			    float a = random(i);
			    float b = random(i + float2(1.0, 0.0));
			    float c = random(i + float2(0.0, 1.0));
			    float d = random(i + float2(1.0, 1.0));
			
			    // Smooth Interpolation
			
			    // Cubic Hermine Curve.  Same as SmoothStep()
			    float2 u = f*f*(3.0-2.0*f);
			
			    // Mix 4 coorners percentages
			    return lerp(a, b, u.x) +
			            (c - a)* u.y * (1.0 - u.x) +
			            (d - b) * u.x * u.y;
			}

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.color=v.color;
                o.particlesData=v.particlesData;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);
                
                float wave1 = sin(_Time.x * i.uv.y)/2+.5;
                float wave2 = sin(_Time.x * i.uv.x)/2+.5;
                float radious=(1-length(i.uv-.5f));
                
                col = i.color*_Blend + col*(1-_Blend);
                col *= _Multiply;
                
                float noize = noise(i.uv*3 + _Time.x + i.particlesData.x * 360 );
                col.a=smoothstep(.5,.8, noize*radious*i.color.a);
                return col;
            }
            ENDCG
        }
    }
}
