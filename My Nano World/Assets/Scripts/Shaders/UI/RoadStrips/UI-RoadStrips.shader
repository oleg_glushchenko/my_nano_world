// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "NanoReality/UI/UI-RoadStrips"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		_Strips("Strips", Color) = (0.04,0.04,0.04,1)
		_Width("Width", Float) = 400
		_Space("Space", Float) = 0.93

		_ClipTopStrips("_ClipTopStrips", Float) = 0.13
		_ClipBottomStrips("_ClipBottomStrips", Float) = 0.72

		_Perspective("Perspective Distortion", Float) = -0.15

		[HideInInspector] _StencilComp ("Stencil Comparison", Float) = 8
		[HideInInspector] _Stencil ("Stencil ID", Float) = 0
		[HideInInspector] _StencilOp ("Stencil Operation", Float) = 0
		[HideInInspector] _StencilWriteMask ("Stencil Write Mask", Float) = 255
		[HideInInspector] _StencilReadMask ("Stencil Read Mask", Float) = 255

		[HideInInspector] _ColorMask ("Color Mask", Float) = 15
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}
		Offset -1, -1
		Stencil
		{
			Ref [_Stencil]
			Comp [_StencilComp]
			Pass [_StencilOp] 
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
		}

		Cull Off
		Lighting Off
		ZWrite Off
		ZTest [unity_GUIZTestMode]
		Blend SrcAlpha OneMinusSrcAlpha
		ColorMask [_ColorMask]

		Pass
		{
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 uv  : TEXCOORD0;
			};
			
			fixed4 _Color;

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				OUT.vertex = UnityObjectToClipPos(IN.vertex);
				OUT.uv = IN.uv;
#ifdef UNITY_HALF_TEXEL_OFFSET
				OUT.vertex.xy += (_ScreenParams.zw-1.0)*float2(-1,1);
#endif
				OUT.color = IN.color * _Color;
				return OUT;
			}

			sampler2D _MainTex;
			float _Perspective;
			float _Width; 
			float _Space;
			float _ClipTopStrips;
			float _ClipBottomStrips;
			float4 _Strips;

			float2 MeshDistortion(float2 uv)
			{
				float center = .5;
				float distFromCenter = uv.x - center;
				float x = (uv.x - (  uv.y  *uv.x  * distFromCenter) * _Perspective) ;
				float y = uv.y ;
				return float2(x, y);
			}

			fixed4 frag(v2f IN) : SV_Target
			{
				half4 color = tex2D(_MainTex, IN.uv) * IN.color;
				clip (color.a - 0.01);
				float clipper = step(_ClipTopStrips, 1-IN.uv.y) * step(1 - _ClipBottomStrips, IN.uv.y);
				return color + smoothstep(_Space-.01,_Space + .01, sin(MeshDistortion(IN.uv).x* _Width)/2+.5)* _Strips* color.a* clipper;
			}
		ENDCG
		}
	}
}
