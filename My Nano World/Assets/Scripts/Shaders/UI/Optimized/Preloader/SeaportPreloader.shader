﻿Shader "NanoReality/UI/SeaportPreloader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_Radious("Radious", Float) = 1
		_Scale("Scale", Float) = .1
		_Scale2("Scale2", Float) = .1
		_Pow("POW", Float) = 1
		_Speed("Speed", Float) = .5
		_Step("Step", Float) = .5
		_StepSize("StepSize", Float) = .1
		_Color1("Color1", Color) = (1,1,1,1)
		_Color2("Color2", Color) = (1,1,1,1)
    }
    SubShader
    {
		Tags { "Queue" = "Transparent"  "RenderType" = "Opaque" }
		LOD 100
			Blend SrcAlpha OneMinusSrcAlpha
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag 
			#pragma multi_compile_instancing
			#pragma fragmentoption ARB_precision_hint_fastest

			#pragma target 2.0

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _Radious;
            float _Scale;
            float _Scale2;
            float _Pow;
            float _StepSize;
            float _Step;
            float _Speed;
            float4 _Color1;
            float4 _Color2;

			float2 pos(float t ) 
			{ 
				return float2(
					((sin(t) / 2) * _Radious + .5),
					((cos(t) / 2) * _Radious + .5)
					); 
			}
			 
			float draw(float r, float2 uv, float scale)
			{ 
				float2 pos1 = pos(r); 
				return pow(1 / scale - abs( length(- pos1 + (uv))  ) / scale, _Pow);//
			} 

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				float pos1 = .5;
				float scale = 2;

				float mask0 =  draw(-_Time.y* _Speed *1.1, i.uv, _Scale);
				float mask1 =  draw((-_Time.y * _Speed * 4.1 + UNITY_PI*4) / 4, i.uv, _Scale);
				float mask2 = draw((-_Time.y * _Speed * 2.2 + UNITY_PI*3) / 4, i.uv, _Scale);
				float mask3 = draw((-_Time.y * _Speed * 3.3 +UNITY_PI*4 ) / 4, i.uv, _Scale);

				fixed4 img0 = sin(length(pos1 - (i.uv)) * UNITY_PI * 2) * (1 - step(.5, length(pos1 - (i.uv))));
				img0 = pow(img0, _Pow);
				img0 = smoothstep(_Step- _StepSize, _Step + _StepSize, img0 * (mask0+ mask1+ mask2+ mask3)/32); 
				return img0* _Color1; 
            }
            ENDCG
        }
    }FallBack "NanoReality/UI/SeaportPreloader_FallBack"
}
