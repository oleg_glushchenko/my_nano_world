﻿Shader "NanoReality/UI/SeaportPreloader_FallBack"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}   
    }
    SubShader
    {
		Tags { "Queue" = "Transparent"  "RenderType" = "Opaque" }
		LOD 100
			Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag 
			#pragma multi_compile_instancing
			#pragma fragmentoption ARB_precision_hint_fastest

			#pragma target 2.0

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST; 

			float2  rotate2d(float angle, float2 uv) {
				float2x2 rotationMatrix = float2x2(cos(angle), -sin(angle), sin(angle), cos(angle));
				return mul(uv, rotationMatrix);
			}

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				float scale=1.5;
				o.uv = rotate2d(_Time.x * 100, o.uv * scale -.5 * scale) + .5; 
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				return col;
			}
            ENDCG
        }
    }
}
