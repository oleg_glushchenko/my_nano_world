﻿Shader "NanoReality/UI/UI-Wash"
{
	Properties
	{
		[HideInInspector] _MainTex("Texture", 2D) = "white" {}
		_WavesShape("Texture", 2D) = "white" {}
		_GrabPassTex("GrabPass Alternative", 2D) = "white" {}
		_Color1("Color1", Color) = (1,1,1,1)
		_Color2("Color2", Color) = (1,1,1,1)
		_Speed("Speed", Float) = 1
		//[Toggle(USE_GRABPASS)] _UseGrabpass("Use Grabpass", Float) = 0
	}
		SubShader
		{
		  Tags { "RenderType" = "Opaque"  "Queue" = "Transparent" }
		  LOD 300
		  Blend SrcAlpha oneMinusSrcAlpha			
			/*
		  GrabPass
		  {
			  "_BehindSplash"
		  }*/
			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag  
				//#pragma shader_feature USE_GRABPASS 

				#include "UnityCG.cginc"

				struct appdata
				{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
					float4 color    : COLOR;
				};

				struct v2f
				{
					float2 uv : TEXCOORD0;
					float4 vertex : SV_POSITION;
					float3 normalDir : TEXCOORD1;
					float3 viewDir : TEXCOORD2;
					float4 timer : TEXCOORD3;
					float4 color    : COLOR;
					float4 grabPos : TEXCOORD4;
				};

					sampler2D _MainTex;
					float4 _MainTex_ST;

					sampler2D _GrabPassTex;
					float4 _GrabPassTex_ST;

					sampler2D _WavesShape;
					float4 _WavesShape_ST;

					float _Speed;

					float4 _Color1;
					float4 _Color2;

					//sampler2D _BehindSplash;
					const float wavesFadeWidth = .04;

					float mod(float x, float y = 1) {
						return x - y * floor(x / y);
					}

					v2f vert(appdata v)
					{
						v2f o;
						o.vertex = UnityObjectToClipPos(v.vertex);
						o.uv = TRANSFORM_TEX(v.uv, _MainTex);

						float x = o.uv.x;
						float y = o.uv.y;

						o.color = v.color;
						float t = _Time.x * _Speed * o.color.w;
						o.timer = mod(t);

						o.grabPos = ComputeGrabScreenPos(o.vertex);
						o.grabPos /= o.grabPos.w;

						return o;
					}


					fixed4 frag(v2f i) : SV_Target
					{
						float t = i.timer.x + i.color.y;
						float y = i.uv.y;

						half spriteColorAlphaRation = i.color.w;

						float4 color = lerp(_Color2, _Color1, y);

						float4 uvaMaps = tex2D(_MainTex, i.uv);

						float4 distortionBase = tex2D(_WavesShape, uvaMaps.rg + float2(0,t)) * (1 - uvaMaps.b);
							   distortionBase += tex2D(_WavesShape, uvaMaps.rg + float2(0,t + .5)) * (1 - uvaMaps.b);


						 float radiousFader = clamp(pow(1 - length(i.uv - .5) + .25, 16), 0, 1) * (distortionBase / 2);

						float4 foam =  tex2D(_WavesShape, uvaMaps.rg + float2(0, t / 1 + .3))* (1 - uvaMaps.b);
						foam *= clamp(pow(1 - length(i.uv - .5) + .25, 16), 0, 1) * (foam  );
						
						distortionBase.a = radiousFader * (distortionBase.r );
						float2 backgroundDistortion = float2(0,  distortionBase.a) * radiousFader;

						float a = 1;
//#ifdef USE_GRABPASS
						fixed3 col = tex2D(_GrabPassTex, i.uv  - backgroundDistortion );//, i.uv  - backgroundDistortion)  ;
						a = distortionBase.a;

						/*
#else
						fixed3 col = lerp(_Color2, _Color1, sin(distortionBase.a ) ); 
						a = distortionBase.a;
#endif*/
						 
						return   float4(col, a) + foam;
					}
					ENDCG
				}
		}
}
