﻿Shader "NanoReality/UI/UI-Water"
{
	Properties
	{
	  [HideInInspector] _MainTex("Texture", 2D) = "white" {}

		_Color1("Color1", Color) = (1,1,1,1)
		_Color2("Color2", Color) = (1,1,1,1)
		_Color3("Color3", Color) = (1,1,1,1)

		_Speed("Speed", Float) = 1

		_Horizont("Horizont", Float) = 1
		_XYRatio("_XYRatio", Float) = 1
		_Perspective("Perspective", Float) = 1
		_ScaleX("ScaleX", Float) = 1
		_ScaleY("ScaleY", Float) = 1

	  [Header(Water Surface)]
		_ComposedMap("ComposedMap", 2D) = "white" {}
		_SurfaceIntencity("Surface Intencity", Float) = 0.5
		_FoamIntencity("Foam Intencity", Float) = 0.5

	  [Header(Underwater)]
		_UnderWaterIntencity("Underwater Intencity", Float) = 1

	  [Header(Reflactions properties)]
		_ReflactionsAndMask("Reflactions And Mask", 2D) = "white" {}
		_ReflactCenter("Reflaction Center", Float) = 0.08
		_ReflactDistortion("Reflaction Distortion", Float) = 15

		_ReflactIntencity("Reflaction Intencity", Float) = 0.72
		_ReflactYFader("Reflaction Y Fader", Float) = -0.5

	  [Header(Other)]
		_SkyBlender("SkyBlender", Float) = 0.05

	  [Header(Debug)]
		[Toggle(SHOW_TEST_GRID)] _Show_test_grid("Show Testing Grid", Float) = 0
	}

		SubShader
		{
			Tags { "RenderType" = "Opaque"  "Queue" = "Transparent" }
			LOD 300
			Blend SrcAlpha oneMinusSrcAlpha
			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag  

			  #pragma shader_feature SHOW_TEST_GRID 

				#include "UnityCG.cginc"

				struct appdata
				{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
					float4 color    : COLOR;
				};

				struct v2f
				{
					float2 uv : TEXCOORD0;
					float4 vertex : SV_POSITION; 
					float4 timer : TEXCOORD1;
					float4 color    : COLOR;
				};

					sampler2D _MainTex;
					float4 _MainTex_ST;

					sampler2D _ReflactionsAndMask;
					float4 _ReflactionsAndMask_ST;

					sampler2D _ComposedMap;
					float4 _ComposedMap_ST;

					float _Speed;
					float4 _Color1;
					float4 _Color2;
					float4 _Color3;
					float _Horizont;
					float _XYRatio;
					float _Perspective;
					float _ScaleX;
					float _ScaleY;

					float _SurfaceIntencity;
					float _FoamIntencity;

					float _SkyBlender;

					float _UnderWaterIntencity;

					float _ReflactCenter;
					float _ReflactDistortion;
					float _ReflactIntencity;
					float _ReflactYFader;

					const float wavesFadeWidth = .04;

					float mod(float x, float y = 1) {
						return x - y * floor(x / y);
					}
					v2f vert(appdata v)
					{
						v2f o;
						o.vertex = UnityObjectToClipPos(v.vertex);
						o.uv = TRANSFORM_TEX(v.uv, _MainTex);

						float x = o.uv.x;
						float y = o.uv.y;

						o.color = v.color;
						float t = _Time.x * _Speed * o.color.w;
						o.timer = frac(t / (2 * UNITY_PI));
						o.timer.x = mod(t);
						o.timer.y = frac(_Time.x / 2);

						return o;
					}
					float2 MeshDistortion(float2 uv)
					{
						float center = .5;
						float distFromCenter = uv.x - center;
						float x = (uv.x - (asin(uv.y) * asin(uv.y) * distFromCenter) * _Perspective) * _ScaleX;
						float y = uv.y * _ScaleY;
						return float2(x,y);
					}
					float4 WaterMath(float2 uv, float t, float4 offsets)
					{
						float2 uv1 = MeshDistortion(uv);
						   uv1.y += t;
						    

						float x = uv1.x;
						float y = uv1.y;

						float4 underwater = tex2D(_ComposedMap, uv).a * _UnderWaterIntencity;
						float4 color = lerp(_Color2, _Color1,  uv.y);

						color = lerp(color, _Color3, underwater);

						float dist = tex2D(_ComposedMap, uv1).r;
							  dist *= tex2D(_ComposedMap, uv1 - float2(0, t)).r;
							  dist += tex2D(_ComposedMap, uv1 + float2(0, t)).r;

					  float reflactions = tex2D(_ReflactionsAndMask, uv + float2(-dist, dist) * .01 * _ReflactDistortion + float2(_ReflactCenter, 0));

					  //aplay gradual intencity
					  reflactions *= _ReflactIntencity * (clamp(2 - uv.y * 2 + _ReflactYFader, 0, 1));
					  float foam0 = tex2D(_ComposedMap, uv1 + float2(-dist, dist) * .2).g;
					  float4 foam = float4(foam0, foam0, foam0, 1) * _FoamIntencity;
					  float4 tex = tex2D(_ComposedMap, uv1 + float2(0, dist)).b * color * _SurfaceIntencity;
  #ifdef SHOW_TEST_GRID
					  return step(.7, mod(x)) + step(.7, mod(y));
  #else
					  return (reflactions + underwater + tex + foam) * float4(1,1,1,  smoothstep(0, _SkyBlender,1 - uv.y));
  #endif
					}

					fixed4 frag(v2f i) : SV_Target
					{
						float t = i.timer.x * 2 * UNITY_PI;
						float t2 = i.timer.y;
						float4 water = WaterMath(i.uv*float2(1, _Horizont), i.timer.x, i.color);
						return  water;
					}
					ENDCG
				}
		}
}
