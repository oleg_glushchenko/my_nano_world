﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Sprites/BuildingPlace"
{
	Properties
	{
		[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
		_Color("Tint", Color) = (1,1,1,1)
		_BrightnessMin("Brightness Min", float) = 0.8
		_BrightnessMax("Brightness Max", float) = 1.5

		_StencilComp("Stencil Comparison", Float) = 8
		_Stencil("Stencil ID", Float) = 0
		_StencilOp("Stencil Operation", Float) = 0
		_StencilWriteMask("Stencil Write Mask", Float) = 255
		_StencilReadMask("Stencil Read Mask", Float) = 255

		_ColorMask("Color Mask", Float) = 15
			 

	}

		SubShader
	{
		Tags
		{
			"Queue" = "Transparent"
			"IgnoreProjector" = "True"
			"RenderType" = "Transparent"
			"PreviewType" = "Plane"
			"CanUseSpriteAtlas" = "True"
		}

		Stencil
		{
			Ref[_Stencil]
			Comp[_StencilComp]
			Pass[_StencilOp]
			ReadMask[_StencilReadMask]
			WriteMask[_StencilWriteMask]
		}

		Cull Off
		Lighting Off
			/**/
		ZWrite Off
		ZTest[unity_GUIZTestMode]
		Blend SrcAlpha OneMinusSrcAlpha
		ColorMask[_ColorMask]

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color : COLOR;
				half2 texcoord  : TEXCOORD0;
			};

			fixed4 _Color;
			float _BrightnessMin;
			float _BrightnessMax;
			 

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				OUT.vertex = UnityObjectToClipPos(IN.vertex);
				OUT.texcoord = IN.texcoord;
		#ifdef UNITY_HALF_TEXEL_OFFSET
				OUT.vertex.xy += (_ScreenParams.zw - 1.0)*float2(-1,1);
		#endif
				float phase = _Time * 30.0;
				float offset = _BrightnessMax - _BrightnessMin;
				OUT.color = IN.color * _Color *(abs(sin(phase + offset)) + _BrightnessMin * _BrightnessMax);
				return OUT;
			}

			sampler2D _MainTex;  

			fixed4 frag(v2f IN) : SV_Target
			{
				half4 color = tex2D(_MainTex, IN.texcoord) * IN.color;

				float s = 5;

				float t = -_Time.y*s ; 

				float grad1 = IN.texcoord.x * 40  ;
				float grad2 = IN.texcoord.y * 40  ;
				  
				float lines1 =  cos( t + 4 - grad1)/2 + .5;
				float lines2 =  cos(-t - 1.7 - grad1)/2 + .5;
				float lines3 =  cos( t + 4 - grad2)/2 + .5;
				float lines4 =  cos(-t - 1.7 - grad2)/2 + .5;
				 
				float sector1 = step(1,(1-IN.texcoord.x) / IN.texcoord.y)*step(1,IN.texcoord.x / IN.texcoord.y) ;
				float sector2 = step(1,(1-IN.texcoord.y) / IN.texcoord.x)*step(1,IN.texcoord.y / IN.texcoord.x) ;
				float sector3 = step(1,(IN.texcoord.y) / (1-IN.texcoord.x))*step(1,IN.texcoord.y / IN.texcoord.x) ;
				float sector4 = step(1,(IN.texcoord.x) / (1-IN.texcoord.y))*step(1,IN.texcoord.x / IN.texcoord.y) ;
				 
				float borderW = .1; 

				float mat =   (cos(t) + 1) / 5;

				float tint = smoothstep(.45- borderW,.55+ borderW, lines4 * sector1 + lines2 * sector2 + lines3 * sector3 + lines1*sector4)+ mat;
				  
				color = float4(IN.color.r, IN.color.g, IN.color.b, tint); 

				return color;
			}
			ENDCG
		}
	}
}
