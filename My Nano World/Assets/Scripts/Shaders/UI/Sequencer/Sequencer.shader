﻿Shader "Unlit/Sequencer"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Cols ("Cols", Float) = 5
        _Rows ("Rows", Float) = 5
        _Speed ("Speed", Float) = 0
        _Delay("Delay", Float) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" "Queue"="Transparent" }
        LOD 100
		Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _Cols;
            float _Rows;
            float _Speed;
            float _Delay;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

			float mod(float x, float y) {
				return x - (int(x / y) * y);
			}


			fixed4 frag(v2f i) : SV_Target
			{
				float episode = mod(_Time.x * _Speed, 1 + _Delay);
				float t = episode *  (_Cols * _Rows);
			 
				float fx = floor( t % _Rows); 
				float fy = _Rows - floor( t / _Cols) - 1 ;
				 
                fixed4 col = tex2D(_MainTex, 
								float2(
									i.uv.x / _Cols + fx/ _Cols,
									i.uv.y / _Rows + fy / _Rows
									)  
							 );

				col.a = col.a - step(1, episode);
                return col;
            }
            ENDCG
        }
    }
}
