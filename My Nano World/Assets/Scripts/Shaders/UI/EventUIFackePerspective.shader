Shader "Unlit/EventUIFackePerspective"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
      
        _U_Scale("U Scale", Float) = 1
        _U_Near_Scale("U Near Scale", Float) = 0
        _UV_Perspective_Min_Max("UV Perspective Min Max", Vector) = (0, 1, 0, 0)
        _V_Perspective_Power("V Perspective Power", Float) = 1
        [NoScaleOffset]_Clouds_Top("Clouds Top", 2D) = "white" {}
        [NoScaleOffset]_Clouds_Bottom("Clouds Bottom", 2D) = "white" {}
        _Clouds_1_U_Scale("Clouds 1 U Scale", Float) = 2
        _Clouds_2_U_Scale("Clouds 2 U Scale", Float) = 2
        _Clouds_U_Perspective("Clouds U Perspective", Float) = 3
        _Clouds_1_V_Remap_In("Clouds 1 V Remap In", Vector) = (0, 1, 0, 0)
        _Clouds_2_V_Remap_In("Clouds 2 V Remap In", Vector) = (0, 1, 0, 0)
        [NoScaleOffset]_Road("Road", 2D) = "white" {}
        _Road_V_Remap_In("Road V Remap In", Vector) = (0, 1, 0, 0)
        [NoScaleOffset]_Grass_Foreground("Grass Foreground", 2D) = "white" {}
        _Grass_Foreground_U_Scale("Grass Foreground U Scale", Float) = 1
        _Grass_Foreground_U_Perspective("Grass Foreground U Perspective", Float) = 1
        _Grass_Foreground_V_Remap_In("Grass Foreground V Remap In", Vector) = (0, 1, 0, 0)
        _Grass_Background_U_Scale("Grass Background U Scale", Float) = 5
        _Grass_Background_V_Remap_In("Grass Background V Remap In", Vector) = (0, 1, 0, 0)
        [NoScaleOffset]_Grass_1("Grass 1", 2D) = "white" {}
        _Grass_1_Amount("Grass 1 Amount", Range(0, 1)) = 0.906
        [NoScaleOffset]_Grass_2("Grass 2", 2D) = "white" {}
        _Grass_2_Amount("Grass 2 Amount", Range(0, 1)) = 0.911
        _Scroll_Speed("Scroll Speed", Float) = 0.1
        _Scroll_Force("Scroll Force", Float) = 1
        _Wind_Speed("Wind Speed", Float) = 0.1
        _Wind_Speed2("Wind Speed 2", Float) = 0.1
        _Color_1("Color 1", Color) = (0, 0.7626443, 1, 0)
        _Color_2("Color 2", Color) = (0.3345942, 0.9716981, 0.9252448, 0)
		
		 [HideInInspector] _StencilComp ("Stencil Comparison", Float) = 8
         [HideInInspector] _Stencil ("Stencil ID", Float) = 0
         [HideInInspector] _StencilOp ("Stencil Operation", Float) = 0
         [HideInInspector] _StencilWriteMask ("Stencil Write Mask", Float) = 255
         [HideInInspector] _StencilReadMask ("Stencil Read Mask", Float) = 255
         [HideInInspector] _ColorMask ("Color Mask", Float) = 15
    }
    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

         // required for UI.Mask
         Stencil
         {
             Ref [_Stencil]
             Comp [_StencilComp]
             Pass [_StencilOp] 
             ReadMask [_StencilReadMask]
             WriteMask [_StencilWriteMask]
         }
          ColorMask [_ColorMask]
          
          
        Cull Back
        Lighting Off
        ZWrite Off
        ZTest [unity_GUIZTestMode]
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                 float4  color : COLOR;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float2 uv2 : TEXCOORD1;
                 float4  color : COLOR;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            
        sampler2D _Clouds_Top;
        sampler2D _Clouds_Bottom;
        sampler2D _Road;
        sampler2D _Grass_Foreground;
        sampler2D _Grass_2;
        sampler2D _Grass_1;
        
        float4 _Clouds_Top_ST;
        float4 _Clouds_Bottom_ST;
        float4 _Road_ST;
        float4 _Grass_Foreground_ST;
        float4 _Grass_2_ST;
        float4 _Grass_1_ST;
            
             
        float _U_Scale;
        float _U_Near_Scale;
        float4 _UV_Perspective_Min_Max;
        float _V_Perspective_Power;
        
        
        float _Clouds_1_U_Scale;
        float _Clouds_2_U_Scale;
        float _Clouds_U_Perspective;
        float4 _Clouds_1_V_Remap_In;
        float4 _Clouds_2_V_Remap_In;
        float4 _Road_V_Remap_In;
        float _Grass_Foreground_U_Scale;
        float _Grass_Foreground_U_Perspective;
        float4 _Grass_Foreground_V_Remap_In;
        float _Grass_Background_U_Scale;
        float4 _Grass_Background_V_Remap_In;
        float _Grass_1_Amount;
        float _Grass_2_Amount;
        uniform float _Scroll_Position;
        float _Scroll_Speed;
        float _Scroll_Force;
        float _Wind_Speed;
        float _Wind_Speed2;
        float4 _Color_1;
        float4 _Color_2;
        
			float4 Remap(float4 In, float2 InMinMax, float2 OutMinMax )
			{
			    return OutMinMax.x + (In - InMinMax.x) * (OutMinMax.y - OutMinMax.x) / (InMinMax.y - InMinMax.x);
			}
			float RandomRange(float2 Seed, float Min, float Max)
			{
			    float randomno =  frac(sin(dot(Seed, float2(12.9898, 78.233)))*43758.5453);
			    return lerp(Min, Max, randomno);
			}
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.uv2 =  ComputeScreenPos(o.vertex);
                o.color=v.color;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
              //  fixed4 col = tex2D(_MainTex, i.uv); 
              
                float scroll = _Time.x*_Scroll_Speed+_Scroll_Position*_Scroll_Force;
                float wind = _Time.x*_Wind_Speed;
                float wind2 = _Time.x*_Wind_Speed2;
                
                float screenCenter = Remap(i.uv2.x, float2(0,1), float2(-1,1) );
                float screenRatio = _ScreenParams.x/_ScreenParams.y;
                
                float uvX = screenCenter*screenRatio;
                
                float clouds1U =( Remap(_Clouds_1_U_Scale*uvX, float2(-1,1), float2(0,1) )+scroll*.5+wind)/(_Clouds_1_U_Scale*_Clouds_U_Perspective);
                float clouds2U =( Remap(_Clouds_2_U_Scale*uvX, float2(-1,1), float2(0,1) )+scroll*.5+wind2)/(_Clouds_2_U_Scale*_Clouds_U_Perspective);
                
                
                float grassPerspF = (_U_Scale-_U_Near_Scale)*_Grass_Foreground_U_Perspective;
                float grassPerspB = (_U_Scale+_U_Near_Scale);
                
                float grassFgU =( Remap(grassPerspF*uvX, float2(-1,1), float2(0,1) ) + scroll)*(_Grass_Foreground_U_Scale);
                float grassBGU =( Remap(grassPerspB*uvX, float2(-1,1), float2(0,1) ) + scroll)*(_Grass_Background_U_Scale);
               // return frac(grassBGU);
                float range = RandomRange(floor(grassBGU),0,1);
                
                float grass1U = step(_Grass_1_Amount,range)*grassBGU;
                float grass2U = step(_Grass_2_Amount,1-range)*grassBGU;
                                
                
                float rY= pow(saturate(Remap(grassPerspB*i.uv2.y, _UV_Perspective_Min_Max, float2(0,1) )),_V_Perspective_Power);
                float roadPersp = lerp((_U_Scale-_U_Near_Scale),(_U_Scale+_U_Near_Scale), rY  ) ;
                
                float roadU = ( Remap( roadPersp * uvX, float2(-1,1), float2(0,1) ) + scroll );
                
                
              float clouds1V = saturate( Remap(i.uv2.y, _Clouds_1_V_Remap_In, float2(0,1)) );
              float clouds2V = saturate( Remap(i.uv2.y, _Clouds_2_V_Remap_In, float2(0,1)) );
              float grassBgV = saturate( Remap(i.uv2.y, _Grass_Background_V_Remap_In, float2(0,1)) );
              float grassFgV = saturate( Remap(i.uv2.y, _Grass_Foreground_V_Remap_In, float2(0,1)) );
              float roadV =    saturate( Remap(i.uv2.y, _Road_V_Remap_In, float2(0,1)) );
              
              
                
                
		        fixed4 cloudsTop = tex2D(_Clouds_Top,  float2(clouds1U,clouds1V)  );
		        fixed4 cloudsBottom = tex2D(_Clouds_Bottom,   float2(clouds2U,clouds2V)  );
		        
		        fixed4 road = tex2D(_Road,  float2(roadU,roadV) );
		        
		        fixed4 grassForeground = tex2D(_Grass_Foreground,   float2(grassFgU,grassFgV) );
		        fixed4 grass1 = tex2D(_Grass_1,   float2(grass1U,grassBgV)  );
		        fixed4 grass2 = tex2D(_Grass_2,   float2(grass2U,grassBgV)  );
		                
                
                fixed4 sky = lerp(_Color_1, _Color_2,clouds1V );
                fixed4 skyCloud = lerp(sky, cloudsBottom,cloudsBottom.a );
                fixed4 skyClouds = lerp(skyCloud, cloudsTop,cloudsTop.a );
                
                fixed4 roadAndGrass = lerp(road, grassForeground, grassForeground.a ); 
                
                fixed4 cloudsAndRoad = lerp(skyClouds, roadAndGrass, road.a );
                
                fixed4 allAndGrass1 = lerp(cloudsAndRoad, grass1, grass1.a );
                fixed4 all = lerp(allAndGrass1, grass2, grass2.a );
                 all.a =1;
                all*=i.color;
                
                return all;    
            }
            ENDCG
        }
    }
}
