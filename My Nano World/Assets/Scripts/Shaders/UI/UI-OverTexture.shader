// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Sprites/AnimatedDecal"
{
    Properties{
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0

        _BlendTex ("Blend Texture", 2D) = "white" {}

		_XScrollSpeed ( "X Scroll Speed", Float ) = 1
		_YScrollSpeed ( "Y Scroll Speed", Float ) = 0

        _ColorMask ("Color Mask", Float) = 15
        _StencilComp ("Stencil Comparison", Float) = 8
		_Stencil ("Stencil ID", Float) = 0
		_StencilOp ("Stencil Operation", Float) = 0
		_StencilWriteMask ("Stencil Write Mask", Float) = 255
		_StencilReadMask ("Stencil Read Mask", Float) = 255
    }
 
    SubShader{
        Tags{
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }
 
        Cull Off
        Lighting Off
        ZWrite Off
        Fog { Mode Off }
        Blend SrcAlpha OneMinusSrcAlpha
		ColorMask [_ColorMask]

        Pass{

        Stencil
        {
            Ref [_Stencil]
            Comp [_StencilComp]
            Pass [_StencilOp]
            ReadMask [_StencilReadMask]
            WriteMask [_StencilWriteMask]
        }

        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile DUMMY PIXELSNAP_ON
            #include "UnityCG.cginc"
           
            struct appdata_t{
                float4 vertex   : POSITION;
                float2 texcoord : TEXCOORD0;
            };
 
            struct v2f{
                float4 vertex   : SV_POSITION;
                half2 texcoord  : TEXCOORD0;
                half2 texcoord2  : TEXCOORD1;
            };
           
            sampler2D _MainTex;
            sampler2D _BlendTex;
			float4 _BlendTex_ST;
			float _XScrollSpeed;
			float _YScrollSpeed;
			fixed4 _Color;
            
 
            v2f vert(appdata_t IN){
                v2f OUT;
                OUT.vertex = UnityObjectToClipPos(IN.vertex);
                OUT.texcoord = IN.texcoord;
                OUT.texcoord2 = TRANSFORM_TEX(IN.texcoord, _BlendTex);
#ifdef PIXELSNAP_ON
                OUT.vertex = UnityPixelSnap (OUT.vertex);
#endif
                return OUT;
            }
 
           
 
            fixed4 frag(v2f IN) : COLOR
            {
                fixed4 color = tex2D(_MainTex, IN.texcoord);				

				fixed2 scrollUV = IN.texcoord2;
				fixed xScrollValue = _XScrollSpeed * _Time.x;
				fixed yScrollValue = _YScrollSpeed * _Time.x;
				scrollUV += fixed2( xScrollValue, yScrollValue );
			
				fixed4 mainText;
				fixed4 decalColor = tex2D(_BlendTex, scrollUV);
				color.rgb =  color.rgb*(1-decalColor.a) + decalColor.rgb*decalColor.a;
				return color;
            }
        ENDCG
        }
    }
    Fallback "Sprites/Default"
}
