﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/UI/BoostEffect"
{
	Properties
	{
		[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
		_Color("Tint", Color) = (1,1,1,1)

		[NoScaleOffset] _RotatedTex("Texture", 2D) = "white" {}
		_Rotation("Rotation", Range(-1,1)) = 0.0
		[NoScaleOffset] _RotatedTex2("Texture", 2D) = "white" {}
		_Rotation2("Rotation2", Range(-1,1)) = 0.0
		_Illumination("Illumination", Range(0.1,10)) = 1.0

		_StencilComp("Stencil Comparison", Float) = 8
		_Stencil("Stencil ID", Float) = 0
		_StencilOp("Stencil Operation", Float) = 0
		_StencilWriteMask("Stencil Write Mask", Float) = 255
		_StencilReadMask("Stencil Read Mask", Float) = 255

		_ColorMask("Color Mask", Float) = 15

		[Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip("Use Alpha Clip", Float) = 0
	}

		SubShader
	{
		Tags
	{
		"Queue" = "Transparent"
		"IgnoreProjector" = "True"
		"RenderType" = "Transparent"
		"PreviewType" = "Plane"
		"CanUseSpriteAtlas" = "True"
	}

		Stencil
	{
		Ref[_Stencil]
		Comp[_StencilComp]
		Pass[_StencilOp]
		ReadMask[_StencilReadMask]
		WriteMask[_StencilWriteMask]
	}

		Cull Off
		Lighting Off
		ZWrite Off
		ZTest[unity_GUIZTestMode]
		//Blend SrcAlpha OneMinusSrcAlpha
		Blend SrcColor One

		ColorMask[_ColorMask]

		Pass
	{
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag

		#include "UnityCG.cginc"
		#include "UnityUI.cginc"

#pragma multi_compile __ UNITY_UI_ALPHACLIP

	struct appdata_t
	{
		float4 vertex   : POSITION;
		float4 color    : COLOR;
		float2 texcoord : TEXCOORD0;
	};

	struct v2f
	{
		float4 vertex   : SV_POSITION;
		fixed4 color : COLOR;
		half2 texcoord  : TEXCOORD0;
		float4 worldPosition : TEXCOORD1;
		float2 rotatedUV: TEXCOORD2;
		float2 rotatedUV2: TEXCOORD3;
	};

	fixed4 _Color;
	fixed4 _TextureSampleAdd;
	float4 _ClipRect;

	sampler2D _RotatedTex;
	sampler2D _RotatedTex2;
	float _Rotation;
	float _Rotation2;
	float _Illumination;

	//***************************************************************
	float2 rotateUV(float2 uv, float degrees)
	{
		// rotating UV
		const float Deg2Rad = (UNITY_PI * 2.0) / 360.0;

		float rotationRadians = degrees * Deg2Rad; // convert degrees to radians
		float s = sin(rotationRadians); // sin and cos take radians, not degrees
		float c = cos(rotationRadians);

		float2x2 rotationMatrix = float2x2(c, -s, s, c); // construct simple rotation matrix

		uv -= 0.5; // offset UV so we rotate around 0.5 and not 0.0
		uv = mul(rotationMatrix, uv); // apply rotation matrix
		uv += 0.5; // offset UV again so UVs are in the correct location

		return uv;
	}
	//***************************************************************


	v2f vert(appdata_t IN)
	{
		v2f OUT;
		OUT.worldPosition = IN.vertex;
		OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);

		OUT.texcoord = IN.texcoord;

#ifdef UNITY_HALF_TEXEL_OFFSET
		OUT.vertex.xy += (_ScreenParams.zw - 1.0)*float2(-1,1);
#endif

		OUT.rotatedUV = rotateUV(IN.texcoord, _Rotation*_Time * 100);
		OUT.rotatedUV2 = rotateUV(IN.texcoord, _Rotation2* _Time * 100);

		OUT.color = IN.color * _Color;
		return OUT;
	}

	sampler2D _MainTex;

	fixed4 frag(v2f IN) : SV_Target
	{
		half4 color = (tex2D(_MainTex, IN.texcoord) + _TextureSampleAdd) * IN.color;

		// rotated texture
		fixed4 c2 = tex2D(_RotatedTex, IN.rotatedUV);
		fixed4 c3 = tex2D(_RotatedTex2, IN.rotatedUV2);

		// blend the two together so we can see them
		color = color * c2*c3*_Illumination;

		color.a *= UnityGet2DClipping(IN.worldPosition.xy, _ClipRect);

#ifdef UNITY_UI_ALPHACLIP
		clip(color.a - 0.001);
#endif

		return color;
	}
		ENDCG
	}
	}
}
