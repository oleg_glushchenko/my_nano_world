using MultiLanguageSystem;
using UnityEngine;
using UnityEngine.UI;

public class UIRegularTextMLS : MonoBehaviour 
{
	[SerializeField]
	private string _localizedKey = "";
	
	[SerializeField]
	private bool _isRichText = true;
	
    private Text _text;

	private void Awake()
	{
		_text = GetComponent<Text>();
	}
	
	private void Start() 
	{
		if (LocalizationMLS.Instance != null)
		{
			LocalizationMLS.Instance.OnChangedLanguage += OnChangeLanguage;

			if (LocalizationMLS.Instance.IsInited)
			{
				OnChangeLanguage();
			}
		}
	}
	
    private void OnDestroy()
    {
	    if (!LocalizationMLS.IsQuitting && LocalizationMLS.Instance != null)
	    {
		    LocalizationMLS.Instance.OnChangedLanguage -= OnChangeLanguage;
	    }
    }

	private void OnChangeLanguage()
	{
		if (!LocalizationMLS.IsQuitting && LocalizationMLS.Instance != null)
		{
			if (_localizedKey == "")
			{
				Debug.LogWarning(string.Format("No key assigned for {1}/{0}", gameObject.name,
					transform.parent != null ? transform.parent.gameObject.name : ""));
			}

			Utility.SetValueText(_text, Utility.StringProperty(LocalizationMLS.Instance.GetText(_localizedKey, _isRichText)));

			if (LocalizationMLS.Instance.IsRtlLanguage)
			{
				_text.text = Utility.ReverseText(_text.text);
			}
		}
	}
}