﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;

public class MLSFileReader
{
	public enum EMLSFileType
	{
		CSV
	}

	public static List<MLSLanguage> ReadLanguadesFormFile( string filePath, EMLSFileType fileType )
	{
		return ReadLanguadesFormString(File.ReadAllText(filePath), fileType);
	}
	
	public static List<MLSLanguage> ReadLanguadesFormString( string containerText, EMLSFileType fileType )
	{
		switch (fileType)
		{
			case EMLSFileType.CSV: return CSVContainerRead(containerText);
		}
		
		Debug.LogError("MLSFileReader::ReadLanguadesFormString, return NULL!");
		return null;
	}
	
	public static System.Action<float> Progress;
	
	public static List<MLSLanguage> CSVContainerRead(string srcCSV)
	{
		List<MLSLanguage> result = new List<MLSLanguage>();
		int lineNumber = 0;
		
		System.Action<List<string>> LineReadDone = (list) =>
			{
				if( lineNumber == 1 )
				{
					for (int i = 1; i < list.Count; i++)
					{
						var lng = new MLSLanguage(list[i]);
						result.Add(lng);
					}
				}
				if( lineNumber >= 2)
				{
					for (int i = 1; i < list.Count; i++)
					{
						result[i-1].Add(EMLSObjectType.String, list[0], list[i]);
					}
				}
				lineNumber++;
			};
		
		#region Parce CSV
		int totalSize = srcCSV.Length;
		int charIdx = 0;
		List<string> currentRow = new List<string>();
		string currentCell = "";
		bool inQuotes = false;
		while (charIdx < totalSize)
		{
			char c = srcCSV[charIdx++];
			switch (c)
			{
				case '"':
					{
						if (!inQuotes)
						{
							inQuotes = true;
						}
						else
						{
							if( charIdx >= totalSize )
							{
								break;
							}
							if (srcCSV[charIdx] == '"')
							{
								currentCell += "\"";
								charIdx++;
							}
							else
							{
								inQuotes = false;
							}
						}
						break;
					}
				case ',':
					{
						if (inQuotes)
						{
							currentCell += c;
						}
						else
						{
							currentRow.Add(currentCell);
							currentCell = "";
							if (Progress != null)
							{
								Progress(charIdx/totalSize);
							}
						}
						break;
					}
				case '\n':
					{
						if (inQuotes)
						{
							currentCell += c;
						}
						else
						{
							currentRow.Add(currentCell);
							LineReadDone( currentRow );
							currentRow.Clear();
							currentCell = "";
							if (Progress != null)
							{
								Progress(charIdx/totalSize);
							}
						}
						break;
					}
				default:
					{
						if (c != '\r')
						{
							currentCell += c; 
						}
						break;
					}
			}
		}
		
		currentRow.Add(currentCell);
		LineReadDone( currentRow );
		currentRow.Clear();
		
		if (Progress != null)
		{
			Progress(charIdx/totalSize);
		}
		#endregion
		return result;
	}
	
}