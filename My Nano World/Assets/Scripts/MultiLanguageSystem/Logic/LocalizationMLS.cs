﻿using System;
using UnityEngine;
using System.Globalization;
using System.Linq;
using I2.Loc;
using NanoLib.Core.Logging;
using NanoReality.Game.Data;
using NanoReality.GameLogic.Utilites;
using NanoReality.StrangeIoC;

public class LocalizationMLS : AViewMediator
{
	private const string DefLangPrefs = "mls_def_lang";
	private const string EnglishLanguageCode = "en";
	public const string ArabicLanguageCode = "ar";
	
	public static bool IsQuitting;
	
	public event Action OnChangedLanguage;

    private static LocalizationMLS instance;

    public static LocalizationMLS Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<LocalizationMLS>();
            }

            return instance;
        }
    }

    public bool InitOnAwake;

    public bool IsInited
    {
        get => _isInited;
        private set
        {
            _isInited = value;
            if(OnInited != null && value) OnInited();
        }
    }
    public Action OnInited = () => { };
    private bool _isInited;

    public string Language = "en";
	public string DefaultLanguage = "en";
	public string LanguagenativeName = "English";
	public string LanguageEnglishName = "English";
	
	public MLSUtility.MLSLanguagesContainer LanguagesContainer;

	public bool UseOnlyEnglish;
	public bool UseDefaultLanguage;
	public bool UseDeviceLanguageOnStart;

	[Inject] public IBuildSettings jBuildSettings { get; set; }    

    public bool IsRtlLanguage { get; private set; }

    protected override void PreRegister()
    {
        
    }

    protected override void OnRegister()
    {
        if (IsInited || !InitOnAwake)
            return;

        Init();
    }

    protected override void OnRemove()
    {
        
    }

    public void Init()
	{
        LanguagesContainer = MLSUtility.GetLanguagesContainer();

		if (jBuildSettings.ForceEnglishLanguage)
		{
			ChangeLanguage(EnglishLanguageCode); 
			IsInited = true;
			return;
		}

        if (UseOnlyEnglish)
		{
			ChangeLanguage(EnglishLanguageCode); 
			IsInited = true;
			return;
		}

		if (UseDefaultLanguage)
		{
			ChangeLanguage(DefaultLanguage);
			IsInited = true;
			return;
		}

		if (UseDeviceLanguageOnStart)
		{
			if (PlayerPrefs.HasKey(DefLangPrefs))
			{
				DefaultLanguage = PlayerPrefs.GetString(DefLangPrefs);
			}
			else
			{
				var deviceLang = string.IsNullOrEmpty(jBuildSettings.FirstStartLanguage) ? 
					GetSystemLanguage() : jBuildSettings.FirstStartLanguage;

				DefaultLanguage = (!string.IsNullOrEmpty(deviceLang) && deviceLang != "Unknown") ? 
					deviceLang : EnglishLanguageCode;
			}
		}

		if (SearchLang(DefaultLanguage))
		{
			ChangeLanguage(DefaultLanguage);
		}
		else if (LanguagesContainer.AvailableLanguages.Count > 0)
		{
			ChangeLanguage(SearchLang(EnglishLanguageCode) ? EnglishLanguageCode : LanguagesContainer.AvailableLanguages[0].Name);
		}
		else
		{
			Debug.LogError("No language is available!");
		}
		IsInited = true;
	}

    private bool SearchLang(string langName)
    {
        return LanguagesContainer.AvailableLanguages.Any(avail => avail.Name == langName);
    }

    public static string ClearRichText( string val )
    {
	    if (val == null)
	    {
		    return string.Empty;
	    }

	    // TODO: create logic for rich text cleaninig
	    val = val.Replace("<Color = # ", "<color=#");
	    val = val.Replace("<color = # ", "<color=#");

	    val = val.Replace("</ color", "</color");
	    val = val.Replace("</ Color", "</color");

	    val = val.Replace("</ del color", "</color");
	    val = val.Replace("</ Del Color", "</color");
	    val = val.Replace("</ Del color", "</color");

	    val = val.Replace("< b", "<b");
	    val = val.Replace("< B", "<b");

	    val = val.Replace("</ b", "</b");
	    val = val.Replace("</ B", "</b");

	    val = val.Replace("< i", "<i");
	    val = val.Replace("< I", "<i");

	    val = val.Replace("</ i", "</i");
	    val = val.Replace("</ I", "</i");

	    val = val.Replace("<br>", "\n");
	    val = val.Replace("<BR>", "\n");

	    return val;
    }

	public bool IsArabicChar(char glyph)
	{
		if (glyph >= 0x600 && glyph <= 0x6ff) return true;
		if (glyph >= 0x750 && glyph <= 0x77f) return true;
		if (glyph >= 0xfb50 && glyph <= 0xfc3f) return true;
		if (glyph >= 0xfe70 && glyph <= 0xfefc) return true;
		
		return false;
	}

	public string GetText(string key, bool isRichText = false)
	{
		if (!IsInited)
		{
			//Init();
			return string.Empty;
		}
		
		//TODO: we don't use LocalizationMLS more. now any access by key to LocalizationMLS will return the result from L2
		var val =LocalizationUtils.Localize(key);
		//LocalizationDictionary.TryGetValue(key, out var val);
		if (isRichText)
		{
			val = ClearRichText(val);
		}
		if (string.IsNullOrEmpty(val))
		{
			Logging.Log(LoggingChannel.Core, $"Invalid Key: [{key}] for language: {Language}\nGameObject: {gameObject.name}");
			return key;
		}
		return val;
	}

    private bool CheckIsRtlLanguage(string language)
    {
        switch (language)
        {
            case "ar":
                return true;
        }
        return false;
    }
    
    public void ChangeLanguage(string language)
	{
		if (!jBuildSettings.AvailableLanguages.Contains(language))
		{
			language = !jBuildSettings.AvailableLanguages.Contains(EnglishLanguageCode) ? 
				jBuildSettings.AvailableLanguages.FirstOrDefault() : EnglishLanguageCode;
		}
		
		SetDefaultLanguage(language);
		Language = language;
        IsRtlLanguage = CheckIsRtlLanguage(Language);

        LocalizationManager.CurrentLanguageCode = language;

		OnChangedLanguage?.Invoke();
	}
	
	public string GetSystemLanguage()
	{
		if(Application.systemLanguage == SystemLanguage.Unknown)
		{
			Debug.LogWarning("The system language of this application is Unknown");
			return "Unknown";
		}

		string systemLanguage = Application.systemLanguage.ToString();
		CultureInfo[] cultureInfos = CultureInfo.GetCultures(CultureTypes.AllCultures);
		foreach(CultureInfo info in cultureInfos)
		{
			if(info.EnglishName == systemLanguage)
			{
				return info.Name;
			}
		}
		Debug.LogError("A system language of this application is could not be found!");
		return "System Language not found!";
	}

	private void SetDefaultLanguage(string languageName)
	{
		PlayerPrefs.SetString(DefLangPrefs, languageName);
	}
	
	private void OnApplicationQuit()
	{
		IsQuitting = true;
	}
}
