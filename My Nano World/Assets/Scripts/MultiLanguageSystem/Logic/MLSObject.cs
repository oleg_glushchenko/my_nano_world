﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

[System.Serializable]
public class MLSObject
{
	[SerializeField]
	EMLSObjectType _type = EMLSObjectType.None;
	public EMLSObjectType ObjectType
	{
		get
		{
			return _type;	
		}
		set
		{
			_type = value;	
		}
	}
	
	public string Key;
	public string Value;
	
	
	public MLSObject( )
	{

	}
	
	public MLSObject( EMLSObjectType type, string key, string val )
	{
		ObjectType = type;
		Key = key;
		Value = val;
	}

	public static explicit operator JSONClass(MLSObject mls)
	{
		var jClass = new JSONClass();

		jClass["typ"].AsInt = (int)mls._type;
		jClass["key"] = mls.Key;
		jClass["val"] = mls.Value;
		
		return jClass;
	}
	
	public static explicit operator MLSObject( JSONClass jClass)
	{
		return new MLSObject((EMLSObjectType)jClass["typ"].AsInt, jClass["key"].Value, jClass["val"].Value);
	}
}