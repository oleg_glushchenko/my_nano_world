﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Multy Language System object type.
/// </summary>
public enum EMLSObjectType
{
	None	= 0,
	String	= 1	
}