﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
using MultiLanguageSystem;

[CanEditMultipleObjects]
[CustomEditor( typeof( UITextMLS ) )]
public class LocalizeTextInspector : Editor
{
	UITextMLS _current;
	string _searchText = "";
	bool _error;
	MLSLanguage _lang;
    private Vector2 _keysScroll;
	
	void Awake()
	{
		_current = (target as UITextMLS);
	}
	
	public override void OnInspectorGUI()
	{
		EditorGUILayout.Space();

		if (_current.KeySetted)
		{
			if (Application.isPlaying)
			{
				EditorGUILayout.LabelField("Language: " + LocalizationMLS.Instance.DefaultLanguage);
			}
			if (GUILayout.Button("Apply value"))
			{
				var text = _current.GetComponent<Text>();
				if (text != null)
				{
					var val = _lang.LanguageObjects.FirstOrDefault(o=>o.Key == _current.LocalizedKey);
					if (val != null)
					{
						Utility.SetValueText(text, Utility.StringProperty( LocalizationMLS.ClearRichText( val.Value) ,_current.TextProperties));
						EditorUtility.SetDirty(_current);
						Repaint();
					}
					else
					{
						Debug.LogError("Language dont have value!");
					}
				}
			}
			_current.LocalizedKey = EditorGUILayout.TextField(_current.LocalizedKey).ToUpper();
			if (GUI.changed)
			{
				_searchText = _current.LocalizedKey.ToUpper();
				_current.KeySetted = false;
			}
		}
		else
		{
			if (_error || _lang == null || _lang.LanguageObjects.Count == 0)
			{
				EditorGUILayout.HelpBox("Go to: \"MLS\"-> \"Load language\", and upload or create language file", MessageType.Error );
			}
			else
			{
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.LabelField("Input Key:");
				_searchText = EditorGUILayout.TextField(_searchText).ToUpper();
				EditorGUILayout.EndHorizontal();

				bool didSearch = !string.IsNullOrEmpty(_searchText);
			    _keysScroll = EditorGUILayout.BeginScrollView(_keysScroll);
			    foreach (MLSObject t in _lang.LanguageObjects)
				{
					if (t.ObjectType != EMLSObjectType.String)
					{
						continue;
					}

					if (didSearch && !t.Key.ToUpper().Contains(_searchText))
					{
						continue;
					}

					if (GUILayout.Button(t.Key + "\n[" + t.Value + "]", GUILayout.Width(Screen.width - 45) ))
					{
						_current.LocalizedKey = t.Key;
						_current.KeySetted = true;
						Repaint();
					}
				}
			    EditorGUILayout.EndScrollView();
			}
		}
	
		serializedObject.Update();
		SerializedProperty tps = serializedObject.FindProperty ("TextProperties");
		EditorGUI.BeginChangeCheck();
		EditorGUILayout.PropertyField(tps, true);
		if (EditorGUI.EndChangeCheck())
		{
			serializedObject.ApplyModifiedProperties();
		}

		_current.IsRichText = EditorGUILayout.Toggle("Is Rich Text", _current.IsRichText);
		
	}
	
}