﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor( typeof( LocalizationMLS ) )]
public class LocalizationMLSInspector : Editor
{
	LocalizationMLS _current;

	void Awake()
	{
		_current = (target as LocalizationMLS);
	}
	
	public override void OnInspectorGUI()
	{
		base.DrawDefaultInspector();
		
		if (Application.isPlaying && _current != null && _current.LanguagesContainer != null)
		{
			EditorGUILayout.LabelField("For testing languages change, select language below");
			for (int i = 0; i < _current.LanguagesContainer.AvailableLanguages.Count; i++)
			{
				if (GUILayout.Button( _current.LanguagesContainer.AvailableLanguages[i].EnglishName, GUILayout.Width(Screen.width - 45) ))
				{
					_current.ChangeLanguage(_current.LanguagesContainer.AvailableLanguages[i].Name);
				}
			}
		}
	}
	
}