﻿using UnityEditor;
using UnityEngine;

public class MMLSEditorWindow : EditorWindow, IMLSEditorWindow
{
	protected GUIStyle foldout;
	
	void OnEnable()
	{
		Initialize();
	}
	void OnFocus()
	{
		Initialize();
	}
	void OnProjectChange()
	{
		Initialize();
	}
	public bool LayoutInitialized = false;
	public void InitLayout()
	{
		if (!LayoutInitialized)
		{
			foldout = EditorStyles.foldout;
			foldout.fixedHeight = 18;
			LayoutInitialized = true;
		}
	}
	
	public virtual void Initialize()
	{
		// action when window show
	}
	
	int _intentOffset = 16;
	public GUIStyle BoxGUIStyle( int intent )
	{
		var box = new GUIStyle("Box");
		var off = intent * _intentOffset;
		if (off == 0)
		{
			off = 5;
		}
		box.margin  = new RectOffset(off,4,0,0);
		box.padding = new RectOffset(2,2,6,6);
		return box;
	}
}