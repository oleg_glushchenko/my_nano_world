﻿using NanoLib.Services.InputService;
using Assets.NanoLib.Utilities.Pulls;
using Assets.Scripts.Engine.BuildingSystem.CityMap;
using Assets.Scripts.Engine.BuildingSystem.CityMap.Highlightings;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.BuildingSystem.MapObjects.Components;
using Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks;
using Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks.TaskLinks;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Controllers;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.api.CityOrderDesks;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.api.Mines;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.api.OrderDesks;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl.Factories;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl.Mines;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.api;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.impl;
using Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks.TaskLinks;
using Engine.UI.Extensions.ThereIsNoVideoPanel;
using GameLogic.BuildingSystem.CityArea;
using NanoReality.Core.RoadConstructor;
using GameLogic.Quests.Models.api.Extensions;
using GameLogic.Quests.Models.impl.Extensions;
using NanoReality.Engine.BuildingSystem.CityCamera;
using NanoReality.Engine.BuildingSystem.CityMap;
using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.BuildingSystem.MapObjects.Components;
using NanoReality.Engine.BuildingSystem.MapObjects.ConstractAnimations;
using NanoReality.Engine.UI.Extensions.EditModePanels;
using NanoReality.Engine.UI.Views.ConstractConfirmPanel;
using NanoReality.Game.Attentions;
using NanoReality.Game.CitizenGifts;
using NanoReality.Game.FoodSupply;
using NanoReality.Game.Production;
using NanoReality.Game.Services.BuildingsUnlockService;
using NanoReality.Game.Services.OrderDesk.SeaportOrderDesk;
using NanoReality.Game.Services.SectorSevices;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem;
using NanoReality.GameLogic.BuildingSystem.CityArea;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Impl;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Impl;
using NanoReality.GameLogic.BuildingSystem.GlobalCityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.GlobalCityMap.Models;
using NanoReality.GameLogic.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Controllers;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Controllers.Agricultural;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Controllers.Factories;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.Happiness;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.impl;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.impl.Factories;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.SupplyFood;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.GameManager.Controllers;
using NanoReality.GameLogic.OrderDesk;
using NanoReality.GameLogic.Orders;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.TrafficSystem;
using NanoReality.UI.Components;

namespace NanoReality
{
    public partial class NanoRealityGameContext
    {
        private void BuildingSystemBinds()
        {
            BuildingsBinds();
            CityMapLogicBings();
            CityCameraBinds();
            ConditionsBinds();
            ConditionsLinksBinds();

            injectionBinder.Bind<IObjectsPull>().To<MObjectsPull>();
            injectionBinder.Bind<IObjectPullWithPeek>().To<MObjectsPull>();
            injectionBinder.Bind<IBuildingProduceHappiness>().To<HappinessBuilding>();
            injectionBinder.Bind<SchoolBuilding>().To<SchoolBuilding>().ToInject(true);
            injectionBinder.Bind<HospitalBuilding>().To<HospitalBuilding>().ToInject(true);
            injectionBinder.Bind<PoliceBuilding>().To<PoliceBuilding>().ToInject(true);
            commandBinder.Bind<SignalEntertainmentBuildingNeedToShipMoney>();

            injectionBinder.Bind<IMapBuilding>().To<MapBuilding>();

            injectionBinder.Bind<IConstructionController>().To<ConstructionController>().ToSingleton();
            injectionBinder.Bind<BuildingConstructor>().ToSingleton(); 

            injectionBinder.Bind<RoadConstructor>().ToSingleton();
            injectionBinder.Bind<RoadConstructorContext>().ToSingleton();
            
            injectionBinder.Bind<IBuildingsUnlockService>().To<BuildingsUnlockService>().ToSingleton();

            injectionBinder.Bind<IProductsToUnlockItem>().To<ProductsToUnlockItem>();
            injectionBinder.Bind<IProductsToUnlock>().To<ProductsToUnlock>();

            commandBinder.Bind<SignalOnConstructControllerChangeState>();
            commandBinder.Bind<SignalOnBuildingShipAward>();
            commandBinder.Bind<SignalOnMovedMapObjectOnServer>();
            commandBinder.Bind<MoveVehicleSignal>();
            commandBinder.Bind<MoveCitizenSignal>();
            commandBinder.Bind<SignalOnMovedVehicle>();
            commandBinder.Bind<SignalOnMovedCitizen>();

            commandBinder.Bind<SignalOnMountObjects>();
            commandBinder.Bind<SignalOnDeleteRoads>();

            commandBinder.Bind<SignalOnEditModeHappinessUpdate>();
            commandBinder.Bind<SignalOnEditModeStorageUpdate>();
            commandBinder.Bind<SignalOnEditModeAOEStateChange>();
            commandBinder.Bind<SignalOnMapBuildingsUpgradeStarted> ();
            commandBinder.Bind<SignalOnMapBuildingUpgradeVerificated>();
            commandBinder.Bind<MapBuildingVerifiedSignal>();
            commandBinder.Bind<MapBuildingSyncSignal>();
        }

        private void ConditionsBinds()
        {
            injectionBinder.Bind<IBuildingBuildCondition>().To<HaveBuildingCondition>();
            injectionBinder.Bind<IBusinessSectorOpendCondition>().To<BusinessSectorOpenedCondition>();
            injectionBinder.Bind<IPopulationAchivedCondition>().To<PopulationAchivedCondition>();
            injectionBinder.Bind<IUserLevelAchievedCondition>().To<UserLevelAchivedCondition>();
            injectionBinder.Bind<IBuildBuildingsCondition>().To<ConstructBuildingCondition>();
            injectionBinder.Bind<IBuildBuildingByTypeCondition>().To<BuildBuildingByTypeCondition>();
            injectionBinder.Bind<IProductsProducedCondition>().To<ProductsProducedCondition>();
            injectionBinder.Bind<IProductsTotalProducedCondition>().To<MProductsTotalProducedCondition>();
            injectionBinder.Bind<IUniqueProductsAmountProducedCondition>().To<MUniqueProductsAmountProducedCondition>();
            injectionBinder.Bind<ICollectCoinsFormCommercialBuildingsCondition>().To<MCollectCoinsFromCommercialBuildingsCondition>();
            injectionBinder.Bind<IResearchCondition>().To<ResearchCondition>();
            injectionBinder.Bind<IAmountFriendsCondition>().To<AmountFriendsCondition>();
            injectionBinder.Bind<IInviteFriendsCondition>().To<InviteFriendsCondition>();
            injectionBinder.Bind<IAuctionSalesCondition>().To<AuctionSalesCondition>();
            injectionBinder.Bind<IAuctionPurchaseCondition>().To<AuctionPurchaseCondition>();
            injectionBinder.Bind<IAuctionSpendCoinsCondition>().To<AuctionSpendCoinsCondition>();
            injectionBinder.Bind<IAuctionGetCoinsCondition>().To<AuctionGetCoinsCondition>();
            injectionBinder.Bind<IPopulationSatisfactionCondition>().To<MPopulationSatisfactionCondition>();
            injectionBinder.Bind<IUnlockSectorForConstruction>().To<UnlockSectorForConstruction>();
            injectionBinder.Bind<ICompleteOrderDeskCondition>().To<CompleteOrderDeskCondition>();
            injectionBinder.Bind<IQuestsCompletedCondition>().To<MQuestsCompletedCondition>();
            injectionBinder.Bind<IBuildingUnlockedCondition>().To<MBuildingUnlockedCondition>();
            injectionBinder.Bind<IGoldGainedCondition>().To<MGoldGainedCondition>();
            injectionBinder.Bind<IPremiumGoldGainedCondition>().To<MPremiumGoldGainedCondition>();
            injectionBinder.Bind<ICityRenamedCondition>().To<CityRenamedCondition>();
            injectionBinder.Bind<IRepairBuildingCondition>().To<RepairBuildingCondition>();
            injectionBinder.Bind<ICollectTaxesCondition>().To<CollectTaxesCondition>();
            injectionBinder.Bind<ICertainQuestDoneCondition>().To<CertainQuestDoneCondition>();
            injectionBinder.Bind<ILoginSocialNetworkCondition>().To<LoginSocialNetworkCondition>();
            injectionBinder.Bind<IUpgradePowerStationCondition>().To<UpgradePowerStationCondition>();
            injectionBinder.Bind<IUnknownCondition>().To<UnknownCondition>();
            injectionBinder.Bind<IConnectBuildingToRoadCondition>().To<ConnectBuildingToRoadCondition>();
            injectionBinder.Bind<ICoverAmountBuildingsByServiceCondition>().To<CoverAmountBuildingsByServiceCondition>();
            injectionBinder.Bind<IShuffleOrderTableCondition>().To<ShuffleOrderTableCondition>();
            injectionBinder.Bind<IBazaarPurchaseLootBoxCondition>().To<BazaarPurchaseLootBoxCondition>();
            injectionBinder.Bind<IBazaarPurchaseProductCondition>().To<BazaarPurchaseProductCondition>();
            injectionBinder.Bind<IBazaarSpendGoldBarCondition>().To<BazaarSpendGoldBarCondition>();
            injectionBinder.Bind<IFoodSupplyFillBarCondition>().To<FoodSupplyFillBarCondition>();
            injectionBinder.Bind<IFoodSupplyCookMealsCondition>().To<FoodSupplyCookMealsCondition>();
            injectionBinder.Bind<IFoodSupplyFillingBarCondition>().To<FoodSupplyFillingBarCondition>();
            injectionBinder.Bind<IReceiveProductFromOrderCondition>().To<ReceiveProductFromOrderCondition>();
            
            injectionBinder.Bind<IBuildingConstructByIdCondition>().To<BuildingConstructByIdCondition>();
            injectionBinder.Bind<IBuildingHaveByIdCondition>().To<BuildingHaveByIdCondition>();
            injectionBinder.Bind<IBuildingConstructByTypeCondition>().To<BuildingConstructByTypeCondition>();
            injectionBinder.Bind<IBuildingHaveByTypeCondition>().To<BuildingHaveByTypeCondition>();
            injectionBinder.Bind<IConstructBuildingWithSubCategoryCondition>().To<ConstructBuildingWithSubCategoryCondition>();
            injectionBinder.Bind<ISpendCurrencyCondition>().To<SpendCurrencyCondition>();
            injectionBinder.Bind<IObtainSpecificCurrencyCondition>().To<ObtainSpecificCurrencyCondition>();
            injectionBinder.Bind<IBuildingHaveWithSubCategoryCondition>().To<BuildingHaveWithSubCategoryCondition>();
            
            injectionBinder.Bind<IDateTimeCondition>().To<DateTimeCondition>();
        }

        private void ConditionsLinksBinds()
        {
            injectionBinder.Bind<IConditionTaskRouter>().To<ConditionTaskRouter>();           

            injectionBinder.Bind<IOpenBuildingsShopLink>().To<OpenBuildingsShopLink>();           
            injectionBinder.Bind<IOpenProductProduceLink>().To<OpenProductProduceLink>();
            injectionBinder.Bind<IOpenOrderDeskWithSectorkLink>().To<OpenOrderDeskWithSectorkLink>();           
            injectionBinder.Bind<IOpenOrderDeskLink>().To<OpenOrderDeskLink>();           
            injectionBinder.Bind<IOpenAuctionLink>().To<OpenAuctionLink>();           
            injectionBinder.Bind<ICityRenameLink>().To<CityRenameLink>();           
            injectionBinder.Bind<ICollectCoinsFromCommercialBuildingLink>().To<CollectCoinsFromCommercialBuildingLink>();           
            injectionBinder.Bind<IAuctionCoinsLink>().To<AuctionCoinsLink>();           
            injectionBinder.Bind<ICollectTaxesLink>().To<CollectTaxesLink>();
            injectionBinder.Bind<IGoToLockedSectorLink>().To<GoToLockedSectorLink>();           
            injectionBinder.Bind<ICoverBuildingsWithAreaOfEffectTask>().To<CoverBuildingsWithAreaOfEffectTask>();           
            injectionBinder.Bind<IUpgradeBuildingTask>().To<UpgradeBuildingTask>();
            injectionBinder.Bind<IOpenBazaarLink>().To<OpenBazaarLink>();
            injectionBinder.Bind<IOpenFoodSupplyLink>().To<OpenFoodSupplyLink>();
            injectionBinder.Bind<IPopulationAchievedLink>().To<PopulationAchievedLink>();
        }

        private void BuildingsBinds()
        {
            injectionBinder.Bind<ISkipIntervalsData>().To<MSkipIntervalsData>().ToSingleton();
            injectionBinder.Bind<IMapObjectsGenerator>().To<UnityMapObjectsGenerator>().ToSingleton();
            injectionBinder.Bind<IMapObjectsData>().To<MapObjectsData>().ToSingleton();
            injectionBinder.Bind<IMapObject>().To<MMapObject>();  // base map object
            injectionBinder.Bind<IGroundBlock>().To<GroundBlock>();
            injectionBinder.Bind<ILockedSector>().To<LockedSector>();
            injectionBinder.Bind<ServicesBuildingsService>().To<ServicesBuildingsService>();
            injectionBinder.Bind<IUnlockSectorService>().To<UnlockSectorService>().ToSingleton();

            injectionBinder.Bind<IMapBuildingsSkipData>().To<MapBuildingsSkipData>().ToSingleton();
            commandBinder.Bind<SignalOnBuildingRoadConnectionChanged>();
            commandBinder.Bind<SignalBuildFinishVerificated>();

            commandBinder.Bind<SignalOnBuildingUnlocked>();
            commandBinder.Bind<SignalOnSubSectorUnlocked>();
            commandBinder.Bind<SignalOnSubSectorUnlockedVerified>();
            commandBinder.Bind<SignalOnSubSectorsUpdated>();
            commandBinder.Bind<SignalOnEditModeStateChange>();
            commandBinder.Bind<SignalOnEditModeEraseStateChange>();
            commandBinder.Bind<SignalOnEditModeRemoveBuild>();

            injectionBinder.Bind<IBuildingSitesContainer>().ToValue(ConstractAnimationsContainer).ToInject(false);
            mediationBinder.Bind<BuildingSiteView>().To<ConstractAnimationMediator>();

            #region Road
            injectionBinder.Bind<IRoad>().To<MRoad>();
            commandBinder.Bind<SignalRoadMapReselect>();
            commandBinder.Bind<RoadConstructSetActiveSignal>();

            #endregion

            #region WareHouses

            injectionBinder.Bind<IWarehouseMapObject>().To<WarehouseMapObject>();

            commandBinder.Bind<SignalWarehouseCapacityUpgrade>();

            #endregion

            #region PowerPlants

            commandBinder.Bind<SignalOnBuildingElectricityChanged>();
            injectionBinder.Bind<IPowerPlantBuilding>().To<PowerPlantBuilding>();

            #endregion

            #region DwellingHouses

            injectionBinder.Bind<IDwellingHouseMapObject>().To<MDwellingHouseMapObject>();

            #endregion

            #region EventBuilding

            injectionBinder.Bind<IEventBuilding>().To<EventBuilding>();
            injectionBinder.Bind<IEventBuildingReward>().To<EventBuildingReward>();

            #endregion

            #region CityHalls

            injectionBinder.Bind<ICityHall>().To<MCityHall>();
            #endregion

            #region Factories

            injectionBinder.Bind<IFactoryObject>().To<FactoryObject>();
            injectionBinder.Bind<IProduceSlot>().To<ProduceSlot>();
            injectionBinder.Bind<ISlotPack>().To<SlotPack>();
            injectionBinder.Bind<IProducingItemData>().To<ProducingItemData>();
            commandBinder.Bind<FactoryShipSlotSignal>();
            commandBinder.Bind<SignalFactoryProduceSlotFinished>();
            commandBinder.Bind<SignalOnStartProduceGoodOnFactory>();

            #endregion

            #region Agricultural

            injectionBinder.Bind<IAgroFieldBuilding>().To<AgroFieldBuilding>();
            commandBinder.Bind<OnChangeGrowingState>();
            commandBinder.Bind<AgriculturalPlantSelectedSignal>();
            commandBinder.Bind<OnProductProduceAndCollectSignal>();
            commandBinder.Bind<OnItemDragStartedSignal>();
            commandBinder.Bind<OnItemDragEndedSignal>();

            #endregion

            #region Mine

            injectionBinder.Bind<IMineObject>().To<MineObject>();
            commandBinder.Bind<SignalMineShipSlot>();
            commandBinder.Bind<SignalOnMineProduceSlotFinished>();

            #endregion

            #region Decoration

            injectionBinder.Bind<IDecorationMapObject>().To<DecorationBuilding>();

            #endregion

            #region Order Desk

            injectionBinder.Bind<ISeaportOrderSlot>().To<SeaportOrderSlot>();
            injectionBinder.Bind<IAwardItem>().To<IAwardItem>();
            injectionBinder.Bind<ISeaportAward>().To<SeaportAward>();
            injectionBinder.Bind<ProductRequirementItem>().To<ProductRequirementItem>();
            injectionBinder.Bind<IOrderDeskMapBuilding>().To<OrderDeskMapBuilding>();
            injectionBinder.Bind<ISeaportOrderDeskService>().To<SeaportOrderDeskService>().ToSingleton();

            commandBinder.Bind<LoadSeaportOrdersCommand.ExecuteCommandSignal>().To<LoadSeaportOrdersCommand>();
            commandBinder.Bind<SendSeaportOrderCommand.ExecuteCommandSignal>().To<SendSeaportOrderCommand>();
            commandBinder.Bind<ShuffleSeaportCommand.ExecuteCommandSignal>().To<ShuffleSeaportCommand>();

            commandBinder.Bind<SignalSeaportOrderSent>();

            commandBinder.Bind<SignalOnOrderTruckTripFinished>();
            commandBinder.Bind<SignalOnOrderTruckSlotLoaded>();
            commandBinder.Bind<SignalOnNeedToGetNewOrder>();
            commandBinder.Bind<SignalOnCityOrderAwardTaken>();
            commandBinder.Bind<SignalOnHideOrderDeskPanelView>();
            commandBinder.Bind<SignalOnCityOrdersLoadedSignal>();
            commandBinder.Bind<SignalOnAchievementAttentionTap>();
            commandBinder.Bind<SignalOnTaxesAttentionTap>();
            commandBinder.Bind<SignalOnFoodSupplyAttentionTap>();
            commandBinder.Bind<SignalOnDwellingHouseCoinAttentionTap>();
            commandBinder.Bind<SignalOnBuildingUpgradeAttentionTap>();
            commandBinder.Bind<SignalOnQuestsBuildingQuestsAttentionTap>();
            commandBinder.Bind<SignalOnBuildingAttentionTap>();

            injectionBinder.Bind<ICityOrderDeskMapBuilding>().To<CityOrderDeskMapBuilding>();
            injectionBinder.Bind<IFoodSupplyBuilding>().To<FoodSupplyBuilding>();

            commandBinder.Bind<MetroOrderDeskUpdatedSignal>();
            commandBinder.Bind<MetroOrderShippingSignal>();
            commandBinder.Bind<MetroOrderSlotWaitingFinishedSignal>();
            commandBinder.Bind<MetroOrderSlotLoadedSignal>();
            commandBinder.Bind<MetroOrderTrainShippedSignal>();
            commandBinder.Bind<MetroOrderRewardCollectedSignal>();
            commandBinder.Bind<MetroOrderShuffledSignal>();
            
            injectionBinder.Bind<IMetroOrderDeskService>().To<MetroOrderDeskService>().ToSingleton();

            #endregion

            #region SupplyFood

            injectionBinder.Bind<IFoodSupplyService>().To<FoodSupplyService>().ToSingleton();
            commandBinder.Bind<FoodSupplyChangedSignal>();

            #endregion

            injectionBinder.Bind<IProductionService>().To<ProductionService>().ToSingleton();

            #region Citizen gifts

            injectionBinder.Bind<ICityGiftsService>().To<CityGiftsService>().ToSingleton();

            #endregion

            #region Quests

            injectionBinder.Bind<IQuestsBuilding>().To<MQuestsBuilding>();
            
            #endregion

            #region GlobalMap

            injectionBinder.Bind<ITradingShopBuilding>().To<MTradingShopBuilding>();
            injectionBinder.Bind<ITradingMarketBuilding>().To<TradingMarketBuilding>();
            injectionBinder.Bind<IIntercityRoad>().To<MIntercityRoad>();

            #endregion

            #region CityArea

            injectionBinder.Bind<ICityAreaData>().To<CityAreaData>().ToSingleton();
            injectionBinder.Bind<ISectorArea>().To<SectorArea>();

            commandBinder.Bind<LockedSectorAddedSignal>();// set object on map signal

            #endregion
        }

        private void ProductsBinds()
        {
            injectionBinder.Bind<IProductsData>().To<ProductsData>().ToSingleton();
            injectionBinder.Bind<IProductForUpgrade>().To<ProductForUpgrade>();

            commandBinder.Bind<StartEntertainmentProductionSignal>();
            commandBinder.Bind<EndEntertainmentProductionSignal>();
            commandBinder.Bind<ProductReadySignal>();
            commandBinder.Bind<ApplyFilterToReadyProducts>();
            commandBinder.Bind<StartProductionSignal>();
            commandBinder.Bind<RemoveProductSlotSignal>();
            commandBinder.Bind<CollectProductAboveBuildingStartSignal>();
            commandBinder.Bind<CollectProductAboveBuildingCompleteSignal>();
            commandBinder.Bind<SignalOnProductsStateChanged>();
        }

        private void CityMapLogicBings()
        {
            mediationBinder.Bind<CityMapView>().To<CityMapMediator>();
            injectionBinder.Bind<ICityMap>().To<MCityMap>();
            injectionBinder.Bind<ICityGrid>().To<MCityGrid>();
            injectionBinder.Bind<CityGridCell>().To<CityGridCell>();
            commandBinder.Bind<SignalOnCityMapLoaded>();
            commandBinder.Bind<StartCityMapInitializationSignal>();
            commandBinder.Bind<SignalOnFullClearMap>();
            commandBinder.Bind<FinishCityMapInitSignal>();
            
            injectionBinder.Bind<HighlightController>().To<HighlightController>().ToSingleton();

            #region MapObjects

            commandBinder.Bind<MapObjectAddedSignal>();// set object on map signal
            commandBinder.Bind<SignalMoveMapObjectView>();// move object on map signal
            commandBinder.Bind<SignalOnMapObjectRemoved>();//remove object from map signal
            commandBinder.Bind<SignalOnBuildStared>();
            commandBinder.Bind<SignalOnBuildStartedVerified>();
            commandBinder.Bind<SignalRemoveMapObjectView>();
            commandBinder.Bind<SignalOnMapObjectUpgradedOnMap>();
            commandBinder.Bind<SignalOnStartRoadCreation>();
            commandBinder.Bind<BuildingConstructionStartedSignal>();
            commandBinder.Bind<SignalOnMapObjectBuildFinished>();
            commandBinder.Bind<TileOfRoadEditedSignal>();

            mediationBinder.Bind<MapObjectView>().To<MapObjectMediator>();
            injectionBinder.Bind<IMapObjectViewComponent>().To<MapObjectSelect>().ToName("SelectComponent");
            injectionBinder.Bind<IMapObjectViewComponent>().To<MapObjectDrag>().ToName("DragComponent");
            injectionBinder.Bind<IMapObjectViewComponent>().To<MapObjectStaticAnimations>().ToName("StaticAnimationsComponent");
            injectionBinder.Bind<IMapObjectViewComponent>().To<ProductionBuildingAnimationComponent>().ToName(nameof(ProductionBuildingAnimationComponent));
            injectionBinder.Bind<IMapObjectViewComponent>().To<DwellingObjectAnimationComponent>().ToName(nameof(DwellingObjectAnimationComponent));
            injectionBinder.Bind<IMapObjectViewComponent>().To<ConstructionComponent>().ToName("ConstractAnimationsComponent");
            injectionBinder.Bind<IMapObjectViewComponent>().To<MapObjectSemiTransparentComponent>().ToName("ObjectSemiTransparentComponent");
            injectionBinder.Bind<IMapObjectViewComponent>().To<MotionArrowsComponent>().ToName(nameof(MotionArrowsComponent));
            injectionBinder.Bind<IMapObjectViewComponent>().To<MapObjectProgressBarComponent>().ToName("ProgressBarComponent");
            injectionBinder.Bind<IMapObjectViewComponent>().To<MapObjectsVFXComponent>().ToName("VFXComponent");
            injectionBinder.Bind<IMapObjectViewComponent>().To<BuildingAttentionComponent>().ToName(nameof(BuildingAttentionComponent));
            injectionBinder.Bind<IMapObjectViewComponent>().To<ProductionInProgressComponent>().ToName(nameof(ProductionInProgressComponent));
            injectionBinder.Bind<IMapObjectViewComponent>().To<ConstructPlaceIndicatorComponent>().ToName(nameof(ConstructPlaceIndicatorComponent));
            injectionBinder.Bind<IMapObjectViewComponent>().To<ReadyProductsComponent>().ToName(ReadyProductsComponent.Key);

            injectionBinder.Bind<IMapObjectViewComponent>().To<ColorHighlightComponent>().ToName(nameof(ColorHighlightComponent));
            injectionBinder.Bind<IMapObjectViewComponent>().To<CitizenGiftBuildingComponent>().ToName(nameof(CitizenGiftBuildingComponent));

            injectionBinder.Bind<IMapObjectViewComponent>().To<HighlightHappinesComponent>().ToName(nameof(HighlightHappinesComponent));
            injectionBinder.Bind<IMapObjectViewComponent>().To<AoeHighlightComponent>().ToName(nameof(AoeHighlightComponent));
            injectionBinder.Bind<IMapObjectViewComponent>().To<PowerNeedHighlightComponent>().ToName(nameof(PowerNeedHighlightComponent));
            injectionBinder.Bind<IMapObjectViewComponent>().To<DwellingFoodSupplyHighlightComponent>().ToName(nameof(DwellingFoodSupplyHighlightComponent));
            
            injectionBinder.Bind<IMapObjectViewComponent>().To<DefaultLongTapComponent>().ToName(nameof(DefaultLongTapComponent));
            injectionBinder.Bind<IMapObjectViewComponent>().To<OpenPanelLongTapComponent>().ToName(nameof(OpenPanelLongTapComponent));
            
            commandBinder.Bind<SignalOnMapObjectChangedSelect>();

            mediationBinder.Bind<ThereIsNoVideoPanelView>().To<ThereIsNoVideoPanelMediator>();
            commandBinder.Bind<SignalOnCancelConstractInView>();
            commandBinder.Bind<SignalOnConfirmConstractInView>();
            commandBinder.Bind<SignalOnConfirmConstructSucceed>();

            #endregion

            #region GlobalCityMap

            injectionBinder.Bind<IGlobalCityMap>().To<MGlobalCityMap>().ToSingleton();
            commandBinder.Bind<SignalOnInitGlobalCityMap>();

            #endregion
            
            commandBinder.Bind<CityMapViewInitializedSignal>();
        }

        private void CityCameraBinds()
        {
            injectionBinder.Bind<IGameCamera>().To<MGameCamera>().ToSingleton();  // creating one silgenton game camera
            injectionBinder.Bind<IInputController>().ToValue(InputController).ToInject(false); // attach IInputController as mouse/touch controller
            mediationBinder.Bind<CameraView>().To<CameraMediator>(); //bind camewa wiew with its mediator
            commandBinder.Bind<SignalCameraIgnoreStartZoomOnce>();
            commandBinder.Bind<SignalOnCameraAutoSwipe>();
            commandBinder.Bind<SignalOnCameraAutoSwipeEnded>();
            commandBinder.Bind<OnCameraAutoZoomedSignal>();
            commandBinder.Bind<SignalOnCameraAutoZoomEnded>();
            commandBinder.Bind<GetScreenshotSignal>();
        }
    }
}
