﻿using Assets.NanoLib.UI.Core.Signals;
using Assets.Scripts.Engine.UI.Extensions.MapObjects.Mines;
using Assets.Scripts.Engine.UI.Views.BubbleMessage;
using Assets.Scripts.Engine.UI.Views.Hud;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Replics;
using Engine.UI;
using Engine.UI.Extensions.NextEventUpgradePanel;
using Engine.UI.Extensions.RepairBuildingPanel;
using Engine.UI.Extensions.ThereIsNoVideoPanel;
using Engine.UI.Extensions.TrophyInfoPanel;
using Engine.UI.Extensions.UnlockAreaPanel;
using NanoReality.Engine.UI;
using NanoReality.Engine.UI.Extensions.AchievementPanel;
using NanoReality.Engine.UI.Extensions.MainPanel;
using NanoReality.Engine.UI.Extensions.MapObjects.Warehouse;
using NanoReality.Engine.UI.Extensions.QuestPanel;
using NanoReality.Engine.UI.Views.Energy;
using NanoReality.Engine.UI.Views.Entertaiment;
using NanoReality.Engine.UI.Views.LevelUpPanel;
using NanoReality.Engine.UI.Views.NotEnoughtResourcesPanel;
using NanoReality.Engine.UI.Views.WorldSpaceUI;
using NanoReality.GameLogic.Quests;
using NanoReality.Game.Tutorial;
using NanoReality.GameLogic.Upgrade;
using Engine.UI.Views.BubbleMessage;
using Engine.UI.Views.Warehouse;
using NanoLib.Core.Pool;
using NanoLib.UI.Core;
using NanoReality.Core.SectorLocks;
using NanoReality.Engine.UI.Extensions.Agricultural;
using NanoReality.Engine.UI.Extensions.DailyBonusPanel;
using NanoReality.Engine.UI.Extensions.EditModePanels;
using NanoReality.Engine.UI.Extensions.MapObjects.CityHallTaxes;
using NanoReality.Engine.UI.Views.ConstractConfirmPanel;
using NanoReality.Engine.UI.Views.Hud;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.Engine.UI.Views.TheEvent;
using NanoReality.GameLogic.BuildingSystem.Happiness;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.UI.Components;
using NanoReality.Game.Attentions;
using NanoReality.Game.UI.BalancePanel;
using NanoReality.Game.UI.BuildingsShopPanel;
using NanoReality.Game.UI;
using NanoReality.Game.UI.BazaarPanel;
using NanoReality.Game.UI.SettingsPanel;
using NanoReality.GameLogic.Common;
using NanoReality.Game.UI.FlyingBalanceItemPanel;
using NanoReality.GameLogic.Bank;
using UnityEngine;
using UnityEngine.UI;
using NanoReality.Engine.UI.Views.OffersTimer;

namespace NanoReality
{
    public partial class NanoRealityGameContext
    {
        private void UIsBinds()
        {
            injectionBinder.Bind<IWorldSpaceCanvas>().ToValue(WorldSpaceCanvasPrefab).ToSingleton();
            mediationBinder.Bind<WorldSpaceCanvas>().To<WorldSpaceCanvasMediator>();
            injectionBinder.Bind<IUIManager>().ToValue(UIManagerObject).ToSingleton();
            injectionBinder.Bind<GraphicRaycaster>().To(((Component) UIManagerObject).GetComponent<GraphicRaycaster>()).ToInject(false);
            injectionBinder.Bind<UIFilter>().ToSingleton();
            injectionBinder.Bind<SpriteLoader>().ToSingleton();
            commandBinder.Bind<SignalOnShownPanel>();
            commandBinder.Bind<SignalOnHidePanel>();
            commandBinder.Bind<OnHudBottomPanelIsVisibleSignal>();
            commandBinder.Bind<OnHudBottomPanelWithoutPanelVisibleSignal>();
            commandBinder.Bind<OnHudButtonsIsClickableSignal>();
            commandBinder.Bind<SignalOnRoadControlsSetVisible>();
            commandBinder.Bind<OnPlayerTooltipClickSignal>();
            commandBinder.Bind<PlayerOpenedPanelSignal>();
            commandBinder.Bind<SignalDisableStandardUICanvas>();
            commandBinder.Bind<SignalBlockExitGamePopup>();
            commandBinder.Bind<SignalUnBlockExitGamePopup>();
            commandBinder.Bind<SignalGameAuthorize>();
            commandBinder.Bind<SignalSocialAuthorize>();
            commandBinder.Bind<HideBalancePanelTooltipSignal>();
            commandBinder.Bind<SignalHardCurrencyUpdate>();
            commandBinder.Bind<SignalSoftCurrencyUpdate>();
            commandBinder.Bind<SignalInfoNanoGemsCurrencyUpdate>();
            commandBinder.Bind<SignalWarehouseProductsAmountChanged>();
            commandBinder.Bind<SignalUpdateHappiness>();
            commandBinder.Bind<AddRewardItemSignal>();
            commandBinder.Bind<SignalNanogemsIntro>();
            commandBinder.Bind<SignalPopulationUpdate>();
            commandBinder.Bind<StartFxPopUpAnimationSignal>();
            commandBinder.Bind<SignalOnGoldCurrencyStateChanged>();
            commandBinder.Bind<SignalOnCurrencyStateChanged>();
            commandBinder.Bind<SignalGoldCurrencyUpdate>();
            commandBinder.Bind<SignalAnalyticBuildStared>();
            commandBinder.Bind<SignalHudIsInitialized>();

            mediationBinder.Bind<DailyBonusView>().To<DailyBonusViewMediator>();

            mediationBinder.Bind<UIMediatorContainer>()
                .To<AchievementPanelMediator>()
                .To<AgroFieldPanelMediator>()
                .To<BankMediator>()
                .To<BuildingsShopPanelMediator>()
                .To<CityHallTaxesMediator>()
                .To<DailyBonusViewMediator>()
                .To<DwellingPanelMediator>()
                .To<EnergyPanelMediator>()
                .To<EnergyLayoutPanelMediator>()
                .To<EntertainmentPanelMediator>()
                .To<EntertaimentAoeLaoutPanelMediator>()
                .To<FarmSectorUnlock>()
                .To<FactoryPanelMediator>()
                .To<HudViewMediator>()
                .To<LevelUpPanelMediator>()
                .To<LoaderPanelMediator>()
                .To<MinePanelMediator>()
                .To<RenameCityPanelMediator>()
                .To<TutorialDialogPanelMediator>()
                .To<RequirmentPanelMediator>()
                .To<SettingsPanelMediator>()
                .To<LayoutEditorPanelMediator>()
                .To<EditorPanelMediator>()
                .To<WarehousePanelMediator>()
                .To<FxPopupPanelMediator>()
                .To<RepairBuildingPanelMediator>()
                .To<UpgradeWithProductsPanelMediator>()
                .To<ConstractConfirmPanelMediator>()
                .To<TutorialLookAtMapObjectMediator>()
                .To<TutorialLookAtWorldSpaceMediator>()
                .To<TutorialLookAtOverlayMediator>()
                .To<BalancePanelMediator>()
                .To<UnlockAreaPanelMediator>()
                .To<UpgradePanelMediator>()
                .To<SeaportPanelMediator>()
                .To<SoukPanelMediator>()
                .To<MetroPanelMediator>()
                .To<FlyingBalanceItemMediator>()
                .To<TheEventMediator>()
                .To<FoodSupplyPanelMediator>()
                .To<TrophyInfoPanelMediator>()
                .To<NextEventUpgradeMediator>();

            injectionBinder.Bind<MetroPanelContext>().ToSingleton();
            injectionBinder.Bind<FoodSupplyPanelContext>().ToSingleton();
            injectionBinder.Bind<BankContext>().ToSingleton();

            injectionBinder.Bind<BalancePanelModel>().ToSingleton();
            injectionBinder.Bind<BalancePanelContext>().ToSingleton();

            injectionBinder.Bind<DailyBonusContext>().ToSingleton();
            injectionBinder.Bind<LevelUpContext>().ToSingleton();
            injectionBinder.Bind<GameLoadingModel>().ToSingleton();
            injectionBinder.Bind<AchievementPanelContext>().ToSingleton();
            injectionBinder.Bind<BuildingsShopPanelContext>().To<BuildingsShopPanelContext>().ToSingleton();
            injectionBinder.Bind<ConstructConfirmPanelModel>().ToSingleton();
            injectionBinder.Bind<BuildingShopPanelModel>().ToSingleton();

            injectionBinder.Bind<WarehouseUpgradeContext>().ToSingleton();
            
            injectionBinder.Bind<SettingsPanelContext>().ToSingleton();
            injectionBinder.Bind<RenameCityPanelContext>().ToSingleton();

            mediationBinder.Bind<HudRoadPanelView>().To<HudRoadPanelMediator>();
            injectionBinder.Bind<HudPanelModel>().ToSingleton();
            injectionBinder.Bind<FarmState>().ToSingleton();

            commandBinder.Bind<SignalOnHudViewSoftCurrencyUpdated>();
            commandBinder.Bind<SignalOnHudViewHardCurrencyUpdated>();

            commandBinder.Bind<SetWindowLastSiblingSignal>();
            commandBinder.Bind<ShowWindowSignal>();
            commandBinder.Bind<HideWindowSignal>();
            commandBinder.Bind<WindowClosedSignal>();
            commandBinder.Bind<WindowOpenedSignal>();
            commandBinder.Bind<SignalNeedToChangeLanguage>();
            commandBinder.Bind<InitFlyingPanelSignal>();

            mediationBinder.Bind<OffersTimerGlobalView>().To<OfferTimerGlobalMediator>();

            commandBinder.Bind<SignalOnNeedToHideTooltip>();
            commandBinder.Bind<SignalOnNeedToShowTooltip>();
            commandBinder.Bind<SignalOnNeedToDisableTooltip>();
            commandBinder.Bind<UserLevelUpSignal>().To<FarmSectorUnlockCommand>();

            injectionBinder.Bind<IPopupManager>().ToValue(PopupManager).ToSingleton().ToInject(false);
            injectionBinder.Bind<IUnityPool<ReadyProductView>>().To<UnityPool<ReadyProductView>>().ToSingleton();
            injectionBinder.Bind<IUnityPool<ProduceSlotView>>().To<UnityPool<ProduceSlotView>>().ToSingleton();
            injectionBinder.Bind<IUnityPool<SeaportProductItemView>>().To<UnityPool<SeaportProductItemView>>().ToSingleton();

            QuestPanelBinds();
            AchievementsPanelBinds();
            NotEnoughResourcesPanelBinds();
            ThereIsNoVideoPanelBinds();
            BubbleMessageBinds();
            
            commandBinder.Bind<SwitchBuildingAttentionsSignal>();
            commandBinder.Bind<InteractBuildingAttentionsSignal>();
            commandBinder.Bind<UpdateWorldSpaceDepth>();
            
            injectionBinder.Bind<IBuildingAttentionsController>().To<BuildingAttentionsController>().ToSingleton();
            injectionBinder.Bind<IUnityPool<BuildingStatesAttentionView>>().To<UnityPool<BuildingStatesAttentionView>>().ToSingleton();

            commandBinder.Bind<SignalOnUpgradeProductPushed>();
            commandBinder.Bind<StartSpentProductsAnimationSignal>();
            
            commandBinder.Bind<SignalWarehouseChangeState>();
            commandBinder.Bind<SignalCityOrderDeskChangeState>();
            commandBinder.Bind<SignalBalancePanelViewChangeState>();
            commandBinder.Bind<SignalUpdateBalancePanelCounters>();
            commandBinder.Bind<SignalOnConfirmConstructSucceed>();
            commandBinder.Bind<SignalOnCancelConstruct>();
            commandBinder.Bind<SignalOnCancelConstractWithoutExit>();
            commandBinder.Bind<SignalExperienceUpdate>();

            commandBinder.Bind<PopupOpenedSignal>();
            commandBinder.Bind<PopupClosedSignal>();
        }

        private void QuestPanelBinds()
        {
            mediationBinder.Bind<QuestPanelView>().To<QuestPanelMediator>();
            commandBinder.Bind<SignalOnQuestRequirmentItemClick>();
            commandBinder.Bind<SignalOnQuestsShowedByUser>();
            commandBinder.Bind<HighlightHudQuestButtonSignal>();
        }

        private void AchievementsPanelBinds()
        {
            commandBinder.Bind<ShowAchievementsPanelSignal>();
        }

        private void NotEnoughResourcesPanelBinds()
        {
            commandBinder.Bind<SignalShowNotEnoughtResourcesPanel>();
        }

        private void ThereIsNoVideoPanelBinds()
        {
            commandBinder.Bind<SignalShowThereIsNoVideoPanel>();
        }

        private void BubbleMessageBinds()
        {
            commandBinder.Bind<ShowBubbleMessageSignal>();
            commandBinder.Bind<HideBubbleMessageSignal>();
        }
    }
}
