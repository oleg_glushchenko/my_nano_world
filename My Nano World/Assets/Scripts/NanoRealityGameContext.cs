﻿using System;
using NanoLib.Services.InputService;
using Assets.NanoLib.ServerCommunication.Models;
using NanoLib.Core.Services.Sound;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UtilitesEngine;
using Assets.NanoLib.UtilitesEngine.CustomDebugging.Models.api;
using Assets.NanoLib.Utilities.DataManager.Models.api;
using Assets.NanoLib.Utilities.DataManager.Models.impl;
using Assets.NanoLib.Utilities.Pulls;
using Assets.NanoLib.Utilities.Serializers;
using Assets.Scripts.Engine.Analytics;
using Assets.Scripts.Engine.Analytics.Model.api;
using Assets.Scripts.GameLogic.DailyBonus.Api;
using Assets.Scripts.GameLogic.DailyBonus.Impl;
using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.BuildingSystem.MapObjects.ConstractAnimations;
using NanoReality.Engine.UI;
using NanoReality.Engine.UI.Extensions.UISignals;
using NanoReality.Engine.Utilities;
using NanoReality.GameLogic.Achievements;
using NanoReality.GameLogic.Achievements.api;
using NanoReality.GameLogic.Achievements.impl;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.City.Models.Impl;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Controllers;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Impl;
using NanoReality.GameLogic.City.Models.Impl;
using NanoReality.GameLogic.Common.Controllers.Commands;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.GameManager.Controllers;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Impl;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.ServerCommunication.Controllers;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.Settings.BuildSettings.Models.Api;
using NanoReality.GameLogic.Settings.GameSettings.Models.Api;
using NanoReality.GameLogic.SocialCommunicator.api;
using NanoReality.GameLogic.SoftCurrencyStore;
using NanoReality.GameLogic.StartApplicationSequence;
using NanoReality.GameLogic.StartApplicationSequence.Controllers.Commands;
using NanoReality.GameLogic.StartApplicationSequence.Controllers.Signals;
using strange.extensions.command.api;
using strange.extensions.command.impl;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using strange.extensions.injector.api;
using strange.extensions.signal.impl;
using UnityEngine;
using Debug = Assets.NanoLib.UtilitesEngine.CustomDebugging.Models.impl.Debug;
using Assets.Scripts.Engine.UI.Views.Popups;
using GameLogic.Bazaar.Controllers.Signals;
using NanoReality.GameLogic.Settings.GamePrefs;
using NanoReality.GameLogic.Localization.Models.api;
using NanoReality.GameLogic.Localization.Models.impl;
using NanoReality.GameLogic.BuildingSystem.Happiness;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.Happiness;
using NanoReality.GameLogic.FX;
using NanoReality.GameLogic.GameManager;
using NanoReality.GameLogic.Services;
using NanoReality.Engine.UI.Extensions;
using NanoReality.GameLogic.BuildingSystem.CityCamera;
using NanoReality.GameLogic.Configs;
using GameLogic.Bazaar.Service;
using GameLogic.BuildingSystem.CityAoe;
using GameLogic.PostLoadingSequence;
using GameLogic.Taxes.Service;
using NanoLib.AssetBundles;
using NanoLib.Core.Timers;
using NanoLib.SoundManager;
using NanoLib.UI.Core;
using NanoReality.Core.Resources;
using NanoReality.Core.Timer;
using NanoReality.Debugging;
using NanoReality.Engine.BuildingSystem.CityCamera;
using NanoReality.Engine.Settings.BuildSettings;
using NanoReality.Engine.UI.Extensions.EditModePanels;
using NanoReality.Game.Analytics;
using NanoReality.Game.CityStatusEffects;
using NanoReality.Game.Data;
using NanoReality.Game.Network.Commands;
using NanoReality.Game.OrderDesk;
using NanoReality.Game.Services.BuildingServices;
using NanoReality.GameLogic.PostLoadingSequence;
using NanoReality.GameLogic.SocialCommunicator.impl;
using NanoReality.Game.Notifications;
using NanoReality.Game.UI.Popups.DownloadPopup;
using NanoReality.GameLogic.Bank;
using NanoReality.GameLogic.Common;
using NanoReality.GameLogic.IAP;
using NanoReality.GameLogic.OrderDesk;
using NanoReality.GameLogic.Common.Controllers;
using NanoReality.GameLogic.SocialCommunicator;
using NanoReality.GameLogic.TheEvent;
using NanoReality.GameLogic.TheEvent.Signals;
#if UNITY_ANDROID
using NanoReality.Game.Notifications.Android.Services;
#elif UNITY_IOS
using NanoReality.Game.Notifications.iOS.Services;
#endif

namespace NanoReality
{
    public class SignalOnApplicationQuit : Signal { }

    public partial class NanoRealityGameContext : MVCSContext
    {
        #region Fields

        public IPopupManager PopupManager { get; set; }
        public IInputController InputController { get; set; }
        public IGlobalMapObjectViewSettings MapObjectViewSettings { get; set; }
        public static IInjectionBinder InjectionBinder { get; private set; }
        public IWorldSpaceCanvas WorldSpaceCanvasPrefab { get; set; }
        public IBuildingSitesContainer ConstractAnimationsContainer { get; set; }
        public IUIManager UIManagerObject { get; set; }
        public IconProvider IconProvider { get; set; }
        public ICityMapSettings CityMapSettings { get; set; }
        public IBuildSettings BuildSettings { get; set; }
        public NetworkSettings NetworkSettings { get; set; }
        public GameCameraSettings GameCameraSettings { get; set; }
        public SoundSettings SoundSettings { get; set; }
        public AppsFlyerObjectScript AppsFlyerScriptableObject { get; set; }
        
        /// <summary>
        /// TODO: need remove this shit.
        /// </summary>
        public SignalOnApplicationQuit SignalOnApplicationQuit => injectionBinder.GetInstance<SignalOnApplicationQuit>();

        #endregion

        public NanoRealityGameContext(MonoBehaviour view, ContextStartupFlags flags)
            : base(view, flags)
        {
            InjectionBinder = injectionBinder;
        }

        public override IContext Start()
        {
            base.Start();
            return this;
        }

        /// <summary>
        /// Application entry point
        /// </summary>
        public void StartApplication()
        {
            var startInitSignal = injectionBinder.GetInstance<StartInitApplicationSignal>();
            startInitSignal.Dispatch();
        }

        protected override void addCoreComponents()
        {
            base.addCoreComponents();

            //change event binder to signal binder
            injectionBinder.Unbind<ICommandBinder>();  
            injectionBinder.Bind<ICommandBinder>().To<SignalCommandBinder>().ToSingleton();
            injectionBinder.Bind<MVCSContext>().ToValue(this).ToName(ContextKeys.CONTEXT);
            injectionBinder.Bind<SignalOnApplicationQuit>().ToValue(new SignalOnApplicationQuit()).ToSingleton();
        }

        protected override void mapBindings()
        {
            base.mapBindings();
            SettingsBinds();
            BindResources();
            DebugBinds();

            //StartTutorialSequenceBinds();
            GameManagerBinds();
            UserCityBinds();
            UtilitiesBinds();
            GameSettingsBinds();
            MapObjectViewBinds();
            CommonBinds();
            PlayerProfileBinds();
            ServerBinds();
            BusinessSectorsBinds();
            AnalyticsBinds();
            PushNotificationsBinds();
            BuildingSystemBinds();
            InputControllersBinds();
			CommonLevelsBinds();
			UIsBinds();
            QuestsBinds();
            AwardBinds();
            DailyBonusBinds();
            AchievementsBinds();
            PopulationBinds();
            ProductsBinds();
            MonetizationBinds();

            SocialNetworksBinds();
            TutorialBinds();
            LocalizationBinds();
            HappinessBinds();
            VfxBinds();
            ConfigsBinds();
            BindingCamera();
            BindActionCommands();
            LongTapProgressBinds();
            StartApplicationSequenceBinds();
            AppsFlyerSettingsBind();
        }

        private void AppsFlyerSettingsBind()
        {
            injectionBinder.Bind<AppsFlyerObjectScript>().ToValue(AppsFlyerScriptableObject).ToInject(false);
        }

        private void BindResources()
        {
            injectionBinder.Bind<IResourcesManager>().To<ResourcesManager>().ToSingleton();

            switch (BuildSettings.ResourcesPolicy)
            {
                case ResourcesPolicyType.LocalResources:
                    injectionBinder.Bind<ResourcesPolicy>().To<LocalResourcesPolicy>().ToSingleton();
                    break;
                
                case ResourcesPolicyType.LocalAssetBundles:
                case ResourcesPolicyType.RemoteAssetBundles:
                    injectionBinder.Bind<ResourcesPolicy>().To<AssetBundleResourcesPolicy>().ToSingleton();
                    break;
                
                default:
                    throw new ArgumentOutOfRangeException();
            }

            commandBinder.Bind<BundlesLoadingFinishedSignal>();
            commandBinder.Bind<BundlesLoadingStartedSignal>();
            commandBinder.Bind<BundleLoadedSignal>();
            commandBinder.Bind<GameResourcesLoadedSignal>();
            commandBinder.Bind<OnGraphicQualityPopupUpdateSignal>();
            
            commandBinder.Bind<GraphicQualityChangedSignal>()
                .To<ChangeGraphicQualityCommand>()
                .To<ReloadApplicationCommand>()
                .InSequence();
        }
        
        private void LongTapProgressBinds()
        {
            commandBinder.Bind<SignalOnStartLongTapMapObjectView>();
            commandBinder.Bind<SignalOnEndLongTapMapObjectView>();
            mediationBinder.Bind<LongTapProgressView>().To<LongTapProgressMediator>();
        }

        private void HappinessBinds()
        {
            injectionBinder.Bind<ICityAoeService>().To<CityAoeService>().ToSingleton();
            injectionBinder.Bind<IHappinessData>().To<HappinessData>().ToSingleton();
            injectionBinder.Bind<IBuildingNeedHappiness>().To<BuildingNeedHappiness>();
        }

        private void LocalizationBinds()
	    {
            injectionBinder.Bind<ILanguageData>().To<LanguageData>();
            injectionBinder.Bind<LanguageObject>();
        }

        /// <summary>
        /// Create commands sequence wich start wheh StartInitApplicationSignal fired
        /// </summary>
        private void StartApplicationSequenceBinds()
        {
            commandBinder.Bind<StartInitApplicationSignal>()
                .InSequence()
                .To<CommandSetGlobalSettings>()
                .To<ApplicationInitCommand>()
                .To<NetworkErrorHandlerCommand>()
                .To<InitializeNetworkCommand>()
                .To<AuthorizeCommand>()
                .To<LoadTheEventDataCommand>()
                .To<LoadAssetBundlesCommand>()
                .To<CommandLoadTutorialStatus>()
                .To<CommandGetCheatPresets>()
                .To<CommandLoadPlayerProfile>()
                .To<LoadGameBalanceCommand>()
                .To<InitGameServicesCommand>()
                .To<DownloadOffersImagesCommand>()
                .To<FinishInitAppSequenceCommand>()
                .Once();
            commandBinder.Bind<SignalPostLoadingAction>()
                .InSequence()
                .To<PostLoadingActionsBegin>()
                .To<PostLoadingActionQuests>()
                .To<PostLoadingActionAchievement>()
                .To<PostLoadingActionShowHUD>()
                .To<PostLoadingActionDaily>()
                .To<PostLoadingActionsComplete>()
                .To<PostLoadingActionBeginTutorial>()
                .Once();

            //***************** Helper commands ***********************
            commandBinder.Bind<HidePreloaderAfterGameLaunchSignal>();
            commandBinder.Bind<SignalGameInitialized>();
            commandBinder.Bind<SignalUserCreated>();
            commandBinder.Bind<PostLoadingActionsCompleteSignal>();
            commandBinder.Bind<UpdateTickSignal>();

            commandBinder.Bind<FinishApplicationInitSignal>();
            commandBinder.Bind<SignalReloadApplication>().To<ReloadApplicationCommand>();
            commandBinder.Bind<SignalDisposeGameServices>().To<DisposeGameServicesCommand>();

            commandBinder.Bind<SignalOnRequestError>();
            commandBinder.Bind<AuthorizationSuccessSignal>();
            commandBinder.Bind<AuthorizationStartedSignal>();
            commandBinder.Bind<SignalOnPlayerProfileLoaded>();
            //************************************************************
        }

        private void CommonBinds()
        {
            injectionBinder.Bind<ITimer>().To<TimeTimer>();
            injectionBinder.Bind<IIapService>().To<IapService>().ToSingleton();

            commandBinder.Bind<SignalOnProductTexturesLoadEnd>();
            commandBinder.Bind<SignalOnBuildingRepaired>();

            commandBinder.Bind<SignalOffersUpdated>().To<DownloadOffersImagesCommand>();
            commandBinder.Bind<DisableOffersButtonSignal>();
            commandBinder.Bind<SignalLoadingSpinner>();
            commandBinder.Bind<PurchaseOfferFinishedSignal>();
            injectionBinder.Bind<OffersValidationController>().ToSingleton();

            commandBinder.Bind<SignalTheEventDataIsLoaded>();
            commandBinder.Bind<SignalOpenUpgradeWindowEventBuilding>();
            commandBinder.Bind<SignalCheckUnclaimedStageRewards>();
            commandBinder.Bind<SignalEnableEventButton>();
            commandBinder.Bind<SignalOpenTheEventPanel>();
            commandBinder.Bind<SignalClickOnTheEventBuildingWhenCannotUpgrade>();
            commandBinder.Bind<SignalSetEventTimerOnHud>();
            commandBinder.Bind<SignalDisableAttentionViewOnEventBuilding>();
            commandBinder.Bind<SignalTheEventIsEnds>();
            commandBinder.Bind<SignalPreEventEndHintTrigger>();

            injectionBinder.Bind<IIconProvider>().ToValue(IconProvider).ToInject(false);

            // Game Services
            injectionBinder.Bind<IAdsService>().To<AdsService>().ToSingleton();
            injectionBinder.Bind<IProductService>().To<ProductService>().ToSingleton();
            injectionBinder.Bind<ISocialNetworkService>().To<SocialNetworkService>().ToSingleton();
            injectionBinder.Bind<IBuildingService>().To<BuildingService>().ToSingleton();
            
            injectionBinder.Bind<ICitySectorService>().To<CitySectorService>().ToSingleton();
            injectionBinder.Bind<ICityStatusEffectService>().To<CityStatusEffectService>().ToSingleton();
            injectionBinder.Bind<IMapObjectPlacementService>().To<MapObjectPlacementService>().ToSingleton();

            injectionBinder.Bind<IBazaarService>().To<BazaarService>().ToSingleton();
            injectionBinder.Bind<IEditModeService>().To<EditModeService>().ToSingleton();
            injectionBinder.Bind<ITheEventService>().To<TheEventService>().ToSingleton();

            
            commandBinder.Bind<BazaarBuyProductSignal>();
            commandBinder.Bind<BazaarBuyLootBoxSignal>();

            injectionBinder.Bind<ICityTaxesService>().To<CityTaxesService>().ToSingleton();



#if (UNITY_ANDROID)
            injectionBinder.Bind<INotificationRegistrationService>().To<NotificationRegistrationService>().ToSingleton();
            injectionBinder.Bind<ILocalNotificationService>().To<AndroidLocalNotificationService>().ToSingleton();
#elif (UNITY_IOS)
            injectionBinder.Bind<INotificationRegistrationService>().To<NotificationRegistrationService>().ToSingleton();
            injectionBinder.Bind<ILocalNotificationService>().To<IOSLocalNotificationService>().ToSingleton();
#endif
        }

        private void GameManagerBinds()
        {
            injectionBinder.Bind<IGameManager>().To<GameManager>().ToSingleton();
            commandBinder.Bind<SignalOnFinishLoadCityMapSequence>();
            //commandBinder.Bind<SignalOnApplicationQuit>();
            commandBinder.Bind<SignalOnApplicationPause>();
            commandBinder.Bind<SignalOnFinishRestoreCity>();

            commandBinder.Bind<GameDataLoadedSignal>();
            commandBinder.Bind<SignalGameDataIsUpdated>();
            // сиквенция комманд на загрузку карты
            commandBinder.Bind<SignalLoadCityMapSequence>()
                .InSequence()
                .To<LoadGlobalCityMapCommand>()
                .To<LoadCityMapCommand>()
                .To<InitUserCityCommand>()
                .To<InitGlobalCityMapCommand>()
                .To<InitCityMapCommand>();
        }

        private void SettingsBinds()
        {
            injectionBinder.Bind<IBuildSettings>().ToValue(BuildSettings).ToInject(false);
            injectionBinder.Bind<NetworkSettings>().ToValue(NetworkSettings).ToInject(false);
            injectionBinder.Bind<LinksConfigsHolder>().ToSingleton(); 
            injectionBinder.Bind<ISoundManager>().To<SoundManager>().ToSingleton();
            injectionBinder.Bind<PlaylistPlayer>();
            injectionBinder.Bind<SoundSettings>().ToValue(SoundSettings).ToSingleton();
            injectionBinder.Bind<ICityMapSettings>().ToValue(CityMapSettings).ToInject(false);
            injectionBinder.Bind<IGamePrefs>().To<GamePrefs>().ToSingleton();
        }

        private void DebugBinds()
        {
            injectionBinder.Bind<DebugConsoleCommands>().ToSingleton();
        }

        private void MapObjectViewBinds()
        {
            injectionBinder.Bind<IGlobalMapObjectViewSettings>().ToValue(MapObjectViewSettings).ToInject(false);
            commandBinder.Bind<SignalOnMapObjectViewMoved>();
        }

        private void InputControllersBinds()
        {

            commandBinder.Bind<SignalDragObject>();
            commandBinder.Bind<MapObjectMovedSignal>();

            commandBinder.Bind<SignalOnBeginTouch>();
			commandBinder.Bind<SignalOnSwipe>();
			commandBinder.Bind<SignalOnTouchNotSwipeEnd>();
			commandBinder.Bind<SignalOnSwipeEnd>();
            commandBinder.Bind<SignalOnPinch>();
            commandBinder.Bind<SignalOnPinchEnd>();
            commandBinder.Bind<BuildingDragBegin>();
            commandBinder.Bind<SignalOnLongTapMapObjectView>();
			commandBinder.Bind<SignalOnMapObjectViewTap>();
			commandBinder.Bind<SignalOnLockedSubSectorTap>();
            commandBinder.Bind<SignalOnNonPlayableBuildTap>();
            commandBinder.Bind<SignalOnInputEnable>();

            commandBinder.Bind<SignalOnGroundTap>();
            commandBinder.Bind<SignalOnTerrainTap>();
            commandBinder.Bind<SignalOnUiTap>();
            commandBinder.Bind<SignalSimulateLongTap>();

            commandBinder.Bind<SignalOnReleaseTouch>();
            commandBinder.Bind<SignalOnManualZoom>();
            commandBinder.Bind<SignalOnManualSwipe>();
            commandBinder.Bind<SignalOnMapMovedUnderTouch>();
        }

        private void BindingCamera()
        {
            injectionBinder.Bind<GameCameraSettings>().ToValue(GameCameraSettings).ToSingleton().ToInject(false);
            injectionBinder.Bind<IGameCameraModel>().To<GameCameraModel>().ToSingleton();
            
            commandBinder.Bind<SignalEnableCamera>();
            commandBinder.Bind<SignalDisableCamera>();
            
            commandBinder.Bind<SignalCameraSwipe>();
            commandBinder.Bind<ZoomCameraSignal>(); //bind with zoom CameraSignals
            commandBinder.Bind<SignalOnCameraSwiped>(); //bind with swipe CameraSignals
            commandBinder.Bind<InertiaCameraSignal>(); //bind with inertia CameraSignals
            commandBinder.Bind<SignalOnAutoSwipeCamera>(); //bind with auto swipe CameraSignals
            commandBinder.Bind<StartCameraAutoZoomSignal>(); //bind with auto zoom CameraSignals
        }

        private void UserCityBinds()
        {
            injectionBinder.Bind<IUserCity>().To<MUserCity>().ToSingleton();
        }

        private void PlayerProfileBinds()
        {
            commandBinder.Bind<UserLevelUpSignal>();
            commandBinder.Bind<SignalOnUserInvited>();
            commandBinder.Bind<SignalOnNanoGemsDropped>();

            injectionBinder.Bind<IPlayerProfile>().To<PlayerProfile>().ToSingleton();
            injectionBinder.Bind<IPlayerResources>().To<PlayerResources>().ToSingleton();
            injectionBinder.Bind<IUserLevelsData>().To<MUserLevelsBalanceData>().ToSingleton();
        }

        private void BusinessSectorsBinds()
        {
            injectionBinder.Bind<IBusinessSector>().To<BusinessSector>();
            commandBinder.Bind<SignalOnUserBusinessSectorsUpdate>();
            commandBinder.Bind<SignalOnSectorUpdate>();
        }

        private void PopulationBinds()
        {
            commandBinder.Bind<UpdatePopulationSatisfactionSignal>();
            commandBinder.Bind<SignalOnPopulationSatisfactionStateChanged>();
            commandBinder.Bind<SignalOnCityPopulationUpdated>();

            injectionBinder.Bind<ICityPopulation>().To<MCityPopulation>().ToSingleton();
        }

        private void AnalyticsBinds()
        {
            commandBinder.Bind<SignalOnActionWithCurrency>();
            commandBinder.Bind<SignalOnAchievementCompleted>();
            commandBinder.Bind<SignalOnSkipProduction>();
            commandBinder.Bind<SignalOnSkipBuilding>();
            commandBinder.Bind<SignalOnFirstPay>();
            commandBinder.Bind<SignalOnInviteSentAction>();
            commandBinder.Bind<SignalOnBuyMissingProductsAction>();
            commandBinder.Bind<SignalOnUIAction>();
            commandBinder.Bind<SignalOnTaxesCollection>();
            commandBinder.Bind<SignalOnRequestSendAction>();
            commandBinder.Bind<SignalOnRequestReceivedAction>();
            commandBinder.Bind<SignalOnUnlockSectorAction>();
            commandBinder.Bind<SignalOnSocialConnectAction>();
            commandBinder.Bind<SignalOnRepairBuildingAction>();
            commandBinder.Bind<SignalOnDailyBonusCollected>();
            commandBinder.Bind<SignalOnCityNameChanged>();
            commandBinder.Bind<SignalOnSettingsLanguageChanged>();

            injectionBinder.Bind<INanoAnalytics>().To<NanoAnalytics>().ToSingleton();
            injectionBinder.Bind<IAnalyticsManager>().To<AnalyticsManager>().ToSingleton();

            injectionBinder.Bind<IAppsFlyerAnalyticsService>().To<AppsFlyerAnalyticsService>();
        }

        private void PushNotificationsBinds()
        {
            commandBinder.Bind<CancelAllPushNotificationSignal>();
            commandBinder.Bind<NotificationInitSignal>();
        }
        
        private void ServerBinds()
        {          
            injectionBinder.Bind<IServerCommunicator>().To<ServerCommunicator>().ToSingleton();
            injectionBinder.Bind<IServerRequest>().To<MServerRequest>().ToSingleton();
            injectionBinder.Bind<IStringSerializer>().To<JsonStringSerializer>().ToSingleton();
            commandBinder.Bind<ServerTimeTickSignal>();
        }

        private void CommonLevelsBinds()
        {
            injectionBinder.Bind<IObjectPullWithLevel>().To<MObjectsPull>();
            injectionBinder.Bind<IPlayerExperience>().To<PlayerExperience>().ToSingleton();
        }

        private void UtilitiesBinds()
        {
            injectionBinder.Bind<ILocalDataManager>().To<LocalDataManager>().ToSingleton();
            injectionBinder.Bind<IDebug>().To<Debug>().ToSingleton();
            injectionBinder.Bind<ITimerManager>().To<TimerManager>().ToSingleton();

            commandBinder.Bind<SignalOnAddRequestToQueue> ();
			commandBinder.Bind<SignalOnSendRequestToServer> ();
			commandBinder.Bind<SignalOnReciveAnswerFromServer> ();
        }

        private void GameSettingsBinds()
        {
            injectionBinder.Bind<IGameSettings>().To<GameSettings>().ToSingleton();
            commandBinder.Bind<SignalOnNeedToRenameCity>();
            commandBinder.Bind<OnCityNameChangedOnServerSignal>();
        }

        private void QuestsBinds()
        {
            // Quests
            
            injectionBinder.Bind<IQuestBalanceData>().To<QuestBalanceData>().ToSingleton();
            injectionBinder.Bind<IQuest>().To<Quest>();
            injectionBinder.Bind<IUserQuestData>().To<UserQuestData>().ToSingleton();
            injectionBinder.Bind<ICondition>().To<Condition>();

            commandBinder.Bind<SignalOnQuestsLoaded>();
            commandBinder.Bind<SignalOnQuestAchive>();
            commandBinder.Bind<SignalOnQuestComplete>();
            commandBinder.Bind<SignalOnQuestStart>();
            commandBinder.Bind<SignalOnQuestUnlock>();
            commandBinder.Bind<SignalOnQuestCompletedOnServer>();
            commandBinder.Bind<SignalOnQuestFailed>();
        }

        private void DailyBonusBinds()
        {
            injectionBinder.Bind<IDailyBonusBalanceData>().To<DailyBonusBalanceData>().ToSingleton();
        }

        private void AwardBinds()
        {
            injectionBinder.Bind<IAwardAction>().To<AwardAction>();
            injectionBinder.Bind<IAwardActionDescription>().To<AwardActionDescription>();
            
            injectionBinder.Bind<IExperienceAward>().To<ExperienceAward>();
            injectionBinder.Bind<ILevelUpAward>().To<LevelUpAward>();
            injectionBinder.Bind<IPremiumCurrencyAwardAction>().To<PremiumCurrencyAwardActionAction>();
            injectionBinder.Bind<ISoftCurrencyAward>().To<SoftCurrencyAward>();
            injectionBinder.Bind<IGiveProductAward>().To<GiveProductAward>();
            injectionBinder.Bind<IGiveRandomProductAward>().To<GiveRandomProductAward>();
            injectionBinder.Bind<INanoGemsAward>().To<NanoGemsAward>();
            injectionBinder.Bind<IGoldBarAwardAction>().To<GoldBarAwardAction>();
        }

        private void AchievementsBinds()
        {
            injectionBinder.Bind<IAchievementsData>().To<MAchievementsData>().ToSingleton();
            injectionBinder.Bind<IUserAchievement>().To<UserAchievement>();
            injectionBinder.Bind<IUserAchievementsData>().To<MUserAchievementsData>().ToSingleton();


            commandBinder.Bind<SignalOnUserAchievementsDataLoaded>();
            commandBinder.Bind<SignalOnUserAchievmentsInit>();
            commandBinder.Bind<SignalOnUserAchievementUnlocked>();
            commandBinder.Bind<AnalyticsAchievementUnlock>();
            commandBinder.Bind<SignalOnAwardTaken>();
			commandBinder.Bind<AchievementConditionUpdateSignal> ();
            commandBinder.Bind<AchievementCompletedSignal>();
        }

        private void MonetizationBinds()
        {
            injectionBinder.Bind<ISoftCurrencyBundlesModel>().To<SoftCurrencyBundlesModel>().ToSingleton();

            commandBinder.Bind<SoftCurrencyBoughtSignal>();
            commandBinder.Bind<SignalOnSoftCurrencyStateChanged>();
            commandBinder.Bind<SignalOnHardCurrencyStateChanged>();
            commandBinder.Bind<SignalOnLanternCurrencyStateChanged>();
            commandBinder.Bind<SignalOnHardCurrencyConsumed>();
            commandBinder.Bind<SignalOnNanoGemsStateChanged>();
            commandBinder.Bind<PurchaseFinishedSignal>();
            commandBinder.Bind<AdsWatchFinishedSignal>();
        }

        private void SocialNetworksBinds()
        {
            injectionBinder.Bind<IFacebookController>().To<FacebookController>().ToSingleton();
            commandBinder.Bind<FBLoggedInSignal>();

            commandBinder.Bind<SignalOnAuthorizeSocialProfile>().To<CommandOnAutorizeInSocialNetwork>();
            commandBinder.Bind<OnFriendInvitedSignal>();
            commandBinder.Bind<OnFriendsUpdatedSignal>();
            commandBinder.Bind<SocialNetworkLogout>();
            

#if UNITY_ANDROID || UNITY_EDITOR || UNITY_STANDALONE
            commandBinder.Bind<SignalOnAuthorizeSocialProfile>();
            injectionBinder.Bind<ISocialNetworkCommunicator>()
                .To<MockSocialNetworkCommunicator>()
                .ToSingleton()
                .ToName(SocialNetworkCommunicatorTypes.Primary);
#elif (UNITY_IOS || UNITY_IPHONE)
            injectionBinder.Bind<ISocialNetworkCommunicator>()
                .To<MockSocialNetworkCommunicator>()
                .ToSingleton()
                .ToName(SocialNetworkCommunicatorTypes.Primary);
#endif
        }

        private void VfxBinds()
        {
            commandBinder.Bind<SignalShowMapClouds>();
            commandBinder.Bind<SignalRemoveMapClouds>();
        }

        private void ConfigsBinds()
        {
            injectionBinder.Bind<IGameConfigData>().To<GameConfigData>().ToSingleton();
            injectionBinder.Bind<IBuildingSlotsData>().To<BuildingSlotsData>().ToSingleton();
        }

        private void BindActionCommands()
        {
            commandBinder.Bind<BuyProductsActionSignal>().To<BuyProductsActionCommand>();
            commandBinder.Bind<BuyResoursesActionSignal>().To<BuyResoursesActionCommand>();
        }
    }
}
