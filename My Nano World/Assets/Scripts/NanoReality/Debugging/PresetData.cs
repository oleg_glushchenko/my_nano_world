namespace NanoReality.Debugging
{
    public struct PresetData
    {
        public int id;
        public string name;
    }
}