﻿using System;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.ServerCommunication.Models;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;


public class SignalOnAddRequestToQueue : Signal<RequestData> {}
public class SignalOnSendRequestToServer : Signal<RequestData> {}
public class SignalOnReciveAnswerFromServer : Signal<RequestData>{}

public class DebugServerRequestQueue : View
{
	[ListDrawerSettings(HideAddButton = true, Expanded = false, ListElementLabelName = "Name")]
	public List<QueueItem> CurrentQueue = new List<QueueItem>();

	[ListDrawerSettings(HideAddButton = true, Expanded = false, ListElementLabelName = "Name")]
	public List<QueueItem> ClientQueueLog = new List<QueueItem>();

	[ListDrawerSettings(HideAddButton = true, Expanded = false, ListElementLabelName = "Name")]
	public List<QueueItem> ServerQueueLog = new List<QueueItem>();

	[ListDrawerSettings(HideAddButton = true, Expanded = true, ListElementLabelName = "Name")]
	public List<QueueItem> ServerAnswersLog = new List<QueueItem>();

	[PostConstruct]
	public void PostConstruct()
	{
		jSignalOnAddRequestToQueue.AddListener (UpdateClientQueueLogList);
		jSignalOnSendRequestToServer.AddListener (UpdateServerQueueLogList);
		jSignalOnReciveAnswerFromServer.AddListener (UpdateServerAnswerQueueLogList);
	}
	
	[Inject]
	public SignalOnAddRequestToQueue jSignalOnAddRequestToQueue{get; set;}

	[Inject]
	public SignalOnSendRequestToServer jSignalOnSendRequestToServer { get; set;}

	[Inject]
	public SignalOnReciveAnswerFromServer jSignalOnReciveAnswerFromServer {get; set;}

	private void UpdateClientQueueLogList(RequestData requestData)
	{
		ClientQueueLog.Add (new QueueItem (requestData));
	}

	private void UpdateServerQueueLogList(RequestData requestData)
	{
		ServerQueueLog.Add (new QueueItem (requestData));
	}

	private void UpdateServerAnswerQueueLogList(RequestData requestData)
	{
		ServerAnswersLog.Add (new QueueItem (requestData));
	}

	[Serializable]
	public class QueueItem
	{
		public string Url;
		public RequestMethod RequestMethod;
		public ServerResponce ServerResponce;
		public List<StringPair> Parameters;
		public List<StringPair> LazeParameters;
		public List<StringPair> Headers;
		public string Name => $"{Url} {RequestMethod}";

		public QueueItem(RequestData requestData)
		{
			RequestMethod = requestData.RequestMethod;
			Url = requestData.GetURL();
			ServerResponce = new ServerResponce(requestData.StatusCode, requestData.ServerAnswer);

			if (requestData.Parameters != null)
			{
				Parameters = new List<StringPair>(requestData.Parameters.Count);
				foreach (KeyValuePair<string, string> dataParameter in requestData.Parameters)
				{
					Parameters.Add(new StringPair
					{
						Key = dataParameter.Key,
						Value = dataParameter.Value
					});
				}
			}

			if (requestData.LazyParametrs != null)
			{
				LazeParameters = new List<StringPair>(requestData.LazyParametrs.Count);
				foreach (KeyValuePair<string, LazyParameter<string>> dataParameter in requestData.LazyParametrs)
				{
					LazeParameters.Add(new StringPair
					{
						Key = dataParameter.Key,
						Value = dataParameter.Value.ToString()
					});
				}
			}

			if (requestData.Headers != null)
			{
				Headers = new List<StringPair>(requestData.Headers.Count);
				foreach (var pair in requestData.Headers)
				{
					Headers.Add(new StringPair
					{
						Key = pair.Key,
						Value = pair.Value
					});
				}
			}
		}
	}

	[Serializable]
	public class StringPair
	{
		public string Key;
		public string Value;
	}

	[Serializable]
	public class ServerResponce
	{
		public int StatusCode;
		public string Message;

		public ServerResponce()
		{
		}

		public ServerResponce(int status, string message)
		{
			StatusCode = status;
			Message = message;
		}
	}


}

