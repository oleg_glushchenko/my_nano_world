﻿using MobileConsole;
using MobileConsole.UI;

namespace NanoReality.Debugging.ConsoleCommands
{
    [ExecutableCommand(name = "Cheats/Skip hint tutorials")]
    public class SkipHintTutorialsCommand : Command
    {
        private SkipHintTutorialsViewBuilder _viewBuilder;

        public override void Execute()
        {
            _viewBuilder ??= new SkipHintTutorialsViewBuilder();

            info.shouldCloseAfterExecuted = true;
            LogConsole.PushSubView(_viewBuilder);
        }
    }

    public class SkipHintTutorialsViewBuilder : ViewBuilder
    {
        public override void OnPrepareToShow()
        {
            base.OnPrepareToShow();
            MobileConsoleContext.Instance.jDebugConsoleCommands.SkipHintTutorials();
            LogConsole.PopSubView();
            LogConsole.Hide();
        }
    }
}