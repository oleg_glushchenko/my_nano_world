using MobileConsole;
using MobileConsole.UI;

namespace NanoReality.Debugging.ConsoleCommands
{
    // [ExecutableCommand(name = "Network/PingSocketConsoleCommand")]
    // public class PingSocketConsoleCommand : Command
    // {
    //     private PingSocketCommandViewBuilder _viewBuilder;
    //
    //     public override void Execute()
    //     {
    //         if (_viewBuilder == null)
    //             _viewBuilder = new PingSocketCommandViewBuilder();
    //         info.shouldCloseAfterExecuted = false;
    //         LogConsole.PushSubView(_viewBuilder);
    //     }
    // }
    //
    // public class PingSocketCommandViewBuilder : ViewBuilder
    // {
    //     public override void OnPrepareToShow()
    //     {
    //         base.OnPrepareToShow();
    //         AddButton("Ping", "action", (c) =>
    //         {
    //             MobileConsoleContext.Instance.jDebugConsoleCommands.jWebSocketConnector.Ping();
    //         });
    //     }
    // }
    //
    // public class ConnectSocketCommandView : ViewBuilder
    // {
    //     public override void OnPrepareToShow()
    //     {
    //         base.OnPrepareToShow();
    //         AddButton("Connect", "action", (c) =>
    //         {
    //             MobileConsoleContext.Instance.jDebugConsoleCommands.jWebSocketConnector.Connect();
    //         });
    //     }
    // }
}