﻿using Assets.NanoLib.ServerCommunication.Models;
using Assets.NanoLib.Utilities.DataManager.Models.api;
using MobileConsole;
using MobileConsole.UI;

namespace NanoReality.Debugging.ConsoleCommands
{
    [ExecutableCommand(name = "Cheats/Set Free Skip Prices", order = 1)]
    public class SetFreeSkipPricesCommand : Command
    {
        private SetFreeSkipPricesCommandViewBuilder _viewBuilder;

        public override void Execute()
        {
            if (_viewBuilder == null)
                _viewBuilder = new SetFreeSkipPricesCommandViewBuilder();
            info.shouldCloseAfterExecuted = false;
            LogConsole.PushSubView(_viewBuilder);
        }
    }

    public class SetFreeSkipPricesCommandViewBuilder : ViewBuilder
    {
        private string _intervalsDataUrl = "/v1/game/skip";
        private ILocalDataManager LocalDataManager => MobileConsoleContext.Instance.jLocalDataManager;
        
        public SetFreeSkipPricesCommandViewBuilder()
        {
            AddResizableText("Don't forgot to reload game for apply changes.");
            AddButton("Set Skip Prices as Free", "action", AddTaxes);
            closeAllSubViewOnAction = true;
        }

        private void AddTaxes(GenericNodeView node)
        {
            MobileConsoleContext.Instance.jServerCommunicator.CheatSetFreeSkipPrices();
            var requestDataKeeper = LocalDataManager.GetData<LocalReuqestDataKeeper>();
            
            var skipIntervalsData = requestDataKeeper.Items.Find(c => c.Url == _intervalsDataUrl);
            requestDataKeeper.Items.Remove(skipIntervalsData);
            LocalDataManager.SaveData();
            
            MobileConsoleContext.Instance.jDebugConsoleCommands.jSignalReloadApplication.Dispatch();
            LogConsole.PopSubView();
        }
    }
}