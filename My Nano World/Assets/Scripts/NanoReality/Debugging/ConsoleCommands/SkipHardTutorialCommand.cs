﻿using MobileConsole;
using MobileConsole.UI;

namespace NanoReality.Debugging.ConsoleCommands
{
    [ExecutableCommand(name = "Cheats/Skip hard tutorial")]
    public class SkipHardTutorialCommand : Command
    {
        private SkipHardTutorialViewBuilder _viewBuilder;

        public override void Execute()
        {
            if(_viewBuilder == null)
                _viewBuilder = new SkipHardTutorialViewBuilder();
            info.shouldCloseAfterExecuted = true;
            LogConsole.PushSubView(_viewBuilder);
        }
    }
    
    public class SkipHardTutorialViewBuilder : ViewBuilder
    {
        public override void OnPrepareToShow()
        {
            base.OnPrepareToShow();
            MobileConsoleContext.Instance.jDebugConsoleCommands.SkipHardTutorial();
            LogConsole.PopSubView();
			LogConsole.Hide();
        }
    }
}