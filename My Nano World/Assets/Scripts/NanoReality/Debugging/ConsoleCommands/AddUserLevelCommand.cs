using MobileConsole;
using MobileConsole.UI;
using UnityEngine;

namespace NanoReality.Debugging.ConsoleCommands
{
    [ExecutableCommand(name = "Cheats/Add user level", order = 0)]
    public class AddUserLevelCommand : Command
    {
        private AddUserLevelViewBuilder _viewBuilder;

        public override void Execute()
        {
            if(_viewBuilder == null)
                _viewBuilder = new AddUserLevelViewBuilder();
            info.shouldCloseAfterExecuted = false;
            LogConsole.PushSubView(_viewBuilder);
        }
    }

    public class AddUserLevelViewBuilder : ViewBuilder
    {
        private string _lastUserInput = "1";
        
        public AddUserLevelViewBuilder()
        {
            DrawSearchControl();
        }
		
        void DrawSearchControl()
        {
            InputNodeView input = AddInput("Level", _lastUserInput, OnInputChanged);
            input.isNumeric = true;
            AddButton("Add level", "action", AddLevel);
        }

        private void OnInputChanged(InputNodeView node)
        {
            _lastUserInput = node.value;
        }

        private void AddLevel(GenericNodeView node)
        {
            if (int.TryParse(_lastUserInput, out var levelValue))
            {
                MobileConsoleContext.Instance.jDebugConsoleCommands.AddLevel(levelValue);
            }

            LogConsole.PopSubView();
            LogConsole.PopSubView();
            LogConsole.Hide();
        }
    }
}