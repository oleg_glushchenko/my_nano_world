using MobileConsole;
using MobileConsole.UI;
using UnityEngine;

namespace NanoReality.Debugging.ConsoleCommands
{
    [ExecutableCommand(name = "User/UnlockAllSectors")]
    public class UnlockAllSectors : Command
    {
        private UnlockAllSectorsViewBuilder _viewBuilder;

        public override void Execute()
        {
            if (_viewBuilder == null)
                _viewBuilder = new UnlockAllSectorsViewBuilder();
            info.shouldCloseAfterExecuted = false;
            LogConsole.PushSubView(_viewBuilder);
        }
    }

    public class UnlockAllSectorsViewBuilder : ViewBuilder
    {
        public override void OnPrepareToShow()
        {
            base.OnPrepareToShow();
            MobileConsoleContext.Instance.jDebugConsoleCommands.UnlockAll();
            LogConsole.PopSubView();
            LogConsole.Hide();
        }
    }
}