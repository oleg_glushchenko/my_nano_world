using MobileConsole;
using MobileConsole.UI;
using UnityEngine;

namespace NanoReality.Debugging.ConsoleCommands
{
    [ExecutableCommand(name = "Cheats/Add product")]
    public class AddProductCommand : Command
    {
        
        private AddProductViewBuilder _viewBuilder;

        public override void Execute()
        {
            if(_viewBuilder == null)
                _viewBuilder = new AddProductViewBuilder();
            info.shouldCloseAfterExecuted = false;
            LogConsole.PushSubView(_viewBuilder);
        }
    }

    public class AddProductViewBuilder : ViewBuilder
    {
        private string _productInput = "1";
        private string _amountInput = "1";
        
        public AddProductViewBuilder()
        {
            DrawSearchControl();
        }
		
        void DrawSearchControl()
        {
            InputNodeView product = AddInput("Product id", _productInput, OnInputProductChanged);
            product.isNumeric = true;
            InputNodeView amount = AddInput("Amount", _amountInput, OnInputAmountChanged);
            amount.isNumeric = true;
            AddButton("Add product", "action", AddProduct);
        }

        private void OnInputProductChanged(InputNodeView node)
        {
            _productInput = node.value;
        }
        
        private void OnInputAmountChanged(InputNodeView node)
        {
            _amountInput = node.value;
        }

        private void AddProduct(GenericNodeView node)
        {
            if (int.TryParse(_productInput, out var product) && int.TryParse(_amountInput, out var amount))
            {
                MobileConsoleContext.Instance.jDebugConsoleCommands.AddProduct(product, amount);
            }

            LogConsole.PopSubView();
            LogConsole.PopSubView();
            LogConsole.Hide();
        }
    }
}