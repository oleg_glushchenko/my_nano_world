﻿using MobileConsole;
using MobileConsole.UI;

namespace NanoReality.Debugging.ConsoleCommands
{
    [ExecutableCommand(name = "User/Clear Local Data")]
    public class ClearLocalDataCommand : Command
    {
        private ClearLocalDataViewBuilder _viewBuilder;

        public override void Execute()
        {
            if (_viewBuilder == null)
                _viewBuilder = new ClearLocalDataViewBuilder();
            info.shouldCloseAfterExecuted = true;
            LogConsole.PushSubView(_viewBuilder);
        }
    }

    public class ClearLocalDataViewBuilder : ViewBuilder
    {
        public override void OnPrepareToShow()
        {
            base.OnPrepareToShow();
            MobileConsoleContext.Instance.jDebugConsoleCommands.ClearLocalData();
            LogConsole.PopSubView();
			LogConsole.Hide();
        }
    }
}