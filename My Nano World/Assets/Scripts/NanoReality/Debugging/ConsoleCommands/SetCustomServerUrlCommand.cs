using MobileConsole;
using MobileConsole.UI;
using NanoLib.Core.Logging;
using UnityEngine;

namespace NanoReality.Debugging.ConsoleCommands
{
    [ExecutableCommand(name = "Network/Set Custom Server Url")]
    public class SetCustomServerUrlCommand : Command
    {
        private SetCustomServerUrlViewBuilder _viewBuilder;

        public override void Execute()
        {
            if (_viewBuilder == null)
                _viewBuilder = new SetCustomServerUrlViewBuilder();
            info.shouldCloseAfterExecuted = false;
            LogConsole.PushSubView(_viewBuilder);
        }
    }
    
    public class SetCustomServerUrlViewBuilder : ViewBuilder
    {
        private string _serverUrl = "Empty";
        private string _protocol = "https";
        private readonly string[] _availableProtocols = {"http", "https"};
        
        public SetCustomServerUrlViewBuilder()
        {
            AddInput("ServerUrl", _serverUrl, OnServerUrlChanged, GetRootNode());
            AddDropdown("Protocol", 1, _availableProtocols, OnProtocolChanged, GetRootNode());
            AddButton("Apply Custom Server", "action", AddLevel, GetRootNode());
            closeAllSubViewOnAction = true;
        }

        private void OnProtocolChanged(DropdownNodeView node, int index)
        {
            _protocol = _availableProtocols[index];
        }

        private void OnServerUrlChanged(InputNodeView node)
        {
            _serverUrl = node.value;
        }

        private void AddLevel(GenericNodeView node)
        {
            string resultUrl = $"{_protocol}://{_serverUrl}";
            Logging.Log($"Set Custom Server {resultUrl}");
            PlayerPrefs.SetString("custom_server", resultUrl);
            PlayerPrefs.Save();

            LogConsole.PopSubView();
            LogConsole.PopSubView();
            LogConsole.Hide();
            
            MobileConsoleContext.Instance.jDebugConsoleCommands.RestartGame();
        }
    }
}