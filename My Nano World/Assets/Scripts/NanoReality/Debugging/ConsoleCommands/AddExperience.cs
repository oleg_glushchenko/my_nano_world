﻿using MobileConsole;
using MobileConsole.UI;

namespace NanoReality.Debugging.ConsoleCommands
{
    [ExecutableCommand(name = "Cheats/Add Experience")]
    public class AddExperienceCommand : Command
    {
        private AddExperienceViewBuilder _viewBuilder;

        public override void Execute()
        {
            if (_viewBuilder == null)
                _viewBuilder = new AddExperienceViewBuilder();
            info.shouldCloseAfterExecuted = false;
            LogConsole.PushSubView(_viewBuilder);
        }
    }

    public class AddExperienceViewBuilder : ViewBuilder
    {
        private string _lastUserInput = "1";
        public AddExperienceViewBuilder()
        {
            DrawSearchControl();
            closeAllSubViewOnAction = true;
        }
		
        void DrawSearchControl()
        {
            InputNodeView input = AddInput("Experience amount", _lastUserInput, OnInputChanged);
            input.isNumeric = true;
            AddButton("Add experience", "action", AddExperience);
        }

        private void OnInputChanged(InputNodeView node)
        {
            _lastUserInput = node.value;
        }

        private void AddExperience(GenericNodeView node)
        {
            if (int.TryParse(_lastUserInput, out var levelValue))
            {
                MobileConsoleContext.Instance.jDebugConsoleCommands.AddExperience(levelValue);
            }

            LogConsole.PopSubView();
            LogConsole.PopSubView();
            LogConsole.Hide();
        }
    }
}