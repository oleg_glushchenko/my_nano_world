﻿using MobileConsole;
using MobileConsole.UI;

namespace NanoReality.Debugging.ConsoleCommands
{
    [ExecutableCommand(name = "Cheats/Activate edit mode")]
    public class EnableEditModeCommand : Command
    {
        private EnableEditModeViewBuilder _viewBuilder;

        public override void Execute()
        {
            if(_viewBuilder == null)
                _viewBuilder = new EnableEditModeViewBuilder();
            info.shouldCloseAfterExecuted = true;
            LogConsole.PushSubView(_viewBuilder);
        }
    }
    
    public class EnableEditModeViewBuilder : ViewBuilder
    {
        public override void OnPrepareToShow()
        {
            base.OnPrepareToShow();
            MobileConsoleContext.Instance.jDebugConsoleCommands.ActivateEditMode();
            LogConsole.PopSubView();
			LogConsole.Hide();
        }
    }
}