﻿using MobileConsole;
using MobileConsole.UI;

namespace NanoReality.Debugging.ConsoleCommands
{
    [ExecutableCommand(name = "Cheats/Add Taxes To Collect", order = 1)]
    public class AddTaxesToCollectCommand : Command
    {
        private AddTaxesToCollectCommandViewBuilder _viewBuilder;

        public override void Execute()
        {
            if (_viewBuilder == null)
                _viewBuilder = new AddTaxesToCollectCommandViewBuilder();
            info.shouldCloseAfterExecuted = false;
            LogConsole.PushSubView(_viewBuilder);
        }
    }

    public class  AddTaxesToCollectCommandViewBuilder : ViewBuilder
    {
        private string _taxesAmount = "1000";
        
        public AddTaxesToCollectCommandViewBuilder()
        {
            InputNodeView input = AddInput("Taxes to Add", _taxesAmount, OnInputChanged);
            input.isNumeric = true;
            AddButton("Add amount", "action", AddTaxes);
            closeAllSubViewOnAction = true;
        }

        private void OnInputChanged(InputNodeView node)
        {
            _taxesAmount = node.value;
        }

        private void AddTaxes(GenericNodeView node)
        {
            if (int.TryParse(_taxesAmount, out var currencyAmount))
            {
                MobileConsoleContext.Instance.jDebugConsoleCommands.jServerCommunicator.CheatAddTaxesToCollect(currencyAmount);
            }

            LogConsole.PopSubView();
            LogConsole.PopSubView();
            LogConsole.Hide();
        }
    }
}