﻿using Assets.NanoLib.Utilities.DataManager.Models.api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using strange.extensions.mediation.impl;

namespace NanoReality.Debugging.ConsoleCommands
{
    public class MobileConsoleContext : View
    {
        public static MobileConsoleContext Instance { get; private set; }

        [Inject] public DebugConsoleCommands jDebugConsoleCommands { get; set; }
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public ILocalDataManager jLocalDataManager { get; set; }

        protected override void Start()
        {
            base.Start();

            Instance = this;
        }
    }
}