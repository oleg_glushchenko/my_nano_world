﻿using MobileConsole;
using MobileConsole.UI;

namespace NanoReality.Debugging.ConsoleCommands
{
    [ExecutableCommand(name = "Cheats/Add Premium Currency", order = 1)]
    public class AddPremiumCurrencyCommand : Command
    {
        private AddPremiumCurrencyViewBuilder _viewBuilder;

        public override void Execute()
        {
            if (_viewBuilder == null)
                _viewBuilder = new AddPremiumCurrencyViewBuilder();
            info.shouldCloseAfterExecuted = false;
            LogConsole.PushSubView(_viewBuilder);
        }
    }

    public class AddPremiumCurrencyViewBuilder : ViewBuilder
    {
        private string _lastUserInput = "1";
        
        public AddPremiumCurrencyViewBuilder()
        {
            DrawSearchControl();
            closeAllSubViewOnAction = true;
        }
		
        void DrawSearchControl()
        {
            InputNodeView input = AddInput("Premium currency", _lastUserInput, OnInputChanged);
            input.isNumeric = true;
            AddButton("Add amount", "action", AddLevel);
        }

        private void OnInputChanged(InputNodeView node)
        {
            _lastUserInput = node.value;
        }

        private void AddLevel(GenericNodeView node)
        {
            if (int.TryParse(_lastUserInput, out var currencyAmount))
            {
                MobileConsoleContext.Instance.jDebugConsoleCommands.AddHardCurrency(currencyAmount);
            }

            LogConsole.PopSubView();
            LogConsole.PopSubView();
            LogConsole.Hide();
        }
    }
}