﻿using MobileConsole;
using MobileConsole.UI;

namespace NanoReality.Debugging.ConsoleCommands
{
    [ExecutableCommand(name = "User/Clear all and reset")]
    public class ClearAllAndResetCommand : Command
    {
        private ClearAllAndResetViewBuilder _viewBuilder;

        public override void Execute()
        {
            if(_viewBuilder == null)
                _viewBuilder = new ClearAllAndResetViewBuilder();
            info.shouldCloseAfterExecuted = true;
            LogConsole.PushSubView(_viewBuilder);
        }
    }
    
    public class ClearAllAndResetViewBuilder : ViewBuilder
    {
        public override void OnPrepareToShow()
        {
            base.OnPrepareToShow();
            MobileConsoleContext.Instance.jDebugConsoleCommands.ClearAllAndReset();
            LogConsole.PopSubView();
			LogConsole.Hide();
        }
    }
}