﻿using MobileConsole;
using MobileConsole.UI;

namespace NanoReality.Debugging.ConsoleCommands
{
    [ExecutableCommand(name = "Cheats/Add Gold Currency")]
    public class AddGoldCommand : Command
    {
        private AddGoldViewBuilder _viewBuilder;

        public override void Execute()
        {
            if (_viewBuilder == null)
                _viewBuilder = new AddGoldViewBuilder();
            info.shouldCloseAfterExecuted = false;
            LogConsole.PushSubView(_viewBuilder);
        }
    }

    public class AddGoldViewBuilder : ViewBuilder
    {
        private string _lastUserInput = "1";
        
        public AddGoldViewBuilder()
        {
            InputNodeView input = AddInput("Amount", _lastUserInput, OnInputChanged);
            input.isNumeric = true;
            AddButton("Add gold", "action", AddLevel);
            closeAllSubViewOnAction = true;
        }

        private void OnInputChanged(InputNodeView node)
        {
            _lastUserInput = node.value;
        }

        private void AddLevel(GenericNodeView node)
        {
            if (int.TryParse(_lastUserInput, out var currencyAmount))
            {
                MobileConsoleContext.Instance.jDebugConsoleCommands.AddGoldCurrency(currencyAmount);
            }

            LogConsole.PopSubView();
            LogConsole.PopSubView();
            LogConsole.Hide();
        }
    }
}