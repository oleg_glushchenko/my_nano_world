﻿using System.Collections.Generic;
using NanoLib.AssetBundles;
using NanoReality.GameLogic.Settings.BuildSettings.Models.Api;

namespace NanoReality.Game.Data
{
	public class BuildSettings : IBuildSettings
    {
	    public int BundleVersion { get; set; }
	    public bool DebugAds { get; set; }
	    public bool EnableLiveAnalytics { get; set; }
	    public string CustomUdid { get; set; }
        public float UpdateNotificationsFrequency { get; set; }
        public float ApplicationPauseTimeout { get; set; }
        public int LongTouchDuration { get; set; }
        public AnalyticsType AnalyticsType { get; set; }
        public bool EnableAnalyticsDebug { get; set; }
        public bool SkipStoryline { get; set; }
        public bool DisableHardTutorial { get; set; }
        public bool DisableSoftTutorials { get; set; }
	    public bool EnablePayments { get; set; }
	    public bool ShowLookerTouchZone { get; set; }
	    public bool CheckBuildVersion { get; set; }
        public int ScreenSleepTimeout { get; set; }
	    public int VideoAdsRefreshInterval { get; set; }
	    public int LocalDataValidityHours { get; set; }
	    public List<string> AvailableLanguages { get; set; }
	    public bool ForceEnglishLanguage { get; set; }
	    public string FirstStartLanguage { get; set; }
	    public ResourcesPolicyType ResourcesPolicy { get; set; }
	    public string UnityAdsPlacementId { get; set; }
	    public string UnityAdsAndroidId { get; set; }
	    public string UnityAdsIOSId { get; set; }
	    public string GoogleRewardedAdsAndroidId { get; set; }
	    public string GoogleRewardedAdsIOSId { get; set; }
	    public string[] IOSTestDeviceIds { get; set; }
	    public string[] AndroidTestDeviceIds { get; set; }
	    public bool UseTestAds { get; set; }
        public int LongTouchProgressDelay { get; set ; }
    }
}