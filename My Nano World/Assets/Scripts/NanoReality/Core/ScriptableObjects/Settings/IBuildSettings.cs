﻿using System.Collections.Generic;
using NanoLib.AssetBundles;

namespace NanoReality.Game.Data
{
    public interface IBuildSettings
    {
	    int BundleVersion { get; set; }
	    bool DebugAds { get; set; }
	    bool EnableLiveAnalytics { get; set; }
        string CustomUdid { get; set;  }
        float UpdateNotificationsFrequency { get; set; }
        float ApplicationPauseTimeout { get; set; }
        int LongTouchDuration { get; set; }
        int LongTouchProgressDelay { get; set; }
		AnalyticsType AnalyticsType { get; set; }
        bool EnableAnalyticsDebug { get; set; }
        bool SkipStoryline { get; set; }
        bool DisableHardTutorial { get; set; }
        bool DisableSoftTutorials { get; set; }
	    bool ShowLookerTouchZone { get; set; }
        bool CheckBuildVersion { get; set; }
        int ScreenSleepTimeout { get; set; }
	    int VideoAdsRefreshInterval { get; set; }
	    int LocalDataValidityHours { get; set; }
	    List<string> AvailableLanguages { get; set; }
	    bool ForceEnglishLanguage { get; set; }
	    string FirstStartLanguage { get; set; }
	    ResourcesPolicyType ResourcesPolicy { get; set; }
	    bool EnablePayments { get; set; }
	    string UnityAdsPlacementId { get; set; }
	    string UnityAdsAndroidId { get; set; }
	    string UnityAdsIOSId { get; }
	    string GoogleRewardedAdsAndroidId { get; }
	    string GoogleRewardedAdsIOSId { get; }
	    string[] IOSTestDeviceIds { get; }
	    string[] AndroidTestDeviceIds { get; }
	    bool UseTestAds { get; set; }
    }
}