﻿using UnityEngine;

namespace NanoReality.Core.RoadConstructor
{
    /// <summary>
    /// Pathfinder - way point data holder
    /// </summary>
    public class WayPoint
    {
        public Vector2Int Position;
        public readonly int Distance = 0;
        public WayPoint(Vector2Int position)
        {
            Position = position;
        }
        public WayPoint(Vector2Int position,int distance)
        {
            Position = position;
            Distance = distance;
        }
    }
}