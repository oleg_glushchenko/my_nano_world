﻿using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoLib.Services.InputService;
using NanoReality.GameLogic.BuildingSystem;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using Assets.Scripts.Engine.BuildingSystem.CityMap;
using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using UnityEngine;

namespace NanoReality.Core.RoadConstructor
{
    /// <summary>
    /// Road Construction 
    /// How it works
    /// the constructor accepts direct calls from InputLayerChecker to CreateMapObject() - we consider it the beginning of the touch
    /// as well as signals to OnReleaseTouch() and Swipe()
    /// CreateMapObject() assigns a start point to find the path
    /// Swipe()  assigns the end to search for the path and calls the pathFinder and performs checks for rollback mechanics (DrawBack)
    ///	- PathFinder - calculates the route and sends a signal to add map objects
    ///		- in the callback of which is added the addition of new pieces to the sketch (Model.RoadSketch) through the indexed RoadSketchData
    ///	- With RoadDrawBack function	- to which you can roll back to - on aproach
    ///	- Confirm and Cancel transfer data to the server and/or clean runtime data
    ///	- exit from construction mode is now available only by a button in the corner of the screen
    /// </summary>
    /// 
    public class RoadConstructor : BaseConstructor
    {
        #region Fields

        private bool _backDrawing;

        // used in snapping
        private int _brakeSnappingDistance = 2;
        private int _stepsBackToDefineSnapping = 2;

        private Vector2Int _lastSnappingPoint;
        private float _lastSnappingPointUpdateTime;
        private int _stepToTrackBack = 10;
        private Vector2Int _lastTryPosition = Vector2Int.down;

        #endregion

        #region Properties

        public bool IsFirstRoadCreated => ConstructorModel.RoadSketch.Count > 0;
        public bool IsCreateRoad { get; set; } = false;
        public bool IsRoadBuildingMode { get; set; } = false;
        public Vector2Int LastPosition => ConstructorModel?.HeadObject?.MapPosition.ToVector2Int()??Vector2Int.down;
        public override bool IsBuildNewBuilding { get; protected set; }
        public override bool IsCurrentlySelectObject { get; set; }
        public override MapObjectView SelectedObjectView { get; set; }

        public override IMapObject SelectedModel
        {
            get => ConstructorModel.SelectedModel != null
                ? ConstructorModel.SelectedModel
                : SelectedObjectView?.MapObjectModel;
            set
            {
                HasSelection = value != null;
                ConstructorModel.SelectedModel = value;
            }
        }

        public override bool IsBuildModeEnabled
        {
            get => isBuildModeEnabled;
            set
            {
                if (isBuildModeEnabled == value)
                {
                    return;
                }

                isBuildModeEnabled = value;
                OnSelectMapObject(jGameManager.CurrentSelectedObjectView, true);
                jSignalOnConstructControllerChangeState?.Dispatch(isBuildModeEnabled);
            }
        }

        private bool DisableSnappingByHoldTime => Time.time - _lastSnappingPointUpdateTime > .33f;
        private RoadConstructorModel ConstructorModel => Context.ConstructorModel;
        private RoadSketchData RoadSketchData => Context.RoadSketchData;
        private RoadConstructorPathFinder RoadConstructorPathFinder => Context.RoadConstructorPathFinder;
        private RoadSelectionService RoadSelectionService => Context.RoadSelectionService;
        private RoadConstructorCheckers RoadConstructorCheckers => Context.RoadConstructorCheckers;
        private RoadRemover RoadRemover => Context.RoadRemover;

        #endregion

        #region Injects

        [Inject] public RoadConstructorContext Context { get; set; }

        #endregion

        #region Signals

        [Inject] public SignalOnStartRoadCreation jSignalStartRoadCreation { get; set; }
        [Inject] public SignalOnSwipe jSignalOnSwipe { get; set; }
        [Inject] public SignalOnReleaseTouch jSignalOnReleaseTouch { get; set; }
        [Inject] public SignalRoadMapReselect SignalRoadMapReselect { get; set; }
        [Inject] public TileOfRoadEditedSignal jTileOfRoadEditedSignal { get; set; }

        #endregion


        [PostConstruct]
        public void PostConstruct()
        {
            SignalRoadMapReselect.AddListener(ConstructorModel.RoadsSelectedToRemove.Clear);
        }

        [Deconstruct]
        public void OnRemove()
        {
            SignalRoadMapReselect.RemoveListener(ConstructorModel.RoadsSelectedToRemove.Clear);
        }

        //////////////  Input Section

        #region USER INPUT

        /// <summary>
        /// Creates one road tile on tap
        /// </summary>	
        public override bool CreateMapObject(Id_IMapObjectType id, BusinessSectorsTypes sectorID,
            Vector3F position = default , BuildingSubCategories buildingSubCategories = BuildingSubCategories.None)
        {
            IMapObject obj = MapObjectsGenerator.GetMapObjectModel(id, 0);
            DefaultBuildingMapId = new Id_IMapObject(DefaultBuildingMapId.Value - 1);
            obj.MapID = DefaultBuildingMapId;
            obj.SectorId = sectorID.GetBusinessSectorId();

            SelectedModel = obj;
            IsBuildModeEnabled = true;

            // if create road from BuildingListPanelView return
            if (position == Vector3F.Zero)
            {
                IsCreateRoad = true;
                jSignalStartRoadCreation.Dispatch(0);
                return true;
            }

            Vector2Int preferGridPosition = CalculatePreferGridPosition(position);
            var isValidPosition = RoadConstructorCheckers.IsPossibleToBuild(preferGridPosition) &&
                                  RoadConstructorCheckers.IsEditAllowed(preferGridPosition);

            if (!isValidPosition)
            {
                return false;
            }

            jSignalOnSwipe.AddListener(OnSwipe);
            jSignalOnReleaseTouch.AddListener(OnReleaseTouch);

            ConstructorModel.FromPoint = preferGridPosition;
            return true;
        }

        public override bool RestoreMapObject(IMapObject obj, Vector3F position = default)
        {
            return false;
        }

        private void OnSwipe(Vector3F position)
        {
            if (!RaycastCityMap(position, out _, out ICityMap map))
            {
                return;
            }

            ConstructorModel.LastMapId = map.BusinessSector.GetBusinessSectorId();
            ConstructorModel.LastCityIMap = jGameManager.GetMap(ConstructorModel.LastMapId);
            ConstructorModel.LastCityMapView = jGameManager.GetMapView(ConstructorModel.LastMapId);

            Vector2Int mapPosition = CalculatePreferGridPosition(position);
            TryToBuildRoad(mapPosition);
        }

        private void OnReleaseTouch()
        {
            ConstructorModel.FromPoint = Vector2Int.down;
            ConstructorModel.ToPoint = Vector2Int.down;
        }

        private void TryToBuildRoad(Vector2Int position)
        {
            if (!RoadConstructorCheckers.IsPossibleToBuild(position))
            {
                return;
            }

            // avoid useless repetition of calculation
            if (position == _lastTryPosition && IsFirstRoadCreated)
            {
                return;
            }

            _lastTryPosition = position;

            // rollback mechanics
            OnBackDrawing(position);

            // Look for a route
            ConstructorModel.ToPoint = position;
            RoadConstructorPathFinder.FindPath();
            jTileOfRoadEditedSignal.Dispatch();
        }

        #endregion

        #region Cancel / Confirm / Select - Inputs

        public override void CancelChangeObject()
        {
            if (SelectedModel == null)
            {
                return;
            }

            RoadViewGrid roadGrid = jGameManager.GetMapView(SelectedModel.SectorId).RoadGrid;
            roadGrid.DeselectRoads(ConstructorModel.RoadsSelectedToRemove);

            SwitchOffBuildingMode();

            ConstructorModel.RoadsSelectedToRemove.Clear();
            RoadSelectionService.DropRemovalSelection();

            jSignalOnSwipe.RemoveListener(OnSwipe);
            jSignalOnReleaseTouch.RemoveListener(OnReleaseTouch);

            RoadSelectionService.RemoveFlags();

            // For the existing objects that we covered - need to deselect
            RoadSelectionService.Deselect();
            RoadSketchData.ClearAndRemoveNewRoadObjects();

            roadGrid.UpdateRoads();
        }

        public override void ConfirmChangeObject()
        {
            RoadSelectionService.Deselect();

            if (!IsRoadBuildingMode || IsCreateRoad || ConstructorModel.RoadsSelectedToRemove.Count == 0)
            {
                SaveRoad();
            }
            else
            {
                RoadRemover.DeleteRoad();
            }

            // todo по добру надо бы добавить в туториал сразу после постройки дороги шаг - клацнуть на выход из конструктора дорог, а это убрать вместе с инджектом
            if (!Context.HardTutorial.IsTutorialCompleted)
            {
                SwitchOffBuildingMode();
            }
        }

        public override void OnSelectMapObject(MapObjectView view, bool isSelected)
        {
            RoadSelectionService.SelectRoadsForDestruction(view);
        }

        #endregion

        public override void SwitchOffBuildingMode()
        {
            if (!isBuildModeEnabled)
            {
                return;
            }

            base.SwitchOffBuildingMode();
            IsCreateRoad = false;
            IsRoadBuildingMode = false;
        }

        private void SaveRoad()
        {
            if (ConstructorModel.RoadSketch.Count == 0)
            {
                return;
            }

            ICityMap map = jGameManager.GetMap(ConstructorModel.RoadSketch[0].SectorId);

            var mapObjectsToMount = new List<IMapObject>();


            for (var index = ConstructorModel.RoadSketch.Count - 1; index >= 0; index--)
            {
                IMapObject mapObject = ConstructorModel.RoadSketch[index];
                var inStack = false;
                for (var index2 = 0; index2 < mapObjectsToMount.Count; index2++)
                {
                    if (mapObjectsToMount[index2].MapPosition == mapObject.MapPosition)
                    {
                        inStack = true;
                        break;
                    }
                }

                if (!inStack && mapObject.MapID < 0)
                {
                    mapObjectsToMount.Add(mapObject);
                }
            }

            map?.MountRoads(mapObjectsToMount);

            RoadSelectionService.Deselect();
            RoadSketchData.ClearAndShowEffects();
        }

        /// <summary>
        /// Determines the coordinate of the cell on which tap was made
        /// Applying snapping correction if needed
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        private Vector2Int CalculatePreferGridPosition(Vector3F position = default)
        {
            // Out of field tap
            if (!RaycastCityMap(position, out RaycastHit hit, out ICityMap map))
            {
                return Vector2Int.down;
            }

            Vector3 hitPointPosition = hit.point;
            
            ConstructorModel.LastMapId = map.BusinessSector.GetBusinessSectorId();
            if (!IsFirstRoadCreated)
            {
                ConstructorModel.FirstRoadMapId = ConstructorModel.LastMapId;
            }
            ConstructorModel.LastCityIMap = jGameManager.GetMap(ConstructorModel.LastMapId);
            ConstructorModel.LastCityMapView = jGameManager.GetMapView(ConstructorModel.LastMapId);

            if (ConstructorModel.LastCityMapView == null)
            {
                return Vector2Int.down;
            }

            // Map alignment
            Vector2Int centerPosition = ConstructorModel.LastCityMapView.ConvertToGridPos(hitPointPosition);

            return ApplySnapping(centerPosition);
        }

        private Vector2Int ApplySnapping(Vector2Int centerPosition)
        {
            if (_lastSnappingPoint != centerPosition)
            {
                _lastSnappingPoint = centerPosition;
                _lastSnappingPointUpdateTime = Time.time;
            }

            if (DisableSnappingByHoldTime)
            {
                return centerPosition;
            }

            var roadLength = ConstructorModel.RoadSketch.Count;
            if (_stepsBackToDefineSnapping > roadLength)
            {
                return centerPosition;
            }

            Vector2Int position = centerPosition;

            var x = ConstructorModel.RoadSketch[roadLength - 1].MapPosition.intX;
            var y = ConstructorModel.RoadSketch[roadLength - 1].MapPosition.intY;

            var lineOnX = true;
            var lineOnY = true;

            for (var index = roadLength - _stepsBackToDefineSnapping; index < roadLength; index++)
            {
                if (x != ConstructorModel.RoadSketch[index].MapPosition.intX)
                {
                    lineOnX = false;
                }

                if (y != ConstructorModel.RoadSketch[index].MapPosition.intY)
                {
                    lineOnY = false;
                }
            }

            if (lineOnX && Mathf.Abs(centerPosition.x - x) < _brakeSnappingDistance)
            {
                position = new Vector2Int(x, centerPosition.y);
            }

            if (lineOnY && Mathf.Abs(centerPosition.y - y) < _brakeSnappingDistance)
            {
                position = new Vector2Int(centerPosition.x, y);
            }

            return position;
        }

        #region Drawback Mechanic

        // User tries to erase the road by driving the mouse back 
        private void OnBackDrawing(Vector2Int position)
        {
            _backDrawing = false;


            if (ConstructorModel.RoadSketch.Count == 0)
            {
                return;
            }

            _backDrawing = DrawBackCheck(position);


            if (!_backDrawing)
            {
                return;
            }

            RoadSelectionService.Select();

            if (ConstructorModel.RoadSketch.Count > 0)
            {
                ConstructorModel.FromPoint = ConstructorModel.RoadSketch[ConstructorModel.RoadSketch.Count-1].MapPosition.ToVector2Int();
            }
            else
            {
                ConstructorModel.FromPoint = position;
            }
        }

        private bool DrawBackCheck(Vector2Int position)
        {
            var backDrawing = false;
            var positionInRoadsList = -1;

            if (ConstructorModel.RoadSketch.Count == 0)
            {
                return false;
            }

            if (ConstructorModel.RoadSketch[ConstructorModel.RoadSketch.Count - 1].MapPosition != position)
            {
                var endOfTrack = ConstructorModel.RoadSketch.Count - _stepToTrackBack >= 0
                    ? ConstructorModel.RoadSketch.Count - _stepToTrackBack
                    : 0;

                for (var index = ConstructorModel.RoadSketch.Count - 2; index >= endOfTrack; index--)
                {
                    if (position == ConstructorModel.RoadSketch[index].MapPosition.ToVector2Int())
                    {
                        positionInRoadsList = index;
                    }
                }
            }

            if (positionInRoadsList != -1)
            {
                while (ConstructorModel.RoadSketch.Count > 0 && positionInRoadsList < ConstructorModel.RoadSketch.Count)
                {
                    backDrawing = true;
                    RoadSketchData.RemoveLastRoad();
                }
            }

            return backDrawing;
        }

        #endregion
    }
}