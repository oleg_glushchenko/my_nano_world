﻿using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.UI;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.GameManager.Models.api;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;

namespace NanoReality.Core.RoadConstructor
{
    public class RoadSelectionService
    {
        private RoadConstructorContext _context;
        private IWorldSpaceCanvas WorldCanvas => _context.WorldCanvas;
        private IGameManager GameManager => _context.GameManager;
        private RoadConstructor RoadConstructor => _context.RoadConstructor;
        private RoadConstructorModel ConstructorModel => _context.ConstructorModel;

        public RoadSelectionService(RoadConstructorContext context)
        {
            _context = context;
        }

        public void Select()
        {
            for (var index = 0; index < ConstructorModel.RoadSketch.Count; index++)
            {
                SetRoadMarker(ConstructorModel.RoadSketch[index], false, false, RoadTileState.New);
            }

            SetRoadMarker(ConstructorModel.HeadObject, true, true, RoadTileState.Head);
            SetRoadMarker(ConstructorModel.TailObject, true, false, RoadTileState.New);
        }

        public void Deselect()
        {
            for (var index = 0; index < ConstructorModel.RoadSketch.Count; index++)
            {
                SetRoadMarker(ConstructorModel.RoadSketch[index], false, false, RoadTileState.Normal);
            }
        }

        public void RemoveFlags()
        {
            if (ConstructorModel.HeadObject != null)
            {
                SetRoadMarker(ConstructorModel.HeadObject, false, true, RoadTileState.Normal);
            }

            if (ConstructorModel.TailObject != null)
            {
                SetRoadMarker(ConstructorModel.TailObject, false, false, RoadTileState.Normal);
            }
        }

        public void SetRoadMarker(IMapObject road, bool isVisible, bool isHead, RoadTileState state)
        {
            if (road == null)
            {
                return;
            }

            var view = (RoadMapObjectView) GameManager.GetMapObjectView(road);
            SetRoadMarker(view, isVisible, isHead, state);
        }

        private void SetRoadMarker(RoadMapObjectView road, bool isVisible, bool isHead, RoadTileState state)
        {
            road.ShowRoadEnd(WorldCanvas, isVisible, isHead, state);
        }

        // Ramover    
        public void SelectRoadsForDestruction(MapObjectView view)
        {
            DropRemovalSelection();

            if (!RoadConstructor.IsBuildModeEnabled)
            {
                return;
            }

            if (!RoadConstructor.IsCreateRoad)
            {
                RoadConstructor.IsRoadBuildingMode = true;
            }

            if (!(view is RoadMapObjectView gameObject))
            {
                return;
            }

            RoadConstructor.IsCurrentlySelectObject = true;
            RoadConstructor.SelectedObjectView = gameObject;
            RoadConstructor.SelectedModel = gameObject.MapObjectModel;

            if (!ConstructorModel.RoadsSelectedToRemove.Contains(gameObject))
            {
                ConstructorModel.RoadsSelectedToRemove.Add(gameObject);
            }
        }

        public void DropRemovalSelection()
        {
            RoadConstructor.SelectedModel = null;
            RoadConstructor.SelectedObjectView = null;
            RoadConstructor.IsCurrentlySelectObject = false;
        }
    }
}
