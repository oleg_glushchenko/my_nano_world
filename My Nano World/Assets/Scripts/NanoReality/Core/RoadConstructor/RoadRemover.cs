﻿using System.Collections.Generic;
using Assets.Scripts.Engine.BuildingSystem.CityMap;
using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.GameManager.Models.api;

namespace NanoReality.Core.RoadConstructor
{
    public class RoadRemover
    {
        private RoadConstructorContext _context;
        private IGameManager GameManager => _context.GameManager;
        private RoadConstructor RoadConstructor => _context.RoadConstructor;
        private RoadSelectionService RoadSelectionService => _context.RoadSelectionService;
        private RoadConstructorModel ConstructorModel => _context.ConstructorModel;

        public RoadRemover(RoadConstructorContext context)
        {
            _context = context;
        }

        public void DeleteRoad()
        {
            DefineCityIMap();

            var idIMapObjects = new List<Id_IMapObject>();
            ICityMap lastCityMap = ConstructorModel.LastCityIMap;

            for (var i = 0; i < ConstructorModel.RoadsSelectedToRemove.Count; i++)
            {
                idIMapObjects.Add(ConstructorModel.RoadsSelectedToRemove[i].MapId);
            }

            RoadConstructor.CityMap?.DeleteRoad(idIMapObjects, ConstructorModel.LastCityMapView.RoadGrid.UpdateRoads);

            // If there is only one road tile for deletion, just remove it from city map
            if (ConstructorModel.RoadsSelectedToRemove.Count == 1)
            {
                lastCityMap.RemoveMapObjectById(ConstructorModel.RoadsSelectedToRemove[0].MapId);
            }
            else if (ConstructorModel.RoadsSelectedToRemove.Count > 1)
            {
                // If there are many road tiles we need to avoid extra work and optimize it
                RoadViewGrid roadGrid = ConstructorModel.LastCityMapView.RoadGrid;

                // setting road grid to construction mode helps to avoid props generation
                roadGrid.SetUnderConstruction(true);
                for (var index = 0; index < ConstructorModel.RoadsSelectedToRemove.Count; index++)
                {
                    lastCityMap.RemoveMapObjectById(ConstructorModel.RoadsSelectedToRemove[index].MapId);
                }

                // after deletion, just return to normal mode
                roadGrid.SetUnderConstruction(false);
                roadGrid.UpdateRoads();
            }

            ConstructorModel.RoadsSelectedToRemove.Clear();
            RoadSelectionService.DropRemovalSelection();
        }

        private void DefineCityIMap()
        {
            if (ConstructorModel.RoadsSelectedToRemove.Count == 0)
            {
                return;
            }

            ConstructorModel.LastMapId = ConstructorModel.RoadsSelectedToRemove[0].MapObjectModel.SectorId;
            ConstructorModel.LastCityIMap = GameManager.GetMap(ConstructorModel.LastMapId);
            ConstructorModel.LastCityMapView = GameManager.GetMapView(ConstructorModel.LastMapId);
        }
    }
}
