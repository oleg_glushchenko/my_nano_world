﻿using System.Collections.Generic;
using UnityEngine;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.BuildingSystem.CityMap;
using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;

namespace NanoReality.Core.RoadConstructor
{
    /// <summary>
    /// All the data used in Roads Construction process
    /// </summary>
    public class RoadConstructorModel
    {
        private RoadConstructorContext _context;

        public RoadConstructorModel(RoadConstructorContext context)
        {
            _context = context;
        }

        // Sketch Coordinator 
        public List<IMapObject> RoadSketch = new List<IMapObject>();

        // Draw From To 
        public Vector2Int FromPoint { get; set; } = Vector2Int.down;
        public Vector2Int ToPoint { get; set; } = Vector2Int.down;

        // Head and Tails
        public IMapObject HeadObject => RoadSketch.Count > 0 ? RoadSketch[RoadSketch.Count - 1] : null;
        public IMapObject TailObject => RoadSketch.Count > 0 ? RoadSketch[0] : null;

        // Reusable references  
        public Id<IBusinessSector> LastMapId { get; set; } = -1;
        public Id<IBusinessSector> FirstRoadMapId { get; set; } = -1;
        public CityMapView LastCityMapView { get; set; } = null;
        public ICityMap LastCityIMap { get; set; } = null;
        public IMapObject SelectedModel { get; set; }

        // List of selected road tiles for deletion
        public List<RoadMapObjectView> RoadsSelectedToRemove { get; } = new List<RoadMapObjectView>();
    }
}