using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.UI;
using NanoReality.Game.Tutorial;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.Services;

namespace NanoReality.Core.RoadConstructor
{
    public class RoadConstructorContext
    {
        [Inject] public IHardTutorial jHardTutorial { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public RoadConstructor jRoadConstructor { get; set; }
        [Inject] public IMapObjectsGenerator jMapObjectsGenerator { get; set; }
        [Inject] public MapObjectAddedSignal jMapObjectAddedSignal { get; set; }
        [Inject] public TileOfRoadEditedSignal jTileOfRoadEditedSignal { get; set; }
        [Inject] public IWorldSpaceCanvas jWorldCanvas { get; set; }
        [Inject] public ICitySectorService jCitySectorService { get; set; }

        public IHardTutorial HardTutorial => jHardTutorial;
        public IGameManager GameManager => jGameManager;
        public RoadConstructor RoadConstructor => jRoadConstructor;
        public IMapObjectsGenerator MapObjectsGenerator => jMapObjectsGenerator;
        public MapObjectAddedSignal MapObjectAddedSignal => jMapObjectAddedSignal;
        public TileOfRoadEditedSignal TileOfRoadEditedSignal => jTileOfRoadEditedSignal;
        public IWorldSpaceCanvas WorldCanvas => jWorldCanvas;
        public ICitySectorService CitySectorService => jCitySectorService;
        
        public RoadConstructorModel ConstructorModel;
        public RoadSketchData RoadSketchData;
        public RoadConstructorPathFinder RoadConstructorPathFinder;
        public RoadSelectionService RoadSelectionService;
        public RoadConstructorCheckers RoadConstructorCheckers;
        public RoadRemover RoadRemover;

        [PostConstruct]
        public void Init()
        {
            ConstructorModel = new RoadConstructorModel(this);
            RoadSketchData = new RoadSketchData(this);
            RoadConstructorPathFinder = new RoadConstructorPathFinder(this);
            RoadSelectionService = new RoadSelectionService(this);
            RoadConstructorCheckers = new RoadConstructorCheckers(this);
            RoadRemover = new RoadRemover(this);
        }
    }
}