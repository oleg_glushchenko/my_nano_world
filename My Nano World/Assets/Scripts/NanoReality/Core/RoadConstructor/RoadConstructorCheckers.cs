﻿using System.Collections.Generic;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Services;
using NanoReality.Game.Tutorial;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using UnityEngine;

namespace NanoReality.Core.RoadConstructor
{
    public class RoadConstructorCheckers
    {
        private ICitySectorService CityAreaService => _context.CitySectorService;
        private IHardTutorial HardTutorial => _context.HardTutorial;
        private RoadConstructorModel ConstructorModel => _context.ConstructorModel;

        private RoadConstructorContext _context;

        public RoadConstructorCheckers(RoadConstructorContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Check for the ability to create a road tile
        /// </summary>
        /// <param name="position">Road Build Position</param>
        public bool IsPossibleToBuild(Vector2Int position)
        {
            if (ConstructorModel?.LastCityIMap == null)
            {
                return false;
            }

            if (ConstructorModel.LastMapId != ConstructorModel.FirstRoadMapId)
            {
                return false;
            }
            
            ICityMap lastCityMap = ConstructorModel.LastCityIMap;

            // Check for going out of the playing field
            var xOutOffGrid = position.x < 0 || position.x >= lastCityMap.jCityGrid.Width;
            var yOutOffGrid = position.y < 0 || position.y >= lastCityMap.jCityGrid.Height;
            if (xOutOffGrid || yOutOffGrid)
            {
                return false;
            }
            
            Id_IMapObjectType idIMapObjectType = ConstructorModel.SelectedModel.ObjectTypeID;
            
            if (CityAreaService.ContainsDisableAreas(lastCityMap.BusinessSector, position, idIMapObjectType))
            {
                return false;
            }

            if (CityAreaService.ContainsDisableAreas(lastCityMap.BusinessSector, position, idIMapObjectType))
            {
                return false;
            }

            if (HardTutorial.IsBuildingApprovedByTutorial(position) == false)
            {
                return false;
            }

            // Check for intersection with a locked sector
            var sectors = lastCityMap.SubLockedMap.LockedSectors;
            for (var i = 0; i < sectors.Count; i++)
            {
                if (sectors[i].LockedSector.Contains(position))
                {
                    return false;
                }
            }

            // prohibit creating a road on top of an existing OBJECTS - but make a exception for roads
            IMapObject mapObject = lastCityMap.GetMapObject(position);
            if (mapObject != null && mapObject.MapObjectType != MapObjectintTypes.Road)
            {
                return false;
            }

            return true;
        }

        // check is coordinates ar neighbour - left right up down or same position points will return true
        public bool IsNeighbor(Vector2Int positionA, Vector2Int positionB)
        {
            return Vector2.Distance(positionA, positionB) <= 1;
        }

        // Says whether it is possible to build on the basis of whether we are close to the tail or maybe a new section
        private void FlipRoad()
        {
            ConstructorModel.RoadSketch.Reverse();
        }

        private void DefineHeadOrTailEditDirectionIs(Vector2Int position)
        {
            if (ConstructorModel.RoadSketch.Count == 0)
            {
                return;
            }

            if (IsNeighbor(ConstructorModel.TailObject.MapPosition.ToVector2Int(), position))
            {
                FlipRoad();
            }
        }

        public bool IsEditAllowed(Vector2Int position)
        {
            DefineHeadOrTailEditDirectionIs(position);

            // if nothing has been build yet, then you can build
            if (ConstructorModel.RoadSketch.Count == 0)
            {
                return true;
            }

            return IsNeighbor(ConstructorModel.HeadObject.MapPosition.ToVector2Int(), position) ||
                   IsNeighbor(ConstructorModel.TailObject.MapPosition.ToVector2Int(), position);
        }
    }
}