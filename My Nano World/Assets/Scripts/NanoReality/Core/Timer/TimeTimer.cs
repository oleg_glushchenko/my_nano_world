﻿using System;
using Assets.NanoLib.UtilitesEngine;
using NanoLib.Core.Timers;
using UniRx;

namespace NanoReality.Engine.Utilities
{
	public sealed class TimeTimer : ITimer
    {
	    public float TimeoutDuration { get; private set; }
	    public float TimeLeft => TimeoutDuration - _timePassed;

	    public event Action OnFinishedAction;
	    public event Action OnCancelTimerAction;
	    private event Action<float> OnTickAction;

	    private float _timeoutTickDuration;
	    private float _timePassed;
	    private long _startUtc;
	    
	    private readonly ServerTimeTickSignal _serverTimeTickSignal;
	    private IDisposable _timerDisposable;

	    public TimeTimer(ServerTimeTickSignal serverTimeTickSignal)
        {
	        _serverTimeTickSignal = serverTimeTickSignal;
        }

	    public void InitTimer(long curUtc)
        {
	        _startUtc = curUtc;
        }

	    public void StartRealTimer(float durationTime, Action onFinishedAction = null, Action<float> onTickAction = null, float tickDuration = 1f)
	    {
		    TimeoutDuration = durationTime;
		    _timeoutTickDuration = tickDuration;
		    
		    OnFinishedAction += onFinishedAction;
		    OnTickAction += onTickAction;
		    
		    var timeSpanDuration = TimeSpan.FromSeconds(_timeoutTickDuration);

		    _timerDisposable = Observable.Interval(timeSpanDuration).Subscribe(OnIntervalCounterTick);

		    void OnIntervalCounterTick(long value)
		    {
			    //Parameter value starts counts from 0.
			    var intervalsPassed = value + 1;
			    _timePassed = intervalsPassed * tickDuration;

			    if (_timePassed >= TimeoutDuration)
			    {
				    OnFinishedAction?.Invoke();
				    OnCancelTimerAction?.Invoke();
				    _timerDisposable.Dispose();
			    }
			    
			    OnTickAction?.Invoke(_timePassed);
		    }
	    }

	    public void StartServerTimer(float durationTime, Action onFinishedAction, Action<float> onTickAction)
	    {
		    TimeoutDuration = durationTime;
		    OnFinishedAction += onFinishedAction;
		    OnTickAction += onTickAction;
		    _serverTimeTickSignal.AddListener(OnServerTimeTick);

		    OnCancelTimerAction += () =>
		    {
			    _serverTimeTickSignal.RemoveListener(OnServerTimeTick);
		    };
		    
		    void OnServerTimeTick(long currentUtc)
		    {
			    _timePassed = currentUtc - _startUtc;
		    
			    if (_timePassed >= TimeoutDuration) 
			    {
				    _serverTimeTickSignal?.RemoveListener(OnServerTimeTick);
				    OnFinishedAction?.Invoke();
				    OnCancelTimerAction?.Invoke();
				    return;
			    }

			    OnTickAction?.Invoke(_timePassed);
		    }
	    }
	    
	    public void CancelTimer(bool complete = false)
	    {
		    if (complete)
		    {
			    OnFinishedAction?.Invoke();
		    }
		    
		    OnCancelTimerAction?.Invoke();
		    
		    Dispose();
	    }

	    public void Dispose()
	    {
		    _timePassed = default;
		    TimeoutDuration = default;
		    _timeoutTickDuration = default;
		    OnFinishedAction = null;
		    OnTickAction = null;
		    OnCancelTimerAction = null;
		    _startUtc = default;
		    _timerDisposable?.Dispose();
	    }
    }
}