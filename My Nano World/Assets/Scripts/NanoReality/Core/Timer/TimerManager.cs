﻿using System;
using System.Collections.Generic;
using Assets.NanoLib.UtilitesEngine;
using NanoLib.Core.Logging;
using NanoLib.Core.Timers;
using NanoReality.Engine.Settings.BuildSettings;
using NanoReality.Engine.Utilities;
using NanoReality.GameLogic.GameManager;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using UniRx;
using UnityEngine;

namespace NanoReality.Core.Timer
{
    public sealed class TimerManager : ITimerManager, IDisposable
    {
        [Inject] public ServerTimeTickSignal jServerTimeTickSignal { get; set; }
        [Inject] public IServerCommunicator ServerCommunicator { get; set; }
        [Inject] public NetworkSettings jNetworkSettings { get; set; }
        [Inject] public SignalOnApplicationPause jSignalOnApplicationPause { get; set; }

        private readonly List<ITimer> _timers = new List<ITimer>();
        private double _pauseStartTime;
        private double _currentUTC;
        private double _startUTC;
        private bool _isSynchronizingTimeWithServer;
        private bool _isInitialized;
        private IDisposable _serverTimerDisposable;
        private const double TimerDelay = 1;
        private Transform _timersContainer;

        public long CurrentUTC => Convert.ToInt64(Math.Round(_currentUTC));
        public double SessionLength => _currentUTC - _startUTC;

        [PostConstruct]
        public void PostConstruct()
        {
            jSignalOnApplicationPause.AddListener(OnApplicationPause);

            SetServerTime(DateTimeUtils.GetUnixTimestamp(DateTime.UtcNow));
        }

        public void Init(long utc)
        {
            SetServerTime(utc);
            _startUTC = utc;
            _isInitialized = true;
        }

        public void Dispose()
        {
            jSignalOnApplicationPause.RemoveListener(OnApplicationPause);

            foreach (var timer in _timers) 
                timer.Dispose();

            _serverTimerDisposable?.Dispose();
        }

        public void StartSyncServerTimeTimer()
        {
            // TODO: need replace this code to some command.
            if (jNetworkSettings.ServerTimeSyncInterval > 0)
            {
                var timeRequestTimer = StartRealTimer(float.MaxValue, null, OnServerTimeUpdate, jNetworkSettings.ServerTimeSyncInterval);
                _timers.Add(timeRequestTimer);
            }
        }

        public ITimer StartRealTimer(float durationTime, Action onFinishedAction = null, Action<float> onTickAction = null, float tickDuration = 1f)
        {
            var timer = CreateNewTimer();
            timer.InitTimer(-1);
            timer.StartRealTimer(durationTime, onFinishedAction, onTickAction, tickDuration);
            return timer;
        }

        public ITimer StartServerTimer(float durationTime, Action onFinishedAction, Action<float> onTickAction = null)
        {
            var timer = CreateNewTimer();
            timer.InitTimer(CurrentUTC);
            timer.StartServerTimer(durationTime, onFinishedAction, onTickAction);
            return timer;
        }

        public ITimer StartServerTimer(long endTimerUtc, Action onFinished, Action<float> onTickCallback = null)
        {
            var timerDuration = Convert.ToSingle(endTimerUtc - _currentUTC);
            if (timerDuration < 0)
            {
                Logging.LogError(LoggingChannel.Core, "EndTimer UTC less than CurrentUtc.");
            }

            var timer = CreateNewTimer();
            timer.InitTimer(CurrentUTC);
            timer.StartServerTimer(timerDuration, onFinished, onTickCallback);
            return timer;
        }

        private void SetServerTime(long utc)
        {
            _currentUTC = utc;
            if (_serverTimerDisposable == null)
            {
                StartServerTimer();
            }
        }

        private void StartServerTimer()
        {
            _serverTimerDisposable = Observable.Timer(TimeSpan.FromSeconds(TimerDelay))
                .Repeat()
                .Subscribe(_ => { OnServerTimerTick(); });
        }

        private void OnServerTimerTick()
        {
            _currentUTC += TimerDelay;
            jServerTimeTickSignal.Dispatch(CurrentUTC);
        }

        private void OnApplicationPause(bool isPaused)
        {
            if (!_isInitialized) return;

            if (isPaused)
            {
                _pauseStartTime = DateTime.UtcNow.TotalSeconds();
            }
            else
            {
                _currentUTC = _currentUTC + (DateTime.UtcNow.TotalSeconds() - _pauseStartTime);
                SetServerTime((long) _currentUTC);

                // force server time update
                OnServerTimeUpdate(0f);
            }
        }

        private void OnServerTimeUpdate(float elapsedTime)
        {
            if (_isSynchronizingTimeWithServer) return;

            _isSynchronizingTimeWithServer = true;

            ServerCommunicator.GetServerTime(OnServerTimeReceived);
        }

        private void OnServerTimeReceived(long time)
        {
            _isSynchronizingTimeWithServer = false;

            SetServerTime(time);
        }

        private TimeTimer CreateNewTimer()
        {
            var timer = new TimeTimer(jServerTimeTickSignal);
            _timers.Add(timer);
            
            return timer;
        }

        // this method used by facebook sdk
        public void OnHideUnity(bool isGameShown)
        {
            Time.timeScale = isGameShown ? 1 : 0;
        }
    }
}