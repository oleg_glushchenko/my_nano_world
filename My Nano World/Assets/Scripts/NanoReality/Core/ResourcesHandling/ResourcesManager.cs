using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.Settings.BuildSettings;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using UnityEngine;

namespace NanoReality.Core.Resources
{
    public sealed class ResourcesManager : IResourcesManager
    {
        [Inject] public ResourcesPolicy jResourcesPolicy { get; set; }
        [Inject] public NetworkSettings jNetworkSettings { get; set; }

        [PostConstruct]
        public void PostConstruct()
        {
            AtlasManager.Subscribe();
            AssetBundlePath.AssetBundlesFolderURI = jNetworkSettings.AssetBundlesUrl;
        }

        public T GetBuildingPrefab<T>(MapObjectId id) where T : MapObjectView
        {
            return jResourcesPolicy.GetAsset<T>(id);
        }

        public Sprite GetBuildingSprite(MapObjectId id)
        {
            return jResourcesPolicy.GetAsset<Sprite>(id);
        }

        public Sprite GetProductSprite(int productId)
        {
            return jResourcesPolicy.GetProductSprite(productId);
        }

        public Sprite GetEventSprite(int index)
        {
            return jResourcesPolicy.GetEventSprite(index);
        }

        public Mesh GetBuildingSpriteMesh(MapObjectId id)
        {
            return jResourcesPolicy.GetBuildingSpriteMesh(id);
        }

        public GameObject GetGlobalMap()
        {
            return jResourcesPolicy.GlobalMap;
        }

        public void UnloadBundle(string bundleName)
        {
            jResourcesPolicy.UnloadBundle(bundleName);
        }
    }
}