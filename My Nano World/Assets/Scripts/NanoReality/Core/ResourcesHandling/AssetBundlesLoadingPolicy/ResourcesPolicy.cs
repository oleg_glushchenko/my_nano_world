﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.Utilities.Meshes;
using NanoLib.Core.Logging;
using NanoReality.Engine.BuildingSystem;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using UnityEngine;
using Object = UnityEngine.Object;

namespace NanoReality.Core.Resources
{
	public abstract class ResourcesPolicy
	{
		private readonly Dictionary<MapObjectId, Mesh> _cachedBuildingMeshes = new Dictionary<MapObjectId, Mesh>();
		
		public abstract GameObject GlobalMap { get; }

		public abstract Sprite GetProductSprite(int productId);
		public abstract Sprite GetEventSprite(int index);
		public abstract void UnloadBundle(string bundleName);

		protected abstract BuildingConfig GetBuildingConfig(MapObjectId id);

		public T GetAsset<T>(MapObjectId id) where T : Object
		{
			var config = GetBuildingConfig(id);
			if (config == null) return null;

			T asset = null;
			switch (typeof(T))
			{
				case Type spriteType when spriteType == typeof(Sprite):
					asset = config.GetBuildingSprite(id.Level) as T;
					break;
				
				case Type mapObjectType when mapObjectType == typeof(MapObjectView):
					asset = config.GetBuildingView(id.Level) as T;
					break;
			}
			
			if (asset == null)
			{
				$"There is no assets with id {id} in building config.".LogError(LoggingChannel.AssetBundles);
			}
            
			return asset;
		}
		
		public Mesh GetBuildingSpriteMesh(MapObjectId id)
		{
			var sprite = GetAsset<Sprite>(id);
			if (_cachedBuildingMeshes.TryGetValue(id, out var spriteMesh)) return spriteMesh;

			spriteMesh = MeshUtils.GenerateMeshFromSprite(sprite);
			_cachedBuildingMeshes.Add(id, spriteMesh);

			return spriteMesh;
		}
	}
}