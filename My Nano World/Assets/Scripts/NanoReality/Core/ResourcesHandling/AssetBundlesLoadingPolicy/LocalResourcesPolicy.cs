using System.Collections.Generic;
using NanoLib.Core.Logging;
using NanoReality.Engine.BuildingSystem;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using UnityEngine;

namespace NanoReality.Core.Resources
{
    public sealed class LocalResourcesPolicy : ResourcesPolicy
    {
        [Inject] public GameResourcesLoadedSignal jGameResourcesLoadedSignal { get; set; }
        [Inject] public BundlesLoadingFinishedSignal jBundlesLoadingFinishedSignal { get; set; }
        
        private readonly Dictionary<int, Sprite> _productSprites = new Dictionary<int, Sprite>();
        private readonly Dictionary<int, Sprite> _eventSprites = new Dictionary<int, Sprite>();
        private IBuildingConfigsStorage _buildingConfigsStorage;

        private const string BuildingConfigsStoragePath = "Assets/ScriptableObjects/BuildingConfigs/BuildingConfigsStorage.asset";
        private const string ProductsIconsPath = "Assets/Art/Textures/Products";
        private const string EventRewardsIconsPath = "Assets/Art/Textures/TheEventRewards";
        
        public override GameObject GlobalMap => LoadAsset<GameObject>("Assets/Art/Prefabs/Map/Map.prefab");

        private IBuildingConfigsStorage BuildingConfigsStorage => _buildingConfigsStorage ??= LoadAsset<BuildingConfigsStorage>(BuildingConfigsStoragePath);

        [PostConstruct]
        public void PostConstruct()
        {
            jBundlesLoadingFinishedSignal.AddOnce(_ => { jGameResourcesLoadedSignal.Dispatch(); });
        }

        public override Sprite GetProductSprite(int productId)
        {
            if (_productSprites.Count == 0)
            {
                LoadProductsIcons();
            }

            if (_productSprites.TryGetValue(productId, out var productIcon))
            {
                return productIcon;
            }

            $"Sprite for product Id {productId} not found.".LogError(LoggingChannel.AssetBundles);
            return null;
        }

        public override Sprite GetEventSprite(int index)
        {
            if (_eventSprites.Count == 0)
            {
                LoadEventRewardIcons();
            }

            if (_eventSprites.TryGetValue(index, out var rewardIcon))
            {
                return rewardIcon;
            }

            $"Sprite for product index {index} not found.".LogError(LoggingChannel.AssetBundles);
            return null;
        }

        protected override BuildingConfig GetBuildingConfig(MapObjectId id)
        {
            var configPath = BuildingConfigsStorage.GetBuildingConfigPath(id.Id);
            if (string.IsNullOrEmpty(configPath))
            {
                $"Config path for id {id.Id} not found.".LogError(LoggingChannel.AssetBundles);
                return null;
            }

            var buildingConfig = LoadAsset<BuildingConfig>(configPath);
            if (buildingConfig == null)
            {
                $"Config for id {id.Id} not found in game resources. Path: {configPath}".LogError(LoggingChannel.AssetBundles);
            }

            return buildingConfig;
        }

        private void LoadProductsIcons()
        {
            _productSprites.Clear();

#if UNITY_EDITOR
            var assetGuids = UnityEditor.AssetDatabase.FindAssets("t:texture", new[] {ProductsIconsPath});
            foreach (var assetGuid in assetGuids)
            {
                var assetPath = UnityEditor.AssetDatabase.GUIDToAssetPath(assetGuid);
                var loadAllAssetsAtPath = UnityEditor.AssetDatabase.LoadAllAssetsAtPath(assetPath);
                foreach (var asset in loadAllAssetsAtPath)
                {
                    var spriteAsset = asset as Sprite;
                    if (spriteAsset != null)
                    {
                        var spriteId = Product.ParseProductName(spriteAsset.name);
                        _productSprites.Add(spriteId, spriteAsset);
                    }
                }
            }
#endif
        }

        private void LoadEventRewardIcons()
        {
            _eventSprites.Clear();

#if UNITY_EDITOR
            var assetGuids = UnityEditor.AssetDatabase.FindAssets("t:texture", new[] { EventRewardsIconsPath });
            foreach (var assetGuid in assetGuids)
            {
                var assetPath = UnityEditor.AssetDatabase.GUIDToAssetPath(assetGuid);
                var loadAllAssetsAtPath = UnityEditor.AssetDatabase.LoadAllAssetsAtPath(assetPath);
                foreach (var asset in loadAllAssetsAtPath)
                {
                    var spriteAsset = asset as Sprite;
                    if (spriteAsset != null)
                    {
                        var spriteId = Product.ParseProductName(spriteAsset.name);
                        _eventSprites.Add(spriteId, spriteAsset);
                    }
                }
            }
#endif
        }

        private static T LoadAsset<T>(string path) where T : Object
        {
            T result = null;
#if UNITY_EDITOR
            result = UnityEditor.AssetDatabase.LoadAssetAtPath<T>(path);
#endif
            return result;
        }
        
        public override void UnloadBundle(string bundleName)
        {
            //Ignored
        }
    }
}