using strange.extensions.signal.impl;

namespace NanoReality.Core.Resources
{
    /// <summary>
    /// Called when asset bundle loading completed.
    /// Parameter show loading result.
    /// </summary>
    public sealed class BundlesLoadingFinishedSignal : Signal<bool> { }
    
    /// <summary>
    /// Calls when bundles loading started, returns count of all bundles for load.
    /// </summary>
    public sealed class BundlesLoadingStartedSignal : Signal<int> { }
    
    /// <summary>
    /// Calls when some bundle loaded, returns it order index.
    /// </summary>
    public sealed class BundleLoadedSignal : Signal<int> { }

    /// <summary>
    /// Calls when all game resources was loaded from bundles or other game resources source.
    /// </summary>
    public sealed class GameResourcesLoadedSignal : Signal { }
    
    /// <summary>
    /// Calls when graphic quality was changed by user.
    /// Value corresponds to High Quality setting.
    /// </summary>
    public sealed class GraphicQualityChangedSignal : Signal<bool> { }
    
    public sealed class OnGraphicQualityPopupUpdateSignal : Signal { }
}