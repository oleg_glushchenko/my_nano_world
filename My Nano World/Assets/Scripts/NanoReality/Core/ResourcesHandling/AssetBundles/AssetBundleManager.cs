using System; 
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using NanoLib.Core.Logging;
using UnityEngine;
using UnityEngine.Networking;
using Object = UnityEngine.Object;

namespace NanoReality.Core.Resources
{
    public static class AssetBundleManager
    {
        private static readonly Dictionary<string, AssetBundle> _loadedAssetBundles = new Dictionary<string, AssetBundle>();

        public static event Action<string> OnAssetBundleLoaded;
        public static event Action<ulong, ulong> OnLoadingProgressUpdate;

        public static AssetBundle GetLoadedBundle(string bundleName)
        {
            if (!_loadedAssetBundles.TryGetValue(bundleName, out var assetBundle))
            {
                $"Bundle ({bundleName}) not exists in loaded bundles list.".LogError(LoggingChannel.AssetBundles);
            }

            return assetBundle;
        }

        public static T GetAssetFromBundle<T>(string assetName) where T : Object
        {
            var bundleName = AssetBundlesNames.GetBundleName(assetName);
            if (string.IsNullOrEmpty(bundleName))
            {
                $"Bundle name for asset {assetName} not found!".LogError(LoggingChannel.AssetBundles);
                return null;
            }

            if (_loadedAssetBundles.TryGetValue(bundleName, out var assetBundle))
            {
                return assetBundle.LoadAsset<T>(assetName);
            }

            $"Bundle ({bundleName}) not exists in loaded bundles list.".Log(LoggingChannel.AssetBundles);
            return null;
        }

        public static IEnumerator LoadLocalAssetBundle(string bundleName, Action<AssetBundle> onCompleteCallback)
        {
            if (_loadedAssetBundles.TryGetValue(bundleName, out var loadedBundle))
            {
                onCompleteCallback?.Invoke(loadedBundle);
                yield break;
            }

            var durationWatch = Stopwatch.StartNew();
            var totalPath = AssetBundlePath.GetLocalAssetBundlePath(bundleName);
            var request = AssetBundle.LoadFromFileAsync(totalPath);
            
            $"Loading asset bundle {bundleName} from StreamingAssets. TotalPath: {totalPath}".Log(LoggingChannel.AssetBundles);

#if UNITY_ANDROID
            while (!request.isDone)
                yield return null;
#else
            var bundleSize = GetLocalBundleSize(bundleName);
            while (!request.isDone)
            {
                OnLoadingProgressUpdate?.Invoke((ulong) (request.progress * bundleSize), bundleSize);
                yield return null;
            }
#endif

            var bundle = request.assetBundle;
            _loadedAssetBundles.Add(bundleName, bundle);

            durationWatch.Stop();
            $"Asset bundle {bundleName} loaded for {durationWatch.ElapsedMilliseconds}ms.".Log(LoggingChannel.AssetBundles);

            OnAssetBundleLoaded?.Invoke(bundleName);
            onCompleteCallback?.Invoke(bundle);
        }

        public static IEnumerator DownloadAssetBundle(string bundleName, Hash128 hash, Action<AssetBundle, UnityWebRequest.Result> onCompleteCallback = null)
        {
            if (_loadedAssetBundles.TryGetValue(bundleName, out var loadedBundle))
            {
                onCompleteCallback?.Invoke(loadedBundle, UnityWebRequest.Result.Success);
                yield break;
            }
            
            var totalPath = AssetBundlePath.GetRemoteAssetBundlePath(bundleName);

            var durationWatch = Stopwatch.StartNew();
            var webRequest = hash != default
                ? UnityWebRequestAssetBundle.GetAssetBundle(totalPath, hash)
                : UnityWebRequestAssetBundle.GetAssetBundle(totalPath);
            
            using (webRequest)
            {
                $"Remote downloading started: {bundleName} TotalPath: {totalPath}".Log(LoggingChannel.AssetBundles);

                webRequest.SendWebRequest();
                if (!webRequest.isDone)
                {
                    var bundleSize = 0ul;
                    var sizeRequestResult = UnityWebRequest.Result.InProgress;
                    yield return GetRemoteBundleSize(bundleName, (size, result) =>
                    {
                        bundleSize = size;
                        sizeRequestResult = result;
                    });
                    
                    if (sizeRequestResult != UnityWebRequest.Result.Success)
                    {
                        HandleResult(sizeRequestResult);
                        yield break;
                    }

                    while (!webRequest.isDone)
                    {
                        OnLoadingProgressUpdate?.Invoke(webRequest.downloadedBytes, bundleSize);
                        yield return null;
                    }
                }

                HandleResult(webRequest.result);
            }
            
            void HandleResult(UnityWebRequest.Result result)
            {
                AssetBundle bundle = null;
                switch (result)
                {
                    case UnityWebRequest.Result.Success:
                        bundle = ((DownloadHandlerAssetBundle) webRequest.downloadHandler).assetBundle;
                        _loadedAssetBundles.Add(bundleName, bundle);

                        OnAssetBundleLoaded?.Invoke(bundleName);
                        break;

                    default:
                        $"Error while loading bundle: {bundleName}. Error: {webRequest.result} : {webRequest.error}".LogError(LoggingChannel.AssetBundles);
                        break;
                }

                durationWatch.Stop();
                LogDownloadResult(bundleName, totalPath, durationWatch.ElapsedMilliseconds, hash);
                
                onCompleteCallback?.Invoke(bundle, result);
            }
        }

        private static ulong GetLocalBundleSize(string bundleName)
        {
            var fileInfo = new System.IO.FileInfo(AssetBundlePath.GetLocalAssetBundlePath(bundleName));

            return (ulong) fileInfo.Length;
        }

        public static IEnumerator GetRemoteBundleSize(string bundleName, Action<ulong, UnityWebRequest.Result> result)
        {
            var path = AssetBundlePath.GetRemoteAssetBundlePath(bundleName);
            var webRequest = UnityWebRequest.Head(path);

            yield return webRequest.SendWebRequest();
            var size = webRequest.GetResponseHeader("Content-Length");

            if(webRequest.result == UnityWebRequest.Result.Success)
                result?.Invoke(Convert.ToUInt64(size), webRequest.result);
            else
            {
                $"{webRequest.result} : {webRequest.error}".LogError(LoggingChannel.AssetBundles);
                result?.Invoke(0, webRequest.result);
            }
        }

        public static bool IsBundlesCached(AssetBundleManifest manifest)
        {
            var contentAssetBundlesNames = manifest.GetAllAssetBundles();

            var allBundlesCached = true;
            foreach (var bundleName in contentAssetBundlesNames)
            {
                var hash = manifest.GetAssetBundleHash(bundleName);
                var path = AssetBundlePath.GetRemoteAssetBundlePath(bundleName);

                var isVersionCached = Caching.IsVersionCached(path, hash);
                $"Is asset bundle by path {path} cached: {isVersionCached}".Log(LoggingChannel.AssetBundles);

                if (isVersionCached) continue;

                allBundlesCached = false;
                break;
            }

            return allBundlesCached;
        }
        
        public static void Unload(string bundleName, bool unloadContent = false)
        {
            if (!_loadedAssetBundles.ContainsKey(bundleName)) return;
            
            var bundle = _loadedAssetBundles[bundleName];
            _loadedAssetBundles.Remove(bundleName);
                
            bundle.Unload(unloadContent);
        }

        private static void UnloadAll()
        {
            foreach (var assetBundle in _loadedAssetBundles.Where(assetBundle => assetBundle.Value != null))
                assetBundle.Value.Unload(true);

            _loadedAssetBundles.Clear();
        }

        public static void Reset()
        {
            Unsubscribe();
            UnloadAll();
        }

        public static void Unsubscribe()
        {
            OnAssetBundleLoaded = delegate { };
            OnLoadingProgressUpdate = delegate { };
        }

        private static void LogDownloadResult(string bundleName, string URI, long time, Hash128 hash)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine($"Remote downloading completed: {bundleName}.");
            stringBuilder.AppendLine($"URI: {URI}.");
            stringBuilder.AppendLine($"Time: {time}ms.");
            stringBuilder.AppendLine($"Hash128: {(hash != default ? hash.ToString() : "-")}.");
            stringBuilder.Log(LoggingChannel.AssetBundles);
        }
    }
}