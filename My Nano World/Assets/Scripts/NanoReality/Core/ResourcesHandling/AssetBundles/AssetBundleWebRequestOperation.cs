using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

namespace NanoReality.Core.Resources
{
	public class AssetBundleWebRequestOperation<T>
	{
		private readonly string _bundleName;
		private readonly Hash128 _hash;
		private readonly Action<T> _onComplete;
		private readonly Action<string> _onErrorReceived;
		private T _object;
		private UnityWebRequest.Result _result = UnityWebRequest.Result.InProgress;

		private const int MaxAttemptsCount = 5;
		
		public AssetBundleWebRequestOperation(string bundleName, Hash128 hash, Action<T> onComplete, Action<string> onErrorReceived)
		{
			_bundleName = bundleName;
			_hash = hash;
			_onComplete = onComplete;
			_onErrorReceived = onErrorReceived;
		}
		
		public IEnumerator Download(Func<string, Hash128, Action<T, UnityWebRequest.Result>, IEnumerator> downloadEnumerator)
		{
			var attempts = default(int);
			while (_result != UnityWebRequest.Result.Success)
			{
				attempts++;

				yield return downloadEnumerator.Invoke(_bundleName, _hash, OnDownloadResultReceived);
				
				switch (_result)
				{
					case UnityWebRequest.Result.Success:
						_onComplete?.Invoke(_object);
						yield break;
				
					case UnityWebRequest.Result.ProtocolError:
						if (AssetBundlePath.FolderName != AssetBundlesFolderName.Reserve)
						{
							AssetBundlePath.FolderName = AssetBundlesFolderName.Reserve;
							attempts = default;
							break;
						}
						else
						{
							_onErrorReceived?.Invoke($"{_result}: bundle with name [{_bundleName}] not found on server.");
							yield break;
						}

					default:
						if (attempts >= MaxAttemptsCount)
						{
							_onErrorReceived?.Invoke($"{_result}: Can't download bundle [{_bundleName}] with [{attempts}] attempts.");
							yield break;
						}
					
						break;
				}
			}
		}

		private void OnDownloadResultReceived(T @object, UnityWebRequest.Result result)
		{
			_object = @object;
			_result = result;
		}
	}
}