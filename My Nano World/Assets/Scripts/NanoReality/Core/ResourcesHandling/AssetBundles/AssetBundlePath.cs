using System;
using System.IO;
using UnityEngine;

namespace NanoReality.Core.Resources
{
	public static class AssetBundlePath
	{
		public static string AssetBundlesFolderURI;

		public static AssetBundlesFolderName FolderName =
#if PRODUCTION
			AssetBundlesFolderName.Main;
#else
			AssetBundlesFolderName.Test;
#endif

		public static string GetPlatformFolder()
		{
			switch (Application.platform)
			{
				case RuntimePlatform.WindowsPlayer:
				case RuntimePlatform.WindowsEditor:
					return "Windows";
                
				case RuntimePlatform.IPhonePlayer:
					return "iOS";
                
				case RuntimePlatform.Android:
					return "Android";
                
				case RuntimePlatform.OSXEditor:
				case RuntimePlatform.OSXPlayer:
					return "StandaloneOSX";
                
				case RuntimePlatform.LinuxPlayer:
					return "StandaloneLinux64";

				default:
					throw new NotSupportedException($"Platform {Application.platform} is not supported");
			}
		}

		public static string GetLocalAssetBundlePath(string bundleName)
		{
			var totalPath = Path.Combine(Application.streamingAssetsPath, "AssetBundles", GetPlatformFolder(), bundleName);

			return totalPath;
		}

		public static string GetRemoteAssetBundlePath(string bundleName)
		{
			var platformPath = Path.Combine(AssetBundlesFolderURI, FolderName.ToString(), GetPlatformFolder());
			var totalPath = Path.Combine(platformPath, bundleName);

			return totalPath;
		}
	}
}