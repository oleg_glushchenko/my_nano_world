using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using NanoLib.Core.Logging;
using UnityEditor;
using UnityEngine;

namespace NanoReality.Core.Resources
{
	[CreateAssetMenu(fileName = nameof(ScriptableObjectsPathHandler), menuName = "ScriptableObjects/Create ScriptableObjects Path Handler", order = 1)]
	public class ScriptableObjectsPathHandler : ScriptableObject
	{
		[SerializeField] private List<ScriptableObjectPath> _paths;

		public string GetPath(string scriptableObjectName)
		{
			var result = _paths.Find(path => path.name == scriptableObjectName);
			if (result == null)
				throw new PathNotFoundException($"Path for ScriptableObject with name {scriptableObjectName} not found!");

			return result.path;
		}

#if UNITY_EDITOR
		[ContextMenu("Update")]
		public void UpdatePaths()
		{
			const string NamePattern = @"(?<=\/)\w+(?=$)";
			const string ResourcesFolder = "Assets/Resources";
			
			_paths.Clear();
			
			var guids = AssetDatabase.FindAssets($"t:{nameof(ScriptableObject)}", new[] {ResourcesFolder});
			foreach (var guid in guids)
			{
				var totalPath = AssetDatabase.GUIDToAssetPath(guid).Replace(".asset", string.Empty);
				var pathInResources = totalPath.Replace($"{ResourcesFolder}/", string.Empty);
				var scriptableObjectName = Regex.Match(totalPath, NamePattern).ToString();

				_paths.Add(new ScriptableObjectPath{name = scriptableObjectName, path = pathInResources});
			}
			
			"ScriptableObjects paths are successfully updated!".Log(LoggingChannel.Editor);
		}
#endif

		[Serializable]
		public class ScriptableObjectPath
		{
			public string name;
			public string path;
		}
	}

	public class PathNotFoundException : Exception
	{
		public PathNotFoundException(string message) : base(message)
		{
		}
	}
}