﻿using strange.extensions.mediation.impl;
using UnityEngine;

namespace NanoReality.Core.Resources
{
    public class GlobalMapPlaceholder : View
    {
        [Inject] public IResourcesManager jResourcesManager { get; set; }
        [Inject] public GameResourcesLoadedSignal jGameResourcesLoadedSignal { get; set; }

        protected override void Start()
        {
            base.Start();

            jGameResourcesLoadedSignal.AddListener(LoadGlobalMap);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            jGameResourcesLoadedSignal.RemoveListener(LoadGlobalMap);
        }

        private void LoadGlobalMap()
        {
            var map = Instantiate(jResourcesManager.GetGlobalMap(), transform, false);
            map.transform.localPosition = Vector3.zero;
            map.transform.localRotation = Quaternion.identity;
            map.transform.localScale = Vector3.one;
        }
    }
}