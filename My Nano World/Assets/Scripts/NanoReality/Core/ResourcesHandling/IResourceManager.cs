using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using UnityEngine;

namespace NanoReality.Core.Resources
{
	public interface IResourcesManager
	{
		T GetBuildingPrefab<T>(MapObjectId id) where T : MapObjectView;
		Sprite GetBuildingSprite(MapObjectId id);
		Sprite GetProductSprite(int productId);
		Sprite GetEventSprite(int index);
		Mesh GetBuildingSpriteMesh(MapObjectId id);
		GameObject GetGlobalMap();
		void UnloadBundle(string bundleName);
	}
}