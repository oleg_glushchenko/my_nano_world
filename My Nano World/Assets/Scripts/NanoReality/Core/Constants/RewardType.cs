namespace NanoReality.GameLogic.Constants
{
    public enum RewardType
    {
        Soft = 1,
        Hard = 2,
        Products = 3,
        Gold = 10,
        Lanterns = 11,
        NanoPass = 13
    }
}