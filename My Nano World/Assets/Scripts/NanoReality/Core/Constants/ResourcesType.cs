using System;

namespace NanoReality.GameLogic.Constants
{
	[Flags]
	public enum ResourcesType
	{
		None = 0,
		Soft = 1,
		Hard = 2,
		Materials = 4,
		Gold = 8
	}
}