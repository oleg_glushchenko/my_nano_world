using System;
using NanoReality.GameLogic.Constants;
using Newtonsoft.Json;

namespace NanoReality.GameLogic.Data
{
    [Serializable]
    public class RewardData
    {
        [JsonProperty("type")]
        public RewardType Type { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("amount")]
        public int Amount { get; set; }
    }
}
