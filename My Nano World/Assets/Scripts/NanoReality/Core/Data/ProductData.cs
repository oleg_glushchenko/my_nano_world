using Newtonsoft.Json;

namespace NanoReality.GameLogic.OrderDesk
{
    public struct ProductData
    {
        [JsonProperty("product_id")]
        public int ProductId;
        
        [JsonProperty("product_count")] 
        public int Count;

        public static bool operator ==(ProductData obj1, ProductData obj2)
        {
            return obj1.Equals(obj2);
        }

        public static bool operator !=(ProductData obj1, ProductData obj2)
        {
            return !obj1.Equals(obj2);
        }

        public ProductData(int productId, int count)
        {
            ProductId = productId;
            Count = count;
        }
    }
}