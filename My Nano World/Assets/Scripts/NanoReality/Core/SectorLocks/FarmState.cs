﻿using UniRx;

namespace NanoReality.Core.SectorLocks
{
    public class FarmState
    {
        public ReactiveProperty<bool> FarmLocked = new ReactiveProperty<bool>(true);
        public ReactiveProperty<int> UnlockLevel = new ReactiveProperty<int>();
    }
}