﻿using NanoReality.GameLogic.PlayerProfile.Models.Api;
using strange.extensions.command.impl;

namespace NanoReality.Core.SectorLocks
{
    public class FarmSectorUnlockCommand : Command
    {
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public FarmState FarmState { get; set; }

        public override void Execute()
        {
            if (FarmState.FarmLocked.Value && jPlayerProfile.CurrentLevelData.CurrentLevel >= FarmState.UnlockLevel.Value - 1)
            {
                FarmState.FarmLocked.Value = false;
            }
        }
    }
}