using UnityEngine;

namespace NanoReality.Core {
    public interface IInfiniteItem
    {
        int Index { get; set; }
        void SetActive(bool value);
        RectTransform Rect { get; }
        GameObject GameObject { get; }
    }
}