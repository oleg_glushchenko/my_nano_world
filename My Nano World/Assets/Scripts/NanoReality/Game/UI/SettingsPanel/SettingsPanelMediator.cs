﻿using Assets.NanoLib.UI.Core;
using Assets.Scripts.Engine.Analytics;
using Assets.Scripts.Engine.UI.Extensions.SettingsPanel;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Assets.Scripts.GameLogic.Utilites;
using NanoReality.Engine.UI.Extensions.UISignals;
using NanoReality.GameLogic.Configs;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.SocialCommunicator.api;
using strange.extensions.signal.impl;
using System.Linq;
using NanoReality.Game.Data;
using NanoReality.GameLogic.SocialCommunicator;
using NanoReality.GameLogic.Utilites;
using UnityEngine;
using UnityEngine.Networking;

namespace NanoReality.Game.UI.SettingsPanel
{
    public class SignalGameAuthorize : Signal { }
    public class SignalSocialAuthorize : Signal { }
    public class SignalNeedToChangeLanguage : Signal<string> { }

    public class SettingsPanelMediator : UIMediator<SettingsPanelView>
    {
        [Inject] public SignalGameAuthorize jSignalGameAuthorize { get; set; }
        [Inject] public SignalSocialAuthorize jSignalSocialAuthorize { get; set; }
        [Inject] public SignalOnNeedToRenameCity SignalOnNeedToRenameCity { get; set; }
        [Inject] public SignalNeedToChangeLanguage jSignalNeedToChangeLanguage { get; set; }
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public SignalOnCityNameChanged jSignalOnCityNameChanged { get; set; }
        [Inject] public OnCityNameChangedOnServerSignal OnCityNameChangedOnServerSignal { get; set; }
        [Inject] public SignalOnSettingsLanguageChanged jSignalOnSettingsLanguageChanged { get; set; }
        [Inject(SocialNetworkCommunicatorTypes.Primary)] public ISocialNetworkCommunicator SocialNetworkCommunicator { get; set; }
        [Inject] public SettingsPanelContext JSettingsPanelContext { get; set; }
        [Inject] public IBuildSettings jBuildSettings { get; set; }
        [Inject] public LinksConfigsHolder jLinksHolder { get; set; }
        [Inject] public FBLoggedInSignal jFBLoggedInSignal { get; set; }
        [Inject] public IFacebookController jFacebookController { get; set; }

        protected override void OnShown()
        {
            InitLanguages();

            jSignalGameAuthorize.AddListener(GameAuth);
            jSignalSocialAuthorize.AddListener(SocialAuth);
            SignalOnNeedToRenameCity.AddListener(OnCityNameRenamed);
            jSignalNeedToChangeLanguage.AddListener(OnLanguageChanged);
            jFBLoggedInSignal.AddListener(OnFBLogin);

            View.SetGameButtonState(SocialNetworkCommunicator.IsUserAuthorized);

            View.SupportButtonClicked += SupportButtonClicked;
            View.FbLoginClicked += OnFbButtonClick;

            base.OnShown();

            var languageSpriteData = View.LanguageSprites.FirstOrDefault(item => item.Language == LocalizationMLS.Instance.Language);
            View.OnLanguageChange(languageSpriteData);
        }

        protected override void OnHidden()
        {
            jSignalGameAuthorize.RemoveListener(GameAuth);
            jSignalSocialAuthorize.RemoveListener(SocialAuth);
            SignalOnNeedToRenameCity.RemoveListener(OnCityNameRenamed);
            jSignalNeedToChangeLanguage.RemoveListener(OnLanguageChanged);
            jFBLoggedInSignal.AddListener(OnFBLogin);

            View.SupportButtonClicked -= SupportButtonClicked;
            View.FbLoginClicked -= OnFbButtonClick;
        }

        public void OnFBLogin(bool loggedIn)
        {
            View.OnFbLogin(loggedIn);
        }

        private void OnFbButtonClick()
        {
            if (jFacebookController.IsLoggedIn == false)
            {
                jFacebookController.Login();
            }
            else
            {
                var settings = new ConfirmPopupSettings
                {
                    Title = LocalizationUtils.Localize(LocalizationKeyConstants.BUYPOPUP_LABEL_CONFIRM),
                    Message = LocalizationUtils.Localize(LocalizationKeyConstants.FB_LOGOUT_CONFIRM_MESSAGE)
                };
                jPopupManager.Show(settings, result =>
                {
                    if (result == PopupResult.Ok)
                    {
                        jFacebookController.Logout();
                    }
                });
            }
        }

        private void SocialAuth()
        {
        }

        private void GameAuth()
        {
            SocialAuth(SocialNetworkCommunicator);
        }

        public void SetButtonAuthState(bool authorized)
        {
            View.SetSocialButtonState(authorized);
        }

        private void OnCityNameRenamed(string cityName)
        {
            jServerCommunicator.RenameCity(cityName, JSettingsPanelContext.CityId, result =>
            {
                if (result)
                {
                    JSettingsPanelContext.SetCityName(cityName);
                    View.SetCityName(cityName);
                    jSignalOnCityNameChanged.Dispatch(cityName);
                }

                OnCityNameChangedOnServerSignal.Dispatch();
            });
        }

        private void OnLanguageChanged(string languageName)
        {
            var languageSpriteData = View.LanguageSprites.FirstOrDefault(item => item.Language == languageName);
            LocalizationMLS.Instance.ChangeLanguage(languageName);
            InitLanguages();
            View.OnLanguageChange(languageSpriteData);
            jSignalOnSettingsLanguageChanged.Dispatch(languageName);
        }

        private void InitLanguages()
        {
            View.LanguagesContainer.ClearCurrentItems();

            var languages = jBuildSettings.AvailableLanguages;
            var isShowLanguages = languages.Count > 1;

            View.LanguageButtonActive = isShowLanguages;

            if (isShowLanguages)
            {
                foreach (var languageId in jBuildSettings.AvailableLanguages)
                {
                    var languageSpriteData = View.LanguageSprites.FirstOrDefault(item => item.Language == languageId);
                    if (languageSpriteData != null && JSettingsPanelContext.ContainsLanguage(languageId))
                    {
                        var languageItem = View.LanguagesContainer.AddItem() as LanguageItemView;
                        if (languageItem != null)
                        {
                            languageItem.InitItem(languageId, languageSpriteData.Sprite, languageSpriteData.SubSprite);
                            languageItem.IsSelected = languageId == LocalizationMLS.Instance.Language;
                            languageItem.SelectSignal.AddListener(OnLanguageChanged);
                        }
                    }
                }
            }
        }
        
        private void SocialAuth(ISocialNetworkCommunicator communicator)
        {
            if (communicator.IsUserAuthorized)
                communicator.Logout((logoutSuccess, socialNetworkCommunicator) => { SetButtonAuthState(!logoutSuccess); });
            else
                communicator.Authorize(result => { SetButtonAuthState(result.IsAuthorized); });
        }
        
        private void SupportButtonClicked()
        {
            var email = jLinksHolder.ContactEmail;
            var subject = EscapeURL("Subject");
            var body = EscapeURL($"Describe your problem here. Your user id: {JSettingsPanelContext.jPlayerProfile.Id}");
            var url = "mailto:" + email + "?subject=" + subject + "&body=" + body;

            Application.OpenURL(url);
        }

        private static string EscapeURL (string url)
        {
            return UnityWebRequest.EscapeURL(url).Replace("+","%20");
        }
    }
}