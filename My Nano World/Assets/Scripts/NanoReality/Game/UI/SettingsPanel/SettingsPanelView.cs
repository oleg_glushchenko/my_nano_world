﻿using System;
using Assets.NanoLib.UI.Core.Components;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.UI.Extensions.SettingsPanel;
using Assets.Scripts.MultiLanguageSystem.Logic;
using System.Collections.Generic;
using NanoReality.Core.Resources;
using NanoReality.Core.UI;
using NanoReality.GameLogic.Settings.GameSettings.Models.Api;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using NanoReality.GameLogic.Configs;
using NanoReality.GameLogic.SocialCommunicator.api;
using NanoReality.GameLogic.Utilites;
 

namespace NanoReality.Game.UI.SettingsPanel
{
    public enum SettingsToggleType
    {
        Music,
        Effects,
        Notifications,
        GraphicQuality
    }

    public class SettingsPanelView : UIPanelView
    {
        #region Inspector Fields

        [SerializeField] private ContactUsView _сontactUsView;
        [SerializeField] private TextMeshProUGUI _cityNameText;
        [SerializeField] private TextMeshProUGUI _fbStatusText;

        [SerializeField] private Button _socialButton;
        [SerializeField] private Button _creditsButton;
        [SerializeField] private Button _renameCityButton;
        [SerializeField] private TextButton _languageButton;
        [SerializeField] private Button _sendUserReportButton;
        [SerializeField] private Button _gameCenterButton;
        [SerializeField] private Button _rateUsButton;
        [SerializeField] private Button _privacyPolicyButton;
        [SerializeField] private Button _termsOfUseButton;
        
        [SerializeField] private SettingsToggleChange _musicToggle;
        [SerializeField] private SettingsToggleChange _effectsToggle;
        [SerializeField] private SettingsToggleChange _pushToggle;
        [SerializeField] private Toggle _graphicToggle;
        [SerializeField] private GameObject _graphicToggleOnImage;
        [SerializeField] private GameObject _graphicQualityGameObject;
        [SerializeField] private GameObject _graphicToggleOffImage;
        [SerializeField] private TextMeshProUGUI _buildDataText;
        [SerializeField] private Image _languageImage;
        [SerializeField] private LoadingIconView _fbLoadingImage;
        [SerializeField] private ItemContainer _langagesContainer;
        [SerializeField] private LanguageItemView _langageItemPrefab;
        [SerializeField] private Transform _languagePanelObject;
        [SerializeField] private List<LanguageSpriteData> _langaugeSprites = new List<LanguageSpriteData>();
        
        public ItemContainer LanguagesContainer => _langagesContainer;
        public List<LanguageSpriteData> LanguageSprites => _langaugeSprites;

        public event Action SupportButtonClicked;

        public event Action FbLoginClicked;

        #endregion

        #region Inject

        [Inject] public ShowWindowSignal jShowWindowSignal { get; set; }
        [Inject] public GraphicQualityChangedSignal jGraphicQualityChangedSignal { get; set; }
        [Inject] public SignalGameAuthorize jSignalGameAuthorize { get; set; }
        [Inject] public SignalSocialAuthorize jSignalSocialAuthorize { get; set; }
        [Inject] public SettingsPanelContext jSettingsPanelContext { get; set; }
        [Inject] public IGameSettings jGameSettings { get; set; }
        [Inject] public LinksConfigsHolder jLinksHolder { get; set; }
        [Inject] public OnGraphicQualityPopupUpdateSignal jOnGraphicQualityPopupCancelSignal { get; set; }
        [Inject] public IFacebookController jFacebookController { get; set; }
        #endregion

        public bool LanguageButtonActive
        {
            set => _languageButton.gameObject.SetActive(value);
        }

        public override void Initialize(object parameters)
        {
            base.Initialize(parameters);

            _langagesContainer.InstaniteContainer(_langageItemPrefab, 0);

            _musicToggle.InitToggle(SettingsToggleType.Music, jGameSettings);
            _effectsToggle.InitToggle(SettingsToggleType.Effects, jGameSettings);
            _pushToggle.InitToggle(SettingsToggleType.Notifications, jGameSettings);

            jOnGraphicQualityPopupCancelSignal.AddListener(InitializeGraphicToggle);
            _graphicToggle.onValueChanged.AddListener(OnGraphicSettingsChanged);
            _renameCityButton.onClick.AddListener(OnChangeCityNameButtonClick);
            _sendUserReportButton.onClick.AddListener(OnSendUserReportButtonClick);
            _languageButton.onClick.AddListener(OnLanguageButtonClick);
            _gameCenterButton.onClick.AddListener(OnSocialGameButtonClick);
            _rateUsButton.onClick.AddListener(OnRateUsButtonClick);
            _privacyPolicyButton.onClick.AddListener(OnPrivacyPolicyButtonClick);
            _termsOfUseButton.onClick.AddListener(OnTermsOfUseClick);
            //_socialButton.onClick.AddListener(OnConnectFbClick);
            InitializeGraphicToggle();
            UpdatePanel();


            if (SystemInfo.systemMemorySize < 3500)
            {
                _graphicQualityGameObject.SetActive(false);
            }
        }

        public override void Hide()
        {
            base.Hide();
            
            jOnGraphicQualityPopupCancelSignal.RemoveListener(InitializeGraphicToggle);
            _graphicToggle.onValueChanged.RemoveListener(OnGraphicSettingsChanged);
            _renameCityButton.onClick.RemoveListener(OnChangeCityNameButtonClick);
            _sendUserReportButton.onClick.RemoveListener(OnSendUserReportButtonClick);
            _languageButton.onClick.RemoveListener(OnLanguageButtonClick);
            _gameCenterButton.onClick.RemoveListener(OnSocialGameButtonClick);
            _rateUsButton.onClick.RemoveListener(OnRateUsButtonClick);
            _privacyPolicyButton.onClick.RemoveListener(OnPrivacyPolicyButtonClick);
            _termsOfUseButton.onClick.RemoveListener(OnTermsOfUseClick);
           // _socialButton.onClick.RemoveListener(OnConnectFbClick);
        }

        private void InitializeGraphicToggle()
        {
            var isHighQuality = jGameSettings.IsHighQualityGraphic;
            
            _graphicToggle.SetIsOnWithoutNotify(isHighQuality);
            _graphicToggleOnImage.SetActive(isHighQuality);
            _graphicToggleOffImage.SetActive(!isHighQuality);
        }

        public void SetCityName(string cityName)
        {
            _cityNameText.text = cityName;
        }

        public void OnChangeCityNameButtonClick() => jShowWindowSignal.Dispatch(typeof(RenameCityPanelView), null);

        private void OnSendUserReportButtonClick()
        {
            SupportButtonClicked?.Invoke();
        }

        private void OnLanguageButtonClick()
        {
            _languagePanelObject.gameObject.SetActive(!_languagePanelObject.gameObject.activeInHierarchy);
        }
        
        private void OnTermsOfUseClick()
        {
            Application.OpenURL(jLinksHolder.TermsOfUseUrl);
        }
        
        private void OnRateUsButtonClick()
        {
#if UNITY_IOS
            UnityEngine.iOS.Device.RequestStoreReview();
#else
            Application.OpenURL(jLinksHolder.GoogleAppUrl);
#endif
        }

        private void OnPrivacyPolicyButtonClick()
        {
            Application.OpenURL(jLinksHolder.PrivacyPolicyUrl);
        }

        public void OnConnectFbClick()
        {
            FbLoginClicked?.Invoke();
        }

        public void OnFbLogin(bool loggedIn)
        {
            _fbStatusText.text = loggedIn ? LocalizationUtils.Localize("SETTINGS_SOCIAL_BTN_LOGOUT") : LocalizationUtils.Localize("SETTINGS_SOCIAL_BTN_LOGIN");
            _fbStatusText.isRightToLeftText = false;
        }

        public void OnSocialNetworkButtonClick()
        {
            _socialButton.interactable = false;
            _fbLoadingImage.Show();
            jSignalSocialAuthorize.Dispatch();
        }

        public void OnSocialGameButtonClick()
        {
            jSignalGameAuthorize.Dispatch();
            Hide();
        }

        public void OnFriendsButtonClick()
        {
            Hide();
        }

        private void UpdatePanel()
        {
            _buildDataText.SetLocalizedText(jSettingsPanelContext.BuildText);
            _cityNameText.text = jSettingsPanelContext.CityName;
            _fbStatusText.text = jFacebookController.IsLoggedIn ? LocalizationUtils.Localize("SETTINGS_SOCIAL_BTN_LOGOUT") : LocalizationUtils.Localize("SETTINGS_SOCIAL_BTN_LOGIN");
            _fbStatusText.isRightToLeftText = false;
        }

        public void OnLanguageChange(LanguageSpriteData languageData)
        {
            UpdatePanel();
            _languagePanelObject.gameObject.SetActive(false);
            _languageImage.sprite = languageData.Sprite;
        }
        
        private void OnGraphicSettingsChanged(bool value)
        {
            jGraphicQualityChangedSignal.Dispatch(!jGameSettings.IsHighQualityGraphic);
        }

        public void SetSocialButtonState(bool isLogged)
        {
        }
        
        public void SetGameButtonState(bool isLogged)
        {
        }
    }
}