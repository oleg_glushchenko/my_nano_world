using Assets.NanoLib.Utilities;
using Assets.Scripts.GameLogic.Utilites;
using NanoReality.Game.Data;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using UnityEngine;

namespace NanoReality.Game.UI.SettingsPanel
{
    public class SettingsPanelContext
    {
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IBuildSettings jBuildSettings { get; set; }

        public string CityName => jPlayerProfile.UserCity.CityName;
        public Id<IUserCity> CityId => jPlayerProfile.UserCity.Id;
        public string PlayerProfileId => jPlayerProfile.Id.Value.ToString();

        public string BuildText => string.Format(LocalizationMLS.Instance.GetText(LocalizationKeyConstants.SETTINGS_GAME_VERSION), Application.version, PlayerProfileId);

        public bool ContainsLanguage(string language)
        {
            return jBuildSettings.AvailableLanguages.Contains(language);
        }

        public void SetCityName(string name)
        {
            jPlayerProfile.UserCity.CityName = name;
        }
    }
}