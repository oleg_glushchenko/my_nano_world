﻿using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using strange.extensions.mediation.impl;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.UI.SettingsPanel
{
    public class ContactUsView : View
    {
        [SerializeField] private TMP_InputField _mailBody;
        [SerializeField] private Button _sendButton;
        [SerializeField] private Button _closeButton;

        public Button CloseButton => _closeButton;

        private const string email = "support@nanoreality.com";
       
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }

        private void OnEnable()
        {
            _sendButton.onClick.AddListener(SendEmail);
            _mailBody.onValueChanged.AddListener(OnInputValueChanged);
        }

        private void OnDisable()
        {
            _sendButton.onClick.RemoveListener(SendEmail);
            _mailBody.onValueChanged.RemoveListener(OnInputValueChanged);
        }
        
        private void OnInputValueChanged(string newValue)
        {
            _sendButton.interactable = !string.IsNullOrEmpty(newValue);
        }

        private void SendEmail()
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com", 587);
                SmtpServer.EnableSsl = true;
                SmtpServer.UseDefaultCredentials = false;
 
                mail.From = new MailAddress("nanorealityuser@gmail.com");
                mail.To.Add(email);
 
                mail.Subject = $"Contact us {jPlayerProfile.Id}";
                mail.Body = $"{_mailBody.text} \n User ID: {jPlayerProfile.Id}";
 
                //========================Attachments==============================
                // Attachment data = new Attachment(FileName, System.Net.Mime.MediaTypeNames.Application.Octet);
                // // Add time stamp information for the file.
                // System.Net.Mime.ContentDisposition disposition = data.ContentDisposition;
                // disposition.CreationDate = System.IO.File.GetCreationTime(FileName);
                // disposition.ModificationDate = System.IO.File.GetLastWriteTime(FileName);
                // disposition.ReadDate = System.IO.File.GetLastAccessTime(FileName);
                //
                // mail.Attachments.Add(data);
                //========================Attachments==============================
 
                SmtpServer.Credentials = (ICredentialsByHost) new NetworkCredential(mail.From.Address, "nano2021");
 
                SmtpServer.EnableSsl = true;
                ServicePointManager.ServerCertificateValidationCallback =
                    delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                    { return true; };
 
 
                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.ToString());
            }
        }
        
    }
}
