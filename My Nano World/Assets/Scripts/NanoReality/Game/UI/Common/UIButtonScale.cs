using DG.Tweening;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

namespace NanoReality.Game.UI
{
    /// <summary>
    /// This script do scale animated effect when button clicked.
    /// </summary>
    // TODO: need add possibility to override tween scale parameters.
    [RequireComponent(typeof(Button))]
    public sealed class UIButtonScale : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField] private Vector3 _punchScale = new Vector3(0.05f, 0.05f, 0.05f);
        [SerializeField] private readonly float _duration = 0.25f;

        private readonly int _vibrato = 1;
        private readonly float _elacity = 1.0f;

        private Button _button;
        private RectTransform _buttonRectTransform;
        private Tween _scaleTween;
        private Vector3 _defaultScale;

        private void Awake()
        {
            _button = GetComponent<Button>();
            _buttonRectTransform = transform as RectTransform;
            _defaultScale = _buttonRectTransform.localScale;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (_button.interactable)
            {
                OnButtonPressed();
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (_button.interactable)
            {
                OnButtonReleased();
            }
        }

        private void OnButtonPressed()
        {
            _buttonRectTransform.DOPause();
            _scaleTween = _buttonRectTransform.DOScale(_defaultScale + _punchScale, _duration);
        }

        private void OnButtonReleased()
        {
            _scaleTween = _buttonRectTransform.DOScale(_defaultScale, _duration);
            _buttonRectTransform.DOPlay();
        }
    }
}