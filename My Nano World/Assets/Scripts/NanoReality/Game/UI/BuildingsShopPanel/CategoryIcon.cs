using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

namespace NanoReality.Game.UI.BuildingsShopPanel
{
    [CreateAssetMenu(fileName = "CategoryIcon", menuName = "NanoReality/UI/CategoryIcon", order = 11)]
    public class CategoryIcon : SerializedScriptableObject
    {
        [SerializeField] private List<CategoryIconData> _categorySprites;

        public Sprite GetCategorySprite(BuildingsCategories category)
        {
            return _categorySprites.FirstOrDefault(c => c.Category == category).Spite;
        }
        
        [Serializable]
        private struct CategoryIconData
        {
            [SerializeField] private BuildingsCategories _category;
            [SerializeField] private SpritePreview _sprite;

            public Sprite Spite => _sprite.Sprite;

            public BuildingsCategories Category => _category;
        }
    }

    [Serializable]
    public struct SpritePreview
    {
        [PreviewField]
        [SerializeField]
        [FormerlySerializedAs("sprite")]
        private Sprite _sprite;

        public Sprite Sprite => _sprite;
    }
}