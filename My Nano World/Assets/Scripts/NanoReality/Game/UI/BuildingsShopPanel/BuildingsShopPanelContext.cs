using System;
using NanoLib.Services.InputService;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.api;
using NanoLib.Core.Services.Sound;
using NanoReality.Engine.BuildingSystem.CityCamera;
using NanoReality.Engine.UI;
using NanoReality.Engine.UI.Extensions.BuildingsShopPanel;
using NanoReality.Game.Services.BuildingsUnlockService;
using NanoReality.Game.Tutorial;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Services;

namespace NanoReality.Game.UI.BuildingsShopPanel
{
    public class BuildingsShopPanelContext
    {
        [Inject] public IHardTutorial jHardTutorial { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public IProductsData jProductsData { get; set; }
        [Inject] public IConstructionController ConstructionController { get; set; }
        [Inject] public IMapObjectsData jMapObjectsData { get; set; }
        [Inject] public IGameCamera jGameCamera { get; set; }
        [Inject] public IGameCameraModel jGameCameraModel { get; set; }
        [Inject] public IPopupManager jPopupManager { get; set; }
        [Inject] public IBuildingService jBuildingService { get; set; }
        [Inject] public IIconProvider jIconProvider { get; set; }
        [Inject] public IInputController jInputController { get; set; }
        [Inject] public ISoundManager jSoundManager { get; set; }
        [Inject] public IBuildingsUnlockService jBuildingsUnlockService { get; set; }

        public SectorDependencies CurrentSectorDependency;
        public BusinessSectorsTypes CurrentChosenBusinesSector = BusinessSectorsTypes.Town;
        public IPlayerResources GetPlayerResources => jPlayerProfile.PlayerResources;
        public Vector3F CurrentFingerPosition => jInputController.CurrentFingerPosition.ToVector3F();

        public Product GetProduct(Id<Product> productId) => jProductsData.GetProduct(productId);
        public void PlaySfx() => jSoundManager.Play(SfxSoundTypes.ClickButton, SoundChannel.SoundFX);

        public void ShowPopup(IPopupSettings settings, Action<PopupResult> callback = null)
        {
            jPopupManager.Show(settings, callback);
        }

        public void CreateMapObject(Id_IMapObjectType id, BusinessSectorsTypes businessSector, Vector3F position = default, BuildingSubCategories buildingSubCategories = BuildingSubCategories.Common)
        {
            ConstructionController.CreateMapObject(id, businessSector, position, buildingSubCategories);
        }
    }
}