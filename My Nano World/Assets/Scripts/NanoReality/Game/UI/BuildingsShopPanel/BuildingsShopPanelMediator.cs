﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.UI.Core;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.Analytics;
using Assets.Scripts.Engine.Analytics.Model;
using Assets.Scripts.Engine.UI.Views.Hud;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using NanoReality.Core.SectorLocks;
using NanoReality.Engine.BuildingSystem.CityCamera;
using NanoReality.Engine.UI.Extensions.BuildingsShopPanel;
using NanoReality.Engine.UI.Views.BuildingsShopPanel;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Services;
using NanoReality.GameLogic.Utilites;
using strange.extensions.injector.api;
using UnityEngine.UI.Extensions;

namespace NanoReality.Game.UI.BuildingsShopPanel
{
    public class BuildingsShopPanelMediator : UIMediator<BuildingsShopPanelView>
    {
        private BuildingsShopPanelController _controller;
        private List<BusinessSectorsTypes> _availableSectorsByBuildingShop = new List<BusinessSectorsTypes>();
        
        [Inject] public BuildingsShopPanelContext Context { get; set; }
        [Inject] public OnHudBottomPanelIsVisibleSignal jOnHudBottomPanelIsVisibleSignal { get; set; }
        [Inject] public SignalOnMapObjectBuildFinished jOnMapBuildFinishedSignal { get; set; }
        [Inject] public BuildingConstructionStartedSignal jBuildingConstructionStartedSignal { get; set; }
        [Inject] public SignalOnUIAction jSignalOnUIAction { get; set; }
        [Inject] public BuildingShopPanelModel jBuildingShopPanelModel { get; set; }
        [Inject] public IHintTutorial jHintTutorial { get; set; }
        [Inject] public IInjectionBinder jInjectionBinder { get; set; }
        [Inject] public FarmState FarmState { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }

        /// <summary>
        /// TODO: Fucking dirty hack. Remove ASAP. 
        /// </summary>
        public static BuildingShopData CategoryData;

        #region PublicAPIForController

        public IPlayerResources GetPlayerResources => Context.GetPlayerResources;

        public IEnumerable<BuildingItem> GetBuildingsInContainer => View.BuildingItems;

        public BuildingPropertiesTooltip BuildingTooltip => View._buildingPropertiesTooltip;

        public IEnumerable<SectorDependencies> GetSectorDependencies => View.SectorDependencieses;
        
        public IList<SubCategoryFilterView> SubCategoryFilterViews => View.SubCategoryFilterViews;

        public bool HaveSubCategory(BuildingsCategories targetCategory)
        {
            return View.SubCategoryFilterViews.Any(c => c.Category == targetCategory);
        }

        public IGameManager GetGameManager => Context.jGameManager;

        public IIconProvider GetIconProvider => Context.jIconProvider;

        public IGameCamera GetGameCamera => Context.jGameCamera;
        public IGameCameraModel GetGameCameraModel => Context.jGameCameraModel;

        public List<BuildingsFilter.Filter> Filters => CurrentSectorDependency.CategoriesFilter.Filters;
        public bool IsHardTutorialCompleted => Context.jHardTutorial.IsTutorialCompleted;
        public bool IsHintTutorialActive => jHintTutorial.IsHintTutorialActive;

        public BuildingsCategories CurrentCategory => CurrentSectorDependency.CategoriesFilter.CurrentSelectedCategory;

        public Vector3F CurrentFingerPosition => Context.CurrentFingerPosition;

        //todo: should be removed
        public ScrollSnap GetScrollSnap => View._scrollSnap;

        public Action OnClickOutsideProductInfo;

        public CustomRaycast CustomRaycast => View.CustomRaycast;

        public SectorDependencies CurrentSectorDependency
        {
            get => Context.CurrentSectorDependency;
            set => Context.CurrentSectorDependency = value;
        }
        
        private BusinessSectorsTypes CurrentChosenBusinessSector
        {
            get => Context.CurrentChosenBusinesSector;
            set => Context.CurrentChosenBusinesSector = value;
        }

        public BuildingItem AddItem()
        {
            return View.AddItem();
        }

        public void UpdateScrolls()
        {
            View.UpdateScrolls();
        }

        public void ShowPopup(IPopupSettings settings, Action<PopupResult> callback = null)
        {
            Context.ShowPopup(settings, callback);
        }

        public void PlaySfxOnClick()
        {
            Context.PlaySfx();
        }

        public Product GetProduct(int productId)
        {
            return Context.GetProduct(productId);
        }

        public void CreateMapObject(Id_IMapObjectType id, Vector3F position = default, BuildingSubCategories buildingSubCategories = BuildingSubCategories.Common)
        {
            Context.CreateMapObject(id, CurrentChosenBusinessSector, position, buildingSubCategories);
        }

        public void ClearCurrentItems()
        {
            View.ClearCurrentItems();
        }

        #endregion

        protected override void OnShown()
        {
            base.OnShown();
            View.IncrementSectorButton.onClick.AddListener(OnDecrementSector);
            View.DecrementSectorButton.onClick.AddListener(OnIncrementSector);
            jOnMapBuildFinishedSignal.AddListener(OnBuildingMapObject);
            jBuildingConstructionStartedSignal.AddListener(OnBuildingMapObject);
            View.OnPointerDown += Adapter;
            jOnHudBottomPanelIsVisibleSignal.Dispatch(false);

            InitAvailableSectorsByBuildingShop(View.SectorDependencieses);
            
            foreach (SubCategoryFilterView subCategoryFilterView in SubCategoryFilterViews)
            {
                subCategoryFilterView.SubCategoryChanged += OnSubCategoryChanged;
            }
        }

        protected override void OnHidden()
        {
            base.OnHidden();
            View.OnPointerDown -= Adapter;
            View.IncrementSectorButton.onClick.RemoveListener(OnDecrementSector);
            View.DecrementSectorButton.onClick.RemoveListener(OnIncrementSector);
            _controller.UnsubscribeDependencies(View.SectorDependencieses);
            jOnMapBuildFinishedSignal.RemoveListener(OnBuildingMapObject);
            jBuildingConstructionStartedSignal.RemoveListener(OnBuildingMapObject);
            jSignalOnUIAction.Dispatch(new UiIteractionInfo(UiActionType.Hide, "BuildingsShop", UiElementType.Panel));
            jOnHudBottomPanelIsVisibleSignal.Dispatch(true);
            View._scrollSnap.ChangePage(0);
            
            _availableSectorsByBuildingShop.Clear();
            
            foreach (SubCategoryFilterView subCategoryFilterView in SubCategoryFilterViews)
            {
                subCategoryFilterView.SubCategoryChanged -= OnSubCategoryChanged;
            }
        }

        private void Adapter()
        {
            OnClickOutsideProductInfo?.Invoke();
        }

        private void OnSubCategoryChanged(BuildingSubCategories newSubCategory)
        {
            PlaySfxOnClick();
            _controller.UpdateOnTabChange();
        }

        private void OnBuildingMapObject(IMapObject mapObject)
        {
            var mapObjectSecorType = Context.jGameManager.GetMap(mapObject.SectorId)?.BusinessSector;
            
            if (ViewVisible && mapObjectSecorType != null && mapObjectSecorType == CurrentChosenBusinessSector)
            {
                _controller.UpdateSectorDependencies(mapObject.SectorId.ToBusinessSectorType());
            }
        }
        
        protected override void Show(object param)
        {
            base.Show(param);
            if (_controller == null)
            {
                _controller = new BuildingsShopPanelController();
                jInjectionBinder.injector.Inject(_controller);
                _controller.SetMediator(this);
            }

            switch (param) {
                case BuildingShopData data:
                    if (CategoryData != null)
                        data = CategoryData;
                    InitByData(data);
                    break;
                case IMapBuilding building:
                    InitByBuilding(building);
                    break;
            }
        }

        private void InitByBuilding(IMapBuilding building)
        {
            if (building == null || building.IsLocked)
                return;

            InitView(building.SectorIds.FirstOrDefault(), building.Categories.FirstOrDefault(), building.SubCategory);
            View.ScrollToBuilding(building.ObjectTypeID);
        }

        private void InitByData(BuildingShopData data)
        {
            InitView(data.BusinessSector, data.Category, data.SubCategory);
        }
        
        private void InitView(BusinessSectorsTypes businessSector, BuildingsCategories category, BuildingSubCategories subCategory)
        {
            _controller.SubscribeDependencies(View.SectorDependencieses);
            ChooseSector(businessSector, category, subCategory);
            BuildingTooltip.OnHide.AddListener(_controller.OnTooltipClosed);
            jSignalOnUIAction.Dispatch(new UiIteractionInfo(UiActionType.Show, "BuildingsShop", UiElementType.Panel));  
        }

        private void InitAvailableSectorsByBuildingShop(List<SectorDependencies> dependencies)
        {
            _availableSectorsByBuildingShop = new List<BusinessSectorsTypes>();
            
            foreach (var dependency in dependencies)
            {
                BusinessSectorsTypes type = dependency.SectorType;

                if (type == BusinessSectorsTypes.Farm && FarmState.FarmLocked.Value) continue;

                _availableSectorsByBuildingShop.Add(type);
            }
        }

        private void ChooseSector(BusinessSectorsTypes businessSector, BuildingsCategories category = BuildingsCategories.None, BuildingSubCategories subCategory = BuildingSubCategories.None)
        {
            CurrentChosenBusinessSector = businessSector;
            _controller.UpdateSectorDependencies(businessSector);
            BuildingsCategories newCategory = SelectCategory(Filters, category);
            _controller.InitCategory(Filters, newCategory);
            View.SectorName.text = LocalizationUtils.GetSectorTypeText(businessSector);

            // TODO: hotfix. need to remove.
            if (HaveSubCategory(newCategory))
            {
                _controller.UpdateSubCategories(newCategory, subCategory);
            }
            else
            {
                _controller.UpdateSubCategories(newCategory, subCategory);
                _controller.UpdateOnTabChange();
            }

            _controller.FocusOnSector(businessSector);
        }

        public void HideView()
        {
            View.Hide();
        }
        
        public BuildingsCategories SelectCategory(List<BuildingsFilter.Filter> filters, BuildingsCategories category)
        {
            bool forceCategory = category != BuildingsCategories.None;

            if (forceCategory)
            {
                return category;
            }

            return filters.Any(c => c.Notification.Value > 0) ? filters.First(c => c.Notification.Value > 0).BuildingsCategory : filters.First().BuildingsCategory;
        }

        private void OnIncrementSector()
        {
            int currentSector = (_availableSectorsByBuildingShop.IndexOf(CurrentChosenBusinessSector) + 1) % _availableSectorsByBuildingShop.Count;
            ChangeSectorOnBuildingShop(_availableSectorsByBuildingShop[currentSector]);
        }
        
        private void OnDecrementSector()
        {
            int currentSector = _availableSectorsByBuildingShop.IndexOf(CurrentChosenBusinessSector) - 1;
            
            if (currentSector < 0)
            {
                currentSector = _availableSectorsByBuildingShop.Count - 1;
            }
            
            ChangeSectorOnBuildingShop(_availableSectorsByBuildingShop[currentSector]);
        }
        
        private void ChangeSectorOnBuildingShop(BusinessSectorsTypes businessSectorsTypes)
        {
            ChooseSector(businessSectorsTypes);
        }

    }

    public class BuildingShopData
    {
        public BusinessSectorsTypes BusinessSector { get; }
        public BuildingsCategories Category { get; } = BuildingsCategories.None;

        public BuildingSubCategories SubCategory { get; } = BuildingSubCategories.None;

        public BuildingShopData(BusinessSectorsTypes businessSector, BuildingsCategories category, BuildingSubCategories subCategory)
        {
            BusinessSector = businessSector;
            Category = category;
            SubCategory = subCategory;
        }
        
        public BuildingShopData(BusinessSectorsTypes businessSector)
        {
            BusinessSector = businessSector;
        }
    }
}
