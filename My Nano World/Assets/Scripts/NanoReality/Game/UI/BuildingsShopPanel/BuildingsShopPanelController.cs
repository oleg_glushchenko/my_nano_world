using System;
using System.Collections.Generic;
using System.Linq;
using NanoLib.Services.InputService;
using Assets.NanoLib.UI.Core.Views;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.Analytics.Model;
using Assets.Scripts.Engine.BuildingSystem.CityMap;
using Assets.Scripts.Engine.UI.Views.Hud;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Assets.Scripts.Extensions;
using NanoLib.Core.Services.Sound;
using NanoReality.Engine.UI.Extensions.BuildingsShopPanel;
using NanoReality.Engine.UI.Views.BuildingsShopPanel;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Production;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using strange.extensions.command.impl;
using UnityEngine;

namespace NanoReality.Game.UI.BuildingsShopPanel
{
    // TODO: need to remove this class ASAP. It piece of shit from 1st line to last line.
    public class BuildingsShopPanelController : Command
    {
        private const float ZoomSpeed = 4f;
        private const float ZoomPercentage = 0.4f;
        private const string Layer = "Ground";
        private readonly Vector3 _viewportPosition = new Vector3(0.5f, 0.5f, 0f);
        private BuildingsShopPanelMediator _mediator;
        private SubCategoryFilterView _subCategoryFilterView;
        
        [Inject] public BuildingsShopPanelContext jPanelContext { get; set; }
        [Inject] public ISoundManager jSoundManager { get; set; }
        [Inject] public OnHudButtonsIsClickableSignal jOnHudButtonsIsClickableSignal { get; set; }

        // TODO: remove this shit.
        public void SetMediator(BuildingsShopPanelMediator mediator)
        {
            _mediator = mediator;
            _mediator.OnClickOutsideProductInfo += HideProductInfo;
        }

        private void OnBuyBuildingSignal(BuildingItem sender)
        {
            //allow only drag and drop during tutorial
            if ((_mediator.IsHintTutorialActive || !_mediator.IsHardTutorialCompleted) && !sender.IsDragNow)
                return;

            _mediator.jSignalOnUIAction.Dispatch(new UiIteractionInfo(UiActionType.Click, "Building",
                UiElementType.Button));

            bool useCurrentFingerPos = sender.IsDragNow || sender.IsLongTap;

            IPlayerResources playerResources = _mediator.GetPlayerResources;
            if (playerResources.IsEnoughtForBuy(sender.MapObjectPrice))
            {
                BuyBuildingAndClosePanel(sender, useCurrentFingerPos);
            }
            else
            {
                if (playerResources.SoftCurrency < sender.MapObjectPrice.SoftPrice)
                {
                    int need = sender.MapObjectPrice.SoftPrice - playerResources.SoftCurrency;
                    _mediator.ShowPopup(new NotEnoughMoneyPopupSettings(PriceType.Soft, need));
                }
                else if (playerResources.HardCurrency < sender.MapObjectPrice.HardPrice)
                {
                    int need = sender.MapObjectPrice.HardPrice - playerResources.HardCurrency;
                    _mediator.ShowPopup(new NotEnoughMoneyPopupSettings(PriceType.Hard, need));
                }
                else if (playerResources.GoldCurrency < sender.MapObjectPrice.GoldPrice)
                {
                    int need = sender.MapObjectPrice.GoldPrice - playerResources.GoldCurrency;
                    _mediator.ShowPopup(new NotEnoughMoneyPopupSettings(PriceType.Gold, need));
                }
                else
                {
                    _mediator.ShowPopup(
                        new NotEnoughResourcesPopupSettings(sender.MapObjectPrice), result =>
                        {
                            if (result == PopupResult.Ok)
                            {
                                BuyBuildingAndClosePanel(sender, useCurrentFingerPos);
                            }
                        });
                }
            }
        }

        private void BuyBuildingAndClosePanel(BuildingItem item, bool useCurrentFingerPos)
        {
            // TODO: Refactor buildings shop someday!!! 
            jOnHudButtonsIsClickableSignal.Dispatch(false);
            item.OnBuyBuildingSuccess();
            _mediator.HideView();
            Vector3F position = useCurrentFingerPos ? _mediator.CurrentFingerPosition : default;
            _mediator.CreateMapObject(item.CurrentMapObject.ObjectTypeID, position, item.CurrentMapObject.SubCategory);
            jSoundManager.Play(SfxSoundTypes.ChangeBuildingmenu, SoundChannel.SoundFX);
        }
        
        public void InitCategory(List<BuildingsFilter.Filter> filters, BuildingsCategories category)
        {
            foreach (BuildingsFilter.Filter filter in filters)
            {
                filter.Toggle.isOn = filter.BuildingsCategory == category;
            }
        }

        private void OnInfoProductButtonClick(BuildingItem item)
        {
            _mediator.CustomRaycast.Active = true;
            if (item.CurrentMapObject.BalanceName == _mediator.jBuildingShopPanelModel.PreviousItemProductInfoClickedName.Value)
            {
                HideProductInfo();
                return;
            }

            _mediator.jBuildingShopPanelModel.PreviousItemProductInfoClickedName.Value = item.CurrentMapObject.BalanceName;
        }

        public void HideProductInfo()
        {
            _mediator.jBuildingShopPanelModel.PreviousItemProductInfoClickedName.Value = string.Empty;
        }

        private void OnItemSelectChanged(BuildingItem buildingItem)
        {
            BuildingPropertiesTooltip tooltip = _mediator.BuildingTooltip;
            if (buildingItem.IsSelected)
            {
                _mediator.GetBuildingsInContainer.Where(x => x != buildingItem).ForEach(x => x.IsSelected = false);
                tooltip.Show();
                tooltip.SetMapObject((IMapBuilding) buildingItem.CurrentMapObject, buildingItem);
            }
            else if(tooltip.Visible)
            {
                tooltip.Hide();
            }
        }

        private Dictionary<BuildingsCategories, int> GetNewBuildings()
        {
            var newBuildingsList = new Dictionary<BuildingsCategories, int>();

            foreach (BuildingsFilter.Filter filter in _mediator.Filters)
            {
                newBuildingsList.Add(filter.BuildingsCategory, 0);
            }

            foreach (IMapObject mapObject in GetMapObjects(FilterMapObjectByTypes))
            {
                foreach (BuildingsFilter.Filter filter in _mediator.Filters)
                {
                    if (CanAddToNotification(mapObject, filter.BuildingsCategory))
                    {
                        newBuildingsList[filter.BuildingsCategory] += 
                            jPanelContext.jBuildingsUnlockService.GetAvailableBuildingInstancesCount(mapObject.ObjectTypeID);
                    }
                }
            }
            
            return newBuildingsList;
        }

        private IEnumerable<IMapObject> GetMapObjects(Func<IMapObject, bool> filterFunc)
        {
            var mapObjects = new List<IMapObject>(20);
            var targetSector = (int) jPanelContext.CurrentSectorDependency.SectorType;
            List<IMapObject> allSectorBuildings = jPanelContext.jMapObjectsData.GetAllMapObjectsForSector(targetSector);
            
            foreach (IMapObject sectorObject in allSectorBuildings)
            {
                if (sectorObject.Level == 0 && filterFunc(sectorObject))
                {
                    mapObjects.Add(sectorObject);
                }
            }

            return mapObjects;
        }

        private static bool FilterMapObjectByTypes(IMapObject mapObj) =>
            mapObj.MapObjectType != MapObjectintTypes.Road & mapObj.MapObjectType != MapObjectintTypes.Warehouse &
            mapObj.MapObjectType != MapObjectintTypes.CityHall & mapObj.MapObjectType != MapObjectintTypes.OrderDesk &
            mapObj.MapObjectType != MapObjectintTypes.CityOrderDesk & mapObj.MapObjectType != MapObjectintTypes.HeadQuarter;
        
        public void OnTooltipClosed(UIPanelView uiPanelView)
        {
            foreach (BuildingItem buildingItem in _mediator.GetBuildingsInContainer)
            {
                if (buildingItem.CurrentMapObject == _mediator.BuildingTooltip.MapObject)
                {
                    buildingItem.IsSelected = false;
                }
            }
        }
        
        private static bool CanAddToNotification(IMapObject mapObject, BuildingsCategories category)
        {
            return mapObject?.Categories != null && mapObject.Categories.Contains(category);
        }

        public void UpdateSubCategories(BuildingsCategories targetCategory, BuildingSubCategories subCategory)
        {
            _subCategoryFilterView = null;
            
            foreach (SubCategoryFilterView subCategoryFilterView in _mediator.SubCategoryFilterViews)
            {
                bool isTargetCategory = targetCategory == subCategoryFilterView.Category;
                subCategoryFilterView.gameObject.SetActive(isTargetCategory);
                
                if (isTargetCategory)
                {
                    _subCategoryFilterView = subCategoryFilterView;
                    _subCategoryFilterView.InitCategory(subCategory);
                }
            }
        }
        
        public void UpdateSectorDependencies(BusinessSectorsTypes businessSector)
        {
            foreach (SectorDependencies dependencies in _mediator.GetSectorDependencies)
            {
                bool isCurrentSector = dependencies.SectorType == businessSector;
                
                if (dependencies.CategoriesFilter != null) 
                    dependencies.CategoriesFilter.CacheMonobehavior.CachedGameObject.SetActive(isCurrentSector);
                
                if(isCurrentSector)
                    _mediator.CurrentSectorDependency = dependencies;
            }
            
            if (!_mediator.IsHardTutorialCompleted)
                return;

            Dictionary<BuildingsCategories, int> newBuildingsCount = GetNewBuildings();
            
            foreach (BuildingsFilter.Filter filter in _mediator.Filters)
            {
                int count = newBuildingsCount[filter.BuildingsCategory];
                filter.Notification.Value = count;
                filter.Toggle.gameObject.SetActive(count > 0 || !NeedToHideCategoryButton(filter.BuildingsCategory));
            }
        }

        private bool NeedToHideCategoryButton(BuildingsCategories category)
        {
            IEnumerable<IMapObject> allCategoryBuildings = GetMapObjects((IMapObject mapObj) =>
                CanAddToNotification(mapObj, category));
            return allCategoryBuildings == null || !allCategoryBuildings.Any();
        }

        private void OnChangeBuildingCategory(BuildingsCategories category)
        {
            _mediator.PlaySfxOnClick();
            UpdateSubCategories(category, BuildingSubCategories.None);
            UpdateOnTabChange();
        }

        public void UpdateOnTabChange()
        {
            UpdateBuildingsList();
            _mediator.UpdateScrolls();
        }
        
        private void UpdateBuildingsList()
        {
            _mediator.ClearCurrentItems();

            BuildingsCategories currentCategory = _mediator.CurrentCategory;
            
            IEnumerable<IMapObject> allCategoryBuildings = GetMapObjects((mapObject) => 
                FilterMapObjectByPanelTabs(mapObject, currentCategory));
            
            IEnumerable<BuildingsShopItemData> buildingItemModels = allCategoryBuildings.Select(building => 
                jPanelContext.jBuildingService.GetBuildingShopItemData(building));
            
            IOrderedEnumerable<BuildingsShopItemData> sortedBuildings = buildingItemModels.OrderByDescending(c => c.BuildingState == BuildingState.CanBeBuild)
                .ThenBy(c => c.UnlockLevel)
                .ThenBy(o => o.BuildingState);
            
            sortedBuildings.ForEach(CreateBuildingItem);
        }
        
        private bool FilterMapObjectByPanelTabs(IMapObject mapObject, BuildingsCategories category)
        {
            if (mapObject == null) return false;
            var result = false;
            if (mapObject.Categories != null)
            {
                result |= mapObject.Categories.Contains(category);

                if (_subCategoryFilterView != null && _subCategoryFilterView.CurrentSubCategory != BuildingSubCategories.None)
                {
                    result &= mapObject.SubCategory == _subCategoryFilterView.CurrentSubCategory;
                }
            }

            result &= FilterMapObjectByTypes(mapObject);
            return result;
        }

        private void CreateBuildingItem(BuildingsShopItemData shopItemData)
        {
            BuildingItem item = _mediator.AddItem();
            
            item.UpdateItem(_mediator.GetIconProvider, shopItemData, _mediator.GetScrollSnap);

            item.SignalOnBuyBuilding.RemoveAllListeners();
            item.SignalOnSelectChanged.RemoveAllListeners();
            item.SignalOnInfoButtonClick.RemoveAllListeners();   
            item.SignalOnBuyBuilding.AddListener(OnBuyBuildingSignal);
            item.SignalOnSelectChanged.AddListener(OnItemSelectChanged);
            item.SignalOnInfoButtonClick.AddListener(OnInfoProductButtonClick);
        }
        
        public void FocusOnSector(BusinessSectorsTypes targetBusinessSector)
        {
            if (!_mediator.IsHardTutorialCompleted)
                return;

            CityMapView chosenMapView = _mediator.GetGameManager.GetMapView(targetBusinessSector);

            BusinessSectorsTypes currentFocusedSector = GetCurrentSectorByCameraPosition();
            if (currentFocusedSector == chosenMapView.BusinessSector)
            {
                if (!CheckLockedSellByCameraPosition())
                    return;
            }
                

            float zoom = Mathf.Lerp(_mediator.GetGameCameraModel.MinZoom, _mediator.GetGameCameraModel.MaxZoom, ZoomPercentage);

            Vector2F averagePosition = chosenMapView.GetAverageBuildingsPosition();
            _mediator.GetGameCamera.ZoomTo(averagePosition, CameraSwipeMode.Fly, zoom, ZoomSpeed, true);
        }
        
        private BusinessSectorsTypes GetCurrentSectorByCameraPosition()
        {
            int layer = 1 << LayerMask.NameToLayer(Layer);
            RaycastHit hit = InputTools.RaycastScene(Env.MainCamera.ViewportToScreenPoint(_viewportPosition), layer);
            ICityMap currentMap = _mediator.GetGameManager.GetMap(hit.point);
            return currentMap?.BusinessSector ?? BusinessSectorsTypes.Unsupported;
        }
        
        private bool CheckLockedSellByCameraPosition()
        {
            int layer = 1 << LayerMask.NameToLayer("LockedSubSector");
            var cameraScreenPoint = Env.MainCamera.ViewportToScreenPoint(_viewportPosition);
            return InputTools.SphereRaycastScene(cameraScreenPoint, layer);
        }

        public void SubscribeDependencies(List<SectorDependencies> dependencies)
        {
            foreach (SectorDependencies dependency in dependencies)
            {
                dependency.CategoriesFilter.BuildingFilterChanged += OnChangeBuildingCategory;
            }
        }
        
        public void UnsubscribeDependencies(List<SectorDependencies> dependencies)
        {
            foreach (SectorDependencies dependency in dependencies)
            {
                dependency.CategoriesFilter.BuildingFilterChanged -= OnChangeBuildingCategory;
            }
        }
    }
}
