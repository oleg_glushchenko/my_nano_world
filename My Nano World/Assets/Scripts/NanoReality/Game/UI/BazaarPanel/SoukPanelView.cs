﻿using System.Collections.Generic;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.UI.Views.Tooltips;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.Game.UI.BazaarPanel.ProductView;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.UI.BazaarPanel
{
    public class SoukPanelView : UIPanelView
    {
        [SerializeField] private List<SoukTopTradeItemView> _topTradeItems;
        [SerializeField] private List<SoukBottomTradeItemView> _bottomTradeItems;
        [SerializeField] private BazaarChestView[] _chestViews;
        [SerializeField] private TextMeshProUGUI _timerText;

        
        [SerializeField] private TextMeshProUGUI _shufflePriceText;
        [SerializeField] private Button _shuffleButton;
        [SerializeField] private BalancePanelView _balancePanelView;

        [SerializeField] private GameObject _loadingOverlay;
        [SerializeField] private TooltipView _panelTooltip;

        public Button ShuffleButton => _shuffleButton;
        public TextMeshProUGUI ShufflePriceText => _shufflePriceText;
        public BalancePanelView BalancePanelView => _balancePanelView;
        public BazaarChestView[] BazaarChestViews => _chestViews;
        public List<SoukTopTradeItemView> TopTradeItems => _topTradeItems;
        public List<SoukBottomTradeItemView> BottomTradeItems => _bottomTradeItems;
        public TextMeshProUGUI TimerText => _timerText;
        public TooltipView PanelTooltip => _panelTooltip;

        public void SetOverlayActive(bool isActive)
        {
            Debug.Log("SetOverlayActive " + isActive);
            _loadingOverlay.SetActive(isActive);
        }
    }
}
