using System.Linq;
using Assets.NanoLib.UI.Core.Views;
using GameLogic.Bazaar.Service;
using NanoLib.Core.Services.Sound;
using NanoLib.Services.InputService;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.Configs;
using NanoReality.GameLogic.Services;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.UI.BazaarPanel.ProductView
{
    public class BazaarTooltipView: UIPanelView
    {
        [SerializeField] private Image[] _commonProductImage;
        [SerializeField] private Image[] _rareProductImage;
        [SerializeField] private TextMeshProUGUI _commonProductChance;
        [SerializeField] private TextMeshProUGUI _rareProductChance;
        [SerializeField] private TextMeshProUGUI _moneyChance;
        [SerializeField] private TextMeshProUGUI _coins;
        [SerializeField] private TextMeshProUGUI _id;
        [SerializeField] private Button _openTooltipButton;
        
        [Inject] public ISoundManager jSoundManager { get; set; }
        [Inject] public IGameConfigData jGameConfigData { get; set; }
        [Inject] public IBazaarService jBazaarService { get; set; }
        [Inject] public SignalOnBeginTouch jSignalOnBeginTouch { private get; set; }
        [Inject] public IIconProvider jIconProvider { private get; set; }

        private bool _ignoreTouch;
        public Button ToolTipButton => _openTooltipButton;

        public override void Show()
        {
            gameObject.SetActive(true);
            jSignalOnBeginTouch.AddListener(Hide);
            jSoundManager.Play(SfxSoundTypes.AppWindow, SoundChannel.SoundFX);
        }

        public override void Hide()
        {
            gameObject.SetActive(false);
            jSignalOnBeginTouch.RemoveListener(Hide);
            jSoundManager.Play(SfxSoundTypes.CloseWindow, SoundChannel.SoundFX);
        }

        public void InitToolTip(int slotId)
        {
            var config = jBazaarService.SoukConfig;
            _openTooltipButton.onClick.AddListener(Show);

            for (int i = 0; i < _commonProductImage.Length; i++)
            {
                _commonProductImage[i].sprite = jIconProvider.GetProductSprite(jBazaarService.CommonProducts[i].ProductId);
            }

            for (int i = 0; i < _rareProductImage.Length; i++)
            {
                _rareProductImage[i].sprite = jIconProvider.GetProductSprite(jBazaarService.RareProducts[i].ProductId);
            }

            var curChance = config.ChestsConfig.FirstOrDefault(x => x.SlotId == slotId);
            
            if (curChance == null)
            {
                Debug.LogError("No bazaar config on game config for slot or chest " + slotId);
                return;
            }
            
            _commonProductChance.text = $"{curChance.CommonItemChance * 100}%";
            _rareProductChance.text = $"{curChance.SpecialItemChance * 100}%";
            
            _moneyChance.text = $"{curChance.NanoBucksChance * 100}%";
            _coins.text =curChance.NanoCoins.ToString();
            _id.text = (curChance.SlotId +1).ToString();
        }
    }
}