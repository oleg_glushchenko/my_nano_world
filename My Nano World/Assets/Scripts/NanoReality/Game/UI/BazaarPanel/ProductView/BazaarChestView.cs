﻿using System;
using Assets.NanoLib.Utilities.Pulls;
using Assets.Scripts.Engine.UI.Views.Trade.Items;
using Assets.Scripts.GameLogic.Utilites;
using GameLogic.Bazaar.Model.impl;
using NanoReality.Game.UI.BazaarPanel.ProductView;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.Services;
using strange.extensions.mediation.impl;
using strange.extensions.pool.api;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.UI.BazaarPanel
{
    public class BazaarChestView : View
    {
        [SerializeField] private TextMeshProUGUI _priceText;
        [SerializeField] private TextMeshProUGUI _coinsText;
        [SerializeField] private Image[] _productImgaImages;
        [SerializeField] private Image _rewardBoxImage;
        [SerializeField] private Button _purchaseButton;
        [SerializeField] private BazaarTooltipView _tooltip;

        [Inject] public IIconProvider jIconProvider { get; set; }

        public BazaarLootBox LootBoxOffer { get; private set; }
        
        public event Action<BazaarChestView> BuyClicked;
        
        private void OnEnable()
        {
            _purchaseButton.onClick.AddListener(OnBuyClicked);
        }

        private void OnDisable()
        {
            _purchaseButton.onClick.RemoveListener(OnBuyClicked);
        }
        
        public void Initialize(BazaarLootBox bazaarLootBox)
        {
            LootBoxOffer = bazaarLootBox;
            _coinsText.text = LootBoxOffer.BoxReward.NanoCoins.ToString();
            _priceText.text = LootBoxOffer.BoxPrice == 0 
                ? LocalizationMLS.Instance.GetText(LocalizationKeyConstants.FREE_LABEL) 
                : LootBoxOffer.BoxPrice.ToString();
            _purchaseButton.gameObject.SetActive(true);
            _purchaseButton.interactable = true;
            _tooltip.InitToolTip(LootBoxOffer.Id);
            _rewardBoxImage.sprite = jIconProvider.GetRewardBoxSprite(LootBoxOffer.Id);
        }

        public void Done()
        {
            _purchaseButton.gameObject.SetActive(false);
            _tooltip.ToolTipButton.onClick.RemoveAllListeners();
            _tooltip.Hide();
        }

        public void Clear()
        {
            _tooltip.ToolTipButton.onClick.RemoveAllListeners();
        }
        
        private void OnBuyClicked()
        {
            BuyClicked?.Invoke(this);
        }
    }
}
