using Assets.Scripts.GameLogic.Utilites;
using DG.Tweening;
using NanoReality.Game.FoodSupply;
using NanoReality.GameLogic.Services;
using NanoReality.GameLogic.Utilites;
using strange.extensions.mediation.impl;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.UI
{
    public sealed class FoodSupplyFeedingItemView : View
    {
        [Header("Common")]
        [SerializeField] private TextMeshProUGUI _caloriesText;
        [SerializeField] private TextMeshProUGUI _timeMultiplierText;
        [SerializeField] private int _xTimeSignSize = 32;
        [SerializeField] private Button _selectItemButton;
        [SerializeField] private GameObject _backgroundPanel;

        [Header("Locked State")]
        [SerializeField] private GameObject _lockedSlotPanel;
        
        [Header("Available To Unlock State")]
        [SerializeField] private GameObject _availableToUnlockPanel;
        [SerializeField] private Image _priceIconImage;
        [SerializeField] private TextMeshProUGUI _priceText;
        [SerializeField] private Button _unlockButton;

        [Header("Unlocked State")]
        [SerializeField] private GameObject _unlockedSlotPanel;
        [SerializeField] private GameObject _emptySlotPanel;
        [SerializeField] private GameObject _unlockedSlotCaloriesLable;
        [SerializeField] private Image _productIconImage;
        [SerializeField] private Image _emptySlotImage;
        [SerializeField] private Material _grayscaleMaterial;

        public FoodSupplyFeedingItemModel Model { get; } = new FoodSupplyFeedingItemModel();
        public Button SelectItemButton => _selectItemButton;

        private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();
        private Tweener _animationTween;
        
        [Inject] public IIconProvider jIconProvider { get; set; }

        protected override void Start()
        {
            base.Start();
            
            Model.IsActive.Subscribe(isActive => gameObject.SetActive(isActive));
            Model.Clicked.BindTo(_selectItemButton).AddTo(_compositeDisposable);
            Model.Clicked.Subscribe(PlayClickAnimation).AddTo(_compositeDisposable);
            Model.Interactable.SubscribeToInteractable(_selectItemButton).AddTo(_compositeDisposable);
            Model.LoadedProductProperty.Subscribe(OnResultProductChanged).AddTo(_compositeDisposable);
            Model.ProductIconSprite.Subscribe(sprite => _productIconImage.sprite = sprite).AddTo(_compositeDisposable);
            Model.SlotProperty.Subscribe(OnSlotChanged).AddTo(_compositeDisposable);
            Model.AvailableToLoad.Subscribe(OnSlotAvailableLoadChanged).AddTo(_compositeDisposable);
            Model.AvailableToUnlock.Subscribe(OnAvailableToUnlockChanged).AddTo(_compositeDisposable);
            Model.UnlockSlotClicked.BindTo(_unlockButton).AddTo(_compositeDisposable);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            _compositeDisposable.Dispose();
            _animationTween?.Kill();
        }

        private void PlayClickAnimation(Unit unit)
        {
            _animationTween?.Kill(true);
            
            const float scale = 0.05f;
            const float duration = 0.25f;
            const int vibrato = 1;
            const float elasticity = 0.1f;

            _animationTween = transform.DOPunchScale(scale * Vector3.one, duration, vibrato, elasticity);
        }

        private void OnAvailableToUnlockChanged(bool isAvailableToLoad)
        {
            _availableToUnlockPanel.gameObject.SetActive(isAvailableToLoad);
            if (isAvailableToLoad)
            {
                _lockedSlotPanel.gameObject.SetActive(false);
            } 
        }
        
        private void OnSlotAvailableLoadChanged(bool isAvailableToLoad)
        {
            _emptySlotImage.material = isAvailableToLoad ? null : _grayscaleMaterial;
        }

        private void OnResultProductChanged(FoodSupplyProduct productRecipe)
        {
            var isEmpty = productRecipe == null;
            
            _emptySlotPanel.gameObject.SetActive(isEmpty);
            _productIconImage.enabled = !isEmpty;
            _backgroundPanel.gameObject.SetActive(isEmpty);
            _unlockedSlotCaloriesLable.gameObject.SetActive(!isEmpty);
            
            if (productRecipe == null)
            {
                _caloriesText.text = string.Empty;
                return;
            }

            _caloriesText.text = string.Format(LocalizationUtils.LocalizeL2("Localization/FOOD_CALORIES"), 
                productRecipe.Calories);
        }

        private void OnSlotChanged(FoodSupplyFeedingSlot slot)
        {
            bool isLocked = slot.IsLocked;
            _lockedSlotPanel.gameObject.SetActive(isLocked);
            _unlockedSlotPanel.gameObject.SetActive(!isLocked);
            if (isLocked)
            {
                var unlockPrice = slot.UnlockPrice;
                _priceIconImage.sprite = jIconProvider.GetCurrencySprite(unlockPrice.PriceType);
                _priceText.text = unlockPrice.GetPriceValue() == 0 
                    ? LocalizationMLS.Instance.GetText(LocalizationKeyConstants.FREE_LABEL) 
                    : unlockPrice.GetPriceValue().ToString();
            }

            _timeMultiplierText.text = $"<size={_xTimeSignSize}>x</size>{slot.CaloriesMultiplier}";
        }
    }

    public sealed class FoodSupplyFeedingItemModel
    {
        public readonly BoolReactiveProperty IsActive = new BoolReactiveProperty(true);
        public readonly ReactiveCommand Clicked = new ReactiveCommand();
        public readonly ReactiveCommand UnlockSlotClicked = new ReactiveCommand();
        public readonly ReactiveProperty<Sprite> ProductIconSprite = new ReactiveProperty<Sprite>();
        public readonly ReactiveProperty<bool> Interactable = new ReactiveProperty<bool>(true);
        
        public readonly ReactiveProperty<FoodSupplyProduct> LoadedProductProperty = new ReactiveProperty<FoodSupplyProduct>();
        public readonly ReactiveProperty<FoodSupplyFeedingSlot> SlotProperty = new ReactiveProperty<FoodSupplyFeedingSlot>();
        public readonly ReactiveProperty<bool> AvailableToLoad = new ReactiveProperty<bool>();
        public readonly ReactiveProperty<bool> AvailableToUnlock = new ReactiveProperty<bool>();


        public FoodSupplyProduct LoadedProduct => LoadedProductProperty.Value;
        public FoodSupplyFeedingSlot Slot => SlotProperty.Value;
    }
}