using System.Collections.Generic;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.UI.Views.Tooltips;
using DG.Tweening;
using Engine.UI.Components;
using NanoReality.UI.Components;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.UI
{
    public sealed class FoodSupplyFeedingPanelView : UiSubPanelView
    {
        [SerializeField] private List<FoodSupplyFeedingItemView> _feedingItemViews;
        [SerializeField] private Button _feedCitizensButton;
        [SerializeField] private Image _feedingAvailableButtonImage;
        [SerializeField] private Image _feedingInProgressButtonImage;
        [SerializeField] private SimpleTooltip _hintTooltip;
        [SerializeField] private ResizableProgressBar _expectedFoodSupplyProgressBar;
        [SerializeField] private GameObject _additionalCoinsContainer;
        [SerializeField] private TextMeshProUGUI _additionalCoinsCount;

        public List<FoodSupplyFeedingItemView> FeedingItemViews => _feedingItemViews;

        public Button FeedCitizensButton => _feedCitizensButton;
        
        public Image FeedingInProgressButtonImage => _feedingInProgressButtonImage;
        
        public Image FeedingAvailableButtonImage => _feedingAvailableButtonImage;
        
        public ResizableProgressBar ExpectedFoodSupplyProgressBar => _expectedFoodSupplyProgressBar;

        public GameObject AdditionalCoinsContainer => _additionalCoinsContainer;

        public TextMeshProUGUI AdditionalCoinsCount => _additionalCoinsCount;

        public SimpleTooltip HintTooltip => _hintTooltip;

        public AsyncSubject<Unit> PlayFeedingButtonFilling()
        {
            var playCookingAnimationAsyncSubject = new AsyncSubject<Unit>();
            playCookingAnimationAsyncSubject.OnNext(Unit.Default);
            
            FeedingInProgressButtonImage.fillAmount = 0;

            FeedingInProgressButtonImage.DOFillAmount(1, 1.5f).OnComplete(() =>
            {
                playCookingAnimationAsyncSubject.OnCompleted();
            });

            return playCookingAnimationAsyncSubject;
        }

        public void SetFeedingAvailableActive(bool isActive)
        {
            FeedingAvailableButtonImage.gameObject.SetActive(isActive);
        }   
        
        public void SetFeedingInProgressActive(bool isActive)
        {
            FeedingInProgressButtonImage.gameObject.SetActive(isActive);
        }
    }
}