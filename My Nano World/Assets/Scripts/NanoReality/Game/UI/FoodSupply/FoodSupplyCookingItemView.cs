using System.Linq;
using DG.Tweening;
using NanoReality.Game.FoodSupply;
using NanoReality.GameLogic.Services;
using strange.extensions.mediation.impl;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.UI
{
    public sealed class FoodSupplyCookingItemView : View
    {
        [SerializeField] private GameObject _container;
        [SerializeField] private Image _rewardProductImage;
        [SerializeField] private Button _selectItemButton;
        [SerializeField] private Material _selectedMaterial;
        [SerializeField] private GameObject _selectedBackgroundVFX;
        [SerializeField] private GameObject _selectedVFX;
        [SerializeField] private GameObject _coockingVFX;

        private float _cookDuration = 1f;

        public FoodSupplyOrderItemModel Model { get; } = new FoodSupplyOrderItemModel();
        public Button SelectItemButton => _selectItemButton;

        private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();
        
        [Inject] public IIconProvider jIconProvider { get; set; }

        protected override void Start()
        {
            base.Start();
            
            Model.IsActive.Subscribe(isActive => _container.SetActive(isActive));
            Model.Clicked.BindTo(_selectItemButton).AddTo(_compositeDisposable);
            Model.Interactable.SubscribeToInteractable(_selectItemButton).AddTo(_compositeDisposable);
            Model.IsSelected.Subscribe(OnSelectChanged).AddTo(_compositeDisposable);
            Model.ProductData.Subscribe(OnResultProductDataChanged).AddTo(_compositeDisposable);
            
            _cookDuration= GetFxMaxLifeTime();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            _compositeDisposable.Dispose();
        }

        private void OnResultProductDataChanged(FoodSupplyProduct productData)
        {
            if (productData == null)
            {
                _rewardProductImage.sprite = null;
                return;
            }
            
            _rewardProductImage.sprite = jIconProvider.GetProductSprite(productData.Id);
        }

        private void OnSelectChanged(bool isSelected)
        {
            _rewardProductImage.material = isSelected ? _selectedMaterial : null;
            
            _selectedVFX.SetActive(isSelected);
            _selectedBackgroundVFX.SetActive(isSelected);

            _selectedBackgroundVFX.transform.localScale = Vector3.zero;
            if (isSelected)
            {
                _selectedBackgroundVFX.transform.DOScale(1.0f, 0.3f);
            }
        }

        public AsyncSubject<Unit> PlayCookingVfx()
        {
            var playCookingVfxAsyncSubject = new AsyncSubject<Unit>();
            playCookingVfxAsyncSubject.OnNext(Unit.Default);

            if (!_coockingVFX)
            {
                playCookingVfxAsyncSubject.OnCompleted();
                return playCookingVfxAsyncSubject;
            }

            _coockingVFX.SetActive(true);
            DOVirtual.DelayedCall(_cookDuration, () =>
            {
                _coockingVFX.SetActive(false);
                playCookingVfxAsyncSubject.OnCompleted();
            });

            return playCookingVfxAsyncSubject;
        }

        private float GetFxMaxLifeTime()
        {
            var particleSystems = _coockingVFX.GetComponentsInChildren<ParticleSystem>();
            return particleSystems.Aggregate(0f, (current, particle) =>
                Mathf.Max(current, particle.main.startLifetime.constantMax + current));
        }
    }
    
    public sealed class FoodSupplyOrderItemModel
    {
        public readonly BoolReactiveProperty IsActive = new BoolReactiveProperty(true);
        public readonly ReactiveProperty<FoodSupplyProductRecipe> ResultProductProperty = new ReactiveProperty<FoodSupplyProductRecipe>();
        public readonly ReactiveProperty<FoodSupplyProduct> ProductData = new ReactiveProperty<FoodSupplyProduct>();
        public readonly ReactiveCommand Clicked = new ReactiveCommand();
        public readonly BoolReactiveProperty IsSelected = new BoolReactiveProperty(false);
        public readonly ReactiveProperty<bool> Interactable = new ReactiveProperty<bool>(true);

        public FoodSupplyProductRecipe ResultProductRecipe => ResultProductProperty.Value;
    }
}