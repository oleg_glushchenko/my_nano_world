using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.UI.Views.Hud;
using DG.Tweening;
using Engine.UI;
using NanoLib.Core.Services.Sound;
using NanoReality.Engine.UI.Extensions;
using NanoReality.Game.CitizenGifts;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.SupplyFood;
using NanoReality.GameLogic.Constants;
using NanoReality.GameLogic.Data;
using NanoReality.GameLogic.Utilites;
using UnityEngine;

namespace NanoReality.Game.UI
{
    public sealed class FoodSupplyPanelMediator : BuildingPanelMediator<FoodSupplyBuilding, FoodSupplyPanelView>
    {
        [Inject] public FoodSupplyPanelContext Context { get; set; }
        [Inject] public ICityGiftsService jCityGiftsService { get; set; }
        [Inject] public OnHudBottomPanelIsVisibleSignal jOnHudBottomPanelIsVisibleSignal { get; set; }

        private UiSubPanelController _currentSubPanel;

        protected override void OnShown()
        {
            base.OnShown();

            View.TabToggleSelected += OnTabToggleSelected;
            foreach (var tab in View.TabToggles)
            {
                tab.Value.Initialize(CurrentModel, Context);
            }

            if (_currentSubPanel == null)
            {
                var tabControllerPair = View.TabToggles.First();
                View.TabsToggleGroup.SetAllTogglesOff();
                tabControllerPair.Key.isOn = true;
                SetSubPanelActive(tabControllerPair.Value);
            }

            View.CloseButton.onClick.AddListener(Hide);
            View.OpenInfoPanelButton.onClick.AddListener(ShowInfoPanel);
            View.CloseInfoPanelButton.onClick.AddListener(HideInfoPanel);
            View.PresentButton.onClick.AddListener(ClickOnGift);

            var balancePanelModel = Context.jBalancePanelModel;
            View.BalancePanelView.Init(balancePanelModel);
            balancePanelModel.TempBalancePanel.Value = View.BalancePanelView;

            UpdateView();
            RewardsUpdate();

            jSoundManager.Play(SfxSoundTypes.Bakery, SoundChannel.SoundFX);

            Context.jFoodSupplyService.FoodSupplyAmountChanged += UpdateProgressBar;
            Context.jFoodSupplyService.FoodSupplyAmountChanged -= TryAddAdditionalReward;
            Context.jFoodSupplyService.FoodSupplyAmountChanged += TryAddAdditionalReward;
            Context.jFoodSupplyService.FoodSupplyCityInfoChanged += UpdateView;
            Context.jFoodSupplyService.FoodSupplyCityInfoChanged += RewardsUpdate;
            Context.jFoodSupplyService.StatusEffectActivated += SetActiveStatusEffectVFX;
            Context.jPlayerExperience.LevelChanged += UpdateAdditionalRewards;
            jCityGiftsService.GiftsUpdates += SetGiftAnimation;
        }

        protected override void OnHidden()
        {
            base.OnHidden();

            View.TabToggleSelected -= OnTabToggleSelected;

            View.CloseButton.onClick.RemoveListener(Hide);
            View.OpenInfoPanelButton.onClick.RemoveListener(ShowInfoPanel);
            View.CloseInfoPanelButton.onClick.RemoveListener(HideInfoPanel);
            View.PresentButton.onClick.RemoveListener(ClickOnGift);

            SetVisibleHudButtons(true);
            View.BalancePanelView.UnsubscribeBalancePanel();

            Context.jBalancePanelModel.TempBalancePanel.Value = null;
            Context.jFoodSupplyService.FoodSupplyCityInfoChanged -= UpdateView;
            Context.jFoodSupplyService.FoodSupplyCityInfoChanged -= RewardsUpdate;
            Context.jFoodSupplyService.StatusEffectActivated -= SetActiveStatusEffectVFX;
            Context.jPlayerExperience.LevelChanged -= UpdateAdditionalRewards;
            jCityGiftsService.GiftsUpdates -= SetGiftAnimation;

            jSoundManager.Play(SfxSoundTypes.CloseWindow, SoundChannel.SoundFX);

            foreach (var subPanelController in View.TabControllers)
            {
                subPanelController.Hide();
            }

            _currentSubPanel = null;
            jOnHudBottomPanelIsVisibleSignal.Dispatch(true);
        }

        private void UpdateView()
        {
            SetVisibleHudButtons(false);

            UpdateProgressBar(Context.jFoodSupplyService.ProgressBarAmount);
            SetRewardItems(Context.jFoodSupplyService.GetAdditionalRewards());

            SetGiftAnimation();

            if (Context.jFoodSupplyService.IsDataLoaded)
            {
                View.ToolTipPanel.gameObject.SetActive(true);
                SetActiveStatusEffectVFX(Context.jFoodSupplyService.IsFilled);
                View.Overlay.SetActive(false);
            }
            else
            {
                View.ToolTipPanel.gameObject.SetActive(false);
                View.Overlay.SetActive(true);
            }
        }

        private void UpdateProgressBar(float amount)
        {
            View.CurrentFoodSupplyProgressBar.Value = amount;

            int maxCalories = Context.jFoodSupplyService.MaxAvailableCalories;
            View.CurrentCaloriesHint.text = Mathf.Floor(maxCalories * amount) + "/" + maxCalories;
        }

        private void SetActiveStatusEffectVFX(bool isActive)
        {
            View.StatusEffectVFX.SetActive(isActive);
            SetNewRewards();
        }

        private void HideInfoPanel()
        {
            View.FoodSupplyInfoPanel.gameObject.SetActive(true);
            View.FoodSupplyInfoPanel.DOFade(0, 0.3f).OnComplete(() =>
            {
                View.FoodSupplyInfoPanel.gameObject.SetActive(false);
            });
        }

        private void ShowInfoPanel()
        {
            View.FoodSupplyInfoPanel.gameObject.SetActive(true);
            View.FoodSupplyInfoPanel.alpha = 0;
            View.FoodSupplyInfoPanel.DOFade(1, 0.3f);
        }

        private void SetVisibleHudButtons(bool isVisible)
        {
            Context.jBalancePanelModel.HappinessModelActive.SetValueAndForceNotify(isVisible);
            Context.jBalancePanelModel.PopulationHappinessModelActive.SetValueAndForceNotify(isVisible);
        }

        private void OnTabToggleSelected(UiSubPanelController selectedTabPanel)
        {
            SetSubPanelActive(selectedTabPanel);
        }

        private void SetSubPanelActive(UiSubPanelController activeSubPanel)
        {
            foreach (var uiSubPanelController in View.TabControllers)
            {
                if (!uiSubPanelController.Equals(activeSubPanel))
                {
                    uiSubPanelController.Hide();
                }
            }

            View.HideToggles(activeSubPanel as FoodSupplyPanelController);
            jSoundManager.Play(SfxSoundTypes.ClickButton, SoundChannel.SoundFX);
            _currentSubPanel = activeSubPanel;
            _currentSubPanel.Show();
        }

        private void SetRewardItems(Dictionary<int, Reward> rewards)
        {
            if (View.RewardItems.Count > 0)
            {
                return;
            }
            
            var isApproachFxSet = false;
            foreach (var reward in rewards)
            {
                var percentValue = reward.Key;
                var rewardItem = View.InstantiateTip(percentValue);

                rewardItem.Initialize(reward.Value, percentValue, GetTipsSprite(reward.Value));
                
                rewardItem.gameObject.SetActive(true);
                // rewardTip.Clicked += RewardTipClicked;
                
                var filledPercent = 100 * Context.jFoodSupplyService.CurrentCalories / Context.jFoodSupplyService.MaxAvailableCalories;

                if (filledPercent >= percentValue)
                {
                    rewardItem.DisableWithoutFx();
                }
                else if (!isApproachFxSet)
                {
                    rewardItem.ShowOnApproachFx();
                    isApproachFxSet = true;
                }

                var rewardTipTransform = rewardItem.transform;
                var currentPosition = rewardTipTransform.localPosition;

                var x = View.ProgressBarTransform.rect.width * percentValue / 100f;
                rewardTipTransform.localPosition = new Vector3(x, currentPosition.y, currentPosition.z);
            }
        }

        private void TryAddAdditionalReward(float amount)
        {
            var filledPercentValue = (int) (amount * 100);
            var tips = View.RewardItems;
            
            var toRemove = new List<FoodSupplyRewardItem>();
            foreach (var rewardTip in tips)
            {
                if (rewardTip.PercentValue > filledPercentValue || rewardTip.isDisabled) continue;

                var position = Context.jGameCamera.ScreenToWorldPoint(rewardTip.gameObject.transform.position);
                var reward = rewardTip.RewardData;
                if (reward.Type.HasFlag(ResourcesType.Soft))
                {
                    var rewardItemSettings =
                        new AddItemSettings(FlyDestinationType.SoftCoins, position, reward.softCurrencyCount);
                    Context.jAddRewardItemSignal.Dispatch(rewardItemSettings);
                    Context.jPlayerProfile.PlayerResources.AddSoftCurrency(reward.softCurrencyCount);
                }

                if (reward.Type.HasFlag(ResourcesType.Hard))
                {
                    var rewardItemSettings = new AddItemSettings(FlyDestinationType.PremiumCoins, position,
                        reward.hardCurrencyCount);
                    Context.jAddRewardItemSignal.Dispatch(rewardItemSettings);
                    Context.jPlayerProfile.PlayerResources.AddHardCurrency(reward.hardCurrencyCount);
                }

                if (reward.Type.HasFlag(ResourcesType.Materials))
                {
                    foreach (var parsedReward in reward.GetParsedRewards())
                    {
                        var sprite = Context.jIconProvider.GetProductSprite(parsedReward.Key);
                        var rewardItemSettings = new AddItemSettings(FlyDestinationType.Warehouse, position,
                            parsedReward.Value, sprite);
                        Context.jAddRewardItemSignal.Dispatch(rewardItemSettings);
                        Context.jPlayerProfile.PlayerResources.AddProduct(parsedReward.Key, parsedReward.Value);
                    }
                }

                toRemove.Add(rewardTip);
            }

            View.DestroyRewardTipsWithAnimation(toRemove);
            HighlightNextReward();
        }

        private void HighlightNextReward()
        {
            var tips = View.RewardItems;
            foreach (FoodSupplyRewardItem rewardTip in tips)
            {
                if (rewardTip.isDisabled)
                {
                    continue;
                }

                rewardTip.ShowOnApproachFx();
                break;
            }
        }

        private void ClickOnGift()
        {
            IMapBuilding gift = jCityGiftsService.GetBuildWithGift();
            if (gift == null)
            {
                return;
            }

            MapObjectView mapObjectView = jGameManager.GetMapObjectView(gift);
            jGameCamera.FocusOnMapObject(mapObjectView.MapObjectModel);
            Hide();
        }

        private void SetGiftAnimation()
        {
            IMapBuilding gift = jCityGiftsService.GetBuildWithGift();
            if (gift == null && !jCityGiftsService.IsAnyGiftsAvailable)
            {
                View.StopGiftAnimation();
                return;
            }

            View.PlayGiftAnimation();
        }

        private void UpdateAdditionalRewards(int level)
        {
            Context.jFoodSupplyService.FoodSupplyCityInfoChanged += SetNewRewards;
        }

        private void SetNewRewards()
        {
            Context.jFoodSupplyService.FoodSupplyCityInfoChanged -= SetNewRewards;

            var filledPercent = 100 * Context.jFoodSupplyService.CurrentCalories /
                                Context.jFoodSupplyService.MaxAvailableCalories;

            View.ActivateRewardTips(filledPercent);

            SetRewardItems(Context.jFoodSupplyService.GetAdditionalRewards());
        }

        private Sprite GetTipsSprite(Reward rewards)
        {
            if (rewards.Type.HasFlag(ResourcesType.Hard))
            {
                return Context.jIconProvider.GetCurrencySprite(PriceType.Hard);
            }

            if (rewards.Type.HasFlag(ResourcesType.Soft))
            {
                return Context.jIconProvider.GetCurrencySprite(PriceType.Soft);
            }

            if (rewards.Type.HasFlag(ResourcesType.Materials))
            {
                foreach (var parsedReward in rewards.GetParsedRewards())
                {
                    return Context.jIconProvider.GetProductSprite(parsedReward.Key);
                }
            }

            return Context.jIconProvider.GetCurrencySprite(PriceType.Soft);
        }

        private void RewardTipClicked(FoodSupplyRewardItem rewardItem)
        {
            var tooltip = View.RewardsTooltip;
            tooltip.SetRewards(rewardItem.RewardData);
            tooltip.transform.position = rewardItem.transform.position;
            tooltip.Title = LocalizationUtils.LocalizeL2("UICommon/REWARD_TITLE_TEXT");
            tooltip.Show();
        }

        private void RewardsUpdate()
        {
            var rewards = Context.jFoodSupplyService.GetAdditionalRewards();
            foreach (var reward in rewards)
            {
                var percentValue = reward.Key;
                var rewardItem = View.InstantiateTip(percentValue);

                rewardItem.Initialize(reward.Value, percentValue, GetTipsSprite(reward.Value));
            }
        }
    }
}