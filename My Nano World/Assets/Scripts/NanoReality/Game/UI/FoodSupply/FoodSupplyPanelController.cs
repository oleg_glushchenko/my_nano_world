using Assets.NanoLib.UI.Core.Views;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.SupplyFood;

namespace NanoReality.Game.UI
{
    public class FoodSupplyPanelController : UiSubPanelController
    {
        protected FoodSupplyPanelContext Context;
        protected IFoodSupplyBuilding CurrentModel;
        
        public void Initialize(IFoodSupplyBuilding buildingModel, FoodSupplyPanelContext context)
        {
            CurrentModel = buildingModel;
            Context = context;
        }
    }
}