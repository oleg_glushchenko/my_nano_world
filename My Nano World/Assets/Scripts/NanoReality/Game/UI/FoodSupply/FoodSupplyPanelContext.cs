using NanoLib.Core.Services.Sound;
using Assets.NanoLib.UI.Core.Signals;
using Assets.Scripts.Engine.UI.Views.Popups;
using NanoReality.Game.FoodSupply;
using NanoReality.Game.OrderDesk;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.Configs;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Services;
using NanoReality.UI.Components;

namespace NanoReality.Game.UI
{
    public sealed class FoodSupplyPanelContext
    {
        [Inject] public IGameCamera jGameCamera { get; set; }
        [Inject] public ISoundManager jSoundManager { get; set; }
        [Inject] public IPopupManager jPopupManager { get; set; }
        [Inject] public IIconProvider jIconProvider { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IGameConfigData jGameConfigData { get; set; }
        [Inject] public IPlayerExperience jPlayerExperience { get; set; }
        [Inject] public IFoodSupplyService jFoodSupplyService { get; set; }
        
        [Inject] public AddRewardItemSignal jAddRewardItemSignal { get; set; }
        [Inject] public ShowWindowSignal jShowWindowSignal { get; set; }
        [Inject] public SignalWarehouseProductsAmountChanged jWarehouseChangedSignal { get; set; }
        [Inject] public BuyProductsActionSignal jBuyProductsActionSignal { get; set; }
        [Inject] public SignalOnNeedToShowTooltip jSignalOnNeedToShowTooltip { get; set; }
        
        [Inject] public BalancePanelModel jBalancePanelModel { get; set; }

        public IPlayerResources PlayerResources => jPlayerProfile.PlayerResources;
    }
}