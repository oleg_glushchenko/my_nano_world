﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Assets.Scripts.GameLogic.Utilites;
using Assets.Scripts.MultiLanguageSystem.Logic;
using NanoReality.Core.Resources;
using NanoReality.Engine.UI.Extensions.MapObjects.CityHallTaxes;
using NanoReality.GameLogic.Utilites;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.UI
{
    public class DownloadPopupView : BasePopupView
    {
        [SerializeField] protected Button _okButton;
        [SerializeField] protected Button _cancelButton;
        [SerializeField] protected TextMeshProUGUI _buttonLabel;

        [Inject] public SignalReloadApplication jSignalReloadApplication { get; set; }
        [Inject] public SignalOnShownPanel jUiPanelOpenedSignal { get; set; }
        
        public override void Show(Action<PopupResult> callback)
        {
            base.Show(callback);

            _okButton.interactable = false;
            
            StartCoroutine(DownloadRemoteBundle(AssetBundlesNames.ManifestAssetBundleName, onCompleteCallback: GetAssetBundlesSizes));

            UpdateLocales(0ul);
            jUiPanelOpenedSignal.AddListener(OnUiPanelOpened);
        }
         private void OnUiPanelOpened(UIPanelView view)
         {
             if (view is CityHallTaxesView ) 
                 view.Hide();
         }

         public override void Hide()
         {
             base.Hide();
             jUiPanelOpenedSignal.RemoveListener(OnUiPanelOpened);
         }

         private void GetAssetBundlesSizes(AssetBundle manifestBundle)
        {
            var manifest = manifestBundle.LoadAsset<AssetBundleManifest>(AssetBundlesNames.ManifestFileName);
            var contentAssetBundlesNames = manifest.GetAllAssetBundles();

            StartCoroutine(GetAssetBundlesSize(contentAssetBundlesNames, OnSizesUpdate));
            AssetBundleManager.Unload(AssetBundlesNames.ManifestAssetBundleName);
        }

        private IEnumerator GetAssetBundlesSize(IEnumerable<string> bundlesName, Action<ulong> onComplete)
        {
            var totalSize = 0ul;
            foreach (var bundleName in bundlesName)
            {
                var downloadOperation = new AssetBundleWebRequestOperation<ulong>(bundleName, default, size => totalSize += size, ShowNetworkErrorMessage);
                yield return downloadOperation.Download((downloadBundleName, hash, resultReceived) => AssetBundleManager.GetRemoteBundleSize(downloadBundleName, resultReceived));
            }

            onComplete?.Invoke(totalSize);
        }
        
        private IEnumerator DownloadRemoteBundle(string bundleName, Hash128 hash = default, Action<AssetBundle> onCompleteCallback = null)
        {
            var downloadOperation = new AssetBundleWebRequestOperation<AssetBundle>(bundleName, hash, onCompleteCallback, ShowNetworkErrorMessage);
            yield return downloadOperation.Download(AssetBundleManager.DownloadAssetBundle);
        }

        private void OnSizesUpdate(ulong bytesSize)
        {
            _okButton.interactable = true;

            UpdateLocales(bytesSize);
        }
        
        private void UpdateLocales(ulong bytesSize)
        {
            var buttonLocale = LocalizationMLS.Instance.GetText(LocalizationKeyConstants.DOWNLOAD_BUTTON);
            _buttonLabel.SetLocalizedText(LocalizationUtils.SafeFormat(buttonLocale, ConvertToMegaBytes(bytesSize).ToString("F2")));
        }

        private void OnEnable()
        {
            _okButton.onClick.AddListener(OnOkButtonClick);
            _cancelButton.onClick.AddListener(OnCancelButtonClick);
        }

        private void OnDisable()
        {
            _okButton.onClick.RemoveListener(OnOkButtonClick);
            _cancelButton.onClick.RemoveListener(OnCancelButtonClick);
        }
        
        private static float ConvertToMegaBytes(ulong bytesSize)
        {
            return bytesSize / 1024f / 1024f;
        }
        
        private void ShowNetworkErrorMessage(string message)
        {
            jPopupManager.Show(new NetworkErrorPopupSettings("Asset Bundles Error", message, onReloadApplicationCallback: OnReloadApplication));

            void OnReloadApplication()
            {
                OnCancelButtonClick();
                jSignalReloadApplication.Dispatch();
            }
        }
    }
}