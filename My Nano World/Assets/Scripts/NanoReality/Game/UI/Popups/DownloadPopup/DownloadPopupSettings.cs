﻿using System;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;

namespace NanoReality.Game.UI
{
    public class DownloadPopupSettings : BasePopupSettings
    {
        public override Type PopupType => typeof(DownloadPopupView);
    }
}