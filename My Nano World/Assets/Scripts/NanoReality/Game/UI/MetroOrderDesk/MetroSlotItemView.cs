using System;
using DG.Tweening;
using NanoLib.Core.Logging;
using NanoReality.Core.Resources;
using NanoReality.GameLogic.OrderDesk;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using strange.extensions.mediation.impl;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.UI
{
    public sealed class MetroSlotItemView : View
    {
        [SerializeField] private Image _productIconImage;
        [SerializeField] private TextMeshProUGUI _productsCountText;
        [SerializeField] private Image _counterGreenImage;
        [SerializeField] private Image _counterRedImage;

        [SerializeField] private GameObject _notLoadedState;
        [SerializeField] private GameObject _loadedState;
        
        [SerializeField] private Button _slotButton;
        [SerializeField] private RectTransform _animatedDoor;
        [SerializeField] private float _doorOpenedHeight;
        [SerializeField] private float _doorClosedHeight;
        [SerializeField] private Sprite _goldIconSprite;
        
        public MetroSlotItemModel Model { get; } = new MetroSlotItemModel();

        private Tween _doorAnimationTween;
        private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();
        public Button SlotButton => _slotButton;

        #region Inject
        
        [Inject] public IResourcesManager jResourcesManager { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }

        #endregion
        
        private void OnEnable()
        {
            Model.Clicked.BindTo(_slotButton).AddTo(_compositeDisposable);
            Model.IsFilled.Subscribe(OnSlotFillChanged).AddTo(_compositeDisposable);
            Model.ProductData.Subscribe(OnProductDataChanged).AddTo(_compositeDisposable);
            Model.OpenDoorCommand.Subscribe(OnOpenCommandExecute).AddTo(_compositeDisposable);
            Model.OpenDoorStatus.Subscribe(OnOpenDoorStatusChange).AddTo(_compositeDisposable);
            Model.Interactable.SubscribeToInteractable(_slotButton).AddTo(_compositeDisposable);

            transform.ObserveEveryValueChanged(x => x.position)
                .Subscribe(newPosition => { Model.WorldPosition.SetValueAndForceNotify(newPosition); })
                .AddTo(_compositeDisposable);
        }

        private void OnDisable()
        {
            _compositeDisposable.Clear();
            _doorAnimationTween?.Kill();
        }

        private void OnProductDataChanged(ProductData data)
        {
            if (data == default)
            {
                _productIconImage.sprite = null;
                _productsCountText.text = string.Empty;
                _counterRedImage.enabled = false;
                _counterGreenImage.enabled = false;
                return;
            }

            if (Model.IsRewardItem)
            {
                _productIconImage.sprite = _goldIconSprite;
                _productsCountText.text = Model.ProductData.Value.Count.ToString();
                _counterRedImage.enabled = false;
                _counterGreenImage.enabled = true;
                return;
            }
            
            _productsCountText.text = Model.ProductData.Value.Count.ToString();
            _productIconImage.sprite = jResourcesManager.GetProductSprite(data.ProductId);

            bool isProductsEnough = jPlayerProfile.PlayerResources.HasEnoughProduct(data.ProductId, data.Count);
            _counterRedImage.enabled = !isProductsEnough;
            _counterGreenImage.enabled = isProductsEnough;

        }

        private void OnSlotFillChanged(bool isFilled)
        {
            _notLoadedState.gameObject.SetActive(!isFilled);
            _loadedState.gameObject.SetActive(isFilled);
        }

        private void PlayOpenDoor(Action onComplete = null)
        {
            Rect doorRect = _animatedDoor.rect;
            float duration = 1.0f;
            Vector2 doorSizeDelta = new Vector2(doorRect.width, _doorOpenedHeight);
            
            _doorAnimationTween?.Kill();
            _doorAnimationTween = _animatedDoor.DOSizeDelta(doorSizeDelta, duration).SetEase(Ease.InOutSine).OnComplete(
                () => { onComplete?.Invoke(); });
        }
        
        private void PlayCloseDoor(Action onComplete = null)
        {
            Rect doorRect = _animatedDoor.rect;
            float duration = 1.0f;
            Vector2 doorSizeDelta = new Vector2(doorRect.width, _doorClosedHeight);
            
            _doorAnimationTween?.Kill();
            _doorAnimationTween = _animatedDoor.DOSizeDelta(doorSizeDelta, duration).SetEase(Ease.InOutSine).OnComplete(
                () => { onComplete?.Invoke(); });
        }
        
        private void OnOpenCommandExecute(bool isOpen)
        {
            Model.IsNotPlaying.SetValueAndForceNotify(true);
            Model.Interactable.SetValueAndForceNotify(false);
            
            void OnAnimationCompleteCallback()
            {
                Model.OpenDoorStatus.SetValueAndForceNotify(isOpen);
                
                if (isOpen)
                {
                    Model.Interactable.SetValueAndForceNotify(!Model.IsFilled.Value);
                }

                Logging.Log(LoggingChannel.Mechanics, "OnOpenCommandExecute SetSlotsInteractable IsNotFilled: " + !Model.IsFilled.Value + " Index: " + Model.Id);
            }
            
            if (isOpen)
            {
                PlayOpenDoor(OnAnimationCompleteCallback);
            }
            else
            {
                PlayCloseDoor(OnAnimationCompleteCallback);
            }
        }

        private void OnOpenDoorStatusChange(bool isOpen)
        {
            if (!isOpen)
            {
                Rect doorRect = _animatedDoor.rect;
                Vector2 doorSizeDelta = new Vector2(doorRect.width, _doorClosedHeight);
                _animatedDoor.sizeDelta = doorSizeDelta;
            }
        }
    }

    public sealed class MetroSlotItemModel
    {
        public readonly ReactiveCommand Clicked;
        public readonly ReactiveProperty<bool> Interactable;
        public readonly Vector3ReactiveProperty WorldPosition;
                
        public readonly ReactiveProperty<ProductData> ProductData;
        public readonly ReactiveProperty<bool> IsFilled;
        
        public readonly ReactiveCommand<bool> OpenDoorCommand;
        public readonly ReactiveProperty<bool> IsNotPlaying;

        public readonly ReactiveProperty<bool> OpenDoorStatus;
        public int Id;
        public bool IsEmpty;
        public bool IsRewardItem;

        public bool IsOpened => OpenDoorStatus.Value;

        public bool IsClosed => !OpenDoorStatus.Value;

        public MetroSlotItemModel()
        {
            Clicked = new ReactiveCommand();
            Interactable = new ReactiveProperty<bool>();
            
            ProductData = new ReactiveProperty<ProductData>();
            IsFilled = new ReactiveProperty<bool>();
            WorldPosition = new Vector3ReactiveProperty();

            IsNotPlaying = new ReactiveProperty<bool>(true);
            // Execute possibility of OpenDoorCommand is controlling by IsPlaying ReactiveProperty.
            OpenDoorCommand = new ReactiveCommand<bool>(IsNotPlaying);
            
            OpenDoorStatus = new ReactiveProperty<bool>(false);
        }

        public void SetEmpty()
        {
            IsEmpty = true;
            IsRewardItem = false;
            Id = 0;
            ProductData.Value = default;
            IsFilled.Value = false;
        }
    }
}