using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.UI.Core;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.UI.Views.Hud;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using NanoLib.Core.Services.Sound;
using NanoLib.Core.StateMachine;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.OrderDesk;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using strange.extensions.mediation.impl;
using UniRx;
using UnityEngine;

namespace NanoReality.Game.UI
{
    public sealed class MetroPanelMediator : UIMediator<MetroOrderDeskPanelView>
    {
        [Inject] public MetroPanelContext Context { get; set; }
        [Inject] public OnHudBottomPanelIsVisibleSignal jOnHudBottomPanelIsVisibleSignal { get; set; }

        private IEnumerable<MetroTrainView> OrderTrainViews => View.OrderTrainViews;

        private readonly Dictionary<MetroOrderTrainModel, IStateMachine> _trainStateMachines = new Dictionary<MetroOrderTrainModel, IStateMachine>();

        private bool _initialized;

        [Deconstruct]
        public void Deconstruct()
        {
            foreach (KeyValuePair<MetroOrderTrainModel, IStateMachine> keyValuePair in _trainStateMachines)
            {
                keyValuePair.Value.Dispose();
            }

            _trainStateMachines.Clear();
        }

        protected override void OnShown()
        {
            base.OnShown();
            
            if (Context.MetroOrderDeskService.IsMetroLocked)
            {
                Hide();
                jOnHudBottomPanelIsVisibleSignal.Dispatch(true);
                return;
            }

            if (!_initialized)
            {
                InitializeStateMachine();
                _initialized = true;
            }

            jSoundManager.Play(SfxSoundTypes.MetroOpen, SoundChannel.SoundFX);
            List<NetworkMetroOrderTrain> networkTrains = Context.MetroOrderDeskService.MetroOrderDesk.OrderTrains;

            int trainCounter = 0;
            foreach (MetroTrainView trainView in OrderTrainViews)
            {
                MetroOrderTrainModel trainModel = trainView.Model;
                if (trainCounter >= networkTrains.Count)
                {
                    trainModel.SetEmpty();
                    trainCounter++;
                    continue;
                }

                NetworkMetroOrderTrain train = networkTrains[trainCounter];
                trainModel.ParseNetworkModel(train);
                trainModel.ShuffleSkipPrice.SetValueAndForceNotify(Context.MetroConfig.ShuffleSkipPrice);
                trainCounter++;
            }

            foreach (KeyValuePair<MetroOrderTrainModel, IStateMachine> keyValue in _trainStateMachines)
            {
                keyValue.Value.Start();
            }

            View.BalancePanelView.Init(Context.BalancePanelModel);
            Context.BalancePanelModel.TempBalancePanel.Value = View.BalancePanelView;
            SetVisibleHudButtons(false);
        }

        protected override void OnHidden()
        {
            base.OnHidden();
            
            jSoundManager.Play(SfxSoundTypes.CloseWindow, SoundChannel.SoundFX);
            
            foreach (KeyValuePair<MetroOrderTrainModel, IStateMachine> keyValue in _trainStateMachines)
            {
                var trainStateMachine = keyValue.Value;
                var trainModel = keyValue.Key;
                trainStateMachine.Stop();
                trainModel.SetEmpty();
            }

            SetVisibleHudButtons(true);
            View.BalancePanelView.UnsubscribeBalancePanel();
            Context.BalancePanelModel.TempBalancePanel.Value = null;
            jOnHudBottomPanelIsVisibleSignal.Dispatch(true);
        }

        private void InitializeStateMachine()
        {
            foreach (MetroTrainView metroTrainView in OrderTrainViews)
            {
                IStateMachine trainStateMachine = new StateMachine();
                MetroTrainStateArgs stateArgs = new MetroTrainStateArgs(metroTrainView, Context);
                
                InitializeMetroTrainState initializeState = new InitializeMetroTrainState(trainStateMachine, stateArgs);
                IdleMetroTrainState idleState = new IdleMetroTrainState(trainStateMachine, stateArgs);
                ShippingMetroTrainState shippingState = new ShippingMetroTrainState(trainStateMachine, stateArgs);
                RewardMetroTrainState rewardState = new RewardMetroTrainState(trainStateMachine, stateArgs);
                WaitingMetroTrainState waitingOrderState = new WaitingMetroTrainState(trainStateMachine, stateArgs);
                NotAvailableMetroTrainState notAvailableMetroTrainState = new NotAvailableMetroTrainState(trainStateMachine, stateArgs);
                ShuffleMetroTrainState shuffleState = new ShuffleMetroTrainState(trainStateMachine, stateArgs);
                ExpiredMetroTrainState expiredState = new ExpiredMetroTrainState(trainStateMachine, stateArgs);

                initializeState.AddNewTransition("Idle", idleState)
                    .AddNewTransition("Shipping", shippingState)
                    .AddNewTransition("Reward", rewardState)
                    .AddNewTransition("Waiting", waitingOrderState)
                    .AddNewTransition("NotAvailable", notAvailableMetroTrainState)
                    .AddNewTransition("Expired", expiredState)
                    .AddNewTransition("Shuffle", shuffleState);
                
                idleState.AddNewTransition("Reward", rewardState);
                idleState.AddNewTransition("Shuffle", shuffleState);
                shuffleState.AddNewTransition("Idle", idleState);
                rewardState.AddNewTransition("Shipping", shippingState);
                shippingState.AddNewTransition("Waiting", waitingOrderState);
                waitingOrderState.AddNewTransition("Idle", idleState);
                notAvailableMetroTrainState.AddNewTransition("Idle", idleState);
                expiredState.AddNewTransition("Idle", idleState);

                trainStateMachine.AddNewState(initializeState)
                    .AddNewState(idleState)
                    .AddNewState(shippingState)
                    .AddNewState(rewardState)
                    .AddNewState(waitingOrderState)
                    .AddNewState(notAvailableMetroTrainState);

                _trainStateMachines.Add(metroTrainView.Model, trainStateMachine);
            }
        }

        private void SetVisibleHudButtons(bool isVisible)
        {
            Context.BalancePanelModel.HappinessModelActive.SetValueAndForceNotify(isVisible);
            Context.BalancePanelModel.PopulationHappinessModelActive.SetValueAndForceNotify(isVisible);
            Context.BalancePanelModel.FoodSupplyPanelActive.SetValueAndForceNotify(isVisible);
        }
    }
}