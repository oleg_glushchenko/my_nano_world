using System.Collections.Generic;
using System.Linq;
using NanoReality.GameLogic.OrderDesk;
using UniRx;
using UnityEngine;

namespace NanoReality.Game.UI
{
    public sealed class MetroOrderTrainModel
    {
        public int Id;

        public ProductData Reward;
        
        public readonly ReactiveProperty<bool> IsEmpty = new ReactiveProperty<bool>();
        public readonly ReactiveProperty<int> ShuffleSkipPrice = new ReactiveProperty<int>();

        public readonly ReactiveProperty<int> WaitingSkipPrice = new ReactiveProperty<int>();
        public readonly ReactiveProperty<bool> SkipWaitingInteractable = new ReactiveProperty<bool>();
        public readonly ReactiveProperty<bool> SkipShuffleInteractable = new ReactiveProperty<bool>();
        
        public long ShuffleStartTime;
        public long ShuffleEndTime;
        public readonly ReactiveProperty<int> ShuffleLeftTime = new ReactiveProperty<int>();
        
        public long WaitingStartTime;
        public long WaitingEndTime;
        public readonly ReactiveProperty<int> WaitingLeftTime = new ReactiveProperty<int>();
        public readonly ReactiveProperty<bool> WaitingActive = new ReactiveProperty<bool>();
        public readonly ReactiveProperty<bool> ShuffleWaitPanelActive = new ReactiveProperty<bool>();

        public readonly ReactiveProperty<IEnumerable<MetroSlotItemModel>> Slots = new ReactiveProperty<IEnumerable<MetroSlotItemModel>>();
        
        public readonly ReactiveCommand ShuffleClicked = new ReactiveCommand();
        public readonly ReactiveProperty<bool> ShuffleVisible = new ReactiveProperty<bool>();
        public readonly ReactiveProperty<bool> ShuffleInteractable = new ReactiveProperty<bool>();
        public readonly ReactiveCommand SkipWaitingClicked = new ReactiveCommand();
        public readonly ReactiveCommand SkipShuffleByCurrencyClicked = new ReactiveCommand();
        
        public readonly ReactiveProperty<bool> IsVisible = new ReactiveProperty<bool>();

        public bool AllSlotsLoaded => Slots.Value.All(c => c.IsEmpty || c.IsFilled.Value);

        public bool IsWaiting => WaitingLeftTime.Value > 0;
        
        public MetroOrderTrainModel(IEnumerable<MetroSlotItemModel> slotModels)
        {
            Slots.Value = slotModels;
        }

        public MetroSlotItemModel FindSlot(int slotId)
        {
            return Slots.Value?.FirstOrDefault(c => c.Id == slotId);
        }

        public bool ContainsSlot(int id)
        {
            if (id == default)
            {
                return false;
            }
            
            if (Slots.Value == null)
            {
                return false;
            }
            
            if (Slots.Value.All(c => c.Id != id))
            {
                return false;
            }
            
            return true;
        }

        public void SetEmpty()
        {
            Id = 0;
            IsEmpty.Value = true;
            WaitingStartTime = default;
            WaitingSkipPrice.Value = default;
            ShuffleSkipPrice.Value = default;
            Reward = default;
            WaitingLeftTime.Value = default;
            WaitingStartTime = default;
            WaitingEndTime = default;
            ShuffleStartTime = default;

            foreach (MetroSlotItemModel slotModel in Slots.Value)
            {
                slotModel.SetEmpty();
            }
        }

        /// <summary>
        /// Is it a last not loaded slot.
        /// </summary>
        /// <param name="slotId">Last slot id</param>
        /// <returns></returns>
        public bool IsLastSlot(int slotId)
        {
            if (slotId == default)
            {
                return false;
            }
            
            if (Slots.Value == null)
            {
                return false;
            }
            
            return Slots.Value
                .Where(c => c.Id != slotId && !c.IsEmpty)
                .All(c => c.IsFilled.Value);
        }

        public void ParseNetworkModel(NetworkMetroOrderTrain networkModel)
        {
            Id = networkModel.Id;
            Reward = networkModel.Reward;
            WaitingSkipPrice.Value = networkModel.SkipPrice;
            
            ShuffleStartTime = networkModel.ShuffleStartTime;
            ShuffleEndTime = networkModel.ShuffleEndTime;

            WaitingStartTime = networkModel.WaitingStartTime;
            WaitingEndTime = networkModel.WaitingEndTime;
            WaitingLeftTime.Value = networkModel.LeftTime;
            IsEmpty.Value = false;

            List<NetworkOrderSlot> networkSlots = networkModel.OrderSlots;
            IEnumerable<MetroSlotItemModel> slotItemsModels = Slots.Value;

            int slotCounter = 0;
            foreach (MetroSlotItemModel slotItemModel in slotItemsModels)
            {
                if (networkSlots == null || networkSlots.Count <= slotCounter || networkSlots[slotCounter] == null)
                {
                    slotItemModel.IsEmpty = true;
                    slotCounter++;
                    continue;
                }

                NetworkOrderSlot networkSlot = networkSlots[slotCounter];
                slotItemModel.Id = networkSlot.Id;
                slotItemModel.IsFilled.Value = networkSlot.IsFilled;
                slotItemModel.ProductData.Value = new ProductData(networkSlot.ProductId, networkSlot.Count);
                
                slotItemModel.IsEmpty = false;

                slotCounter++;
            }
        }
    }
}