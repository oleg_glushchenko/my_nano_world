using System.Collections.Generic;
using Assets.NanoLib.UI.Core.Views;
using NanoReality.Engine.UI.Views.Panels;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.UI
{
    public sealed class MetroOrderDeskPanelView : UIPanelView
    {
        [SerializeField] private Button _closeButton;
        [SerializeField] private List<MetroTrainView> _orderTrainViews;
        [SerializeField] private BalancePanelView _balancePanelView;

        public IEnumerable<MetroTrainView> OrderTrainViews => _orderTrainViews;
        public Button CloseButton => _closeButton;
        public BalancePanelView BalancePanelView => _balancePanelView;

        private void OnEnable()
        {
            _closeButton.onClick.AddListener(Hide);
            
            // TODO: hotfix. by some reason, unity not save order of trains in prefab 0_0.
            for (int i = 0; i < _orderTrainViews.Count; i++)
            {
                MetroTrainView trainView = _orderTrainViews[i];
                trainView.transform.SetSiblingIndex(i);
            }
        }

        private void OnDisable()
        {
            _closeButton.onClick.RemoveListener(Hide);
        }
    }
}