using System;
using System.Linq;
using Engine.UI;
using NanoLib.Core.Services.Sound;
using NanoLib.Core.StateMachine;
using NanoReality.GameLogic.PlayerProfile.Models.Impl;
using UniRx;
using UnityEngine;

namespace NanoReality.Game.UI
{
    public sealed class RewardMetroTrainState : MetroTrainState
    {
        private IDisposable _onCollectClicked;

        public RewardMetroTrainState(IStateMachine parent, StateArgs args) : base(parent, args)
        {
        }

        public override void OnStateEnter()
        {
            var firstSlot = TrainModel.Slots.Value.First();
            
            if (firstSlot.IsRewardItem)
            {
                firstSlot.IsFilled.SetValueAndForceNotify(false);
                firstSlot.IsRewardItem = true;
                firstSlot.Interactable.SetValueAndForceNotify(true);
                firstSlot.ProductData.SetValueAndForceNotify(TrainModel.Reward);
                _onCollectClicked = firstSlot.Clicked.Subscribe(OnCollectRewardClicked);
                
                if (!firstSlot.IsOpened)
                {
                    firstSlot.OpenDoorCommand.Execute(true);
                }
                
                return;
            }
                
            int counter = 0;
            foreach (MetroSlotItemModel slotItemModel in TrainModel.Slots.Value)
            {
                // We need show reward in first train slot. Other slots must be an empty.
                if (counter == 0)
                {
                    slotItemModel.IsFilled.SetValueAndForceNotify(false);
                    slotItemModel.IsRewardItem = true;
                    slotItemModel.Interactable.SetValueAndForceNotify(true);
                    slotItemModel.ProductData.SetValueAndForceNotify(TrainModel.Reward);
                    _onCollectClicked = slotItemModel.Clicked.Subscribe(OnCollectRewardClicked);
                }
                else
                {
                    slotItemModel.SetEmpty();
                }

                slotItemModel.OpenDoorCommand.Execute(false);
                counter++;
            }

            TrainModel.ShuffleVisible.SetValueAndForceNotify(false);

            var showWaitingSubject = TrainView.PlayShowAnimation();
            showWaitingSubject.Subscribe((_) => ShowRewardTrain());
        }

        private void ShowRewardTrain()
        {
            Context.jSoundManager.Play(SfxSoundTypes.TrainSended, SoundChannel.SoundFX);
            foreach (MetroSlotItemModel slotModel in TrainModel.Slots.Value)
            {
                if (slotModel.IsRewardItem)
                {
                    slotModel.Interactable.SetValueAndForceNotify(true);
                }
                slotModel.OpenDoorCommand.Execute(!slotModel.IsEmpty);
            }
        }

        public override void OnStateExit()
        {
            _onCollectClicked?.Dispose();
            _onCollectClicked = null;
        }

        private void OnCollectRewardClicked(Unit unit)
        {
            PlayTrainDoorsAnimation(false, (doorModel) => doorModel != null);
            
            MetroSlotItemModel rewardSlotModel = TrainModel.Slots.Value.First();
            rewardSlotModel.IsRewardItem = false;

            Vector3 startPosition = Env.MainCamera.ScreenToWorldPoint(rewardSlotModel.WorldPosition.Value);
            int productCount = TrainModel.Reward.Count;

            var addItemSettings = new AddItemSettings(FlyDestinationType.GoldCurrency, startPosition, productCount.ToString());
            Context.jAddRewardItemSignal.Dispatch(addItemSettings);
            Context.PlayerProfile.PlayerResources.AddGoldCurrency(productCount);
            Context.jAnalytics.OnPlayerCurrenciesIncrease(PlayerResources.CurencyType.Gold, productCount, AwardSourcePlace.MetroReward, TrainModel.Id.ToString());
            Context.jSoundManager.Play(SfxSoundTypes.CrateLoaded, SoundChannel.SoundFX);

            _onCollectClicked.Dispose();
            PushTransitionEvent("Shipping");
        }
    }
}