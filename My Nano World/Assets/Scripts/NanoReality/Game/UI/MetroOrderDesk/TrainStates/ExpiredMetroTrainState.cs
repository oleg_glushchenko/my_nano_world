using NanoLib.Core.Services.Sound;
using NanoLib.Core.StateMachine;
using UniRx;

namespace NanoReality.Game.UI
{
    public sealed class ExpiredMetroTrainState : MetroTrainState
    {
        private bool _responseReceived;
        private bool _animationFinished;

        public ExpiredMetroTrainState(IStateMachine parent, StateArgs args) : base(parent, args)
        {
        }

        public override void OnStateEnter()
        {
            var updateOrderDeskAsyncSubject = new AsyncSubject<Unit>();
            updateOrderDeskAsyncSubject.OnNext(Unit.Default);

            MetroService.UpdateOrderDesk(updateOrderDeskAsyncSubject.OnCompleted);
            var playShowAnimationAsyncSubject = TrainView.PlayShowAnimation();
            
            TrainModel.WaitingActive.SetValueAndForceNotify(false);
            TrainModel.ShuffleWaitPanelActive.SetValueAndForceNotify(false);
            
            Observable.WhenAll(updateOrderDeskAsyncSubject, playShowAnimationAsyncSubject)
                .Subscribe((_) => CheckForFinishState());
        }

        private void CheckForFinishState()
        {
            Context.jSoundManager.Play(SfxSoundTypes.TrainSended, SoundChannel.SoundFX);
            TrainModel.ParseNetworkModel(MetroService.MetroOrderDesk.FindTrain(TrainModel.Id));
            PlayTrainDoorsAnimation(true, door => !door.IsEmpty, () => { PushTransitionEvent("Idle"); });
        }
    }
}