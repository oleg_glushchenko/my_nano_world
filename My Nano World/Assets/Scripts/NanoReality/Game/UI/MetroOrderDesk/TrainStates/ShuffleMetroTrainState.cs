using System;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using NanoLib.Core.Services.Sound;
using NanoLib.Core.StateMachine;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.OrderDesk;
using UniRx;
using UnityEngine;

namespace NanoReality.Game.UI
{
    public sealed class ShuffleMetroTrainState : MetroTrainState
    {
        private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();
        private AsyncSubject<Unit> _updateOrderDeskAsyncSubject = new AsyncSubject<Unit>();
        private Action _onFinishState;

        private int SkipShufflePrice
        {
            get
            {
                int leftTime = CalculateShuffleLeftTime();
                int clampedTime = Mathf.Clamp(leftTime, 0, int.MaxValue);
                return CalculateSkipWaitingPrice(clampedTime);
            }
        }

        public ShuffleMetroTrainState(IStateMachine parent, StateArgs args) : base(parent, args)
        {
        }

        public override void OnStateEnter()
        {
            base.OnStateEnter();

            _updateOrderDeskAsyncSubject.OnNext(Unit.Default);
            _onFinishState += FinalizeShuffleWaiting;

            TrainModel.SkipShuffleByCurrencyClicked.Subscribe(OnSkipByCurrencyClicked).AddTo(_compositeDisposable);

            TrainModel.ShuffleVisible.SetValueAndForceNotify(false);
            TrainModel.ShuffleInteractable.SetValueAndForceNotify(false);

            Context.jAdsService.VideoWatched += OnVideoWatched;
            Context.jAdsService.VideoSkipped += OnVideoSkipped;

            if (TrainModel.ShuffleEndTime == default)
            {
                PlayTrainDoorsAnimation(false, door => !door.IsEmpty, OnDoorClosed);

                MetroService.ShuffleOrder(TrainModel.Id,
                    () => { Context.jMetroOrderDeskUpdatedSignal.AddListener(OnMetroOrderDeskUpdated); });

                void OnDoorClosed()
                {
                    var showWaitingSubject = TrainView.PlayHideAnimation();
                    showWaitingSubject.Subscribe((_) => ShowWaitingPanel());
                }
            }
            else
            {
                Context.jMetroOrderDeskUpdatedSignal.AddListener(OnMetroOrderDeskUpdated); 
                TrainView.SetHidden();
                ShowWaitingPanel();
            }
        }

        public override void OnStateExit()
        {
            base.OnStateExit();
            _onFinishState -= FinalizeShuffleWaiting;
            TrainModel.ShuffleLeftTime.Value = default;
            SetVisibleShuffleWaitPanel(false);
            Context.jServerTimeTickSignal.RemoveListener(OnServerTick);
            _compositeDisposable.Clear();
            Context.jMetroOrderDeskUpdatedSignal.RemoveListener(OnMetroOrderDeskUpdated);
            
            Context.jAdsService.VideoWatched -= OnVideoWatched;
            Context.jAdsService.VideoSkipped -= OnVideoSkipped;
        }

        private void ShowWaitingPanel()
        {
            UpdateTrainState();

            SetVisibleShuffleWaitPanel(true);

            int shuffleLeftTime = CalculateShuffleLeftTime();
            TrainModel.ShuffleLeftTime.SetValueAndForceNotify(shuffleLeftTime);
            TrainModel.ShuffleSkipPrice.SetValueAndForceNotify(CalculateSkipWaitingPrice(shuffleLeftTime));

            Context.jServerTimeTickSignal.AddListener(OnServerTick);
        }

        private void OnServerTick(long serverTime)
        {
            int leftTime = CalculateShuffleLeftTime();

            TrainModel.ShuffleLeftTime.SetValueAndForceNotify(leftTime);
            TrainModel.ShuffleSkipPrice.SetValueAndForceNotify(SkipShufflePrice);

            if (leftTime >= 0)
            {
                return;
            }

            Context.jServerTimeTickSignal.RemoveListener(OnServerTick);
            SetVisibleShuffleWaitPanel(false);
            _onFinishState?.Invoke();
        }

        private void OnSkipByCurrencyClicked(Unit unit)
        {
            int skipShufflePrice = SkipShufflePrice;
            
            void ConfirmSkip()
            {
                Context.jServerTimeTickSignal.RemoveListener(OnServerTick);
                SetVisibleShuffleWaitPanel(false);
                MetroService.SkipShuffleOrder(TrainModel.Id, SkipShufflePrice, _onFinishState);
            }
            
            if (skipShufflePrice <= 0)
            {
                ConfirmSkip();
                return;
            }

            if (Context.PlayerResources.HardCurrency < skipShufflePrice)
            {
                Context.PopupManager.Show(new NotEnoughMoneyPopupSettings(PriceType.Hard, skipShufflePrice));
                return;
            }
            
            var popupSettings = new PurchaseByPremiumPopupSettings(skipShufflePrice);
            
            void UpdatePrice(long _)
            {
                popupSettings?.InvokePriceChangeAction(SkipShufflePrice);
            }
            
            Context.jServerTimeTickSignal.AddListener(UpdatePrice);
                        
            Context.PopupManager.Show(popupSettings, result => 
            {
                Context.jServerTimeTickSignal.RemoveListener(UpdatePrice);
                                
                if (result == PopupResult.Ok)
                {
                    ConfirmSkip();
                }
            });
        }

        private void OnVideoSkipped(object sender, EventArgs e)
        {
            Context.jServerTimeTickSignal.AddListener(OnServerTick);
        }

        private void OnVideoWatched(object sender, EventArgs e)
        {
            Context.jServerTimeTickSignal.RemoveListener(OnServerTick);
            SetVisibleShuffleWaitPanel(false);
            MetroService.SkipShuffleOrder(TrainModel.Id, 0, _onFinishState);
        }

        private void FinalizeShuffleWaiting()
        {
            var playShowAnimationAsyncSubject = TrainView.PlayShowAnimation();
            
            void OnShuffledMetroUpdated(Unit onComplete)
            {
                Context.jSoundManager.Play(SfxSoundTypes.TrainSended, SoundChannel.SoundFX);
                UpdateTrainState();
                PlayTrainDoorsAnimation(true, (doorModel) => !doorModel.IsEmpty,
                    () => { PushTransitionEvent("Idle"); });
            }

            Observable.WhenAll(playShowAnimationAsyncSubject, _updateOrderDeskAsyncSubject).Subscribe(OnShuffledMetroUpdated);
        }

        private void SetVisibleShuffleWaitPanel(bool isVisible)
        {
            TrainModel.SkipShuffleInteractable.SetValueAndForceNotify(isVisible);
            TrainModel.ShuffleWaitPanelActive.SetValueAndForceNotify(isVisible);
        }

        private void UpdateTrainState()
        {
            NetworkMetroOrderTrain networkMetroOrderTrain = MetroService.MetroOrderDesk.FindTrain(TrainModel.Id);
            TrainModel.ParseNetworkModel(networkMetroOrderTrain);
        }

        private int CalculateShuffleLeftTime()
        {
            return Convert.ToInt32(TrainModel.ShuffleEndTime - Context.jTimerManager.CurrentUTC);
        }

        private void OnMetroOrderDeskUpdated(NetworkMetroOrderDesk _)
        {
            _updateOrderDeskAsyncSubject.OnCompleted();
        }
    }
}
