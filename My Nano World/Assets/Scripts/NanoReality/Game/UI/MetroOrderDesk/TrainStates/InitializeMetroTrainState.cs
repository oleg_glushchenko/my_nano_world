using NanoLib.Core.StateMachine;

namespace NanoReality.Game.UI
{
    public sealed class NotAvailableMetroTrainState : MetroTrainState
    {
        public NotAvailableMetroTrainState(IStateMachine parent, StateArgs args) : base(parent, args)
        {
        }

        public override void OnStateEnter()
        {
        }

        public override void OnStateExit()
        {
        }
    }
    
    public sealed class InitializeMetroTrainState : MetroTrainState
    {
        public InitializeMetroTrainState(IStateMachine parent, StateArgs args) : base(parent, args)
        {
        }

        public override void OnStateEnter()
        {
            foreach (var slot in TrainModel.Slots.Value)
            {
                slot.OpenDoorStatus.SetValueAndForceNotify(false);
            }
            
            if (TrainModel.IsEmpty.Value)
            {
                PushTransitionEvent("NotAvailable");
                return;
            }
            
            bool waitNewOrderExpired = TrainModel.WaitingEndTime != 0 && TrainModel.WaitingEndTime <= Context.jTimerManager.CurrentUTC;
            bool waitShuffleOrderExpired = TrainModel.ShuffleEndTime != 0 && TrainModel.ShuffleEndTime < Context.jTimerManager.CurrentUTC;
            if (waitNewOrderExpired || waitShuffleOrderExpired)
            {
                PushTransitionEvent("Expired");
                return;
            }

            if (TrainModel.IsWaiting)
            {
                PushTransitionEvent("Waiting");
                return;
            }


            if (TrainModel.ShuffleEndTime > 0)
            {
                PushTransitionEvent("Shuffle");
                return;
            }

            if (TrainModel.AllSlotsLoaded)
            {
                PushTransitionEvent("Reward");
                return;
            }
            
            PlayTrainDoorsAnimation(true, (doorModel) => !doorModel.IsEmpty,
                () => { PushTransitionEvent("Idle"); });

        }

        public override void OnStateExit()
        {
            
        }
    }
}