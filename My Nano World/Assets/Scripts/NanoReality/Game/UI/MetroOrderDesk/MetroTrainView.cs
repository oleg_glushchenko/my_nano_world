using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using NanoReality.Core.UI;
using NanoReality.GameLogic.Utilites;
using strange.extensions.mediation.impl;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.UI
{
    public sealed class MetroTrainView : View
    {
        [SerializeField] private TextButton _shuffleSlotsButton;
        [SerializeField] private CanvasGroup _shufflePanelCanvasGroup;

        [SerializeField] private List<MetroSlotItemView> _orderSlotsViews;
        
        [SerializeField] private Animator _truckAnimator;
        [SerializeField] private GameObject _notAvailablePanel;

        [Header("Wait New Order State")] 
        [SerializeField] private CanvasGroup _waitingPanelCanvasGroup;
        [SerializeField] private TextButton _skipOrderWaitingButton;
        [SerializeField] private TextMeshProUGUI _waitingTimeCounterText;
        [SerializeField] private HorizontalProgressBar _waitingProgressBar;
        
        [Header("Wait Shuffle Order State")] 
        [SerializeField] private CanvasGroup _shuffleStateCanvasGroup;
        [SerializeField] private TextButton _skipShuffleByCurrencyButton;
        [SerializeField] private TextButton _skipShuffleByAdButton;
        [SerializeField] private TextMeshProUGUI _waitingShuffleTimeCounterText;
        [SerializeField] private HorizontalProgressBar _waitingShuffleProgressBar;

        public MetroOrderTrainModel Model { get; private set; } 

        public IEnumerable<MetroSlotItemView> OrderSlotViews => _orderSlotsViews;
        
        private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();
        private Tween _shuffleFadeTween;
        private Tween _shuffleAnimationTween;
        private Tween _truckAnimationTween;
        private Tween _waitingPanelFadeTween;

        protected override void Awake()
        {
            base.Awake();
            Model = new MetroOrderTrainModel(_orderSlotsViews.Select(c => c.Model));
        }

        private void OnEnable()
        {
            Model.IsEmpty.Subscribe(OnAvailableChanged).AddTo(_compositeDisposable);
            
            Model.WaitingLeftTime.Subscribe(OnWaitNewTrainLeftTimeChanged).AddTo(_compositeDisposable);
            Model.WaitingActive.Subscribe(OnWaitingTrainPanelActiveChanged).AddTo(_compositeDisposable);

            Model.SkipWaitingInteractable.SubscribeToInteractable(_skipOrderWaitingButton).AddTo(_compositeDisposable);
            Model.SkipWaitingClicked.BindTo(_skipOrderWaitingButton).AddTo(_compositeDisposable);
            Model.WaitingSkipPrice.Subscribe(OnSkipPriceChanged).AddTo(_compositeDisposable);
            
            Model.ShuffleVisible.ObserveEveryValueChanged(x => x.Value)
                .Subscribe(OnShuffleVisibleChanged).AddTo(_compositeDisposable);
            Model.ShuffleSkipPrice.Subscribe(OnShuffleSkipPriceChanged).AddTo(_compositeDisposable);
            Model.ShuffleInteractable.SubscribeToInteractable(_shuffleSlotsButton).AddTo(_compositeDisposable);

            Model.ShuffleClicked.BindTo(_shuffleSlotsButton).AddTo(_compositeDisposable);
            Model.SkipShuffleByCurrencyClicked.BindTo(_skipShuffleByCurrencyButton).AddTo(_compositeDisposable);
            Model.SkipShuffleInteractable.Subscribe(OnShuffleWaitInteractableChanged).AddTo(_compositeDisposable);
            Model.ShuffleWaitPanelActive.Subscribe(OnShuffleWaitPanelActiveChanged).AddTo(_compositeDisposable);
            Model.ShuffleLeftTime.Subscribe(OnShuffleLeftTimeChanged).AddTo(_compositeDisposable);
            _skipShuffleByAdButton.Text = LocalizationUtils.Localize("WATCH_VIDEO_BUTTON");
        }

        private void OnDisable()
        {
            _compositeDisposable.Clear();
            _shuffleFadeTween?.Kill();
            _waitingPanelFadeTween?.Kill();
            _truckAnimationTween?.Kill();
            ForceHidePanels();
        }

        private void OnShuffleVisibleChanged(bool isVisible)
        {
            _shuffleFadeTween?.Kill();
            
            float alphaValue = isVisible ? 1 : 0;
            float duration = 0.5f;
            _shufflePanelCanvasGroup.blocksRaycasts = isVisible;
            _shuffleFadeTween = _shufflePanelCanvasGroup.DOFade(alphaValue, duration);
        }
        
        private void OnShuffleWaitPanelActiveChanged(bool isActive)
        {
            _shuffleAnimationTween?.Kill();
            
            float targetAlphaValue = isActive ? 1 : 0;
            float animationDuration = 0.5f;

            _shuffleAnimationTween = _shuffleStateCanvasGroup.DOFade(targetAlphaValue, animationDuration);
            _shuffleStateCanvasGroup.blocksRaycasts = isActive;
        }

        private void ForceHidePanels()
        {
            _shuffleStateCanvasGroup.alpha = 0;
            _waitingPanelCanvasGroup.alpha = 0;
            _shuffleStateCanvasGroup.blocksRaycasts = false;
            _waitingPanelCanvasGroup.blocksRaycasts = false;
        }

        private void OnShuffleLeftTimeChanged(int leftTime)
        {
            long totalShuffleWaitingTime = Model.ShuffleEndTime - Model.ShuffleStartTime;
            float progress =  1 - (float)leftTime /  totalShuffleWaitingTime;
            _waitingShuffleProgressBar.FillAmount = progress;
            _waitingShuffleTimeCounterText.text = UiUtilities.GetFormattedTimeFromSeconds_M_S(leftTime);
        }

        private void OnShuffleWaitInteractableChanged(bool interactable)
        {
            _shuffleStateCanvasGroup.interactable = interactable;
        }

        private void OnShuffleSkipPriceChanged(int shufflePrice)
        {
            _skipShuffleByCurrencyButton.Text = shufflePrice <= 0
                ? LocalizationUtils.Localize("FREE_LABEL")
                : shufflePrice.ToString();
            LayoutRebuilder.ForceRebuildLayoutImmediate(_skipShuffleByCurrencyButton.transform as RectTransform);
        }

        private void OnAvailableChanged(bool isAvailable)
        {
            _notAvailablePanel.gameObject.SetActive(isAvailable);
        }

        private void OnWaitingTrainPanelActiveChanged(bool isActive)
        {
            float targetAlphaValue = isActive ? 1 : 0;
            float animationDuration = 0.5f;
            
            _waitingPanelFadeTween?.Kill();
            _waitingPanelFadeTween = _waitingPanelCanvasGroup.DOFade(targetAlphaValue, animationDuration);
            _waitingPanelCanvasGroup.blocksRaycasts = isActive;
        }

        private void OnWaitNewTrainLeftTimeChanged(int leftTime)
        {
            long totalWaitingTime = Model.WaitingEndTime - Model.WaitingStartTime;
            float progress = 1 - (float)leftTime / totalWaitingTime;
            _waitingProgressBar.FillAmount = progress;
            _waitingTimeCounterText.text = UiUtilities.GetFormattedTimeFromSeconds_M_S(leftTime);
        }

        public AsyncSubject<Unit> PlayHideAnimation()
        {
            var asyncSubject = new AsyncSubject<Unit>();
            asyncSubject.OnNext(Unit.Default);

            ResetTimers();
            _truckAnimator.Play("SendOrder");
            _truckAnimationTween?.Kill();

            _truckAnimationTween = DOVirtual.DelayedCall(3.0f, () => { asyncSubject.OnCompleted(); });

            return asyncSubject;
        }

        public AsyncSubject<Unit> PlayShowAnimation()
        {
            foreach (var slot in Model.Slots.Value)
            {
                slot.OpenDoorStatus.SetValueAndForceNotify(false);
            }
            
            var asyncSubject = new AsyncSubject<Unit>();
            asyncSubject.OnNext(Unit.Default);

            _truckAnimator.Play("ResultArrived");
            _truckAnimationTween?.Kill();
            _truckAnimationTween = DOVirtual.DelayedCall(3.0f, () => asyncSubject.OnCompleted());
            return asyncSubject;
        }

        private void ResetTimers()
        {
            _waitingProgressBar.FillAmount = _waitingShuffleProgressBar.FillAmount = 0;
            _waitingTimeCounterText.text = _waitingShuffleTimeCounterText.text = "";
        }

        public void SetHidden()
        {
            _truckAnimator.Play("CarInTrip");
        }

        private void OnSkipPriceChanged(int skipPrice)
        {
            _skipOrderWaitingButton.Text = skipPrice <= 0
                ? LocalizationUtils.Localize("FREE_LABEL")
                : skipPrice.ToString();
        }
    }
}