using System;
using Assets.NanoLib.UI.Core.Views;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.UI
{
    public class RenameCityPanelView : UIPanelView
    {
        [SerializeField] private Button _okButton;
        [SerializeField] private Button _cancelButton;
        [SerializeField] private TMP_InputField _cityInputField;
        
        private string _cityNameText = String.Empty;
        private string _oldName;

        public Button CancelButton => _cancelButton;
        public string OldName => _oldName;
        public string CityNameText
        {
            get => _cityNameText;
            set => _cityNameText = value;
        }

        public Button OkButton => _okButton;
        public TMP_InputField CityInputField => _cityInputField;

        public override void Initialize(object parameter)
        {
            var cityName = parameter.ToString();
            _oldName = cityName;
            if (cityName == "DefaultName" || string.IsNullOrEmpty(cityName))
            {
                _cityNameText = String.Empty;
                OkButton.interactable = false;
            }
            else
            {
                _cityNameText = cityName;
                OkButton.interactable = true;
            }

            UpdateInputFieldText(_cityNameText);
        }
        
        public void UpdateInputFieldText(string text)
        {
            CityInputField.text = text;
        }
    }
}