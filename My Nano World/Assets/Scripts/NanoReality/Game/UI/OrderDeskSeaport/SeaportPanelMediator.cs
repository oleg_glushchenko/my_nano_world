using System;
using System.Linq;
using Assets.NanoLib.UI.Core;
using Assets.Scripts.Engine.Analytics.Model.api;
using Assets.Scripts.Engine.UI.Views.Hud;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Engine.UI;
using NanoLib.Core.Services.Sound;
using NanoLib.Core.Timers;
using NanoReality.Core.Resources;
using NanoReality.Engine.UI.Extensions.MainPanel;
using NanoReality.Game.Services.OrderDesk.SeaportOrderDesk;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Impl;
using NanoReality.GameLogic.Services;
using NanoReality.GameLogic.OrderDesk;
using NanoReality.UI.Components;
using UniRx;
using UnityEngine;

namespace NanoReality.Game.UI
{
    public sealed class SeaportPanelMediator : UIMediator<SeaportPanelView>, ISeaportStateMachine
    {
        #region Inject
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IAdsService jAdsService { get; set; }
        [Inject] public IResourcesManager jResourcesManager { get; set; }
        [Inject] public AddRewardItemSignal jAddRewardItemSignal { get; set; }
        // TODO: workaround. need replace directly changing of balance panel model on signal.
        [Inject] public BalancePanelModel jBalancePanelModel { get; set; }
        [Inject] public ISkipIntervalsData jSkipIntervalsData { get; set; }
        [Inject] public ISeaportOrderDeskService jSeaportOrderDeskService { get; set; }
        [Inject] public INanoAnalytics jNanoAnalytics { get; set; }
        [Inject] public ServerTimeTickSignal jServerTimeTickSignal { get; set; }
        [Inject] public BuyResoursesActionSignal jResoursesBuySignal { get; set; }
        [Inject] public OnHudBottomPanelIsVisibleSignal jOnHudBottomPanelIsVisibleSignal { get; set; }
        [Inject] public OnHudButtonsIsClickableSignal jOnHudButtonsIsClickableSignal { get; set; }

        #endregion
        
        private Action _callbackWithData;
        private int _selectedSlotPosition;
        private bool _isShuffleProcessing;
        
        public int SelectedSlotPosition => _selectedSlotPosition;
        
        protected override void Show(object param)
        {
            _selectedSlotPosition = jSeaportOrderDeskService.SeaportCollection.First().ServerPosition;
            if (param is Product product)
            {
                foreach (var slot in jSeaportOrderDeskService.SeaportCollection)
                {
                    var serverProductID = slot.Awards.Product.ProductId;
                    if (serverProductID != product.ProductTypeID) continue;
                    _selectedSlotPosition = slot.ServerPosition;
                    break;
                }
            }
            base.Show(param);
        }

        protected override void OnShown()
        {
            base.OnShown();
            
            SetState(new SeaportIdleState(this));
            
            SetOrdersInteractable(true);
            PutDataToOrderSlotViews();
            AddListeners();
            SelectOrder(SelectedSlotPosition);

            View.BalancePanelView.Init(jBalancePanelModel);
            jSoundManager.Play(SfxSoundTypes.SeaportOpen, SoundChannel.SoundFX);
            
            jBalancePanelModel.TempBalancePanel.Value = View.BalancePanelView;
            
            //some logic to set start from state
            if (jSeaportOrderDeskService.GetSlot(SelectedSlotPosition).IsShuffling)
            {
                SetState(new SeaportShuffleState(this));
            }
        }

        protected override void OnHidden()
        {
            base.OnHidden();
            View.BalancePanelView.UnsubscribeBalancePanel();
            RemoveListeners();
            jBalancePanelModel.TempBalancePanel.Value = null;
            jOnHudBottomPanelIsVisibleSignal.Dispatch(true);
            jOnHudButtonsIsClickableSignal.Dispatch(true);
        }

        private void AddListeners()
        {
            for (var i = 0; i < View.OrderItemsList.Count; i++)
            {
                SeaportOrderItemView orderView = View.OrderItemsList[i];
                orderView.ItemSelected += OnOrderSelectClicked;
                orderView.SkipWaitingByPremium = () =>
                {
                    State.Select(orderView.ServerPosition);
                    OnBuyOrderShuffleSkip();
                };

                orderView.SkipWaitingByAdWatch = () =>
                {
                    State.Select(orderView.ServerPosition);
                };

                orderView.ShuffleOrderClick += () =>
                {
                    State.Select(orderView.ServerPosition);
                    ShuffleOrderClick();
                };
            }

            View.OrderStatusPanel.ActionButtonClicked += OnSendButtonClick;

            jAdsService.VideoWatched += OnVideoWatched;
            jAdsService.VideoSkipped += OnVideoSkipped;
            jAdsService.VideoStarted += OnVideoStarted;
        }

        private void RemoveListeners()
        {
            foreach (SeaportOrderItemView orderView in View.OrderItemsList)
            {
                orderView.ItemSelected -= OnOrderSelectClicked;
                orderView.SkipWaitingByPremium = null;
                orderView.SkipWaitingByAdWatch = null;
                orderView.ShuffleOrderClick = null;
                orderView.SetSelected(false);
            }

            View.OrderStatusPanel.ActionButtonClicked -= OnSendButtonClick;
            
            jAdsService.VideoWatched -= OnVideoWatched;
            jAdsService.VideoSkipped -= OnVideoSkipped;
            jAdsService.VideoStarted -= OnVideoStarted;
        }

        #region State logic

        public SeaportState State { get; private set; }
        public void SetState(SeaportState state)
        {
            if(state != null && State != null && State.GetType() == state.GetType()) return;

            State = state;
        }

        private void OnOrderSelectClicked(SeaportOrderItemView seaportOrderItemView)
        {
            jSoundManager.Play(SfxSoundTypes.SeaportCrateChoose, SoundChannel.SoundFX);
            State.Select(seaportOrderItemView.ServerPosition);
        }

        private void OnSendButtonClick()
        {
            State.Send();
        }

        private void ShuffleOrderClick()
        {
            jSoundManager.Play(SfxSoundTypes.MadeShuffle, SoundChannel.SoundFX);
            State.Shuffle();
        }

        private void OnBuyOrderShuffleSkip()
        {
            State.SkipByPremium();
        }

        #endregion State logic

        public void ShuffleOrder()
        {
            int slotPosition = SelectedSlotPosition;
            var item = GetOrderViewByPosition(slotPosition);
            View.OrderStatusPanel.Lock();
            item.Lock();
            jSeaportOrderDeskService.StartShuffleOrder(slotPosition, OnShuffleStarted);
            
            void OnShuffleStarted()
            {
                PutDataToOrderSlotViews();
                var slot = jSeaportOrderDeskService.GetSlot(slotPosition);
                var orderView = GetOrderViewByPosition(slot.ServerPosition);
                RefreshSlotView(slot, orderView);
                item.Unlock();
            }
        }

        public void BuySkipShuffleWaitingTime()
        {
            if (_isShuffleProcessing) return;

            _isShuffleProcessing = true;
            
            var orderSlot = jSeaportOrderDeskService.GetSlot(SelectedSlotPosition);

            int Price()
            {
                return jSkipIntervalsData.GetCurrencyForTime((int) orderSlot.ShuffleTimeLeft.Value);
            }
            
            int price = Price();

            if (orderSlot.FreeSkip > 0 || price <= 0)
            {
                SkipShuffleOrderWaiting();
                return;
            }

            if (jPlayerProfile.PlayerResources.HardCurrency < price)
            {
                jPopupManager.Show(new NotEnoughMoneyPopupSettings(PriceType.Hard,
                    price - jPlayerProfile.PlayerResources.HardCurrency), result =>
                {
                    if (result == PopupResult.Ok)
                    {
                        Hide();
                    }
                });
                return;
            }

            price = Price();
            jPlayerProfile.PlayerResources.BuyHardCurrency(price);
            jNanoAnalytics.OnPlayerCurrenciesDecrease(PlayerResources.CurencyType.Bucks, price,
                CurrenciesSpendingPlace.SkipShuffleSeaport, "skip shuffle");
            SkipShuffleOrderWaiting();
            
            void SkipShuffleOrderWaiting()
            {
                jSeaportOrderDeskService.SkipShuffleOrderWaiting(SelectedSlotPosition, false, () =>
                {
                    ShowAndUpdateStatusPanel(null);
                    string priceInfo = orderSlot.FreeSkip > 0 ? "Free" : price.ToString();
                    jNanoAnalytics.OnShuffle("Seaport", priceInfo, SelectedSlotPosition.ToString());
                    
                    _isShuffleProcessing = false;
                });
            }
        }

        private void OnVideoWatched(object sender, EventArgs e)
        {
            jSeaportOrderDeskService.SkipShuffleOrderWaiting(SelectedSlotPosition, true, () =>
            {
                ShowAndUpdateStatusPanel(null);
                jNanoAnalytics.OnShuffle("Seaport", "Video Watched", SelectedSlotPosition.ToString());
            });
        }

        private void OnVideoSkipped(object sender, EventArgs e)
        {
            SetOrdersInteractable(true);
        }
        
        private void OnVideoStarted(object sender, EventArgs e)
        {
            SetOrdersInteractable(false);
        }

        public void SelectOrder(int orderServerPosition)
        {
            var prevSelectedOrderView = GetOrderViewByPosition(_selectedSlotPosition);
            var newSelectedOrderView = GetOrderViewByPosition(orderServerPosition);
            _selectedSlotPosition = orderServerPosition;
            if (prevSelectedOrderView != null)
            {
                prevSelectedOrderView.SetSelected(false);
            }

            View.OrderStatusPanel.SetOrder(jSeaportOrderDeskService.GetSlot(orderServerPosition));
            newSelectedOrderView.SetSelected(true);
            View.OrderStatusPanel.PlaySelectPanelAnimation();
        }

        public void SendOrder()
        {
            _callbackWithData = null;
            View.OrderStatusPanel.PlayHidePanelAnimation(() =>
            {
                _callbackWithData?.Invoke();
                View.OrderStatusPanel.Unlock();
            });

            GetOrderViewByPosition(SelectedSlotPosition).Lock();
            View.OrderStatusPanel.Lock();
        }

        private void RefreshSlotView(ISeaportOrderSlot orderSlot, SeaportOrderItemView orderView)
        {
            orderView.SetOrder(orderSlot);
            orderView.PlayContentUpdateTweens();

            if (orderView.Selected)
            {
                View.OrderStatusPanel.Unlock();
                View.OrderStatusPanel.PlaySelectPanelAnimation();
                View.OrderStatusPanel.SetOrder(orderSlot);
            }
        }

        public void RefreshCurrentSlot()
        {
            View.OrderStatusPanel.SetOrder(jSeaportOrderDeskService.GetSlot(_selectedSlotPosition));
        }

        private void PutDataToOrderSlotViews()
        {
            foreach (var orderView in View.OrderItemsList)
            {
                var slot = jSeaportOrderDeskService.GetSlot(orderView.ServerPosition);
                if (slot == null)
                {
                    orderView.gameObject.SetActive(false);
                    continue;
                }

                orderView.gameObject.SetActive(true);
                orderView.SetOrder(slot);
            }
        }

        private void ShowAndUpdateStatusPanel(Action animationCallback)
        {
            RefreshCurrentSlot();
            void OnStatusPanelShowedCallback()
            {
                SetOrdersInteractable(true);
                GetOrderViewByPosition(SelectedSlotPosition).Unlock();
                animationCallback?.Invoke();
            }
            
            GetOrderViewByPosition(SelectedSlotPosition).PlayContentUpdateTweens();
            View.OrderStatusPanel.PlayShowPanelAnimation(OnStatusPanelShowedCallback);
        }
        
        public void OnShuffleFinished(CollectionReplaceEvent<ISeaportOrderSlot> slot)
        {
            var orderView = GetOrderViewByPosition(slot.NewValue.ServerPosition);
            RefreshSlotView(slot.NewValue, orderView);
            if (jPopupManager.IsPopupActive<PurchaseByPremiumPopupView>())
            {
                jPopupManager.ForceClosePopup(typeof(PurchaseByPremiumPopupView));
            }

            orderView.Unlock();
            ShowAndUpdateStatusPanel(null);
        }

        public void OnOrderCompleteNetworkCallback(CollectionReplaceEvent<ISeaportOrderSlot> orderSlot, Action animationCallback)
        {
            ISeaportOrderSlot oldSlot = orderSlot.OldValue;
            ISeaportOrderSlot newSlot = orderSlot.NewValue;

            void OnResponseReceived()
            {
                RefreshSlotView(newSlot, GetOrderViewByPosition(newSlot.ServerPosition));
                ShowAndUpdateStatusPanel(animationCallback);
            }
            
            if (!View.Visible)
            {
                return;
            }

            PlayCollectProductAnimations(oldSlot.Awards);
            if (View.OrderStatusPanel.IsHidden) // If panel already hidden, start update panel view and play show panel animation.
            {
                OnResponseReceived();
                return;
            }

            _callbackWithData = OnResponseReceived;
        }

        public SeaportOrderItemView GetOrderViewByPosition(int serverPosition)
        {
            return View.OrderItemsList.Find(itemView => itemView.ServerPosition == serverPosition);
        }

        private void PlayCollectProductAnimations(ISeaportAward awardItem)
        {
            string softAmount = awardItem.Coins.ToString();
            string experienceAmount = awardItem.Experience.ToString();
            var selectedOrderItem = GetOrderViewByPosition(SelectedSlotPosition);
            Vector3 productStartPosition = Env.MainCamera.ScreenToWorldPoint(selectedOrderItem.RewardProductTransform.position);
            Vector3 softStartPosition = Env.MainCamera.ScreenToWorldPoint(selectedOrderItem.SoftCurrencyRewardText.position);
            Vector3 hardStartPosition = Env.MainCamera.ScreenToWorldPoint(selectedOrderItem.ExperienceRewardText.position);

            if (awardItem?.Products != null)
            {
                foreach (var awardProduct in awardItem.Products)
                {
                    string productsAmount = awardProduct.Count.ToString();
                    Sprite productSprite = jResourcesManager.GetProductSprite(awardProduct.ProductId.Value);
                    jAddRewardItemSignal.Dispatch(new AddItemSettings(FlyDestinationType.Warehouse, productStartPosition, productsAmount, productSprite));
                }
            }

            jAddRewardItemSignal.Dispatch(new AddItemSettings(FlyDestinationType.SoftCoins, softStartPosition, softAmount));
            jAddRewardItemSignal.Dispatch(new AddItemSettings(FlyDestinationType.Experience,  hardStartPosition, experienceAmount));
        }

        public void SetOrdersInteractable(bool isInteractable)
        {
            foreach (SeaportOrderItemView orderItemView in View.OrderItemsList)
            {
                if (isInteractable)
                {
                    orderItemView.Unlock();
                }
                else
                {
                    orderItemView.Lock();   
                }
            }
        }
    }
}
