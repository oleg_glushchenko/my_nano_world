using System;
using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.UI.Views.Popups;
using DG.Tweening;
using I2.Loc;
using NanoLib.Core.Pool;
using NanoLib.Core.Services.Sound;
using NanoReality.Game.Services.OrderDesk.SeaportOrderDesk;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.OrderDesk;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Services;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.UI
{
    public sealed class SeaportOrderStatusPanelView : View
    {
        #region Layout

        [SerializeField] private Button _actionButton;
        [SerializeField] private Localize _actionButtonText;
        [SerializeField] private RectTransform _orderProductsContainer;
        [SerializeField] private SeaportProductItemView _productItemPrefab;

        public Button ActionButton => _actionButton;
        public bool IsEnoughToSend { get; private set; }
        private int _cost;

        #endregion

        private ISeaportOrderSlot _currentOrderSlot;

        private readonly List<SeaportProductItemView> _visibleProductItems = new List<SeaportProductItemView>();
        private Tween _animationTween;
        private Tween _actionButtonTween;
        private RectTransform _rectTransform;
        private bool _animationInProgress;

        public event Action ActionButtonClicked;

        public ISeaportOrderSlot SelectedSlot => _currentOrderSlot;

        #region Inject

        [Inject] public IPopupManager jPopupManager { get; set; }
        [Inject] public IUnityPool<SeaportProductItemView> jProductItemsPool { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IProductService jProductService { get; set; }
        [Inject] public SignalWarehouseProductsAmountChanged jWarehouseChangedSignal { get; set; }
        [Inject] public ISoundManager jSoundManager { get; set; }
        [Inject] public BuyResoursesActionSignal jResoursesBuySignal { get; set; }

        #endregion

        public bool IsHidden { get; private set; }

        protected override void Awake()
        {
            base.Awake();
            _rectTransform = (RectTransform)transform;
        }
        
        [PostConstruct]
        public void OnPostConstruct()
        {
            jProductItemsPool.InstanceProvider = new PrefabInstanceProvider(_productItemPrefab);
            jWarehouseChangedSignal.AddListener(UpdateView);
        }

        [Deconstruct]
        public void OnDeconstruct()
        {
            jWarehouseChangedSignal.RemoveListener(UpdateView);
            jProductItemsPool.Dispose();
            jProductItemsPool = null;
        }

        private void OnEnable()
        {
            _actionButton.onClick.AddListener(OnActionButtonClicked);
            _rectTransform.anchoredPosition = new Vector2(GetPanelDefaultStatePosition(), _rectTransform.anchoredPosition.y);
        }

        private void OnDisable()
        {
            _actionButton.onClick.RemoveListener(OnActionButtonClicked);
        }

        public void Lock()
        {
            _actionButton.interactable = false;
            _visibleProductItems.ForEach( item => item.SetInteractable(false));
        }

        public void Unlock()
        {
            _actionButton.interactable = true;
            _visibleProductItems.ForEach(item => item.SetInteractable(true));
        }
        
        public void SetOrder(ISeaportOrderSlot orderSlot)
        {
            _currentOrderSlot = orderSlot;
            jWarehouseChangedSignal.Dispatch();

            if (orderSlot.IsShuffling)
            {
                Lock();
            }
            else
            {
                Unlock();
            }
        }

        private void UpdateView()
        {
            foreach (SeaportProductItemView activeProductItem in _visibleProductItems)
            {
                activeProductItem.ItemClicked -= OnProductItemClicked;
                activeProductItem.BuyClicked -= OnBuyProductsClicked;
                jProductItemsPool.Push(activeProductItem);
            }
            
            _visibleProductItems.Clear();
            _actionButton.transform.localScale = Vector3.one;
            _actionButtonTween?.Kill();

            if (_currentOrderSlot == null)
                return;

            IsEnoughToSend = true;

            foreach (ProductRequirementItem requirementItem in _currentOrderSlot.ProductOrderItems)
            {
                SeaportProductItemView productItemView = jProductItemsPool.Pop();
                SeaportProductItemModel itemModel = productItemView.ItemModel;
                itemModel.ProductId = requirementItem.ProductID;
                itemModel.WarehouseCount = jPlayerProfile.PlayerResources.GetProductCount(requirementItem.ProductID);
                itemModel.RequiredCount = requirementItem.Count;
                itemModel.BuyPrice = new Price
                {
                    PriceType = PriceType.Resources,
                    HardPrice = GetProductBuyoutCost(requirementItem.ProductID, itemModel.RequiredCount),
                    ProductsPrice = new Dictionary<Id<Product>, int>
                    {
                        { requirementItem.ProductID, itemModel.RequiredCount - itemModel.WarehouseCount }
                    }
                };
                
                productItemView.transform.SetParent(_orderProductsContainer, false);
                productItemView.transform.SetAsLastSibling();
                productItemView.ItemClicked += OnProductItemClicked;
                productItemView.BuyClicked += OnBuyProductsClicked;
                
                productItemView.UpdateView();

                IsEnoughToSend &= itemModel.WarehouseCount >= itemModel.RequiredCount;
                
                _visibleProductItems.Add(productItemView);
            }

            if (IsEnoughToSend)
            {
                _actionButtonText.Term = "UICommon/SEND_TEXT";
                PlayActionButtonLoopScaleAnimation();
            }
            else
            {
                _actionButtonText.Term = "UICommon/LOAD_TEXT";
            }
        }

        private void OnBuyProductsClicked(SeaportProductItemView obj)
        {
            SeaportProductItemModel itemModel = obj.ItemModel;
            Price resultPrice = new Price
            {
                PriceType = PriceType.Resources,
                ProductsPrice = new Dictionary<Id<Product>, int>() {{itemModel.ProductId, itemModel.RequiredCount - itemModel.WarehouseCount }}
            };

            jResoursesBuySignal.Dispatch(resultPrice, result =>{});
        }

        private void OnProductItemClicked(SeaportProductItemView obj)
        {
            
        }
        
        private void OnActionButtonClicked()
        {
            ActionButtonClicked?.Invoke();
        }

        private int GetProductBuyoutCost(int productId, int requiredCount)
        {
            var playerProductAmount = jPlayerProfile.PlayerResources.GetProductCount(productId);
            var productDifference = requiredCount - playerProductAmount;
            return jProductService.GetProduct(productId).HardPrice * productDifference;
        }

        public void PlayHidePanelAnimation(TweenCallback onCompleteCallback)
        {
            _animationTween?.Kill();

            float duration = 0.7f;
            float endPositionX = GetPanelHiddenStatePosition();

            _animationInProgress = true;
            
            var hidePanelTween = _rectTransform.DOAnchorPosX(endPositionX, duration);
            hidePanelTween.OnStart(() =>
            {
                jSoundManager.Play(SfxSoundTypes.SeaportOrderLoaded, SoundChannel.SoundFX);
            });
            hidePanelTween.OnComplete(() =>
            {
                IsHidden = true;
                onCompleteCallback?.Invoke();
                _animationInProgress = false;
                _animationTween = null;
            });

            _animationTween = hidePanelTween;
        }

        public void PlayShowPanelAnimation(TweenCallback onCompleteCallback)
        {
            float duration = 0.7f;
            float endPositionX = GetPanelDefaultStatePosition();
            
            _animationInProgress = true;
            
            var showPanelTween = _rectTransform.DOAnchorPosX(endPositionX, duration);
            showPanelTween.OnStart(() =>
            {
                //TODO: sound of return
            });
            showPanelTween.OnComplete(() =>
            {
                IsHidden = false;
                onCompleteCallback?.Invoke();
                _animationTween?.Kill();
                _animationInProgress = false;
                _rectTransform.anchoredPosition = new Vector2(GetPanelDefaultStatePosition(), _rectTransform.anchoredPosition.y);
            });
            
            _animationTween?.Kill();
            _animationTween = showPanelTween;
        }
        
        public void PlaySelectPanelAnimation()
        {
            if (_animationInProgress)
            {
                return;
            }
            
            var rectTransform = (RectTransform) transform;
            
            Vector3 punch = new Vector3(10, 0, 0);
            float duration = 0.5f;
            int vibrato = 7;
            float elacity = 1;
            _animationInProgress = true;
            
            _animationTween = rectTransform.DOPunchPosition(punch, duration, vibrato, elacity).OnComplete(() =>
            {
                _animationTween?.Kill();
                _animationInProgress = false;
            });
        }

        private void PlayActionButtonLoopScaleAnimation()
        {
            float punchScale = 0.05f;
            float duration = 1.0f;
            int vibrato = 1;
            float elasticity = 0.5f;
            
            _actionButtonTween = _actionButton.transform.DOPunchScale(punchScale * Vector3.one, duration, vibrato, elasticity).SetLoops(-1);
        }

        private float GetPanelHiddenStatePosition()
        {
            var rectTransform = (RectTransform) transform;
            var parentRectTransform = (RectTransform)rectTransform.parent;
            var endPosition = -1 * (parentRectTransform.rect.width / 2.0f + rectTransform.rect.width / 2.0f);

            return endPosition;
        }

        private float GetPanelDefaultStatePosition()
        {
            const float defaultPanelPositionX = 0;
            return defaultPanelPositionX;
        }
    }
}