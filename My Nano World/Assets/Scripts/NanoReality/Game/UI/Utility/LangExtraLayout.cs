﻿using Sirenix.OdinInspector;
using strange.extensions.mediation.impl;
using TMPro;
using UnityEngine;
﻿
namespace NanoReality.Game.UI.Utility
{
    public class LangExtraLayout : View
    {
        [SerializeField] private ExtraLayoutSettings _defaultSettings;
        [SerializeField] private LayoutKey _persistentId;
        [SerializeField] public ExtraLayoutStorage _extraLayoutStorage;
        private TextMeshProUGUI _defaultTMP;

        public TextMeshProUGUI DefaultTmp
        {
            get
            {
                if (_defaultTMP == null)
                {
                    _defaultTMP = GetComponent<TextMeshProUGUI>();
                }

                return _defaultTMP;
            }
            set => _defaultTMP = value;
        }

        public LayoutKey PersistentID
        {
            get
            {
                if (string.IsNullOrEmpty(_persistentId.PersistentName))
                {
#if UNITY_EDITOR
                    var prefab = UnityEditor.Experimental.SceneManagement.PrefabStageUtility.GetCurrentPrefabStage().prefabContentsRoot;
                    if (UnityEditor.Experimental.SceneManagement.PrefabStageUtility.GetCurrentPrefabStage() == null)
                    {
                        prefab = UnityEditor.PrefabUtility.GetNearestPrefabInstanceRoot(gameObject);
                    }

                    _persistentId = new LayoutKey
                    {
                        PersistentName = gameObject.name,
                        PersistentParentType = prefab.GetComponent<MonoBehaviour>().GetType().Name
                    };
#endif
                }

                return _persistentId;
            }
        }

        public void SetLayoutStorageLink(ExtraLayoutStorage extraLayoutStorage)
        {
            _extraLayoutStorage = extraLayoutStorage;
        }

        public void SaveDefaultSettings(TextMeshProUGUI defaultSettings)
        {
            _defaultSettings = new ExtraLayoutSettings(defaultSettings);
        }

        protected override void Start()
        {
            base.Start();
            DefaultTmp = GetComponent<TextMeshProUGUI>();
            if (LocalizationMLS.Instance != null)
            {
                LocalizationMLS.Instance.OnChangedLanguage += OnChangeLanguage;

                if (LocalizationMLS.Instance.IsInited)
                {
                    OnChangeLanguage();
                }
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (!LocalizationMLS.IsQuitting)
            {
                if (LocalizationMLS.Instance != null)
                {
                    LocalizationMLS.Instance.OnChangedLanguage -= OnChangeLanguage;
                }
            }
        }

        void OnChangeLanguage()
        {
            if (!LocalizationMLS.IsQuitting)
            {
                if (LocalizationMLS.Instance != null)
                {
                    ExtraLayoutSettings settings;
                    settings = LocalizationMLS.Instance.IsRtlLanguage
                        ? _extraLayoutStorage.GetItem(PersistentID).ArabicSettings
                        : _defaultSettings;

                    SetLayout(settings);
                }
            }
        }

        [Button]
        private void SetDefaultLayout()
        {
            SetLayout(_defaultSettings);
        }

        void SetLayout(ExtraLayoutSettings settings)
        {
            DefaultTmp.fontSize = settings.FontSize;
            DefaultTmp.enableWordWrapping = settings.Wrapping;
            DefaultTmp.enableAutoSizing = settings.AutoSize;
        }
    }
}