﻿using System.Collections.Generic;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.Utilities;
using Engine.UI;
using NanoLib.Core.Pool;
using NanoLib.Core.Services.Sound;
using NanoLib.UI.Core;
using NanoReality.Core.Resources;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.UI.Components;
using strange.extensions.mediation.impl;
using UniRx;
using UnityEngine;

namespace NanoReality.Game.UI.FlyingBalanceItemPanel
{
    public class FlyingBalanceItemMediator : Mediator
    {
        [Inject] public AddRewardItemSignal jAddRewardItemSignal { get; set; }
        [Inject] public StartSpentProductsAnimationSignal jStartSpentProductsAnimationSignal { get; set; }
        
        [Inject] public SignalExperienceUpdate jSignalExperienceUpdate { get; set; }
        [Inject] public SignalHardCurrencyUpdate jSignalHardCurrencyUpdate { get; set; }
        [Inject] public SignalSoftCurrencyUpdate jSignalSoftCurrencyUpdate { get; set; }
        [Inject] public SignalGoldCurrencyUpdate jSignalGoldCurrencyUpdate { get; set; }
        [Inject] public SignalPopulationUpdate jSignalPopulationUpdate { get; set; }
        [Inject] public SignalInfoNanoGemsCurrencyUpdate jSignalInfoNanoGemsCurrencyUpdate { get; set; }
        [Inject] public SignalWarehouseProductsAmountChanged jSignalWarehouseProductsAmountChanged { get; set; }
        [Inject] public InitFlyingPanelSignal jInitFlyingPanelSignal { get; set; }
        [Inject] public SpriteLoader SpriteLoader { get; set; }
        [Inject] public BalancePanelModel PanelModel { get; set; }
        [Inject] public IResourcesManager jResourcesManager { get; set; }
        [Inject] public IUIManager jUiManager { get; set; }
        [Inject] public IGameCamera jCamera { get; set; }
        [Inject] public ISoundManager jSoundManager { get; set; }

        private Transform _container;
        private FlyingBalanceItemPanel _view;

        private readonly Dictionary<FlyDestinationType, Transform>
            _destinations = new Dictionary<FlyDestinationType, Transform>();
        
        private readonly Dictionary<FlyDestinationType, Sprite>
            _sprites = new Dictionary<FlyDestinationType, Sprite>();

        private readonly IUnityPool<FlyingObject> _madeObjectPool = new UnityPool<FlyingObject>();
        private readonly IUnityPool<FlyingObject> _spentObjectPool = new UnityPool<FlyingObject>();
        private readonly Vector3 _startOffset = new Vector3(0, 300, 0);
        private readonly float _distanceBetweenFlyingObjects = 200f;

        
        public override void OnRegister()
        {
            base.OnRegister();
            
            _view = jUiManager.FlyingPanel;
            _container = _view.Container;
            
            _madeObjectPool.InstanceProvider = new PrefabInstanceProvider(_view.MadeObject);
            _spentObjectPool.InstanceProvider = new PrefabInstanceProvider(_view.SpentObject);
            
            jAddRewardItemSignal.AddListener(OnAddRewardItem);
            jStartSpentProductsAnimationSignal.AddListener(OnStartSpentProductsAnimation);
            jInitFlyingPanelSignal.AddOnce(OnInitFlyingPanelSignal);
        }

        [Deconstruct]
        public override void OnRemove()
        {
            base.OnRemove();
            
            jAddRewardItemSignal.RemoveListener(OnAddRewardItem);
            jStartSpentProductsAnimationSignal.RemoveListener(OnStartSpentProductsAnimation);
        }
        
        private void OnInitFlyingPanelSignal()
        {
            BindSpritesWithTypes(SpriteLoader.GetSpriteData<CityCountersSprites>());
            
            PanelModel.TempBalancePanel.Subscribe( tempView =>
            {
                BindDestWithTypes(tempView == null ? jUiManager.GetView<BalancePanelView>() : tempView);
            });
            
            BindDestWithTypes(jUiManager.GetView<BalancePanelView>());
        }

        private void OnAddRewardItem(AddItemSettings settings)
        {
            if (settings.Target == FlyDestinationType.NoTarget)
                return;
            
            if (int.Parse(settings.AmountText) == 0)
                return;

            FlyingObject flyingObject = _madeObjectPool.Pop();
            flyingObject.transform.SetParent(_container);

            if (_sprites.TryGetValue(settings.Target, out Sprite sprite))
            {
                settings.Sprite = sprite;
            }
            
            Transform destination = settings.Destination
                ? settings.Destination
                : _destinations[settings.Target];

            Vector2 screenPosition = jCamera.WorldToScreenPoint(settings.Position);
            flyingObject.Play(screenPosition, settings.Sprite,$"+{settings.AmountText}", destination);

            flyingObject.OnEffectStopped += (stoppedObject) =>
            {
                DispatchViewSignal(settings.Target);
                Release(stoppedObject, _madeObjectPool);
            };
            
            flyingObject.OnEffectCanceled += (canceledObject) =>
            {
                Release(canceledObject, _madeObjectPool);
            };
        }

        private void OnStartSpentProductsAnimation(Vector3 startPosition, Dictionary<Id<Product>, int> products)
        {
            if (products == null)
                return;

            int counter = 0;
            foreach (var product in products)
            {
                if (product.Value == 0)
                    continue;
                
                FlyingObject flyingObject = _spentObjectPool.Pop();
                flyingObject.transform.SetParent(_container);
                
                Vector3 horizontalSpread = HorizontalSpread(counter, products.Count);
                Vector3 position = startPosition + _startOffset + horizontalSpread;
                Sprite sprite = jResourcesManager.GetProductSprite(product.Key.Value);
                counter++;
                
                flyingObject.Play(position, sprite, $"-{product.Value}", _view.SpentObjectDestination);
                    
                flyingObject.OnEffectStopped += (stoppedObject) =>
                {
                    DispatchViewSignal(FlyDestinationType.Spend);
                    Release(stoppedObject, _spentObjectPool);
                };
                
                flyingObject.OnEffectCanceled += (canceledObject) =>
                {
                    Release(canceledObject, _spentObjectPool);
                };
            }
        }

        private Vector3 HorizontalSpread(int index, int total)
        {
            if (total == 1)
                return Vector3.zero;

            return Vector3.left * (index * _distanceBetweenFlyingObjects - total * _distanceBetweenFlyingObjects / 2 +
                                   _distanceBetweenFlyingObjects / 2);
        }

        private static void Release(FlyingObject flyingObject, IUnityPool<FlyingObject> pool)
        {
            pool.Push(flyingObject);
        }

        private void DispatchViewSignal(FlyDestinationType type)
        {
            SfxSoundTypes sfxSoundTypes = SfxSoundTypes.FlyExpstars;
            
            switch (type)
            {
                case FlyDestinationType.Spend:
                case FlyDestinationType.Warehouse:
                case FlyDestinationType.FoodSupply:
                    jSignalWarehouseProductsAmountChanged.Dispatch();
                    sfxSoundTypes = SfxSoundTypes.FlyGoods;
                    break;
                case FlyDestinationType.NanoGems:
                    jSignalInfoNanoGemsCurrencyUpdate.Dispatch();
                    sfxSoundTypes = SfxSoundTypes.FlyCurrencies;
                    break;
                case FlyDestinationType.PremiumCoins:
                    jSignalHardCurrencyUpdate.Dispatch();
                    sfxSoundTypes = SfxSoundTypes.FlyCurrencies;
                    break;
                case FlyDestinationType.SoftCoins:
                    jSignalSoftCurrencyUpdate.Dispatch();
                    sfxSoundTypes = SfxSoundTypes.FlyCurrencies;
                    break;
                case FlyDestinationType.Citizens:
                    jSignalPopulationUpdate.Dispatch();
                    break;
                case FlyDestinationType.Experience:
                    jSignalExperienceUpdate.Dispatch();
                    sfxSoundTypes = SfxSoundTypes.FlyExpstars;
                    break;
                case FlyDestinationType.GoldCurrency:
                    jSignalGoldCurrencyUpdate.Dispatch();
                    sfxSoundTypes = SfxSoundTypes.FlyCurrencies;
                    break;
            }
            
            jSoundManager.Play(sfxSoundTypes, SoundChannel.SoundFX);
       
        }

        private void BindSpritesWithTypes(CityCountersSprites sprites)
        {
            _sprites.Add(FlyDestinationType.SoftCoins, sprites.SoftCoinSprite);
            _sprites.Add(FlyDestinationType.PremiumCoins, sprites.PremiumCoinSprite);
            _sprites.Add(FlyDestinationType.Experience, sprites.ExperienceSprite);
            _sprites.Add(FlyDestinationType.Citizens, sprites.CitizenSprite);
            _sprites.Add(FlyDestinationType.GoldCurrency, sprites.GoldCurrencySprite);
        }
        
        private void BindDestWithTypes(BalancePanelView view)
        {
            _destinations.Clear();
            
            _destinations.Add(FlyDestinationType.SoftCoins, view.SoftCurrency);
            _destinations.Add(FlyDestinationType.PremiumCoins, view.HardCurrency);
            _destinations.Add(FlyDestinationType.GoldCurrency, view.GoldCurrency);
            _destinations.Add(FlyDestinationType.Experience, view.LevelPanel);
            _destinations.Add(FlyDestinationType.Warehouse, view.WarehousePanel);
            _destinations.Add(FlyDestinationType.Workers, view.HappinessPanel);
            _destinations.Add(FlyDestinationType.Citizens, view.PopulationHappinessPanel);
            _destinations.Add(FlyDestinationType.FoodSupply, view.FoodSupplyTransform);
            _destinations.Add(FlyDestinationType.Lantern, view.SoftCurrency);
        }
    }
}