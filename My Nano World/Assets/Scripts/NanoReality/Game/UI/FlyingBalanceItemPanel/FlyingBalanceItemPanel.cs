﻿using strange.extensions.mediation.impl;
using UnityEngine;

namespace NanoReality.Game.UI.FlyingBalanceItemPanel
{
    public class FlyingBalanceItemPanel : View
    {
        [SerializeField] private Transform _container;
        [SerializeField] private FlyingObject _madeObject;
        [SerializeField] private FlyingObject _spentObject;
        [SerializeField] private Transform _spentObjectDestination;

        public Transform Container => _container;
        public FlyingObject MadeObject => _madeObject;
        public FlyingObject SpentObject => _spentObject;

        public Transform SpentObjectDestination => _spentObjectDestination;
    }
}
