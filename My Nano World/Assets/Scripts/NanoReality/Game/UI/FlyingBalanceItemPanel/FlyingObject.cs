﻿using System;
using NanoReality.Engine.UI.UIAnimations;
using strange.extensions.pool.api;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.UI.FlyingBalanceItemPanel
{
    public class FlyingObject : MonoBehaviour, IPoolable
    {
        [SerializeField] private Image _spriteIcon;
        [SerializeField] private GameObjectFlyEffect _specialEffect;
        [SerializeField] private TextMeshProUGUI _amountText;

        public event Action<FlyingObject> OnEffectStopped;
        public event Action<FlyingObject> OnEffectCanceled;

        private void Start()
        {
            _specialEffect.EffectCanceled.AddListener(effect => { OnEffectCanceled?.Invoke(this); });
            _specialEffect.EffectStopped.AddListener(effect => { OnEffectStopped?.Invoke(this); });
        }

        public void Play(Vector3 position, Sprite sprite, string amount, Transform destination)
        {
            transform.localScale = Vector3.one;
            _specialEffect.Destination = destination;
            transform.position = position;
            _spriteIcon.sprite = sprite;
            _amountText.text = amount;

            _specialEffect.Play();
        }

        public void Restore()
        {
        }

        public void Retain()
        {
            gameObject.SetActive(true);
        }

        public void Release()
        {
            gameObject.SetActive(false);
        }

        public bool retain { get; }
    }
}
