﻿using NanoReality.Engine.UI.UIAnimations;

namespace NanoReality.Game.UI.FlyingBalanceItemPanel
{
    public interface IFlyingItem
    {
        GameObjectFlyEffect Effect { get; }
    }
}
