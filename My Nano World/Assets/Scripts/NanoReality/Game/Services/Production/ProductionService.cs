using System;
using System.Collections.Generic;
using System.Linq;
using NanoReality.Game.CityStatusEffects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Production;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using UnityEngine;

namespace NanoReality.Game.Production
{
    public sealed class ProductionService : IProductionService
    {
        [Inject] public ICityStatusEffectService jCityStatusEffectService { get; set; }
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }

        private CityStatusEffectSettings _productionStatusEffect;
        
        public void Init()
        {
            jCityStatusEffectService.StatusEffectsChanged += OnStatusEffectChanged;
        }

        public void Dispose()
        {
            jCityStatusEffectService.StatusEffectsChanged -= OnStatusEffectChanged;
        }

        private void OnStatusEffectChanged()
        {
            CityStatusEffect productionStatusEffect = jCityStatusEffectService.ActiveStatusEffects.FirstOrDefault(c =>
                jCityStatusEffectService.GetStatusEffectSettings(c.EffectId).EffectType == StatusEffect.ProductionSpeed);

            if (productionStatusEffect == null)
            {
                _productionStatusEffect = null;
                UpdateBuildingProductionSlots();
                return;
            }
            
            _productionStatusEffect = jCityStatusEffectService.GetStatusEffectSettings(productionStatusEffect.EffectId);
            UpdateBuildingProductionSlots(null);
        }

        public float GetProductionTimeMultiplier(IProducingItemData product)
        {
            float timeMultiplier = 1;
            if (_productionStatusEffect != null)
            {
                if (_productionStatusEffect.EffectInfluence == StatusEffectInfluence.Increase)
                {
                    timeMultiplier -= timeMultiplier * _productionStatusEffect.EffectValue;
                }
                else if(_productionStatusEffect.EffectInfluence == StatusEffectInfluence.Decrease)
                {
                    timeMultiplier += timeMultiplier * _productionStatusEffect.EffectValue;
                }
            }

            return Mathf.CeilToInt(product.ProducingTime) * timeMultiplier;
        }

        public void UpdateBuildingProductionSlots(Action onSuccess = null)
        {
            void OnSlotsLoaded(List<BuildingProductionInfo> buildingProductionInfos)
            {
                foreach (BuildingProductionInfo productionInfo in buildingProductionInfos)
                {
                    var productionBuilding = jGameManager.FindMapObjectById<IProductionBuilding>(productionInfo.BuildingId);
                    foreach (IProduceSlot activeSlot in productionBuilding.CurrentProduceSlots)
                    {
                        IProduceSlot updatedSlot = productionInfo.ProductionSlots.FirstOrDefault(c => c.Id == activeSlot.Id);
                        if (updatedSlot == null)
                        {
                            continue;
                        }
                        activeSlot.CurrentProducingTime = updatedSlot.CurrentProducingTime;
                        activeSlot.RefreshTimerTimeout();
                    }
                }
                
                onSuccess?.Invoke();
            }
            
            jServerCommunicator.GetProductionSlots(OnSlotsLoaded);
        }
    }
}