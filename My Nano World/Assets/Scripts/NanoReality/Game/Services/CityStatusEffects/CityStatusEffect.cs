using Newtonsoft.Json;

namespace NanoReality.Game.CityStatusEffects
{
    /// <summary>
    /// Contains information about database status effect id and date time when effect will be finished.
    /// </summary>
    public sealed class CityStatusEffect
    {
        /// <summary>
        /// Unique effect instance id.
        /// </summary>
        [JsonProperty("id")]
        public int Id;

        /// <summary>
        /// Database effect id.
        /// </summary>
        [JsonProperty("status_effect_id")]
        public int EffectId;

        /// <summary>
        /// Unix Time.
        /// </summary>
        [JsonProperty("finish_at")]
        public long EffectEndTime;
    }
}