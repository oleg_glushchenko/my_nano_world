﻿using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;

namespace NanoReality.Game.Services.SectorSevices
{
    public interface IUnlockSectorService
    {
        void UnlockAllSectors();
        void BuySector(CityLockedSector cityLockedSector);
    }
}
