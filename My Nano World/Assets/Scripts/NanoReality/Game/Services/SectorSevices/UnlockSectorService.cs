﻿using System;
using Assets.NanoLib.UtilitesEngine.CustomDebugging.Models.api;
using Assets.Scripts.Engine.Analytics.Model.api;
using NanoLib.Core.Services.Sound;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.Services;

namespace NanoReality.Game.Services.SectorSevices
{
    public class UnlockSectorService : IUnlockSectorService
    {
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public IDebug jDebug { get; set; }
        [Inject] public ICitySectorService jCitySectorService { get; set; }
        [Inject] public INanoAnalytics jNanoAnalytics { get; set; }
        [Inject] public SignalOnSubSectorUnlocked jSignalOnSubSectorUnlocked { get; set; }
        [Inject] public SignalOnSubSectorUnlockedVerified jSignalOnSubSectorUnlockedVerified { get; set; }
        [Inject] public SignalOnSubSectorsUpdated jSignalOnSubSectorsUpdated { get; set; }
        [Inject] public ISoundManager jSoundManager { get; set; }
        

        public void UnlockAllSectors()
        {
            foreach (ICityMap map in jGameManager.CurrentUserCity.CityMaps)
            {
                foreach (CityLockedSector lockedSector in map.SubLockedMap.LockedSectors)
                {
                    UnlockSector(OnSuccess, lockedSector);
                    
                    void OnSuccess(bool result)
                    {
                        if (!result)
                        {
                            jDebug.LogError($"Locked sector {lockedSector.LockedSector.Id} in city {lockedSector.CityId} failed to unlock");
                        }
                    }
                }
                
                map.SubLockedMap.LockedSectors.Clear();
            }
        }

        public void BuySector(CityLockedSector cityLockedSector)
        {
            UnlockSector(OnSuccess, cityLockedSector);
            var sectorType = cityLockedSector.BusinessSectorId.ToBusinessSectorType();
            var cityMap = jGameManager.GetMap(sectorType);
            cityMap.SubLockedMap.LockedSectors.Remove(cityLockedSector);
            jCitySectorService.UpdateLockedMap(cityMap.MapSize, cityMap.SubLockedMap.LockedSectors, cityMap.BusinessSector);
            jSignalOnSubSectorsUpdated.Dispatch();

            jPlayerProfile.PlayerResources.BuyItem(cityLockedSector.LockedSector.Price);
            jNanoAnalytics.OnPlayerCurrenciesDecrease(cityLockedSector.LockedSector.Price, CurrenciesSpendingPlace.UnlockSector, cityLockedSector.LockedSector.Id.Value.ToString());

            void OnSuccess(bool result)
            {
                if (result)
                {
                    jSoundManager.Play(SfxSoundTypes.AreaUnlocked, SoundChannel.SoundFX);
                    jSignalOnSubSectorUnlockedVerified.Dispatch(cityLockedSector.LockedSector);
                }
            }
        }

        private void UnlockSector(Action<bool> onSuccess, CityLockedSector cityLockedSector)
        {
            jServerCommunicator.UnlockSubSector(jGameManager.CurrentUserCity.Id, cityLockedSector.LockedSector.Id, onSuccess);
            jSignalOnSubSectorUnlocked.Dispatch(cityLockedSector);
        }
    }
}
