using System.Collections.Generic;
using AppsFlyerSDK;
using Assets.NanoLib.UI.Core.Signals;
using NanoLib.Core.Logging;
using NanoReality.GameLogic.IAP;
using NanoReality.GameLogic.Settings.GamePrefs;

public interface IAppsFlyerAnalyticsService
{
	void Initialize();
}

public class AppsFlyerAnalyticsService : IAppsFlyerAnalyticsService
{
	[Inject] public IPlayerExperience jPlayerExperience { get; set; }
	[Inject] public IGamePrefs jGamePrefs { get; set; }
	[Inject] public PurchaseFinishedSignal jPurchaseFinishedSignal { get; set; }
	[Inject] public AdsWatchFinishedSignal jAdsWatchFinishedSignal { get; set; }
	[Inject] public UserLevelUpSignal jUserLevelUpSignal { get; set; }
	[Inject] public AppsFlyerObjectScript jAppsFlyerObjectScript { get; set; }

	private const string FirstPurchaseSaveKey = "TheFirstPurchaseWasMade";
	
	public void Initialize()
	{
		jAppsFlyerObjectScript.AnalyticsStart();
		
		jPurchaseFinishedSignal.AddListener(OnPurchaseFinished);
		jPurchaseFinishedSignal.AddListener(OnFirstPurchaseFinished);
		jAdsWatchFinishedSignal.AddListener(OnAdsWatchFinished);
		jUserLevelUpSignal.AddListener(OnLevelUp);
	}
	
	private static void OnPurchaseFinished(IStoreProduct product, bool isSuccess)
	{
		if (!isSuccess)
		{
			"Purchase finished with status failed. Can't send analytics data".LogError(LoggingChannel.Analytics);
			return;
		} 
		
		var purchaseEvent = new Dictionary<string, string>
		{
			{AFInAppEvents.CURRENCY, "USD"},
			{AFInAppEvents.REVENUE, product.Price},
			{AFInAppEvents.QUANTITY, "1"},
			{"SKU", product.StoreId},
			{"TRANSACTION_ID", "1234"},
			{"PLACEMENT", "Shop"}
		};
		
		AppsFlyer.sendEvent("af_purchase", purchaseEvent);
	}
	
	private void OnFirstPurchaseFinished(IStoreProduct _, bool isSuccess)
	{
		if (!isSuccess)
		{
			"First purchase failed. Can't send analytics data".LogError(LoggingChannel.Analytics);
			return;
		} 
		
		var theFirstPurchaseWasMade = jGamePrefs.HasKey(FirstPurchaseSaveKey) && jGamePrefs.GetBool(FirstPurchaseSaveKey);
		if (theFirstPurchaseWasMade)
			return;

		jGamePrefs.SetBool(FirstPurchaseSaveKey, true);
		AppsFlyer.sendEvent("Unique_PU", new Dictionary<string, string>());
	}

	private static void OnAdsWatchFinished(AdTypePanel adTypePanel)
	{
		var rvFinishEvent = new Dictionary<string, string>()
		{
			{"Placement", adTypePanel.ToString()}
		};
		
		AppsFlyer.sendEvent("RV_finish", rvFinishEvent);
	}

	private void OnLevelUp()
	{
		var rvFinishEvent = new Dictionary<string, string>()
		{
			{"Level", jPlayerExperience.CurrentLevel.ToString()},
			{"Status", "win"}
		};

		AppsFlyer.sendEvent("Progress", rvFinishEvent);
	}

	[Deconstruct]
	public void Deconstruct()
	{
		jPurchaseFinishedSignal.RemoveListener(OnPurchaseFinished);
		jPurchaseFinishedSignal.RemoveListener(OnFirstPurchaseFinished);
		jAdsWatchFinishedSignal.RemoveListener(OnAdsWatchFinished);
		jUserLevelUpSignal.RemoveListener(OnLevelUp);
	}
}