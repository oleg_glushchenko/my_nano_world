﻿using System.Collections.Generic;
using System.Text;
using Assets.NanoLib.UtilitesEngine;
using Assets.Scripts.Engine.Analytics.Model;
using Assets.Scripts.Engine.Analytics.Model.api;
using NanoLib.Core.Logging;
using NanoReality.Engine.Settings.BuildSettings;
using NanoReality.Game.Data;
using UnityEngine;

namespace NanoReality.Game.Analytics
{
    public class AnalyticsManager : IAnalyticsManager
    {
        [Inject] public IBuildSettings jBuildSettings { get; set; }
        [Inject] public INanoAnalytics jAnalytics { get; set; }
        [Inject] public NetworkSettings jNetworkSettings { get; set; }
        [Inject] public IPlayerExperience jPlayerExperience { get; set; }
        [Inject] public ITimerManager jTimerManager { get; set; }
        [Inject] public IAppsFlyerAnalyticsService jAppsFlyerAnalyticsService { get; set; }

        private List<IAnalyticsProvider> _analyticsProviders;
        private string _playerId;
        private bool _permissionsGranted = true;

        public bool PermissionsGranted => _permissionsGranted;

        public void Init()
        {
#if UNITY_IOS
            var status = Unity.Advertisement.IosSupport.ATTrackingStatusBinding.GetAuthorizationTrackingStatus();
            $"Analytics permissions status: {status}".Log(LoggingChannel.Analytics);
            
            _permissionsGranted = status == Unity.Advertisement.IosSupport.ATTrackingStatusBinding.AuthorizationTrackingStatus.AUTHORIZED;
#endif

            // if (!_permissionsGranted)
            // {
            //     "Analytics are not initialized because permissions not granted".Log(LoggingChannel.Analytics);
            //     return;
            // }

            //jAppsFlyerAnalyticsService.Initialize();

            if (jBuildSettings.EnableLiveAnalytics)
            {
#if UNITY_ANDROID || UNITY_IOS
                _analyticsProviders = new List<IAnalyticsProvider>
                {
                    new DeltaDnaAnalyticsProvider().Initialize(_playerId), 
                    new UnityAnalyticsProvider().Initialize(_playerId)
                };
#endif
                "AnalyticsManager initialized".Log(LoggingChannel.Analytics);
            }
            else
            {
                "Analytics are disabled is build settings.".Log(LoggingChannel.Analytics);
            }
        }
        
        public void InitAnalyticManager(int playerId)
        {
            _playerId = playerId.ToString();
            Init();
        }

        public void LogEvent(string name)
        {
            if (_analyticsProviders == null) return;
            
            name.Log(LoggingChannel.Analytics);

            foreach (var analyticsProvider in _analyticsProviders)
            {
                analyticsProvider.LogEvent(name);
            }
        }

        public void LogEvent(string name, string paramName, object paramValue)
        {
            if (_analyticsProviders == null) return;
            
            var message = new StringBuilder(name);
            message.Append(": [");
            message.Append(paramName);
            message.Append(": ");
            message.Append(paramValue);
            message.Append("]");
            message.Log(LoggingChannel.Analytics);

            foreach (var analyticsProvider in _analyticsProviders)
            {
                analyticsProvider.LogEvent(name, paramName, paramValue);
            }
        }

        public void LogEvent(CustomEventData customEventData)
        {
            if (_analyticsProviders == null) return;
            
            var serverResponseAnalytic = jAnalytics.GetServerResponseTime();
            
            if (customEventData.Params != null)
            {
                customEventData.Params.Add("current_server_name", jNetworkSettings.CurrentServerName);
                customEventData.Params.Add("userLevel", jPlayerExperience.CurrentLevel);
                customEventData.Params.Add("session_length", jTimerManager.SessionLength);
                customEventData.Params.Add("response_time_quart1", serverResponseAnalytic.Response_time_quart1);
                customEventData.Params.Add("response_time_median", serverResponseAnalytic.Response_time_median);
                customEventData.Params.Add("response_time_quart3", serverResponseAnalytic.Response_time_quart3);
                var message = new StringBuilder(customEventData.EventName);
                message.Append(": [");
                foreach (var param in customEventData.Params)
                {
                    message.Append(param.Key);
                    message.Append(": ");
                    message.Append(param.Value);
                    message.Append(", ");
                }

                message.Append("]");
                message.Log(LoggingChannel.Analytics);

                foreach (var analyticsProvider in _analyticsProviders)
                {
                    analyticsProvider.LogEvent(customEventData);
                }
            }
            else
            {
                LogEvent(customEventData.EventName);
            }
        }
        
        public void Dispose()
        {
            _analyticsProviders = null;
            jAnalytics.Dispose();
        }
    }
}