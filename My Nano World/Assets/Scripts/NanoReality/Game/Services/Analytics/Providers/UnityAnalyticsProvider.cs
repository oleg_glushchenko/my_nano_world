using System.Collections.Generic;
using Assets.Scripts.Engine.Analytics.Model;
using NanoLib.Core.Logging;
using UnityEngine.Analytics;

namespace NanoReality.Game.Analytics
{
    public class UnityAnalyticsProvider: IAnalyticsProvider
    {
        private string _userId;        
        
        public bool Initialized { get; private set; }
        
        public IAnalyticsProvider Initialize(string userId, bool isActive = true)
        {
            if (Initialized)
            {
                Logging.LogError(LoggingChannel.Analytics, "UnityAnalyticsProvider is already initialized.");
                return this;
            }

            _userId = userId;
            Initialized = true;
            
            return this;
        }

        public void LogEvent(string name)
        {
            AnalyticsEvent.Custom(name, null);
        }

        public void LogEvent(string name, string paramName, object paramValue)
        {
            AnalyticsEvent.Custom(name, new Dictionary<string, object>
            {
                {"userId", _userId},
                { paramName, paramValue }
            });
        }

        public void LogEvent(CustomEventData customEventData)
        {
            var optionsEvent = new Dictionary<string, object> {{"userId", _userId}};

            foreach (var param in customEventData.Params)
            {
                optionsEvent.Add(param.Key, param.Value);
            }

            AnalyticsEvent.Custom(customEventData.EventName, optionsEvent);
        }
    }
}