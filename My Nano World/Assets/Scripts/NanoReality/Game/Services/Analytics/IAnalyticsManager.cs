using Assets.Scripts.Engine.Analytics.Model;
using NanoReality.Game.Services;

namespace NanoReality.Game.Analytics
{
	public interface IAnalyticsManager : IGameService
	{
		bool PermissionsGranted { get; }

		void InitAnalyticManager(int playerId);

		void LogEvent(string name);
        void LogEvent(string name, string paramName, object paramValue);
        void LogEvent(CustomEventData customEventData);
    }
}