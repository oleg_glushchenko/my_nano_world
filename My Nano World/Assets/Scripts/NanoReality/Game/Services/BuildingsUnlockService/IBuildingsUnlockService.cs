﻿using System.Collections.Generic;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace NanoReality.Game.Services.BuildingsUnlockService
{
    public interface IBuildingsUnlockService
    {
        void Init(List<BuildingInstanceUnlockInfo> instancesUnlocksList);

        /// <summary>
        /// Returns required player level on which building can be upgraded.
        /// </summary>
        /// <param name="buildingId">Balance Building Id</param>
        /// <param name="instanceIndex">Target </param>
        /// <param name="currentBuildingLevel">Current level</param>
        /// <returns>Level, on which player can be upgrade this building instance.</returns>
        int GetPlayerLevelForUpgrade(int buildingId, int instanceIndex, int currentBuildingLevel);

        /// <summary>
        /// Returns required player level on which building can be unlocked for place on the map.
        /// </summary>
        /// <param name="buildingId">Balance Building Id</param>
        /// <returns></returns>
        int GetBuildingUnlockLevel(int buildingId);

        /// <summary>
        /// Returns list with ids of buildings that unlocked for target player level.
        /// </summary>
        /// <param name="playerLevel">Target Player Level</param>
        /// <returns></returns>
        IEnumerable<int> GetNewUnlockedBuildings(int playerLevel);

        Price GetNewBuildingInstancePrice(int buildingId);

        Price GetBuildingInstancePrice(int buildingId, int instanceIndex);

        /// <summary>
        /// Returns price for upgrade target building instance to target level.
        /// </summary>
        /// <param name="buildingId">Building Id.</param>
        /// <param name="nextBuildingLevel">Next building level.</param>
        /// <param name="instanceIndex">Building instance index.</param>
        /// <returns>Returns upgrade price or default value(if there aren't upgrade to nextLevel)</returns>
        Price GetUpgradeBuildingPrice(int buildingId, int nextBuildingLevel, int instanceIndex);
        
        int GetMaxAvailableInstancesCount(int buildingId, int playerLevel);
        
        bool GetNextAvailableInstancesCount(int buildingId, out int nextAvailableCount, out int nextPlayerLevel);

        int GetAvailableBuildingsCountFromList(List<int> buildingsIdsList);

        /// <summary>
        /// Returns count of new instances available for construction for current player level.
        /// </summary>
        /// <param name="buildingId">Target Building Id.</param>
        /// <returns>Count of new instances available for construction </returns>
        int GetAvailableBuildingInstancesCount(int buildingId);

        /// <summary>
        /// Returns multiplier of construction time for building with 0 level.
        /// </summary>
        /// <param name="buildingId"></param>
        /// <param name="buildingLevel"></param>
        /// <param name="instanceIndex"></param>
        /// <returns></returns>
        int GetBuildingConstructionTime(int buildingId, int buildingLevel, int instanceIndex);

        /// <summary>
        /// Returns multiplier for experience reward for finish building construction.
        /// </summary>
        /// <param name="buildingId"></param>
        /// <param name="buildingLevel"></param>
        /// <param name="instanceIndex"></param>
        /// <returns></returns>
        int GetExperienceReward(int buildingId, int buildingLevel, int instanceIndex);

        int GetPopulationForDwelling(int buildingId, int buildingLevel, int instanceIndex);

        /// <summary>
        /// Can be building upgrade for current player level.
        /// </summary>
        /// <param name="buildingId"></param>
        /// <param name="nextBuildingLevel"></param>
        /// <param name="instanceIndex"></param>
        /// <returns></returns>
        bool IsUpgradeAvailable(int buildingId, int nextBuildingLevel, int instanceIndex);

        bool IsUpgradeAvailable(IMapObject targetBuilding);
    }
}