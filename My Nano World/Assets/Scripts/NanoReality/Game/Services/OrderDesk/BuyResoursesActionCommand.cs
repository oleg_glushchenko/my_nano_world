using System;
using System.Text;
using Assets.Scripts.Engine.Analytics;
using Assets.Scripts.Engine.Analytics.Model;
using Assets.Scripts.Engine.Analytics.Model.api;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Impl;
using NanoReality.GameLogic.Services;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.StrangeIoC;
using strange.extensions.signal.impl;

namespace NanoReality.GameLogic.OrderDesk
{
    public sealed class BuyResoursesActionSignal : Signal<Price, Action<PopupResult>> { }
    
    public sealed class BuyResoursesActionCommand : AGameCommand
    {
        [Inject] public Price jProductsPrice { get; set; }
        [Inject] public Action<PopupResult> jResultCallback { get; set; }
        [Inject] public IPopupManager jPopupManager { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IProductService jProductService { get; set; }
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public SignalOnBuyMissingProductsAction jSignalOnBuyMissingProductsAction { get; set; }
        [Inject] public INanoAnalytics jNanoAnalytics { get; set; }

        private int _cost;
        
        public override void Execute()
        {
            base.Execute();
            Retain();
            BuyResourses();
        }

        private void OnPopupResultCallback(PopupResult popupResult)
        {
            jResultCallback.Invoke(popupResult);
            Release();
        }

        private void BuyResourses()
        {
            _cost = 0;
            foreach (var product in jProductsPrice.ProductsPrice)
            {
                _cost += product.Value * jProductService.GetProduct(product.Key).HardPrice;
            }
            
            if (_cost <= jPlayerProfile.PlayerResources.HardCurrency)
            {
                jServerCommunicator.PurchaseProducts(jPlayerProfile.UserCity.Id, jProductsPrice.ProductsPrice);

                jPlayerProfile.PlayerResources.AddProducts(jProductsPrice.ProductsPrice);
                jPlayerProfile.PlayerResources.BuyHardCurrency(_cost);
                        
                var products = new StringBuilder();
                foreach (var product in jProductsPrice.ProductsPrice)
                {
                    products.Append(product.Key.Value + " ");
                }

                jNanoAnalytics.OnPlayerCurrenciesDecrease(PlayerResources.CurencyType.Bucks, _cost, CurrenciesSpendingPlace.NotEnoughtResourcesBuy, products.ToString());
                jSignalOnBuyMissingProductsAction.Dispatch(new MissingProductsInfo(jProductsPrice.ProductsPrice, _cost));
                
                OnPopupResultCallback(PopupResult.Ok);
                return;
            }

            jPopupManager.Show(new NotEnoughMoneyPopupSettings(PriceType.Hard, _cost));
        }
    }
}