using System;
using System.Collections.Generic;
using NanoReality.Game.UI;

namespace NanoReality.Game.Services.OrderDesk.SeaportOrderDesk
{
    public interface ISeaportOrderDeskService : IGameService
    {
        ReactiveSeaportCollection SeaportCollection { get; }
        int ShuffleTime { get; }

        event Action<ISeaportOrderSlot> OnSeaportSlotShuffleUpdated;

        void LoadSeaportOrders(Action<NetworkSeaportOrderDesk> resultCallback = null);
        void SendOrder(int slotPosition);
        void StartShuffleOrder(int serverPosition, Action resultCallback);
        void SkipShuffleOrderWaiting(int serverPosition, bool skipByAds, Action resultCallback);
        void RemoveProducts(int serverPosition);

        /// <summary>
        /// Returns slot for it order index.
        /// </summary>
        /// <param name="position">Order positions in order desk.</param>
        /// <returns></returns>
        ISeaportOrderSlot GetSlot(int position);

        float GetTimeLeft(int slotServerPosition);
    }
}