using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;

namespace NanoReality.Game.Services.OrderDesk.SeaportOrderDesk
{
    public class SeaportIdleState : SeaportState
    {
        public SeaportIdleState(SeaportPanelMediator mediator) : base(mediator)
        {
        }

        public override void Select(int orderServerPosition)
        {
            Mediator.SelectOrder(orderServerPosition);
            var slotModel = Mediator.jSeaportOrderDeskService.GetSlot(orderServerPosition); 
            if (slotModel.IsShuffling)
            {
                Mediator.SetState(new SeaportShuffleState(Mediator));
            }
            else
            {
                Mediator.SetState(new SeaportIdleState(Mediator));
            }
        }

        public override void Send()
        {
            if (IsCanSendOrder())
            {
                Mediator.SetState(new SeaportSendState(Mediator));
                Mediator.jSeaportOrderDeskService.SendOrder(Mediator.SelectedSlotPosition);
                Mediator.jSeaportOrderDeskService.RemoveProducts(Mediator.SelectedSlotPosition);
                Mediator.SendOrder();
            }
        }

        public override void Shuffle()
        {
            Mediator.SetState(new SeaportShuffleState(Mediator));
            Mediator.ShuffleOrder();
        }
        
        private bool IsCanSendOrder()
        {
            Price GetPrice()
            {
                var result = new Price
                {
                    SoftPrice = 0,
                    HardPrice = 0,
                    ProductsPrice = new Dictionary<Id<Product>, int>(),
                    PriceType = PriceType.Resources
                };

                var slotModel = Mediator.jSeaportOrderDeskService.GetSlot(Mediator.SelectedSlotPosition);
                foreach (ProductRequirementItem requirementItem in slotModel.ProductOrderItems)
                {
                    result.ProductsPrice.Add(new Id<Product>(requirementItem.ProductID), requirementItem.Count);
                }

                return result;
            }

            Mediator.SetOrdersInteractable(false);
            var price = GetPrice();
            if (!Mediator.jPlayerProfile.PlayerResources.IsEnoughtForBuy(price))
            {
                Mediator.jPopupManager.Show(new NotEnoughResourcesPopupSettings(price), _ => Mediator.RefreshCurrentSlot());
                Mediator.SetOrdersInteractable(true);
                return false;
            }

            return true;
        }
    }
}