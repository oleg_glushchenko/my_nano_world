using System;
using NanoReality.Game.UI;
using UniRx;

namespace NanoReality.Game.Services.OrderDesk.SeaportOrderDesk
{
    public class SeaportShuffleState : SeaportState
    {
        private IDisposable _shuffleDisposable;
        public SeaportShuffleState(SeaportPanelMediator mediator) : base(mediator)
        {
            var orderView = Mediator.GetOrderViewByPosition(Mediator.SelectedSlotPosition);
            _shuffleDisposable = Mediator.jSeaportOrderDeskService.SeaportCollection.ObserveShuffle()
                .Where(slot => slot.NewValue.ServerPosition == orderView.ServerPosition)
                .Subscribe(slot =>
                {
                    Mediator.OnShuffleFinished(slot);
                    ToIdle();
                });
        }

        public override void Select(int orderServerPosition)
        {
            Mediator.SelectOrder(orderServerPosition);
            var slotModel = Mediator.jSeaportOrderDeskService.GetSlot(orderServerPosition); 
            if (slotModel.IsShuffling)
            {
                Mediator.SetState(new SeaportShuffleState(Mediator));
            }
            else
            {
                Mediator.SetState(new SeaportIdleState(Mediator));
            }
        }

        private void ToIdle()
        {
            _shuffleDisposable.Dispose();
            Mediator.SetState(new SeaportIdleState(Mediator));
        }

        public override void Send() { }

        public override void Shuffle() { }

        public override void SkipByAd() { }

        public override void SkipByPremium()
        {
            Mediator.BuySkipShuffleWaitingTime();
        }
    }
}