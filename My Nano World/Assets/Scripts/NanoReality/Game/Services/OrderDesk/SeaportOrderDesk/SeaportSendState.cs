using System;
using NanoReality.Game.UI;
using UniRx;

namespace NanoReality.Game.Services.OrderDesk.SeaportOrderDesk
{
    public class SeaportSendState : SeaportState
    {
        private IDisposable _sendDisposable;
        public SeaportSendState(SeaportPanelMediator mediator) : base(mediator)
        {

            var orderView = Mediator.GetOrderViewByPosition(Mediator.SelectedSlotPosition);
            _sendDisposable = Mediator.jSeaportOrderDeskService.SeaportCollection.ObserveSend()
                .Where(slot => slot.NewValue.ServerPosition == orderView.ServerPosition)
                .Subscribe(slot =>
                {
                    Mediator.OnOrderCompleteNetworkCallback(slot, ToIdle);
                });
        }

        public override void Select(int orderServerPosition) { }

        public override void Send() { }

        public override void Shuffle() { }

        private void ToIdle()
        {
            _sendDisposable.Dispose();
            Mediator.SetState(new SeaportIdleState(Mediator));
        }
    }
}