using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using UniRx;

namespace NanoReality.Game.Services.OrderDesk.SeaportOrderDesk
{
    public interface ISeaportOrderSlot
    {
        Id<ISeaportOrderSlot> OrderId { get; set; }
        
        int ServerPosition { get; set; }
        
        int FreeSkip { get; set; }
        
        int FreeSkipTotal { get; set; }
        
        SeaportAward Awards { get; set; }
        
        List<ProductRequirementItem> ProductOrderItems { get; set; }
        
        bool IsShuffling { get; }
        
        long ShuffleEndTime { get; set; }
        
        ReactiveProperty<long> ShuffleTimeLeft { get; }
    }
}