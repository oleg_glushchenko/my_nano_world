using System.Collections.Generic;
using Newtonsoft.Json;

namespace NanoReality.Game.Services.OrderDesk.SeaportOrderDesk
{
    public class SeaportAward : ISeaportAward
    {
        [JsonProperty("coins")]
        public int Coins { get; set; }
        [JsonProperty("exp")]
        public int Experience { get; set; }
        [JsonProperty("product")]
        public AwardProduct Product { get; set; }
        [JsonProperty("products")]
        public List<AwardProduct> Products { get; set; }
    }
}