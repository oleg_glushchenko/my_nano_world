﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.ServerCommunication.Models;
using Assets.NanoLib.Utilities.Serializers;
using NanoLib.Core.Logging;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Configs;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.StrangeIoC;
using strange.extensions.signal.impl;

namespace NanoReality.Game.Services.OrderDesk.SeaportOrderDesk
{
    public sealed class LoadSeaportOrderCommandArgs : CommandArgs
    {
        public readonly Action<NetworkSeaportOrderDesk> CompleteCallback;

        public LoadSeaportOrderCommandArgs(Action<NetworkSeaportOrderDesk> completeCallback)
        {
            CompleteCallback = completeCallback;
        }
    }
    
    public sealed class LoadSeaportOrdersCommand : AGameCommand
    {
        public sealed class ExecuteCommandSignal : Signal<LoadSeaportOrderCommandArgs>
        {
        }

        [Inject] public IHintTutorial jHintTutorial { get; set; }
        [Inject] public IServerRequest jServerRequest { get; set; }
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public IStringSerializer jStringSerializer { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public LoadSeaportOrderCommandArgs jCommandArgs { get; set; }
        [Inject] public IGameConfigData jGameConfigData { get; set; }

        private HintType _sendStep = HintType.SendSeaportOrder;
        private HintType _shuffleStep = HintType.SeaportOrderDialog;
        private int TutorialBuildingId;

        public override void Execute()
        {
            base.Execute();

            if (!jHintTutorial.IsHintCompleted(_sendStep))
            {
                TutorialActionData actionData = jGameConfigData.TutorialConfigDate.HintStepActionData
                    ?.FirstOrDefault(a => a.StepId == (int) HintType.StartSeaport)?.ActionData[0];
                int buildingId = actionData.BuildingData[0].Id;
                var mapObjectType = actionData.BuildingData[0].MapObjectType;
                var businessSectorsType = actionData.BusinessSectorsType;
                IMapObject mapObject = jGameManager.FindMapObjectOnMapByType(businessSectorsType,
                    mapObjectType, buildingId);

                if (mapObject == null || mapObject.Level < 1)
                {
                    Logging.Log(LoggingChannel.Tutorial, $"Can't load tutorial seaport, building for upgrade not found. Id:[{buildingId.ToString()}]]");
                }
                else
                {
                    LoadTutorialOrder(mapObject);
                    Retain();
                    return;
                }
            }

            LoadOrders();
            Retain();
        }

        private void LoadOrders()
        {
            const string url = "/v1/user/seaport";

            jServerRequest.SendRequest(RequestMethod.GET, url, OnOrderDeskLoaded, OnRequestError);
        }

        private void LoadTutorialOrder(IMapObject targetBuilding)
        {
            List<IProductForUpgrade> productsForUpgrade = ((IMapBuilding)targetBuilding).ProductsForUpgrade;

            string url = $"/v1/tutorial/seaport/{productsForUpgrade[0].ProductID}";

            jServerRequest.SendRequest(RequestMethod.GET, url, OnOrderDeskLoaded, OnRequestError);
        }

        private void OnOrderDeskLoaded(string responseText)
        {
            var orderDesk = (NetworkSeaportOrderDesk) jStringSerializer.Deserialize(responseText, typeof(NetworkSeaportOrderDesk));
            jCommandArgs.CompleteCallback?.Invoke(orderDesk);
            Release();
        }

        private void OnRequestError(IErrorData errorData)
        {
            jServerCommunicator.OnRequestError(errorData);
        }
    }
}