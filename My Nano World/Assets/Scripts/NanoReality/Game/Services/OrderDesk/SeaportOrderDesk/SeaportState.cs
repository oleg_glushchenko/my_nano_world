using NanoReality.Game.UI;

namespace NanoReality.Game.Services.OrderDesk.SeaportOrderDesk
{
    public abstract class SeaportState
    {
        protected SeaportPanelMediator Mediator;

        public SeaportState(SeaportPanelMediator mediator)
        {
            Mediator = mediator;
        }

        public abstract void Select(int orderServerPosition);
        public abstract void Send();
        public abstract void Shuffle();
        public virtual void SkipByAd() {}
        public virtual void SkipByPremium() {}
    }
}