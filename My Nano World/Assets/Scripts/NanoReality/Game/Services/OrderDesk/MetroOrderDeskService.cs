using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UtilitesEngine;
using Assets.Scripts.Engine.Analytics.Model.api;
using DG.Tweening;
using NanoLib.Core.Timers;
using NanoReality.Game.Services;
using NanoReality.Game.Services.BuildingsUnlockService;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.Game.UI;
using NanoReality.GameLogic.Configs;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Impl;
using NanoReality.GameLogic.ServerCommunication.Models;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.StartApplicationSequence.Controllers.Signals;
using Newtonsoft.Json;
using UnityEngine;

namespace NanoReality.GameLogic.OrderDesk
{
    public interface IMetroOrderDeskService : IGameService
    {
        bool IsMetroLocked { get; }
        NetworkMetroOrderDesk MetroOrderDesk { get; }
        
        void UpdateOrderDesk(Action onSuccess = null);

        void ShuffleOrder(int orderId, Action onSuccess = null);

        void SkipShuffleOrder(int orderId, int price, Action onSuccess = null);
        
        void ShippingOrder(int trainId, Action< NetworkMetroOrderTrain> onSuccess = null);
        
        void SkipWaitingTime(MetroOrderTrainModel order, Action<int> onComplete = null);
        
        void LoadSlotProducts(int orderSlotId, Action<NetworkMetroOrderTrain> callback = null);
    }

    public sealed class MetroConfig
    {
        [JsonProperty("shuffle_time")]
        public float ShuffleTime;

        [JsonProperty("shuffle_skip_price")]
        public int ShuffleSkipPrice;
    }
    
    public sealed class MetroOrderDeskService : IMetroOrderDeskService
    {
        [Inject] public MetroOrderDeskUpdatedSignal jMetroOrderDeskUpdatedSignal { get; set; }
        [Inject] public MetroOrderSlotWaitingFinishedSignal jMetroOrderSlotWaitingFinishedSignal { get; set; }
        [Inject] public MetroOrderSlotLoadedSignal jMetroOrderLoadedSignal { get; set; }
        [Inject] public MetroOrderShippingSignal jMetroOrderShippingSignal { get; set; }
        [Inject] public MetroOrderShuffledSignal jMetroOrderShuffleSignal { get; set; }
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public FinishApplicationInitSignal jFinishInitSignal { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public UserLevelUpSignal LevelUpSignal { get; set; }
        [Inject] public IBuildingsUnlockService jBuildingsUnlockService { get; set; }
        [Inject] public IGameConfigData jGameConfigData { get; set; }
        [Inject] public INanoAnalytics jNanoAnalytics { get; set; }
        [Inject] public IHintTutorial jHintTutorial { get; set; }
        [Inject] public ITimerManager jTimeManager { get; set; }

        private const int MetroBuildingId = 135;
        private const HintType TutorialRewardStep = HintType.GetMetroRewardHint;
        private const HintType TutorialSendStep = HintType.SendMetroHint;
        private long _finishTimerTime;
        private Dictionary<int, ITimer> _trainTimers = new Dictionary<int, ITimer>();

        public bool IsMetroLocked { get; set; }
        public NetworkMetroOrderDesk MetroOrderDesk { get; private set; }

        private MetroConfig MetroConfig => jGameConfigData.MetroConfig;

        public void Init()
        {
            jFinishInitSignal.AddOnce(() =>
            {
                UpdateOrderDesk(() =>
                {
                    foreach (var train in MetroOrderDesk.OrderTrains)
                    {
                        long endTime = train.ShuffleEndTime != default ? train.ShuffleEndTime : train.WaitingEndTime;

                        if (endTime != default)
                        {
                            var waitTime = endTime - jTimeManager.CurrentUTC;
                            StartTrainTimer(train.Id, waitTime);
                        }
                    }
                });
            });

            LevelUpSignal.AddListener(OnUserLevelUp);
        }

        public void Dispose()
        {
            LevelUpSignal.RemoveListener(OnUserLevelUp);
        }

        private void OnUserLevelUp()
        {
            UpdateOrderDesk();
        }

        public void UpdateOrderDesk(Action onSuccess = null)
        {
            void OnLoadComplete(NetworkMetroOrderDesk orderDesk)
            {
                MetroOrderDesk = orderDesk;
                jMetroOrderDeskUpdatedSignal.Dispatch(orderDesk);
                onSuccess?.Invoke();
            }

            int metroUnlockLevel = jBuildingsUnlockService.GetBuildingUnlockLevel(MetroBuildingId);

            if (jPlayerProfile.CurrentLevelData.CurrentLevel >= metroUnlockLevel)
            {
                if (!jHintTutorial.IsHintCompleted(TutorialRewardStep))
                {
                    jServerCommunicator.LoadMetroTutorialOrderDesk(jHintTutorial.IsHintCompleted(TutorialSendStep),
                        OnLoadComplete);
                    return;
                }

                jServerCommunicator.LoadMetroOrderDesk(OnLoadComplete);
            }
        }

        public void ShuffleOrder(int orderId, Action onSuccess)
        {
            void OnShuffleComplete(NetworkMetroOrderDesk orderDesk)
            {
                MetroOrderDesk = orderDesk;
                onSuccess?.Invoke();

                jMetroOrderShuffleSignal.Dispatch(orderId);
                jMetroOrderDeskUpdatedSignal.Dispatch(orderDesk);

                NetworkMetroOrderTrain metroOrderTrain =
                    orderDesk.OrderTrains.FirstOrDefault(c => c.Id == orderId);
                
                var waitTime = metroOrderTrain.ShuffleEndTime - jTimeManager.CurrentUTC;
                StartTrainTimer(orderId, waitTime, OnTimerFinish);
            }

            void OnTimerFinish(int trainId)
            {
                string trainIndex = MetroOrderDesk.OrderTrains.FindIndex((train) => train.Id == orderId).ToString();
                jNanoAnalytics.OnShuffle("Metro", "By waiting", trainIndex.ToString());
            }

            NetworkMetroOrderTrain networkMetroOrderTrain = MetroOrderDesk.FindTrain(orderId);
            networkMetroOrderTrain.ShuffleEndTime = jTimeManager.CurrentUTC + Convert.ToInt64(MetroConfig.ShuffleTime);

            jServerCommunicator.ShuffleMetroOrder(orderId, OnShuffleComplete);
        }

        public void SkipShuffleOrder(int orderId, int price, Action onSuccess)
        {
            bool isPaid = price > 0;

            void OnSkipComplete(NetworkMetroOrderDesk orderDesk)
            {
                string trainId = MetroOrderDesk.OrderTrains.FindIndex((train) => train.Id == orderId).ToString();
                jNanoAnalytics.OnShuffle("Metro", price.ToString(), trainId.ToString());
                MetroOrderDesk = orderDesk;

                if (isPaid)
                {
                    jPlayerProfile.PlayerResources.BuyHardCurrency(price);
                    jNanoAnalytics.OnPlayerCurrenciesDecrease(PlayerResources.CurencyType.Bucks, price,
                        CurrenciesSpendingPlace.SkipShuffleMetro,
                        orderId.ToString());
                }

                jMetroOrderDeskUpdatedSignal.Dispatch(orderDesk);
                onSuccess?.Invoke();
            }

            FinishTimer(orderId);

            jServerCommunicator.SkipShuffleMetroOrder(orderId, isPaid, OnSkipComplete);
        }

        public void ShippingOrder(int trainId, Action<NetworkMetroOrderTrain> onSuccess = null)
        {
            IsMetroLocked = true;
            int trainOrderIndex = MetroOrderDesk.OrderTrains.FindIndex(c => c.Id == trainId);

            void OnShippingComplete(NetworkMetroOrderDesk orderDesk)
            {
                jNanoAnalytics.OnOrderSent("Metro", trainId, "gold");
                MetroOrderDesk = orderDesk;
                NetworkMetroOrderTrain train = MetroOrderDesk.OrderTrains[trainOrderIndex];

                IsMetroLocked = false;
                jMetroOrderDeskUpdatedSignal.Dispatch(orderDesk);
                jMetroOrderShippingSignal.Dispatch(trainId, train);
                onSuccess?.Invoke(train);
                
                var waitTime = train.WaitingEndTime - jTimeManager.CurrentUTC;
                StartTrainTimer(train.Id, waitTime);
            }

            if (!jHintTutorial.IsHintCompleted(TutorialRewardStep))
            {
                jServerCommunicator.ShippingMetroTutorialOrder(trainId, OnShippingComplete);
                return;
            }

            jServerCommunicator.ShippingMetroOrder(trainId, OnShippingComplete);
        }

        public void SkipWaitingTime(MetroOrderTrainModel order, Action<int> onComplete = null)
        {
            void OnSkipComplete(NetworkMetroOrderDesk orderDesk)
            {
                MetroOrderDesk = orderDesk;
                jMetroOrderSlotWaitingFinishedSignal.Dispatch(order.Id);
                onComplete?.Invoke(order.Id);
                jMetroOrderDeskUpdatedSignal.Dispatch(orderDesk);
                jPlayerProfile.PlayerResources.BuyHardCurrency(order.WaitingSkipPrice.Value);
                jNanoAnalytics.OnPlayerCurrenciesDecrease(PlayerResources.CurencyType.Bucks,
                    order.WaitingSkipPrice.Value,
                    CurrenciesSpendingPlace.SkipWaitingMetro, order.Id.ToString());
            }
            
            FinishTimer(order.Id);
            jServerCommunicator.SkipMetroOrderWaitingTime(order.Id, OnSkipComplete);
        }

        public void LoadSlotProducts(int orderSlotId, Action<NetworkMetroOrderTrain> callback = null)
        {
            NetworkOrderSlot requiredProducts = MetroOrderDesk.FindParentTrain(orderSlotId).FindSlot(orderSlotId);
            jPlayerProfile.PlayerResources.RemoveProduct(requiredProducts.ProductId, requiredProducts.Count);

            void OnLoadComplete(NetworkMetroOrderDesk orderDesk)
            {
                MetroOrderDesk = orderDesk;
                NetworkMetroOrderTrain metroOrderTrain =
                    orderDesk.OrderTrains.First(c => c.OrderSlots.Any(s => s.Id == orderSlotId));

                callback?.Invoke(metroOrderTrain);
                jMetroOrderDeskUpdatedSignal.Dispatch(orderDesk);
                jMetroOrderLoadedSignal.Dispatch(metroOrderTrain.OrderSlots.Find(c => c.Id == orderSlotId));
            }

            if (!jHintTutorial.IsHintCompleted(TutorialRewardStep))
            {
                jServerCommunicator.LoadMetroTutorialProductsToSlot(orderSlotId, OnLoadComplete);
                return;
            }

            jServerCommunicator.LoadMetroProductsToSlot(orderSlotId, OnLoadComplete);
        }
        
        public void StartTrainTimer(int trainId, float duration, Action<int> callback = null)
        {
            float syncDuration = duration + 1;
            ITimer timer = jTimeManager.StartRealTimer(syncDuration, OnTrainTimerFinish);
            
            if (!_trainTimers.ContainsKey(trainId))
            {
                _trainTimers.Add(trainId, timer);
            }
            else
            {
                _trainTimers[trainId].CancelTimer();
                _trainTimers[trainId] = timer;
            }

            void OnTrainTimerFinish()
            {
                callback?.Invoke(trainId);
                FinishTimer(trainId);
                UpdateOrderDesk();
            }
        }

        private void FinishTimer(int trainId)
        {
            if (_trainTimers.TryGetValue(trainId, out ITimer trainTimer))
            {
                trainTimer.CancelTimer();
                _trainTimers.Remove(trainId);
            }
        }
    }
}
