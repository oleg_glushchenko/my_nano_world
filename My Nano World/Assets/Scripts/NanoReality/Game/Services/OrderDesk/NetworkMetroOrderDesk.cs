using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using strange.extensions.signal.impl;
using UnityEngine;

namespace NanoReality.GameLogic.OrderDesk
{
    public sealed class MetroOrderDeskUpdatedSignal : Signal<NetworkMetroOrderDesk> { }
    
    public sealed class MetroOrderSlotWaitingFinishedSignal : Signal<int> { }
    
    public sealed class MetroOrderTrainShippedSignal : Signal<int> { }

    public sealed class MetroOrderShuffledSignal : Signal<int> { }

    public sealed class NetworkMetroOrderDesk
    {
        [JsonProperty("trains")]
        public List<NetworkMetroOrderTrain> OrderTrains;

        [JsonProperty("skip_price")]
        public int SkipPrice;

        [JsonProperty("skip_time")] 
        public int SkipTime;

        [JsonProperty("timer")]
        public int Timer;

        public NetworkMetroOrderTrain FindTrain(int trainId)
        {
            return OrderTrains.FirstOrDefault(c => c.Id == trainId);
        }

        public NetworkMetroOrderTrain FindParentTrain(int slotId)
        {
            return OrderTrains.Find(train => train.OrderSlots.Any(slot => slot.Id == slotId));
        }
    }

    public sealed class NetworkMetroOrderTrain
    {
        [JsonProperty("id")]
        public int Id;

        [JsonProperty("done_status")]
        public bool IsDone;

        [JsonProperty("reward")]
        public ProductData Reward;

        [JsonProperty("skip_price")]
        public int SkipPrice;
        
        [JsonProperty("skip_count")]
        public int SkipCount;
        
        /// <summary>
        /// Date time when waiting new train timer started.
        /// </summary>
        [JsonProperty("waiting_start")]
        public long WaitingStartTime; 
        
        [JsonProperty("waiting_end")]
        public long WaitingEndTime;

        /// <summary>
        /// How many seconds left to wait new train.
        /// </summary>
        [JsonProperty("left_time")]
        public int LeftTime;
        
        /// <summary>
        /// Date time when shuffle timer started.
        /// </summary>
        [JsonProperty("shuffle_start")]
        public long ShuffleStartTime;
        
        /// <summary>
        /// How many second left to shuffle waiting end.
        /// </summary>
        [JsonProperty("shuffle_end")]
        public long ShuffleEndTime;

        [JsonProperty("crates")]
        public List<NetworkOrderSlot> OrderSlots;

        public NetworkOrderSlot FindSlot(int slotId)
        {
            return OrderSlots.Find(c => c.Id == slotId);
        }
    }

    public sealed class NetworkOrderSlot
    {
        [JsonProperty("id")]
        public int Id;
        
        [JsonProperty("product_id")]
        public int ProductId;
        
        [JsonProperty("product_count")]
        public int Count;
        
        [JsonProperty("type")]
        public int Type;
        
        [JsonProperty("fill_status")]
        public bool IsFilled;
    }
}