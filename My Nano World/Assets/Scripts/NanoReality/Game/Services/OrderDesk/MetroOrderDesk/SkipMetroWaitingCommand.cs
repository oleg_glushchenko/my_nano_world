﻿using System;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.StrangeIoC;
using strange.extensions.signal.impl;

namespace NanoReality.Game.OrderDesk
{
    public sealed class BuyProductsActionSignal : Signal<Price, Action<bool>> { }

    public sealed class BuyProductsActionCommand : AGameCommand
    {
        [Inject] public Price jProductsPrice { get; set; }
        [Inject] public Action<bool> jResultCallback { get; set; }
        [Inject] public IPopupManager jPopupManager { get; set; }
        
        public override void Execute()
        {
            base.Execute();
            Retain();
            jPopupManager.Show(new NotEnoughResourcesPopupSettings(jProductsPrice), OnPopupResultCallback);
        }

        private void OnPopupResultCallback(PopupResult popupResult)
        {
            jResultCallback.Invoke(popupResult == PopupResult.Ok);
            Release();
        }
    }
}