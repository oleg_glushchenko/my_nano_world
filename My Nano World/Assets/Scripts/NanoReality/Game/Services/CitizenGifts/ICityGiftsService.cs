using System;
using System.Collections.Generic;
using NanoReality.Game.Services;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace NanoReality.Game.CitizenGifts
{
    public interface ICityGiftsService : IGameService
    {
        event Action GiftsUpdates;
        
        bool HasAvailableGifts(int buildingMapId);
        bool IsAnyGiftsAvailable {get;}
        IEnumerable<CitizenGift> GetGifts(int buildingMapId);
        void ClaimGift(int buildingMapId, int giftId, Action onSuccess = null);
        IMapBuilding GetBuildWithGift();
        void LoadAvailableGifts(Action onComplete);
    }
}