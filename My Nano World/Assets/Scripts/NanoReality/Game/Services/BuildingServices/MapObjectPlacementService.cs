﻿using System;
using System.Collections.Generic;
using GameLogic.BuildingSystem.CityArea;
using NanoLib.Core.Logging;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.Services;
using UnityEngine;

namespace NanoReality.Game.Services.BuildingServices
{
    public class MapObjectPlacementService : IMapObjectPlacementService
    {
        [Inject] public IGameManager jGameManager {get; set;}
        [Inject] public ICitySectorService jCitySectorService { get; set; }
        
        private readonly ServiceContext _context = new ServiceContext();

        public void SetContextData(IMapObject mapObject)
        {
            var sectorType = mapObject.SectorId.ToBusinessSectorType();
            _context.SectorAreas = jCitySectorService.SectorAreas(sectorType);

            _context.CityMap = jGameManager.GetMap(mapObject.SectorId);
            _context.ObjectTypeId = mapObject.ObjectTypeID;
            _context.BuildingMapId = mapObject.MapID;
            
            _context.BuildingDimension = new Vector2Int(mapObject.Dimensions.intX, mapObject.Dimensions.intY);
            
            _context.FoundPosition = null;
        }
        
        public Vector2Int FindConstructPosition(Vector2Int startFoundPosition)
        {
            FindPosition(startFoundPosition, Constructable);
            if(_context.FoundPosition != null)
            {
                return _context.FoundPosition.Value;
            }

            return FindEnabledPosition(startFoundPosition);
        }

        public bool HasConstructionPosition()
        {
            FindPosition(Vector2Int.zero, Constructable);

            return _context.FoundPosition != null;
        }

        public Vector2Int FindEnablePosition(Vector2Int newGridPos, Vector2Int oldGridPos)
        {
            return FindPosition(newGridPos, oldGridPos, Movable);
        }
        
        public bool Constructable(Vector2Int newGridPos)
        {
            if (!Movable(newGridPos))
                return false;

            if (_context.CityMap.SubLockedMap.ContainsLockedSectors(newGridPos, _context.BuildingDimension))
                return false;

            if (ContainsOtherMapObjects(newGridPos))
                return false;

            return true;
        }

        public bool Movable(Vector2Int newGridPos)
        {
            Vector2Int buildingDimensions = _context.BuildingDimension;
            if(!_context.CityMap.jCityGrid.Contains(newGridPos, buildingDimensions))
                return false;

            if (jCitySectorService.ContainsDisabledAreas(newGridPos, buildingDimensions, _context.SectorAreas, _context.ObjectTypeId))
                return false;
            
            if (!jCitySectorService.IsInsideOfDragArea(newGridPos, buildingDimensions))
                return false;

            return true;
        }
        
        private Vector2Int FindEnabledPosition(Vector2Int startFoundPosition)
        {
            FindPosition(startFoundPosition, Movable);
            if(_context.FoundPosition != null)
            {
                return _context.FoundPosition.Value;
            }

            Logging.LogError(LoggingChannel.Debug,$"There is no existent grid position for this building with ObjectTypeID {_context.ObjectTypeId}");
            return Vector2Int.zero;
        }

        private void FindPosition(Vector2Int startFoundPosition, Func<Vector2Int, bool> availableStrategy)
        {
            Vector2Int mapDimension = _context.CityMap.jCityGrid.Dimensions;
            startFoundPosition.Clamp(Vector2Int.zero, mapDimension - _context.BuildingDimension);
            
            if (availableStrategy(startFoundPosition))
            {
                _context.FoundPosition = startFoundPosition;
                return;
            }
            
            var pathDelta = 1;

            while (pathDelta <= mapDimension.x || pathDelta <= mapDimension.y)
            {
                for (int y = startFoundPosition.y - pathDelta; y <= startFoundPosition.y + pathDelta; y++)
                {
                    var leftPos = new Vector2Int(startFoundPosition.x - pathDelta, y);
                    
                    if (availableStrategy(leftPos))
                    {
                        _context.FoundPosition = leftPos;
                        return;
                    }
                    
                    var rightPos = new Vector2Int(startFoundPosition.x + pathDelta, y);
                    
                    if (availableStrategy(rightPos))
                    {
                        _context.FoundPosition = rightPos;
                        return;
                    }
                }
                
                for (int x = startFoundPosition.x - pathDelta; x < startFoundPosition.x + pathDelta; x++)
                {
                    var bottomPos = new Vector2Int(x, startFoundPosition.y - pathDelta);
                    
                    if (availableStrategy(bottomPos))
                    {
                        _context.FoundPosition = bottomPos;
                        return;
                    }
                    
                    var topPos = new Vector2Int(x, startFoundPosition.y + pathDelta);
                    
                    if (availableStrategy(topPos))
                    {
                        _context.FoundPosition = topPos;
                        return;
                    }
                }

                pathDelta++;
            }
        }

        private Vector2Int FindPosition(Vector2Int startFoundPosition, Vector2Int oldGridPos, Func<Vector2Int, bool> availableStrategy)
        {
            Vector2Int mapDimension = _context.CityMap.jCityGrid.Dimensions;
            startFoundPosition.Clamp(Vector2Int.zero, mapDimension - _context.BuildingDimension);
            
            if (availableStrategy(startFoundPosition))
            {
                return startFoundPosition;
            }
            
            Vector2Int intermediateGridPos = oldGridPos;
            Vector2Int bufferPos = oldGridPos;
            bool moveAllowed;

            moveAllowed = true;
            while (moveAllowed && bufferPos.x != startFoundPosition.x)
            {
                if (bufferPos.x < startFoundPosition.x) bufferPos.x++;
                if (bufferPos.x > startFoundPosition.x) bufferPos.x--;

                moveAllowed = availableStrategy(bufferPos);
                if (moveAllowed)
                {
                    intermediateGridPos = bufferPos;
                }
            }
            
            bufferPos = intermediateGridPos;

            moveAllowed = true;
            while (moveAllowed && bufferPos.y != startFoundPosition.y)
            {
                if (bufferPos.y < startFoundPosition.y) bufferPos.y++;
                if (bufferPos.y > startFoundPosition.y) bufferPos.y--;

                moveAllowed = availableStrategy(bufferPos);
                if (moveAllowed)
                {
                    intermediateGridPos = bufferPos;
                }
            }
            
            return availableStrategy(intermediateGridPos) ? intermediateGridPos : oldGridPos;
        }
        
        private bool ContainsOtherMapObjects(Vector2Int gridPos)
        {
            Vector2Int startGrid = gridPos;
            Vector2Int endGrid = gridPos + (_context.BuildingDimension - Vector2Int.one);
            return _context.CityMap.jCityGrid.ContainsOtherMapObjects(startGrid, endGrid, _context.BuildingMapId);
        }
        
        private sealed class ServiceContext
        {
            public ICityMap CityMap { get; set; }
            
            public int BuildingMapId { get; set; }
            
            public int ObjectTypeId { get; set; }
            
            public IReadOnlyList<ISectorArea> SectorAreas { get; set; }
            
            public Vector2Int BuildingDimension { get; set; }
            
            public Vector2Int? FoundPosition { get; set; }
        }
    }
}
