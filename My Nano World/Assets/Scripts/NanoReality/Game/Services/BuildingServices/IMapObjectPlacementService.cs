﻿using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using UnityEngine;

namespace NanoReality.Game.Services.BuildingServices
{
    public interface IMapObjectPlacementService
    {
        void SetContextData(IMapObject mapObject);
        Vector2Int FindConstructPosition(Vector2Int startFoundPosition);

        Vector2Int FindEnablePosition(Vector2Int newGridPos, Vector2Int oldGridPos);
        bool HasConstructionPosition();

        bool Constructable(Vector2Int newGridPos);

        bool Movable(Vector2Int newGridPos);
    }
}
