using System.Collections.Generic;
using NanoLib.Core.Logging;
using Newtonsoft.Json;

namespace NanoReality.Game.FoodSupply
{
    public sealed class FoodSupplyConfig
    {
        [JsonProperty("unlock_level")] 
        public int UnlockLevel;

        [JsonProperty("products")] 
        public List<FoodSupplyProduct> Products;

        private Dictionary<int, FoodSupplyProduct> _cachedProducts;

        private Dictionary<int, FoodSupplyProduct> CachedProducts => _cachedProducts ?? (_cachedProducts = CacheProducts());

        public FoodSupplyProduct GetProduct(int productId)
        {
            if (CachedProducts.TryGetValue(productId, out var productConfig))
            {
                return productConfig;
            }

            $"Config for Food SupplyProduct [{productId}] not found in FoodSupplyConfig.".LogError(LoggingChannel.Mechanics);
            return null;
        }

        public bool IsFoodSupplyProduct(int productId)
        {
            return CachedProducts.ContainsKey(productId);
        }

        private Dictionary<int, FoodSupplyProduct> CacheProducts()
        {
            var cachedProducts = new Dictionary<int, FoodSupplyProduct>(Products.Count);
            foreach (var productConfig in Products)
            {
                var productId = productConfig.Id;
                if (cachedProducts.ContainsKey(productId))
                {
                    $"Food Supply Product [{productId}] contains in FS Product list twice.".LogError(LoggingChannel.Mechanics);
                    continue;
                }

                cachedProducts.Add(productId, productConfig);
            }

            return cachedProducts;
        }
    }
}