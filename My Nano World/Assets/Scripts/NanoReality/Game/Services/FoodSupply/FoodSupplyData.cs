using System.Collections.Generic;
using System.Linq;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.OrderDesk;
using Newtonsoft.Json;

namespace NanoReality.Game.FoodSupply
{
    public class FoodSupplyData
    {
        /// <summary>
        /// FSProducts list that available for production in this FSBuilding.
        /// </summary>
        [JsonProperty("available_products")]
        public List<FoodSupplyProductRecipe> AvailableProducts;

        [JsonProperty("feeding_slots")]
        public List<FoodSupplyFeedingSlot> FeedingSlots;

        public FoodSupplyProductRecipe FindProduct(int recipeId)
        {
            return AvailableProducts.FirstOrDefault(c => c.RecipeId == recipeId);
        }
    }

    public sealed class FoodSupplyFeedingSlot
    {
        /// <summary>
        /// Ordinal slot number.
        /// </summary>
        [JsonProperty("id")]
        public int Id;

        [JsonProperty("time_multiplier")]
        public float CaloriesMultiplier;
        
        [JsonProperty("is_locked")]
        public bool IsLocked;
        
        [JsonProperty("unlock_price")]
        public Price UnlockPrice;
        
        [JsonProperty("unlock_level")]
        public int UnlockLevel;
    }
    
    public class FoodSupplyOrder
    {
        /// <summary>
        /// Food Supply product Id.
        /// </summary>
        [JsonProperty("food_supply_id")]
        public int FoodSupplyId;

        [JsonProperty("time_start")]
        public long TimeStarted;

        [JsonProperty("time_end")]
        public long TimeEnd;
    }
    
    public class FoodSupplyProductRecipe
    {
        /// <summary>
        /// Unique id of food supply product.
        /// </summary>
        [JsonProperty("recipe_id", Required = Required.Always)]
        public int RecipeId;
        
        /// <summary>
        /// Type of FSProduct.
        /// </summary>
        [JsonProperty("id", Required = Required.Always)]
        public int Id;

        /// <summary>
        /// Required products.
        /// </summary>
        [JsonProperty("products", Required = Required.Always)]
        public List<ProductData> Products;
    }

    public sealed class FoodSupplyProduct
    {
        [JsonProperty("id", Required = Required.Always)]
        public int Id;

        [JsonProperty("duration", Required = Required.Always)]
        public int Calories;
        
        [JsonProperty("slot_id")]
        public int SlotId;
    }
}