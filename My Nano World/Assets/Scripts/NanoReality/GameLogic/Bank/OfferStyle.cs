namespace NanoReality.GameLogic.Bank
{
    public enum OfferStyle
    {
        Common = 1,
        Uncommon = 2,
        Rare = 3,
        NanoPass = 4
    }
}