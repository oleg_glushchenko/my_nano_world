﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using Object = UnityEngine.Object;

public class UiFontSearch : EditorWindow
{
	[MenuItem("Tools/UI/Font Searcher")]
	public static void ShowWindow()
	{
		var window = GetWindow(typeof(UiFontSearch));
		window.minSize = new Vector2(600, 100);
	}
		
	private List<Text> lables;


	private Vector2 scrollPos = Vector2.zero;
	private Object fontPattern = null;
	private Object fontReplace = null;
	private Enum fontReplaceStyle = FontStyle.Normal;
	static private bool searchIncludesPrefabs;

	private void OnGUI()
	{
		EditorGUILayout.BeginVertical();
		fontPattern = EditorGUILayout.ObjectField("Font to search", fontPattern, typeof(Font), false);
		searchIncludesPrefabs = EditorGUILayout.Toggle("Including prefabs", searchIncludesPrefabs);
		fontReplace = EditorGUILayout.ObjectField("Font to replace with", fontReplace, typeof(Font), false);
		fontReplaceStyle = EditorGUILayout.EnumPopup("Replaced font style", fontReplaceStyle);
		if (GUILayout.Button("Search") && fontPattern != null)
		{
			UpdateLabelList();
		}

		if (GUILayout.Button("Replace") && fontPattern != null && fontReplace != null && lables.Count > 0)
		{
			foreach (var text in lables)
			{
				if (text == null)
				{
					continue;
				}

				text.font = (Font)fontReplace;
				text.fontStyle = (FontStyle)fontReplaceStyle;
			}

			foreach (var text in lables)
			{
				if (text == null)
				{
					continue;
				}

				var instanceRoot = PrefabUtility.FindRootGameObjectWithSameParentPrefab(text.gameObject);

				if (instanceRoot != null)
				{
					var targetPrefab = PrefabUtility.GetPrefabParent(instanceRoot);

					if (targetPrefab != null)
					{
						PrefabUtility.ReplacePrefab(instanceRoot, targetPrefab, ReplacePrefabOptions.ReplaceNameBased);
					}
					else
					{
						
					}
				}
			}
		}

		if ((lables != null && lables.Count > 0))
		{
			scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
			if (lables != null)
				for (int index = 0; index < lables.Count; index++)
				{
					if (lables[index] != null)
						lables[index] = EditorGUILayout.ObjectField(lables[index].name, lables[index], typeof(Text), true) as Text;
				}
			EditorGUILayout.EndScrollView();
		}

		EditorGUILayout.EndVertical();

	}

	private void UpdateLabelList()
	{
		lables = new List<Text>();
		var allLables = FindInScene<Text>();



		foreach (var label in allLables)
		{
			if (label.font != null && label.font == fontPattern)
				lables.Add(label);
		}
	}

	static List<T> FindInScene<T>() where T : Component
	{
		T[] comps = Resources.FindObjectsOfTypeAll(typeof(T)) as T[];

		List<T> list = new List<T>();

		foreach (T comp in comps)
		{
			if (comp.gameObject.hideFlags == 0)
			{
				if (!searchIncludesPrefabs && EditorUtility.IsPersistent(comp.gameObject))
				{
					continue;
				}

				list.Add(comp);
			}
		}
		return list;
	}
}