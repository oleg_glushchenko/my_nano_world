﻿using System.Collections.Generic;

namespace NanoReality.Editor.Defines
{
    public static class DefinesSetup
    {
        public static IEnumerable<string> Defines => new[]
        {
            "PRODUCTION",
            "DISABLE_CONFIRMATION",
            "ENABLE_LOG",
            "TMP_PRESENT",
            "ODIN_INSPECTOR",
            "DDNA_IOS_PUSH_NOTIFICATIONS_REMOVED",
            "TextMeshPro"
        };
        
        public static IEnumerable<string> Development => new[]
        {
            "DISABLE_CONFIRMATION",
            "ENABLE_LOG",
            "TMP_PRESENT",
            "DDNA_IOS_PUSH_NOTIFICATIONS_REMOVED",
            "TextMeshPro"
        };

        public static IEnumerable<string> Test => new[]
        {
            "ENABLE_LOG",
            "TMP_PRESENT",
            "DDNA_IOS_PUSH_NOTIFICATIONS_REMOVED",
            "TextMeshPro"
        };
        
        public static IEnumerable<string> ProductionTest => new[]
        {
            "ENABLE_LOG",
            "TMP_PRESENT",
            "DDNA_IOS_PUSH_NOTIFICATIONS_REMOVED",
            "TextMeshPro"
        };
        
        public static IEnumerable<string> Production => new[]
        {
            "PRODUCTION",
            "TMP_PRESENT",
            "DDNA_IOS_PUSH_NOTIFICATIONS_REMOVED",
            "TextMeshPro"
        };
    }
}