﻿using System;
using System.IO;
using System.Linq;
using System.Xml;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;

public class BuildController
{
    static string[] _fixScenes;
    private static string _iosBuildTarget = "";

    [MenuItem("BDSM/Builds/Windows")]
    public static void PerformWindowsBuild()
    {
        PerformBuild(BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows, _fixScenes, BuildOptions.None);
    }

    [MenuItem("BDSM/Builds/Android")]
    public static void PerformAndroidBuild()
    {
        PerformBuild(BuildTargetGroup.Android, BuildTarget.Android, _fixScenes, BuildOptions.None);
    }

    [MenuItem("BDSM/Builds/iOS/Store")]
    public static void PerformIOsBuildAstore()
    {
        _iosBuildTarget = "app-store";
        PerformBuild(BuildTargetGroup.iOS, BuildTarget.iOS, _fixScenes, BuildOptions.None);
    }

    [MenuItem("BDSM/Builds/iOS/AdHoc")]
    public static void PerformIOsBuildAdHoc()
    {
        _iosBuildTarget = "ad-hoc";
        PerformBuild(BuildTargetGroup.iOS, BuildTarget.iOS, _fixScenes, BuildOptions.None);
    }

    private static string GetArg(string name)
    {
        var args = Environment.GetCommandLineArgs();
        for (int i = 0; i < args.Length; i++)
        {
            if (args[i] == name && args.Length > i + 1)
            {
                return args[i + 1];
            }
        }

        return null;
    }

    public static void PerformBuild(BuildTargetGroup buildTargetGroup, BuildTarget buildTarget, string[] scenes,
        BuildOptions buildOptions)
    {
        var outputDir = GetArg("-outputDir");

        if (string.IsNullOrEmpty(outputDir))
        {
            #if UNITY_EDITOR
            outputDir = "Builds";
            #else
            throw new Exception();
            #endif
        }

        _fixScenes = EditorBuildSettings.scenes.Where(x => x.enabled).Select(x => x.path).ToArray();

        EditorUserBuildSettings.SwitchActiveBuildTarget(buildTargetGroup, buildTarget);
        
        EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Gradle;

        BuildPipeline.BuildPlayer(_fixScenes, outputDir, buildTarget, buildOptions);
    }

    [PostProcessBuild(100)]
    public static void OnPostProcessBuild(BuildTarget target, string pathToBuiltProject)
    {
        if (target != BuildTarget.iOS)
        {
            return;
        }

        string fileName = "Info.plist";
        string fullPath = Path.Combine(pathToBuiltProject, fileName);

        XmlDocument doc = new XmlDocument();
        doc.Load(fullPath);

        if (doc == null)
        {
            Debug.LogError("Couldn't load " + fullPath);
            return;
        }

        XmlNode dict = FindPlistDictNode(doc);
        if (dict == null)
        {
            Debug.LogError("Error parsing " + fullPath);
            return;
        }

        AddChildElement(doc, dict, "key", "method");
        AddChildElement(doc, dict, "string", _iosBuildTarget);
        doc.Save(fullPath);
    }


    private static XmlNode FindPlistDictNode(XmlDocument doc)
    {
        XmlNode curr = doc.FirstChild;
        while (curr != null)
        {
            if (curr.Name.Equals("plist") && curr.ChildNodes.Count == 1)
            {
                XmlNode dict = curr.FirstChild;
                if (dict.Name.Equals("dict"))
                    return dict;
            }

            curr = curr.NextSibling;
        }

        return null;
    }

    private static XmlElement AddChildElement(XmlDocument doc, XmlNode parent, string elementName,
        string innerText = null)
    {
        XmlElement newElement = doc.CreateElement(elementName);
        if (innerText != null && innerText.Length > 0)
            newElement.InnerText = innerText;

        parent.AppendChild(newElement);
        return newElement;
    }
}