﻿using TMPro;
using UnityEditor;
using UnityEditor.Experimental.SceneManagement;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace NanoReality.Game.UI.Utility
{
    public class LangExtraLayoutEditor : EditorWindow
    {
        public static ExtraLayoutStorage ExtraLayoutStorage; 
        [MenuItem("Window/ExtraLayout/Open")]
        static void Init()
        {
            // Get existing open window or if none, make a new one:
            LangExtraLayoutEditor window = (LangExtraLayoutEditor) GetWindow(typeof(LangExtraLayoutEditor));
            window.Show();
            ExtraLayoutStorage = Resources.Load<ExtraLayoutStorage>("ExtraLayoutStorage");
            Debug.LogError(ExtraLayoutStorage);
        }
        
        [MenuItem("Window/ExtraLayout/Add extra layout &l")]
        static void DoSomethingWithAShortcutKey()
        {
            if(Selection.activeGameObject == null) return;
            if (ExtraLayoutStorage == null)
            {
                ExtraLayoutStorage = Resources.Load<ExtraLayoutStorage>("ExtraLayoutStorage");
            }

            if (!Selection.activeGameObject.GetComponent<TextMeshProUGUI>()) return;
            var extraLayout = Selection.activeGameObject.GetComponent<LangExtraLayout>();
            if (extraLayout == null)
            {
                extraLayout = Selection.activeGameObject.AddComponent<LangExtraLayout>();
            }

            var prefabLink =
                AssetDatabase.LoadAssetAtPath<GameObject>(PrefabStageUtility.GetCurrentPrefabStage().prefabAssetPath);
            var settings = Selection.activeGameObject.GetComponent<TextMeshProUGUI>();
            ExtraLayoutStorage.AddItem(extraLayout.PersistentID,
                new ExtraLayoutItem(Selection.activeGameObject.GetComponent<TextMeshProUGUI>()), prefabLink);
            extraLayout.SetLayoutStorageLink(ExtraLayoutStorage);
            extraLayout.SaveDefaultSettings(settings);
            AssetDatabase.SaveAssets();

            EditorSceneManager.MarkSceneDirty(PrefabStageUtility.GetCurrentPrefabStage().scene);
            ExtraLayoutStorage.UseKey();
            Debug.LogError("saved");
        }
    }
}