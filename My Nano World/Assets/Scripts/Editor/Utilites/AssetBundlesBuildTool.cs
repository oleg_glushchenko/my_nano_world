﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using NanoLib.Core.Logging;
using UnityEditor;
using UnityEditor.U2D;
using UnityEngine;
using UnityEngine.U2D;

namespace NanoReality.Utility.Editor
{
    public static class AssetBundlesBuildTool
    {
        private static readonly string[] _spriteAtlasesNames = {
            "BuildingsSprites",
            "CharacterSprites",
            "MapSprites",
            "ProductsSprites",
            "UiSprites",
            "TheEventRewardsSprites"
        };

        private static readonly Dictionary<BuildTarget, TextureImporterFormat> _lqFormats = new Dictionary<BuildTarget, TextureImporterFormat>()
        {
            {BuildTarget.Android, TextureImporterFormat.ETC2_RGBA8},
            {BuildTarget.iOS, TextureImporterFormat.ASTC_6x6},
            {BuildTarget.StandaloneWindows, TextureImporterFormat.RGBA32},
            {BuildTarget.StandaloneOSX, TextureImporterFormat.RGBA32}
        };
        
        private static readonly Dictionary<BuildTarget, TextureImporterFormat> _hqFormats = new Dictionary<BuildTarget, TextureImporterFormat>()
        {
            {BuildTarget.Android, TextureImporterFormat.RGBA32},
            {BuildTarget.iOS, TextureImporterFormat.RGBA32},
            {BuildTarget.StandaloneWindows, TextureImporterFormat.RGBA32},
            {BuildTarget.StandaloneOSX, TextureImporterFormat.RGBA32}
        };

        public static void BuildAssetBundles(string assetBundlesFolderPath, BuildTarget target, bool isHighQuality)
        {
            SetTexturesFormat(target, isHighQuality);

            var directoryPath = Path.Combine(assetBundlesFolderPath, GetTargetPlatformFolderName(target));
            
            ClearDirectory(assetBundlesFolderPath);
            Directory.CreateDirectory(directoryPath);

            var bundleManifest = BuildPipeline.BuildAssetBundles(directoryPath, BuildAssetBundleOptions.StrictMode, target);

            LogBuildingResult(target, bundleManifest, directoryPath);
            EditorUtility.RevealInFinder(assetBundlesFolderPath);
        }

        public static void ClearDirectory(string path)
        {
            if (Directory.Exists(path))
            {
                var directories = Directory.GetDirectories(path);

                foreach (var directoryPath in directories)
                {
                    FileUtil.DeleteFileOrDirectory(directoryPath);
                    FileUtil.DeleteFileOrDirectory(directoryPath + ".meta");
                }
            }
            else
                Directory.CreateDirectory(path);
        }

        private static string GetTargetPlatformFolderName(BuildTarget target)
        {
            switch (target)
            {
                case BuildTarget.StandaloneWindows:
                    return "Windows";
                
                case BuildTarget.iOS:
                    return "iOS";
                
                case BuildTarget.Android:
                    return "Android";
                
                case BuildTarget.StandaloneOSX:
                    return "StandaloneOSX";
                
                default:
                    throw new NotSupportedException($"Platform {target} is not supported!");
            }
        }
        
        private static List<SpriteAtlas> LoadAtlases(string[] atlasesNames)
        {
            var atlases = new List<SpriteAtlas>();
            foreach (var atlasName in atlasesNames)
            {
                var atlas = AssetDatabase.LoadAssetAtPath<SpriteAtlas>($"Assets/Art/Textures/{atlasName}.spriteatlas");
                if (atlas != null)
                {
                    atlases.Add(atlas);
                }
            }

            return atlases;
        }
        
        private static void SetTexturesFormat(BuildTarget target, bool isHighQuality)
        {
            var atlases = LoadAtlases(_spriteAtlasesNames);
            var platformName = GetTargetPlatformName(target);
            foreach (var atlas in atlases)
            {
                var texture = atlas.GetPlatformSettings(platformName);
                texture.format = isHighQuality ? _hqFormats[target] : _lqFormats[target];
                atlas.SetPlatformSettings(texture);
            }
        }

        public static void SetIncludeInBuildToggle(bool isInclude)
        {
            var atlases = LoadAtlases(_spriteAtlasesNames);
            foreach (var atlas in atlases)
            {
                atlas.SetIncludeInBuild(isInclude);
            }
        }

        private static string GetTargetPlatformName(BuildTarget target)
        {
            string targetName;
            switch (target)
            {
                case BuildTarget.StandaloneWindows:
                case BuildTarget.StandaloneOSX:
                    targetName = "Standalone";
                    break;
                case BuildTarget.Android:
                    targetName = "Android";
                    break;
                case BuildTarget.iOS:
                    targetName = "iPhone";
                    break;
                default:
                    targetName = "Default";
                    break;
            }

            return targetName;
        }
        
        private static void LogBuildingResult(BuildTarget target, AssetBundleManifest manifest, string path)
        {
            var resultMessage = new StringBuilder();
            resultMessage.Append("AssetBundles build finished.");
            resultMessage.AppendLine($"Platform: {target} Hash: {manifest.GetAssetBundleHash(target.ToString())}");
            resultMessage.AppendLine($"Export path: {path}");
            resultMessage.Log(LoggingChannel.AssetBundles);
        }
    }
}