﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using NanoLib.AssetBundles;
using I2.Loc;
using DeltaDNA;
using NanoLib.Core.Logging;
using NanoReality.Core.Resources;
using NanoReality.Editor.Defines;
using NanoReality.Engine.Settings.BuildSettings;
using NanoReality.GameLogic.Constants;
using NanoReality.Utility.Editor;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace NanoReality.Editor
{
	public class BuildTool : EditorWindow
	{
		private ResourcesPolicyType _resourcesPolicyType = ResourcesPolicyType.LocalAssetBundles;
		private BuildTargetPlatform _targetPlatform;
		private BuildType _buildType;
		private Company _company;
		private int _selectedServerIndex;
		private int _selectedLanguageIndex;
		private bool _buildBundles;
		private bool _useMasterServer;
		private string[] _companies;
		
		private static string[] _servers;
		private static StringBuilder _logs;
		private static ResourcesPolicyType _editorPolicyType;
		private static int _editorServerIndex;
		private static bool _editorUseMasterServer;
		private static bool _editorUseCustomServer;
		
		private const int DevDeltaDNAKey = 0;
		private const int LiveDeltaDNAKey = 1;
		private const string DebugConsoleName = "ConsoleWatcher";
		private const string BuildPath = "Builds/MyNanoWorld_{0}";
		private const string AssetBundlesFolderName = "AssetBundles";
		private const string MainScenePath = "Assets/Scenes/Main.unity";
		private const string DevelopLocalizationKey = "1BOf5izzbLg74gWJBJpTxQXR28EGocBB5C-3OF09NU9Q";
		private const string ReleaseLocalizationKey = "1EnPyHQYC1JMrHm7B2qX_SWbLKAPc-cvVFIP-Ed0Bd4Q";
		private const string DevelopLocalizationName = "I2Loc DEVELOP My Nano World Localization";
		private const string ReleaseLocalizationName = "I2Loc RELEASE My Nano World Localization";

		private readonly List<LanguageType> _availableLanguages = new List<LanguageType> {LanguageType.Arabic, LanguageType.English};
		private readonly List<CompanySettings> _companiesSettings = new List<CompanySettings>
		{
			new CompanySettings
			{
				company = Company.MBC,
				androidBundleIdentifier = "net.mbc.nanoreality",
				iOSBundleIdentifier = "net.mbc.nanoworld"
			},
			new CompanySettings
			{
				company = Company.NanoReality,
				androidBundleIdentifier = "com.nanorealitygames.mynanoworld",
				iOSBundleIdentifier = "com.nanorealitygames.mynanoworldalpha"
			}
		};

		private bool IsProductionBuild => _buildType == BuildType.Production || _buildType == BuildType.ProductionTest;
		private static string[] Servers => _servers ??= NetworkSettingsSO.ServersList.Where(server => !string.IsNullOrEmpty(server.Name)).Select(server => server.Name).ToArray();
		private static Configuration DeltaDNAConfigurationSO => ScriptableObjectsManager.Get<Configuration>("ddna_configuration");
		private static BuildSettings BuildSettingsSO => ScriptableObjectsManager.Get<BuildSettings>();
		private static NetworkSettings NetworkSettingsSO => ScriptableObjectsManager.Get<NetworkSettings>();
		private static LanguageSourceAsset LanguageSourceAsset => ScriptableObjectsManager.Get<LanguageSourceAsset>("I2Languages");
		private static string AssetBundlesPath => Path.Combine(Application.streamingAssetsPath, AssetBundlesFolderName);

		[MenuItem("Tools/NanoReality/Builds Tool &#b")]
		private static void OpenWindow()
		{
			_servers = null;

			GetWindow<BuildTool>("Build Tool");
		}
		
		protected void OnGUI()
		{
			DrawCommonSettings();
			DrawLanguagesPopup(_buildType);
			DrawGameVersionFields(_targetPlatform);
			DrawServerDropdown();
			DrawResourcesPolicySettings();
			DrawMasterServerCheckbox();
			DrawPlatformDependSettings(_targetPlatform);

			if (GUILayout.Button("Build"))
			{
				_logs = new StringBuilder();
				var target = GetBuildTarget(_targetPlatform);
				SetBuildSettings(target, _buildType, _company, _resourcesPolicyType);
				SetLocalesSheet(_buildType, OnLocalesSheetUpdated);

				void OnLocalesSheetUpdated()
				{
					if (_buildBundles)
						BuildBundles(target);

					Build(target, _buildType);
				}
			}
		}

		#region Labels GUI

		private void DrawCommonSettings()
		{
			_targetPlatform = (BuildTargetPlatform) EditorGUILayout.EnumPopup("Target: ", _targetPlatform);
			_buildType = (BuildType) EditorGUILayout.EnumPopup("Type: ", _buildType);
			_company = (Company) EditorGUILayout.EnumPopup("Company", _company);
		}

		private void DrawLanguagesPopup(BuildType type)
		{
			const string FieldNameText = "Languages: ";

			switch (type)
			{
				case BuildType.Test:
				case BuildType.Development:
					EditorGUILayout.BeginHorizontal();
					EditorGUILayout.LabelField(FieldNameText, GUILayout.MaxWidth(150));
					EditorGUILayout.LabelField(string.Join(", ", _availableLanguages));
					EditorGUILayout.EndHorizontal();
					break;

				case BuildType.ProductionTest:
				case BuildType.Production:
					_selectedLanguageIndex = EditorGUILayout.Popup(FieldNameText, _selectedLanguageIndex, _availableLanguages.Select(language => language.ToString()).ToArray());
					break;
			}
		}

		private void DrawServerDropdown()
		{
			_selectedServerIndex = EditorGUILayout.Popup("Server: ", _selectedServerIndex, Servers);
		}

		private void DrawResourcesPolicySettings()
		{
			if (IsProductionBuild)
				_resourcesPolicyType = ResourcesPolicyType.LocalAssetBundles;

			using (new EditorGUI.DisabledScope(IsProductionBuild))
				_resourcesPolicyType = (ResourcesPolicyType) EditorGUILayout.EnumPopup("Asset Bundles Loading:", _resourcesPolicyType);

			using (new EditorGUI.DisabledScope(_resourcesPolicyType != ResourcesPolicyType.LocalAssetBundles))
				_buildBundles = EditorGUILayout.Toggle("Build bundles: ", _buildBundles);
		}

		private static void DrawGameVersionFields(BuildTargetPlatform platform)
		{
			const int VersionFieldSize = 50;

			EditorGUILayout.BeginHorizontal();

			float.TryParse(PlayerSettings.bundleVersion, out var bundleVersion);
			PlayerSettings.bundleVersion = EditorGUILayout.FloatField("Version:", bundleVersion, GUILayout.ExpandWidth(false)).ToString(CultureInfo.CurrentCulture);

			switch (platform)
			{
				case BuildTargetPlatform.Android:
					PlayerSettings.Android.bundleVersionCode = EditorGUILayout.IntField(string.Empty, PlayerSettings.Android.bundleVersionCode, GUILayout.MaxWidth(VersionFieldSize));
					break;

				case BuildTargetPlatform.iOS:
					int.TryParse(PlayerSettings.iOS.buildNumber, out var buildVersion);
					PlayerSettings.iOS.buildNumber = EditorGUILayout.IntField(string.Empty, buildVersion, GUILayout.MaxWidth(VersionFieldSize)).ToString();
					break;
			}

			EditorGUILayout.EndHorizontal();
		}

		private void DrawPlatformDependSettings(BuildTargetPlatform platform)
		{
			switch (platform)
			{
				case BuildTargetPlatform.Android:
					var style = new GUIStyle();
					string text;
					if (IsProductionBuild)
					{
						if (string.IsNullOrEmpty(PlayerSettings.Android.keystorePass))
						{
							text = "Custom keystore password is empty";
							style.normal.textColor = Color.red;
						}
						else
						{
							text = "The game will be signed with a custom key";
							style.normal.textColor = Color.green;
						}
					}
					else
					{
						text = "The game will be signed with a debug key";
						style.normal.textColor = Color.yellow;
					}
					
					EditorUserBuildSettings.buildAppBundle = EditorGUILayout.Toggle("App Bundle build: ", EditorUserBuildSettings.buildAppBundle);
					EditorGUILayout.LabelField(text, style);
					break;
			}
		}

		private void DrawMasterServerCheckbox()
		{
			_useMasterServer = EditorGUILayout.Toggle("Use Master Server: ", _useMasterServer);
		}

		#endregion

		#region Build
		
		private static void Build(BuildTarget target, BuildType buildType)
		{
			var path = GetBuildPath(target, buildType);
			var buildPlayerOptions = new BuildPlayerOptions
			{
				scenes = GetScenes(buildType),
				locationPathName = path,
				target = target,
				options = BuildOptions.None
			};

			var report = BuildPipeline.BuildPlayer(buildPlayerOptions);
			var summary = report.summary;

			OnBuildFinished(summary.result, path);
			WriteLogsToFile(target, buildType);
		}

		private static void OnBuildFinished(BuildResult result, string path)
		{
			LoadEditorSettings();
			
			AssetBundlesBuildTool.SetIncludeInBuildToggle(true);

			switch (result)
			{
				case BuildResult.Succeeded:
					$"Build saved to {string.Format(BuildPath, Application.version)}".Log(LoggingChannel.Editor);
					EditorUtility.RevealInFinder(path);
					break;

				default:
					$"Building finished with result: {result}".Log(LoggingChannel.Editor);
					break;
			}
			
			OpenWindow();
		}

		private static void BuildBundles(BuildTarget target)
		{
			AssetBundlesBuildTool.BuildAssetBundles(AssetBundlesPath, target, false);

			AssetDatabase.Refresh();
		}
		
		#endregion

		#region Prebuild Settings

		private void SetBuildSettings(BuildTarget target, BuildType buildType, Company company, ResourcesPolicyType resourcesPolicyType)
		{
			SaveEditorSettings();
			SetBuildTypeRelatedSettings(buildType);
			SetPlatformRelatedSettings(target, buildType, _companiesSettings.First(settings => settings.company == company));

			var serverSettings = NetworkSettingsSO.ServersList.FirstOrDefault(server => server.Name == Servers[_selectedServerIndex]) ?? NetworkSettingsSO.ServerSettings;
			NetworkSettingsSO.ServerSettings = serverSettings;
			NetworkSettingsSO.UseMasterServer = _useMasterServer;
			BuildSettingsSO.ResourcesPolicyType = resourcesPolicyType;

			AddMainInfoToLogs();
			AddServerInfoLogs(serverSettings);
			AddAnalyticsLogs();

			EditorUtility.SetDirty(NetworkSettingsSO);
			EditorUtility.SetDirty(BuildSettingsSO);

			if (resourcesPolicyType != ResourcesPolicyType.LocalAssetBundles)
				AssetBundlesBuildTool.ClearDirectory(AssetBundlesPath);
			
			AssetBundlesBuildTool.SetIncludeInBuildToggle(false);

			PlayerSettings.SplashScreen.showUnityLogo = false;
			DefinesController.SetDefines(GetDefines(buildType));

			SaveScriptableObjects();
		}
		
		private void SetBuildTypeRelatedSettings(BuildType buildType)
		{
			var debugConsole = SceneManager.GetActiveScene().GetRootGameObjects().First(gameObject => gameObject.name == DebugConsoleName);
			debugConsole.SetActive(buildType != BuildType.Production);

			switch (buildType)
			{
				case BuildType.Test:
				case BuildType.Development:
					EditorUserBuildSettings.development = true;

					NetworkSettingsSO.UseCustomServer = true;
					BuildSettingsSO.AvailableLanguages = _availableLanguages;
					DeltaDNAConfigurationSO.environmentKey = DevDeltaDNAKey;
					break;

				case BuildType.ProductionTest:
				case BuildType.Production:
					EditorUserBuildSettings.development = false;

					NetworkSettingsSO.UseCustomServer = false;
					BuildSettingsSO.AvailableLanguages = new List<LanguageType> {_availableLanguages[_selectedLanguageIndex]};
					DeltaDNAConfigurationSO.environmentKey = DevDeltaDNAKey;//LiveDeltaDNAKey; todo: Return when it need
					break;
			}
		}

		private static void SetPlatformRelatedSettings(BuildTarget target, BuildType buildType, CompanySettings companySettings)
		{
			switch (target)
			{
				case BuildTarget.Android:
					switch (buildType)
					{
						case BuildType.Production:
						case BuildType.ProductionTest:
							PlayerSettings.Android.useCustomKeystore = true;

							if (string.IsNullOrEmpty(PlayerSettings.Android.keystorePass)) 
								"Production builds must be signed by custom keystore.".LogError(LoggingChannel.Editor);

							break;

						default:
							PlayerSettings.Android.useCustomKeystore = false;
							break;
					}

					PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, companySettings.androidBundleIdentifier);
					BuildSettingsSO.SetBundleVersion(PlayerSettings.Android.bundleVersionCode);
					break;

				case BuildTarget.iOS:
					PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.iOS, companySettings.iOSBundleIdentifier);
					int.TryParse(PlayerSettings.iOS.buildNumber, out var version);
					BuildSettingsSO.SetBundleVersion(version);
					break;
			}
		}

		#endregion

		#region Locales Sheets Settings

		private void SetLocalesSheet(BuildType type, Action onFinish)
		{
			var currentKey = LanguageSourceAsset.mSource.Google_SpreadsheetKey;
			var currentName = LanguageSourceAsset.mSource.Google_SpreadsheetName;
			var isProductionBuild = type == BuildType.Production || type == BuildType.ProductionTest;

			void OnComplete()
			{
				Selection.activeObject = null;
				AddLocalizationLogs(LanguageSourceAsset.mSource);
				onFinish?.Invoke();
			}

			switch (isProductionBuild)
			{
				case true when currentKey != ReleaseLocalizationKey || currentName != ReleaseLocalizationName:
				case false when currentKey != DevelopLocalizationKey || currentName != DevelopLocalizationName:
					Selection.activeObject = LanguageSourceAsset;

					SetLocalizationSettings(type);
					ReplaceLocalizationSheet(OnComplete);

					return;
			}

			OnComplete();
		}

		private static void SetLocalizationSettings(BuildType buildType)
		{
			var source = LanguageSourceAsset.mSource;

			switch (buildType)
			{
				case BuildType.ProductionTest:
				case BuildType.Production:
					source.Google_SpreadsheetKey = ReleaseLocalizationKey;
					source.Google_SpreadsheetName = ReleaseLocalizationName;
					break;

				default:
					source.Google_SpreadsheetKey = DevelopLocalizationKey;
					source.Google_SpreadsheetName = DevelopLocalizationName;
					break;
			}
		}

		private static void ReplaceLocalizationSheet(Action onFinish)
		{
			if (LocalizationEditor.mLanguageSourceEditor == null)
				LocalizationEditor.LocalizationEnabled += ImportGoogleSheet;
			else
				ImportGoogleSheet();

			void ImportGoogleSheet()
			{
				LocalizationEditor.LocalizationEnabled -= ImportGoogleSheet;

				var localizationEditor = LocalizationEditor.mLanguageSourceEditor;
				localizationEditor.Import_Google(eSpreadsheetUpdateMode.Replace);
				localizationEditor.GoogleDriveConnected += onFinish;
			}
		}

		#endregion

		#region Logs

		private void AddMainInfoToLogs()
		{
			var target = _targetPlatform;
			_logs.AppendLine("***Build info***");
			_logs.AppendLine($"Target: {target}");
			_logs.AppendLine($"Build type: {_buildType}");
			_logs.AppendLine($"Development build: {EditorUserBuildSettings.development}");
			_logs.AppendLine($"Version: {GetBuildVersion(GetBuildTarget(target))}");
			_logs.AppendLine($"Time: {DateTime.Now}");

			_logs.AppendLine(string.Empty);
			_logs.AppendLine("***Company***");
			_logs.AppendLine($"Name: {_company}");
			_logs.AppendLine($"Bundle Id: {PlayerSettings.applicationIdentifier}");

			_logs.AppendLine(string.Empty);
			_logs.AppendLine("***Resources info***");
			_logs.AppendLine($"Policy type: {_resourcesPolicyType}");
			_logs.AppendLine($"Build asset bundles: {_buildBundles}");
		}

		private void AddServerInfoLogs(ServerSettings settings)
		{
			_logs.AppendLine(string.Empty);
			_logs.AppendLine("***Server info***");
			_logs.AppendLine($"Name: {settings.Name}");
			_logs.AppendLine($"URL: {settings.GameServerUrl}");
			_logs.AppendLine($"Asset Bundles URL: {settings.AssetBundlesUrl}");
			_logs.AppendLine($"Use master server: {_useMasterServer}");
		}

		private void AddLocalizationLogs(LanguageSourceData data)
		{
			_logs.AppendLine(string.Empty);
			_logs.AppendLine("***Localization info***");
			_logs.AppendLine($"Name: {data.Google_SpreadsheetName}");
			_logs.AppendLine($"Key: {data.Google_SpreadsheetKey}");
			_logs.AppendLine($"Languages: {(IsProductionBuild ? _availableLanguages[_selectedLanguageIndex].ToString() : string.Join(", ", _availableLanguages))}");
		}
		
		private void AddAnalyticsLogs()
		{
			_logs.AppendLine(string.Empty);
			_logs.AppendLine("***Analytics info***");
			_logs.AppendLine($"Delta DNA environment key: {DeltaDNAConfigurationSO.environmentKey}");
		}

		private static void WriteLogsToFile(BuildTarget target, BuildType type)
		{
			var path = string.Format(BuildPath, $"{GetBuildVersion(target)} ({type})") + ".txt";
			File.WriteAllText(path, _logs.ToString());
		}

		#endregion
		
		#region Editor Settings

		private static void SaveEditorSettings()
		{
			_editorPolicyType = BuildSettingsSO.ResourcesPolicyType;
			_editorUseMasterServer = NetworkSettingsSO.UseMasterServer;
			_editorUseCustomServer = NetworkSettingsSO.UseCustomServer;

			var currentSettings = NetworkSettingsSO.ServerSettings;
			for (var serverIndex = 0; serverIndex < Servers.Length; serverIndex++)
			{
				if (Servers[serverIndex] != currentSettings.Name) continue;
				
				_editorServerIndex = serverIndex;
				break;
			}
		}
		
		private static void LoadEditorSettings()
		{
			BuildSettingsSO.ResourcesPolicyType = _editorPolicyType;
			NetworkSettingsSO.UseMasterServer = _editorUseMasterServer;
			NetworkSettingsSO.UseCustomServer = _editorUseCustomServer;
			DeltaDNAConfigurationSO.environmentKey = DevDeltaDNAKey;
			
			var serverSettings = NetworkSettingsSO.ServersList.FirstOrDefault(server => server.Name == Servers[_editorServerIndex]);
			if (serverSettings != null) 
				NetworkSettingsSO.ServerSettings = serverSettings;

			SaveScriptableObjects();
		}

		private static void SaveScriptableObjects()
		{
			EditorUtility.SetDirty(NetworkSettingsSO);
			EditorUtility.SetDirty(BuildSettingsSO);
			
			AssetDatabase.Refresh();
			AssetDatabase.SaveAssets();
		}

		#endregion

		private static string[] GetScenes(BuildType type)
		{
			switch (type)
			{
				case BuildType.Production:
					return new[] {MainScenePath};

				default:
					return EditorBuildSettings.scenes.Select(scene => scene.path).ToArray();
			}
		}

		private static BuildTarget GetBuildTarget(BuildTargetPlatform target)
		{
			switch (target)
			{
				case BuildTargetPlatform.iOS:
					return BuildTarget.iOS;

				default:
					return BuildTarget.Android;
			}
		}

		private static IEnumerable<string> GetDefines(BuildType listName)
		{
			switch (listName)
			{
				case BuildType.Test:
					return DefinesSetup.Test;

				case BuildType.ProductionTest:
					return DefinesSetup.ProductionTest;

				case BuildType.Production:
					return DefinesSetup.Production;

				case BuildType.Development:
					return DefinesSetup.Development;

				default:
					return DefinesSetup.Production;
			}
		}

		private static string GetBuildPath(BuildTarget target, BuildType type)
		{
			switch (target)
			{
				case BuildTarget.Android:
					return string.Format(BuildPath, $"{GetBuildVersion(target)} ({type})") + (EditorUserBuildSettings.buildAppBundle ? ".aab" : ".apk");

				case BuildTarget.iOS:
					return string.Format(BuildPath, $"{GetBuildVersion(target)} ({type})");

				default:
					return $"{BuildPath} {Application.version} ({type})";
			}
		}

		private static string GetBuildVersion(BuildTarget target)
		{
			switch (target)
			{
				case BuildTarget.Android:
					return $"{PlayerSettings.bundleVersion} {PlayerSettings.Android.bundleVersionCode}";

				case BuildTarget.iOS:
					return $"{PlayerSettings.bundleVersion} {PlayerSettings.iOS.buildNumber}";

				default:
					return $"{Application.version}";
			}
		}

		private class CompanySettings
		{
			public Company company;
			public string androidBundleIdentifier;
			public string iOSBundleIdentifier;
		}

		private enum Company
		{
			MBC,
			NanoReality
		}

		private enum BuildTargetPlatform
		{
			Android,
			iOS
		}

		private enum BuildType
		{
			Test,
			ProductionTest,
			Production,
			Development
		}
	}
}