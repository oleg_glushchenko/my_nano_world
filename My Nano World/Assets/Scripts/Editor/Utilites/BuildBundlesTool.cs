using System.IO;
using UnityEditor;
using UnityEngine;
using NanoReality.Utility.Editor;

namespace NanoReality.Editor
{
    public class BuildBundlesTool : EditorWindow
    {
        private const string AssetBundlesFolderName = "AssetBundles";
        private static string AssetBundlesPath => Path.Combine(Application.streamingAssetsPath, AssetBundlesFolderName);

        private BundleTargetPlatform _targetPlatform;
        private bool _highQuality;

        [MenuItem("Tools/NanoReality/Asset Bundles Build Tool &%a")]
        private static void OpenWindow()
        {
            GetWindow<BuildBundlesTool>("Asset Bundle Build Tool");
        }

        protected void OnGUI()
        {
            DrawDropdowns();

            if (GUILayout.Button("Build bundles"))
            {
                var target = GetBuildTarget(_targetPlatform);
                if (target != BuildTarget.NoTarget)
                {
                    AssetBundlesBuildTool.BuildAssetBundles(AssetBundlesPath, target, _highQuality);

                    AssetDatabase.Refresh();
                }
            }
        }

        private void DrawDropdowns()
        {
            _targetPlatform = (BundleTargetPlatform) EditorGUILayout.EnumPopup("Target: ", _targetPlatform);
            _highQuality = EditorGUILayout.Toggle("High Quality: ", _highQuality);
            
            GUILayout.FlexibleSpace();
        }

        private static BuildTarget GetBuildTarget(BundleTargetPlatform target)
        {
            var result = BuildTarget.NoTarget;
            switch (target)
            {
                case BundleTargetPlatform.Android:
                    result = BuildTarget.Android;
                    break;
                case BundleTargetPlatform.iOS:
                    result = BuildTarget.iOS;
                    break;
                case BundleTargetPlatform.Windows:
                    result = BuildTarget.StandaloneWindows;
                    break;
                case BundleTargetPlatform.OSX:
                    result = BuildTarget.StandaloneOSX;
                    break;
            }

            return result;
        }

        private enum BundleTargetPlatform
        {
            Android,
            iOS,
            Windows,
            OSX
        }
    }
}