using UnityEditor;
using UnityEditor.Callbacks;
using System.IO;
using NanoReality.Core.Resources;
using NanoReality.Engine.Settings.BuildSettings;
using NanoReality.GameLogic.Constants;
using UnityEditor.iOS.Xcode;

public class PostBuildStep
{
	private const string AppleTrackingTransparencyWindowMessageEnglish =
		"Do you want to improve your ad experience? We would like to collect and share data with our ad/analytics partners to provide you with a personalised ad experience. Please note that disagreeing will not remove ads, instead, you may see unrelated ads to your interests.";

	private const string AppleTrackingTransparencyWindowMessageArabic =
		"هل تريد تحسين تجربتك مع الإعلانات؟" +
		"يتم دعم عالم النانو من خلال الإعلانات. لجعل هذه الإعلانات مناسبة لك أكثر نود ان نجمع بعض البيانات ومشاركتها مع شركائنا مزودي الخدمات الاعلانية أو الاحصائية. يرجى العلم في حال عدم موافقتك لن يتم ازالة الاعلانات ولكن قد تظهر لك اعلانات ابعد" +
		"عن اهتماماتك.";

	[PostProcessBuild(0)]
	public static void OnPostprocessBuild(BuildTarget buildTarget, string pathToXcode)
	{
		if (buildTarget == BuildTarget.iOS)
			AddPListValues(pathToXcode);
	}

	private static void AddPListValues(string pathToXcode)
	{
		var plistPath = pathToXcode + "/Info.plist";

		var plistObj = new PlistDocument();
		plistObj.ReadFromString(File.ReadAllText(plistPath));

		var buildSettings = ScriptableObjectsManager.Get<BuildSettings>();
		var description = buildSettings.AvailableLanguages.Contains(LanguageType.Arabic) ? AppleTrackingTransparencyWindowMessageArabic : AppleTrackingTransparencyWindowMessageEnglish;

		var plistRoot = plistObj.root;
		plistRoot.SetString("NSUserTrackingUsageDescription", description);
		plistRoot.SetBoolean("GADIsAdManagerApp", true);

		File.WriteAllText(plistPath, plistObj.WriteToString());
	}
}