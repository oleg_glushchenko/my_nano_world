using UnityEditor;
using UnityEditor.UI;
using UnityEngine;

namespace Engine.UI.Common
{
    [CustomEditor(typeof(LoadableImage), true)]
    [CanEditMultipleObjects]
    public class LoadableImageEditor : ImageEditor
    {
        public override void OnInspectorGUI()
        {
            var component = (LoadableImage)target;
            component.Spinner = (Material)EditorGUILayout.ObjectField("Spinner", component.Spinner, typeof(Material), true);
            base.OnInspectorGUI();
        }
    }
}