﻿using UnityEngine;
using UnityEditor;
using UnityEditor.UI;

[CustomEditor(typeof(ImageMirror), true)]
[CanEditMultipleObjects]
public class ImageMirrorInspector : ImageEditor
{
    SerializedProperty m_MirrorType;
    SerializedProperty m_FlipType;

    protected override void OnEnable()
    {
        base.OnEnable();
        m_MirrorType = serializedObject.FindProperty("m_MirrorType");
        m_FlipType = serializedObject.FindProperty("m_FlipType");
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        serializedObject.Update();
        EditorGUI.BeginChangeCheck();
        EditorGUILayout.PropertyField(m_MirrorType);
        EditorGUILayout.PropertyField(m_FlipType);
        EditorGUI.EndChangeCheck();
        serializedObject.ApplyModifiedProperties();


    }
}