﻿using UnityEditor;

namespace NanoReality.Editor
{
    public class EditorBuildProcessor
    {
       // [PostProcessBuild(999)]
        public static void OnPostProcessBuild(BuildTarget buildTarget, string path)
        {
//            // we have several third party libraries that still do not support Bitcode on iOS or need other frameworks
//            // eg. GameAnalytics, GoogleAnalytics, Firebase, AppsFlyer etc
//            // https://support.unity3d.com/hc/en-us/articles/207942813-How-can-I-disable-Bitcode-support-
//            // https://stackoverflow.com/questions/41896479/firebase-xcode-linker-command-error-using-firebase-unity-sdk
//            if(buildTarget == BuildTarget.iOS)
//            {
//                var projectPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";
//
//                var pbxProject = new PBXProject();
//                pbxProject.ReadFromFile(projectPath);
//
//                var target = pbxProject.TargetGuidByName("Unity-iPhone");            
//                pbxProject.SetBuildProperty(target, "ENABLE_BITCODE", "NO");
//                
//                // List of frameworks that will be added to project
//                var frameworks = new List<string>
//                {
//                    "CoreData.framework",
//                    "SystemConfiguration.framework",
//                    
//                    // AppsFlyer collects IDFA only if you include this framework.
//                    // Failure to add this framework means that you cannot track Facebook,
//                    // Twitter and most other ad networks.
//                    "AdSupport.framework",
//                    
//                    // Adding this framework to your app project is highly recommended,
//                    // since it is mandatory for tracking Apple Search Ads with AppsFlyer.
//                    "iAd.framework",
//                };
//                frameworks.ForEach(framework => 
//                {
//                    pbxProject.AddFrameworkToProject(target, framework, false);
//                });
//
//                pbxProject.WriteToFile (projectPath);
//            }
        }
    }
}
