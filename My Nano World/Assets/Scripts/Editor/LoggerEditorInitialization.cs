﻿using NanoLib.Core.Logging.Settings;
using NanoReality.Core.Resources;
using UnityEditor;
using UnityEngine;

namespace NanoLib.Core.Logging
{
	[InitializeOnLoad]
	public static class LoggerEditorInitialization
	{
		static LoggerEditorInitialization()
		{
			Debug.unityLogger.logEnabled = true;
            
			var settings = ScriptableObjectsManager.Get<LoggingSettings>();
			if (settings == null) return;
			
			Logging.Initialize(settings.Channels);
			Logging.SetChannels(settings.ActiveChannels);
		}
	}
}