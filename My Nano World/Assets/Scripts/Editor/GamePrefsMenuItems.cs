﻿using System.IO;
using UnityEditor;
using UnityEngine;

public class GamePrefsMenuItems
{
    [MenuItem("NanoReality/Tools/User/Clear Player Prefs", priority = 0)]
    private static void DeleteAll()
    {
        if (EditorUtility.DisplayDialog("Game Prefs", "Are you sure to delete all player prefs?", "OK", "CANCEL"))
        {
            PlayerPrefs.DeleteAll();
        }
    }

    [MenuItem("NanoReality/Tools/User/Clear Local Data", priority = 1)]
    private static void DeleteLocalData()
    {
        if (!EditorUtility.DisplayDialog("Game Prefs", "Are you sure to delete all player data?", "OK", "CANCEL"))
        {
            return;
        }

        PlayerPrefs.DeleteAll();
        
        var aPath = Path.Combine(Application.persistentDataPath, "AppData.bin");
        if (!File.Exists(aPath)) return;
        
        File.Delete(aPath);
        Debug.Log(aPath + " was removed!");
    }
}
