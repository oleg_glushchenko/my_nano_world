﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.BuildingSystem;
using UnityEditor;
using UnityEngine;

public class BuildingConfigsValidator : MonoBehaviour
{
    private static Object _folder;
    private static List<BuildingConfig> configs;
    private static Material _cloudedSpriteMat;

    [MenuItem("Tools/ValidateBuildingConfigs")]
    private static void Init()
    {
        configs = GetAssetList<BuildingConfig>("Assets/Prefabs/BuildingConfigs");
        Debug.Log("total config files = " + configs.Count);
        _cloudedSpriteMat = AssetDatabase.LoadAssetAtPath<Material>("Assets/Art/Clouds/cloudedSprite.mat");
        Validate();
    }

    public static List<T> GetAssetList<T>(string path) where T : class
    {
        string[] fileEntries = Directory.GetFiles(path);

        return fileEntries.Select(fileName =>
            {
                string assetPath = fileName.Substring(fileName.IndexOf("Assets"));
                assetPath = Path.ChangeExtension(assetPath, null);
                return AssetDatabase.LoadAssetAtPath(assetPath, typeof(T));
            })
            .OfType<T>()
            .ToList();
    }

    private static void Validate()
    {
        foreach (var config in configs)
        {
            CheckViewListForNull(config);
        }
    }

    private static void CheckViewListForNull(BuildingConfig config)
    {
        if (config.ViewsList == null)
        {
            Debug.LogErrorFormat("config {0}ViewsList Is Empty", config.name);
        }
        else
        {
            var viewsList = config.ViewsList;

            foreach (var v in viewsList)
            {
                var i = v.Level.ToString();
                if (v.View == null)
                {
                    Debug.LogErrorFormat("config {0} level{1} View is NULL", config.name, v.Level);
                }
                else
                {
                    CheckViewSpriteForNull(config.name, v.View);
                    CheckViewForWrongComponents(v.View);
                }

                if (v.Sprite == null)
                {
                    Debug.LogErrorFormat("config {0} of level{1} Sprite is NULL", config.name, v.Level);
                }
            }
        }
    }

    private static void CheckViewSpriteForNull(string configName, MapObjectView view)
    {
        if (view.SpriteRenderer == null)
        {
            Debug.LogErrorFormat("View {0} SpriteRendere is Null", view.name);
        }
        else
        {
            if (view.SpriteRenderer.sharedMaterial != _cloudedSpriteMat)
            {
                view.SpriteRenderer.sharedMaterial = _cloudedSpriteMat;
            }
        }
    }

    private static void CheckViewForWrongComponents(MapObjectView view)
    {
        List<Component> componentsList = view.GetComponents(typeof(Component)).ToList();
        List<Component> inValidComponents = new List<Component>();


        foreach (var component in componentsList)
        {
            var cType = component.GetType();
            if (cType == typeof(Transform))
            {
            }
            else if (cType.IsSubclassOf(typeof(MapObjectView)))
            {
            }
            else if (cType == typeof(Animation))
            {
            }
            else if (cType == typeof(Animator))
            {
            }
            else
            {
                inValidComponents.Add(component);
                Debug.LogErrorFormat("View {0} has wrong component {1}", view.name, component.GetType());
            }
        }
    }
}