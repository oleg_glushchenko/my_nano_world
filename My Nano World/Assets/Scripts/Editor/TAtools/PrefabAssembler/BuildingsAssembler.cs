﻿using System.Collections.Generic;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using UnityEditor;
using UnityEngine;

public class AnimationClipOverrides : List<KeyValuePair<AnimationClip, AnimationClip>>
{
    public AnimationClipOverrides(int capacity) : base(capacity) {}

    public AnimationClip this[string name]
    {
        get { return this.Find(x => x.Key.name.Equals(name)).Value; }
        set
        {
            int index = this.FindIndex(x => x.Key.name.Equals(name));
            if (index != -1)
                this[index] = new KeyValuePair<AnimationClip, AnimationClip>(this[index].Key, value);
        }
    }
}

public class BuildingsAssembler: MonoBehaviour
{
    private static RuntimeAnimatorController _referenceController;
    private static GameObject _selectedBuilding;
    private static MapObjectView _objectView;
    private static Animator _animator;
    private static AnimatorOverrideController _overrideController;
    private static SpriteRenderer _backgroundSpriteRenderer;
    private static string _path;
    private static AnimationClip _freezeClip;
    private static AnimationClip _idleClip;
    private static AnimationClip _workClip;
    
    [MenuItem("Assets/TaTools/Prepare Building Prefab")]
    static void Init()
    {
        if (!Selection.activeGameObject is GameObject)
        {
            Debug.LogError("Wrong Type select Prefab");
        }
        else
        {
            _selectedBuilding = Selection.activeGameObject;
            if (_selectedBuilding.layer != 9)
            {
                Debug.LogError("This prefab doesn't have layer: Buildings");
            }
            else
            {
                
                if (_selectedBuilding.GetComponent<MapObjectView>() == null)
                {
                    Debug.LogError("Can not find MapObjectView component");
                    
                }
                else
                {
                    _objectView = _selectedBuilding.GetComponent<MapObjectView>();
                    _path = AssetDatabase.GetAssetPath(_selectedBuilding)
                        .Replace(_selectedBuilding.name + ".prefab", "");
                    _referenceController =
                        AssetDatabase.LoadAssetAtPath<RuntimeAnimatorController>(
                            "Assets/Buildings/common/Building.controller");
                    PreparePrefab();
                }
            }
        }
    }

    private static void PreparePrefab()
    {
        RemoveWrongComponents();
        //AddCollider();
        AddAnimator();
        CreateOverrideAnimationController();
        CreateAnimations();
        PopulateOverrideAnimationController();
        PopulateAnimator();
        
        EditorUtility.SetDirty(_selectedBuilding);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    private static void ResetTransform()
    {
        _selectedBuilding.transform.position = new Vector3(0,0,0);
        _selectedBuilding.transform.rotation = new Quaternion(0,0,0,0);
        _selectedBuilding.transform.position = new Vector3(1,1,1);
    }

    private static void CreateOverrideAnimationController()
    {
        AssetDatabase.CreateAsset(new AnimatorOverrideController(), _path + _selectedBuilding.name + ".overrideController");
        _overrideController =
            AssetDatabase.LoadAssetAtPath<AnimatorOverrideController>(
                _path + _selectedBuilding.name + ".overrideController");
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    private static void CreateAnimations()
    {
        AssetDatabase.CreateAsset(new AnimationClip{wrapMode = WrapMode.Loop, frameRate = 15}, _path + _selectedBuilding.name + "_freeze.anim");
        _freezeClip = AssetDatabase.LoadAssetAtPath<AnimationClip>(_path + _selectedBuilding.name + "_freeze.anim");
        AssetDatabase.CreateAsset(new AnimationClip{wrapMode = WrapMode.Loop, frameRate = 15}, _path + _selectedBuilding.name + "_idle.anim");
        _idleClip = AssetDatabase.LoadAssetAtPath<AnimationClip>(_path + _selectedBuilding.name + "_idle.anim");
        AssetDatabase.CreateAsset(new AnimationClip{wrapMode = WrapMode.Loop, frameRate = 15}, _path + _selectedBuilding.name + "_work.anim");
        _workClip = AssetDatabase.LoadAssetAtPath<AnimationClip>(_path + _selectedBuilding.name + "_work.anim");
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    private static void PopulateOverrideAnimationController()
    {
        _overrideController.runtimeAnimatorController = _referenceController;
        _overrideController.animationClips[0] = _freezeClip;
        _overrideController.animationClips[1] = _idleClip;
        _overrideController.animationClips[2] = _workClip;

        var clipOverrides = new AnimationClipOverrides(_overrideController.overridesCount);
        _overrideController.GetOverrides(clipOverrides);
        
        clipOverrides["BuildingFreezeStub"] = _freezeClip;
        clipOverrides["BuildingIdleStub"] = _idleClip;
        clipOverrides["BuildingWorkStub"] = _workClip;

        _overrideController.ApplyOverrides(clipOverrides);
    }

    private static void PopulateAnimator()
    {
        Debug.Log("override name " + _overrideController.name);
        Debug.Log("loaded override name " + AssetDatabase.LoadAssetAtPath<AnimatorOverrideController>(_path + _selectedBuilding.name + ".overrideController").name);
        Debug.Log("AnimatorName " + _animator.name);
        _animator.runtimeAnimatorController = AssetDatabase.LoadAssetAtPath<AnimatorOverrideController>(_path + _selectedBuilding.name + ".overrideController");
        
        Debug.Log("setted override name " + _animator.runtimeAnimatorController.name);
    }

    private static void RemoveWrongComponents()
    {
        if (_selectedBuilding.GetComponent<SpriteRenderer>() != null)
        {
            DestroyImmediate(_selectedBuilding.GetComponent<SpriteRenderer>(), true);
        }
        if (_selectedBuilding.GetComponent<MeshFilter>() != null)
        {
            DestroyImmediate(_selectedBuilding.GetComponent<MeshFilter>(), true);
        }
        if (_selectedBuilding.GetComponent<MeshRenderer>() != null)
        {
            DestroyImmediate(_selectedBuilding.GetComponent<MeshRenderer>(), true);
        }
        if (_selectedBuilding.GetComponent<MeshCollider>() != null)
        {
            DestroyImmediate(_selectedBuilding.GetComponent<MeshCollider>(), true);
        }
        if (_selectedBuilding.GetComponent<Animation>() != null)
        {
            DestroyImmediate(_selectedBuilding.GetComponent<Animation>(), true);
        }
    }

    private static void CreateBackgroundSprite()
    {
        var sprite = Instantiate(new GameObject(), _selectedBuilding.transform);
        sprite.transform.SetParent(_selectedBuilding.transform);
        _backgroundSpriteRenderer = sprite.AddComponent<SpriteRenderer>();
    }

    private static void AttachBackgroundToMapView()
    {
        //_objectView.SpriteRenderer = _backgroundSpriteRenderer;
    }

    private static void AddCollider()
    {
        BoxCollider collider = null;
        if (_selectedBuilding.GetComponent<BoxCollider>() != null)
        {
            collider = _selectedBuilding.GetComponent<BoxCollider>();
        }
        else
        {
            collider = _selectedBuilding.AddComponent<BoxCollider>();
        }
        collider.center = new Vector3(0, 1.5f, 0);
        collider.size = new Vector3(5,3,1);
    }

    private static void AddAnimator()
    {
        if (_selectedBuilding.GetComponent<Animator>() == null)
        {
            _animator = _selectedBuilding.AddComponent<Animator>();
        }
        else
        {
            _animator = _selectedBuilding.GetComponent<Animator>();
        }
    }
}
