﻿using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UseSpriteMeshChecckerAll
{
    private static string globalPath = "Assets/";

    [MenuItem("NanoReality/Tools/CheckUseSpriteMeshForAll")]
    private static void Execute()
    {
        var allAssets = AssetDatabase.FindAssets("t:prefab", new[] {"Assets"});

        foreach (var asset in allAssets)
        {
            var path = AssetDatabase.GUIDToAssetPath(asset);
            var prefab = AssetDatabase.LoadAssetAtPath<GameObject>(path);
            if (prefab.layer == 5)
            {
                AssetDatabase.OpenAsset(AssetDatabase.LoadAssetAtPath<GameObject>(path));

                var _images = GameObject.FindObjectsOfType(typeof(Image));
                Debug.LogFormat("prefasb {0} has {1} images", prefab.name, _images.Length);

                foreach (var image in _images)
                {
                    var currentFlag = typeof(Image).GetField("m_UseSpriteMesh",
                        System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);

                    currentFlag.SetValue(image, true);
                }

                EditorUtility.SetDirty(prefab);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }
        }
    }
}