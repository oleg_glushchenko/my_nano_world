﻿using UnityEngine;

namespace I2.Loc
{
	public static class ScriptLocalization
	{

		public static class Building
		{
			public static string OBJECT_BUILDING_101 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_101"); } }
			public static string OBJECT_BUILDING_103 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_103"); } }
			public static string OBJECT_BUILDING_105 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_105"); } }
			public static string OBJECT_BUILDING_106 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_106"); } }
			public static string OBJECT_BUILDING_107 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_107"); } }
			public static string OBJECT_BUILDING_109 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_109"); } }
			public static string OBJECT_BUILDING_110 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_110"); } }
			public static string OBJECT_BUILDING_111 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_111"); } }
			public static string OBJECT_BUILDING_116 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_116"); } }
			public static string OBJECT_BUILDING_117 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_117"); } }
			public static string OBJECT_BUILDING_119 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_119"); } }
			public static string OBJECT_BUILDING_120 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_120"); } }
			public static string OBJECT_BUILDING_121 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_121"); } }
			public static string OBJECT_BUILDING_123 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_123"); } }
			public static string OBJECT_BUILDING_124 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_124"); } }
			public static string OBJECT_BUILDING_126 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_126"); } }
			public static string OBJECT_BUILDING_127 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_127"); } }
			public static string OBJECT_BUILDING_131 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_131"); } }
			public static string OBJECT_BUILDING_132 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_132"); } }
			public static string OBJECT_BUILDING_135 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_135"); } }
			public static string OBJECT_BUILDING_139 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_139"); } }
			public static string OBJECT_BUILDING_140 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_140"); } }
			public static string OBJECT_BUILDING_141 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_141"); } }
			public static string OBJECT_BUILDING_142 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_142"); } }
			public static string OBJECT_BUILDING_143 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_143"); } }
			public static string OBJECT_BUILDING_144 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_144"); } }
			public static string OBJECT_BUILDING_145 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_145"); } }
			public static string OBJECT_BUILDING_146 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_146"); } }
			public static string OBJECT_BUILDING_154 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_154"); } }
			public static string OBJECT_BUILDING_155 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_155"); } }
			public static string OBJECT_BUILDING_156 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_156"); } }
			public static string OBJECT_BUILDING_157 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_157"); } }
			public static string OBJECT_BUILDING_172 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_172"); } }
			public static string OBJECT_BUILDING_173 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_173"); } }
			public static string OBJECT_BUILDING_174 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_174"); } }
			public static string OBJECT_BUILDING_175 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_175"); } }
			public static string OBJECT_BUILDING_176 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_176"); } }
			public static string OBJECT_BUILDING_27 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_27"); } }
			public static string OBJECT_BUILDING_30 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_30"); } }
			public static string OBJECT_BUILDING_31 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_31"); } }
			public static string OBJECT_BUILDING_44 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_44"); } }
			public static string OBJECT_BUILDING_48 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_48"); } }
			public static string OBJECT_BUILDING_55 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_55"); } }
			public static string OBJECT_BUILDING_56 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_56"); } }
			public static string OBJECT_BUILDING_57 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_57"); } }
			public static string OBJECT_BUILDING_58 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_58"); } }
			public static string OBJECT_BUILDING_69 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_69"); } }
			public static string OBJECT_BUILDING_94 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_94"); } }
			public static string OBJECT_BUILDING_96 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_96"); } }
			public static string OBJECT_BUILDING_97 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_97"); } }
			public static string OBJECT_BUILDING_98 		{ get{ return LocalizationManager.GetTranslation ("Building/OBJECT_BUILDING_98"); } }
		}

		public static class LoaderHint
		{
			public static string LOADER_HINT_DEFAULT_1 		{ get{ return LocalizationManager.GetTranslation ("LoaderHint/LOADER_HINT_DEFAULT_1"); } }
			public static string LOADER_HINT_DEFAULT_10 		{ get{ return LocalizationManager.GetTranslation ("LoaderHint/LOADER_HINT_DEFAULT_10"); } }
			public static string LOADER_HINT_DEFAULT_11 		{ get{ return LocalizationManager.GetTranslation ("LoaderHint/LOADER_HINT_DEFAULT_11"); } }
			public static string LOADER_HINT_DEFAULT_12 		{ get{ return LocalizationManager.GetTranslation ("LoaderHint/LOADER_HINT_DEFAULT_12"); } }
			public static string LOADER_HINT_DEFAULT_13 		{ get{ return LocalizationManager.GetTranslation ("LoaderHint/LOADER_HINT_DEFAULT_13"); } }
			public static string LOADER_HINT_DEFAULT_14 		{ get{ return LocalizationManager.GetTranslation ("LoaderHint/LOADER_HINT_DEFAULT_14"); } }
			public static string LOADER_HINT_DEFAULT_15 		{ get{ return LocalizationManager.GetTranslation ("LoaderHint/LOADER_HINT_DEFAULT_15"); } }
			public static string LOADER_HINT_DEFAULT_16 		{ get{ return LocalizationManager.GetTranslation ("LoaderHint/LOADER_HINT_DEFAULT_16"); } }
			public static string LOADER_HINT_DEFAULT_17 		{ get{ return LocalizationManager.GetTranslation ("LoaderHint/LOADER_HINT_DEFAULT_17"); } }
			public static string LOADER_HINT_DEFAULT_18 		{ get{ return LocalizationManager.GetTranslation ("LoaderHint/LOADER_HINT_DEFAULT_18"); } }
			public static string LOADER_HINT_DEFAULT_19 		{ get{ return LocalizationManager.GetTranslation ("LoaderHint/LOADER_HINT_DEFAULT_19"); } }
			public static string LOADER_HINT_DEFAULT_2 		{ get{ return LocalizationManager.GetTranslation ("LoaderHint/LOADER_HINT_DEFAULT_2"); } }
			public static string LOADER_HINT_DEFAULT_20 		{ get{ return LocalizationManager.GetTranslation ("LoaderHint/LOADER_HINT_DEFAULT_20"); } }
			public static string LOADER_HINT_DEFAULT_3 		{ get{ return LocalizationManager.GetTranslation ("LoaderHint/LOADER_HINT_DEFAULT_3"); } }
			public static string LOADER_HINT_DEFAULT_4 		{ get{ return LocalizationManager.GetTranslation ("LoaderHint/LOADER_HINT_DEFAULT_4"); } }
			public static string LOADER_HINT_DEFAULT_5 		{ get{ return LocalizationManager.GetTranslation ("LoaderHint/LOADER_HINT_DEFAULT_5"); } }
			public static string LOADER_HINT_DEFAULT_6 		{ get{ return LocalizationManager.GetTranslation ("LoaderHint/LOADER_HINT_DEFAULT_6"); } }
			public static string LOADER_HINT_DEFAULT_7 		{ get{ return LocalizationManager.GetTranslation ("LoaderHint/LOADER_HINT_DEFAULT_7"); } }
			public static string LOADER_HINT_DEFAULT_8 		{ get{ return LocalizationManager.GetTranslation ("LoaderHint/LOADER_HINT_DEFAULT_8"); } }
			public static string LOADER_HINT_DEFAULT_9 		{ get{ return LocalizationManager.GetTranslation ("LoaderHint/LOADER_HINT_DEFAULT_9"); } }
		}

		public static class Localization
		{
			public static string ERROR_MAINTENANCE_TITLE 		{ get{ return LocalizationManager.GetTranslation ("Localization/ERROR_MAINTENANCE_TITLE"); } }
			public static string ERROR_MAINTENANCE_DESCRIPTION 		{ get{ return LocalizationManager.GetTranslation ("Localization/ERROR_MAINTENANCE_DESCRIPTION"); } }
		}

		public static class Notifications
		{
			public static string AUCTION_PURCHASE_DESCRIPTION_1 		{ get{ return LocalizationManager.GetTranslation ("Notifications/AUCTION_PURCHASE_DESCRIPTION_1"); } }
			public static string AUCTION_PURCHASE_DESCRIPTION_2 		{ get{ return LocalizationManager.GetTranslation ("Notifications/AUCTION_PURCHASE_DESCRIPTION_2"); } }
			public static string AUCTION_PURCHASE_TITLE 		{ get{ return LocalizationManager.GetTranslation ("Notifications/AUCTION_PURCHASE_TITLE"); } }
			public static string AUCTION_SELL_DESCRIPTION_1 		{ get{ return LocalizationManager.GetTranslation ("Notifications/AUCTION_SELL_DESCRIPTION_1"); } }
			public static string AUCTION_SELL_TITLE 		{ get{ return LocalizationManager.GetTranslation ("Notifications/AUCTION_SELL_TITLE"); } }
			public static string BUILDING_CONSTRUCTED_DESCRIPTION_1 		{ get{ return LocalizationManager.GetTranslation ("Notifications/BUILDING_CONSTRUCTED_DESCRIPTION_1"); } }
			public static string BUILDING_CONSTRUCTED_DESCRIPTION_2 		{ get{ return LocalizationManager.GetTranslation ("Notifications/BUILDING_CONSTRUCTED_DESCRIPTION_2"); } }
			public static string BUILDING_CONSTRUCTED_TITLE 		{ get{ return LocalizationManager.GetTranslation ("Notifications/BUILDING_CONSTRUCTED_TITLE"); } }
			public static string BUILDING_UPGRADED_DESCRIPTION_1 		{ get{ return LocalizationManager.GetTranslation ("Notifications/BUILDING_UPGRADED_DESCRIPTION_1"); } }
			public static string BUILDING_UPGRADED_DESCRIPTION_2 		{ get{ return LocalizationManager.GetTranslation ("Notifications/BUILDING_UPGRADED_DESCRIPTION_2"); } }
			public static string BUILDING_UPGRADED_DESCRIPTION_3 		{ get{ return LocalizationManager.GetTranslation ("Notifications/BUILDING_UPGRADED_DESCRIPTION_3"); } }
			public static string BUILDING_UPGRADED_TITLE 		{ get{ return LocalizationManager.GetTranslation ("Notifications/BUILDING_UPGRADED_TITLE"); } }
			public static string COOK_TIME_DESCRIPTION_1 		{ get{ return LocalizationManager.GetTranslation ("Notifications/COOK_TIME_DESCRIPTION_1"); } }
			public static string COOK_TIME_DESCRIPTION_2 		{ get{ return LocalizationManager.GetTranslation ("Notifications/COOK_TIME_DESCRIPTION_2"); } }
			public static string COOK_TIME_TITLE 		{ get{ return LocalizationManager.GetTranslation ("Notifications/COOK_TIME_TITLE"); } }
			public static string FOOD_READY_DESCRIPTION_1 		{ get{ return LocalizationManager.GetTranslation ("Notifications/FOOD_READY_DESCRIPTION_1"); } }
			public static string FOOD_READY_DESCRIPTION_2 		{ get{ return LocalizationManager.GetTranslation ("Notifications/FOOD_READY_DESCRIPTION_2"); } }
			public static string FOOD_READY_TITLE 		{ get{ return LocalizationManager.GetTranslation ("Notifications/FOOD_READY_TITLE"); } }
			public static string FRIEND_VISIT_DESCRIPTION_1 		{ get{ return LocalizationManager.GetTranslation ("Notifications/FRIEND_VISIT_DESCRIPTION_1"); } }
			public static string FRIEND_VISIT_TITLE 		{ get{ return LocalizationManager.GetTranslation ("Notifications/FRIEND_VISIT_TITLE"); } }
			public static string HARVEST_READY_DESCRIPTION_1 		{ get{ return LocalizationManager.GetTranslation ("Notifications/HARVEST_READY_DESCRIPTION_1"); } }
			public static string HARVEST_READY_DESCRIPTION_2 		{ get{ return LocalizationManager.GetTranslation ("Notifications/HARVEST_READY_DESCRIPTION_2"); } }
			public static string HARVEST_READY_TITLE 		{ get{ return LocalizationManager.GetTranslation ("Notifications/HARVEST_READY_TITLE"); } }
			public static string MISS_YOU_DESCRIPTION_1 		{ get{ return LocalizationManager.GetTranslation ("Notifications/MISS_YOU_DESCRIPTION_1"); } }
			public static string MISS_YOU_DESCRIPTION_2 		{ get{ return LocalizationManager.GetTranslation ("Notifications/MISS_YOU_DESCRIPTION_2"); } }
			public static string MISS_YOU_DESCRIPTION_3 		{ get{ return LocalizationManager.GetTranslation ("Notifications/MISS_YOU_DESCRIPTION_3"); } }
			public static string MISS_YOU_TITLE 		{ get{ return LocalizationManager.GetTranslation ("Notifications/MISS_YOU_TITLE"); } }
			public static string PRODUCTION_READY_DESCRIPTION_1 		{ get{ return LocalizationManager.GetTranslation ("Notifications/PRODUCTION_READY_DESCRIPTION_1"); } }
			public static string PRODUCTION_READY_DESCRIPTION_2 		{ get{ return LocalizationManager.GetTranslation ("Notifications/PRODUCTION_READY_DESCRIPTION_2"); } }
			public static string PRODUCTION_READY_DESCRIPTION_3 		{ get{ return LocalizationManager.GetTranslation ("Notifications/PRODUCTION_READY_DESCRIPTION_3"); } }
			public static string PRODUCTION_READY_TITLE 		{ get{ return LocalizationManager.GetTranslation ("Notifications/PRODUCTION_READY_TITLE"); } }
			public static string STORAGE_FULL_DESCRIPTION_1 		{ get{ return LocalizationManager.GetTranslation ("Notifications/STORAGE_FULL_DESCRIPTION_1"); } }
			public static string STORAGE_FULL_DESCRIPTION_2 		{ get{ return LocalizationManager.GetTranslation ("Notifications/STORAGE_FULL_DESCRIPTION_2"); } }
			public static string STORAGE_FULL_DESCRIPTION_3 		{ get{ return LocalizationManager.GetTranslation ("Notifications/STORAGE_FULL_DESCRIPTION_3"); } }
			public static string STORAGE_FULL_TITLE 		{ get{ return LocalizationManager.GetTranslation ("Notifications/STORAGE_FULL_TITLE"); } }
			public static string TAXES_READY_DESCRIPTION_1 		{ get{ return LocalizationManager.GetTranslation ("Notifications/TAXES_READY_DESCRIPTION_1"); } }
			public static string TAXES_READY_DESCRIPTION_2 		{ get{ return LocalizationManager.GetTranslation ("Notifications/TAXES_READY_DESCRIPTION_2"); } }
			public static string TAXES_READY_TITLE 		{ get{ return LocalizationManager.GetTranslation ("Notifications/TAXES_READY_TITLE"); } }
		}

		public static class Quests
		{
			public static string OBJECT_CONDITION_DSCR__1 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_-1"); } }
			public static string OBJECT_CONDITION_DSCR_0 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_0"); } }
			public static string OBJECT_CONDITION_DSCR_1 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_1"); } }
			public static string OBJECT_CONDITION_DSCR_10 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_10"); } }
			public static string OBJECT_CONDITION_DSCR_11 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_11"); } }
			public static string OBJECT_CONDITION_DSCR_12 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_12"); } }
			public static string OBJECT_CONDITION_DSCR_13 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_13"); } }
			public static string OBJECT_CONDITION_DSCR_14 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_14"); } }
			public static string OBJECT_CONDITION_DSCR_15 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_15"); } }
			public static string OBJECT_CONDITION_DSCR_16 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_16"); } }
			public static string OBJECT_CONDITION_DSCR_17 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_17"); } }
			public static string OBJECT_CONDITION_DSCR_18 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_18"); } }
			public static string OBJECT_CONDITION_DSCR_19 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_19"); } }
			public static string OBJECT_CONDITION_DSCR_2 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_2"); } }
			public static string OBJECT_CONDITION_DSCR_20 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_20"); } }
			public static string OBJECT_CONDITION_DSCR_21 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_21"); } }
			public static string OBJECT_CONDITION_DSCR_22 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_22"); } }
			public static string OBJECT_CONDITION_DSCR_23 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_23"); } }
			public static string OBJECT_CONDITION_DSCR_24 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_24"); } }
			public static string OBJECT_CONDITION_DSCR_25 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_25"); } }
			public static string OBJECT_CONDITION_DSCR_26 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_26"); } }
			public static string OBJECT_CONDITION_DSCR_27 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_27"); } }
			public static string OBJECT_CONDITION_DSCR_28 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_28"); } }
			public static string OBJECT_CONDITION_DSCR_29 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_29"); } }
			public static string OBJECT_CONDITION_DSCR_30 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_30"); } }
			public static string OBJECT_CONDITION_DSCR_31 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_31"); } }
			public static string OBJECT_CONDITION_DSCR_32 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_32"); } }
			public static string OBJECT_CONDITION_DSCR_33 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_33"); } }
			public static string OBJECT_CONDITION_DSCR_34 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_34"); } }
			public static string OBJECT_CONDITION_DSCR_35 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_35"); } }
			public static string OBJECT_CONDITION_DSCR_36 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_36"); } }
			public static string OBJECT_CONDITION_DSCR_37 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_37"); } }
			public static string OBJECT_CONDITION_DSCR_38 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_38"); } }
			public static string OBJECT_CONDITION_DSCR_39 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_39"); } }
			public static string OBJECT_CONDITION_DSCR_4 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_4"); } }
			public static string OBJECT_CONDITION_DSCR_40 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_40"); } }
			public static string OBJECT_CONDITION_DSCR_41 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_41"); } }
			public static string OBJECT_CONDITION_DSCR_42 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_42"); } }
			public static string OBJECT_CONDITION_DSCR_43 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_43"); } }
			public static string OBJECT_CONDITION_DSCR_46 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_46"); } }
			public static string OBJECT_CONDITION_DSCR_47 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_47"); } }
			public static string OBJECT_CONDITION_DSCR_48 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_48"); } }
			public static string OBJECT_CONDITION_DSCR_49 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_49"); } }
			public static string OBJECT_CONDITION_DSCR_5 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_5"); } }
			public static string OBJECT_CONDITION_DSCR_50 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_50"); } }
			public static string OBJECT_CONDITION_DSCR_51 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_51"); } }
			public static string OBJECT_CONDITION_DSCR_52 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_52"); } }
			public static string OBJECT_CONDITION_DSCR_53 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_53"); } }
			public static string OBJECT_CONDITION_DSCR_6 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_6"); } }
			public static string OBJECT_CONDITION_DSCR_7 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_7"); } }
			public static string OBJECT_CONDITION_DSCR_8 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_8"); } }
			public static string OBJECT_CONDITION_DSCR_9 		{ get{ return LocalizationManager.GetTranslation ("Quests/OBJECT_CONDITION_DSCR_9"); } }
		}

		public static class Tutorial
		{
			public static string TUTORIAL_HINT_BUILD_HAPPINESS 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_HINT_BUILD_HAPPINESS"); } }
			public static string TUTORIAL_HINT_HAPPINESS_CONGRAT 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_HINT_HAPPINESS_CONGRAT"); } }
			public static string TUTORIAL_HINT_HAPPINESS_INTRO 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_HINT_HAPPINESS_INTRO"); } }
			public static string TUTORIAL_HINT_LOOK_HAPPINESS_BUILDING 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_HINT_LOOK_HAPPINESS_BUILDING"); } }
			public static string TUTORIAL_HINT_REPLACE_HAPPINESS_BUILDING 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_HINT_REPLACE_HAPPINESS_BUILDING"); } }
			public static string TUTORIAL_HINT_ROTATE_HAPPINESS_AOE 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_HINT_ROTATE_HAPPINESS_AOE"); } }
			public static string TUTORIAL_HINT_SHOW 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_HINT_SHOW"); } }
			public static string TUTORIAL_HINT_TITLE 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_HINT_TITLE"); } }
			public static string TUTORIAL_SHOW_ME_BUTTON 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_SHOW_ME_BUTTON"); } }
			public static string TUTORIAL_SKIP_BUTTON 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_SKIP_BUTTON"); } }
			public static string TUTORIAL_DIALOG_SOLAR_PANEL_BUILD 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_SOLAR_PANEL_BUILD"); } }
			public static string TUTORIAL_DIALOG_SOLAR_PANEL_AOE 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_SOLAR_PANEL_AOE"); } }
			public static string TUTORIAL_DIALOG_SERVICE_UNHAPPY 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_SERVICE_UNHAPPY"); } }
			public static string TUTORIAL_DIALOG_SERVICE_PLACE_BUILDING 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_SERVICE_PLACE_BUILDING"); } }
			public static string TUTORIAL_DIALOG_SERVICE_HAPPY 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_SERVICE_HAPPY"); } }
			public static string TUTORIAL_DIALOG_REPLACE_DUPLEX 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_REPLACE_DUPLEX"); } }
			public static string TUTORIAL_DIALOG_MINE_CAN_PRODUCE 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_MINE_CAN_PRODUCE"); } }
			public static string TUTORIAL_DIALOG_MINE_ADD_SLOT 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_MINE_ADD_SLOT"); } }
			public static string TUTORIAL_DIALOG_FINISH_HARD_TUTORAL 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_FINISH_HARD_TUTORAL"); } }
			public static string TUTORIAL_DIALOG_ENGENEER_HELLO 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_ENGENEER_HELLO"); } }
			public static string TUTORIAL_DIALOG_BUILD_APARTMENT_3 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_BUILD_APARTMENT_3"); } }
			public static string TUTORIAL_DIALOG_BUILD_APARTMENT_2 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_BUILD_APARTMENT_2"); } }
			public static string TUTORIAL_DIALOG_BUILD_APARTMENT_1 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_BUILD_APARTMENT_1"); } }
			public static string TUTORIAL_DIALOG_BUILDING_ROADS_2 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_BUILDING_ROADS_2"); } }
			public static string TUTORIAL_DIALOG_BUILDING_ROADS_1 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_BUILDING_ROADS_1"); } }
			public static string TUTORIAL_DIALOG_ASSISTANT_TAXES_REMINDER 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_ASSISTANT_TAXES_REMINDER"); } }
			public static string TUTORIAL_DIALOG_ASSISTANT_SKIP_TIMER 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_ASSISTANT_SKIP_TIMER"); } }
			public static string TUTORIAL_DIALOG_ASSISTANT_RENAME 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_ASSISTANT_RENAME"); } }
			public static string TUTORIAL_DIALOG_ASSISTANT_QUEST 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_ASSISTANT_QUEST"); } }
			public static string TUTORIAL_DIALOG_ASSISTANT_LEVEL_GRATZ 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_ASSISTANT_LEVEL_GRATZ"); } }
			public static string TUTORIAL_DIALOG_ASSISTANT_HELLO_2 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_ASSISTANT_HELLO_2"); } }
			public static string TUTORIAL_DIALOG_ASSISTANT_HELLO_1 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_ASSISTANT_HELLO_1"); } }
			public static string TUTORIAL_DIALOG_ASSISTANT_HAPPY_TAXES 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_ASSISTANT_HAPPY_TAXES"); } }
			public static string TUTORIAL_DIALOG_ASSISTANT_CHECK_TAXES 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_ASSISTANT_CHECK_TAXES"); } }
			public static string TUTORIAL_DIALOG_SERVICE_HAPPINESS_REMINDER 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_SERVICE_HAPPINESS_REMINDER"); } }
			public static string TUTORIAL_DIALOG_SERVICE_HAPPINESS_AOE 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_SERVICE_HAPPINESS_AOE"); } }
			public static string TUTORIAL_DIALOG_SERVICE_BIGGER_AOE 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_SERVICE_BIGGER_AOE"); } }
			public static string TUTORIAL_DIALOG_MINE_PRODUCE_IRON_ORE 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_MINE_PRODUCE_IRON_ORE"); } }
			public static string TUTORIAL_DIALOG_HINT_SEAPORT_THANKS 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_HINT_SEAPORT_THANKS"); } }
			public static string TUTORIAL_DIALOG_HINT_SEAPORT_START 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_HINT_SEAPORT_START"); } }
			public static string TUTORIAL_DIALOG_HINT_SEAPORT_SHUFFLE_LIMIT 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_HINT_SEAPORT_SHUFFLE_LIMIT"); } }
			public static string TUTORIAL_DIALOG_HINT_SEAPORT_ORDER 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_HINT_SEAPORT_ORDER"); } }
			public static string TUTORIAL_DIALOG_HINT_SEAPORT_OPEN 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_HINT_SEAPORT_OPEN"); } }
			public static string TUTORIAL_DIALOG_HINT_SEAPORT_NO_MATERIALS 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_HINT_SEAPORT_NO_MATERIALS"); } }
			public static string TUTORIAL_DIALOG_HINT_SEAPORT_CRATE 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_HINT_SEAPORT_CRATE"); } }
			public static string TUTORIAL_DIALOG_ASSISTANT_HAVE_IRON_ORE 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_ASSISTANT_HAVE_IRON_ORE"); } }
			public static string TUTORIAL_DIALOG_HINT_SEAPORT_FINISH 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_HINT_SEAPORT_FINISH"); } }
			public static string TUTORIAL_DIALOG_HINT_METRO_NEED_PRODUCTS 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_HINT_METRO_NEED_PRODUCTS"); } }
			public static string TUTORIAL_DIALOG_HINT_METRO_GIVE_PRODUCTS 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_HINT_METRO_GIVE_PRODUCTS"); } }
			public static string TUTORIAL_DIALOG_HINT_METRO_GET_PRODUCTS 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_HINT_METRO_GET_PRODUCTS"); } }
			public static string TUTORIAL_DIALOG_HINT_BAZAAR_START 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_HINT_BAZAAR_START"); } }
			public static string TUTORIAL_DIALOG_HINT_BAZAAR_RETURN 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_HINT_BAZAAR_RETURN"); } }
			public static string TUTORIAL_DIALOG_HINT_BAZAAR_OPENED_CHEST 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_HINT_BAZAAR_OPENED_CHEST"); } }
			public static string TUTORIAL_DIALOG_HINT_BAZAAR_NO_MATERIALS 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_HINT_BAZAAR_NO_MATERIALS"); } }
			public static string TUTORIAL_DIALOG_HINT_BAZAAR_NEED_GOLD 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_HINT_BAZAAR_NEED_GOLD"); } }
			public static string TUTORIAL_DIALOG_HINT_BAZAAR_INTRODUCTION 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_HINT_BAZAAR_INTRODUCTION"); } }
			public static string TUTORIAL_DIALOG_HINT_BAZAAR_FINISH 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_HINT_BAZAAR_FINISH"); } }
			public static string TUTORIAL_DIALOG_HINT_BAZAAR_DIRECT_PURCHASE 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_HINT_BAZAAR_DIRECT_PURCHASE"); } }
			public static string TUTORIAL_DIALOG_HINT_BAZAAR_BUY_CHEST 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_HINT_BAZAAR_BUY_CHEST"); } }
			public static string TUTORIAL_DIALOG_HOSPITAL_OPEN 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_HOSPITAL_OPEN"); } }
			public static string TUTORIAL_DIALOG_METRO_ORDER 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_METRO_ORDER"); } }
			public static string TUTORIAL_DIALOG_METRO_REWARD 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_METRO_REWARD"); } }
			public static string TUTORIAL_DIALOG_METRO_START 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_METRO_START"); } }
			public static string TUTORIAL_DIALOG_HOSPITAL_FINISH 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_HOSPITAL_FINISH"); } }
			public static string TUTORIAL_DIALOG_HOSPITAL_BUILDING 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_HOSPITAL_BUILDING"); } }
			public static string TUTORIAL_DIALOG_HINT_SEAPORT_SHUFFLE 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_HINT_SEAPORT_SHUFFLE"); } }
			public static string TUTORIAL_DIALOG_FARM_START 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_FARM_START"); } }
			public static string TUTORIAL_DIALOG_FARM_SOW_PLOTS 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_FARM_SOW_PLOTS"); } }
			public static string TUTORIAL_DIALOG_FARM_SKIP_PLOTS 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_FARM_SKIP_PLOTS"); } }
			public static string TUTORIAL_DIALOG_FARM_SECOND_PLOT 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_FARM_SECOND_PLOT"); } }
			public static string TUTORIAL_DIALOG_FARM_HARVEST 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_FARM_HARVEST"); } }
			public static string TUTORIAL_DIALOG_FARM_FIRST_PLOT 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_FARM_FIRST_PLOT"); } }
			public static string TUTORIAL_DIALOG_FARM_FIND_ME 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_FARM_FIND_ME"); } }
			public static string TUTORIAL_DIALOG_FARM_ELECTRICITY 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_FARM_ELECTRICITY"); } }
			public static string TUTORIAL_DIALOG_FARM_BARNIE 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_FARM_BARNIE"); } }
			public static string TUTORIAL_DIALOG_HOSPITAL_NO_SPACE 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_HOSPITAL_NO_SPACE"); } }
			public static string TUTORIAL_DIALOG_HOSPITAL_SECOND_FINISH 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_HOSPITAL_SECOND_FINISH"); } }
			public static string TUTORIAL_DIALOG_HOSPITAL_THIRD_FINISH 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_HOSPITAL_THIRD_FINISH"); } }
			public static string TUTORIAL_DIALOG_FOOD_SUPPLY_ZONES 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_FOOD_SUPPLY_ZONES"); } }
			public static string TUTORIAL_DIALOG_FOOD_SUPPLY_WELLCOME 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_FOOD_SUPPLY_WELLCOME"); } }
			public static string TUTORIAL_DIALOG_FOOD_SUPPLY_TIMER 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_FOOD_SUPPLY_TIMER"); } }
			public static string TUTORIAL_DIALOG_FOOD_SUPPLY_FEED 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_FOOD_SUPPLY_FEED"); } }
			public static string TUTORIAL_DIALOG_FOOD_SUPPLY_COOK 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_FOOD_SUPPLY_COOK"); } }
			public static string TUTORIAL_DIALOG_METRO_REMEMBER 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_METRO_REMEMBER"); } }
			public static string TUTORIAL_DIALOG_FOOD_SUPPLY_NOTICE 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_FOOD_SUPPLY_NOTICE"); } }
			public static string TUTORIAL_DIALOG_FOOD_SUPPLY_HAVE_FUN 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_FOOD_SUPPLY_HAVE_FUN"); } }
			public static string TUTORIAL_DIALOG_FOOD_SUPPLY_GIFTS 		{ get{ return LocalizationManager.GetTranslation ("Tutorial/TUTORIAL_DIALOG_FOOD_SUPPLY_GIFTS"); } }
		}

		public static class UICommon
		{
			public static string BANING_TEXT_BODY 		{ get{ return LocalizationManager.GetTranslation ("UICommon/BANING_TEXT_BODY"); } }
			public static string BANING_TEXT_TITLE 		{ get{ return LocalizationManager.GetTranslation ("UICommon/BANING_TEXT_TITLE"); } }
			public static string CARGO_TRUCKS_TITLE 		{ get{ return LocalizationManager.GetTranslation ("UICommon/CARGO_TRUCKS_TITLE"); } }
			public static string CHEST_NAME_1 		{ get{ return LocalizationManager.GetTranslation ("UICommon/CHEST_NAME_1"); } }
			public static string CHEST_NAME_2 		{ get{ return LocalizationManager.GetTranslation ("UICommon/CHEST_NAME_2"); } }
			public static string CHEST_NAME_3 		{ get{ return LocalizationManager.GetTranslation ("UICommon/CHEST_NAME_3"); } }
			public static string COINS_TEXT 		{ get{ return LocalizationManager.GetTranslation ("UICommon/COINS_TEXT"); } }
			public static string COMMON_TEXT 		{ get{ return LocalizationManager.GetTranslation ("UICommon/COMMON_TEXT"); } }
			public static string CREDITS_BUTTON 		{ get{ return LocalizationManager.GetTranslation ("UICommon/CREDITS_BUTTON"); } }
			public static string CREDITS_TEXT_BODY 		{ get{ return LocalizationManager.GetTranslation ("UICommon/CREDITS_TEXT_BODY"); } }
			public static string CREDITS_TEXT_TITLE 		{ get{ return LocalizationManager.GetTranslation ("UICommon/CREDITS_TEXT_TITLE"); } }
			public static string INFO_BTN_GO_TO 		{ get{ return LocalizationManager.GetTranslation ("UICommon/INFO_BTN_GO_TO"); } }
			public static string INGRIDIENTS_TEXT 		{ get{ return LocalizationManager.GetTranslation ("UICommon/INGRIDIENTS_TEXT"); } }
			public static string LOAD_TEXT 		{ get{ return LocalizationManager.GetTranslation ("UICommon/LOAD_TEXT"); } }
			public static string NEW_ORDER_WILL_AVAILABLE_TEXT 		{ get{ return LocalizationManager.GetTranslation ("UICommon/NEW_ORDER_WILL_AVAILABLE_TEXT"); } }
			public static string RARE_TEXT 		{ get{ return LocalizationManager.GetTranslation ("UICommon/RARE_TEXT"); } }
			public static string REFRESH_TEXT 		{ get{ return LocalizationManager.GetTranslation ("UICommon/REFRESH_TEXT"); } }
			public static string REWARD_TITLE_TEXT 		{ get{ return LocalizationManager.GetTranslation ("UICommon/REWARD_TITLE_TEXT"); } }
			public static string SEND_TEXT 		{ get{ return LocalizationManager.GetTranslation ("UICommon/SEND_TEXT"); } }
			public static string SHUFFLE_BUTTON_TEXT 		{ get{ return LocalizationManager.GetTranslation ("UICommon/SHUFFLE_BUTTON_TEXT"); } }
			public static string SKIP_BUTTON_TEXT 		{ get{ return LocalizationManager.GetTranslation ("UICommon/SKIP_BUTTON_TEXT"); } }
			public static string COOK_BUTTON_TEXT 		{ get{ return LocalizationManager.GetTranslation ("UICommon/COOK_BUTTON_TEXT"); } }
			public static string COOKING_BUTTON_TEXT 		{ get{ return LocalizationManager.GetTranslation ("UICommon/COOKING_BUTTON_TEXT"); } }
			public static string FODD_SUPPLY_COOK_TOOLTIP 		{ get{ return LocalizationManager.GetTranslation ("UICommon/FODD_SUPPLY_COOK_TOOLTIP"); } }
			public static string ADS_LIMIT_DESCRIPTION 		{ get{ return LocalizationManager.GetTranslation ("UICommon/ADS_LIMIT_DESCRIPTION"); } }
			public static string ADS_LIMIT_TITLE 		{ get{ return LocalizationManager.GetTranslation ("UICommon/ADS_LIMIT_TITLE"); } }
			public static string ADS_PROVIDER_DESCRIPTION 		{ get{ return LocalizationManager.GetTranslation ("UICommon/ADS_PROVIDER_DESCRIPTION"); } }
		}
	}

    public static class ScriptTerms
	{

		public static class Building
		{
		    public const string OBJECT_BUILDING_101 = "Building/OBJECT_BUILDING_101";
		    public const string OBJECT_BUILDING_103 = "Building/OBJECT_BUILDING_103";
		    public const string OBJECT_BUILDING_105 = "Building/OBJECT_BUILDING_105";
		    public const string OBJECT_BUILDING_106 = "Building/OBJECT_BUILDING_106";
		    public const string OBJECT_BUILDING_107 = "Building/OBJECT_BUILDING_107";
		    public const string OBJECT_BUILDING_109 = "Building/OBJECT_BUILDING_109";
		    public const string OBJECT_BUILDING_110 = "Building/OBJECT_BUILDING_110";
		    public const string OBJECT_BUILDING_111 = "Building/OBJECT_BUILDING_111";
		    public const string OBJECT_BUILDING_116 = "Building/OBJECT_BUILDING_116";
		    public const string OBJECT_BUILDING_117 = "Building/OBJECT_BUILDING_117";
		    public const string OBJECT_BUILDING_119 = "Building/OBJECT_BUILDING_119";
		    public const string OBJECT_BUILDING_120 = "Building/OBJECT_BUILDING_120";
		    public const string OBJECT_BUILDING_121 = "Building/OBJECT_BUILDING_121";
		    public const string OBJECT_BUILDING_123 = "Building/OBJECT_BUILDING_123";
		    public const string OBJECT_BUILDING_124 = "Building/OBJECT_BUILDING_124";
		    public const string OBJECT_BUILDING_126 = "Building/OBJECT_BUILDING_126";
		    public const string OBJECT_BUILDING_127 = "Building/OBJECT_BUILDING_127";
		    public const string OBJECT_BUILDING_131 = "Building/OBJECT_BUILDING_131";
		    public const string OBJECT_BUILDING_132 = "Building/OBJECT_BUILDING_132";
		    public const string OBJECT_BUILDING_135 = "Building/OBJECT_BUILDING_135";
		    public const string OBJECT_BUILDING_139 = "Building/OBJECT_BUILDING_139";
		    public const string OBJECT_BUILDING_140 = "Building/OBJECT_BUILDING_140";
		    public const string OBJECT_BUILDING_141 = "Building/OBJECT_BUILDING_141";
		    public const string OBJECT_BUILDING_142 = "Building/OBJECT_BUILDING_142";
		    public const string OBJECT_BUILDING_143 = "Building/OBJECT_BUILDING_143";
		    public const string OBJECT_BUILDING_144 = "Building/OBJECT_BUILDING_144";
		    public const string OBJECT_BUILDING_145 = "Building/OBJECT_BUILDING_145";
		    public const string OBJECT_BUILDING_146 = "Building/OBJECT_BUILDING_146";
		    public const string OBJECT_BUILDING_154 = "Building/OBJECT_BUILDING_154";
		    public const string OBJECT_BUILDING_155 = "Building/OBJECT_BUILDING_155";
		    public const string OBJECT_BUILDING_156 = "Building/OBJECT_BUILDING_156";
		    public const string OBJECT_BUILDING_157 = "Building/OBJECT_BUILDING_157";
		    public const string OBJECT_BUILDING_172 = "Building/OBJECT_BUILDING_172";
		    public const string OBJECT_BUILDING_173 = "Building/OBJECT_BUILDING_173";
		    public const string OBJECT_BUILDING_174 = "Building/OBJECT_BUILDING_174";
		    public const string OBJECT_BUILDING_175 = "Building/OBJECT_BUILDING_175";
		    public const string OBJECT_BUILDING_176 = "Building/OBJECT_BUILDING_176";
		    public const string OBJECT_BUILDING_27 = "Building/OBJECT_BUILDING_27";
		    public const string OBJECT_BUILDING_30 = "Building/OBJECT_BUILDING_30";
		    public const string OBJECT_BUILDING_31 = "Building/OBJECT_BUILDING_31";
		    public const string OBJECT_BUILDING_44 = "Building/OBJECT_BUILDING_44";
		    public const string OBJECT_BUILDING_48 = "Building/OBJECT_BUILDING_48";
		    public const string OBJECT_BUILDING_55 = "Building/OBJECT_BUILDING_55";
		    public const string OBJECT_BUILDING_56 = "Building/OBJECT_BUILDING_56";
		    public const string OBJECT_BUILDING_57 = "Building/OBJECT_BUILDING_57";
		    public const string OBJECT_BUILDING_58 = "Building/OBJECT_BUILDING_58";
		    public const string OBJECT_BUILDING_69 = "Building/OBJECT_BUILDING_69";
		    public const string OBJECT_BUILDING_94 = "Building/OBJECT_BUILDING_94";
		    public const string OBJECT_BUILDING_96 = "Building/OBJECT_BUILDING_96";
		    public const string OBJECT_BUILDING_97 = "Building/OBJECT_BUILDING_97";
		    public const string OBJECT_BUILDING_98 = "Building/OBJECT_BUILDING_98";
		}

		public static class LoaderHint
		{
		    public const string LOADER_HINT_DEFAULT_1 = "LoaderHint/LOADER_HINT_DEFAULT_1";
		    public const string LOADER_HINT_DEFAULT_10 = "LoaderHint/LOADER_HINT_DEFAULT_10";
		    public const string LOADER_HINT_DEFAULT_11 = "LoaderHint/LOADER_HINT_DEFAULT_11";
		    public const string LOADER_HINT_DEFAULT_12 = "LoaderHint/LOADER_HINT_DEFAULT_12";
		    public const string LOADER_HINT_DEFAULT_13 = "LoaderHint/LOADER_HINT_DEFAULT_13";
		    public const string LOADER_HINT_DEFAULT_14 = "LoaderHint/LOADER_HINT_DEFAULT_14";
		    public const string LOADER_HINT_DEFAULT_15 = "LoaderHint/LOADER_HINT_DEFAULT_15";
		    public const string LOADER_HINT_DEFAULT_16 = "LoaderHint/LOADER_HINT_DEFAULT_16";
		    public const string LOADER_HINT_DEFAULT_17 = "LoaderHint/LOADER_HINT_DEFAULT_17";
		    public const string LOADER_HINT_DEFAULT_18 = "LoaderHint/LOADER_HINT_DEFAULT_18";
		    public const string LOADER_HINT_DEFAULT_19 = "LoaderHint/LOADER_HINT_DEFAULT_19";
		    public const string LOADER_HINT_DEFAULT_2 = "LoaderHint/LOADER_HINT_DEFAULT_2";
		    public const string LOADER_HINT_DEFAULT_20 = "LoaderHint/LOADER_HINT_DEFAULT_20";
		    public const string LOADER_HINT_DEFAULT_3 = "LoaderHint/LOADER_HINT_DEFAULT_3";
		    public const string LOADER_HINT_DEFAULT_4 = "LoaderHint/LOADER_HINT_DEFAULT_4";
		    public const string LOADER_HINT_DEFAULT_5 = "LoaderHint/LOADER_HINT_DEFAULT_5";
		    public const string LOADER_HINT_DEFAULT_6 = "LoaderHint/LOADER_HINT_DEFAULT_6";
		    public const string LOADER_HINT_DEFAULT_7 = "LoaderHint/LOADER_HINT_DEFAULT_7";
		    public const string LOADER_HINT_DEFAULT_8 = "LoaderHint/LOADER_HINT_DEFAULT_8";
		    public const string LOADER_HINT_DEFAULT_9 = "LoaderHint/LOADER_HINT_DEFAULT_9";
		}

		public static class Localization
		{
		    public const string ERROR_MAINTENANCE_TITLE = "Localization/ERROR_MAINTENANCE_TITLE";
		    public const string ERROR_MAINTENANCE_DESCRIPTION = "Localization/ERROR_MAINTENANCE_DESCRIPTION";
		}

		public static class Notifications
		{
		    public const string AUCTION_PURCHASE_DESCRIPTION_1 = "Notifications/AUCTION_PURCHASE_DESCRIPTION_1";
		    public const string AUCTION_PURCHASE_DESCRIPTION_2 = "Notifications/AUCTION_PURCHASE_DESCRIPTION_2";
		    public const string AUCTION_PURCHASE_TITLE = "Notifications/AUCTION_PURCHASE_TITLE";
		    public const string AUCTION_SELL_DESCRIPTION_1 = "Notifications/AUCTION_SELL_DESCRIPTION_1";
		    public const string AUCTION_SELL_TITLE = "Notifications/AUCTION_SELL_TITLE";
		    public const string BUILDING_CONSTRUCTED_DESCRIPTION_1 = "Notifications/BUILDING_CONSTRUCTED_DESCRIPTION_1";
		    public const string BUILDING_CONSTRUCTED_DESCRIPTION_2 = "Notifications/BUILDING_CONSTRUCTED_DESCRIPTION_2";
		    public const string BUILDING_CONSTRUCTED_TITLE = "Notifications/BUILDING_CONSTRUCTED_TITLE";
		    public const string BUILDING_UPGRADED_DESCRIPTION_1 = "Notifications/BUILDING_UPGRADED_DESCRIPTION_1";
		    public const string BUILDING_UPGRADED_DESCRIPTION_2 = "Notifications/BUILDING_UPGRADED_DESCRIPTION_2";
		    public const string BUILDING_UPGRADED_DESCRIPTION_3 = "Notifications/BUILDING_UPGRADED_DESCRIPTION_3";
		    public const string BUILDING_UPGRADED_TITLE = "Notifications/BUILDING_UPGRADED_TITLE";
		    public const string COOK_TIME_DESCRIPTION_1 = "Notifications/COOK_TIME_DESCRIPTION_1";
		    public const string COOK_TIME_DESCRIPTION_2 = "Notifications/COOK_TIME_DESCRIPTION_2";
		    public const string COOK_TIME_TITLE = "Notifications/COOK_TIME_TITLE";
		    public const string FOOD_READY_DESCRIPTION_1 = "Notifications/FOOD_READY_DESCRIPTION_1";
		    public const string FOOD_READY_DESCRIPTION_2 = "Notifications/FOOD_READY_DESCRIPTION_2";
		    public const string FOOD_READY_TITLE = "Notifications/FOOD_READY_TITLE";
		    public const string FRIEND_VISIT_DESCRIPTION_1 = "Notifications/FRIEND_VISIT_DESCRIPTION_1";
		    public const string FRIEND_VISIT_TITLE = "Notifications/FRIEND_VISIT_TITLE";
		    public const string HARVEST_READY_DESCRIPTION_1 = "Notifications/HARVEST_READY_DESCRIPTION_1";
		    public const string HARVEST_READY_DESCRIPTION_2 = "Notifications/HARVEST_READY_DESCRIPTION_2";
		    public const string HARVEST_READY_TITLE = "Notifications/HARVEST_READY_TITLE";
		    public const string MISS_YOU_DESCRIPTION_1 = "Notifications/MISS_YOU_DESCRIPTION_1";
		    public const string MISS_YOU_DESCRIPTION_2 = "Notifications/MISS_YOU_DESCRIPTION_2";
		    public const string MISS_YOU_DESCRIPTION_3 = "Notifications/MISS_YOU_DESCRIPTION_3";
		    public const string MISS_YOU_TITLE = "Notifications/MISS_YOU_TITLE";
		    public const string PRODUCTION_READY_DESCRIPTION_1 = "Notifications/PRODUCTION_READY_DESCRIPTION_1";
		    public const string PRODUCTION_READY_DESCRIPTION_2 = "Notifications/PRODUCTION_READY_DESCRIPTION_2";
		    public const string PRODUCTION_READY_DESCRIPTION_3 = "Notifications/PRODUCTION_READY_DESCRIPTION_3";
		    public const string PRODUCTION_READY_TITLE = "Notifications/PRODUCTION_READY_TITLE";
		    public const string STORAGE_FULL_DESCRIPTION_1 = "Notifications/STORAGE_FULL_DESCRIPTION_1";
		    public const string STORAGE_FULL_DESCRIPTION_2 = "Notifications/STORAGE_FULL_DESCRIPTION_2";
		    public const string STORAGE_FULL_DESCRIPTION_3 = "Notifications/STORAGE_FULL_DESCRIPTION_3";
		    public const string STORAGE_FULL_TITLE = "Notifications/STORAGE_FULL_TITLE";
		    public const string TAXES_READY_DESCRIPTION_1 = "Notifications/TAXES_READY_DESCRIPTION_1";
		    public const string TAXES_READY_DESCRIPTION_2 = "Notifications/TAXES_READY_DESCRIPTION_2";
		    public const string TAXES_READY_TITLE = "Notifications/TAXES_READY_TITLE";
		}

		public static class Quests
		{
		    public const string OBJECT_CONDITION_DSCR__1 = "Quests/OBJECT_CONDITION_DSCR_-1";
		    public const string OBJECT_CONDITION_DSCR_0 = "Quests/OBJECT_CONDITION_DSCR_0";
		    public const string OBJECT_CONDITION_DSCR_1 = "Quests/OBJECT_CONDITION_DSCR_1";
		    public const string OBJECT_CONDITION_DSCR_10 = "Quests/OBJECT_CONDITION_DSCR_10";
		    public const string OBJECT_CONDITION_DSCR_11 = "Quests/OBJECT_CONDITION_DSCR_11";
		    public const string OBJECT_CONDITION_DSCR_12 = "Quests/OBJECT_CONDITION_DSCR_12";
		    public const string OBJECT_CONDITION_DSCR_13 = "Quests/OBJECT_CONDITION_DSCR_13";
		    public const string OBJECT_CONDITION_DSCR_14 = "Quests/OBJECT_CONDITION_DSCR_14";
		    public const string OBJECT_CONDITION_DSCR_15 = "Quests/OBJECT_CONDITION_DSCR_15";
		    public const string OBJECT_CONDITION_DSCR_16 = "Quests/OBJECT_CONDITION_DSCR_16";
		    public const string OBJECT_CONDITION_DSCR_17 = "Quests/OBJECT_CONDITION_DSCR_17";
		    public const string OBJECT_CONDITION_DSCR_18 = "Quests/OBJECT_CONDITION_DSCR_18";
		    public const string OBJECT_CONDITION_DSCR_19 = "Quests/OBJECT_CONDITION_DSCR_19";
		    public const string OBJECT_CONDITION_DSCR_2 = "Quests/OBJECT_CONDITION_DSCR_2";
		    public const string OBJECT_CONDITION_DSCR_20 = "Quests/OBJECT_CONDITION_DSCR_20";
		    public const string OBJECT_CONDITION_DSCR_21 = "Quests/OBJECT_CONDITION_DSCR_21";
		    public const string OBJECT_CONDITION_DSCR_22 = "Quests/OBJECT_CONDITION_DSCR_22";
		    public const string OBJECT_CONDITION_DSCR_23 = "Quests/OBJECT_CONDITION_DSCR_23";
		    public const string OBJECT_CONDITION_DSCR_24 = "Quests/OBJECT_CONDITION_DSCR_24";
		    public const string OBJECT_CONDITION_DSCR_25 = "Quests/OBJECT_CONDITION_DSCR_25";
		    public const string OBJECT_CONDITION_DSCR_26 = "Quests/OBJECT_CONDITION_DSCR_26";
		    public const string OBJECT_CONDITION_DSCR_27 = "Quests/OBJECT_CONDITION_DSCR_27";
		    public const string OBJECT_CONDITION_DSCR_28 = "Quests/OBJECT_CONDITION_DSCR_28";
		    public const string OBJECT_CONDITION_DSCR_29 = "Quests/OBJECT_CONDITION_DSCR_29";
		    public const string OBJECT_CONDITION_DSCR_30 = "Quests/OBJECT_CONDITION_DSCR_30";
		    public const string OBJECT_CONDITION_DSCR_31 = "Quests/OBJECT_CONDITION_DSCR_31";
		    public const string OBJECT_CONDITION_DSCR_32 = "Quests/OBJECT_CONDITION_DSCR_32";
		    public const string OBJECT_CONDITION_DSCR_33 = "Quests/OBJECT_CONDITION_DSCR_33";
		    public const string OBJECT_CONDITION_DSCR_34 = "Quests/OBJECT_CONDITION_DSCR_34";
		    public const string OBJECT_CONDITION_DSCR_35 = "Quests/OBJECT_CONDITION_DSCR_35";
		    public const string OBJECT_CONDITION_DSCR_36 = "Quests/OBJECT_CONDITION_DSCR_36";
		    public const string OBJECT_CONDITION_DSCR_37 = "Quests/OBJECT_CONDITION_DSCR_37";
		    public const string OBJECT_CONDITION_DSCR_38 = "Quests/OBJECT_CONDITION_DSCR_38";
		    public const string OBJECT_CONDITION_DSCR_39 = "Quests/OBJECT_CONDITION_DSCR_39";
		    public const string OBJECT_CONDITION_DSCR_4 = "Quests/OBJECT_CONDITION_DSCR_4";
		    public const string OBJECT_CONDITION_DSCR_40 = "Quests/OBJECT_CONDITION_DSCR_40";
		    public const string OBJECT_CONDITION_DSCR_41 = "Quests/OBJECT_CONDITION_DSCR_41";
		    public const string OBJECT_CONDITION_DSCR_42 = "Quests/OBJECT_CONDITION_DSCR_42";
		    public const string OBJECT_CONDITION_DSCR_43 = "Quests/OBJECT_CONDITION_DSCR_43";
		    public const string OBJECT_CONDITION_DSCR_46 = "Quests/OBJECT_CONDITION_DSCR_46";
		    public const string OBJECT_CONDITION_DSCR_47 = "Quests/OBJECT_CONDITION_DSCR_47";
		    public const string OBJECT_CONDITION_DSCR_48 = "Quests/OBJECT_CONDITION_DSCR_48";
		    public const string OBJECT_CONDITION_DSCR_49 = "Quests/OBJECT_CONDITION_DSCR_49";
		    public const string OBJECT_CONDITION_DSCR_5 = "Quests/OBJECT_CONDITION_DSCR_5";
		    public const string OBJECT_CONDITION_DSCR_50 = "Quests/OBJECT_CONDITION_DSCR_50";
		    public const string OBJECT_CONDITION_DSCR_51 = "Quests/OBJECT_CONDITION_DSCR_51";
		    public const string OBJECT_CONDITION_DSCR_52 = "Quests/OBJECT_CONDITION_DSCR_52";
		    public const string OBJECT_CONDITION_DSCR_53 = "Quests/OBJECT_CONDITION_DSCR_53";
		    public const string OBJECT_CONDITION_DSCR_6 = "Quests/OBJECT_CONDITION_DSCR_6";
		    public const string OBJECT_CONDITION_DSCR_7 = "Quests/OBJECT_CONDITION_DSCR_7";
		    public const string OBJECT_CONDITION_DSCR_8 = "Quests/OBJECT_CONDITION_DSCR_8";
		    public const string OBJECT_CONDITION_DSCR_9 = "Quests/OBJECT_CONDITION_DSCR_9";
		}

		public static class Tutorial
		{
		    public const string TUTORIAL_HINT_BUILD_HAPPINESS = "Tutorial/TUTORIAL_HINT_BUILD_HAPPINESS";
		    public const string TUTORIAL_HINT_HAPPINESS_CONGRAT = "Tutorial/TUTORIAL_HINT_HAPPINESS_CONGRAT";
		    public const string TUTORIAL_HINT_HAPPINESS_INTRO = "Tutorial/TUTORIAL_HINT_HAPPINESS_INTRO";
		    public const string TUTORIAL_HINT_LOOK_HAPPINESS_BUILDING = "Tutorial/TUTORIAL_HINT_LOOK_HAPPINESS_BUILDING";
		    public const string TUTORIAL_HINT_REPLACE_HAPPINESS_BUILDING = "Tutorial/TUTORIAL_HINT_REPLACE_HAPPINESS_BUILDING";
		    public const string TUTORIAL_HINT_ROTATE_HAPPINESS_AOE = "Tutorial/TUTORIAL_HINT_ROTATE_HAPPINESS_AOE";
		    public const string TUTORIAL_HINT_SHOW = "Tutorial/TUTORIAL_HINT_SHOW";
		    public const string TUTORIAL_HINT_TITLE = "Tutorial/TUTORIAL_HINT_TITLE";
		    public const string TUTORIAL_SHOW_ME_BUTTON = "Tutorial/TUTORIAL_SHOW_ME_BUTTON";
		    public const string TUTORIAL_SKIP_BUTTON = "Tutorial/TUTORIAL_SKIP_BUTTON";
		    public const string TUTORIAL_DIALOG_SOLAR_PANEL_BUILD = "Tutorial/TUTORIAL_DIALOG_SOLAR_PANEL_BUILD";
		    public const string TUTORIAL_DIALOG_SOLAR_PANEL_AOE = "Tutorial/TUTORIAL_DIALOG_SOLAR_PANEL_AOE";
		    public const string TUTORIAL_DIALOG_SERVICE_UNHAPPY = "Tutorial/TUTORIAL_DIALOG_SERVICE_UNHAPPY";
		    public const string TUTORIAL_DIALOG_SERVICE_PLACE_BUILDING = "Tutorial/TUTORIAL_DIALOG_SERVICE_PLACE_BUILDING";
		    public const string TUTORIAL_DIALOG_SERVICE_HAPPY = "Tutorial/TUTORIAL_DIALOG_SERVICE_HAPPY";
		    public const string TUTORIAL_DIALOG_REPLACE_DUPLEX = "Tutorial/TUTORIAL_DIALOG_REPLACE_DUPLEX";
		    public const string TUTORIAL_DIALOG_MINE_CAN_PRODUCE = "Tutorial/TUTORIAL_DIALOG_MINE_CAN_PRODUCE";
		    public const string TUTORIAL_DIALOG_MINE_ADD_SLOT = "Tutorial/TUTORIAL_DIALOG_MINE_ADD_SLOT";
		    public const string TUTORIAL_DIALOG_FINISH_HARD_TUTORAL = "Tutorial/TUTORIAL_DIALOG_FINISH_HARD_TUTORAL";
		    public const string TUTORIAL_DIALOG_ENGENEER_HELLO = "Tutorial/TUTORIAL_DIALOG_ENGENEER_HELLO";
		    public const string TUTORIAL_DIALOG_BUILD_APARTMENT_3 = "Tutorial/TUTORIAL_DIALOG_BUILD_APARTMENT_3";
		    public const string TUTORIAL_DIALOG_BUILD_APARTMENT_2 = "Tutorial/TUTORIAL_DIALOG_BUILD_APARTMENT_2";
		    public const string TUTORIAL_DIALOG_BUILD_APARTMENT_1 = "Tutorial/TUTORIAL_DIALOG_BUILD_APARTMENT_1";
		    public const string TUTORIAL_DIALOG_BUILDING_ROADS_2 = "Tutorial/TUTORIAL_DIALOG_BUILDING_ROADS_2";
		    public const string TUTORIAL_DIALOG_BUILDING_ROADS_1 = "Tutorial/TUTORIAL_DIALOG_BUILDING_ROADS_1";
		    public const string TUTORIAL_DIALOG_ASSISTANT_TAXES_REMINDER = "Tutorial/TUTORIAL_DIALOG_ASSISTANT_TAXES_REMINDER";
		    public const string TUTORIAL_DIALOG_ASSISTANT_SKIP_TIMER = "Tutorial/TUTORIAL_DIALOG_ASSISTANT_SKIP_TIMER";
		    public const string TUTORIAL_DIALOG_ASSISTANT_RENAME = "Tutorial/TUTORIAL_DIALOG_ASSISTANT_RENAME";
		    public const string TUTORIAL_DIALOG_ASSISTANT_QUEST = "Tutorial/TUTORIAL_DIALOG_ASSISTANT_QUEST";
		    public const string TUTORIAL_DIALOG_ASSISTANT_LEVEL_GRATZ = "Tutorial/TUTORIAL_DIALOG_ASSISTANT_LEVEL_GRATZ";
		    public const string TUTORIAL_DIALOG_ASSISTANT_HELLO_2 = "Tutorial/TUTORIAL_DIALOG_ASSISTANT_HELLO_2";
		    public const string TUTORIAL_DIALOG_ASSISTANT_HELLO_1 = "Tutorial/TUTORIAL_DIALOG_ASSISTANT_HELLO_1";
		    public const string TUTORIAL_DIALOG_ASSISTANT_HAPPY_TAXES = "Tutorial/TUTORIAL_DIALOG_ASSISTANT_HAPPY_TAXES";
		    public const string TUTORIAL_DIALOG_ASSISTANT_CHECK_TAXES = "Tutorial/TUTORIAL_DIALOG_ASSISTANT_CHECK_TAXES";
		    public const string TUTORIAL_DIALOG_SERVICE_HAPPINESS_REMINDER = "Tutorial/TUTORIAL_DIALOG_SERVICE_HAPPINESS_REMINDER";
		    public const string TUTORIAL_DIALOG_SERVICE_HAPPINESS_AOE = "Tutorial/TUTORIAL_DIALOG_SERVICE_HAPPINESS_AOE";
		    public const string TUTORIAL_DIALOG_SERVICE_BIGGER_AOE = "Tutorial/TUTORIAL_DIALOG_SERVICE_BIGGER_AOE";
		    public const string TUTORIAL_DIALOG_MINE_PRODUCE_IRON_ORE = "Tutorial/TUTORIAL_DIALOG_MINE_PRODUCE_IRON_ORE";
		    public const string TUTORIAL_DIALOG_HINT_SEAPORT_THANKS = "Tutorial/TUTORIAL_DIALOG_HINT_SEAPORT_THANKS";
		    public const string TUTORIAL_DIALOG_HINT_SEAPORT_START = "Tutorial/TUTORIAL_DIALOG_HINT_SEAPORT_START";
		    public const string TUTORIAL_DIALOG_HINT_SEAPORT_SHUFFLE_LIMIT = "Tutorial/TUTORIAL_DIALOG_HINT_SEAPORT_SHUFFLE_LIMIT";
		    public const string TUTORIAL_DIALOG_HINT_SEAPORT_ORDER = "Tutorial/TUTORIAL_DIALOG_HINT_SEAPORT_ORDER";
		    public const string TUTORIAL_DIALOG_HINT_SEAPORT_OPEN = "Tutorial/TUTORIAL_DIALOG_HINT_SEAPORT_OPEN";
		    public const string TUTORIAL_DIALOG_HINT_SEAPORT_NO_MATERIALS = "Tutorial/TUTORIAL_DIALOG_HINT_SEAPORT_NO_MATERIALS";
		    public const string TUTORIAL_DIALOG_HINT_SEAPORT_CRATE = "Tutorial/TUTORIAL_DIALOG_HINT_SEAPORT_CRATE";
		    public const string TUTORIAL_DIALOG_ASSISTANT_HAVE_IRON_ORE = "Tutorial/TUTORIAL_DIALOG_ASSISTANT_HAVE_IRON_ORE";
		    public const string TUTORIAL_DIALOG_HINT_SEAPORT_FINISH = "Tutorial/TUTORIAL_DIALOG_HINT_SEAPORT_FINISH";
		    public const string TUTORIAL_DIALOG_HINT_METRO_NEED_PRODUCTS = "Tutorial/TUTORIAL_DIALOG_HINT_METRO_NEED_PRODUCTS";
		    public const string TUTORIAL_DIALOG_HINT_METRO_GIVE_PRODUCTS = "Tutorial/TUTORIAL_DIALOG_HINT_METRO_GIVE_PRODUCTS";
		    public const string TUTORIAL_DIALOG_HINT_METRO_GET_PRODUCTS = "Tutorial/TUTORIAL_DIALOG_HINT_METRO_GET_PRODUCTS";
		    public const string TUTORIAL_DIALOG_HINT_BAZAAR_START = "Tutorial/TUTORIAL_DIALOG_HINT_BAZAAR_START";
		    public const string TUTORIAL_DIALOG_HINT_BAZAAR_RETURN = "Tutorial/TUTORIAL_DIALOG_HINT_BAZAAR_RETURN";
		    public const string TUTORIAL_DIALOG_HINT_BAZAAR_OPENED_CHEST = "Tutorial/TUTORIAL_DIALOG_HINT_BAZAAR_OPENED_CHEST";
		    public const string TUTORIAL_DIALOG_HINT_BAZAAR_NO_MATERIALS = "Tutorial/TUTORIAL_DIALOG_HINT_BAZAAR_NO_MATERIALS";
		    public const string TUTORIAL_DIALOG_HINT_BAZAAR_NEED_GOLD = "Tutorial/TUTORIAL_DIALOG_HINT_BAZAAR_NEED_GOLD";
		    public const string TUTORIAL_DIALOG_HINT_BAZAAR_INTRODUCTION = "Tutorial/TUTORIAL_DIALOG_HINT_BAZAAR_INTRODUCTION";
		    public const string TUTORIAL_DIALOG_HINT_BAZAAR_FINISH = "Tutorial/TUTORIAL_DIALOG_HINT_BAZAAR_FINISH";
		    public const string TUTORIAL_DIALOG_HINT_BAZAAR_DIRECT_PURCHASE = "Tutorial/TUTORIAL_DIALOG_HINT_BAZAAR_DIRECT_PURCHASE";
		    public const string TUTORIAL_DIALOG_HINT_BAZAAR_BUY_CHEST = "Tutorial/TUTORIAL_DIALOG_HINT_BAZAAR_BUY_CHEST";
		    public const string TUTORIAL_DIALOG_HOSPITAL_OPEN = "Tutorial/TUTORIAL_DIALOG_HOSPITAL_OPEN";
		    public const string TUTORIAL_DIALOG_METRO_ORDER = "Tutorial/TUTORIAL_DIALOG_METRO_ORDER";
		    public const string TUTORIAL_DIALOG_METRO_REWARD = "Tutorial/TUTORIAL_DIALOG_METRO_REWARD";
		    public const string TUTORIAL_DIALOG_METRO_START = "Tutorial/TUTORIAL_DIALOG_METRO_START";
		    public const string TUTORIAL_DIALOG_HOSPITAL_FINISH = "Tutorial/TUTORIAL_DIALOG_HOSPITAL_FINISH";
		    public const string TUTORIAL_DIALOG_HOSPITAL_BUILDING = "Tutorial/TUTORIAL_DIALOG_HOSPITAL_BUILDING";
		    public const string TUTORIAL_DIALOG_HINT_SEAPORT_SHUFFLE = "Tutorial/TUTORIAL_DIALOG_HINT_SEAPORT_SHUFFLE";
		    public const string TUTORIAL_DIALOG_FARM_START = "Tutorial/TUTORIAL_DIALOG_FARM_START";
		    public const string TUTORIAL_DIALOG_FARM_SOW_PLOTS = "Tutorial/TUTORIAL_DIALOG_FARM_SOW_PLOTS";
		    public const string TUTORIAL_DIALOG_FARM_SKIP_PLOTS = "Tutorial/TUTORIAL_DIALOG_FARM_SKIP_PLOTS";
		    public const string TUTORIAL_DIALOG_FARM_SECOND_PLOT = "Tutorial/TUTORIAL_DIALOG_FARM_SECOND_PLOT";
		    public const string TUTORIAL_DIALOG_FARM_HARVEST = "Tutorial/TUTORIAL_DIALOG_FARM_HARVEST";
		    public const string TUTORIAL_DIALOG_FARM_FIRST_PLOT = "Tutorial/TUTORIAL_DIALOG_FARM_FIRST_PLOT";
		    public const string TUTORIAL_DIALOG_FARM_FIND_ME = "Tutorial/TUTORIAL_DIALOG_FARM_FIND_ME";
		    public const string TUTORIAL_DIALOG_FARM_ELECTRICITY = "Tutorial/TUTORIAL_DIALOG_FARM_ELECTRICITY";
		    public const string TUTORIAL_DIALOG_FARM_BARNIE = "Tutorial/TUTORIAL_DIALOG_FARM_BARNIE";
		    public const string TUTORIAL_DIALOG_HOSPITAL_NO_SPACE = "Tutorial/TUTORIAL_DIALOG_HOSPITAL_NO_SPACE";
		    public const string TUTORIAL_DIALOG_HOSPITAL_SECOND_FINISH = "Tutorial/TUTORIAL_DIALOG_HOSPITAL_SECOND_FINISH";
		    public const string TUTORIAL_DIALOG_HOSPITAL_THIRD_FINISH = "Tutorial/TUTORIAL_DIALOG_HOSPITAL_THIRD_FINISH";
		    public const string TUTORIAL_DIALOG_FOOD_SUPPLY_ZONES = "Tutorial/TUTORIAL_DIALOG_FOOD_SUPPLY_ZONES";
		    public const string TUTORIAL_DIALOG_FOOD_SUPPLY_WELLCOME = "Tutorial/TUTORIAL_DIALOG_FOOD_SUPPLY_WELLCOME";
		    public const string TUTORIAL_DIALOG_FOOD_SUPPLY_TIMER = "Tutorial/TUTORIAL_DIALOG_FOOD_SUPPLY_TIMER";
		    public const string TUTORIAL_DIALOG_FOOD_SUPPLY_FEED = "Tutorial/TUTORIAL_DIALOG_FOOD_SUPPLY_FEED";
		    public const string TUTORIAL_DIALOG_FOOD_SUPPLY_COOK = "Tutorial/TUTORIAL_DIALOG_FOOD_SUPPLY_COOK";
		    public const string TUTORIAL_DIALOG_METRO_REMEMBER = "Tutorial/TUTORIAL_DIALOG_METRO_REMEMBER";
		    public const string TUTORIAL_DIALOG_FOOD_SUPPLY_NOTICE = "Tutorial/TUTORIAL_DIALOG_FOOD_SUPPLY_NOTICE";
		    public const string TUTORIAL_DIALOG_FOOD_SUPPLY_HAVE_FUN = "Tutorial/TUTORIAL_DIALOG_FOOD_SUPPLY_HAVE_FUN";
		    public const string TUTORIAL_DIALOG_FOOD_SUPPLY_GIFTS = "Tutorial/TUTORIAL_DIALOG_FOOD_SUPPLY_GIFTS";
		    public const string TUTORIAL_THE_EVENT_Key_1 = "Tutorial/TUTORIAL_THE_EVENT_Key_1";
		    public const string TUTORIAL_THE_EVENT_Key_2 = "Tutorial/TUTORIAL_THE_EVENT_Key_2";
		    public const string TUTORIAL_THE_EVENT_Key_3 = "Tutorial/TUTORIAL_THE_EVENT_Key_3";
		    public const string TUTORIAL_THE_EVENT_Key_4 = "Tutorial/TUTORIAL_THE_EVENT_Key_4";
		    public const string TUTORIAL_THE_EVENT_Key_5 = "Tutorial/TUTORIAL_THE_EVENT_Key_5";
		    public const string TUTORIAL_THE_EVENT_Key_6 = "Tutorial/TUTORIAL_THE_EVENT_Key_6";
		    public const string TUTORIAL_THE_EVENT_Key_7 = "Tutorial/TUTORIAL_THE_EVENT_Key_7";
		    public const string TUTORIAL_THE_EVENT_Key_8 = "Tutorial/TUTORIAL_THE_EVENT_Key_8";
		    public const string TUTORIAL_THE_EVENT_Key_9 = "Tutorial/TUTORIAL_THE_EVENT_Key_9";
		    public const string TUTORIAL_THE_EVENT_Key_10 = "Tutorial/TUTORIAL_THE_EVENT_Key_10";
		    public const string TUTORIAL_THE_EVENT_Key_11 = "Tutorial/TUTORIAL_THE_EVENT_Key_11";
		    public const string TUTORIAL_THE_EVENT_Key_12 = "Tutorial/TUTORIAL_THE_EVENT_Key_12";
		}

		public static class UICommon
		{
		    public const string BANING_TEXT_BODY = "UICommon/BANING_TEXT_BODY";
		    public const string BANING_TEXT_TITLE = "UICommon/BANING_TEXT_TITLE";
		    public const string CARGO_TRUCKS_TITLE = "UICommon/CARGO_TRUCKS_TITLE";
		    public const string CHEST_NAME_1 = "UICommon/CHEST_NAME_1";
		    public const string CHEST_NAME_2 = "UICommon/CHEST_NAME_2";
		    public const string CHEST_NAME_3 = "UICommon/CHEST_NAME_3";
		    public const string COINS_TEXT = "UICommon/COINS_TEXT";
		    public const string COMMON_TEXT = "UICommon/COMMON_TEXT";
		    public const string CREDITS_BUTTON = "UICommon/CREDITS_BUTTON";
		    public const string CREDITS_TEXT_BODY = "UICommon/CREDITS_TEXT_BODY";
		    public const string CREDITS_TEXT_TITLE = "UICommon/CREDITS_TEXT_TITLE";
		    public const string INFO_BTN_GO_TO = "UICommon/INFO_BTN_GO_TO";
		    public const string INGRIDIENTS_TEXT = "UICommon/INGRIDIENTS_TEXT";
		    public const string LOAD_TEXT = "UICommon/LOAD_TEXT";
		    public const string NEW_ORDER_WILL_AVAILABLE_TEXT = "UICommon/NEW_ORDER_WILL_AVAILABLE_TEXT";
		    public const string RARE_TEXT = "UICommon/RARE_TEXT";
		    public const string REFRESH_TEXT = "UICommon/REFRESH_TEXT";
		    public const string REWARD_TITLE_TEXT = "UICommon/REWARD_TITLE_TEXT";
		    public const string SEND_TEXT = "UICommon/SEND_TEXT";
		    public const string SHUFFLE_BUTTON_TEXT = "UICommon/SHUFFLE_BUTTON_TEXT";
		    public const string SKIP_BUTTON_TEXT = "UICommon/SKIP_BUTTON_TEXT";
		    public const string COOK_BUTTON_TEXT = "UICommon/COOK_BUTTON_TEXT";
		    public const string COOKING_BUTTON_TEXT = "UICommon/COOKING_BUTTON_TEXT";
		    public const string FODD_SUPPLY_COOK_TOOLTIP = "UICommon/FODD_SUPPLY_COOK_TOOLTIP";
		    public const string ADS_LIMIT_DESCRIPTION = "UICommon/ADS_LIMIT_DESCRIPTION";
		    public const string ADS_LIMIT_TITLE = "UICommon/ADS_LIMIT_TITLE";
		    public const string ADS_PROVIDER_DESCRIPTION = "UICommon/ADS_PROVIDER_DESCRIPTION";
		}
	}
}