﻿using System;
using System.Collections.Generic;
using GoogleMobileAds.Api;
using NanoLib.Core.Logging;
using NanoReality.Game.Advertisements;
using UniRx;

namespace NanoReality.Game.Services.Advertisements
{
    public class GoogleAdsWrapper : IAdsWrapper
    {
        private readonly string _unitId;
        private readonly List<string> _deviceIds;

        private RewardedAd _rewardedAd;
        private bool _rewardIsEarned;

        public bool IsVideoLoaded => _rewardedAd != null && _rewardedAd.IsLoaded();

        public event EventHandler VideoStarted;
        public event EventHandler VideoLoaded;
        public event EventHandler VideoWatched;
        public event EventHandler VideoSkipped;

        public GoogleAdsWrapper(string unitId, List<string> deviceIds)
        {
            _unitId = unitId;
            _deviceIds = deviceIds;
        }

        public void Initialize()
        {
            var requestConfiguration = new RequestConfiguration.Builder()
                .SetTagForChildDirectedTreatment(TagForChildDirectedTreatment.True)
                .SetTestDeviceIds(_deviceIds).build();

            MobileAds.SetRequestConfiguration(requestConfiguration);
            MobileAds.Initialize(OnInit);
        }

        public void ShowVideo()
        {
            _rewardedAd.Show();
        }

        public void LoadVideo()
        {
            if (_rewardedAd == null)
                CreateAndLoadRewardedAd();
        }

        public void Dispose()
        {
            if (_rewardedAd == null) return;

            RemoveListeners(_rewardedAd);
            _rewardedAd = null;
        }

        private void CreateAndLoadRewardedAd()
        {
            _rewardedAd = new RewardedAd(_unitId);
            AddListeners(_rewardedAd);

            var request = new AdRequest.Builder().Build();
            
            $"Google ad loading started".Log(LoggingChannel.Ads);
            _rewardedAd.LoadAd(request);
        }

        private void OnAdFailedToShow(object sender, AdErrorEventArgs e)
        {
            MainThreadDispatcher.Send((c) =>
            {
                $"A google ad failed to show {e.Message}".LogError(LoggingChannel.Ads);
            }, null);
        }

        private void OnAdFailedToLoad(object sender, AdErrorEventArgs e)
        {
            MainThreadDispatcher.Send((c) =>
            {
                $"A google ad failed to load {e.Message}".LogError(LoggingChannel.Ads);
            }, null);
        }

        private void OnAdOpening(object sender, EventArgs e)
        {
            MainThreadDispatcher.Send((c) =>
            {
                $"Google ad opened".Log(LoggingChannel.Ads);
                
                VideoStarted?.Invoke(this, e);
            }, null);
        }

        private void OnAdClosed(object sender, EventArgs e)
        {
            MainThreadDispatcher.Send((c) =>
            {
                $"Google ad closed. Is reward earned: {_rewardIsEarned}".Log(LoggingChannel.Ads);
                
                Dispose();
                
                if (!_rewardIsEarned)
                {
                    VideoSkipped?.Invoke(this, e);
                }
                _rewardIsEarned = false;
            }, null);
        }

        private void OnUserEarnedReward(object sender, Reward e)
        {
            MainThreadDispatcher.Send((c) =>
            {
                "User earned reward".Log(LoggingChannel.Ads);
                
                _rewardIsEarned = true;
                VideoWatched?.Invoke(this, e);

                Dispose();
            }, null);
        }

        private void OnAdLoaded(object sender, EventArgs e)
        {
            MainThreadDispatcher.Send((c) =>
            {
                "Google ad loaded".Log(LoggingChannel.Ads);
                
                VideoLoaded?.Invoke(this, e);
            }, null);
        }

        private void RemoveListeners(RewardedAd rewardedAd)
        {
            rewardedAd.OnAdLoaded -= OnAdLoaded;
            rewardedAd.OnUserEarnedReward -= OnUserEarnedReward;
            rewardedAd.OnAdClosed -= OnAdClosed;
            rewardedAd.OnAdOpening -= OnAdOpening;
            rewardedAd.OnAdFailedToLoad -= OnAdFailedToLoad;
            rewardedAd.OnAdFailedToShow -= OnAdFailedToShow;
        }

        private void AddListeners(RewardedAd rewardedAd)
        {
            rewardedAd.OnAdLoaded += OnAdLoaded;
            rewardedAd.OnUserEarnedReward += OnUserEarnedReward;
            rewardedAd.OnAdClosed += OnAdClosed;
            rewardedAd.OnAdOpening += OnAdOpening;
            rewardedAd.OnAdFailedToLoad += OnAdFailedToLoad;
            rewardedAd.OnAdFailedToShow += OnAdFailedToShow;
        }

        private void OnInit(InitializationStatus status)
        {
        }
    }
}