﻿using System;

namespace NanoReality.Game.Advertisements
{
    public interface IAdsWrapper : IDisposable
    {
        bool IsVideoLoaded { get; }
        event EventHandler VideoStarted;
        event EventHandler VideoLoaded;
        event EventHandler VideoWatched;
        event EventHandler VideoSkipped;

        void Initialize();
        void ShowVideo();
        void LoadVideo();
    }
}