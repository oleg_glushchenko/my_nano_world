using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace NanoReality.Game.Utilities
{
    public static class PriceUtility
    {
        public static PriceType GetPriceType(int value)
        {
            PriceType resultValue = PriceType.None;
            
            switch (value)
            {
                case 0: resultValue = PriceType.None; break;
                case 1: resultValue = PriceType.Soft; break;
                case 2: resultValue = PriceType.Hard; break;
                case 3: resultValue = PriceType.Resources; break;
                case 4: resultValue = PriceType.Soft | PriceType.Hard; break;
                case 5: resultValue = PriceType.Soft | PriceType.Resources; break;
                case 6: resultValue = PriceType.Hard | PriceType.Resources; break;
                case 7: resultValue = PriceType.Soft | PriceType.Hard | PriceType.Resources; break;
                case 8: resultValue = PriceType.ListResources; break;
                case 10: resultValue = PriceType.Gold; break;
            }

            return resultValue;
        }
    }
}