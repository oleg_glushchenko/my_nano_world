using I2.Loc;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.Game.UI;

namespace Game.Tutorial.HintTutorial
{
    [HintStep(HintType.TheEventHint3)]
    public class TheEventTutorialHintStep3 : HintTutorialStep
    {
        protected override void AddTutorialActions()
        {
            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_THE_EVENT_Key_12,
                DialogCharacter.Girl, DialogPosition.Left, CharacterExpression.Cool);
        }
    }
}