using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using I2.Loc;
using NanoLib.Services.InputService;
using NanoReality.Engine.UI.Views.ConstractConfirmPanel;
using NanoReality.Engine.UI.Views.Entertaiment;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using UnityEngine;

namespace Game.Tutorial.HintTutorial
{
    [HintStep(HintType.HappinessAoe)]
    public sealed class AoeHintStep : HintTutorialStep
    {
        [Inject] public SignalOnCancelConstractInView jSignalOnCancelConstractInView { get; set; }

        private const BusinessSectorsTypes BuildingSector = BusinessSectorsTypes.Town;

        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();
            
            MapObjectView objectView = FindMapObjectViewOnMapByType(BuildingSector, MapObjectintTypes.EntertainmentBuilding, 0);
            jSignalOnCancelConstractInView.Dispatch();
            
            TutorialAction action = AddOpenMapObjectPanelAction(objectView, typeof(EntertaimentAoeLayoutPanelView));
            action.OnComplete += () => 
            {
                var changeLayoutAction = CreateAction<HighlightToggleTutorialAction>();
                changeLayoutAction.Init(GetLayoutButtonObject);
                changeLayoutAction.OnComplete += HideFireEffects;
                ActionsToDoList.Add(changeLayoutAction);

                AddWaitForSecondsAction(1.0f);

                AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_HINT_HAPPINESS_CONGRAT, DialogCharacter.Girl);
            };
            action.SetDialog(new DialogData(ScriptLocalization.Tutorial.TUTORIAL_HINT_LOOK_HAPPINESS_BUILDING,DialogCharacter.Girl))
                .SetCamera(objectView.MapObjectModel)
                .SetInputState(InputActionType.Tap);

            AddWaitForPanelCloseAction(typeof(EntertaimentAoeLayoutPanelView));
            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_HINT_HAPPINESS_CONGRAT, DialogCharacter.Girl);
        }
        
        private void HideFireEffects()
        {
            List<IMapObject> mapDwellings = jGameManager.FindAllMapObjectsOnMapByType(BuildingSector, MapObjectintTypes.DwellingHouse);
            
            if (mapDwellings == null)
                return;
            
            foreach (MapObjectView buildingView in mapDwellings.Select(dwelling => jGameManager.GetMapObjectView(dwelling)))
            {
                buildingView.VFXComponent.HideFireEffect();
            }
        }
        
        private GameObject GetLayoutButtonObject()
        {
            return jUIManager.GetView<EntertaimentAoeLayoutPanelView>().GetToggleFor(AoeLayout.BottomRight).gameObject;
        }
    }
}