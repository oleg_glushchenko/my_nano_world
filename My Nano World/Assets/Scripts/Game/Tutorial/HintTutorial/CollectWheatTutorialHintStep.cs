using System.Collections.Generic;
using I2.Loc;
using NanoLib.Services.InputService;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using UnityEngine;

namespace Game.Tutorial.HintTutorial
{
    [HintStep(HintType.CollectWheat)]
    public sealed class CollectWheatTutorialHintStep : HintTutorialStep
    {
        private readonly Vector2 _buildCameraPosition = new Vector2(20, 2);
        private const BusinessSectorsTypes BuildingSector = BusinessSectorsTypes.Farm;
        
        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();
            
            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_FARM_HARVEST,
                DialogCharacter.Farmer, DialogPosition.Left, CharacterExpression.Idle).SetCameraInSector(_buildCameraPosition.ToVector2F(), BuildingSector);

            var agrofieldsAction = CreateAction<TutorialActionStartHarvestCrops>();

            ActionsToDoList.Add(agrofieldsAction);
            agrofieldsAction.SetInputState(InputActionType.Tap);
            IEnumerable<IAgroFieldBuilding> agrofields =
                FindAllMapObjects<IAgroFieldBuilding>(BusinessSectorsTypes.Farm,
                    MapObjectintTypes.AgriculturalField);

            agrofieldsAction.SetBuildingModel(agrofields);
            agrofieldsAction.OnComplete += () =>
            {
                AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_FARM_ELECTRICITY,
                    DialogCharacter.Farmer, DialogPosition.Left, CharacterExpression.Cool);
                AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_FARM_FIND_ME,
                    DialogCharacter.Farmer, DialogPosition.Left, CharacterExpression.Idle);
                AddWaitForSecondsAction(1);
            };
        }
    }
}
