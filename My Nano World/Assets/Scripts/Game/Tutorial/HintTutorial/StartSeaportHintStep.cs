﻿using System;
using System.Linq;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Tooltips;
using DG.Tweening;
using I2.Loc;
using NanoLib.Core.Logging;
using NanoLib.Services.InputService;
using NanoReality.Engine.UI.Extensions.QuestsBuildingPanel;
using NanoReality.Engine.UI.Views.LevelUpPanel;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.Game.UI;
using NanoReality.GameLogic.Upgrade;
using UnityEngine;

namespace Game.Tutorial.HintTutorial
{
    [HintStep(HintType.StartSeaport)]
    public sealed class StartSeaportHintStep : HintTutorialStep
    {
        [Inject] public SignalOnNeedToDisableTooltip jSignalOnNeedToDisableTooltip { get; set; }
        
        private MapObjectView _buildingView, _seaportView;

        private const int FindBuilding = 0;
        private const int SeaportBuilding = 1;
        
        protected override void AddTutorialActions()
        {
            int buildingId = ActionData[FindBuilding].BuildingData[0].Id;
            var mapObjectType = ActionData[FindBuilding].BuildingData[0].MapObjectType;
            var businessSectorsType = ActionData[FindBuilding].BusinessSectorsType;

            int seaportId = ActionData[SeaportBuilding].BuildingData[0].Id;
            var seaportObjectType = ActionData[SeaportBuilding].BuildingData[0].MapObjectType;
            var seaportBusinessSectorsType = ActionData[SeaportBuilding].BusinessSectorsType;
            
            _buildingView = jGameManager.FindMapObjectViewOnMapByType(businessSectorsType,
                mapObjectType, buildingId);
            _seaportView = jGameManager.FindMapObjectViewOnMapByType(seaportBusinessSectorsType,
                seaportObjectType, seaportId);

            if (!_buildingView || !_seaportView)
            {
                jDebug.LogError($"Building for upgrade not found. HintStep: {GetType().Name} skipped.");
                return;
            }
            
            if (_buildingView.ModelLevel > 1)
            {
                jDebug.LogError($"Building already upgraded. HintStep: {GetType().Name} skipped.");
                return;
            }

            AddWaitForSecondsAction(2f).OnComplete += () =>
            {
                base.AddTutorialActions();

                if(jUIManager.IsViewVisible(typeof(LevelUpPanelView)))
                {
                    AddWaitForPanelCloseAction(typeof(LevelUpPanelView), false);
                    AddWaitForPopupShowAction(typeof(RewardBoxPopupView), false);
                    AddWaitForPopupCloseAction(typeof(RewardBoxPopupView), false);
                }

                AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_HINT_SEAPORT_START,
                        DialogCharacter.Girl, DialogPosition.Left, CharacterExpression.Idle)
                    .SetCamera(_buildingView.MapObjectModel);

                if (_buildingView.MapObjectModel.IsConnectedToGeneralRoad)
                {
                    AddPressBuildingAttentionAction(_buildingView, true, typeof(UpgradeWithProductsPanelView))
                        .SetCamera(_buildingView.MapObjectModel).SetInputState(InputActionType.Tap)
                        .OnComplete += HighlightFirstUpgradeItem;
                    return;
                }

                AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_HINT_SEAPORT_NO_MATERIALS,
                    DialogCharacter.Girl, DialogPosition.Left, CharacterExpression.Attention);
                AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_HINT_SEAPORT_OPEN,
                    DialogCharacter.Engineer, DialogPosition.Right, CharacterExpression.Attention);
            };
        }

        private void HighlightFirstUpgradeItem()
        {
            var upgradePanel = jUIManager.GetActiveView<UpgradeWithProductsPanelView>();
            if (upgradePanel == null)
            {
                Logging.LogError(LoggingChannel.Debug, "Something wrong.");
                return;
            }
            
            AddWaitForSecondsAction(1.0f);
            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_HINT_SEAPORT_NO_MATERIALS,
                DialogCharacter.Girl, DialogPosition.Left, CharacterExpression.Attention);
            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_HINT_SEAPORT_OPEN,
                DialogCharacter.Engineer, DialogPosition.Right, CharacterExpression.Attention);
            
            DraggableUpgradeItem targetUpgradeItem = upgradePanel.UpgradeItems.First();
            targetUpgradeItem.AllowDrag = false;
            targetUpgradeItem.transform.DOPunchScale(0.3f * Vector3.one, 1f);

            var holdUpgradeItemAction = CreateAction<HoldDragItemTutorialAction>();
            ActionsToDoList.Add(holdUpgradeItemAction);
            holdUpgradeItemAction.Init(targetUpgradeItem);
            holdUpgradeItemAction.OnComplete += OnShowGoToTooltip;
            
            void OnShowGoToTooltip()
            {
                targetUpgradeItem.CancelDrag();
                SpecialItemsTooltip goToTooltip = upgradePanel.ProductGoToTooltip;
                goToTooltip.OverlayHideTrigger.enabled = false;
                AddWaitForSecondsAction(0.5f);

                AddPressButtonAction(goToTooltip.GoToButton).OnComplete += () =>
                {
                    targetUpgradeItem.AllowDrag = true;
                    goToTooltip.OverlayHideTrigger.enabled = true;
                };
            }
        }
    }
}