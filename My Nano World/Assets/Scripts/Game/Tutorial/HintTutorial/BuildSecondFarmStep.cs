﻿using Assets.Scripts.Engine.UI.Views.Popups;
using I2.Loc;
using NanoReality.Engine.UI.Views.LevelUpPanel;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.Game.UI;
using NanoReality.Game.UI.BuildingsShopPanel;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using UnityEngine;

namespace Game.Tutorial.HintTutorial
{
    [HintStep(HintType.BuildSecondFarmHint)]
    public sealed class BuildSecondFarmStep : HintTutorialStep
    {
        private const BuildingsCategories BuildingCategory = BuildingsCategories.Crops;
        private const BusinessSectorsTypes BuildingSector = BusinessSectorsTypes.Farm;
        private const BuildingSubCategories SubCategory = BuildingSubCategories.Common;
        private const int AgroFieldBuildingId = 127;

        private readonly Vector2 _buildingGridPosition = new Vector2(21, 0);
        private readonly Vector2 _buildCameraPosition = new Vector2(21, 2);

        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();

            if (jUIManager.IsViewVisible(typeof(LevelUpPanelView)))
            {
                AddWaitForPanelCloseAction(typeof(LevelUpPanelView), false);
                AddWaitForPopupShowAction(typeof(RewardBoxPopupView), false);
                AddWaitForPopupCloseAction(typeof(RewardBoxPopupView), false);
            }

            if (HasBuildingOnMap(BuildingSector, AgroFieldBuildingId, 0, _buildingGridPosition)) return;

            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_FARM_SECOND_PLOT,
                    DialogCharacter.Farmer, DialogPosition.Left, CharacterExpression.Idle)
                .SetCameraInSector(_buildCameraPosition.ToVector2F(), BuildingSector).OnComplete += () =>
            {
                var dragLimits = new Rect(16, 0, 22, 4);
                jShowWindowSignal.Dispatch(typeof(BuildingsShopPanelView), new BuildingShopData(BusinessSectorsTypes.Farm));
                AddBuildBuildingActions(BuildingSector, AgroFieldBuildingId, BuildingCategory, SubCategory, _buildingGridPosition, dragLimits);
            };
        }
        
        public override bool IsAllowedBuildPosition(Vector2 gridPosition)
        {
            return gridPosition.Equals(_buildingGridPosition);
        }
    }
}