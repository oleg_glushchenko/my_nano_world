using System.Linq;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoLib.Services.InputService;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace Game.Tutorial.HintTutorial
{
    [HintStep(HintType.SkipSecondHarvestWheatHint)]
    public sealed class SkipSecondHarvestTutorialHintStep : HintTutorialStep
    {
        private const BusinessSectorsTypes SectorType = BusinessSectorsTypes.Farm;
        private const MapObjectintTypes BuildingType = MapObjectintTypes.AgriculturalField;
        private bool _isDialogShowed;

        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();

            var agrofields = FindMapObjectsViewOnMapByType(SectorType, BuildingType, 0);
            var field = agrofields.FirstOrDefault(c => ((IAgroFieldBuilding) c.MapObjectModel).GrowingTimeLeft > 0);

            if (field == null) return;

            var action = ActionCropsReadyToHarvest(field);
            ActionsToDoList.Add(action);
        }
        
        private TutorialActionCropsReadyToHarvest ActionCropsReadyToHarvest(MapObjectView agrofield)
        {
            var tutorialActionCropsReadyToHarvest = CreateAction<TutorialActionCropsReadyToHarvest>();
            tutorialActionCropsReadyToHarvest.SetBuildingModel(agrofield.MapObjectModel as IAgroFieldBuilding);
            tutorialActionCropsReadyToHarvest.SetInputState(InputActionType.Tap);
            tutorialActionCropsReadyToHarvest.Init(agrofield.gameObject);
            tutorialActionCropsReadyToHarvest.SetCamera(agrofield.MapObjectModel);
            tutorialActionCropsReadyToHarvest.IsBuildingsNotInteractable = true;
            tutorialActionCropsReadyToHarvest.InteractableBuilding = agrofield;
            return tutorialActionCropsReadyToHarvest;
        }
    }
}