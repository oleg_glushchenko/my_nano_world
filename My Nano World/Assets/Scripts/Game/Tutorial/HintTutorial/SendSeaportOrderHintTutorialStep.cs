﻿using System;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.UI.Views.Popups;
using NanoLib.Services.InputService;
using NanoReality.Engine.UI.Views.NotEnoughtResourcesPanel;
using NanoReality.Game.Services.OrderDesk.SeaportOrderDesk;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using UnityEngine.UI;

namespace Game.Tutorial.HintTutorial
{
    [HintStep(HintType.SendSeaportOrder)]
    public sealed class SendSeaportOrderHintTutorialStep : HintTutorialStep
    {
        private MapObjectView _buildingView, _seaportView;
        private SeaportPanelView _seaportPanelView;
        private SeaportOrderItemView _seaportOrderItem;
        
        private const int FindBuilding = 0;
        private const int SeaportBuilding = 1;


        [Inject] public ISeaportOrderDeskService jSeaportOrderDeskService { get; set; }

        protected SeaportOrderStatusPanelView OrderStatusPanel => _seaportPanelView.OrderStatusPanel;

        protected override void AddTutorialActions()
        {
            int buildingId = ActionData[FindBuilding].BuildingData[0].Id;
            var mapObjectType = ActionData[FindBuilding].BuildingData[0].MapObjectType;
            var businessSectorsType = ActionData[FindBuilding].BusinessSectorsType;
            int buildingLevel = ActionData[0].BuildingData[FindBuilding].Level;

            int seaportId = ActionData[SeaportBuilding].BuildingData[0].Id;
            var seaportObjectType = ActionData[SeaportBuilding].BuildingData[0].MapObjectType;
            var seaportBusinessSectorsType = ActionData[SeaportBuilding].BusinessSectorsType;
            int seaportBuildingLevel = ActionData[SeaportBuilding].BuildingData[0].Level;

            _buildingView = FindMapObjectViewInSector(businessSectorsType,
                mapObjectType, buildingId, buildingLevel);

            _seaportView = FindMapObjectViewInSector(seaportBusinessSectorsType,
                seaportObjectType, seaportId, seaportBuildingLevel);

            if (!_buildingView || !_seaportView)
            {
                jDebug.LogError($"Building for upgrade not found. HintStep: {GetType().Name} skipped.");
                return;
            }

            if (_buildingView.ModelLevel > 1)
            {
                jDebug.LogError($"Building already upgraded. HintStep: {GetType().Name} skipped.");
                return;
            }

            AddOpenMapObjectPanelAction(_seaportView, typeof(SeaportPanelView))
                .SetInputState(InputActionType.Tap).SetCamera(_seaportView.MapObjectModel)
                .AddCompleteListener(() =>
                {
                    _seaportPanelView = jUIManager.GetView<SeaportPanelView>();
                    _seaportOrderItem = _seaportPanelView.OrderItemsList[1];
                    _seaportPanelView.BalancePanelView.SetButtonsAndTogglesEnabled(false);

                    float leftShuffleTime = jSeaportOrderDeskService.GetTimeLeft(2);
                    if (Math.Abs(leftShuffleTime) < float.Epsilon)
                    {
                        leftShuffleTime = 0.5f;
                    }
                    AddWaitForSecondsAction(leftShuffleTime).AddCompleteListener(PurchasePhase);
                });
        }

        private void PurchasePhase()
        {
            ISeaportOrderSlot targetSlot = jSeaportOrderDeskService.GetSlot(2);
            bool secondSlotSelected = OrderStatusPanel.SelectedSlot.ServerPosition == targetSlot.ServerPosition;
            if (!secondSlotSelected)
            {
                _seaportOrderItem.ShuffleOrderButton.interactable = false;
                var selectSeaportOrderButton = _seaportOrderItem.GetComponent<Button>();
                AddPressButtonAction(selectSeaportOrderButton).AddCompleteListener(TryToSendOrder);
                return;
            }
            
            TryToSendOrder();
        }

        private void TryToSendOrder()
        {
            if (!OrderStatusPanel.IsEnoughToSend)
            {
                // Try to buy resources by Hard Currency.
                OptionalPurchasePhase();
                return;
            }
                    
            SendOrder();
        }

        private void SendOrder()
        {
            AddPressButtonAction(OrderStatusPanel.ActionButton);
            
            ISeaportOrderSlot targetSlot = jSeaportOrderDeskService.GetSlot(2);
            var completeOrderAction = CreateActionAndAdd<CompleteSeaportOrderTutorialAction>();
            completeOrderAction.SetTargetOrderId(targetSlot.OrderId.Value);
        }

        private void OptionalPurchasePhase()
        {
            jDebug.LogError("Seaport Hint: Don't have required materials for order. Attempting premium currency route.");

            AddPressButtonAction(OrderStatusPanel.ActionButton);
            Type popupType = typeof(NotEnoughResourcesPopupView);
            AddWaitForPopupShowAction(popupType).OnComplete += () =>
            {
                AddPressButtonAction((jPopupManager.GetActivePopupByType<NotEnoughResourcesPopupView>()).BuyButton);

                popupType = typeof(PurchaseByPremiumPopupView);
                AddWaitForPopupShowAction(popupType).AddCompleteListener(() =>
                    {
                        Button buyResourcesButton = jPopupManager.GetActivePopupByType<PurchaseByPremiumPopupView>().YesButton;
                        AddPressButtonAction(buyResourcesButton).AddCompleteListener(SendOrder);
                    }
                );
            };
        }
    }
}