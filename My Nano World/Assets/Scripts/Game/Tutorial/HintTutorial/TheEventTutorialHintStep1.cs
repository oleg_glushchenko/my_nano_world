using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.UI.Views.Hud;
using Assets.Scripts.Engine.UI.Views.Popups;
using I2.Loc;
using NanoReality.Engine.UI.Views.LevelUpPanel;
using NanoReality.Engine.UI.Views.TheEvent;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.Game.UI;
using NanoReality.GameLogic.TheEvent;
using UnityEngine.UI;

namespace Game.Tutorial.HintTutorial
{
    [HintStep(HintType.TheEventHint1)]
    public sealed class TheEventTutorialHintStep1 : HintTutorialStep
    {
        [Inject] public ITheEventService jTheEventService { get; set; }

        protected override void AddTutorialActions()
        {
            Button eventButton = jUIManager.GetView<HudView>().TheEventButton;
            MapObjectView view = jTheEventService.GetEventBuildingView();
            AddWaitForSecondsAction(2f).OnComplete += () =>
            {
                base.AddTutorialActions();

                if (jUIManager.IsViewVisible(typeof(LevelUpPanelView)))
                {
                    AddWaitForPanelCloseAction(typeof(LevelUpPanelView), false);
                    AddWaitForPopupShowAction(typeof(RewardBoxPopupView), false);
                    AddWaitForPopupCloseAction(typeof(RewardBoxPopupView), false);
                }

                AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_THE_EVENT_Key_1,
                        DialogCharacter.Girl, DialogPosition.Left, CharacterExpression.Attention)
                    .SetCamera(view.MapObjectModel);
                AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_THE_EVENT_Key_2,
                    DialogCharacter.Girl, DialogPosition.Left, CharacterExpression.Idle);
                AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_THE_EVENT_Key_3,
                    DialogCharacter.Girl, DialogPosition.Left, CharacterExpression.Idle);
                AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_THE_EVENT_Key_4,
                    DialogCharacter.Girl, DialogPosition.Left, CharacterExpression.Attention);

                AddPressButtonAction(eventButton);

                AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_THE_EVENT_Key_5,
                    DialogCharacter.Girl, DialogPosition.Left, CharacterExpression.Idle);

                AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_THE_EVENT_Key_6,
                        DialogCharacter.Girl, DialogPosition.Left, CharacterExpression.Attention).OnComplete +=
                    () =>
                    {
                        var eventPanel = jUIManager.GetView<TheEventPanelView>();
                        eventPanel.DailyTab.ScrollRect.enabled = false;
                        Button rewardButton = eventPanel.DailyTab.CommonItems[0].ClaimButton;
                        AddPressButtonAction(rewardButton).OnComplete+= () =>
                        {
                            eventPanel.DailyTab.ScrollRect.enabled = true;
                        };
                    };
            };
        }
    }
}