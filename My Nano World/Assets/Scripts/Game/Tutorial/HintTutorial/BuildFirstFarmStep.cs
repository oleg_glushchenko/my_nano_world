﻿using Assets.Scripts.Engine.UI.Views.Hud;
using Assets.Scripts.Engine.UI.Views.Popups;
using I2.Loc;
using NanoReality.Engine.UI.Views.LevelUpPanel;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.Game.UI;
using NanoReality.Game.UI.BuildingsShopPanel;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using UnityEngine;

namespace Game.Tutorial.HintTutorial
{
    [HintStep(HintType.BuildFirstFarmHint)]
    public sealed class BuildFirstFarmStep : HintTutorialStep
    {
        private const int MoveCameraDataAction = 0;
        private const int BuildDataAction = 1;
        private Vector2 _buildingGridPosition;

        protected override void AddTutorialActions()
        {
            Vector2 cameraPosition = ActionData[MoveCameraDataAction].Positions[0];
            _buildingGridPosition = ActionData[BuildDataAction].Positions[0];
            var buildingSector = ActionData[BuildDataAction].BusinessSectorsType;
            var cameraSector = ActionData[MoveCameraDataAction].BusinessSectorsType;
            var buildingCategory = ActionData[BuildDataAction].BuildingData[0].Category[0];
            var subCategory = ActionData[BuildDataAction].BuildingData[0].SubCategory;
            var buildingId = ActionData[BuildDataAction].BuildingData[0].Id;
            var buildingLevel = ActionData[BuildDataAction].BuildingData[0].Level;
            var dragLimits = ActionData[BuildDataAction].TutorialDragLimit[0];
            
            base.AddTutorialActions();
            
            AddWaitForSecondsAction(2).OnComplete += () =>
            {
                if (jUIManager.IsViewVisible(typeof(LevelUpPanelView)))
                {
                    AddWaitForPanelCloseAction(typeof(LevelUpPanelView), false);
                    AddWaitForPopupShowAction(typeof(RewardBoxPopupView), false);
                    AddWaitForPopupCloseAction(typeof(RewardBoxPopupView), false);
                }

                if (HasBuildingOnMap(buildingSector, buildingId, buildingLevel, _buildingGridPosition)) return;

                AddOnlyDialogAction(
                        ScriptTerms.Tutorial.TUTORIAL_DIALOG_FARM_START,
                        DialogCharacter.Girl, DialogPosition.Right, CharacterExpression.Idle)
                    .SetCameraInSector(cameraPosition.ToVector2F(), cameraSector);
                AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_FARM_BARNIE,
                    DialogCharacter.Farmer, DialogPosition.Left, CharacterExpression.Cool);
                AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_FARM_FIRST_PLOT,
                    DialogCharacter.Farmer, DialogPosition.Left, CharacterExpression.Idle).OnComplete += () =>
                {
                    jShowWindowSignal.Dispatch(typeof(BuildingsShopPanelView), new BuildingShopData(buildingSector));
                    AddBuildBuildingActions(buildingSector, buildingId, buildingCategory, subCategory, _buildingGridPosition, dragLimits)
                        .OnComplete += () => { jConstructConfirmPanelModel.isAcceptButtonEnabled.Value = false; };

                    GetActiveAction<PlaceAndConstructBuildingTutorialAction>().OnActivate += () =>
                    {
                        jUIManager.GetView<HudView>().SetButtonsAndTogglesEnabled(false);
                        jUIManager.GetView<BalancePanelView>().SetButtonsAndTogglesEnabled(false);
                    };
                };
            };
        }

        public override bool IsAllowedBuildPosition(Vector2 gridPosition)
        {
            return gridPosition.Equals(_buildingGridPosition);
        }
    }
}