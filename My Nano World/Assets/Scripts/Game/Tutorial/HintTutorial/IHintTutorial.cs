﻿using System;
using System.Collections.Generic;
using strange.extensions.signal.impl;
using UnityEngine;

namespace NanoReality.Game.Tutorial.HintTutorial
{
    public sealed class HintTriggeredSignal : Signal<HintTrigger> { }

    public interface IHintTutorial
    {
        bool IsHintTutorialActive { get; }
        void StartTutorial();

        void SkipTutorial(Action successCallback = null);

        void UpdateStepsStatus(List<HintStepStatus> statusData);

        bool IsHintCompleted(HintType hintType);
        bool IsBuildingApprovedByHintTutorial(Vector2 position);
    }
}