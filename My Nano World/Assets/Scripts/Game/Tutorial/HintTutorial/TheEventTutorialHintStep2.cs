using System.Linq;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.UI.Views.Tooltips;
using DG.Tweening;
using I2.Loc;
using NanoLib.Core.Logging;
using NanoReality.Engine.UI.Views.TheEvent;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.Game.UI;
using NanoReality.GameLogic.TheEvent;
using NanoReality.GameLogic.Upgrade;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Tutorial.HintTutorial
{
    [HintStep(HintType.TheEventHint2)]
    public class TheEventTutorialHintStep2 : HintTutorialStep
    {
        [Inject] public ITheEventService jTheEventService { get; set; }

        private int _specialEventProductId = 393;

        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();
            MapObjectView view = jTheEventService.GetEventBuildingView();
            jTheEventService.OpenEventPanel();

            TheEventPanelView eventPanel = jUIManager.GetView<TheEventPanelView>();
            Button timeline = eventPanel.TimelineTabButton.Button;
            Button goTo = eventPanel.TimelineTab.StageItems[0].GoToButton;

            AddWaitForSecondsAction(2f);

            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_THE_EVENT_Key_7,
                DialogCharacter.Girl, DialogPosition.Left, CharacterExpression.Cool);

            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_THE_EVENT_Key_8,
                DialogCharacter.Girl, DialogPosition.Left, CharacterExpression.Attention);

            AddPressButtonAction(timeline, true, new VeilParams(PointerMode.TapRight));

            AddWaitForSecondsAction(1.5f);

            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_THE_EVENT_Key_9,
                DialogCharacter.Girl, DialogPosition.Left, CharacterExpression.Attention).OnComplete+= () =>
            {
                eventPanel.TimelineTab.ScrollRect.enabled = false;
            };

            AddPressButtonAction(goTo).OnComplete += () =>
            {
                eventPanel.TimelineTab.ScrollRect.enabled = true;

                view.Interactable = true;
                view.OnMapObjectViewTap();

                AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_THE_EVENT_Key_10,
                    DialogCharacter.Girl, DialogPosition.Left, CharacterExpression.Idle);

                AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_THE_EVENT_Key_11,
                        DialogCharacter.Girl, DialogPosition.Left, CharacterExpression.Attention)
                    .OnComplete += HighlightFirstUpgradeItem;
            };
        }

        private void HighlightFirstUpgradeItem()
        {
            var upgradePanel = jUIManager.GetActiveView<UpgradeWithProductsPanelView>();
            if (upgradePanel == null)
            {
                Logging.LogError(LoggingChannel.Debug, "Something wrong.");
                return;
            }

            AddWaitForSecondsAction(1.0f);

            DraggableUpgradeItem targetUpgradeItem =
                upgradePanel.UpgradeItems.First(
                    item => item.CurrentProductForUpgrade.ProductID == _specialEventProductId);
            targetUpgradeItem.AllowDrag = false;
            targetUpgradeItem.transform.DOPunchScale(0.3f * Vector3.one, 1f);

            var holdUpgradeItemAction = CreateAction<HoldDragItemTutorialAction>();
            ActionsToDoList.Add(holdUpgradeItemAction);
            holdUpgradeItemAction.Init(targetUpgradeItem);
            holdUpgradeItemAction.OnComplete += OnShowGoToTooltip;

            void OnShowGoToTooltip()
            {
                targetUpgradeItem.CancelDrag();
                SpecialItemsTooltip goToTooltip = upgradePanel.ProductGoToTooltip;

                if (!goToTooltip.BuyForLanternsButton.isActiveAndEnabled)
                {
                    FinishStep();
                    return;
                }

                goToTooltip.OverlayHideTrigger.enabled = false;
                AddPressButtonAction(goToTooltip.BuyForLanternsButton).OnComplete += FinishStep;

                void FinishStep()
                {
                    targetUpgradeItem.AllowDrag = true;
                    goToTooltip.OverlayHideTrigger.enabled = true;
                }
            }
        }
    }
}