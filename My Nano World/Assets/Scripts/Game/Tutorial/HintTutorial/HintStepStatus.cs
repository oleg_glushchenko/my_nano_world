﻿using NanoReality.Game.CityStatusEffects;
using Newtonsoft.Json;

namespace NanoReality.Game.Tutorial.HintTutorial
{
    public sealed class HintStepStatus
    {
        [JsonProperty("id")]
        public int Id;

        [JsonProperty("player_id")]
        public StatusEffect PlayerId;

        [JsonProperty("hint_type")]
        public HintType StepType;

        [JsonProperty("status")]
        public bool IsCompleted;
    }
}