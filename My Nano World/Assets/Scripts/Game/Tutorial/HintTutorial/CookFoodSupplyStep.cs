﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.UI.Views.Popups;
using I2.Loc;
using NanoLib.Services.InputService;
using NanoReality.Engine.UI.Views.ConstractConfirmPanel;
using NanoReality.Engine.UI.Views.LevelUpPanel;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.Game.UI;
using NanoReality.Game.UI.BazaarPanel;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.MinePanel;
using NanoReality.UI.Components;

namespace Game.Tutorial.HintTutorial
{
    [HintStep(HintType.CookFoodSupplyHint)]
    public sealed class CookFoodSupplyStep : HintTutorialStep
    {
        [Inject] public BalancePanelModel jBalancePanelModel { get; set; }
        [Inject] public SignalOnNeedToDisableTooltip jSignalOnNeedToDisableTooltip { get; set; }

        private MapObjectView _buildingView;
        private const int FindBuilding = 0;

        private readonly List<Type> _waitingViewList = new List<Type>()
        {
            typeof(SeaportPanelView), typeof(MetroOrderDeskPanelView), typeof(SoukPanelView), typeof(MinePanelView), typeof(FactoryPanelView),
            typeof(ConstractConfirmPanelView)
        };

        protected override void AddTutorialActions()
        {
            int buildingId = ActionData[FindBuilding].BuildingData[0].Id;
            var buildingObjectType = ActionData[FindBuilding].BuildingData[0].MapObjectType;
            var buildingBusinessSectorsType = ActionData[FindBuilding].BusinessSectorsType;
            var buildingLevel = ActionData[FindBuilding].BuildingData[0].Level;
            
            base.AddTutorialActions();
            
            AddWaitForSecondsAction(2).OnComplete += () =>
            {
                jSignalOnNeedToDisableTooltip.Dispatch(true);

                NeedWaitForUi(_waitingViewList, base.AddTutorialActions);

                if (!HasBuildingOnMap(buildingBusinessSectorsType, buildingId, buildingLevel)) return;

                _buildingView = jGameManager.FindMapObjectViewOnMapByType(buildingBusinessSectorsType,
                    buildingObjectType);

                AddOnlyDialogAction(
                    ScriptTerms.Tutorial.TUTORIAL_DIALOG_FOOD_SUPPLY_WELLCOME,
                    DialogCharacter.Farmer, DialogPosition.Left, CharacterExpression.Idle);

                AddOpenMapObjectPanelAction(_buildingView, typeof(FoodSupplyPanelView))
                    .SetInputState(InputActionType.Tap).SetCamera(_buildingView.MapObjectModel)
                    .OnComplete += () =>
                {
                    AddWaitForFoodSupplyLoadedAction().OnComplete += () =>
                    {
                        jBalancePanelModel.FoodSupplyPanelActive.Value = true;

                        AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_FOOD_SUPPLY_COOK,
                            DialogCharacter.Farmer);
                        AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_FOOD_SUPPLY_TIMER,
                            DialogCharacter.Farmer);

                        FoodSupplyPanelView view = jUIManager.GetView<FoodSupplyPanelView>();
                        view.BalancePanelView.SetButtonsAndTogglesEnabled(false);

                        var pair = view.TabToggles.First(t => t.Value is FoodSupplyCookingPanelController);
                        var controller = pair.Value;
                        var foodcontroller = (FoodSupplyCookingPanelController) controller;
                        FoodSupplyCookingSubPanelView
                            foodPanelView = foodcontroller
                                .View; //((FoodSupplyCookingPanelController) view.TabToggles.First( t => t.Value is FoodSupplyCookingPanelController).Value).View;
                        AddPressButtonAction(foodPanelView.OrderItemsList.First().SelectItemButton);
                        AddPressButtonAction(foodPanelView.CookFoodButton);
                    };
                };
            };
        }

        private void NeedWaitForUi(List<Type> types, Action callback)
        {
            if (jUIManager.IsViewVisible(typeof(LevelUpPanelView)))
            {
                AddWaitForPanelCloseAction(typeof(LevelUpPanelView), false);
                AddWaitForPopupShowAction(typeof(RewardBoxPopupView), false);
                AddWaitForPopupCloseAction(typeof(RewardBoxPopupView), false).OnComplete += () => { callback?.Invoke(); };

                return;
            }

            bool needWait = false;

            foreach (var type in types)
            {
                if (jUIManager.IsViewVisible(type))
                {
                    AddWaitForPanelCloseAction(type, false).OnComplete += () =>
                    {
                        callback?.Invoke();
                    };

                    needWait = true;
                }
            }

            if (!needWait)
            {
                callback?.Invoke();
            }
        }
    }
}