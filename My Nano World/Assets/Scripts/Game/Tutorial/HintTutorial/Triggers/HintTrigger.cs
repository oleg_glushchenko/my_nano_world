using Game.Tutorial.HintTutorial;

namespace NanoReality.Game.Tutorial.HintTutorial
{
    public abstract class HintTrigger
    {
        [Inject] public HintTriggeredSignal jHintTriggeredSignal { get; set; }

        public HintType HintType;
        
        public void Activate()
        {
            OnActivate();
            IsCompleteOnActivate();
        }

        public void Deactivate()
        {
            OnDeactivate();
        }

        protected abstract void OnActivate();

        protected abstract void OnDeactivate();

        protected abstract void IsCompleteOnActivate();

        protected void PushHintTriggeredEvent()
        {
            jHintTriggeredSignal.Dispatch(this);
        }
    }
}