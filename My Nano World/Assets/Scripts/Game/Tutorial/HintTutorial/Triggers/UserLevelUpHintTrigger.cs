using Assets.NanoLib.UI.Core.Signals;
using NanoReality.GameLogic.PlayerProfile.Models.Api;

namespace NanoReality.Game.Tutorial.HintTutorial
{
    public sealed class UserLevelUpHintTrigger : HintTrigger
    {
        [Inject] public UserLevelUpSignal jLevelUpSignal { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        
        public int TriggerUserLevel { get; set; }
        
        protected override void OnActivate()
        {
            jLevelUpSignal.AddListener(OnUserLevelUp);
        }

        protected override void OnDeactivate()
        {
            jLevelUpSignal.RemoveListener(OnUserLevelUp);
        }

        private void OnUserLevelUp()
        {
            if (TriggerUserLevel == jPlayerProfile.CurrentLevelData.CurrentLevel)
            {
                PushHintTriggeredEvent();
            }
        }

        protected override void IsCompleteOnActivate()
        {
            if (jPlayerProfile.CurrentLevelData.CurrentLevel >= TriggerUserLevel)
            {
                PushHintTriggeredEvent();
            }
        }
    }
}