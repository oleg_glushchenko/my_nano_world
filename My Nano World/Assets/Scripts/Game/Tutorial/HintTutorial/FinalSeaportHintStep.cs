﻿using System.Linq;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using DG.Tweening;
using I2.Loc;
using NanoLib.Services.InputService;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.Engine.UI.Views.WorldSpaceUI;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Upgrade;
using UnityEngine;

namespace Game.Tutorial.HintTutorial
{
    [HintStep(HintType.SeaportFinal)]
    public sealed class FinalSeaportHintStep : HintTutorialStep
    {
        private MapObjectView _buildingView;

        protected override void AddTutorialActions()
        {
            jUIManager.GetView<BalancePanelView>().SetButtonsAndTogglesEnabled(true);
            
            int buildingId = ActionData[0].BuildingData[0].Id;
            int buildingLevel = ActionData[0].BuildingData[0].Level;
            var mapObjectType = ActionData[0].BuildingData[0].MapObjectType;
            var businessSectorsType = ActionData[0].BusinessSectorsType;

            _buildingView = jGameManager.FindMapObjectViewOnMapByType(businessSectorsType,
                mapObjectType, buildingId);

            if (!_buildingView)
            {
                jDebug.LogError($"Building for upgrade not found. HintStep: {GetType().Name} skipped.");
                return;
            }
            
            if (_buildingView.ModelLevel > 1)
            {
                jDebug.LogError($"Building already upgraded. HintStep: {GetType().Name} skipped.");
                return;
            }

            if (!jUIManager.IsViewVisible(typeof(SeaportPanelView)))
            {
                FinishActions();
                return;
            }
        
            SeaportPanelView seaportPanelView = jUIManager.GetView<SeaportPanelView>();
            seaportPanelView.BalancePanelView.SetButtonsAndTogglesEnabled(false);

            AddWaitForSecondsAction(1f).SetInputState(InputActionType.None).OnComplete += () =>
            {
                AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_HINT_SEAPORT_THANKS,
                        DialogCharacter.Girl, DialogPosition.Left, CharacterExpression.Cool)
                    .SetCamera(_buildingView.MapObjectModel)
                    .OnComplete +=  () =>
                {
                    seaportPanelView.BalancePanelView.SetButtonsAndTogglesEnabled(true);
                    jHideWindowSignal.Dispatch(typeof(SeaportPanelView));
                    FinishActions();
                };
            };
        }

        private void FinishActions()
        {
            if (!_buildingView.MapObjectModel.IsConnectedToGeneralRoad)
            {
                AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_HINT_SEAPORT_FINISH, DialogCharacter.Girl,
                    DialogPosition.Left, CharacterExpression.Cool);
                
                jDebug.LogError($"Building not connected to road. HintStep: {GetType().Name} partially skipped.");
                return;
            }

            BuildingStatesAttentionView attentionView = _buildingView.AttentionComponent.AttentionView;
            HighlightPressButtonTutorialAction action = AddPressButtonAction(attentionView.AttentionButton);
            action.SetCamera(_buildingView.MapObjectModel);
            action.OnActivateCompleted += () => attentionView.SetButtonInteractability(true);
            
            AddPressBuildingAttentionAction(_buildingView, true, typeof(UpgradeWithProductsPanelView))
                .SetCamera(_buildingView.MapObjectModel).SetInputState(InputActionType.Tap)
                .OnComplete += HighlightFirstUpgradeItem;

            AddUpgradeBuildingActions().OnComplete += () => 
                AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_HINT_SEAPORT_FINISH, DialogCharacter.Girl,
                    DialogPosition.Left, CharacterExpression.Cool);
        }
        
        private void HighlightFirstUpgradeItem()
        {
            var upgradePanel = jUIManager.GetActiveView<UpgradeWithProductsPanelView>();
            if (upgradePanel == null)
                return;
            
            DraggableUpgradeItem targetUpgradeItem = upgradePanel.UpgradeItems.FirstOrDefault();
            if (targetUpgradeItem != null)
            {
                targetUpgradeItem.transform.DOPunchScale(0.3f * Vector3.one, 1f);
            }
        }
    }
}