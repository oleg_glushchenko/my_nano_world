﻿using System;
using System.Linq;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.UI.Views.Popups;
using I2.Loc;
using NanoLib.Services.InputService;
using NanoReality.Engine.UI.Views.LevelUpPanel;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace Game.Tutorial.HintTutorial
{
    [HintStep(HintType.SendMetroHint)]
    public sealed class MetroOrderHintStep : HintTutorialStep
    {
        [Inject] public SignalOnNeedToDisableTooltip jSignalOnNeedToDisableTooltip { get; set; }
        
        private MapObjectView _metroView;
        private const int MetroBuilding = 0;

        protected override void AddTutorialActions()
        {
            int metroId = ActionData[MetroBuilding].BuildingData[0].Id;
            var metroObjectType = ActionData[MetroBuilding].BuildingData[0].MapObjectType;
            var metroBusinessSectorsType = ActionData[MetroBuilding].BusinessSectorsType;
            
            AddWaitForSecondsAction(2).OnComplete += () =>
            {
                jSignalOnNeedToDisableTooltip.Dispatch(true);

                base.AddTutorialActions();
                
                if (jUIManager.IsViewVisible(typeof(LevelUpPanelView)))
                {
                    AddWaitForPanelCloseAction(typeof(LevelUpPanelView), false);
                    AddWaitForPopupShowAction(typeof(RewardBoxPopupView), false);
                    AddWaitForPopupCloseAction(typeof(RewardBoxPopupView), false);
                }

                _metroView = jGameManager.FindMapObjectViewOnMapByType(metroBusinessSectorsType,
                    metroObjectType, metroId);

                AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_METRO_START,
                    DialogCharacter.Girl,
                    DialogPosition.Right, CharacterExpression.Cool);

                if (jUIManager.IsViewVisible(typeof(MetroOrderDeskPanelView)))
                {
                    AddWaitForSecondsAction(3f).SetInputState(InputActionType.None);
                    CompleteOrder();
                    return;
                }

                AddOpenMapObjectPanelAction(_metroView, typeof(MetroOrderDeskPanelView))
                    .SetInputState(InputActionType.Tap).SetCamera(_metroView.MapObjectModel)
                    .OnComplete += CompleteOrder;
            };
        }

        private void CompleteOrder()
        {
            MetroOrderDeskPanelView view = jUIManager.GetView<MetroOrderDeskPanelView>();
            MetroTrainView train = view.OrderTrainViews.FirstOrDefault();
            
            view.BalancePanelView.SetButtonsAndTogglesEnabled(false);
            
            var orderPanels = train.OrderSlotViews;
            bool animationStart = false;

            view.CloseButton.interactable = false;

            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_METRO_ORDER, DialogCharacter.Engineer);
            AddWaitForSecondsAction(0.5f);

            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_METRO_REWARD,
                DialogCharacter.Engineer).OnComplete += () =>
            {
                foreach (var order in orderPanels)
                {
                    if (!order.Model.IsEmpty && !order.Model.IsFilled.Value)
                    {
                        AddPressButtonAction(order.SlotButton);
                    }
                }
            };
        }
    }
}