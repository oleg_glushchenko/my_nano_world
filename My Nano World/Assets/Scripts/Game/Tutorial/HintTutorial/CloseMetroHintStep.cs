﻿using I2.Loc;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.OrderDesk;
using NanoReality.GameLogic.Utilites;
using UnityEngine;

namespace Game.Tutorial.HintTutorial
{
    [HintStep(HintType.CloseMetroHint)]
    public sealed class CloseMetroHintStep : HintTutorialStep
    {
        [Inject] public SignalOnNeedToDisableTooltip jSignalOnNeedToDisableTooltip { get; set; }
        [Inject] public IMetroOrderDeskService jMetroOrderDeskService { get; set; }

        private readonly Vector2 _buildCameraPosition = new Vector2(9, 8);
        private const float CameraZoom = 11f;
        private const BusinessSectorsTypes BuildingSector = BusinessSectorsTypes.Town;

        protected override void AddTutorialActions()
        {
            if (jUIManager.IsViewVisible(typeof(MetroOrderDeskPanelView)))
            {
                MetroOrderDeskPanelView view = jUIManager.GetView<MetroOrderDeskPanelView>();
                view.CloseButton.interactable = true;
                view.Hide();
                ShowGoodbyeMessage();
            }
            else
            {
                ShowGoodbyeMessage();
            }
        }

        private void ShowGoodbyeMessage()
        {
            AddWaitForSecondsAction(1);

            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_METRO_REMEMBER,
                    DialogCharacter.Farmer, DialogPosition.Left, CharacterExpression.Idle)
                .SetCameraInSector(_buildCameraPosition.ToVector2F(), BuildingSector, CameraZoom).OnComplete += (() =>
            {
                jUIManager.GetView<BalancePanelView>().SetButtonsAndTogglesEnabled(true);
            });
            
            AddWaitForSecondsAction(1).OnComplete += () =>
            {
                jSignalOnNeedToDisableTooltip.Dispatch(false);
            };
        }
    }
}