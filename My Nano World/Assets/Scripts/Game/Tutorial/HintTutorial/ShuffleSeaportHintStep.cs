﻿using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using I2.Loc;
using NanoLib.Services.InputService;
using NanoReality.Game.Services.OrderDesk.SeaportOrderDesk;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace Game.Tutorial.HintTutorial
{
    [HintStep(HintType.SeaportOrderDialog)]
    public sealed class ShuffleSeaportHintStep : HintTutorialStep
    {
        [Inject] public ISeaportOrderDeskService jSeaportOrderDeskService { get; set; }

        private MapObjectView _buildingView, _seaportView;
        
        private const int FindBuilding = 0;
        private const int SeaportBuilding = 1;

        protected override void AddTutorialActions()
        {
            int buildingId = ActionData[FindBuilding].BuildingData[0].Id;
            var mapObjectType = ActionData[FindBuilding].BuildingData[0].MapObjectType;
            var businessSectorsType = ActionData[FindBuilding].BusinessSectorsType;

            int seaportId = ActionData[SeaportBuilding].BuildingData[0].Id;
            var seaportObjectType = ActionData[SeaportBuilding].BuildingData[0].MapObjectType;
            var seaportBusinessSectorsType = ActionData[SeaportBuilding].BusinessSectorsType;
            
            _buildingView = jGameManager.FindMapObjectViewOnMapByType(businessSectorsType,
                mapObjectType, buildingId);
            _seaportView = jGameManager.FindMapObjectViewOnMapByType(seaportBusinessSectorsType,
                seaportObjectType, seaportId);

            if (!_buildingView || !_seaportView)
            {
                jDebug.LogError($"Building for upgrade not found. HintStep: {GetType().Name} skipped.");
                return;
            }

            if (_buildingView.ModelLevel > 1)
            {
                jDebug.LogError($"Building already upgraded. HintStep: {GetType().Name} skipped.");
                return;
            }

            if (jUIManager.IsViewVisible(typeof(SeaportPanelView)))
            {
                AddWaitForSecondsAction(3f).SetInputState(InputActionType.None);

                ShufflePhase();
                return;
            }

            AddOpenMapObjectPanelAction(_seaportView, typeof(SeaportPanelView))
                .SetInputState(InputActionType.Tap).SetCamera(_seaportView.MapObjectModel)
                .OnComplete += ShufflePhase;
        }

        private void ShufflePhase()
        {
            SeaportPanelView seaportPanelView = jUIManager.GetView<SeaportPanelView>();
            seaportPanelView.BalancePanelView.SetButtonsAndTogglesEnabled(false);
            
            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_HINT_SEAPORT_ORDER, DialogCharacter.Engineer);
            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_HINT_SEAPORT_CRATE, DialogCharacter.Engineer);
        }
    }
}