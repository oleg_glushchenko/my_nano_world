using System.Collections.Generic;
using I2.Loc;
using NanoLib.Services.InputService;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using UnityEngine;

namespace Game.Tutorial.HintTutorial
{
    [HintStep(HintType.ProduceWheatHint)]
    public sealed class ProduceWheatTutorialHintStep : HintTutorialStep
    {
        private const int ProductId = 177;
        private readonly Vector2 _buildCameraPosition = new Vector2(20, 2);
        private const BusinessSectorsTypes BuildingSector = BusinessSectorsTypes.Farm;

        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();

            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_FARM_SOW_PLOTS,
                DialogCharacter.Farmer, DialogPosition.Left, CharacterExpression.Cool).SetCameraInSector(_buildCameraPosition.ToVector2F(), BuildingSector);

            var plantAgrofieldsAction = CreateAction<StartFarmProductionTutorialAction>();

            ActionsToDoList.Add(plantAgrofieldsAction);
            plantAgrofieldsAction.SetInputState(InputActionType.Tap);
            IEnumerable<IAgroFieldBuilding> agrofields =
                FindAllMapObjects<IAgroFieldBuilding>(BusinessSectorsTypes.Farm,
                    MapObjectintTypes.AgriculturalField);

            plantAgrofieldsAction.SetBuildingModel(agrofields, ProductId);

            AddWaitForSecondsAction(1f);
        }
    }
}