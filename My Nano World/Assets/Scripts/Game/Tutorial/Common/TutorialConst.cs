﻿using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.TrafficSystem;
using UnityEngine;

namespace NanoReality.Game.Tutorial
{
    public static class TutorialConst
    {
        public const float DefaultTargetCameraSpeed = 3f;
        public const float DefaultCameraZoom = 9f;
        
        public static readonly Vector2F ParkCameraPos = new Vector2F(6.5f, 2.5f);
        public static readonly Vector2F CasinoCameraPos = new Vector2F(3.5f, 18.5f);

        public static readonly Vector2 ParkPos = new Vector2(5, 1);
        public static readonly Vector2 CasinoPos = new Vector2(1, 16);
        public static readonly Vector2 TownhousePos = new Vector2(9, 1);
        public static readonly Vector2 ToolsFactoryPos = new Vector2(15, 22);
        public static readonly Vector2 AgroFieldPos = new Vector2(26, 10);
        public static readonly Vector2 FlourMillPos = new Vector2(19, 10);
        public static readonly Vector2 SolarPanelFarmPos = new Vector2(17, 7);

        public static readonly Vector2 ParkDimentions = new Vector2(3, 3);
        public static readonly Vector2 CasinoDimentions = new Vector2(5, 5);
        public static readonly Vector2 TownhouseDimentions = new Vector2(4, 4);
        public static readonly Vector2 ToolsFactoryDimentions = new Vector2(4, 4);
        public static readonly Vector2 AgroFieldDimentions = new Vector2(4, 4);
        public static readonly Vector2 FlourMillDimentions = new Vector2(4, 4);
        public static readonly Vector2 SolarPanelFarmDimentions = new Vector2(2, 2);

        public static readonly Id<Product> IronBarID = new Id<Product>(327);
        public static readonly Id<Product> RivetID = new Id<Product>(341);
        public static readonly Id<Product> WhiteFlourID = new Id<Product>(178);


        public const int ParkID = 63;
        public const int CasinoTypeID = 154;
        public const int TownhouseTypeID = 131;
        public const int AgriculturalFieldID = 127;
        public const int ToolsFactoryTypeID = 98;
        public const int FlourMillTypeID = 120;
        public const int SolarPanelFarmTypeID = 147;

        public static readonly Id<IQuest> QuestSecondGoalID = new Id<IQuest>(2);
        public static readonly Id<IQuest> QuestThirdGoalID = new Id<IQuest>(3);
        public static readonly Id<IQuest> QuestCasinoID = new Id<IQuest>(10);
        public static readonly Id<IQuest> QuestTownhouseID = new Id<IQuest>(11);
        public static readonly Id<IQuest> QuestBuildFactoryID = new Id<IQuest>(22);
        
        public static readonly Id<IQuest> QuestBuildOnePlot = new Id<IQuest>(24);
        public static readonly Id<IQuest> QuestBuildBakery = new Id<IQuest>(25);
        public static readonly Id<IQuest> QuestSendOrdersInCityOD = new Id<IQuest>(27);
        public static readonly Id<IQuest> QuestCinema = new Id<IQuest>(26);

        public static readonly List<PathPoint> WarehouseTruckPath = new List<PathPoint>
        {
            new PathPoint(new Vector2Int(19, 9), Directions.RightTop, 0f),
            new PathPoint(new Vector2Int(16, 9), Directions.LeftTop, 1.5f)
        };
        
        public static readonly List<PathPoint> UpgradedResidentialCitizenPath = new List<PathPoint>
        {
            new PathPoint(new Vector2Int(8, 0), Directions.RightTop, 0f),
            new PathPoint(new Vector2Int(8, 9), Directions.RightBottom, 0f)
        };

        public const int UpgradedResidentialCitizensAmount = 5;
    }
}
