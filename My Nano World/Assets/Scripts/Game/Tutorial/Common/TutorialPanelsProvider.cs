﻿using System;
using Engine.UI;
using Object = UnityEngine.Object;

namespace NanoReality.Game.Tutorial
{
    public sealed class TutorialPanelsProvider
    {
        private TutorialCanvas _tutorialCanvas;
        private UIPrefabLinks _uiPrefabLinks;
        
        private TutorialCanvas TutorialCanvas => _tutorialCanvas != null ? _tutorialCanvas : _tutorialCanvas = Object.FindObjectOfType<TutorialCanvas>();

        public void Show(Type panelType, object parameters)
        {
            TutorialCanvas.Show(panelType, parameters);
        }

        public void Hide()
        {
            TutorialCanvas.Hide();
        }
    }
}