﻿using UnityEngine;

namespace NanoReality.Game.Tutorial
{
    public class VeilParams
    {
        public PointerMode PointerMode = PointerMode.Tap;

        /// <summary>
        /// Bottom Left corner offset
        /// </summary>
        public Vector2 OffsetMin;

        /// <summary>
        /// Top Right corner offset
        /// </summary>
        public Vector2 OffsetMax;

        public Color TintColor = new Color();

        public GameObject DragTargetObject;

        public VeilParams() { }

        public VeilParams(PointerMode pointerMode)
        {
            PointerMode = pointerMode;
        }

        public VeilParams(float offsetLeft, float offsetBottom, float offsetRight, float offsetTop, PointerMode mode = PointerMode.Tap, GameObject dragTarget = null)
        {
            OffsetMin = new Vector2(offsetLeft, offsetBottom);
            OffsetMax = new Vector2(offsetRight, offsetTop);
            PointerMode = mode;
            DragTargetObject = dragTarget;
        }
    }

    public enum PointerMode
    {
        Invisible,
        Tap,
        TapLeft,
        TapRight,
        TapBottom,
        Drag,
        LongTap
    }
}