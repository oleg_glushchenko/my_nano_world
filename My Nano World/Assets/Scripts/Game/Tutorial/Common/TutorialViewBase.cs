﻿using System;
using System.Collections.Generic;
using System.Linq;
using NanoLib.Services.InputService;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UtilitesEngine.CustomDebugging.Models.api;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using strange.extensions.injector.api;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Engine.UI.Views.Hud;
using Assets.Scripts.Engine.UI.Views.Popups;
using I2.Loc;
using NanoReality.Engine.BuildingSystem.CityCamera;
using NanoLib.UI.Core;
using NanoReality.Engine.BuildingSystem.GlobalCityMap;
using NanoReality.Engine.UI.Extensions.Agricultural;
using NanoReality.Engine.UI.Views.ConstractConfirmPanel;
using NanoReality.Engine.UI.Views.NotEnoughtResourcesPanel;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.Game.UI;
using NanoReality.Game.UI.BuildingsShopPanel;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.AgriculturalFields;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Production;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.Configs;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.MinePanel;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Upgrade;

namespace NanoReality.Game.Tutorial
{
    public abstract class TutorialViewBase
    {
        #region Inject

        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IInjectionBinder jInjectionBinder { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public IUserQuestData jUserQuestData { get; set; }
        [Inject] public IUIManager jUIManager { get; set; }
        [Inject] public IPopupManager jPopupManager { get; set; }
        [Inject] public TutorialMapObjects jTutorialMapObjects { get; set; }
        [Inject] public IMapObjectsData jMapObjectsData { get; set; }

        [Inject] public IDebug jDebug { get; set; }

        [Inject] public TutorialActionCompleteSignal jTutorialActionCompleteSignal { get; set; }
        [Inject] public TutorialStepCompleteSignal jTutorialStepCompleteSignal { get; set; }

        [Inject] public HideWindowSignal jHideWindowSignal { get; set; }
        [Inject] public ShowWindowSignal jShowWindowSignal { get; set; }
        
        [Inject] public SignalOnShownPanel jUiPanelOpenedSignal { get; set; }
        [Inject] public PopupOpenedSignal jPopupOpenedSignal { get; set; }
        
        [Inject] public ConstructConfirmPanelModel jConstructConfirmPanelModel { get; set; }

        #endregion

        #region Fields

        /// <summary>
        /// Current tutorial action.
        /// </summary>
        private TutorialAction _currentAction;

        /// <summary>
        /// List of all actions for current tutorial step.
        /// </summary>
        protected readonly List<TutorialAction> ActionsToDoList = new List<TutorialAction>();

        private List<TutorialActionData> _actionData;

        public List<TutorialActionData> ActionData
        {
            get => _actionData;
            set => _actionData = value != null && value[0].ActionType != default ? value : GetLocalActionData();
        }

        public InputState InputState { get; set; }

        #endregion

        #region Construct

        [PostConstruct]
        public void PostConstruct()
        {
            OnPostConstruct();
        }

        [Deconstruct]
        public void Deconstruct()
        {
            OnDeconstruct();
        }

        #endregion

        #region Methods

        protected virtual void OnPostConstruct()
        {
            jTutorialActionCompleteSignal.AddListener(OnTutorialActionComplete);
        }

        protected virtual void OnDeconstruct()
        {
            jTutorialActionCompleteSignal.RemoveListener(OnTutorialActionComplete);
        }

        private void OnTutorialActionComplete(TutorialAction completedAction)
        {
            if (completedAction != _currentAction)
                return;
            
            _currentAction?.Deactivate();
            ProcessNextTutorialAction();
        }

        protected void ProcessNextTutorialAction()
        {
            if (IsAllActionsCompleted())
            {
                // Finish this tutorial step if tutorial actions list ended.
                jTutorialStepCompleteSignal.Dispatch(this);
                return;
            }

            HideAllElements();

            _currentAction = GetNextAction();

            ActivateCurrentAction();
        }

        private void ActivateCurrentAction()
        {
            TutorialAction tutorialAction = _currentAction;

            if (tutorialAction.PreActivateIsCompletedCheck())
            {
                tutorialAction.DoCompleteAction();
            }
            else
            {
                if (tutorialAction.IsDialogEnabled)
                {
                    var replicaData = new Assets.Scripts.Engine.UI.Views.Replics.DialogData(
                        tutorialAction.CharacterToShow,
                        tutorialAction.DialogText,
                        tutorialAction.DialogPosition,
                        tutorialAction.CharacterExpression,
                        tutorialAction.IsDialogDarkness,
                        tutorialAction.IsDialogButtonEnabled,
                        tutorialAction.DialogButtonText,
                        tutorialAction.IsDialogButtonRewardType,
                        tutorialAction.ShowAcceptButtons,
                        tutorialAction.AcceptButtonText,
                        tutorialAction.DeclineButtonText,
                        tutorialAction.AcceptResult,
                        tutorialAction.UseMessageBoxPopup);
                    jShowWindowSignal.Dispatch(typeof(DialogPanelView), replicaData);
                }

                tutorialAction.Activate();
            }
        }

        public virtual void Deactivate()
        {
            _currentAction?.Deactivate();
        }

        public TAction GetActiveAction<TAction>() where TAction : TutorialAction
        {
            return (TAction) ActionsToDoList.Find(action => action is TAction);
        }

        protected void HideAllElements()
        {
            jTutorialMapObjects.Hide();
        }

        private bool IsAllActionsCompleted()
        {
            return ActionsToDoList.Count == 0 || ActionsToDoList.All(actionToDo => actionToDo.IsCompleted);
        }

        protected void BaseInit()
        {
            InputState = GameObject.FindObjectOfType<InputLayerChecker>()?.CurrentInputState;

            if (this is StorylineTutorialView)
                return;
            
            var waitCloudsAction = CreateAction<TutorialActionWaitForClouds>();
            waitCloudsAction.Init();

            ActionsToDoList.Add(waitCloudsAction);
        }

        /// <summary>
        /// Get next tutorial action from actions list.
        /// </summary>
        /// <returns>Returns null if actions list ended.</returns>
        private TutorialAction GetNextAction()
        {
            TutorialAction nextAction = null;
            if (_currentAction == null)
            {
                nextAction = ActionsToDoList.FirstOrDefault();
            }

            int nextActionIndex = ActionsToDoList.IndexOf(_currentAction) + 1;
            if (nextActionIndex < ActionsToDoList.Count)
            {
                nextAction = ActionsToDoList[nextActionIndex];
            }

            return nextAction;
        }
        
        protected virtual List<TutorialActionData> GetLocalActionData()
        {
            return null;
        }

        #endregion

        #region Helper Methods

        protected StartIndustryProductionTutorialAction AddStartProductionAction(Id<Product> productId, int targetSlot,
            MapObjectintTypes mapObjectType, BusinessSectorsTypes businessSectorType)
        {
            var productionBuilding = FindMapObject<IProductionBuilding>(businessSectorType, mapObjectType);
            bool isMine = mapObjectType == MapObjectintTypes.Mine;
            var veilParams = new VeilParams
            {
                PointerMode = PointerMode.Drag,
                DragTargetObject = isMine ? jUIManager.GetView<MinePanelView>().gameObject : jUIManager.GetView<FactoryPanelView>().gameObject,
            };

            DraggableProductItem draggableItem = isMine
                ? jUIManager.GetView<MinePanelView>().DraggableProductItems.First(item => item.ProducingItemData.OutcomingProducts == productId)
                : jUIManager.GetView<FactoryPanelView>().DraggableProductItems.First(item => item.ProducingItemData.OutcomingProducts == productId);
            GameObject draggableParent = draggableItem.transform.parent.gameObject;
            
            var actionStartProduce = CreateAction<StartIndustryProductionTutorialAction>();
            actionStartProduce.InitExplicit(null, true, veilParams, null);
            actionStartProduce.SetMembers(productId, targetSlot, productionBuilding);
            actionStartProduce.OnActivate += () =>
            {
                actionStartProduce.SetHighlight(draggableParent, true, veilParams);
                
                if (productionBuilding.CurrentProduceSlots[targetSlot].ItemDataIsProducing == null)
                {
                    if (!draggableItem.IsProductsEnoughtForProduction)
                    {
                        var notEnoughPanelShowedOnce = false;

                        jUiPanelOpenedSignal.AddListener(panel =>
                        {
                            if (actionStartProduce.IsCompleted)
                                return;
                            
                            var notEnoughResourcesPopup = panel as NotEnoughResourcesPopupView;
                            var buyPopupView = panel as PurchaseByPremiumPopupView;

                            if (!notEnoughPanelShowedOnce && notEnoughResourcesPopup != null)
                            {
                                notEnoughPanelShowedOnce = true;
                                actionStartProduce.SwitchHighlight(notEnoughResourcesPopup.BuyButton.gameObject, true);
                            }
                            else if (buyPopupView != null)
                            {
                                actionStartProduce.SwitchHighlight(buyPopupView.YesButton.gameObject, true);
                            }
                        });
                    }
                }
                else
                {
                    actionStartProduce.DoCompleteAction();
                }
            };

            ActionsToDoList.Add(actionStartProduce);

            return actionStartProduce;
        }

        protected TutorialActionMessage AddOnlyDialogAction(string localizationId, DialogCharacter dialogCharacter,
            DialogPosition position = DialogPosition.Left, CharacterExpression characterExpression =
                CharacterExpression.LookAt, bool useDarkness = true)
        {
            var action = CreateAction<TutorialActionMessage>();
            action.InitExplicit(null, false, new VeilParams {PointerMode = PointerMode.Invisible}, null);
            action.SetDialog(new DialogData(localizationId, dialogCharacter, position, characterExpression, useDarkness));
            ActionsToDoList.Add(action);
            return action;
        }

        protected TutorialActionShowPanel AddPressBuildingsShopAction(Button button, Action<bool> buttonAnimation, string localizationId = "",
            DialogCharacter dialogCharacter = DialogCharacter.Girl, DialogPosition position = DialogPosition.Left,
            CharacterExpression characterExpression = CharacterExpression.Idle)
        {
            var action = CreateAction<TutorialActionShowPanel>();
            action.Init(button.gameObject);
            action.TargetPanelType = typeof(BuildingsShopPanelView);
            action.IsBuildingsNotInteractable = true;

            action.OnActivate += () =>
            {
                buttonAnimation.Invoke(true);
                button.enabled = true;
                button.gameObject.SetActive(true);

                jConstructConfirmPanelModel.isCancelButtonEnabled.SetValueAndForceNotify(false);
            };

            action.OnComplete += () =>
            {
                button.enabled = false;
                buttonAnimation.Invoke(false);
            };

            if (string.IsNullOrEmpty(localizationId) == false)
            {
                action.SetDialog(new DialogData(localizationId, dialogCharacter, position,
                    characterExpression));
            }

            ActionsToDoList.Add(action);

            return action;
        }

        protected HighlightBuildingInShopTutorialAction GetChooseBuildingInPanelAction(BusinessSectorsTypes sectorType,
            BuildingsCategories buildingCategory, BuildingSubCategories subCategory, int buildingId, Vector2 mapPosition)
        {
            var action = CreateAction<HighlightBuildingInShopTutorialAction>();
            action.SetBuildingTypeId(buildingId);
            action.SetBuildingCategory(buildingCategory, subCategory);
            action.SectorID = sectorType;
            
            return action;
        }

        private PlaceAndConstructBuildingTutorialAction GetPlaceBuildingAction(BusinessSectorsTypes sectorType, int buildingId, Vector2 mapPosition,
            Vector2 dimensions, Rect? dragLimits)
        {
            var veilParams = new VeilParams
            {
                PointerMode = PointerMode.Drag,
                DragTargetObject = jTutorialMapObjects.GameObjectBuildPlace,
                OffsetMax = new Vector2(0, -100f)
            };
            
            var action = CreateAction<PlaceAndConstructBuildingTutorialAction>();
            action.DragLimits = dragLimits;
            action.Init(() => jGameManager.CurrentSelectedObjectView.gameObject, true, veilParams);
            action.Set(sectorType, buildingId, mapPosition).SetInputState(InputActionType.Drag);
            action.OnActivate += () =>
            {
                if (mapPosition != default)
                {
                    jTutorialMapObjects.ShowBuildPlace(sectorType, mapPosition, dimensions);
                }
            };

            return action;
        }

        private TutorialActionStartBuildBuilding GetStartBuildingConstructionAction(BusinessSectorsTypes sectorType, int buildingId)
        {
            var action = CreateAction<TutorialActionStartBuildBuilding>();
            action.Init(blockOtherElements:false);
            action.MapObjectTypesNeedToBuild.Add(buildingId);
            action.SectorID = sectorType;
            
            return action;
        }

        protected TutorialAction AddBuildBuildingActions(BusinessSectorsTypes sectorType, int buildingId, BuildingsCategories category, BuildingSubCategories subCategory,
            Vector2 mapPosition = default, Rect? dragLimits = null, string characterDialogId = "", DialogCharacter character = DialogCharacter.Girl)
        {
            IMapObject buildingData = jMapObjectsData.GetByBuildingIdAndLevel(buildingId, 0);
            var dimensions = buildingData.Dimensions.ToVector2();

            TutorialAction chooseBuildingAction = GetChooseBuildingInPanelAction(sectorType, category, subCategory, buildingId, mapPosition);
            chooseBuildingAction.IsBuildingsNotInteractable = true;

            TutorialAction placeBuildingAction = GetPlaceBuildingAction(sectorType, buildingId, mapPosition, dimensions, dragLimits);
            if (!string.IsNullOrEmpty(characterDialogId))
            {
                placeBuildingAction.SetDialog(new DialogData(characterDialogId, character, DialogPosition.Right));
                placeBuildingAction.IsLookerCollidersActive = false;
            }

            TutorialAction startBuildAction = GetStartBuildingConstructionAction(sectorType, buildingId);
            startBuildAction.IsLookerCollidersActive = false;

            ActionsToDoList.Add(chooseBuildingAction);
            ActionsToDoList.Add(placeBuildingAction);
            ActionsToDoList.Add(startBuildAction);

            return chooseBuildingAction;
        }

        protected TutorialActionWaitForSecond AddWaitForSecondsAction(float seconds)
        {
            var action = CreateAction<TutorialActionWaitForSecond>();
            action.Init();
            action.IsBuildingsNotInteractable = true;

            action.Seconds = seconds;
            ActionsToDoList.Add(action);
            return action;
        }

        protected HighlightPressButtonTutorialAction AddPressButtonAction(Button button,
            bool needDisableBuildingsColliders = true, VeilParams veilParams = null)
        {
            var pressButtonAction = CreateAction<HighlightPressButtonTutorialAction>();
            pressButtonAction.Init(button.gameObject, true, veilParams);
            pressButtonAction.IsBuildingsNotInteractable = needDisableBuildingsColliders;

            pressButtonAction.OnActivate += () => button.enabled = true;
            ActionsToDoList.Add(pressButtonAction);
            return pressButtonAction;
        }

        protected HighlightToggleTutorialAction AddPressToggleAction(Toggle toggle,
            bool needDisableBuildingsColliders = true, VeilParams veilParams = null)
        {
            var pressToggleAction = CreateAction<HighlightToggleTutorialAction>();
            pressToggleAction.Init(toggle.gameObject, true, veilParams);
            pressToggleAction.IsBuildingsNotInteractable = needDisableBuildingsColliders;

            pressToggleAction.OnActivate += () => toggle.enabled = true;
            ActionsToDoList.Add(pressToggleAction);
            return pressToggleAction;
        }

        /// <summary>
        /// Action, which forces used to drop all required products for upgrade.
        /// Due to structure, has to go last, but can be extended through OnComplete.
        /// </summary>
        protected TutorialAction AddUpgradeBuildingActions()
        {
            // Wait for complete view initialization before calling actions
            TutorialAction waitAction = AddWaitForSecondsAction(0.2f);
            waitAction.OnComplete += () =>
            {
                var upgradePanel = jUIManager.GetView<UpgradeWithProductsPanelView>();
                var veilParams = new VeilParams()
                {
                    PointerMode = PointerMode.Drag,
                    DragTargetObject = upgradePanel.DragReceiver.gameObject
                };

                // Add actions for each empty and not loaded slot
                foreach (DraggableUpgradeItem upgradeItem in upgradePanel.UpgradeItems)
                {
                    if (upgradeItem.CurrentStatus == DraggableUpgradeItemStatus.Empty ||
                        upgradeItem.CurrentStatus == DraggableUpgradeItemStatus.Loaded)
                        continue;

                    var actionFillUpgradeProduct = CreateAction<TutorialActionFillUpgradeProduct>();

                    actionFillUpgradeProduct.Init(upgradeItem.transform.parent.gameObject, true, veilParams);
                    actionFillUpgradeProduct.IsBuildingsNotInteractable = false;

                    // If somehow we don't have required products (error in game balance for example)
                    // - force user to buy them for premium
                    if (upgradeItem.CurrentStatus == DraggableUpgradeItemStatus.ProductsNotEnought)
                    {
                        actionFillUpgradeProduct.OnActivate += () =>
                        {
                            void OnPopupOpened(BasePopupView panel)
                            {
                                var notEnoughResourcesPanel = panel as NotEnoughResourcesPopupView;
                                var buyPopupView = panel as PurchaseByPremiumPopupView;

                                if (notEnoughResourcesPanel != null)
                                {
                                    actionFillUpgradeProduct.SwitchHighlight(notEnoughResourcesPanel.BuyButton.gameObject, true);
                                }
                                else if (buyPopupView != null)
                                {
                                    actionFillUpgradeProduct.SwitchHighlight(buyPopupView.YesButton.gameObject, true);
                                    jPopupOpenedSignal.RemoveListener(OnPopupOpened);
                                }
                            }

                            jPopupOpenedSignal.AddListener(OnPopupOpened);
                        };
                    }

                    ActionsToDoList.Add(actionFillUpgradeProduct);
                }

                AddWaitForSecondsAction(3.0f);
            };

            return waitAction;

            // IMPORTANT!!! No actions should be added directly after this one,
            // because they will be called before that one finished, which blocks the game.
            // That can be avoided by using OnComplete as way to extend the flow.
        }

        protected TutorialActionShowPanel AddShowBuildingPanelAction(Type panelType, BusinessSectorsTypes sectorId,
            Func<GameObject> findFunc, Vector2 offsetMax, Vector2 offsetMin, PointerMode pointerMode = PointerMode.Tap,
            bool isForAttention = false)
        {
            MapObjectView mapObjectView = isForAttention ? null : findFunc().GetComponent<MapObjectView>();
            var vailParams = new VeilParams
            {
                OffsetMax = offsetMax,
                OffsetMin = offsetMin,
                TintColor = Color.clear,
                PointerMode = pointerMode
            };

            var globalMapObjectView = mapObjectView as AGlobalCityBuildingView;
            if (globalMapObjectView != null && !globalMapObjectView.HasGraphics)
            {
                vailParams.OffsetMax = Vector2.one * 100f;
                vailParams.OffsetMin = Vector2.one * 100f;
            }

            var actionOpenPanel = CreateAction<TutorialActionShowPanel>();
            actionOpenPanel.TargetPanelType = panelType;
            actionOpenPanel.Init(findFunc, true, vailParams);
            actionOpenPanel.IsBuildingsNotInteractable = true;
            actionOpenPanel.SectorID = sectorId;
            actionOpenPanel.MapObjectView = mapObjectView;
            actionOpenPanel.InteractableBuilding = mapObjectView;

            if (mapObjectView != null)
            {
                actionOpenPanel.SetCamera(mapObjectView.MapObjectModel);
            }

            ActionsToDoList.Add(actionOpenPanel);

            return actionOpenPanel;
        }

        protected WaitFinishConstructionTutorialAction AddWaitForConstructionFinishedAction(BusinessSectorsTypes sectorType, int buildingTypeId,
            Vector2 position = default)
        {
            var action = CreateAction<WaitFinishConstructionTutorialAction>();
            action.InitConstructionData(sectorType, buildingTypeId, position);
            action.Init();
            ActionsToDoList.Add(action);
            return action;
        }

        protected T FindMapObject<T>(BusinessSectorsTypes sectorType, MapObjectintTypes mapObjectType)
        {
            return (T)jGameManager.GetMap(sectorType.GetBusinessSectorId()).FindMapObject(mapObjectType);
        }

        protected IEnumerable<TMapObject> FindAllMapObjects<TMapObject>(BusinessSectorsTypes sectorType, MapObjectintTypes mapObjectType)
            where TMapObject : IMapObject
        {
            return jGameManager.GetMap(sectorType.GetBusinessSectorId()).FindAllMapObjects(mapObjectType).Cast<TMapObject>();
        }

        protected MapObjectView FindMapObjectView(BusinessSectorsTypes sectorType, MapObjectintTypes mapObjectType)
        {
            return jGameManager.GetMapView(sectorType.GetBusinessSectorId()).GetMapObjectViewByType(mapObjectType);
        }
        
        protected MapObjectView FindMapObjectViewInSector(BusinessSectorsTypes sectorId,
            MapObjectintTypes typeObjectToFind, int mapObjectId, int level = -9999)
        {
            List<IMapObject> list = jGameManager.FindAllMapObjectsOnMapByType(sectorId, typeObjectToFind);

            if (list == null || list.Count <= 0)
            {
                return null;
            }

            IMapObject result = null;

            foreach (IMapObject mapObject in list)
            {
                if (mapObject.ObjectTypeID != mapObjectId || mapObject.Level != level) continue;
                
                result = mapObject;
                break;
            }

            // Если модель - null, значит на карте здания нету, но оно является выбраным, получаем его
            return jGameManager.GetMapObjectView(result);
        }

        //TODO: заменить реутрн на FindMapObjectsViewOnMapByType.first если не нарушена логика
        protected MapObjectView FindMapObjectViewOnMapByType(BusinessSectorsTypes sectorId,
            MapObjectintTypes typeObjectToFind, int level = -9999, int xPos = -1, int yPos = -1)
        {
            List<IMapObject> list = jGameManager.FindAllMapObjectsOnMapByType(sectorId, typeObjectToFind);

            if (list == null || list.Count <= 0)
            {
                return null;
            }

            IMapObject result = null;

            foreach (IMapObject mapObject in list)
            {
                if (level < 0)
                {
                    result = mapObject;
                    break;
                }

                if (level < 0 || mapObject.Level != level) continue;
                    
                if (xPos >= 0 && yPos >= 0)
                {
                    if (xPos != mapObject.MapPosition.intX || yPos != mapObject.MapPosition.intY) continue;
                            
                    result = mapObject;
                    break;
                }
                        
                result = mapObject;
                break;
            }

            // Если модель - null, значит на карте здания нету, но оно является выбраным, получаем его
            return result == null ? jGameManager.CurrentSelectedObjectView : jGameManager.GetMapObjectView(result);
        }
        
        protected List<MapObjectView> FindMapObjectsViewOnMapByType(BusinessSectorsTypes sectorId,
            MapObjectintTypes typeObjectToFind, int level = -9999, int xPos = -1, int yPos = -1)
        {
            List<IMapObject> list = jGameManager.FindAllMapObjectsOnMapByType(sectorId, typeObjectToFind);

            if (list == null || list.Count <= 0)
            {
                return null;
            }

            List<MapObjectView> filteredList = new List<MapObjectView>();

            foreach (IMapObject mapObject in list)
            {
                if (level < 0)
                {
                    filteredList.Add(jGameManager.GetMapObjectView(mapObject));
                    continue;
                }

                if (level < 0 || mapObject.Level != level) continue;

                if (xPos >= 0 && yPos >= 0)
                {
                    if (xPos != mapObject.MapPosition.intX || yPos != mapObject.MapPosition.intY) continue;

                    filteredList.Add(jGameManager.GetMapObjectView(mapObject));
                    continue;
                }

                filteredList.Add(jGameManager.GetMapObjectView(mapObject));
            }

            if (filteredList.Count == 0)
            {
                var view = jGameManager.CurrentSelectedObjectView;
                filteredList.Add(view);
            }

            return filteredList;
        }
        
        protected bool HasBuildingOnMap(BusinessSectorsTypes sectorType, Id_IMapObjectType objectTypeId, int level, Vector2 position = default)
        {
            List<IMapObject> list = jGameManager.FindAllMapObjectsOnMapByType(sectorType, objectTypeId);

            if (list == null)
            {
                return false;
            }

            bool isPositionDefault = position == default;

            foreach (IMapObject mapObject in list)
            {
                bool correctPosition = isPositionDefault || mapObject.MapPosition.ToVector2() == position;
                if (correctPosition && mapObject.Level == level)
                {
                    return true;
                }
            }

            return false;
        }

        protected T CreateAction<T>() where T : TutorialAction
        {
            var actionInstance = jInjectionBinder.GetInstance<T>();
            return actionInstance;
        }
        
        protected T CreateActionAndAdd<T>() where T : TutorialAction
        {
            var actionInstance = jInjectionBinder.GetInstance<T>();
            ActionsToDoList.Add(actionInstance);
            return actionInstance;
        }

        #endregion

        protected TutorialActionShowPanel AddWaitForPanelShowAction(Type panelType, bool blockOtherElements = true)
        {
            var action = CreateAction<TutorialActionShowPanel>();
            action.Init(blockOtherElements:blockOtherElements);
            action.TargetPanelType = panelType;
            ActionsToDoList.Add(action);
            return action;
        }

        protected TutorialActionWaitForPopupShow AddWaitForPopupShowAction(Type popupType, bool blockOtherElements = true)
        {
            var action = CreateAction<TutorialActionWaitForPopupShow>();
            action.Init(blockOtherElements:blockOtherElements);
            action.TargetPopupType = popupType;
            ActionsToDoList.Add(action);
            return action;
        }
        
        protected TutorialActionWaitForPanelClose AddWaitForPanelCloseAction(Type panelType, bool blockOtherElements = true)
        {
            var action = CreateAction<TutorialActionWaitForPanelClose>();
            action.Init(blockOtherElements:blockOtherElements);
            action.TargetPanelType = panelType;
            ActionsToDoList.Add(action);
            return action;
        }    
        
        protected TutorialActionWaitForFoodSupplyGifts AddWaitForFoodSupplyGifts(bool blockOtherElements = true)
        {
            var action = CreateAction<TutorialActionWaitForFoodSupplyGifts>();
            action.Init(blockOtherElements:blockOtherElements);
            ActionsToDoList.Add(action);
            return action;
        }
        
        protected TutorialActionWaitForFoodSupplyLoaded AddWaitForFoodSupplyLoadedAction(bool blockOtherElements = true)
        {
            var action = CreateAction<TutorialActionWaitForFoodSupplyLoaded>();
            action.Init(blockOtherElements:blockOtherElements);
            ActionsToDoList.Add(action);
            return action;
        }

        protected TutorialActionWaitForPopupClose AddWaitForPopupCloseAction(Type popupType, bool blockOtherElements = true)
        {
            var action = CreateAction<TutorialActionWaitForPopupClose>();
            action.Init(blockOtherElements:blockOtherElements);
            action.TargetPopupType = popupType;
            ActionsToDoList.Add(action);
            return action;
        }

        protected TutorialActionShowPanel AddOpenMapObjectPanelAction(MapObjectView mapObjectView, Type panelToOpen, VeilParams veilParams = null,
            bool blockUI = true)
        {
            var action = CreateAction<TutorialActionShowPanel>();
            action.Init(mapObjectView.gameObject, veilParams: veilParams);

            action.TargetPanelType = panelToOpen;
            action.IsBuildingsNotInteractable = true;
            action.InteractableBuilding = mapObjectView;
            action.MapObjectView = mapObjectView;

            action.IsLookerCollidersActive = blockUI;
            ActionsToDoList.Add(action);
            return action;
        }

        //TODO: remove
        protected void AddHudQuestButtonAction(Id<IQuest> questId, BusinessSectorsTypes sector)
        {
            
        }

        protected TutorialActionBuildRoad AddBuildRoadAction(List<Vector2> roadPositions, BusinessSectorsTypes sectorId)
        {
            TutorialMapObjects tutorialMapObjects = jTutorialMapObjects;
            var action = CreateAction<TutorialActionBuildRoad>();
            var veilParams = new VeilParams
            {
                PointerMode = PointerMode.Drag,
                DragTargetObject = tutorialMapObjects.GameObjectRoadEnd
            };
            action.Init(tutorialMapObjects.GameObjectRoadStart, true, veilParams);
            action.SetInputState(InputActionType.Drag);
            action.IsLookerCollidersActive = false;
            action.IsBuildingsNotInteractable = true;
            action.RequiredPositions = roadPositions;
            action.SectorID = sectorId;
            action.OnActivate += () =>
            {
                jUIManager.GetView<HudView>().HudRoadPanel.ConstuctRoadButton.enabled = false;

                tutorialMapObjects.ShowRoadPlace(sectorId, roadPositions.First(), roadPositions.Last());
            };

            action.OnComplete += HideAllElements;

            ActionsToDoList.Add(action);

            return action;
        }
        
        protected TutorialActionBuildingAttention AddPressBuildingAttentionAction(MapObjectView targetBuilding, bool onlyButton,
            Type panelToOpen, VeilParams veilParams = null)
        {
            var action = CreateAction<TutorialActionBuildingAttention>();
            action.Init(targetBuilding.gameObject, true, veilParams);
            action.TargetBuilding = targetBuilding;
            action.OnlyButton = onlyButton;
            action.TargetPanelType = panelToOpen;
            
            action.IsBuildingsNotInteractable = true;
            action.IsLookerCollidersActive = true;
            action.IsBuildingsNotInteractable = true;
            action.InteractableBuilding = onlyButton ? null : targetBuilding;

            ActionsToDoList.Add(action);
            return action;
        }
        
        protected TutorialActionWaitForLevelUp AddWaitForLevelUpAction(int minLevel)
        {
            var action = CreateAction<TutorialActionWaitForLevelUp>();
            action.MinExpectedLevel = minLevel;
            action.Init();
            ActionsToDoList.Add(action);
            return action;
        }
    }

    public class DialogData
    {
        public readonly string DialogText;
        public readonly DialogCharacter CharacterToShow;
        public readonly DialogPosition DialogPosition;
        public readonly CharacterExpression CharacterExpression;
        public readonly bool UseDarkness;
        public readonly bool UseMessageBoxPopup;

        public readonly bool ShowAcceptButtons;
        public readonly string AcceptButtonText;
        public readonly string DeclineButtonText;

        public DialogData(string dialogText, DialogCharacter characterToShow, DialogPosition dialogPosition =
            DialogPosition.Left, CharacterExpression characterExpression = CharacterExpression.LookAt, bool useDarkness
            = false, bool showAcceptButtons = false, string acceptButtonText = "", string declineButtonText = "", bool useMessageBoxPopup = false)
        {
            DialogText = dialogText;
            CharacterToShow = characterToShow;
            DialogPosition = dialogPosition;
            CharacterExpression = characterExpression;
            UseDarkness = useDarkness;
            ShowAcceptButtons = showAcceptButtons;
            AcceptButtonText = acceptButtonText;
            DeclineButtonText = declineButtonText;
            UseMessageBoxPopup = useMessageBoxPopup;
        }
    }
}