﻿using System;
using NanoLib.Services.InputService;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UtilitesEngine;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.BuildingSystem.CityMap;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Extensions;
using I2.Loc;
using NanoLib.Core.Logging;
using NanoLib.UI.Core;
using NanoReality.Engine.BuildingSystem.CityCamera;
using NanoReality.Engine.UI.Views.ConstractConfirmPanel;
using NanoReality.Game.Attentions;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.Settings.BuildSettings.Models.Api;
using UnityEngine;
using Object = UnityEngine.Object;

namespace NanoReality.Game.Tutorial
{
    public delegate void OnActivate();
    public delegate void OnComplete();
    public delegate void OnActivateCompleted();

    public abstract class TutorialAction
    {
        #region Fields

        public event OnActivate OnActivate = null;
        public event OnComplete OnComplete = null;
        public event OnActivateCompleted OnActivateCompleted = null;
        
        public bool IsLookerCollidersActive = true;
        
        protected GameObject ObjectToHighlight;
        protected Func<GameObject> GetObjectToHighlight;
        protected Func<bool> ParentFunc;
        protected InputState InputState;
        
        private bool _blockOtherElements;
        private ITimer _callFuncTimer = null;
        
        public bool IsCompleted { get; private set; }
        public MapObjectView MapObjectView { get; set; }
        public BusinessSectorsTypes SectorID { get; set; }
        
        public bool IsBuildingsNotInteractable { get; set; }
        public MapObjectView InteractableBuilding { get; set; }
        public bool IsAttentionsInteractable { get; protected set; }
        
        public VeilParams VeilParams { get; set; }
        public bool IsDialogEnabled { get; private set; }
        public bool IsDialogDarkness { get; private set; }
        public DialogCharacter CharacterToShow { get; private set; }
        public DialogPosition DialogPosition { get; private set; }
        public CharacterExpression CharacterExpression { get; private set; }
        public string DialogText { get; private set; }
        
        public bool ShowAcceptButtons { get; private set; }
        public string AcceptButtonText { get; private set; }
        public string DeclineButtonText { get; private set; }
        public Action<bool> AcceptResult { get; private set; }
        
        public bool UseMessageBoxPopup { get; private set; }


        public bool IsDialogButtonEnabled { get; protected set; }
        public bool IsDialogButtonRewardType { get; protected set; }
        public string DialogButtonText { get; protected set; }

        public bool IsCameraSet { get; set; }
        public Vector2F TargetCameraPos { get; private set; }
        public float TargetCameraZoom { get; private set; }
        public float TargetCameraSpeed { get; private set; } = -1;

        #endregion

        #region Injections
        
        [Inject] public IGameCamera jGameCamera { get; set; }
        [Inject] public IGameCameraModel jGameCameraModel { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public IUIManager jUIManager { get; set; }
        [Inject] public IPopupManager jPopupManager { get; set; }
        [Inject] public ITimerManager jTimerManager { get; set; }
        [Inject] public ICityMapSettings jCityMapSettings { get; set; }
        [Inject] public TutorialMapObjects jTutorialMapObjects { get; set; }
        [Inject] public TutorialActionCompleteSignal jTutorialActionComleteSignal { get; set; }
        [Inject] public InteractBuildingAttentionsSignal jInteractBuildingAttentionsSignal { get; set; }
        [Inject] public SignalOnReleaseTouch jSignalOnReleaseTouch { get; set; }
        [Inject] public HideWindowSignal jHideWindowSignal { get;set; }
        
        [Inject] public ConstructConfirmPanelModel jConstructConfirmPanelModel { get; set; }
        [Inject] public ShowWindowSignal jShowWindowSignal { get; set; }
        [Inject] public TutorialPanelsProvider jTutorialPanelProvider { get; set; }

        #endregion

        #region TutorialAction members

        public virtual bool PreActivateIsCompletedCheck()
        {
            return ParentFunc != null && ParentFunc();
        }

        public virtual void Activate()
        {
            Logging.Log(LoggingChannel.Tutorial,$"TutorialAction is started <b>[{ToString()}]</b>");

            if (IsCompleted)
                return;

            if (OnActivate != null)
            {
                OnActivate();
                
                if (IsCompleted)
                    return;
            }

            if (_blockOtherElements && ObjectToHighlight == null && GetObjectToHighlight != null)
            {
                TryToGetObject();
            }

            if (IsCameraSet)
            {
                MoveCameraToTarget();
            }

            SetLookController();
            ApplyInputState();

            if (IsBuildingsNotInteractable)
            {
                SetAllBuildingsInteractable(false);
                if (InteractableBuilding != null)
                    InteractableBuilding.Interactable = true;
            }

            jInteractBuildingAttentionsSignal.Dispatch(IsAttentionsInteractable);

            OnActivateCompleted?.Invoke();
        }

        public virtual void Deactivate()
        {
            SetAllBuildingsInteractable(true);
            HideLookers();
            ObjectToHighlight = null;
            GetObjectToHighlight = null;
            OnActivate = null;
            OnActivateCompleted = null;
            OnComplete = null;
            jConstructConfirmPanelModel.isCancelButtonEnabled.Value = true;
        }

        protected virtual void OnFoundObjectToHighlight(GameObject objectToHighlight)
        {
            
        }

        public virtual void DoCompleteAction(bool isCancel = false)
        {
            if (IsCompleted)
            {
                Logging.LogError(LoggingChannel.Tutorial,$"TutorialAction {GetType().Name} already complete. Can't complete it twice.");
                return;
            }

            Logging.Log(LoggingChannel.Tutorial,$"TutorialAction: <b>{GetType().Name}</b> is completed");

            IsCompleted = true;
            OnComplete?.Invoke();

            if (!isCancel)
            {
                jTutorialActionComleteSignal.Dispatch(this);
            }
        }

        protected void ApplyInputState()
        {
            if (InputState == null)
            {
                InputState = new InputState() { CurrentState = InputActionType.None };
            }
            
            var inputLayerChecker = Object.FindObjectOfType<InputLayerChecker>();
            inputLayerChecker.CurrentInputState.CurrentState = InputState.CurrentState;
        }

        #endregion

        #region Methods

        public void InitExplicit(GameObject objectToHighlight, bool blockOtherElements, VeilParams veilParams, Func<GameObject> getObjectToHighlight)
        {
            SetHighlight(objectToHighlight, blockOtherElements, veilParams, getObjectToHighlight);

            IsCompleted = false;
            IsCameraSet = false;

            IsDialogEnabled = false;
            DialogText = "";
        }

        public void Init(Func<GameObject> getObjectToHighlight, bool blockOtherElements = true, VeilParams veilParams = null)
        {
            InitExplicit(null, blockOtherElements, veilParams, getObjectToHighlight);
        }

        public void Init(GameObject objectToHighlight = null, bool blockOtherElements = true, VeilParams veilParams = null)
        {
            InitExplicit(objectToHighlight, blockOtherElements, veilParams, null);
        }

        /// <summary>
        /// Установка лукера на объекте, если таковой имеется. Если же объекта для фокусировки нет - лукер скроется
        /// </summary>
        private void SetLookController()
        {
            if (!_blockOtherElements)
            {
                HideLookers();
                return;
            }

            Type targetPanelType = null;
            if (ObjectToHighlight == null)
            {
                if (GetObjectToHighlight != null)
                {
                    HideLookers();
                    return;
                }
        
                targetPanelType = typeof(TutorialLookAtControllerOverlay);
            }
            else
            {
                var mapObjectView = ObjectToHighlight.GetComponent<MapObjectView>();

                if (mapObjectView != null)
                {
                    targetPanelType = typeof(TutorialLookAtMapObject);
                }
                else
                {
                    var canvas = ObjectToHighlight.GetComponentInParent<Canvas>();

                    if (canvas != null && canvas.renderMode == RenderMode.ScreenSpaceOverlay)
                    {
                        targetPanelType = typeof(TutorialLookAtControllerOverlay);
                    }
                    else
                    {
                        targetPanelType = typeof(TutorialLookAtControllerWorldSpace);
                    }
                }
            }

            jTutorialPanelProvider.Hide();
            jTutorialPanelProvider.Show(targetPanelType, (ObjectToHighlight, VeilParams, IsLookerCollidersActive));
        }
        
        protected void HideLookers()
        {
            jTutorialPanelProvider.Hide();
        }

        /// <summary>
        /// Поиск объекта для подсветки. Если подтянуть объект не удается - метод рекурсивно запустится вновь, и будет
        /// пытаться получить объект каждых 0.5 секунд до тех пор, пока не добъется своего.
        /// </summary>
        private void TryToGetObject()
        {
            if (_callFuncTimer != null)
            {
                _callFuncTimer.CancelTimer();
                _callFuncTimer = null;
            }

            var objectToHighlight = GetObjectToHighlight();
            if (objectToHighlight == null)
            {
                Logging.Log(LoggingChannel.Tutorial, "Try to get object to focus was failed; Next try after 0.5 seconds");

                _callFuncTimer = jTimerManager.StartRealTimer(0.5f, TryToGetObject,null, 0.5f);
            }
            else
            {
                ObjectToHighlight = objectToHighlight;
                
                SetLookController();
                
                OnFoundObjectToHighlight(objectToHighlight);
            }
        }

        #endregion

        protected void MoveCameraToTarget()
        {
            jGameCamera.ZoomTo(TargetCameraPos, CameraSwipeMode.Fly, TargetCameraZoom, TargetCameraSpeed, true);
        }

        protected void SetAllBuildingsInteractable(bool isInteractable)
        {
            jGameManager.CityMapViews.ForEach(x =>
            {
                x.Value.Objects.ForEach(y => y.Interactable = isInteractable);
            });

            jGameManager.GlobalCityMapView.Objects.ForEach(x => x.Interactable = isInteractable);
        }

        #region Setters

        /// <summary>
        /// При активации действие будет считаться выполненным, если в момент
        /// Activate() будет выполенным действие, переданное параметром в
        /// <see cref="funcToDepend"/>
        /// </summary>
        /// <param name="funcToDepend">действие, от выполненности которого будет
        /// зависить выполненность этого действие</param>
        public void SetDependencyWithAction(Func<bool> funcToDepend)
        {
            ParentFunc = funcToDepend;
        }
        
        public TutorialAction SetCamera(Vector2 newCameraPos, float cameraZoom, float speed = -1f)
        {
            TargetCameraPos = newCameraPos.ToVector2F();
            TargetCameraZoom = cameraZoom;
            TargetCameraSpeed = speed < 0f ? TutorialConst.DefaultTargetCameraSpeed : speed;
            IsCameraSet = true;
            
            return this;
        }
        
        public TutorialAction SetCamera(Vector2F newCameraPos)
        {
            TargetCameraPos = newCameraPos;
            TargetCameraZoom = TutorialConst.DefaultCameraZoom;
            TargetCameraSpeed = TutorialConst.DefaultTargetCameraSpeed;
            IsCameraSet = true;

            return this;
        }

        public TutorialAction SetCameraInSector(Vector2F newCameraPos, BusinessSectorsTypes sectorType, float cameraZoom = 0, float speed = float.Epsilon)
        {
            switch (sectorType)
            {
                case BusinessSectorsTypes.Town:
                case BusinessSectorsTypes.HeavyIndustry:
                case BusinessSectorsTypes.Farm:
                    CityMapView mapView = jGameManager.GetMapView(sectorType.GetBusinessSectorId());
                    if (mapView != null)
                    {
                        var mapPosition = mapView.transform.position;
                        var offset = new Vector2F(mapPosition.x, mapPosition.z);
                        TargetCameraPos = jCityMapSettings.GridCellScale * newCameraPos + offset;
                    }
                    else
                    {
                        TargetCameraPos = newCameraPos;
                    }
                    break;
                default:
                    TargetCameraPos = newCameraPos;
                    break;
            }
            TargetCameraZoom = cameraZoom > float.Epsilon ? cameraZoom : TutorialConst.DefaultCameraZoom;
            TargetCameraSpeed = speed > float.Epsilon ? speed : TutorialConst.DefaultTargetCameraSpeed;
            IsCameraSet = true;
            
            return this;
        }
        
        public TutorialAction SetCamera(IMapObject mapObject, float cameraZoom, float speed = -1f)
        {
            TargetCameraPos = jGameCamera.GetMapObjectPosition(mapObject);
            TargetCameraZoom = cameraZoom;
            TargetCameraSpeed = speed < 0f ? TutorialConst.DefaultTargetCameraSpeed : speed;
            IsCameraSet = true;
            
            return this;
        }
        
        public TutorialAction SetCamera(IMapObject mapObject)
        {
            TargetCameraPos = jGameCamera.GetMapObjectPosition(mapObject);
            TargetCameraZoom = TutorialConst.DefaultCameraZoom;
            TargetCameraSpeed = TutorialConst.DefaultTargetCameraSpeed;
            IsCameraSet = true;

            return this;
        }

        public TutorialAction SetHighlight(GameObject objectToHighlight,
            bool blockOtherElements, VeilParams veilParams = null, Func<GameObject> getObjectToHighlight = null)
        {
            GetObjectToHighlight = getObjectToHighlight;
            ObjectToHighlight = objectToHighlight;
            _blockOtherElements = blockOtherElements;

            if (veilParams == null)
                veilParams = new VeilParams();

            VeilParams = veilParams;

            return this;
        }

        public void SwitchHighlight(GameObject objectToHighlight, bool blockOtherElements, VeilParams veilParams = null)
        {
            HideLookers();
            SetHighlight(objectToHighlight, blockOtherElements, veilParams);

            SetLookController();
        }


		public TutorialAction SetDialog(DialogData data)
        {
            DialogText = data.DialogText;
            CharacterToShow = data.CharacterToShow;
            DialogPosition = data.DialogPosition;
            CharacterExpression = data.CharacterExpression;
            IsDialogDarkness = data.UseDarkness;
            IsDialogEnabled = true;
            AcceptButtonText = GetLocalizedText(data.AcceptButtonText);
            DeclineButtonText = GetLocalizedText(data.DeclineButtonText);
            ShowAcceptButtons = data.ShowAcceptButtons;

            return this;
        }

		public TutorialAction SetDialogButton(bool buttonEnabled = false, string buttonText = "", bool isRewardButtonType = false)
		{
			IsDialogButtonEnabled = buttonEnabled;
            DialogButtonText = GetLocalizedText(buttonText);
			IsDialogButtonRewardType = isRewardButtonType;
            return this;
		}

        public TutorialAction SetDialogAcceptButtons(bool isEnabled, string acceptText, string declineText, Action<bool> acceptResultAction, bool useMessageBoxPopup = false)
        {
            ShowAcceptButtons = isEnabled;
            AcceptButtonText = GetLocalizedText(acceptText);
            DeclineButtonText = GetLocalizedText(declineText);
            AcceptResult = acceptResultAction;
            UseMessageBoxPopup = useMessageBoxPopup;
            return this;
        }

        public TutorialAction SetInputState(InputActionType input)
        {
            InputState = new InputState() { CurrentState = input};
            return this;
        }

        private static string GetLocalizedText(string localizationKey)
        {
            string localizedText = LocalizationManager.GetTranslation(localizationKey);
            return localizedText != localizationKey ? localizedText : LocalizationMLS.Instance.GetText(localizationKey, true);
        }

        #endregion

        public override string ToString()
        {
            return $"{GetType().Name}";
        }

        public TutorialAction AddCompleteListener(OnComplete callback)
        {
            OnComplete += callback;
            return this;
        }
    }

    [Serializable]
    public class TutorialStepData
    {
        [SerializeField] private StepType _step;
        [SerializeField] private HudStateData _hudState;
        
        public StepType Step => _step;

        public HudStateData HudState => _hudState;
    }

    [Serializable]
    public class HudStateData
    {
        public ButtonState LevelPanel = new ButtonState();
        public ButtonState HappinessPanel = new ButtonState();
        public ButtonState PopulationPanel = new ButtonState();
        public ButtonState WarehousePanel = new ButtonState();
        
        public ButtonState QuestsPanel = new ButtonState();
        
        public ButtonState BuildingsPanel = new ButtonState();
        public ButtonState RoadPanel = new ButtonState();
        public ButtonState BalancePanel = new ButtonState();
        public ButtonState SettingsPanel = new ButtonState();
        public ButtonState EditPanel = new ButtonState();
        public ButtonState FoodSupplyPanel = new ButtonState();
    }

    [Serializable]
    public class ButtonState
    {
        public bool IsActive = false;
        public bool IsInteractable = false;
    }
}
