﻿namespace NanoReality.Game.Tutorial
{
    public enum StepType
    {
        None = 0,
        Storyline = 1,
        
        BuildCityRoad = 2,
        ReplaceDuplex = 3,
        BuildApartments = 4,
        StartApartmentConstruction = 5,

        AddMineProductionSlot = 6,
        ProduceIronOre = 7,
        CollectIronOre = 8,
        
        FinishApartmentConstruction = 9,
        SkipTimer = 10,
        FinishCityQuest = 11,
        LevelUp = 12,
        
        BuildSchool = 13,
        SchoolAoE = 14,
        CollectTaxes = 15,
        CollectTaxesFinish = 16,

        TutorialFinished = 1000,

        //Unused
        
        RepairCityHall,
        
        BuildIronMine,

        BuildSmelter,
        BuildSmelterRoad ,
        ProduceIronBar,
        CollectIronBar,
        FinishIndustryQuest,
        
        BuildAgroField,
        ProduceWheat,
        CollectWheat,
        FinishAgroQuest,
        SendCityOrder,
        CollectCityOrder,

        FinishCityOrderQuest,
        IndustryOrder,
        UpgradeWarehouse,

        StartUpgradeCityHall,
        BuildMill,
        BuildAgroPowerPlant,
        ProduceFlour,
        CollectFlour,
        SendFlourToOrderDesk,
        UpgradeCityHall,
    }
}
