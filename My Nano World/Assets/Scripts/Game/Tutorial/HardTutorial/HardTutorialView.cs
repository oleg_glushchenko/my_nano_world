﻿using UnityEngine;

namespace NanoReality.Game.Tutorial
{
    public abstract class HardTutorialView : TutorialViewBase
    {
        public void StartTutorialStep()
        {
            BaseInit();
            AddTutorialActions();
            ProcessNextTutorialAction();
        }

        protected virtual void AddTutorialActions()
        {
            jUIManager.HideAll(true);
            jPopupManager.ForceCloseAllActivePopups();
        }

        public virtual bool IsAllowedBuildPosition(Vector2 gridPosition)
        {
            return true;
        }
    }
}
