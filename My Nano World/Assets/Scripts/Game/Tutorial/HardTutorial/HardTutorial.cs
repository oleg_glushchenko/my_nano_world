﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Assets.NanoLib.UtilitesEngine.CustomDebugging.Models.api;
using Assets.Scripts.Engine.UI.Views.Hud;
using NanoLib.Services.InputService;
using NanoLib.UI.Core;
using NanoReality.Engine.BuildingSystem.CityCamera;
using NanoReality.Engine.UI.Extensions.MainPanel;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.Game.Attentions;
using NanoReality.Game.Data;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.GameLogic.Configs;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.UI.Components;
using strange.extensions.injector.api;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace NanoReality.Game.Tutorial
{
    public sealed class HardTutorial : IHardTutorial
    {
        #region Injections
        [Inject] public InteractBuildingAttentionsSignal jInteractBuildingAttentionsSignal { get; set; }
        [Inject] public IUIManager jUIManager { get; set; }
        [Inject] public IGameCameraModel jGameCameraModel { get; set; }
        [Inject] public IDebug jDebug { get; set; }
        [Inject] public IInjectionBinder jInjectionBinder { get; set; }
        [Inject] public IBuildSettings jBuildSettings { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public StartTutorialSignal jStartTutorialSignal { get; set; }
        [Inject] public SignalOnTutorialStepStarted jTutorialStepStarted { get; set; }
        [Inject] public SignalOnTutorialStepChanged jTutorialStepChanged { get; set; }
        [Inject] public AnaliticsTutorialStepCompletedSignal jAnaliticsTutorialStepCompletedSignal { get; set; }
        [Inject] public SignalOnFinishTutorial jSignalOnFinishTutorial { get; set; }
        [Inject] public TutorialStepCompleteSignal jTutorialStepCompleteSignal { get; set; }
        [Inject] public SwitchBuildingAttentionsSignal jSwitchBuildingsAttentionsSignal { get; set; }
        [Inject] public GraphicRaycaster jUiGraphicRaycaster { get; set; }
        [Inject] public TutorialFlowSettings jTutorialFlowSettings { get; set; }
        [Inject] public BalancePanelModel jBalancePanelModel { get; set; }
        [Inject] public HudPanelModel jHudPanelModel { get; set; }
        [Inject] public TutorialMapObjects jTutorialMapObjects { get; set; }
        [Inject] public IGameConfigData jGameConfigData { get; set; }
        #endregion

        private StepType _currentStep = StepType.None;
        private HardTutorialView _currentTutorialView;
        private readonly Color _logColor = new Color(1f, 0.55f, 0f);
        
        private readonly Dictionary<StepType, Type> _boundTutorialSteps = new Dictionary<StepType, Type>();
        private readonly HashSet<Type> _availableTutorialSteps = new HashSet<Type>
        {
            typeof(StorylineTutorialView),
            typeof(BuildCityRoadTutorialView),
            typeof(BuildApartmentsTutorialView),
            typeof(BuildSchoolTutorialView),
            typeof(ReplaceDuplexTutorialView),
            typeof(BuildIronMineTutorialView),
            typeof(AddMineProductionSlotTutorialView),
            typeof(ProduceIronOreTutorialView),
            typeof(CollectIronOreTutorialView),
            typeof(SkipTimerTutorialView),
            typeof(SchoolAoeTutorialView),
            typeof(RepairCityHallTutorialView),
            typeof(FinishFirstQuestTutorialView),
            typeof(StartApartmentConstructionTutorialView),
            typeof(BuildRoadToSmelterTutorialView),
            typeof(BuildSmelterTutorialView),
            typeof(ProduceIronBarTutorialView),
            typeof(CollectIronBarTutorialView),
            typeof(FinishApartmentConstructionTutorialView),
            typeof(BuildAgroFieldTutorialView),
            typeof(ProduceWheatTutorialView),
            typeof(CollectWheatTutorialView),
            typeof(SendCityOrderTutorialView),
            typeof(CollectCityOrderTutorialView),
            typeof(FinishCityOrderQuestTutorialView),
            typeof(UpgradeWarehouseTutorialView),
            typeof(BuildMillTutorialView),
            typeof(BuildAgroPowerPlantTutorialView),
            typeof(ProduceFlourTutorialView),
            typeof(CollectFlourTutorialView),
            typeof(FinishAgroQuestTutorialView),
            typeof(FinishIndustryQuestTutorialView),
            typeof(UpgradeCityHallTutorialView),
            typeof(LevelUpTutorialView),
            typeof(CollectTaxesTutorialView),
            typeof(CollectTaxesFinishTutorialView),
        };
        
        #region IHardTutorial implementation

        public bool IsTutorialCompleted => (_currentStep >= jTutorialFlowSettings.TutorialSteps.Last().Step);

        public void UpdateStep()
        {
            _currentStep = jPlayerProfile.TutorialStep;
        }

        public bool IsBuildingApprovedByTutorial(Vector2 position)
        {
            return _currentStep >= StepType.TutorialFinished || _currentTutorialView.IsAllowedBuildPosition(position);
        }

        public void StartTutorial()
        {
            if (jBuildSettings.DisableHardTutorial)
            {
                SkipTutorial();
                return;
            }

            if (!IsTutorialCompleted)
            {
                jTutorialMapObjects.SetActive(true);
                
                if(_currentStep == StepType.Storyline)
                    jStartTutorialSignal.Dispatch(_currentStep);

                LaunchNextStep(_currentStep);

                jGameCameraModel.IsNeedToLockSwipeProperty.Value = true;
            }
            else
            {
                jTutorialMapObjects.SetActive(false);
            }
        }
        
        public void JumpStep(StepType stepType, Action successCallback)
        {
            if (_currentStep != stepType)
            {
                jDebug.Log($"Tutorial Jump: {stepType}", true);
                jServerCommunicator.JumpHardTutorialStep(stepType, () =>
                {
                    OnTutorialStepCompletedCallback(stepType);
                    successCallback?.Invoke();
                });
            }
            else
            {
                successCallback?.Invoke();
            }
        }
        
        public void SkipTutorial(Action successCallback = null)
        {
            JumpStep(StepType.TutorialFinished, () =>
            {
                _currentStep = StepType.TutorialFinished;
                successCallback?.Invoke();
            });
        }

        #endregion

        public void CompleteCurrentStep()
        {
            jDebug.Log("<b>Complete HardTutorial Step:</b> " + _currentStep, _logColor, true);

            jUiGraphicRaycaster.enabled = false;
            jServerCommunicator.HardTutorialStepCompleted(OnTutorialStepCompletedCallback);
        }

        #region Construct implementation

        [PostConstruct]
        public void PostConstruct()
        {
            BindTutorialSteps();
            jTutorialStepCompleteSignal.AddListener(OnTutorialStepComplete);
        }

        [Deconstruct]
        public void Deconstruct()
        {
            _availableTutorialSteps.Clear();
            _boundTutorialSteps.Clear();

            ResolveTutorialSteps();
            
            jTutorialStepCompleteSignal.RemoveListener(OnTutorialStepComplete);
        }

        #endregion

        private void BindTutorialSteps()
        {
            foreach (Type tutorialStepType in _availableTutorialSteps)
            {
                var stepAttribute = (TutorialStepAttribute)tutorialStepType.GetCustomAttributes().FirstOrDefault(c => c is TutorialStepAttribute);
                if (stepAttribute == null)
                {
                    throw new Exception($"Tutorial Step <b>{tutorialStepType.Name}</b> hasn't TutorialStepAttribute in class declaration.");
                }
                
                StepType step = stepAttribute.Step;
                
                _boundTutorialSteps.Add(step, tutorialStepType);
                jInjectionBinder.Bind(tutorialStepType).To(tutorialStepType);
            }
        }

        private void ResolveTutorialSteps()
        {
            foreach (Type tutorialStepType in _availableTutorialSteps)
            {
                IInjectionBinding binding = jInjectionBinder.GetBinding(tutorialStepType);
                jInjectionBinder.ResolveBinding(binding, tutorialStepType);
            }
        }

        private void LaunchNextStep(StepType step)
        {
            if (step >= StepType.TutorialFinished)
            {
                _currentStep = step;
                jPlayerProfile.TutorialStep = _currentStep;
                jTutorialStepChanged.Dispatch(_currentStep);
                FinishTutorial();
                return;
            }
            
            if (!_boundTutorialSteps.TryGetValue(step, out Type tutorialStepType))
            {
                jDebug.LogError($"Bound TutorialViewType for step {step} not found.");
                CompleteCurrentStep();
                return;
            }
                
            _currentTutorialView = (HardTutorialView)jInjectionBinder.GetInstance(tutorialStepType);
            
            _currentStep = step;
            _currentTutorialView.ActionData = jGameConfigData.TutorialConfigDate.HardStepActionData.FirstOrDefault(a => a.StepId == (int)step)?.ActionData;
            
            jPlayerProfile.TutorialStep = _currentStep;
            jTutorialStepChanged.Dispatch(_currentStep);
            
            jDebug.Log($"Start HardTutorial Step: <b>{step}</b>", _logColor, true);
            
            _currentTutorialView.StartTutorialStep();
            jTutorialStepStarted.Dispatch(_currentStep);

            ApplyTutorialStepData();
        }

        private void FinishTutorial()
        {
            jDebug.Log("<b>HardTutorial finished</b>", _logColor, true);
            // TODO: workaround.
            var findObjectOfType = Object.FindObjectOfType<InputLayerChecker>();
            findObjectOfType.CurrentInputState = InputState.CreateDefault();
            
            jUiGraphicRaycaster.enabled = true;

            _currentTutorialView = null;
            jSignalOnFinishTutorial.Dispatch();

            RemovedBlockers();
            
            jSwitchBuildingsAttentionsSignal.Dispatch(true);
            jGameCameraModel.IsNeedToLockSwipeProperty.Value = false;
            jTutorialMapObjects.SetActive(false);
            jInteractBuildingAttentionsSignal.Dispatch(true);
            
            ApplyTutorialStepData();
        }

        private void RemovedBlockers()
        {
            var hudView = jUIManager.GetView<HudView>();
            hudView.SetButtonsAndTogglesEnabled(true);
            hudView.HudRoadPanel.OpenConstructRoadPanelButton.enabled = true;
            hudView.HudQuestsPanel.UpdateQuestList();
            jUIManager.GetView<BalancePanelView>().SetButtonsAndTogglesEnabled(true);
        }
        
        private void OnTutorialStepComplete(TutorialViewBase completedStep)
        {
            if (completedStep == _currentTutorialView)
            {
                CompleteCurrentStep();
            }
        }

        private void OnTutorialStepCompletedCallback(StepType currentStep)
        {
            jUiGraphicRaycaster.enabled = true;
            
            jAnaliticsTutorialStepCompletedSignal.Dispatch(_currentStep, HintType.None);
            _currentTutorialView?.Deactivate();
            
            LaunchNextStep(currentStep);
        }

        private void ApplyTutorialStepData()
        {
            TutorialStepData tutorialStepData = GetCurrentStepData();
            HudStateData hudState = tutorialStepData.HudState;
            jBalancePanelModel.HappinessModelActive.Value = hudState.HappinessPanel.IsActive;
            jBalancePanelModel.PopulationHappinessModelActive.Value = hudState.PopulationPanel.IsActive;
            jBalancePanelModel.LevelModelActive.Value = hudState.LevelPanel.IsActive;
            jBalancePanelModel.WarehouseModelActive.Value = hudState.WarehousePanel.IsActive;
            jBalancePanelModel.BalanceModelActive.Value = hudState.BalancePanel.IsActive;
            jBalancePanelModel.FoodSupplyPanelActive.Value = hudState.FoodSupplyPanel.IsActive;

            jHudPanelModel.BuildingsPanelModel.IsActive = hudState.BuildingsPanel.IsActive;
            jHudPanelModel.RoadPanelModel.IsActive = hudState.RoadPanel.IsActive;
            jHudPanelModel.QuestsPanelModel.IsActive = hudState.QuestsPanel.IsActive;
            jHudPanelModel.SettingsModel.IsActive = hudState.SettingsPanel.IsActive;
            jHudPanelModel.EditModel.IsActive = hudState.SettingsPanel.IsActive;

            if (_currentStep > StepType.Storyline)
            {
                jHudPanelModel.SetChangedAndNotify();
            }
        }
        
        /// <summary>
        /// Finds step data for current step, or if not configured, get previous step data.
        /// </summary>
        /// <returns></returns>
        private TutorialStepData GetCurrentStepData()
        {
            var lastStep = StepType.None;
            foreach (TutorialStepData stepData in jTutorialFlowSettings.TutorialSteps)
            {
                if (stepData.Step <= _currentStep && stepData.Step > lastStep)
                {
                    lastStep = stepData.Step;
                }
            }

            return jTutorialFlowSettings.GetStepData(lastStep);
        }

        public void Init()
        {
            UpdateStep();
        }

        public void Dispose()
        {
            _currentStep = StepType.None;
        }
    }
}
