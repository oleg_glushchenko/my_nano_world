using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.GameLogic.Utilites;
using NanoReality.Engine.UI.Extensions.Agricultural;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using UnityEngine;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.ProduceWheat)]
    public sealed class ProduceWheatTutorialView : HardTutorialView
    {
        private readonly BusinessSectorsTypes SectorType = BusinessSectorsTypes.Farm;
        private readonly MapObjectintTypes BuildingType = MapObjectintTypes.AgriculturalField;
        
        protected override void AddTutorialActions()
        {
            MapObjectView agroField = FindMapObjectViewOnMapByType(SectorType, BuildingType, 0);

            AddOpenMapObjectPanelAction(FindFactory(), typeof(AgroFieldPanelView));
            AddOnlyDialogAction(LocalizationKeyConstants.SOFT_TUTORIAL_REPLIC_LEVEL_4_PLANTING_CROPS,
                DialogCharacter.Farmer, DialogPosition.Left, CharacterExpression.Cool);

            var collectProduct = CreateAction<TutorialActionPlantCrops>();
            collectProduct.Init(FindFactoryGameObject);
            collectProduct.SetCamera(agroField.MapObjectModel);
            collectProduct.SetBuildingModel(agroField.MapObjectModel as IAgroFieldBuilding);
            collectProduct.IsLookerCollidersActive = false;
            ActionsToDoList.Add(collectProduct);
            AddWaitForSecondsAction(0.5f);
        }
        
        private GameObject FindFactoryGameObject()
        {
            return FindFactory().gameObject;
        }

        private MapObjectView FindFactory()
        {
            return FindMapObjectViewOnMapByType(SectorType, BuildingType, 0);
        }
    }
}