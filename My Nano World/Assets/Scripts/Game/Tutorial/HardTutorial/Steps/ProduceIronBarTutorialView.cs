﻿using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.GameLogic.Utilites;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.ProduceIronBar)]
    public class ProduceIronBarTutorialView : HardTutorialView
    {
        protected override void AddTutorialActions()
        {
            var factory = FindFactory();

            AddOnlyDialogAction(LocalizationKeyConstants.TUTORIAL_REPLIC_LETS_PRODUCE_IRON_BAR, DialogCharacter.Engineer);

            var showPanel = AddOpenMapObjectPanelAction(factory, typeof(FactoryPanelView));

            showPanel.SetCamera(factory.MapObjectModel);

            ///////////////////////////////////////////////////////////////////////////////////////
            AddWaitForSecondsAction(0.1f).OnComplete += () =>
                AddStartProductionAction(TutorialConst.IronBarID.Value, 0, MapObjectintTypes.Factory,
                    BusinessSectorsTypes.HeavyIndustry).OnComplete += () => AddWaitForSecondsAction(3f);
        }

        private MapObjectView FindFactory()
        {
            return FindMapObjectViewOnMapByType(BusinessSectorsTypes.HeavyIndustry, MapObjectintTypes.Factory, 0);
        }
    }
}
