using System;
using System.Collections.Generic;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.UI.Views.Hud;
using I2.Loc;
using NanoReality.Engine.UI.Views.Hud;
using NanoReality.Game.UI;
using NanoReality.Game.UI.BuildingsShopPanel;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Configs;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.BuildSchool)]
    public sealed class BuildSchoolTutorialView : HardTutorialView
    {
        private const int MoveCameraDataAction = 0;
        private const int BuildDataAction = 1;
        private const int FindBuildDataAction = 2;

        private Vector2 _buildingGridPosition;

        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();
            
            Vector2 cameraPosition = ActionData[MoveCameraDataAction].Positions[0];

            _buildingGridPosition = ActionData[BuildDataAction].Positions[0];
            BusinessSectorsTypes BuildingSector = ActionData[BuildDataAction].BusinessSectorsType;
            BuildingsCategories BuildingCategory = ActionData[BuildDataAction].BuildingData[0].Category[0];
            BuildingSubCategories SubCategory = ActionData[BuildDataAction].BuildingData[0].SubCategory;
            int schoolBuildingId = ActionData[BuildDataAction].BuildingData[0].Id;
            int schoolBuildingLevel = ActionData[BuildDataAction].BuildingData[0].Level;
            var dragLimits = ActionData[BuildDataAction].TutorialDragLimit[0];
            
            int findBuildingId = ActionData[FindBuildDataAction].BuildingData[0].Id;
            BusinessSectorsTypes findBuildingSector = ActionData[FindBuildDataAction].BusinessSectorsType;
            MapObjectintTypes findObjectType = ActionData[FindBuildDataAction].BuildingData[0].MapObjectType;

            if (HasBuildingOnMap(BuildingSector, schoolBuildingId, schoolBuildingLevel, _buildingGridPosition))
            {
                jDebug.LogError($"School already exists. TutorialStep: {GetType().Name} skipped.");
                return;
            }

            var hudView = jUIManager.GetView<HudView>();

            HudBuildingsPanelView hudBuildingsPanel = hudView.HudBuildingsPanel;
            hudBuildingsPanel.BuildingsPanelButton.interactable = false;
            Button industryBuildingsButton = hudBuildingsPanel.IndustryBuildingsButton;
            Button cityBuildingsButton = hudBuildingsPanel.CityBuildingsButton;
            Button farmBuildingsButton = hudBuildingsPanel.FarmBuildingsButton;
            Action<bool> cityAnimation = hudBuildingsPanel.DoCityAnimation;

            MapObjectView buildingView = jGameManager.FindMapObjectViewOnMapByType(findBuildingSector, findObjectType, findBuildingId);
            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_SERVICE_UNHAPPY, DialogCharacter.Girl,
                    DialogPosition.Left, CharacterExpression.Attention)
                .SetCamera(buildingView.MapObjectModel);
            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_SERVICE_PLACE_BUILDING, DialogCharacter.Girl)
                .OnComplete += () => hudBuildingsPanel.BuildingsPanelButton.interactable = true;
            
            BuildingsShopPanelMediator.CategoryData = new BuildingShopData(BuildingSector, BuildingCategory, BuildingSubCategories.Common);
            AddPressButtonAction(hudBuildingsPanel.BuildingsPanelButton);

            TutorialActionShowPanel pressBuildingsShopAction = AddPressBuildingsShopAction(cityBuildingsButton, cityAnimation);
            pressBuildingsShopAction.SetCamera(cameraPosition.ToVector2F());
            pressBuildingsShopAction.OnActivate += () =>
            {
                cityAnimation.Invoke(true);
                cityBuildingsButton.enabled = true;
                cityBuildingsButton.gameObject.SetActive(true);
                
                industryBuildingsButton.enabled = false;
                farmBuildingsButton.enabled = false;
            };
            pressBuildingsShopAction.OnComplete += () =>
            {
                cityBuildingsButton.enabled = false;
                cityAnimation.Invoke(false);
                BuildingsShopPanelMediator.CategoryData = null;
            };

            AddBuildBuildingActions(BuildingSector, schoolBuildingId, BuildingCategory, SubCategory, _buildingGridPosition, dragLimits)
                .OnComplete += () => { jConstructConfirmPanelModel.isCancelButtonEnabled.Value = false; };
            
            GetActiveAction<PlaceAndConstructBuildingTutorialAction>().OnActivate += () => jUIManager.GetView<HudView>().SetButtonsAndTogglesEnabled(false);

            AddWaitForConstructionFinishedAction(BuildingSector, schoolBuildingId, _buildingGridPosition)
                .SetCamera(cameraPosition.ToVector2F());
        }
        
        public override bool IsAllowedBuildPosition(Vector2 gridPosition)
        {
            return gridPosition.Equals(_buildingGridPosition);
        }
        
        protected override List<TutorialActionData> GetLocalActionData()
        {
            List<TutorialActionData> localData = new List<TutorialActionData>();
            
            TutorialActionData cameraAction = new TutorialActionData();
            cameraAction.Positions = new List<Vector2>()
            {
                new Vector2(-8, 15),
            };
            cameraAction.BusinessSectorsType = BusinessSectorsTypes.Town;

            TutorialActionData buildAction = new TutorialActionData();
            buildAction.Positions = new List<Vector2>()
            {
                new Vector2(1, 1)
            };
            buildAction.BusinessSectorsType = BusinessSectorsTypes.Town;
            buildAction.BuildingData = new List<TutorialBuildingData>()
            {
                new TutorialBuildingData(69, 0, MapObjectintTypes.School, new List<BuildingsCategories>() {BuildingsCategories.Service},
                    BuildingSubCategories.Common)
            };
            buildAction.TutorialDragLimit = new List<Rect>()
            {
                new Rect(0, 0, 10, 11)
            };
            
            TutorialActionData findBuildAction = new TutorialActionData
            {
                BusinessSectorsType = BusinessSectorsTypes.Town,
                BuildingData = new List<TutorialBuildingData>() {new TutorialBuildingData(145, 1, MapObjectintTypes.DwellingHouse)}
            };

            localData.Add(findBuildAction);
            localData.Add(cameraAction);
            localData.Add(buildAction);

            return localData;
        }
    }
}
