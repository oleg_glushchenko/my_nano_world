﻿using System.Collections.Generic;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.UI.Views.Hud;
using I2.Loc;
using NanoReality.Engine.UI.Views.Hud;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Configs;
using UnityEngine;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.ReplaceDuplex)]
    public sealed class ReplaceDuplexTutorialView : HardTutorialView
    {
        private const int MoveCameraDataAction = 0;
        private const int BuildDataAction = 1;
        
        private BusinessSectorsTypes SectorType;
        private int BuildingId;
        private Vector2 _buildingGridPosition;
        private Vector2 _replaceDuplexCameraPosition;
        private HudBuildingsPanelView hudBuildingsPanel;

        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();
            SectorType = ActionData[MoveCameraDataAction].BusinessSectorsType;
            _replaceDuplexCameraPosition = ActionData[MoveCameraDataAction].Positions[0];
            BuildingId = ActionData[BuildDataAction].BuildingData[0].Id;
            _buildingGridPosition = ActionData[BuildDataAction].Positions[0];
            Rect dragLimits = ActionData[BuildDataAction].TutorialDragLimit[0];
            MapObjectintTypes mapObjectType = ActionData[BuildDataAction].BuildingData[0].MapObjectType;

            if (HasBuildingOnMap(SectorType, BuildingId, 1, _buildingGridPosition))
            {
                jDebug.LogError($"Duplex already replaced. Tutorial Step: {GetType().Name} skipped.");
                return;
            }

            ICityMap cityMap = jGameManager.GetMap(SectorType.GetBusinessSectorId());
            IMapObject duplexModel = cityMap.MapObjects.Find(c => c.ObjectTypeID == BuildingId);
            
            MapObjectView buildingView = jGameManager.FindMapObjectViewOnMapByType(SectorType, mapObjectType, BuildingId);
            var holdDuplex = new VeilParams(200, -50, 200, 350, PointerMode.LongTap, buildingView.gameObject);
            hudBuildingsPanel = jUIManager.GetView<HudView>().HudBuildingsPanel;
           
            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_REPLACE_DUPLEX, DialogCharacter.Girl)
                .SetCamera(_replaceDuplexCameraPosition.ToVector2F());

            var action = CreateAction<ReplaceBuildingTutorialAction>();
            action.MapObjectView = buildingView;
            action.IsBuildingsNotInteractable = true;
            action.InteractableBuilding = buildingView;
            action.Init(buildingView.gameObject, true, holdDuplex);
            action.Initialize(duplexModel.MapID, _buildingGridPosition, SectorType, dragLimits);
            action.SetCamera(_replaceDuplexCameraPosition.ToVector2F());
            
            jConstructConfirmPanelModel.isCancelButtonEnabled.SetValueAndForceNotify(false);
            action.OnComplete += () => hudBuildingsPanel.BuildingsPanelButton.interactable = false;
            ActionsToDoList.Add(action);
        }

        public override bool IsAllowedBuildPosition(Vector2 gridPosition)
        {
            return gridPosition == _buildingGridPosition;
        }
        
        protected override List<TutorialActionData> GetLocalActionData()
        {
            List<TutorialActionData> localData = new List<TutorialActionData>();
            
            TutorialActionData cameraAction = new TutorialActionData();
            cameraAction.Positions = new List<Vector2>()
            {
                new Vector2(-2, 15),
            };
            cameraAction.BusinessSectorsType = BusinessSectorsTypes.Town;

            TutorialActionData buildAction = new TutorialActionData();
            buildAction.Positions = new List<Vector2>()
            {
                new Vector2(7, 1)
            };
            buildAction.BusinessSectorsType = BusinessSectorsTypes.Town;
            buildAction.BuildingData = new List<TutorialBuildingData>()
            {
                new TutorialBuildingData(144, 1, MapObjectintTypes.DwellingHouse)
            };
            buildAction.TutorialDragLimit = new List<Rect>()
            {
                new Rect(0,0,10,11)
            };

            localData.Add(cameraAction);
            localData.Add(buildAction);

            return localData;
        }

        public override void Deactivate()
        {
            base.Deactivate();
            hudBuildingsPanel.BuildingsPanelButton.interactable = false;
        }
    }
}