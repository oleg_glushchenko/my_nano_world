﻿using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.UI;
using NanoReality.Game.Production;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Production;
using NanoReality.GameLogic.Configs;
using NanoReality.GameLogic.MinePanel;
using UnityEngine;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.CollectIronOre)]
    public sealed class CollectIronOreTutorialView : HardTutorialView
    {
        protected override void AddTutorialActions()
        {
            int buildingLevel = ActionData[0].BuildingData[0].Level;
            BusinessSectorsTypes businessSectorsType = ActionData[0].BusinessSectorsType;
            MapObjectintTypes mapObjectType = ActionData[0].BuildingData[0].MapObjectType; 
            
            MapObjectView mine = FindMapObjectViewOnMapByType(businessSectorsType, mapObjectType, buildingLevel);
            if (mine == null)
            {
                jDebug.LogError($"Mine not found. Tutorial Step: {GetType().Name} skipped.");
                return;
            }

            AddWaitForSecondsAction(0.1f).SetCamera(mine.MapObjectModel);
            
            if (jUIManager.IsViewVisible(typeof(MinePanelView)))
            {
                var collectProducts = CreateAction<CollectProductionPanelProductTutorialAction>();
                collectProducts.Set((IProductionBuilding)mine.MapObjectModel);
                ActionsToDoList.Add(collectProducts);

                var mineView = jUIManager.GetView<MinePanelView>();
                mineView.BuyNewSlotButton.interactable = false;
                mineView.AddNewSlotButton.interactable = false;
                
                AddWaitForSecondsAction(0.5f).OnComplete += () =>
                {
                    mineView.BuyNewSlotButton.interactable = true;
                    mineView.AddNewSlotButton.interactable = true;
                    
                    jHideWindowSignal.Dispatch(typeof(MinePanelView));
                };
            }
            else
            {
                base.AddTutorialActions();
                var collectProduct = CreateAction<TutorialActionCollectProducedProducts>();
                collectProduct.Init();
                collectProduct.SetCamera(mine.MapObjectModel);
                collectProduct.Set((IProductionBuilding)mine.MapObjectModel);

                ActionsToDoList.Add(collectProduct);

                AddWaitForSecondsAction(0.5f);
            }
        }
        
        protected override List<TutorialActionData> GetLocalActionData()
        {
            List<TutorialActionData> localData = new List<TutorialActionData>();

            TutorialActionData buildAction = new TutorialActionData();
            buildAction.BusinessSectorsType = BusinessSectorsTypes.HeavyIndustry;
            buildAction.BuildingData = new List<TutorialBuildingData>()
            {
                new TutorialBuildingData(0, 0, MapObjectintTypes.Mine)
            };

            localData.Add(buildAction);

            return localData;
        }
    }
}
