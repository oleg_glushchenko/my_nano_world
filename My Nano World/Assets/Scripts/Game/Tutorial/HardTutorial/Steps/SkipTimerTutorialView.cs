using System.Collections.Generic;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using I2.Loc;
using NanoLib.Services.InputService;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Configs;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.SkipTimer)]
    public sealed class SkipTimerTutorialView : HardTutorialView
    {

        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();
            
            int buildingId = ActionData[0].BuildingData[0].Id;
            BusinessSectorsTypes buildingSector = ActionData[0].BusinessSectorsType;
            MapObjectintTypes mapObjectType = ActionData[0].BuildingData[0].MapObjectType;

            MapObjectView building = jGameManager.FindMapObjectViewOnMapByType(buildingSector, mapObjectType, buildingId);

            if (building && building.MapObjectModel.Level > 0 && building.MapObjectModel.IsConstructed)
            {
                jDebug.LogError($"Dwelling already upgraded. Tutorial Step: {GetType().Name} skipped.");
                return;
            }

            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_ASSISTANT_SKIP_TIMER, 
                    DialogCharacter.Girl, DialogPosition.Left, CharacterExpression.Attention).SetCamera(building.MapObjectModel)
                .OnComplete += () =>
            {
                var action = CreateAction<TutorialActionSkipConstructionTime>();
                action.Init(buildingId, true);
                action.SetInputState(InputActionType.Tap);
                ActionsToDoList.Add(action);
            };
        }
        
        protected override List<TutorialActionData> GetLocalActionData()
        {
            List<TutorialActionData> localData = new List<TutorialActionData>();

            TutorialActionData buildAction = new TutorialActionData
            {
                BusinessSectorsType = BusinessSectorsTypes.Town,
                BuildingData = new List<TutorialBuildingData>() {new TutorialBuildingData(145, 1, MapObjectintTypes.DwellingHouse)}
            };

            localData.Add(buildAction);

            return localData;
        }
    }
}
