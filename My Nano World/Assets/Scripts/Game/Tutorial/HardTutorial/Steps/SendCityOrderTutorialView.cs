﻿using System.Linq;
using Assets.Scripts.Engine.UI.Views.CityOrderDeskPanel;
using Assets.Scripts.GameLogic.Utilites;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using UnityEngine.UI;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.SendCityOrder)]
    public class SendCityOrderTutorialView : HardTutorialView
    {
        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();
        }
    }
}