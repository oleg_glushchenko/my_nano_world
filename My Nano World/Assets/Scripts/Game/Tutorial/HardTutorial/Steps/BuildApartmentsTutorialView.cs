using System.Collections.Generic;
using Assets.Scripts.Engine.UI.Views.Hud;
using I2.Loc;
using NanoReality.Engine.UI.Views.Hud;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.Game.UI;
using NanoReality.Game.UI.BuildingsShopPanel;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Configs;
using UnityEngine;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.BuildApartments)]
    public class BuildApartmentsTutorialView : HardTutorialView
    {
        private const int MoveCameraDataAction = 0;
        private const int BuildDataAction = 1;
        
        private BuildingsCategories BuildingCategory;
        private BusinessSectorsTypes BuildingSector;
        private BusinessSectorsTypes CameraSector;
        private BuildingSubCategories SubCategory;
        private int BuildingId;

        private Vector2 _buildingGridPosition;

        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();
            Vector2 cameraPosition = ActionData[MoveCameraDataAction].Positions[0];
            _buildingGridPosition = ActionData[BuildDataAction].Positions[0];
            BuildingSector = ActionData[BuildDataAction].BusinessSectorsType;
            CameraSector = ActionData[MoveCameraDataAction].BusinessSectorsType;
            BuildingCategory = ActionData[BuildDataAction].BuildingData[0].Category[0];
            SubCategory = ActionData[BuildDataAction].BuildingData[0].SubCategory;
            BuildingId = ActionData[BuildDataAction].BuildingData[0].Id;
            var dragLimits = ActionData[BuildDataAction].TutorialDragLimit[0];


            if (HasBuildingOnMap(BuildingSector, BuildingId, 0, _buildingGridPosition))
            {
                jDebug.LogError($"Building base already placed. Tutorial Step: {GetType().Name} skipped.");
                return;
            }

            HudBuildingsPanelView hudBuildingsPanel = jUIManager.GetView<HudView>().HudBuildingsPanel;
            hudBuildingsPanel.BuildingsPanelButton.interactable = false;
            
            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_BUILD_APARTMENT_1, DialogCharacter.Girl)
                .SetCameraInSector(cameraPosition.ToVector2F(), CameraSector)
                .OnComplete += () => hudBuildingsPanel.BuildingsPanelButton.interactable = true;

            BuildingsShopPanelMediator.CategoryData = new BuildingShopData(BuildingSector, BuildingCategory, SubCategory);

            AddPressButtonAction(hudBuildingsPanel.BuildingsPanelButton);
           
            AddPressBuildingsShopAction(hudBuildingsPanel.CityBuildingsButton, hudBuildingsPanel.DoCityAnimation)
                .OnComplete += () => BuildingsShopPanelMediator.CategoryData = null;

            AddBuildBuildingActions(BuildingSector, BuildingId, BuildingCategory, SubCategory, _buildingGridPosition, dragLimits)
                 .OnComplete += () => jConstructConfirmPanelModel.isCancelButtonEnabled.Value = false;

            GetActiveAction<PlaceAndConstructBuildingTutorialAction>().OnActivate += () =>
            {
                jUIManager.GetView<HudView>().SetButtonsAndTogglesEnabled(false);
                jUIManager.GetView<BalancePanelView>().SetButtonsAndTogglesEnabled(false);
            };

            AddWaitForConstructionFinishedAction(BuildingSector, BuildingId, _buildingGridPosition);
        }

        public override bool IsAllowedBuildPosition(Vector2 gridPosition)
        {
            return gridPosition.Equals(_buildingGridPosition);
        }
        
        protected override List<TutorialActionData> GetLocalActionData()
        {
            List<TutorialActionData> localData = new List<TutorialActionData>();
            
            TutorialActionData cameraAction = new TutorialActionData();
            cameraAction.Positions = new List<Vector2>()
            {
                new Vector2(5, 1),
            };
            cameraAction.BusinessSectorsType = BusinessSectorsTypes.Town;

            TutorialActionData buildAction = new TutorialActionData();
            buildAction.Positions = new List<Vector2>()
            {
                new Vector2(5, 1)
            };
            buildAction.BusinessSectorsType = BusinessSectorsTypes.Town;
            buildAction.BuildingData = new List<TutorialBuildingData>()
            {
                new TutorialBuildingData(145, 0, MapObjectintTypes.None, new List<BuildingsCategories>() {BuildingsCategories.Residential},
                    BuildingSubCategories.Common)
            };
            buildAction.TutorialDragLimit = new List<Rect>()
            {
                new Rect(0, 0, 10, 11)
            };

            localData.Add(cameraAction);
            localData.Add(buildAction);

            return localData;
        }

        public override void Deactivate()
        {
            BuildingsShopPanelMediator.CategoryData = null;
            base.Deactivate();
        }
    }
}
