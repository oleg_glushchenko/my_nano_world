﻿using System.Collections.Generic;
using Assets.Scripts.Engine.UI.Views.Hud;
using Engine.UI.Views.Hud.ButtonNotifications;
using I2.Loc;
using NanoReality.Engine.UI.Extensions.Hud;
using NanoReality.Engine.UI.Views.Hud;
using NanoReality.Game.UI;
using NanoReality.GameLogic.Configs;
using NanoReality.GameLogic.Quests;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.FinishCityQuest)]
    public sealed class FinishFirstQuestTutorialView : HardTutorialView
    {
        private HudBuildingsPanelView hudBuildingsPanel;
        protected override void AddTutorialActions()
        {
            int questFirstGoalId = ActionData[0].QuestId;
            HudQuestButtonsView hudQuestButtonsView = jUIManager.GetView<HudView>().HudQuestsPanel;
            QuestButtonView questButton = hudQuestButtonsView.GetQuestButton(questFirstGoalId);
            hudQuestButtonsView.gameObject.SetActive(false);
            questButton.OpenQuest.interactable = false;
            
            if (jUIManager.IsViewVisible(typeof(FxPopUpPanelView)))
            {
                AddWaitForPanelCloseAction(typeof(FxPopUpPanelView), false).OnComplete += () =>
                {
                    base.AddTutorialActions();
                };
            }
            else
            {
                base.AddTutorialActions();
            }

            IQuest firstGoalQuest = jUserQuestData.GetQuestById(questFirstGoalId);
            if (firstGoalQuest.QuestState == UserQuestStates.Completed)
            {
                jDebug.LogError($"Quest already completed. Tutorial Step: {GetType().Name} skipped.");
                return;
            }
            
            hudBuildingsPanel = jUIManager.GetView<HudView>().HudBuildingsPanel;
            
            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_ASSISTANT_QUEST, 
                    DialogCharacter.Girl, DialogPosition.Right, CharacterExpression.Attention)
                .OnComplete += () =>
            {
                hudQuestButtonsView.gameObject.SetActive(true);
                questButton.OpenQuest.interactable = true;
            };

            AddPressButtonAction(questButton.OpenQuest);

            AddWaitForSecondsAction(1f).OnComplete += () =>
            {
                QuestView questItem = hudQuestButtonsView.QuestView;
                questItem.Close.interactable = false;
                AddPressButtonAction(questItem.Claim);
                AddWaitForLevelUpAction(2).OnComplete += () =>
                {
                    hudBuildingsPanel.BuildingsPanelButton.interactable = false;
                    questItem.Close.interactable = true;
                };
            };
        }
        
        protected override List<TutorialActionData> GetLocalActionData()
        {
            List<TutorialActionData> localData = new List<TutorialActionData>();

            TutorialActionData quest = new TutorialActionData
            {
                QuestId = 1
            };

            localData.Add(quest);

            return localData;
        }
        
        public override void Deactivate()
        {
            base.Deactivate();
            hudBuildingsPanel.BuildingsPanelButton.interactable = false;
        }
    }
}
