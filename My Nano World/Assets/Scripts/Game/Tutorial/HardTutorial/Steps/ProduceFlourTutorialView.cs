using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.GameLogic.Utilites;
using NanoReality.Engine.UI.Views.NotEnoughtResourcesPanel;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using UnityEngine;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.ProduceFlour)]
    public sealed class ProduceFlourTutorialView : HardTutorialView
    {
        private readonly BusinessSectorsTypes SectorType = BusinessSectorsTypes.Farm;
        private readonly MapObjectintTypes MillObjectType = MapObjectintTypes.Factory;
        
        private readonly Id<Product> FlourId = new Id<Product>(178);

        protected override void AddTutorialActions()
        {
            var millMapObjectView = FindMill();

            AddOnlyDialogAction(LocalizationKeyConstants.TUTORIAL_REPLIC_PRODUCE_FLOUR, DialogCharacter.Farmer);
            AddOpenMapObjectPanelAction(millMapObjectView, typeof(FactoryPanelView))
                .SetCamera(millMapObjectView.MapObjectModel);

            AddWaitForSecondsAction(0.1f)
                .OnComplete += () => AddStartProductionAction(FlourId, 0, MillObjectType, SectorType)
                .OnComplete += () => AddWaitForSecondsAction(3f);

        }

        private bool IsFinishProduce()
        {
            return FindMapObject<IFactoryObject>(SectorType, MillObjectType).HasProductsToCollect();
        }

        private GameObject FindMillGameObject()
        {
            return FindMill().gameObject;
        }

        private MapObjectView FindMill()
        {
            return FindMapObjectViewOnMapByType(SectorType, MillObjectType, 0);
        }
    }
}