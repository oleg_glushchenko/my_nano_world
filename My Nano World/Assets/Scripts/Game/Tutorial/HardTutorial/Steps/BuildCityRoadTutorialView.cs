﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Engine.UI.Views.Hud;
using I2.Loc;
using NanoReality.Engine.UI.Views.Hud;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.Configs;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.BuildCityRoad)]
    public class BuildCityRoadTutorialView : HardTutorialView
    {
        private List<Vector2> _roadGridPositions = new List<Vector2>();
        
        private List<Vector2> _currentPositions;

        private const int MoveCameraFirstDataAction = 0;
        private const int MoveCameraSecondDataAction = 1;
        private const int BuildDataAction = 2;
        private const DialogCharacter Character = DialogCharacter.Girl;
        private  BusinessSectorsTypes SectorTypeFirst = BusinessSectorsTypes.Unsupported;
        private  BusinessSectorsTypes SectorTypeSecond = BusinessSectorsTypes.Unsupported;

        private Vector2 _cameraPositionFirst;
        private Vector2 _cameraPositionSecond;
        private int RoadId;

        
        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();

            SectorTypeFirst = ActionData[MoveCameraFirstDataAction].BusinessSectorsType;
            _cameraPositionFirst = ActionData[MoveCameraFirstDataAction].Positions[0];
            _cameraPositionSecond = ActionData[MoveCameraSecondDataAction].Positions[0];
            SectorTypeSecond = ActionData[BuildDataAction].BusinessSectorsType;
            _roadGridPositions = ActionData[BuildDataAction].Positions;
            RoadId = ActionData[BuildDataAction].BuildingData[0].Id;
            
            if (IsRoadAlreadyExists())
            {
                jDebug.LogError($"Road already constructed. Tutorial Step: {GetType().Name} skipped.");
                return;
            }

            HudRoadPanelView roadPanel = jUIManager.GetView<HudView>().HudRoadPanel;

            Button openConstructRoadButton = roadPanel.OpenConstructRoadPanelButton;
            Button constructRoadButton = roadPanel.ConstuctRoadButton;
            Button destroyRoadButton = roadPanel.DestoryRoadButton;
            Button closeConstructRoadButton = roadPanel.CloseConstructRoadPanelButton;
            Action<bool> highlightOpenRoadPanel = roadPanel.PlayOpenRoadConstructButtonAnimation;
            GameObject highlightRoadButtonParticles = roadPanel.GameObjectParticlesRoad;

            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_ASSISTANT_HELLO_1, Character, 
                DialogPosition.Left, CharacterExpression.Idle).SetCameraInSector(_cameraPositionFirst.ToVector2F(), SectorTypeFirst);
            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_ASSISTANT_HELLO_2, Character, 
                DialogPosition.Left, CharacterExpression.Idle);
            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_BUILDING_ROADS_1, Character)
                .IsBuildingsNotInteractable = true;
            
            TutorialAction pressHudRoadPanel = AddPressButtonAction(openConstructRoadButton).SetCamera(_cameraPositionSecond.ToVector2F());

            pressHudRoadPanel.OnActivate += () =>
            {
                openConstructRoadButton.enabled = true;
                highlightOpenRoadPanel(true);
                highlightRoadButtonParticles.SetActive(true);
            };

            pressHudRoadPanel.OnComplete += () =>
            {
                highlightOpenRoadPanel(false);
                highlightRoadButtonParticles.SetActive(false);
                constructRoadButton.enabled = true;
                closeConstructRoadButton.enabled = false;
                openConstructRoadButton.enabled = false;
            };

            HighlightPressButtonTutorialAction pressNextRoadButtonAction = AddPressButtonAction(constructRoadButton);
            pressNextRoadButtonAction.OnComplete += () => destroyRoadButton.enabled = false;
            
            AddBuildRoadAction(_roadGridPositions, SectorTypeSecond).OnComplete += () =>
            {
                destroyRoadButton.enabled = true;
                closeConstructRoadButton.enabled = true;
            };
            
            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_BUILDING_ROADS_2, Character,
                DialogPosition.Left, CharacterExpression.Attention);
        }
        
        public override bool IsAllowedBuildPosition(Vector2 gridPosition)
        {
            return _roadGridPositions.Contains(gridPosition);
        }

        private bool IsRoadAlreadyExists()
        {
            return _roadGridPositions.All(roadGridPosition => HasBuildingOnMap(SectorTypeSecond, RoadId, 0, roadGridPosition));
        }
        
        protected override List<TutorialActionData> GetLocalActionData()
        {
            List<TutorialActionData> localData = new List<TutorialActionData>();
            
            TutorialActionData cameraAction = new TutorialActionData();
            cameraAction.Positions = new List<Vector2>()
            {
                new Vector2(-20, -5),
            };
            cameraAction.BusinessSectorsType = BusinessSectorsTypes.Global;

            TutorialActionData cameraActionTwo = new TutorialActionData();
            cameraActionTwo.Positions = new List<Vector2>()
            {
                new Vector2(-5, 15),
            };
            cameraActionTwo.BusinessSectorsType = BusinessSectorsTypes.Town;

            TutorialActionData buildAction = new TutorialActionData();
            buildAction.Positions = new List<Vector2>()
            {
                new Vector2(0, 0),
                new Vector2(1, 0),
                new Vector2(2, 0),
                new Vector2(3, 0),
                new Vector2(4, 0),
                new Vector2(5, 0),
                new Vector2(6, 0),
                new Vector2(7, 0),
                new Vector2(8, 0),
                new Vector2(9, 0),
            };
            buildAction.BusinessSectorsType = BusinessSectorsTypes.Town;
            buildAction.BuildingData = new List<TutorialBuildingData>()
            {
                new TutorialBuildingData(31, 0)
            };

            localData.Add(cameraAction);            
            localData.Add(cameraActionTwo);
            localData.Add(buildAction);

            return localData;
        }
    }
}
