﻿using Assets.Scripts.Engine.UI.Views.Hud;
using Assets.Scripts.Engine.UI.Views.Popups;
using I2.Loc;
using NanoReality.Engine.UI.Views.Hud;
using NanoReality.Engine.UI.Views.LevelUpPanel;
using NanoReality.Game.UI;
using UnityEngine;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.LevelUp)]
    public sealed class LevelUpTutorialView : HardTutorialView
    {
        protected override void AddTutorialActions()
        {
            AddWaitForLevelUpAction(2).OnComplete +=(() =>
            {
                AddWaitForSecondsAction(0.2f).OnComplete += () =>
                {
                    if (jUIManager.IsViewVisible(typeof(LevelUpPanelView)))
                    {
                        AddWaitForPanelCloseAction(typeof(LevelUpPanelView), false);
                        AddWaitForPopupShowAction(typeof(RewardBoxPopupView));
                        AddWaitForPopupCloseAction(typeof(RewardBoxPopupView));
                    }
                    AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_ASSISTANT_LEVEL_GRATZ,
                        DialogCharacter.Girl, DialogPosition.Left, CharacterExpression.Cool);
                };
            });
        }
    }
}