using System;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.UI.Views.Hud;
using Assets.Scripts.GameLogic.Utilites;
using NanoReality.Engine.UI.Views.ConstractConfirmPanel;
using NanoReality.Engine.UI.Views.Hud;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.BuildMill)]
    public sealed class BuildMillTutorialView : HardTutorialView
    {
        private readonly BuildingsCategories BuildingCategory = BuildingsCategories.Crops;
        private readonly BusinessSectorsTypes BuildingSector = BusinessSectorsTypes.Farm;
        private readonly BuildingSubCategories SubCategory = BuildingSubCategories.Common;
        
        private readonly int MillBuildingId = 120;
        
        private readonly Vector2F BuildingCameraPosition = new Vector2F(15, 6);
        private readonly Vector2 BuildingGridPosition = new Vector2(15, 6);

        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();
            
            if (HasBuildingOnMap(BuildingSector, MillBuildingId, 0, BuildingGridPosition))
            {
                Debug.LogError($"Mill already exist. Tutorial Step : {GetType().Name} skipped. ");
                return;
            }

            AddOnlyDialogAction(LocalizationKeyConstants.TUTORIAL_REPLIC_FARM_BUILD_FLOURMILL, DialogCharacter.Farmer);
            HudView hudView = jUIManager.GetView<HudView>();

            HudBuildingsPanelView hudBuildingsPanel = hudView.HudBuildingsPanel;
            Button buildingsButton = hudBuildingsPanel.FarmBuildingsButton;
            Action<bool> buttonAnimation = hudBuildingsPanel.DoAgroAnimation;

            TutorialActionShowPanel pressBuildingsShopAction = AddPressBuildingsShopAction(buildingsButton, buttonAnimation);
            pressBuildingsShopAction.SetCameraInSector(BuildingCameraPosition, BuildingSector);

            AddBuildBuildingActions(BuildingSector, MillBuildingId, BuildingCategory, SubCategory, BuildingGridPosition);
        }
        
        public override bool IsAllowedBuildPosition(Vector2 gridPosition)
        {
            return gridPosition.Equals(BuildingGridPosition);
        }
    }
}