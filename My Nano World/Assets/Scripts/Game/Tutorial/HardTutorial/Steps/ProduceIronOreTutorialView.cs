﻿using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl.Mines;
using I2.Loc;
using NanoLib.Services.InputService;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Production;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.Configs;
using NanoReality.GameLogic.MinePanel;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.ProduceIronOre)]
    public sealed class ProduceIronOreTutorialView : HardTutorialView
    {
        private MinePanelView MinePanelView => jUIManager.GetView<MinePanelView>();
        
        private float _cachedProducingTime;
        private float _dataProducingTime;
        private IProducingItemData _ironOreProducingData;


        protected override void AddTutorialActions()
        {
            if (!jUIManager.IsViewVisible(typeof(MinePanelView)))
            {
                base.AddTutorialActions();
            }

            BusinessSectorsTypes sectorType = ActionData[0].BusinessSectorsType;
            MapObjectintTypes mineObjectType = ActionData[0].ProduceProductData[0].MapObjectType;
            MapObjectintTypes mineObjectTypeSecond = ActionData[0].ProduceProductData[1].MapObjectType;
            int ironOreId = ActionData[0].ProduceProductData[0].Id;
            int slot = ActionData[0].ProduceProductData[0].Slot;
            int slotSecond = ActionData[0].ProduceProductData[1].Slot;
            int ironOreIdSecond = ActionData[0].ProduceProductData[1].Id;
            float producingTime = ActionData[0].ProduceProductData[0].ProductionTime;

            MapObjectView mine = FindMapObjectViewOnMapByType(sectorType, mineObjectType, 0);
            if (mine == null)
            {
                jDebug.LogError($"Mine not found. Tutorial Step: {GetType().Name} skipped.");
                return;
            }

            if (IsProductionFinished((MineObject)mine.MapObjectModel))
            {
                jDebug.LogError($"Ore production finished. Tutorial Step: {GetType().Name} skipped.");
                return;
            }

            jUIManager.GetView<BalancePanelView>().SetButtonsAndTogglesEnabled(false);

            TutorialAction showPanel = AddOpenMapObjectPanelAction(mine, typeof(MinePanelView));
            showPanel.SetInputState(InputActionType.Tap);
            showPanel.SetCamera(mine.MapObjectModel);
            showPanel.OnComplete += () =>
            {
                MinePanelView.BuyNewSlotButton.interactable = false;
                MinePanelView.AddNewSlotButton.interactable = false;
            };

            AddWaitForSecondsAction(0.1f).OnComplete += () =>
            {
                AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_MINE_PRODUCE_IRON_ORE, DialogCharacter.Engineer);
                
                DraggableProductItem draggableProductItem = MinePanelView.DraggableProductItems
                    .Find(c => c.ProducingItemData != null && c.ProducingItemData.OutcomingProducts == ironOreId);
                _ironOreProducingData = draggableProductItem.ProducingItemData;
                _dataProducingTime = _ironOreProducingData.ProducingTime;
                _ironOreProducingData.ProducingTime = producingTime;

                StartIndustryProductionTutorialAction firstProductionSlotAction = AddStartProductionAction(ironOreId, slot, mineObjectType, sectorType);
                StartIndustryProductionTutorialAction secondProductionSlotAction = AddStartProductionAction(ironOreIdSecond, slotSecond, mineObjectTypeSecond, sectorType);
                
                firstProductionSlotAction.OnComplete += () => { OnSlotProduceStarted(firstProductionSlotAction, secondProductionSlotAction); };
                secondProductionSlotAction.OnComplete += () => { OnSlotProduceStarted(secondProductionSlotAction, firstProductionSlotAction); };
            };
        }

        private static bool IsProductionFinished(IProductionBuilding mine)
        {
            return mine.CurrentProduceSlots.All(slot => !slot.IsUnlocked || slot.IsFinishedProduce);
        }

        private void OnSlotProduceStarted(StartIndustryProductionTutorialAction startedSlot, StartIndustryProductionTutorialAction anotherSlot)
        {
            MakeSlotButtonsEnable(startedSlot, false);
            
            if (anotherSlot.IsCompleted)
            {
                _ironOreProducingData.ProducingTime = _dataProducingTime;
                return;
            }
            
            _cachedProducingTime = startedSlot.ProduceSlot.ItemDataIsProducing.ProducingTime;
            WaitUntilAnotherSlotFinished(startedSlot, anotherSlot);
        }

        private void MakeSlotButtonsEnable(StartIndustryProductionTutorialAction slot, bool enable)
        {
            ProduceSlotView produceSlotView = MinePanelView.ProduceSlotViews.First(c => c.ProduceSlot == slot.ProduceSlot);
            produceSlotView.SkipButton.enabled = enable;
            produceSlotView.CollectButton.enabled = enable;
        }

        private void WaitUntilAnotherSlotFinished(StartIndustryProductionTutorialAction startedSlot, StartIndustryProductionTutorialAction anotherSlot)
        {
            AddWaitForSecondsAction(_cachedProducingTime).OnComplete += () =>
            {
                if (anotherSlot.IsCompleted)
                {
                    startedSlot.ProduceSlot.ItemDataIsProducing.ProducingTime = anotherSlot.ProduceSlot.ItemDataIsProducing.ProducingTime;
                    
                    MakeSlotButtonsEnable(anotherSlot, true);
                    MakeSlotButtonsEnable(startedSlot, true);
                }
                else
                {
                    startedSlot.ProduceSlot.ItemDataIsProducing.ProducingTime = _cachedProducingTime;
                    WaitUntilAnotherSlotFinished(startedSlot, anotherSlot);
                } 
            };
        }
        
        protected override List<TutorialActionData> GetLocalActionData()
        {
            List<TutorialActionData> localData = new List<TutorialActionData>();

            TutorialActionData produceAction = new TutorialActionData
            {
                BusinessSectorsType = BusinessSectorsTypes.HeavyIndustry, ProduceProductData = new List<TutorialProduceProductData>()
            };

            TutorialProduceProductData data = new TutorialProduceProductData
            {
                Slot = 0, Id = 157, MapObjectType = MapObjectintTypes.Mine, ProductionTime = 3
            };
            produceAction.ProduceProductData.Add(data);
            data.Slot = 1;
            produceAction.ProduceProductData.Add(data);

            localData.Add(produceAction);

            return localData;
        }
    }
}