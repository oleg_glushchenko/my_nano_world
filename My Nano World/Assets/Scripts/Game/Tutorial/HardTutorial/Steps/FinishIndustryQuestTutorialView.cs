using Assets.NanoLib.Utilities;
using Assets.Scripts.GameLogic.Utilites;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.Quests;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.FinishIndustryQuest)]
    public sealed class FinishIndustryQuestTutorialView : HardTutorialView
    {

        private readonly Id<IQuest> _industryQuestId = new Id<IQuest>(3);
        private readonly Id<IQuest> _upgradeApartmentsQuestId = new Id<IQuest>(11);

        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();
            
            AddOnlyDialogAction(LocalizationKeyConstants.SOFT_TUTORIAL_REPLIC_LEVEL_4_REWARD,
                DialogCharacter.Engineer, DialogPosition.Left, CharacterExpression.Cool);

            AddWaitForSecondsAction(3.0f).OnComplete += () =>
                AddHudQuestButtonAction(_industryQuestId, BusinessSectorsTypes.HeavyIndustry);

            AddWaitForSecondsAction(0.5f).OnComplete += () =>
                AddHudQuestButtonAction(_upgradeApartmentsQuestId, BusinessSectorsTypes.Town);
        }
    }
}