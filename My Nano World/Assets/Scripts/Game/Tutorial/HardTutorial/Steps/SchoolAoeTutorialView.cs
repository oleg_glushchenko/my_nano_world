﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using I2.Loc;
using NanoLib.Services.InputService;
using NanoReality.Engine.UI.Views.Entertaiment;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.Happiness;
using NanoReality.GameLogic.Configs;
using UnityEngine;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.SchoolAoE)]
    public sealed class SchoolAoeTutorialView : HardTutorialView
    {
        private BusinessSectorsTypes BuildingSector;
        private MapObjectintTypes BuildingType;

        private HighlightToggleTutorialAction _pressLayoutToggleAction;
        private EntertaimentAoeLayoutPanelView AoeLayoutPanelView => jUIManager.GetView<EntertaimentAoeLayoutPanelView>();

        private const AoeLayout RequiredAoeLayout = AoeLayout.TopRight;
        
        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();
            
            int buildLevel = ActionData[0].BuildingData[0].Level;
             BuildingSector = ActionData[0].BusinessSectorsType;
             BuildingType = ActionData[0].BuildingData[0].MapObjectType;

            if (IsValidAoe())
            {
                jDebug.LogError($"School already correctly set up. Tutorial Step: {GetType().Name} skipped.");
                return;
            }
            
            AddWaitForSecondsAction(0.2f);
            
            MapObjectView schoolView = FindMapObjectViewOnMapByType(BuildingSector, BuildingType, buildLevel);
            
            TutorialActionShowPanel openSchoolAction = AddOpenMapObjectPanelAction(schoolView, typeof(EntertaimentPanelView));
            openSchoolAction.SetInputState(InputActionType.Tap);
            openSchoolAction.SetCamera(schoolView.MapObjectModel);
            openSchoolAction.OnComplete += () => DialogActions().OnComplete += PostDialogActions;
        }

        private TutorialAction DialogActions()
        {
            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_SERVICE_HAPPINESS_AOE, DialogCharacter.Girl, DialogPosition.Left,
                CharacterExpression.Attention)
                .OnActivateCompleted += () => AoeLayoutPanelView.gameObject.SetActive(false);

            TutorialAction action = AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_SERVICE_HAPPINESS_REMINDER, DialogCharacter.Girl, DialogPosition.Right,
                CharacterExpression.Attention);
            action.OnComplete += () => AoeLayoutPanelView.gameObject.SetActive(true);
            
            return action;
        }

        private void PostDialogActions()
        {
            _pressLayoutToggleAction = AddPressToggleAction(AoeLayoutPanelView.TopRight);
            _pressLayoutToggleAction.SetDependencyWithAction(IsValidAoe);
            _pressLayoutToggleAction.OnComplete += () =>  AoeLayoutPanelView.SetButtonsAndTogglesEnabled(false);

            AddWaitForSecondsAction(1f).OnComplete += () =>
            {
                AoeLayoutPanelView.SetButtonsAndTogglesEnabled(true);
                jHideWindowSignal.Dispatch(typeof(EntertaimentPanelView));
                jHideWindowSignal.Dispatch(typeof(EntertaimentAoeLayoutPanelView));
                        
                AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_SERVICE_HAPPY, DialogCharacter.Girl, DialogPosition.Left,
                    CharacterExpression.Cool);
                AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_SERVICE_BIGGER_AOE, DialogCharacter.Girl, DialogPosition.Left,
                    CharacterExpression.Attention);
            };
        }

        private bool IsValidAoe()
        {
            if (jUIManager.IsViewVisible(typeof(EntertaimentPanelView)) && AoeLayoutPanelView.HappinessBuilding != null)
                return AoeLayoutPanelView.HappinessBuilding.AoeLayoutType == RequiredAoeLayout;
            
            List<IMapObject> serviceBuildings = jGameManager.GetMap(BuildingSector.GetBusinessSectorId()).FindAllMapObjects(BuildingType);
            return ((IBuildingProduceHappiness)serviceBuildings[0]).AoeLayoutType == RequiredAoeLayout;
        }
        
        protected override List<TutorialActionData> GetLocalActionData()
        {
            List<TutorialActionData> localData = new List<TutorialActionData>();

            TutorialActionData buildAction = new TutorialActionData
            {
                BusinessSectorsType = BusinessSectorsTypes.Town,
                BuildingData = new List<TutorialBuildingData>() {new TutorialBuildingData(-1, 0, MapObjectintTypes.School)}
            };

            localData.Add(buildAction);

            return localData;
        }
    }
}
