using System;
using Assets.Scripts.Engine.UI.Views.Hud;
using Assets.Scripts.GameLogic.Utilites;
using NanoReality.Engine.UI.Views.Hud;
using NanoReality.Game.UI;
using NanoReality.Game.UI.BuildingsShopPanel;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.BuildIronMine)]
    public class BuildIronMineTutorialView : HardTutorialView
    {
        private const BuildingsCategories BuildingCategory = BuildingsCategories.Metal;
        private const BusinessSectorsTypes BuildingSector = BusinessSectorsTypes.HeavyIndustry;
        private const BuildingSubCategories SubCategory = BuildingSubCategories.Common;
        private const int IronMineBuildingId = 105;

        private readonly Vector2 _buildingGridPosition = new Vector2(3, 18);
        private readonly Vector2 _cameraPosition = new Vector2(-18, -17);

        private const string ConstructDwellingDialog = LocalizationKeyConstants.TUTORIAL_REPLIC_HELLO_INDUSTRIAL;
        private const string BuildPlaceDwellingDialog = LocalizationKeyConstants.TUTORIAL_REPLIC_CONSTRUCT_METALMINE;


        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();
            
            if (HasBuildingOnMap(BuildingSector, IronMineBuildingId, 0, _buildingGridPosition))
            {
                Debug.LogError($"Mine already exist. Tutorial Step : {GetType().Name} skipped. ");
                return;
            }

            var hudView = jUIManager.GetView<HudView>();

            HudBuildingsPanelView hudBuildingsPanel = hudView.HudBuildingsPanel;
            Button industryBuildingsButton = hudBuildingsPanel.IndustryBuildingsButton;
            Action<bool> industryAnimation = hudBuildingsPanel.DoIndustryAnimation;
            
            BuildingsShopPanelMediator.CategoryData = new BuildingShopData(BusinessSectorsTypes.HeavyIndustry, BuildingsCategories.Metal, BuildingSubCategories.Common);
            TutorialAction pressBuildingsShopAction = AddPressBuildingsShopAction(industryBuildingsButton,industryAnimation, ConstructDwellingDialog, DialogCharacter.Engineer);
            pressBuildingsShopAction.SetCamera(_cameraPosition.ToVector2F());
            pressBuildingsShopAction.OnComplete += () =>
            {
                BuildingsShopPanelMediator.CategoryData = null;
            };

            var dragLimits = new Rect(0, 0, 8, 9);
            AddBuildBuildingActions(BuildingSector, IronMineBuildingId, BuildingCategory, SubCategory, _buildingGridPosition, null,
                BuildPlaceDwellingDialog, DialogCharacter.Engineer);

            AddWaitForConstructionFinishedAction(BuildingSector, IronMineBuildingId, _buildingGridPosition);
        }
        
        public override bool IsAllowedBuildPosition(Vector2 gridPosition)
        {
            return gridPosition.Equals(_buildingGridPosition);
        }
    }
}