using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.GameLogic.Utilites;
using NanoReality.Engine.UI.Extensions.Agricultural;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using UnityEngine;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.CollectWheat)]
    public sealed class CollectWheatTutorialView : HardTutorialView
    {
        private readonly BusinessSectorsTypes SectorType = BusinessSectorsTypes.Farm;
        private readonly MapObjectintTypes BuildingType = MapObjectintTypes.AgriculturalField;

        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();
            
            MapObjectView agroField = FindMapObjectViewOnMapByType(SectorType, BuildingType, 0);

            AddOpenMapObjectPanelAction(FindFactory(), typeof(AgroFieldPanelView));

            AddOnlyDialogAction(LocalizationKeyConstants.TUTORIAL_FARM_1,
                DialogCharacter.Farmer, DialogPosition.Left, CharacterExpression.Cool);

            var collectProduct = CreateAction<TutorialActionCropsHarvested>();
            collectProduct.Init(FindFactoryGameObject);
            collectProduct.SetCamera(agroField.MapObjectModel);
            collectProduct.SetBuildingModel(agroField.MapObjectModel as IAgroFieldBuilding);
            collectProduct.IsLookerCollidersActive = false;
            collectProduct.IsBuildingsNotInteractable = false;

            ActionsToDoList.Add(collectProduct);
            AddWaitForSecondsAction(0.5f);
        }
        
        private GameObject FindFactoryGameObject()
        {
            return FindFactory().gameObject;
        }

        private MapObjectView FindFactory()
        {
            return FindMapObjectViewOnMapByType(SectorType, BuildingType, 0);
        }
    }
}