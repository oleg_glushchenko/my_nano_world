﻿using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Production;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.CollectIronBar)]
    public sealed class CollectIronBarTutorialView : HardTutorialView
    {
        private const BusinessSectorsTypes SectorType = BusinessSectorsTypes.HeavyIndustry;
        private const MapObjectintTypes BuildingType = MapObjectintTypes.Factory;

        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();
            
            MapObjectView mine = FindMapObjectViewOnMapByType(SectorType, BuildingType, 0);
            ((IProductionBuilding) mine.MapObjectModel).MaximalSlotsCount = 2;

            if (jUIManager.IsViewVisible(typeof(FactoryPanelView)))
            {
                var collectAction = CreateAction<CollectProductionPanelProductTutorialAction>();
                collectAction.Set((IProductionBuilding)mine.MapObjectModel);
                ActionsToDoList.Add(collectAction);

                AddWaitForPanelShowAction(typeof(FxPopUpPanelView));
                AddWaitForPanelCloseAction(typeof(FxPopUpPanelView));
                
                AddWaitForSecondsAction(0.1f).OnComplete += () => jHideWindowSignal.Dispatch(typeof(FactoryPanelView));
            }
            else
            {
                var collectAction = CreateAction<TutorialActionCollectProducedProducts>();
                collectAction.Init();
                collectAction.SetCamera(mine.MapObjectModel);
                collectAction.Set((IProductionBuilding)mine.MapObjectModel);
                ActionsToDoList.Add(collectAction);
                
                AddWaitForPanelShowAction(typeof(FxPopUpPanelView));
                AddWaitForPanelCloseAction(typeof(FxPopUpPanelView));
            }
        }
    }
}
