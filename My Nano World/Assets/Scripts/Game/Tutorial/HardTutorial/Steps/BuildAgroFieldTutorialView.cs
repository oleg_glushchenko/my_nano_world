using System;
using Assets.Scripts.Engine.UI.Views.Hud;
using NanoReality.Engine.UI.Views.Hud;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using UnityEngine;
using UnityEngine.UI;
using static Assets.Scripts.GameLogic.Utilites.LocalizationKeyConstants;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.BuildAgroField)]
    public sealed class BuildAgroFieldTutorialView : HardTutorialView
    {
        private readonly BuildingsCategories BuildingCategory = BuildingsCategories.Crops;
        private readonly BusinessSectorsTypes BuildingSector = BusinessSectorsTypes.Farm;
        private readonly BuildingSubCategories SubCategory = BuildingSubCategories.Common;
        private readonly int AgroFieldBuildingId = 127;

        private readonly Vector2 BuildingGridPosition = new Vector2(21, 2);
        
        private readonly Vector2 BuildCameraPosition = new Vector2(21, 2);

        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();
            
            if (HasBuildingOnMap(BuildingSector, AgroFieldBuildingId, 0, BuildingGridPosition))
            {
                return;
            }

            var hudView = jUIManager.GetView<HudView>();

            HudBuildingsPanelView hudBuildingsPanel = hudView.HudBuildingsPanel;
            Button agroBuildingsButton = hudBuildingsPanel.FarmBuildingsButton;
            Action<bool> cityButtonAnimation = hudBuildingsPanel.DoAgroAnimation;


            AddPressBuildingsShopAction(agroBuildingsButton, cityButtonAnimation, TUTORIAL_FARM_0,
                    DialogCharacter.Farmer)
                .SetCameraInSector(BuildCameraPosition.ToVector2F(), BuildingSector);
            AddBuildBuildingActions(BuildingSector, AgroFieldBuildingId, BuildingCategory, SubCategory, BuildingGridPosition);
        }

        public override bool IsAllowedBuildPosition(Vector2 gridPosition)
        {
            return gridPosition.Equals(BuildingGridPosition);
        }
    }
}