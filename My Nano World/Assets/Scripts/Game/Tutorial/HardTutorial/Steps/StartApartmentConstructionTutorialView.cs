using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl;
using DG.Tweening;
using I2.Loc;
using NanoLib.Services.InputService;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Configs;
using NanoReality.GameLogic.Upgrade;
using UnityEngine;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.StartApartmentConstruction)]
    public sealed class StartApartmentConstructionTutorialView : HardTutorialView
    {
        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();
            int buildingId = ActionData[0].BuildingData[0].Id;
            int buildingLevel = ActionData[0].BuildingData[0].Level;
            Vector2 buildingGridPosition = ActionData[0].Positions[0];
            BusinessSectorsTypes businessSectorsType = ActionData[0].BusinessSectorsType;
            MapObjectintTypes mapObjectType = ActionData[0].BuildingData[0].MapObjectType; 

            if (HasBuildingOnMap(businessSectorsType, buildingId, buildingLevel, buildingGridPosition))
            {
                jDebug.LogError($"Building already exists and repaired. Tutorial Step: {GetType().Name} skipped.");
                return;
            }

            MapObjectView buildingView = jGameManager.FindMapObjectViewOnMapByType(businessSectorsType, mapObjectType, buildingId);
            if (buildingView == null)
            {
                jDebug.LogError($"Building does not exist. Tutorial Step: {GetType().Name} skipped.");
                return;
            }
            
            if (!((MDwellingHouseMapObject)buildingView.MapObjectModel).CanBeUpgraded)
            {
                jDebug.LogError($"Building is already upgraded. Tutorial Step: {GetType().Name} skipped.");
                return;
            }
            
            AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_BUILD_APARTMENT_2, DialogCharacter.Girl,
                DialogPosition.Left, CharacterExpression.Attention)
                .SetCamera(buildingView.MapObjectModel)
                .OnComplete += () =>
            {
                AddPressBuildingAttentionAction(buildingView, false, typeof(UpgradeWithProductsPanelView))
                    .SetCamera(buildingView.MapObjectModel).SetInputState(InputActionType.Tap)
                    .OnComplete += HighlightFirstUpgradeItem;
            
                AddWaitForSecondsAction(1.5f);
                
                AddOnlyDialogAction(ScriptTerms.Tutorial.TUTORIAL_DIALOG_BUILD_APARTMENT_3, DialogCharacter.Girl,
                        DialogPosition.Right, CharacterExpression.Attention)
                    .OnComplete += () =>
                {
                    if(jUIManager.IsViewVisible(typeof(UpgradeWithProductsPanelView)))
                        jUIManager.GetView<UpgradeWithProductsPanelView>().Hide();
                };
            };
        }

        private void HighlightFirstUpgradeItem()
        {
            var upgradePanel = jUIManager.GetActiveView<UpgradeWithProductsPanelView>();
            if (upgradePanel == null)
                return;
            
            DraggableUpgradeItem targetUpgradeItem = upgradePanel.UpgradeItems.FirstOrDefault();
            if (targetUpgradeItem != null)
            {
                targetUpgradeItem.transform.DOPunchScale(0.3f * Vector3.one, 1f);
            }
        }
        
        protected override List<TutorialActionData> GetLocalActionData()
        {
            List<TutorialActionData> localData = new List<TutorialActionData>();

            TutorialActionData buildAction = new TutorialActionData();
            buildAction.Positions = new List<Vector2>()
            {
                new Vector2(6, 5)
            };
            buildAction.BusinessSectorsType = BusinessSectorsTypes.Town;
            buildAction.BuildingData = new List<TutorialBuildingData>()
            {
                new TutorialBuildingData(145, 1, MapObjectintTypes.DwellingHouse)
            };

            localData.Add(buildAction);

            return localData;
        }
    }
}
