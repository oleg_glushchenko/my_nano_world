﻿using Assets.Scripts.Engine.UI.Views.Warehouse;
using Assets.Scripts.GameLogic.Utilites;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.Game.UI;
using UnityEngine.UI;

namespace NanoReality.Game.Tutorial
{
    [TutorialStep(StepType.UpgradeWarehouse)]
    public sealed class UpgradeWarehouseTutorialView : HardTutorialView
    {
        private readonly VeilParams _veilParams = new VeilParams(PointerMode.TapBottom);
        private Button WarehouseTooltipButton => jUIManager.GetView<BalancePanelView>().WarehouseButton;
        private Button UpgradeButton => jUIManager.GetView<WarehouseView>().UpgradeButton;

        protected override void AddTutorialActions()
        {
            base.AddTutorialActions();
            
            AddOnlyDialogAction(LocalizationKeyConstants.SOFT_TUTORIAL_REPLIC_LEVEL_5_HEAVY_OD_TOOLS,
                DialogCharacter.Girl);
            AddPressButtonAction(WarehouseTooltipButton, veilParams: _veilParams);
            AddWaitForSecondsAction(0.1f).OnComplete += () =>
            {
                AddPressButtonAction(UpgradeButton)
                    .OnComplete += () => jHideWindowSignal.Dispatch(typeof(WarehouseView));
            };
        }
    }
}