﻿using System.Collections.Generic;
using NanoReality.Game.Tutorial.HintTutorial;
using strange.extensions.signal.impl;

namespace NanoReality.Game.Tutorial
{
    public class SignalOnTutorialStepStarted : Signal<StepType> { }
    public class AnaliticsTutorialStepCompletedSignal : Signal<StepType,HintType> { }
    public class SignalOnTutorialStepChanged : Signal<StepType> { }
}
