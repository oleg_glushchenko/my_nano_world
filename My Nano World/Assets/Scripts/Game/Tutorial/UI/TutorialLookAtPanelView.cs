﻿using System;
using System.Collections.Generic;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.GameLogic.TutorialAndHintsSystem.Common.TutorialLookAtController;
using NanoLib.UI.Core;
using NanoReality.Game.Data;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.Tutorial
{
    public abstract class TutorialLookAtPanelView : UIPanelView
    {
        [SerializeField] private List<Image> _colorImages;
        [SerializeField] private Image _centerLookerImage;

        [SerializeField] private AnimatedHand _animatedHand;
        [SerializeField] private AnimatedArrow _animatedArrow;

        [SerializeField] protected RectTransform _rectTransformTarget;

        // Default touch area color
        private readonly Color _colorBlack = new Color(0, 0, 0, 0.75f);
        
        private RectTransform _thisRectTransform;
        protected RectTransform ThisRectTransform => _thisRectTransform ? _thisRectTransform : _thisRectTransform = GetComponent<RectTransform>();

        private Camera _mainCamera;
        protected Camera MainCamera => _mainCamera ? _mainCamera : _mainCamera = Camera.main;
        
        private RectTransform _parentCanvasRect;
        protected RectTransform ParentCanvasRect => _parentCanvasRect ? _parentCanvasRect : 
            _parentCanvasRect = ThisRectTransform.GetComponentInParent<Canvas>()?.GetComponent<RectTransform>();

        private GameObject _targetObjectToHighlight;
        private VeilParams _veilParams = new VeilParams();

        private IPointerTargetAnimator _targetAnimator;

        [Inject] public IUIManager jUIManager { get; set; }
        [Inject] public IBuildSettings jBuildSettings { get; set; }
        
        public override void Initialize(object parameters)
        {
            base.Initialize(parameters);
            if (parameters == null)
                return;

            (GameObject objectToHighlight, VeilParams veilParams, bool raycastActive) 
                = ((GameObject, VeilParams, bool raycastActive)) parameters;
            
            if (!objectToHighlight)
            {
                objectToHighlight = ((UIManager)jUIManager).gameObject;
                veilParams = new VeilParams
                {
                    OffsetMax = -objectToHighlight.GetComponent<RectTransform>().sizeDelta,
                    OffsetMin = Vector2.zero,
                    PointerMode = PointerMode.Invisible,
                    DragTargetObject = null
                };
            }
            
            SetLookerColliders(raycastActive);
            LookAtObject(objectToHighlight, veilParams);
        }
        
        private void LateUpdate()
        {
            if (gameObject.activeSelf && _targetObjectToHighlight)
                SetPosition(_targetObjectToHighlight, _veilParams);
        }

        private void LookAtObject(GameObject objectToHighlight, VeilParams param)
        {
            if (!objectToHighlight)
                return;
            
            SetPosition(objectToHighlight, param);
            
            _targetObjectToHighlight = objectToHighlight;
            _veilParams = param;

            SetColor(jBuildSettings.ShowLookerTouchZone ? _colorBlack : param.TintColor);

            _targetAnimator = objectToHighlight.GetComponent<IPointerTargetAnimator>();
            if (_targetAnimator != null)
            {
                _targetAnimator.PlayItemAnimation(ShowPointer, param.PointerMode);
            }
            else
            {
                ShowPointer(param.PointerMode);
            }
        }
        
        private void SetLookerColliders(bool isEnabled)
        {
            foreach (Image colorImage in _colorImages)
                colorImage.raycastTarget = isEnabled;
        }

        private void SetColor(Color targetColor)
        {
            _centerLookerImage.color = targetColor;
            foreach (Image colorImage in _colorImages)
                colorImage.color = targetColor;
        }
        
        protected virtual void SetPosition(GameObject objectToHighlight, VeilParams veilParams) { }
        
        protected virtual void SetPosition(GameObject objectToHighlight, VeilParams veilParams, RectTransform targetRect) { }

        public void UpdateArrow(PointerMode newMode)
        {
            if (_veilParams != null)
                _veilParams.PointerMode = newMode;

            ShowPointer(newMode);            
        }
        
        private void ShowPointer(PointerMode mode, Action onCompleted = null)
        {
            _animatedHand.ShowPointer(mode, onCompleted);
            _animatedArrow.ShowPointer(mode);
        }

        private void OnEnable()
        {
            ShowPointer(PointerMode.Invisible);            
        }

        private void OnDisable()
        {
            ShowPointer(PointerMode.Invisible);       
            _targetAnimator?.StopItemAnimation();
        }
    }
}