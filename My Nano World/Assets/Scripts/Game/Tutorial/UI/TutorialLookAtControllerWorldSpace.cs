﻿using UnityEngine;

namespace NanoReality.Game.Tutorial
{
    public class TutorialLookAtControllerWorldSpace : TutorialLookAtPanelView
    {
        protected override void SetPosition(GameObject objectToHighlight, VeilParams veilParams)
        {
            SetPosition(objectToHighlight, veilParams, ThisRectTransform);

            if (veilParams.PointerMode != PointerMode.Drag || !veilParams.DragTargetObject)
                return;
            
            _rectTransformTarget.SetParent(ThisRectTransform.parent, false);
            SetPosition(veilParams.DragTargetObject, veilParams, _rectTransformTarget);
            _rectTransformTarget.SetParent(ThisRectTransform, true);
        }

        protected override void SetPosition(GameObject objectToHighlight, VeilParams veilParams, RectTransform targetRect)
        {
            Vector3 viewMin, viewMax;

            var targetRectTransform = objectToHighlight.GetComponent<RectTransform>();
            if (targetRectTransform)
            {
                var corners = new Vector3[4];
                targetRectTransform.GetWorldCorners(corners);
                viewMin = MainCamera.WorldToViewportPoint(corners[0]);
                viewMax = MainCamera.WorldToViewportPoint(corners[2]);
            }
            else
            {
                viewMin = viewMax = MainCamera.WorldToViewportPoint(objectToHighlight.transform.position);
            }

            Vector2 localMin = GetScreenPos(viewMin, ParentCanvasRect) + veilParams.OffsetMin;
            Vector2 localMax = GetScreenPos(viewMax, ParentCanvasRect) + veilParams.OffsetMax;
            
            targetRect.anchoredPosition = (localMin + localMax) / 2;
            targetRect.sizeDelta = localMax - localMin;
        }

        private Vector2 GetScreenPos(Vector3 viewPortPos, RectTransform targetCanvasRect)
        {
            Vector2 targetSizeDelta = targetCanvasRect.sizeDelta;
            Vector2 sizeDelta = ParentCanvasRect.sizeDelta;
            return new Vector2(viewPortPos.x * targetSizeDelta.x - sizeDelta.x * 0.5f, viewPortPos.y * targetSizeDelta.y - sizeDelta.y * 0.5f);
        }
    }
}