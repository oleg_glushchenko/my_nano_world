﻿using System;
using System.Collections.Generic;
using Assets.Scripts.GameLogic.TutorialAndHintsSystem.Common.TutorialLookAtController;
using NanoReality.Engine.UI.UIAnimations;
using NanoReality.GameLogic.Utilites;
using NanoReality.GameLogic.Utilities;
using strange.extensions.mediation.impl;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

// ReSharper disable once CheckNamespace
namespace NanoReality.Game.Tutorial
{
    public class AnimatedHand : View, ILookAtPointer
    {
        [SerializeField] private Animator _animation;
        [SerializeField] private GameObjectFlyEffect _flyEffect;
        [SerializeField] private TextMeshProUGUI _helpText;

        private const string LongTapKey = "UICommon/TUTORIAL_HAND_LONG_TAP";

        protected readonly Dictionary<PointerMode, string> _animationNames =
            new Dictionary<PointerMode, string>
            {
                {PointerMode.Tap, "TapAnimation"},
                {PointerMode.LongTap, "LongTapAnimation"},
                {PointerMode.Drag, "DragAnimation"}
            };

        private Action _onCompleted;

        #region Methods

        public void ShowPointer(PointerMode mode, Action onCompleted = null)
        {
            void ActivatePointer()
            {
                gameObject.SetActive(true);
                SetAnimation(mode);
            }

            _onCompleted = onCompleted;
            switch (mode)
            {
                case PointerMode.Drag:
                    ActivatePointer();
                    break;
                case PointerMode.LongTap:
                    ActivatePointer();
                    _helpText.gameObject.SetActive(true);
                    _helpText.text = LocalizationUtils.LocalizeL2(LongTapKey);
                    break;
                default:
                    gameObject.SetActive(false);
                    _helpText.gameObject.SetActive(false);
                    break;
            }
        }

        protected virtual void SetAnimation(PointerMode animationType)
        {
            ((RectTransform) transform).anchoredPosition = Vector2.zero;

            _animation.Play(_animationNames[animationType]);
        }

        public void MoveObjectToStartPositionByAnimation()
        {
            ((RectTransform)transform).anchoredPosition = Vector2.zero;

            _flyEffect.Completed -= FlyEffectOnCompleted;
            _flyEffect.Stop();
        }

        public void MoveObjectToTargetPositionByAnimation()
        {
            _flyEffect.Completed += FlyEffectOnCompleted;
            _flyEffect.Play();
        }

        private void FlyEffectOnCompleted()
        {
            _onCompleted.SafeRaise();
        }

        #endregion
    }
}