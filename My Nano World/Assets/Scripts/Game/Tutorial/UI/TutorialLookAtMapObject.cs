﻿using UnityEngine;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.BuildingSystem.MapObjects.Components;
using NanoReality.Engine.BuildingSystem.MapObjects.Components;

namespace NanoReality.Game.Tutorial
{
    public class TutorialLookAtMapObject : TutorialLookAtPanelView
    {
        protected override void SetPosition(GameObject objectToHighlight, VeilParams veilParams)
        {
            SetPosition(objectToHighlight, veilParams, ThisRectTransform);

            if (veilParams.PointerMode == PointerMode.Drag && veilParams.DragTargetObject)
                SetPosition(veilParams.DragTargetObject, veilParams, _rectTransformTarget);
        }

        protected override void SetPosition(GameObject objectToHighlight, VeilParams veilParams, RectTransform targetRect)
        {
            var mapObjectView = objectToHighlight.GetComponent<MapObjectView>();

            if (mapObjectView)
            {
                ConstructionComponent constructComponent = mapObjectView.ConstructionComponent;
                bool isConstruction = constructComponent != null && constructComponent.ConstructView;
                Bounds bounds = isConstruction ? constructComponent.ConstructView.SpriteRenderer.bounds : mapObjectView.SpriteRenderer.bounds;
            
                Vector3 position = mapObjectView.Transform.position;
                position.y += bounds.size.y / 2;

                Vector3 viewportPosition = MainCamera.WorldToViewportPoint(position);
                Vector2 anchoredPosition = GetScreenPos(viewportPosition, ParentCanvasRect);

                if (targetRect.anchoredPosition != anchoredPosition)
                    targetRect.anchoredPosition = anchoredPosition;
            }
            else
            {
                Bounds bounds = objectToHighlight.GetComponent<Renderer>().bounds;

                Vector3 center = bounds.center;
                Vector3 extents = bounds.extents;

                var minFrontPosition = new Vector3(center.x - extents.x, center.y - extents.y, center.z - extents.z);
                minFrontPosition = objectToHighlight.transform.TransformPoint(minFrontPosition);
                var maxFrontPosition = new Vector3(center.x - extents.x, center.y - extents.y, center.z - extents.z);
                maxFrontPosition = objectToHighlight.transform.TransformPoint(maxFrontPosition);

                Vector3 viewMin = MainCamera.WorldToViewportPoint(minFrontPosition);
                Vector3 viewMax = MainCamera.WorldToViewportPoint(maxFrontPosition);

                Vector2 localMin = GetScreenPos(viewMin, ParentCanvasRect) + veilParams.OffsetMin;
                Vector2 localMax = GetScreenPos(viewMax, ParentCanvasRect) + veilParams.OffsetMax;
                
                targetRect.anchoredPosition = (localMin + localMax) / 2;
                targetRect.sizeDelta = localMax - localMin;
            }
        }

        private static Vector2 GetScreenPos(Vector3 viewPortPos, RectTransform targetCanvasRect)
        {
            Vector2 sizeDelta = targetCanvasRect.sizeDelta;
            return new Vector2((viewPortPos.x - 0.5f) * sizeDelta.x, (viewPortPos.y - 0.5f) * sizeDelta.y);
        }
    }
}