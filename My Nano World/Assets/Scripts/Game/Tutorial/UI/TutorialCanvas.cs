﻿using System;
using NanoLib.Core.Logging;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace NanoReality.Game.Tutorial
{
    public sealed class TutorialCanvas : View
    {
        [SerializeField] private TutorialLookAtControllerOverlay _uiPanel;
        [SerializeField] private TutorialLookAtControllerWorldSpace _worldSpacePanel;
        [SerializeField] private TutorialLookAtMapObject _mapObjectPanel;

        private TutorialLookAtPanelView _activePanel;

        public void Show(Type panelType, object parameters)
        {
            if (_activePanel != null)
            {
                _activePanel.Hide();
            }

            if (panelType == typeof(TutorialLookAtControllerOverlay))
            {
                _activePanel = _uiPanel;
            }
            else if(panelType == typeof(TutorialLookAtControllerWorldSpace))
            {
                _activePanel = _worldSpacePanel;
            }
            else if (panelType == typeof(TutorialLookAtMapObject))
            {
                _activePanel = _mapObjectPanel;
            }
            else
            {
                Logging.LogError(LoggingChannel.Debug, $"Not valid panel type [{panelType}]" );
                return;
            }
            
            _activePanel.gameObject.SetActive(true);
            _activePanel.Initialize(parameters);
            _activePanel.Show();
        }

        public void Hide()
        {
            if (_activePanel != null)
            {
                _activePanel.gameObject.SetActive(false);
                _activePanel.Hide();
                _activePanel = null;
            }
        }
    }
}