﻿using UnityEngine;

namespace NanoReality.Game.Tutorial
{
    public class TutorialLookAtControllerOverlay : TutorialLookAtPanelView
    {
        private Vector3 _lastHighlightPosition = Vector3.zero;
        private int _lastScreenWidth;

        protected override void SetPosition(GameObject objectToHighlight, VeilParams veilParams)
        {
            if (objectToHighlight.transform.position == _lastHighlightPosition && _lastScreenWidth == Screen.width)
                return;

            _lastHighlightPosition = objectToHighlight.transform.position;
            _lastScreenWidth = Screen.width;
            
            SetPosition(objectToHighlight, veilParams, ThisRectTransform);

            if (veilParams.PointerMode == PointerMode.Drag && veilParams.DragTargetObject)
                SetPosition(veilParams.DragTargetObject, veilParams, _rectTransformTarget);
        }

        protected override void SetPosition(GameObject objectToHighlight, VeilParams veilParams, RectTransform targetRect)
        {
            Transform currParent = targetRect.parent;
            int index = targetRect.GetSiblingIndex();
            targetRect.SetParent(objectToHighlight.transform);
            
            targetRect.anchoredPosition = Vector2.zero;
            targetRect.sizeDelta = veilParams.OffsetMax - veilParams.OffsetMin;
            targetRect.pivot = Vector2.one / 2;
            targetRect.SetParent(currParent);
            targetRect.SetSiblingIndex(index);
            targetRect.localScale = Vector3.one;
        }
    }
}