﻿namespace NanoReality.Game.Tutorial
{
    public enum SoftTutorialType
    {
        None = 0,
        Cinema,
        Shop1,
        Shop2,
        BurgerGame,
        NanoCertificates,
        PremiumCurrency,
        NanoCrystalsAndAmazonCoupons
    }
}
