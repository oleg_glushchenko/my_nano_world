﻿using System;
using System.Collections.Generic;

namespace NanoReality.Game.Tutorial
{
    /// <summary>
    /// Представление софт туториала
    /// </summary>
    public abstract class SoftTutorialView : TutorialViewBase
    {
        #region Injections

        [Inject]
        public SignalLaunchSoftTutorial jSignalLaunchSoftTutorial { get; set; }

        #endregion
        
        #region Fields

        protected abstract SoftTutorialType SoftTutorialType { get; }

        #endregion

        #region override TutorialViewBase

        #endregion

        public virtual void OnPreloaderCloudHideEnded()
        {
        }

        public void OnLaunchSoftTutorial()
        {
            BaseInit();

            AddTutorialActions();
            
            ProcessNextTutorialAction();
        }

        public virtual void OnSoftTutorialCompleted(SoftTutorialType type)
        {
        }
        
        protected abstract void AddTutorialActions();
    }
}
