﻿using strange.extensions.signal.impl;


namespace NanoReality.Game.Tutorial
{
    /// <summary>
    /// Сигнал о необходимости запустить софт туториал. Первый параметр - тип софт туториала, второй - если true, софт
    /// туториал будет дожидаться закрытия всех панелей, прежде чем запуститься, если false - туториал запустится тут
    /// же (необходимо для того, что бы запускать софт туториал для конкретных панелей)
    /// </summary>
    public class SignalLaunchSoftTutorial : Signal<SoftTutorialType, bool> { }
    
    public class SignalSoftTutorialStarted : Signal<SoftTutorialType> { }

    public class SignalSoftTutorialCompleted : Signal<SoftTutorialType> { }
}