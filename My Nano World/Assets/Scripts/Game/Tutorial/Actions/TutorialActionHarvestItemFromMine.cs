﻿using System;
using Assets.NanoLib.Utilities;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.api.Mines;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;


namespace NanoReality.Game.Tutorial
{
    public class TutorialActionHarvestItemFromMine : TutorialAction
    {
        public Id<Product> ProductId;
        public Func<IMineObject> GetMineFunc;

        private IMineObject _mine;

        public override bool PreActivateIsCompletedCheck()
        {
            // если есть родительская функция, и она выполнена, игнорируя условия
            // автоматически засчитываем это действие как выполненое
            if (ParentFunc != null && ParentFunc())
                return true;

            _mine = GetMineFunc();

            if (_mine.CurrentProduceSlots == null || _mine.CurrentProduceSlots.Count == 0)
                return true;

            foreach (var produceSlot in _mine.CurrentProduceSlots)
            {
                if (produceSlot.ItemDataIsProducing != null &&
                    produceSlot.ItemDataIsProducing.OutcomingProducts == ProductId)
                {
                    return false;
                }
            }
            
            return true;
        }

        public override void Activate()
        {
            base.Activate();

            if (_mine != null)
            {
                _mine.jOnProductProduceAndCollectSignal.AddOnce((OnMineCollected));
            }
            else
            {
                DoCompleteAction();
            }
        }

        private void OnMineCollected(IMapObject mapObject, Id<Product> id, int arg3)
        {
            if (mapObject == _mine && id == ProductId)
            {
                DoCompleteAction();
            }
        }
    }
}