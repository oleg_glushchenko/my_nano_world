using NanoLib.Core.Logging;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.Tutorial
{
    public sealed class HighlightToggleTutorialAction : TutorialAction
    {
        private Toggle _targetToggle;

        #region Overrides of TutorialAction

        public override bool PreActivateIsCompletedCheck()
        {
            if (base.PreActivateIsCompletedCheck()) return true;

            GameObject buttonGameObject;
            if (ObjectToHighlight != null)
            {
                buttonGameObject = ObjectToHighlight;
            }
            else if (GetObjectToHighlight != null)
            {
                buttonGameObject = GetObjectToHighlight.Invoke();
            }
            else
            {
                Logging.LogError(LoggingChannel.Tutorial, "There is no object to highlight and no way to find it!");

                return true;
            }
            
            _targetToggle = buttonGameObject.GetComponent<Toggle>();

            if (_targetToggle == null)
            {
                Logging.Log(LoggingChannel.Tutorial, "Waiting for next try to retrieve object to highlight!");
            }
            
            return false;
        }

        public override void Activate()
        {
            base.Activate();
            _targetToggle?.onValueChanged.AddListener(OnToggleStateChanged);
        }

        public override void Deactivate()
        {
            base.Deactivate();
            _targetToggle?.onValueChanged.RemoveListener(OnToggleStateChanged);
        }
        public override void DoCompleteAction(bool isCancel = false)
        {
            HideLookers();
            base.DoCompleteAction(isCancel);
        }

        protected override void OnFoundObjectToHighlight(GameObject objectToHighlight)
        {
            base.OnFoundObjectToHighlight(objectToHighlight);

            if (_targetToggle != null) return;
            
            _targetToggle = objectToHighlight.GetComponent<Toggle>();
            _targetToggle?.onValueChanged.AddListener(OnToggleStateChanged);
        }

        #endregion


        private void OnToggleStateChanged(bool toggleValue)
        {
            if (toggleValue)
            {
                DoCompleteAction();
            }
        }
    }
}