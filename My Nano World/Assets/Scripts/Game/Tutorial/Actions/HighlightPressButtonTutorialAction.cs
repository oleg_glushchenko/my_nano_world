﻿using NanoLib.Core.Logging;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.Game.Tutorial
{
    public sealed class HighlightPressButtonTutorialAction : TutorialAction
    {
        private Button _button;

        public override bool PreActivateIsCompletedCheck()
        {
            if (base.PreActivateIsCompletedCheck()) return true;

            GameObject buttonGameObject;
            if (ObjectToHighlight != null)
            {
                buttonGameObject = ObjectToHighlight;
            }
            else if (GetObjectToHighlight != null)
            {
                buttonGameObject = GetObjectToHighlight.Invoke();
            }
            else
            {
                Logging.LogError(LoggingChannel.Tutorial, "There is no object to highlight and no way to find it!");

                return true;
            }
            
            _button = buttonGameObject.GetComponent<Button>();

            if (_button == null)
            {
                Logging.Log(LoggingChannel.Tutorial, "Waiting for next try to retrieve object to highlight!");
            }
            
            return false;
        }

        public override void Activate()
        {
            base.Activate();
            _button?.onClick.AddListener(OnButtonClick);
        }

        public override void Deactivate()
        {
            base.Deactivate();
            _button?.onClick.RemoveListener(OnButtonClick);
        }
        public override void DoCompleteAction(bool isCancel = false)
        {
            HideLookers();
            base.DoCompleteAction(isCancel);
        }

        protected override void OnFoundObjectToHighlight(GameObject objectToHighlight)
        {
            base.OnFoundObjectToHighlight(objectToHighlight);

            if (_button != null) return;
            
            _button = objectToHighlight.GetComponent<Button>();
            _button?.onClick.AddListener(OnButtonClick);
        }

        private void OnButtonClick()
        {
            DoCompleteAction();
        }

        public override string ToString()
        {
            return $"{base.ToString()} HighlightButton:{_button?.name}";
        }
    }
}
