﻿using NanoReality.Game.Tutorial;
using strange.extensions.signal.impl;

public class TutorialActionWaitForCondition : TutorialAction
{
    public Signal SignalToReact;
    public override void Activate()
    {
        base.Activate();
        SignalToReact.AddListener(CheckCondition);
    }

    private void CheckCondition()
    {
        if (!ParentFunc())
            return;

        SignalToReact.RemoveListener(CheckCondition);
        DoCompleteAction();
    }
}
