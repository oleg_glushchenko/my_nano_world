﻿using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects;

namespace NanoReality.Game.Tutorial
{
    public class TutorialActionPlantCrops : TutorialAction
    {
        private IAgroFieldBuilding _building;
        
        public void SetBuildingModel(IAgroFieldBuilding model)
        {
            _building = model;
        }

        #region Overrides of TutorialAction

        public override bool PreActivateIsCompletedCheck()
        {
            if (ParentFunc != null && ParentFunc())
                return true;

            return _building.CurrentState != AgriculturalFieldState.Empty;
        }

        public override void Activate()
        {
            base.Activate();
            
            jTutorialMapObjects.ShowBuildPlace(BusinessSectorsTypes.Farm, _building.MapPosition.ToVector2(), _building.Dimensions.ToVector2());
            _building.OnGrowingStart += OnGrowingStart;
            _building.OnGrowingStartVerified += OnGrowingStartVerified;
        }

        public override void DoCompleteAction(bool isCancel = false)
        {
            base.DoCompleteAction(isCancel);
            
            _building.OnGrowingStart -= OnGrowingStart;
            _building.OnGrowingStartVerified -= OnGrowingStartVerified;
        }

        #endregion

        private void OnGrowingStart(IAgroFieldBuilding agroFieldBuilding)
        {
            jTutorialMapObjects.Hide();
            HideLookers();
        }

        private void OnGrowingStartVerified(IAgroFieldBuilding agroFieldBuilding)
        {
            DoCompleteAction();
        }
    }
}