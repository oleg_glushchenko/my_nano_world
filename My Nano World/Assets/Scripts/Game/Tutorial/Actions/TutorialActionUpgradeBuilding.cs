﻿using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.UI.Views.Popups;
using NanoReality.Engine.UI.Views.NotEnoughtResourcesPanel;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace NanoReality.Game.Tutorial
{
    public class TutorialActionUpgradeBuilding : TutorialAction
    {
        [Inject]
        public SignalOnMapBuildingsUpgradeStarted jSignalOnMapBuildingsUpgradeStarted { get; set; }
        
        [Inject]
        public SignalOnMapBuildingUpgradeVerificated jSignalOnMapBuildingUpgradeVerificated { get; set; }
        
        [Inject]
        public SignalOnShownPanel jSignalOnShownPanel { get; set; }
        
        [Inject]
        public SignalOnHidePanel jSignalOnHidePanel { get; set; }

        private IMapBuilding _building;
        private int _level;

        public void SetBuilding(IMapBuilding building, int level)
        {
            _building = building;
            _level = level;
        }

        #region Overrides of TutorialAction

        public override bool PreActivateIsCompletedCheck()
        {
            if (base.PreActivateIsCompletedCheck())
                return true;

            return _building == null || _building.Level >= _level || !_building.CanBeUpgraded;
        }

        public override void Activate()
        {
            base.Activate();

            jSignalOnMapBuildingsUpgradeStarted.AddListener(OnMapBuildingsUpgradeStarted);
            jSignalOnMapBuildingUpgradeVerificated.AddListener(OnBuildFinishVerified);
            jSignalOnShownPanel.AddListener(OnShownPanel);
            jSignalOnHidePanel.AddListener(OnHidePanel);
        }

        public override void DoCompleteAction(bool isCancel = false)
        {
            HideLookers();
            jHideWindowSignal.Dispatch(typeof(DialogPanelView));

            
            jSignalOnMapBuildingsUpgradeStarted.RemoveListener(OnMapBuildingsUpgradeStarted);
            jSignalOnMapBuildingUpgradeVerificated.RemoveListener(OnBuildFinishVerified);
            jSignalOnShownPanel.RemoveListener(OnShownPanel);
            jSignalOnHidePanel.RemoveListener(OnHidePanel);
            
            base.DoCompleteAction(isCancel);
        }

        #endregion

        private void OnMapBuildingsUpgradeStarted(IMapBuilding obj)
        {
            if (obj != _building)
                return;
            
            HideLookers();
            jHideWindowSignal.Dispatch(typeof(DialogPanelView));
        }

        private void OnBuildFinishVerified(IMapBuilding obj)
        {
            if (obj == _building)
            {
                DoCompleteAction();
            }
        }

        private void OnShownPanel(UIPanelView panel)
        {
            var upgradePanel = panel as UpgradePanelView;
            if (upgradePanel != null)
            {
                SwitchHighlight(upgradePanel.ButtonUpgrade.gameObject, true);
                return;
            }
            
            var notEnoughResourcesPanel = panel as NotEnoughResourcesPopupView;
            if (notEnoughResourcesPanel != null)
            {
                SwitchHighlight(notEnoughResourcesPanel.BuyButton.gameObject, true);
                return;
            }

            var buyPopupView = panel as PurchaseByPremiumPopupView;
            if (buyPopupView != null)
            {
                SwitchHighlight(buyPopupView.YesButton.gameObject, true);
            }
        }

        private void OnHidePanel(UIPanelView obj)
        {
            HideLookers();
            jHideWindowSignal.Dispatch(typeof(DialogPanelView));
        }
    }
}
