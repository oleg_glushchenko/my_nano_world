using Assets.NanoLib.UI.Core.Signals;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.UI.Extensions.BuildingsShopPanel;
using NanoReality.Game.UI.BuildingsShopPanel;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using UnityEngine;

namespace NanoReality.Game.Tutorial
{
    public class HighlightBuildingInShopTutorialAction : TutorialAction
    {
        [Inject] public SignalOnMapObjectChangedSelect jOnMapObjectSelectedSignal { get; set; }

        public Id_IMapObjectType TargetBuildingTypeId { get; private set; }
        public BuildingsCategories TargetBuildingCategory { get; private set; }
        public BuildingSubCategories TargetBuildingSubCategory { get; private set; }
        private BuildingItem buildingItem;

        public void SetBuildingTypeId(Id_IMapObjectType targetId)
        {
            TargetBuildingTypeId = targetId;
        }

        public void SetBuildingCategory(BuildingsCategories category, BuildingSubCategories subCategory)
        {
            TargetBuildingCategory = category;
            TargetBuildingSubCategory = subCategory;
        }

        private void OnSelectBuilding(MapObjectView view, bool isSelected)
        {
            if (!isSelected)
            {
                return;
            }

            if (TargetBuildingTypeId != view.ObjectTypeId)
            {
                return;
            }
            
            DoCompleteAction();
        }

        public override void DoCompleteAction(bool isCancel = false)
        {
            jOnMapObjectSelectedSignal.RemoveListener(OnSelectBuilding);

            base.DoCompleteAction(isCancel);
        }

        public override void Activate()
        {
            base.Activate();

            jOnMapObjectSelectedSignal.AddListener(OnSelectBuilding);
            
            var shopView = jUIManager.GetView<BuildingsShopPanelView>();
            var chooseBuildingParams = new VeilParams(-0.2f, 0f, -0.2f, 0f)
            {
                PointerMode = PointerMode.Drag, DragTargetObject = shopView.gameObject
            };
            
            buildingItem = FindObjectInShop(shopView, TargetBuildingTypeId);
            buildingItem.BuyBuildingButton.interactable = false;

            SwitchHighlight(buildingItem.gameObject, true, chooseBuildingParams);
        }

        public override void Deactivate()
        {
            base.Deactivate();
            buildingItem.BuyBuildingButton.interactable = true;
            jOnMapObjectSelectedSignal.RemoveListener(OnSelectBuilding);
        }

        private BuildingItem FindObjectInShop(BuildingsShopPanelView buildingsShopPanelView, Id_IMapObjectType typeId)
        {
            foreach (var pullableObject in buildingsShopPanelView.BuildingsContainer.CurrentItems)
            {
                var item = (BuildingItem)pullableObject;

                if (item.CurrentMapObject.ObjectTypeID == typeId)
                {
                    buildingsShopPanelView.ScrollToAndLock(item);
                    return item;
                }
            }
            
            return null;
        }
    }
}