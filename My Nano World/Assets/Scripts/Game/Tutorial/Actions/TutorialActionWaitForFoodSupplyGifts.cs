﻿using System;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.UI.Views.Popups;
using NanoReality.Engine.UI.Views.WorldSpaceUI;
using NanoReality.Game.Attentions;
using NanoReality.Game.CitizenGifts;

namespace NanoReality.Game.Tutorial
{
    public sealed class TutorialActionWaitForFoodSupplyGifts : TutorialAction
    {
        [Inject] public ICityGiftsService jCityGiftService { get; set; }  

        public override void Activate()
        {
            base.Activate();
            jCityGiftService.LoadAvailableGifts(null);
            jCityGiftService.GiftsUpdates += OnGiftsUpdated;
        }

        public override void Deactivate()
        {
            base.Deactivate();
            jCityGiftService.GiftsUpdates -= OnGiftsUpdated;
        }

        private void OnGiftsUpdated()
        {
            DoCompleteAction();
        }
    }
}