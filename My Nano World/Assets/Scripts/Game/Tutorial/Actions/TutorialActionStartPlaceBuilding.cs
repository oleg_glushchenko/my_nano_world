﻿using System;
using NanoLib.Services.InputService;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using UnityEngine;

namespace NanoReality.Game.Tutorial
{
    public sealed class TutorialActionStartPlaceBuilding : TutorialAction
    {
        [Inject] public SignalMoveMapObjectView jSignalMoveMapObjectView { get; set; }   
        
        [Inject] public SignalOnReleaseTouch jTouchReleaseSignal { get; set; }

        private Id<IBusinessSector> _sectorType;
        private int _buildingTypeId;
        private Vector2 _position;

        private bool NeedCheckBuildingPosition => _position != default;

        private bool _mapObjectInValidPosition;
        
        public void Set(BusinessSectorsTypes sectorType, int buildingTypeId, Vector2 mapPosition = default(Vector2))
        {
            _sectorType = sectorType.GetBusinessSectorId();
            _buildingTypeId = buildingTypeId;
            _position = mapPosition;
        }

        public override void Activate()
        {
            base.Activate();
            jSignalMoveMapObjectView.AddListener(OnMapObjectMove);
            jTouchReleaseSignal.AddListener(OnTouchReleased);
        }

        private void OnTouchReleased()
        {
            
        }

        public override void Deactivate()
        {
            base.Deactivate();
            jSignalMoveMapObjectView.RemoveListener(OnMapObjectMove);
            jTouchReleaseSignal.RemoveListener(OnTouchReleased);
        }
        
        private void OnMapObjectMove(Vector2Int newPos, IMapObject mapObjectModel, Action callback = null)
        {
            HideLookers();

            if (mapObjectModel == null)
            {
                return;
            }

            if (mapObjectModel.SectorId != _sectorType || mapObjectModel.ObjectTypeID != _buildingTypeId)
            {
                return;
            }
            
            if (NeedCheckBuildingPosition && newPos != _position)
            {
                return;
            }
                
            DoCompleteAction();
        }
    }
}