﻿using NanoReality.GameLogic.Orders;

namespace NanoReality.Game.Tutorial
{
    public class TutorialActionCollectCityOrderReward : TutorialAction
    {
        [Inject] public SignalOnCityOrderAwardTaken jSignalOnCityOrderAwardTaken { get; set; }

        #region Overrides of TutorialAction

        public override void Activate()
        {
            base.Activate();
            
            jSignalOnCityOrderAwardTaken.AddListener(OnCityOrderAwardTaken);
        }

        public override void DoCompleteAction(bool isCancel = false)
        {
            base.DoCompleteAction(isCancel);
            
            jSignalOnCityOrderAwardTaken.RemoveListener(OnCityOrderAwardTaken);
        }

        #endregion

        private void OnCityOrderAwardTaken()
        {
            DoCompleteAction();
        }
    }
}
