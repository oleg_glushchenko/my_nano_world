﻿using System;
using Assets.Scripts.Engine.UI.Views.Popups;

namespace NanoReality.Game.Tutorial
{
    public sealed class TutorialActionWaitForPopupClose : TutorialAction
    {
        [Inject] public PopupClosedSignal jPopupClosedSignal { get; set; }

        public Type TargetPopupType;

        public override bool PreActivateIsCompletedCheck()
        {
            return base.PreActivateIsCompletedCheck() || !jPopupManager.IsPopupActive(TargetPopupType);
        }

        public override void Activate()
        {
            base.Activate();
            jPopupClosedSignal.AddListener(OnHideTargetPopup);
        }

        public override void Deactivate()
        {
            base.Deactivate();
            jPopupClosedSignal.RemoveListener(OnHideTargetPopup);
        }

        private void OnHideTargetPopup(BasePopupView popupView)
        {
            if (popupView.GetType() == TargetPopupType)
            {
                DoCompleteAction();
            }
        }
    }
}