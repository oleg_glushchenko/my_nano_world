using System.Linq;
using Assets.NanoLib.Utilities;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Controllers;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.api.Mines;
using NanoLib.Core.Logging;
using NanoLib.UI.Core;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Controllers.Agricultural;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Production;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.MinePanel;

namespace NanoReality.Game.Tutorial
{
    public sealed class CollectProductionPanelProductTutorialAction : TutorialAction 
    {
        [Inject] public OnProductProduceAndCollectSignal jOnProductProduceAndCollectSignal { get; set; }
        [Inject] public SignalOnMineProduceSlotFinished jSignalOnMineProduceSlotFinished { get; set; }
        [Inject] public SignalMineShipSlot jSignalMineShipSlot { get; set; }

        [Inject] public IUIManager jUiManager { get; set; }

        private IProductionBuilding _building;
        private VeilParams _veilParams;

        public void Set(IProductionBuilding building)
        {
            _building = building;
        }

        public override bool PreActivateIsCompletedCheck()
        {
            if (base.PreActivateIsCompletedCheck()) return true;

            return !HasProductsToCollect();
        }

        public override void Activate()
        {
            base.Activate();
            
            jOnProductProduceAndCollectSignal.AddListener(OnProductProduceAndCollect);
            jSignalOnMineProduceSlotFinished.AddListener(OnProduceProduced);
            jSignalMineShipSlot.AddListener(OnMineShipSlot);
            
            _veilParams = new VeilParams(0, 50, 0, 50);
            
            HighlightProduct();
        }

        public override void Deactivate()
        {
            base.Deactivate();
            jOnProductProduceAndCollectSignal.RemoveListener(OnProductProduceAndCollect);
            jSignalOnMineProduceSlotFinished.RemoveListener(OnProduceProduced);
            jSignalMineShipSlot.RemoveListener(OnMineShipSlot);
        }

        private bool HasProductsToCollect()
        {
            return _building != null && (_building.HasProductsToCollect() || !_building.IsProductionEmpty());
        }

        private void OnProductProduceAndCollect(IMapObject mapObject, Id<Product> product, int amount)
        {
            if (mapObject == _building && !HasProductsToCollect())
            {
                DoCompleteAction();
            }
            else
            {
                HighlightProduct();
            }
        }
        
        private void OnProduceProduced(Id_IMapObject productionBuildingId)
        {
            if (_building.MapID == productionBuildingId)
            {
                HighlightProduct();
            }
        }
        
        private void OnMineShipSlot(Id_IMapObject productionBuildingId, IProduceSlot slot)
        {
            if (_building.MapID == productionBuildingId)
            {
                HighlightProduct();
            }
        }

        private void HighlightProduct()
        {
            IProduceSlot slot = FindProducedSlot();
            if (slot == null)
            {
                HideLookers();
                return;
            }

            ProduceSlotView produceSlotView;
            if (_building is IMineObject)
            {
                var minePanelView = jUiManager.GetView<MinePanelView>();
                produceSlotView = minePanelView.ProduceSlotViews.First(c => c.ProduceSlot == slot); 
            }
            else 
            {
                var factoryPanel = jUiManager.GetView<FactoryPanelView>();
                produceSlotView = factoryPanel.ProduceSlotViews.First(c => c.ProduceSlot == slot);
            }
                        
            SwitchHighlight(produceSlotView.CollectButton.gameObject, true, _veilParams);
        }
        
        private IProduceSlot FindProducedSlot()
        {
            var producedSlot = _building.CurrentProduceSlots.FirstOrDefault(c => c.IsFinishedProduce && !c.IsWaitingResponse);
            
            Logging.Log(LoggingChannel.Tutorial, $"First not collected slot. ID: {producedSlot?.SlotId}");

            return producedSlot;
        }
    }
}