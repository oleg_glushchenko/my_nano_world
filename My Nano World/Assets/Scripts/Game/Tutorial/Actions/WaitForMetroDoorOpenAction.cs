using System;
using NanoReality.Game.Tutorial;
using NanoReality.Game.UI;
using UniRx;
using UnityEngine;

namespace Game.Tutorial.HintTutorial
{
    public class WaitForMetroDoorOpenAction : TutorialAction
    {
        private MetroSlotItemModel _slotModel;
        private IDisposable _disposableCommand;

        public void SetMetroSlotModel(MetroSlotItemModel model)
        {
            _slotModel = model;
        }

        public override void Activate()
        {
            base.Activate();

            _disposableCommand = _slotModel.OpenDoorCommand.Subscribe(state =>
            {
                if (state)
                {
                    DoCompleteAction();
                }
            });
        }

        public override void DoCompleteAction(bool isCancel = false)
        {
            _disposableCommand.Dispose();
            base.DoCompleteAction(isCancel);
        }
        
    }
}