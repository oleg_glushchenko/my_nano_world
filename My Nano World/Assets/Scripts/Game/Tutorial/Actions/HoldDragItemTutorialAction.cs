﻿namespace NanoReality.Game.Tutorial
{
    public sealed class HoldDragItemTutorialAction : TutorialAction
    {
        private DraggableItem _targetItem;

        public void Init(DraggableItem targetItem)
        {
            _targetItem = targetItem;
        }

        public override void Activate()
        {
            base.Activate();
            _targetItem.OnItemClickSignal.AddListener(OnItemHold);
            _targetItem.DragStarted += OnDragStarted;
            SwitchHighlight(_targetItem.gameObject, true);
        }

        public override void Deactivate()
        {
            base.Deactivate();
            _targetItem.OnItemClickSignal.RemoveListener(OnItemHold);
            _targetItem.DragStarted -= OnDragStarted;
        }

        private void OnItemHold(DraggableItem item)
        {
            DoCompleteAction();
            _targetItem.CancelDrag();
        }
        
        private void OnDragStarted(DraggableItem obj)
        {
            _targetItem.CancelDrag();
        }
    }
}