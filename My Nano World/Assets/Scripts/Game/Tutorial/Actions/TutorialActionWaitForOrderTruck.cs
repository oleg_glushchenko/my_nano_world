﻿using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.UI.Views.Popups;
using NanoReality.GameLogic.Orders;

namespace NanoReality.Game.Tutorial
{
    public class TutorialActionWaitForOrderTruck : TutorialAction
    {
        [Inject]
        public SignalOnOrderTruckTripFinished jSignalOnOrderTruckTripFinished { get; set; }

        [Inject]
        public SignalOnShownPanel jSignalOnShownPanel { get; set; }

        private int _truckPosition;

        public void SetParams(int truckIndex)
        {
            _truckPosition = truckIndex;
        }

        #region Overrides of TutorialAction

        public override void Activate()
        {
            base.Activate();
            
            jSignalOnShownPanel.AddListener(OnShownPanel);
            jSignalOnOrderTruckTripFinished.AddListener(OnOrderTruckTripFinished);
        }

        public override void DoCompleteAction(bool isCancel = false)
        {
            base.DoCompleteAction(isCancel);
            
            jSignalOnShownPanel.RemoveListener(OnShownPanel);
            jSignalOnOrderTruckTripFinished.RemoveListener(OnOrderTruckTripFinished);
        }

        #endregion

        private void OnShownPanel(UIPanelView view)
        {
            var panel = view as PurchaseByPremiumPopupView;
            if (panel != null)
            {
                SwitchHighlight(panel.YesButton.gameObject, true);
            }
        }

        private void OnOrderTruckTripFinished(int truckIndex)
        {
            if (truckIndex == _truckPosition)
            {
                DoCompleteAction();
            }
        }
    }
}
