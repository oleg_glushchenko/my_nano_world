﻿using System;
using NanoReality.GameLogic.Services;

namespace NanoReality.Game.Tutorial
{
    public class TutorialActionVideoWatched : TutorialAction
    {
        [Inject] public IAdsService jAdsService { get; set; }

        public override bool PreActivateIsCompletedCheck()
        {
            if (ParentFunc != null && ParentFunc())
                return true;

            return !jAdsService.IsVideoLoaded;
        }

        public override void Activate()
        {
            base.Activate();
            throw new NotImplementedException();
        }

        private void OnVideoWatched()
        {
            DoCompleteAction();
        }
    }
}