﻿using System;
using Assets.NanoLib.UI.Core.Signals;
using NanoReality.Game.UI;

namespace NanoReality.Game.Tutorial
{
    public sealed class TutorialActionMessage : TutorialAction
    {
        [Inject] public WindowClosedSignal jWindowClosedSignal { get; set; }

        public override void Activate()
        {
            base.Activate();
            jWindowClosedSignal.AddListener(OnWindowClosed);
        }

        public override void Deactivate()
        {
            base.Deactivate();
            jWindowClosedSignal.RemoveListener(OnWindowClosed);
        }

        private void OnWindowClosed(Type closedWindowType)
        {
            if (closedWindowType == typeof(DialogPanelView))
            {
                DoCompleteAction();
            }
        }
    }
}