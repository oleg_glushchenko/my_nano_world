﻿using System;
using System.Collections;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoLib.Services.InputService;
using UniRx;
using UnityEngine.UI;

namespace NanoReality.Game.Tutorial
{
    public sealed class TutorialActionBuildingAttention : TutorialAction
    {
        public bool OnlyButton;
        public Type TargetPanelType;
        public MapObjectView TargetBuilding;
        
        private Button _attentionButton;
        private bool _isCompleted;
        
        [Inject] public SignalOnMapObjectViewTap jSignalOnMapObjectViewTap { get; set; }
        [Inject] public SignalOnShownPanel jSignalOnShownPanel { get; set; }

        public override bool PreActivateIsCompletedCheck()
        {
            return base.PreActivateIsCompletedCheck() || jUIManager.IsViewVisible(TargetPanelType);
        }

        public override void Activate()
        {
            base.Activate();

            Observable.FromCoroutine(WaitForAttentionButton).Subscribe();

            jSignalOnMapObjectViewTap.AddListener(OnMapObjectViewTap);
            jSignalOnShownPanel.AddListener(OnShownPanel);
        }

        public override void Deactivate()
        {
            base.Deactivate();

            if (_attentionButton)
                _attentionButton.onClick.RemoveListener(OnAttentionClicked);
            
            jSignalOnMapObjectViewTap.RemoveListener(OnMapObjectViewTap);
            jSignalOnShownPanel.RemoveListener(OnShownPanel);
        }
        
        public override void DoCompleteAction(bool isCancel = false)
        {
            HideLookers();
            base.DoCompleteAction(isCancel);
        }

        private IEnumerator WaitForAttentionButton()
        {
            while (true)
            {
                if (!TargetBuilding.AttentionComponent.IsShown)
                {
                    yield return null;
                    continue;
                }
                
                _attentionButton = TargetBuilding.AttentionComponent.AttentionView.AttentionButton;
                break;
            }

            if (OnlyButton)
                SwitchHighlight(_attentionButton.gameObject, true);
            
            TargetBuilding.AttentionComponent.AttentionView.SetButtonInteractability(true);
            _attentionButton.onClick.AddListener(OnAttentionClicked);
        }

        private void OnMapObjectViewTap(MapObjectView mapObjectView)
        {
            if (MapObjectView != null && mapObjectView == MapObjectView && IsDialogEnabled)
                HideLookers();
        }
        
        private void OnAttentionClicked()
        {
            CompleteAction();
        }

        private void OnShownPanel(UIPanelView obj)
        {
            if (obj.GetType() == TargetPanelType)
                CompleteAction();
        }

        private void CompleteAction()
        {
            if (_isCompleted)
                return;

            _isCompleted = true;
            DoCompleteAction();
        }
    }
}