﻿using System;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.UI.Views.Popups;

namespace NanoReality.Game.Tutorial
{
    public sealed class TutorialActionWaitForPanelClose : TutorialAction
    {
        [Inject] public SignalOnHidePanel SignalOnHidePanel { get; set; }

        public Type TargetPanelType;

        public override bool PreActivateIsCompletedCheck() =>
            base.PreActivateIsCompletedCheck() || !jUIManager.IsViewVisible(TargetPanelType);

        public override void Activate()
        {
            base.Activate();
            SignalOnHidePanel.AddListener(OnHideTargetPanel);
        }

        public override void Deactivate()
        {
            base.Deactivate();
            SignalOnHidePanel.RemoveListener(OnHideTargetPanel);
        }

        private void OnHideTargetPanel(UIPanelView view)
        {
            if (view.GetType() == TargetPanelType)
            {
                DoCompleteAction();
            }
        }
    }
}