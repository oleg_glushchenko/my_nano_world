﻿using System;
using Assets.Scripts.Engine.UI.Views.Popups;
using NanoReality.Game.UI;
using UnityEngine.UI;

namespace NanoReality.Game.Tutorial
{
    public class TutorialActionWaitForPopupShow : TutorialAction
    {
        #region Fields

        public Type TargetPopupType;

        #endregion

        [Inject] public PopupOpenedSignal jPopupOpenedSignal { get; set; }
        
        private Button _button;

        #region Methods

        public override bool PreActivateIsCompletedCheck()
        {
            return base.PreActivateIsCompletedCheck() || jPopupManager.IsPopupActive(TargetPopupType);
        }

        public override void Activate()
        {
            base.Activate();

            if (ObjectToHighlight != null)
            {
                _button = ObjectToHighlight.GetComponent<Button>();
                if (_button != null)
                {
                    _button.onClick.AddListener(OnButtonClick);
                }
            }
            
            jPopupOpenedSignal.AddListener(OnShownPopup);
        }

        public override void Deactivate()
        {
            base.Deactivate();
            
            jPopupOpenedSignal.RemoveListener(OnShownPopup);
            
            if (_button != null)
            {
                _button.onClick.RemoveListener(OnButtonClick);
            }
        }

        private void OnButtonClick()
        {
            HideLookers();
            jHideWindowSignal.Dispatch(typeof(DialogPanelView));
        }

        private void OnShownPopup(BasePopupView popupView)
        {
            if (popupView.GetType() == TargetPopupType)
                DoCompleteAction();
        }

        #endregion
    }
}