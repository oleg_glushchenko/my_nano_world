﻿using Assets.NanoLib.UI.Core.Signals;
using NanoReality.GameLogic.PlayerProfile.Models.Api;

namespace NanoReality.Game.Tutorial
{
    public sealed class TutorialActionWaitForLevelUp : TutorialAction
    {
        public int MinExpectedLevel;
        
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public UserLevelUpSignal jUserLevelUpSignal { get; set; }

        public override bool PreActivateIsCompletedCheck()
        {
            return base.PreActivateIsCompletedCheck() || IsFinished();
        }

        public override void Activate()
        {
            base.Activate();

            jUserLevelUpSignal.AddListener(OnLevelUp);
        }

        public override void Deactivate()
        {
            base.Deactivate();

            jUserLevelUpSignal.RemoveListener(OnLevelUp);
        }

        private void OnLevelUp()
        {
            if (!IsFinished())
                return;
            
            HideLookers();
            DoCompleteAction();
        }
        
        private bool IsFinished()
        {
            return jPlayerProfile.CurrentLevelData.CurrentLevel + 1 >= MinExpectedLevel;
        }
    }
}