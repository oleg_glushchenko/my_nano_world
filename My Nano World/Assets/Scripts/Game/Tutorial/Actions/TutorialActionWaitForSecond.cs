using Assets.NanoLib.UtilitesEngine;

namespace NanoReality.Game.Tutorial
{
    public class TutorialActionWaitForSecond : TutorialAction
    {
        public float Seconds;
        private ITimer _timer;

        public override void Activate()
        {
            base.Activate();
            _timer = jTimerManager.StartRealTimer(Seconds, OnFinishTimer);
        }

        private void OnFinishTimer()
        {
            _timer.CancelTimer();
            _timer = null;
            DoCompleteAction();
        }

        public override string ToString()
        {
            return $"{base.ToString()} WaitingTime:{Seconds}s";
        }
    }
}