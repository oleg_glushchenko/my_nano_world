﻿using UnityEngine;
using System.Collections.Generic;
using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.UI.Views.ConstractConfirmPanel;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Utilites;

namespace NanoReality.Game.Tutorial
{
    public sealed class TutorialActionBuildRoad : TutorialAction
    {
        [Inject] public SignalTutorialRoadCreate jRoadCreateSignal { get; set; }
        [Inject] public SignalRemoveMapObjectView jRoadRemoveSignal { get; set; }
        [Inject] public SignalOnConfirmConstructSucceed jSignalOnConfirmConstructSucceed { get; set; }
        [Inject] public SignalOnMapObjectRemoved SignalOnMapObjectRemoved { get; set; }

        public List<Vector2> RequiredPositions;
        
        private List<Vector2> _currentPositions;

        private bool _isValidPosition;

        private ConstractConfirmPanelView ConfirmConstructPanel => jUIManager.GetView<ConstractConfirmPanelView>();

        public override bool PreActivateIsCompletedCheck()
        {
            if (ParentFunc != null && ParentFunc())
                return true;

            _currentPositions = new List<Vector2>();
            
            var mapView = jGameManager.GetMapView(SectorID.GetBusinessSectorId());

            foreach (var needPosition in RequiredPositions)
            {
                var needPositionFilledWithRoad = false;

                foreach (var mapObjectView in mapView.Objects)
                {
                    if (mapObjectView.MapObjectModel != null && mapObjectView.MapObjectModel.MapObjectType == MapObjectintTypes.Road)
                    {
                        if ((int)mapObjectView.GridPosition.x == needPosition.GetIntX() && (int)mapObjectView.GridPosition.y == needPosition.GetIntY())
                        {
                            if (!_currentPositions.Contains(mapObjectView.GridPosition))
                                _currentPositions.Add(mapObjectView.GridPosition);
                            needPositionFilledWithRoad = true;
                            break;
                        }
                    }
                }

                if (needPositionFilledWithRoad == false)
                    return false;
            }

            return true;
        }

        public override void Activate()
        {
            base.Activate();
            jRoadCreateSignal.AddListener(OnRoadWasCreated);
            jRoadRemoveSignal.AddListener(OnRoadWasRemoved);
            SignalOnMapObjectRemoved.AddListener(OnRoadWasRemoved);
            jSignalOnConfirmConstructSucceed.AddListener(OnConstructConfirmed);

            jConstructConfirmPanelModel.isAcceptButtonEnabled.SetValueAndForceNotify(false);
            jConstructConfirmPanelModel.isCancelButtonEnabled.SetValueAndForceNotify(false);
        }

        public override void Deactivate()
        {
            base.Deactivate();
            jRoadCreateSignal.RemoveListener(OnRoadWasCreated);
            jRoadRemoveSignal.RemoveListener(OnRoadWasRemoved);
            SignalOnMapObjectRemoved.RemoveListener(OnRoadWasRemoved);
            jSignalOnConfirmConstructSucceed.RemoveListener(OnConstructConfirmed);
        }

        private void OnRoadWasCreated(RoadMapObjectView obj)
        {
            if (!_currentPositions.Contains(obj.GridPosition))
                _currentPositions.Add(obj.GridPosition);

            CheckPositions();
        }

        private void OnRoadWasRemoved(IMapObject obj)
        {
            if (_currentPositions.Contains(obj.MapPosition.ToVector2()))
                _currentPositions.Remove(obj.MapPosition.ToVector2());

            CheckPositions();
        }

        private void CheckPositions()
        {
            if (_currentPositions.Count != RequiredPositions.Count)
            {
                _isValidPosition = false;
                jSignalOnReleaseTouch.RemoveListener(WaitForConfirmConstruct);
            }
            else
            {
                _isValidPosition = true;
                jSignalOnReleaseTouch.AddListener(WaitForConfirmConstruct);
            }
        }

        private void WaitForConfirmConstruct()
        {
            if (!_isValidPosition)
            {
                return;
            }
            
            IsLookerCollidersActive = true;
            
            jSignalOnReleaseTouch.RemoveListener(WaitForConfirmConstruct);

            jConstructConfirmPanelModel.isAcceptButtonEnabled.SetValueAndForceNotify(true);

            jTutorialMapObjects.Hide();
            SwitchHighlight(ConfirmConstructPanel.ConfirmButton.gameObject, true);
        }

        private void OnConstructConfirmed()
        {
            DoCompleteAction();
        }
    }
}
