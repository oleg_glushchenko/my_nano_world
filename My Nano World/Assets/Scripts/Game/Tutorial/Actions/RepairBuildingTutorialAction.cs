﻿using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Common.Controllers.Signals;

namespace NanoReality.Game.Tutorial
{
    public sealed class RepairBuildingTutorialAction : TutorialAction
    {
        public Id_IMapObjectType MapObjectTypeToRepair;

        #region Inject

        [Inject] public SignalOnBuildingRepaired jSignalOnBuildingRepaired { get; set; }

        #endregion

        #region TutorialAction implementation

        public override void Activate()
        {
            base.Activate();
            jSignalOnBuildingRepaired.AddListener(OnRepairBuilding);
        }

        public override void Deactivate()
        {
            base.Deactivate();
            jSignalOnBuildingRepaired.RemoveListener(OnRepairBuilding);
        }

        #endregion

        public override bool PreActivateIsCompletedCheck()
        {
            if (ParentFunc != null && ParentFunc())
            {
                return true;
            }

            var buildingModel = jGameManager.FindMapObjectByTypeId<IMapBuilding>(MapObjectTypeToRepair);
            return buildingModel != null && !buildingModel.IsLocked;
        }

        private void OnRepairBuilding(Id_IMapObjectType mapObjectTypeId, Id_IMapObject mapObjectId)
        {
            if (mapObjectTypeId == MapObjectTypeToRepair)
                DoCompleteAction();
        }
    }
}