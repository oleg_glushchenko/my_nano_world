using NanoReality.Game.Services.OrderDesk.SeaportOrderDesk;
using NanoReality.GameLogic.Orders;

namespace NanoReality.Game.Tutorial
{
    public sealed class CompleteSeaportOrderTutorialAction : TutorialAction
    {
        public int TargetOrderId { get; private set; }
        
        [Inject] public SignalSeaportOrderSent jSignalSeaportOrderSent { get; set; }

        public override void Activate()
        {
            base.Activate();
            jSignalSeaportOrderSent.AddListener(OnSeaportOrderComplete);
        }

        public override void Deactivate()
        {
            base.Deactivate();
            jSignalSeaportOrderSent.RemoveListener(OnSeaportOrderComplete);
        }

        public CompleteSeaportOrderTutorialAction SetTargetOrderId(int orderId)
        {
            TargetOrderId = orderId;
            return this;
        }

        private void OnSeaportOrderComplete(ISeaportOrderSlot obj)
        {
            if (obj?.OrderId == TargetOrderId)
            {
                DoCompleteAction();
            }
        }
        
        public override string ToString()
        {
            return $"{base.ToString()} TargetOrderId:{TargetOrderId}";
        }
    }
}