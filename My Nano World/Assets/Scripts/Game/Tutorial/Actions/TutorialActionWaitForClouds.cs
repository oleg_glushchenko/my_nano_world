﻿using NanoReality.Engine.UI.Extensions.LoaderPanel;
using NanoReality.GameLogic.StartApplicationSequence.Controllers.Signals;

namespace NanoReality.Game.Tutorial
{
    public class TutorialActionWaitForClouds : TutorialAction
    {
        [Inject] public PostLoadingActionsCompleteSignal jPostLoadingActionsCompleteSignal { get; set; }

        public override bool PreActivateIsCompletedCheck()
        {
            if (ParentFunc != null && ParentFunc())
                return true;

            if (!jUIManager.IsViewVisible(typeof(LoaderPanelView)))
            {
                return true;
            }

            return false;
        }

        public override void Activate()
        {
            base.Activate();

            jPostLoadingActionsCompleteSignal.AddOnce(OnReadyToCompleteAction);
        }

        private void OnReadyToCompleteAction()
        {
            DoCompleteAction();
        }
    }
}