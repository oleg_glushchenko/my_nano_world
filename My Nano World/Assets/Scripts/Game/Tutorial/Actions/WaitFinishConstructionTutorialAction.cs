﻿using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using UnityEngine;

namespace NanoReality.Game.Tutorial
{
    public class WaitFinishConstructionTutorialAction : TutorialAction
    {
        [Inject] public SignalBuildFinishVerificated jSignalBuildFinishVerificated { get; set; }

        private BusinessSectorsTypes _sectorType;
        private int _buildingTypeId;
        private Vector2 _position;
        
        /// <summary>
        /// InitConstructionData
        /// </summary>
        /// <param name="sectorType"></param>
        /// <param name="buildingTypeId"></param>
        /// <param name="constructionPosition">If default(Vector2), action will not check building position.</param>
        public void InitConstructionData(BusinessSectorsTypes sectorType, int buildingTypeId, Vector2 constructionPosition = default)
        {
            _sectorType = sectorType;
            _buildingTypeId = buildingTypeId;
            _position = constructionPosition;
        }

        public override void Activate()
        {
            base.Activate();

            var mapObject = jGameManager.FindMapObjectByTypeId<IMapObject>(_buildingTypeId);

            if (IsMapObjectConstructed(mapObject))
            {
                DoCompleteAction();
                return;
            }
            
            jSignalBuildFinishVerificated.AddListener(OnMapObjectBuildFinished);
        }

        private void OnMapObjectBuildFinished(IMapObject mapObject)
        {
            if (IsMapObjectConstructed(mapObject))
            {
                jSignalBuildFinishVerificated.RemoveListener(OnMapObjectBuildFinished);
                
                DoCompleteAction();
            }
        }

        private bool IsMapObjectConstructed(IMapObject mapObject)
        {
            if (mapObject == null)
            {
                return false;
            }

            if (mapObject.SectorId != _sectorType.GetBusinessSectorId())
            {
                return false;
            }

            if (mapObject.ObjectTypeID != _buildingTypeId)
            {
                return false;
            }

            bool needCheckPosition = _position != default(Vector2);
            if (needCheckPosition && mapObject.MapPosition != _position.ToVector2F())
            {
                return false;
            }

            if (!mapObject.IsConstructed)
            {
                return false;
            }

            return true;
        }
    }
}
