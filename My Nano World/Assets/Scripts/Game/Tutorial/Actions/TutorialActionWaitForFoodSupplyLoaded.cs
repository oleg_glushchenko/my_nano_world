﻿using NanoReality.Game.FoodSupply;

namespace NanoReality.Game.Tutorial
{
    public sealed class TutorialActionWaitForFoodSupplyLoaded : TutorialAction
    {
        [Inject] public IFoodSupplyService jFoodSupplyService { get; set; }
        
        public override void Activate()
        {
            base.Activate();
            
            if (jFoodSupplyService.IsDataLoaded)
            {
                DoCompleteAction();
                return;
            }

            jFoodSupplyService.FoodSupplyCityInfoChanged += OnFoodSupplyLoaded;
        }

        public override void Deactivate()
        {
            base.Deactivate();
            jFoodSupplyService.FoodSupplyCityInfoChanged -= OnFoodSupplyLoaded;
        }

        private void OnFoodSupplyLoaded()
        {
            DoCompleteAction();
        }
    }
}