﻿using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Controllers;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.api.Mines;
using NanoReality.Engine.UI;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.GameLogic.MinePanel;
using NanoReality.Game.Tutorial;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Controllers.Factories;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Production;

public class TutorialActionFinishFactoryOrMineProduction : TutorialAction
{
    [Inject]
    public SignalFactoryProduceSlotFinished jSignalFactoryProduceSlotFinished { get; set; }

    /// <summary>
    /// Вызывается при начале разработки продукта
    /// </summary>
    [Inject]
    public SignalOnMineProduceSlotFinished jSignalOnMineProduceSlotFinished { get; set; }
    
    [Inject]
    public SignalOnShownPanel jSignalOnShownPanel { get; set; }

    public IProductionBuilding ProductionBuilding;
    
    public PointerMode HighlightPointerMode = PointerMode.Invisible;

    #region Overrides of TutorialAction

    public override bool PreActivateIsCompletedCheck()
    {
        if (base.PreActivateIsCompletedCheck())
            return true;

        return ProductionBuilding != null && ProductionBuilding.HasProductsToCollect();
    }

    public override void Activate()
    {
        base.Activate();

        jSignalOnShownPanel.AddListener(OnPanelShown);
        jSignalFactoryProduceSlotFinished.AddListener(OnFinished);
        jSignalOnMineProduceSlotFinished.AddListener(OnFinished);

        if (ProductionBuilding is IMineObject)
        {
            var minePanel = jUIManager.GetView<MinePanelView>();
            if (!minePanel.Visible)
            {
                minePanel.Show();
            }
            else
            {
                HighlightSkipButton();
            }
        }
        else if (ProductionBuilding is IFactoryObject)
        {
            var factoryPanelView = jUIManager.GetView<FactoryPanelView>();
            if (!factoryPanelView.Visible)
            {
                factoryPanelView.Show();
            }
            else
            {
                HighlightSkipButton();
            }
        }
    }

    public override void DoCompleteAction(bool isCancel = false)
    {
        jUIManager.GetView<MinePanelView>().Hide();
        jUIManager.GetView<FactoryPanelView>().Hide();

        jSignalOnShownPanel.RemoveListener(OnPanelShown);
        jSignalFactoryProduceSlotFinished.RemoveListener(OnFinished);
        jSignalOnMineProduceSlotFinished.RemoveListener(OnFinished);
        
        base.DoCompleteAction(isCancel);
    }

    #endregion

    private void HighlightSkipButton()
    {
        ProduceSlotView slotView = null;
        if (ProductionBuilding is IMineObject)
        {
            var minePanel = jUIManager.GetView<MinePanelView>();
            if (minePanel.Visible)
            {
                slotView = minePanel.FirstSlotView;
            }
        }
        else if (ProductionBuilding is IFactoryObject)
        {
            var factoryPanelView = jUIManager.GetView<FactoryPanelView>();
            if (factoryPanelView.Visible)
            {
                slotView = factoryPanelView.FirstSlotView;
            }
        }

        if (slotView != null)
        {
            SwitchHighlight(slotView.SkipButton.gameObject, true, new VeilParams
            {
                PointerMode = HighlightPointerMode
            });
        }
    }

    private void OnPanelShown(UIPanelView view)
    {
        if (view is MinePanelView || view is FactoryPanelView)
        {
            HighlightSkipButton();
        }
    }
    
    private void OnFinished(Id_IMapObject mapID)
    {
        if (ProductionBuilding.MapID == mapID)
        {
            DoCompleteAction();
        }
    }
}
