﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.UI.Extensions.Agricultural;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects;
using UnityEngine;

namespace NanoReality.Game.Tutorial
{
    public class TutorialActionStartHarvestCrops : TutorialAction
    {
        private IEnumerable<IAgroFieldBuilding> _targetAgrofields;
        private List<IAgroFieldBuilding> _harvestedAgrofields;

        private readonly Type _agrofieldPanelType = typeof(AgroFieldPanelView);
        private IAgroFieldBuilding _targetAgrofield;
        
        [Inject] public SignalOnShownPanel jSignalOnShownPanel { get; set; }
        
        public void SetBuildingModel(IEnumerable<IAgroFieldBuilding> model)
        {
            _targetAgrofields = model;
        }

        #region Overrides of TutorialAction

        public override bool PreActivateIsCompletedCheck()
        {
            if (ParentFunc != null && ParentFunc())
            {
                return true;
            }
            if (_targetAgrofields == null)
            {
                return false;
            }

            return _targetAgrofields.All(c => c.CurrentState == AgriculturalFieldState.Empty);
        }

        public override void Activate()
        {
            base.Activate();
            _harvestedAgrofields = new List<IAgroFieldBuilding>();
            foreach (IAgroFieldBuilding agroFieldBuilding in _targetAgrofields.Where(c => c.CurrentState == AgriculturalFieldState.Empty))
            {
                _harvestedAgrofields.Add(agroFieldBuilding);
            }
            
            ProcessUi();
        }

        public override void DoCompleteAction(bool isCancel = false)
        {
            base.DoCompleteAction(isCancel);
            jTutorialMapObjects.Hide();
            HideLookers();
        }

        #endregion

        private void ProcessUi()
        {
            if (jUIManager.IsViewVisible(_agrofieldPanelType))
            {
                var view = jUIManager.GetView<AgroFieldPanelView>();
                var veilParams = new VeilParams
                {
                    PointerMode = PointerMode.Drag,
                    DragTargetObject = view.gameObject,
                };

                DraggableProductItem draggableItem = view.CropItem;
                GameObject draggableParent = draggableItem.transform.parent.gameObject;

                SwitchHighlight(draggableParent, true, veilParams);
        
                draggableItem.DragEnded += OnItemDragEnded;
                
                view.OnHide.AddListener(OnAgrofieldPanelHide);
                HarvestNextAgrofield();
            }
            else
            {
                IAgroFieldBuilding targetAgrofield = GetNextAgrofield();;
                if (targetAgrofield == null)
                {
                    return;
                }
                MapObjectView targetAgrofieldView = jGameManager.GetMapObjectView(targetAgrofield);
                SwitchHighlight(targetAgrofieldView.gameObject, true);
                SetAllBuildingsInteractable(false);
                targetAgrofieldView.Interactable = true;
                jSignalOnShownPanel.AddListener(OnPanelShown);
            }
        }
        
        private void HarvestNextAgrofield()
        {
            var plantedAgrofield = GetNextAgrofield();

            if (plantedAgrofield == null)
            {
                jUIManager.GetView<AgroFieldPanelView>().CropItem.DragEnded -= OnItemDragEnded;
                DoCompleteAction();
                return;
            }

            jTutorialMapObjects.ShowBuildPlace(BusinessSectorsTypes.Farm, plantedAgrofield.MapPosition.ToVector2(),
                plantedAgrofield.Dimensions.ToVector2());
            
            plantedAgrofield.OnHarvestStarted.AddOnce(() => OnHarwest(plantedAgrofield));
        }

        private void OnPanelShown(UIPanelView targetPanel)
        {
            if (targetPanel.GetType() == typeof(AgroFieldPanelView))
            {
                ProcessUi();
                jSignalOnShownPanel.RemoveListener(OnPanelShown);
            }
        }

        private void OnAgrofieldPanelHide(UIPanelView agrofieldPanel)
        {
            agrofieldPanel.OnHide.RemoveListener(OnAgrofieldPanelHide);
            ProcessUi();
        }

        private void OnHarwest(IAgroFieldBuilding agrofield)
        {
            _harvestedAgrofields.Add(agrofield);
            HideLookers();
            HarvestNextAgrofield();
        }

        private void OnItemDragEnded(DraggableItem obj)
        {
            obj.DragEnded -= OnItemDragEnded;
            if (_targetAgrofields.Count() == _harvestedAgrofields.Count)
            {
                DoCompleteAction();
            }
        }

        private IAgroFieldBuilding GetNextAgrofield()
        {
            return _targetAgrofields.FirstOrDefault(c => !_harvestedAgrofields.Contains(c));
        }
    }
}