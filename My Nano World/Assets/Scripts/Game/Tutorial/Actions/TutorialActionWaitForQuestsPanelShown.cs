﻿using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using NanoReality.Engine.UI.Extensions.QuestsBuildingPanel;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;

namespace NanoReality.Game.Tutorial
{
    public class TutorialActionWaitForQuestsPanelShown : TutorialAction
    {
        [Inject]
        public SignalOnShownPanel jSignalOnShownPanel { get; set; }

        
        private BusinessSectorsTypes _sectorType;

        public void SetTarget(BusinessSectorsTypes sectorType)
        {
            _sectorType = sectorType;
        }

        public override bool PreActivateIsCompletedCheck()
        {
            // если есть родительская функция, и она выполнена, игнорируя условия
            // автоматически засчитываем это действие как выполненое
            if (ParentFunc != null && ParentFunc())
                return true;

            return false; //_panelView.Visible && _panelView.SectorType == _sectorType;
        }

        public override void Activate()
        {
            base.Activate();

            jSignalOnShownPanel.AddListener(OnShownPanel);
        }

        private void OnShownPanel(UIPanelView view)
        {
            //TODO: add new code
        }
    }
}
