﻿using strange.extensions.mediation.impl;
using UnityEngine;

namespace NanoReality.Game.Tutorial
{
    public class BaseTutorialMapObject : View
    {
        public static readonly Color ColorBorder = Color.yellow;
        public static readonly Color ColorArea = ColorBorder * 0.4f; 
        
        [SerializeField] protected GameObject _gameObject;
        
        #region API

        public void Hide()
        {
            _gameObject?.SetActive(false);
        }
        
        #endregion
    }
}