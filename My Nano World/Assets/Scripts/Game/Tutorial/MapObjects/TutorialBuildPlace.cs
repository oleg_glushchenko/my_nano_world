﻿using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.Settings.BuildSettings.Models.Api;
using UnityEngine;

namespace NanoReality.Game.Tutorial
{
	public class TutorialBuildPlace : BaseTutorialMapObject
	{
		[SerializeField] private Transform _highlightAreaTransform;
		[SerializeField] private MeshFilter _meshFilterPulsingBorder;
		
		[Inject] public ICityMapSettings jCityMapSettings { get; set; }
		[Inject] public IGameManager jGameManager { get; set; }

		private Vector2 _currentGridPosition;
		private Vector2 _currentAreaGridDimensions;
		private BusinessSectorsTypes _currentSector;
		
		public GameObject GetPulsingBorder()
		{
			return _meshFilterPulsingBorder.gameObject;
		}

		public void Show(BusinessSectorsTypes sectorType, Vector2 gridPosition, Vector2 gridDimensions)
		{
			_currentSector = sectorType;
			_currentGridPosition = gridPosition;
			_currentAreaGridDimensions = gridDimensions;
			
			ActivateAndUpdatePositions(gridPosition);
		}
		
		#region privates
		
		private void ActivateAndUpdatePositions(Vector2 sectorGridPosition)
		{
			_gameObject.SetActive(true);

			Vector2 scaledGripPosition = sectorGridPosition * jCityMapSettings.GridCellScale;
			
			Vector3 worldSpacePosition = new Vector3(scaledGripPosition.x, 0, scaledGripPosition.y);
			var sectorView = jGameManager.GetMapView(_currentSector.GetBusinessSectorId());
			Vector3 sectorOffset = sectorView.transform.position;

			Vector2 areaScale = _currentAreaGridDimensions * jCityMapSettings.GridCellScale;	
			Vector3 areaScaleWorldSpace = new Vector3(areaScale.x, 1, areaScale.y);
			_highlightAreaTransform.localScale = areaScaleWorldSpace;
			_highlightAreaTransform.localPosition = new Vector3(areaScaleWorldSpace.x / 2, 0, areaScaleWorldSpace.z / 2);
			transform.position = sectorOffset + worldSpacePosition;
			//_transformParticles.position = depthWorldPosition;
		}
		
//		private void UpdateParticles(Vector2F dimensions)
//		{
//			var shape = _particleSystem.shape;
//			shape.scale = new Vector3(dimensions.X, dimensions.Y, 0.1f);
//		}
		
		#endregion
	}
}