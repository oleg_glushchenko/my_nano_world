﻿using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.StrangeIoC;
using UnityEngine;

namespace NanoReality.Game.Tutorial
{
	public class TutorialMapObjects : AView
	{
		[SerializeField] private TutorialBuildPlace _tutorialBuildPlace;
		[SerializeField] private TutorialRoadPlace _tutorialRoadPlace;

		public GameObject GameObjectRoadStart => _tutorialRoadPlace.GameObjectStart;

		public GameObject GameObjectRoadEnd => _tutorialRoadPlace.GameObjectEnd;

		public GameObject GameObjectBuildPlace => _tutorialBuildPlace.GetPulsingBorder();

		public void ShowBuildPlace(BusinessSectorsTypes sectorType, Vector2 gridPosition, Vector2 dimensions)
		{
			SetActive(true);
			_tutorialBuildPlace.Show(sectorType, gridPosition, dimensions);
		}

		public void ShowRoadPlace(BusinessSectorsTypes citySector, Vector2 roadStartPosition, Vector2 roadEndPosition)
		{
			_tutorialRoadPlace.Show(citySector, roadStartPosition, roadEndPosition);
		}

		public void SetActive(bool isActive)
		{
			gameObject.SetActive(isActive);
		}
		
		public override void Hide()
		{
			base.Hide();
			_tutorialBuildPlace.Hide();
			_tutorialRoadPlace.Hide();
		}
	}
}