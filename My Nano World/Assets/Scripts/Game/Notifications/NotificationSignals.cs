﻿using strange.extensions.signal.impl;

namespace NanoReality.Game.Notifications
{
    public class CancelAllPushNotificationSignal : Signal {}
    public class NotificationInitSignal : Signal<bool> {}
}