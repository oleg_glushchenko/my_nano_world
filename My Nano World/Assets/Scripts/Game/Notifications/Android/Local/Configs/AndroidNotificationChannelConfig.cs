﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace NanoReality.Game.Notifications.Android.Local.Configs
{
    [CreateAssetMenu(fileName = "Channel", menuName = "NanoReality/Notifications/Android/Channel", order = 10)]
    public sealed class AndroidNotificationChannelConfig : SerializedScriptableObject
    {
        [SerializeField] private string _id;
        [SerializeField] private string _name;
        [SerializeField] private string _descrption;
        [SerializeField] private Importance _importance;

        public string Id => _id;
        public string Name => _name;
        public string Description => _descrption;
        public Importance Importance => _importance;
    }
    
    public enum Importance
    {
        /// <summary>
        /// A notification with no importance: does not show in the shade.
        /// </summary>
        None = 0,
        
        /// <summary>
        /// Low importance, notification is shown everywhere, but is not intrusive. 
        /// </summary>
        Low = 2,

        /// <summary> 
        /// Default importance, notification is shown everywhere, makes noise, but does not intrude visually.
        /// </summary>
        Default = 3,

        /// <summary>
        /// High importance, notification is shown everywhere, makes noise and is shown on the screen.
        /// </summary>
        High = 4,
    }
}
