﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace NanoReality.Game.Notifications.Android.Local.Configs
{
    [CreateAssetMenu(fileName = "DataConfig", menuName = "NanoReality/Notifications/Android/DataConfig", order = 10)]
    public sealed class AndroidNotificationDataConfig : SerializedScriptableObject
    {
        [SerializeField] private string _title;
        [SerializeField] private string[] _descriptions;
        
        [Tooltip("Notification small icon" +
                 "It will be used to represent the notification in the status bar and content view (unless overridden there by a large icon)" +
                 "The icon PNG file has to be placed in the `/Assets/Plugins/Android/res/drawable` folder and it's name has to be specified without the extension.")]
        [SerializeField] private string _smallIconName;
        
        [Tooltip("Notification large icon." +
                 "Add a large icon to the notification content view. This image will be shown on the left of the notification view in place of the small icon (which will be placed in a small badge atop the large icon)." +
                 "The icon PNG file has to be placed in the `/Assets/Plugins/Android/res/drawable folder` and it's name has to be specified without the extension.")]
        [SerializeField] private string _largeIconName;

        /// <summary>
        /// The notification will be be repeated on every specified time interval.
        /// Do not set for one time notifications.
        /// </summary>
        [SerializeField] private TimeSpanInfo _repeatInterval;
        
        /// <summary>
        /// Apply a custom style to the notification.
        /// Currently only BigPicture and BigText styles are supported.
        /// </summary>
        [SerializeField] private NotificationStyle _style;

        /// <summary>
        /// Accent color to be applied by the standard style templates when presenting this notification.
        /// The template design constructs a colorful header image by overlaying the icon image (stenciled in white) atop a field of this color. Alpha components are ignored.
        /// </summary>
        [SerializeField] private  Color _color;
        
        public string Title => _title;
        public string[] Descriptions => _descriptions;
        public string SmallIconName => _smallIconName;
        public string LargeIconName => _largeIconName;
        public TimeSpan? RepeatInterval => _repeatInterval.TimeSpan;
        
        public NotificationStyle Style => _style;
        public Color Color => _color;
        
        [Serializable]
        public struct TimeSpanInfo
        {
            [SerializeField] private int seconds;
            [SerializeField] private int minutes;
            [SerializeField] private int hours;
            [SerializeField] private int days;
            
            public TimeSpan? TimeSpan
            {
                get
                {
                    if(seconds > 0 || minutes > 0 || hours > 0 || days > 0)
                        return new TimeSpan(days, hours, minutes, seconds);
                    
                    return null;
                }
            }
        }
        
        public enum NotificationStyle
        {
            /// <summary>
            /// Use the default style.
            /// </summary>
            None = 0,
        
            /// <summary>
            /// Generate a large-format notification that includes a lot of text.
            /// </summary>
            BigTextStyle = 2
        }
    }
}
