﻿using System;
using System.Collections.Generic;
using NanoReality.Game.Notifications.Android.Local.Configs;
using Sirenix.OdinInspector;
using UnityEngine;

namespace NanoReality.Game.Notifications.Android.Local.Settings
{
    [CreateAssetMenu(fileName = "Settings", menuName = "NanoReality/Notifications/Android/Settings", order = 10)]
    public class AndroidNotificationSettings : SerializedScriptableObject
    {
        [SerializeField] private List<AndroidNotificationConfig> _configs;

        public List<AndroidNotificationConfig> Configs => _configs;
    }
    
    [Serializable]
    public class AndroidNotificationConfig
    {
        [SerializeField] private LocalNotificationType _type;
        [SerializeField] private AndroidNotificationDataConfig _notification;
        [SerializeField] private AndroidNotificationChannelConfig _channel;

        public LocalNotificationType Type => _type;

        public AndroidNotificationDataConfig Notification => _notification;

        public AndroidNotificationChannelConfig Channel => _channel;
    }
}
