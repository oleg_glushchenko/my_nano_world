﻿#if UNITY_ANDROID
using System;
using System.Collections.Generic;
using NanoLib.Core.Logging;
using NanoReality.Game.Notifications.Android.Local.Configs;
using NanoReality.Game.Notifications.Android.Local.Settings;
using Unity.Notifications.Android;
using UnityEngine;

namespace NanoReality.Game.Notifications.Android.Services
{
    public class AndroidLocalNotificationService : LocalNotificationService
    {
        private AndroidNotificationSettings _settings;
        private AndroidNotificationChannel[] _channels;
        private readonly Dictionary<Type, Dictionary<int, int>> _identifiersByTypeAndId = new Dictionary<Type, Dictionary<int, int>>();

        public override void Init()
        {
            base.Init();

            AndroidNotificationCenter.OnNotificationReceived += OnNotificationReceived;

            _settings = Resources.Load<AndroidNotificationSettings>("Notifications/Android/AndroidNotificationSettings");

            _channels = new AndroidNotificationChannel[_settings.Configs.Count];
            CreateChannels();
            RegisterChannels();
        }

        public override void Dispose()
        {
            base.Dispose();

            AndroidNotificationCenter.OnNotificationReceived -= OnNotificationReceived;
        }

        protected override void SendNotification(NotificationData data)
        {
            var configs = _settings.Configs.Find(x => x.Type == data.Type);
            if (configs == null)
            {
                Logging.LogError(LoggingChannel.Notification, $"Type {data.Type} is not set");
                return;
            }

            var type = data.SourceType;
            var id = data.Id;

            if (_identifiersByTypeAndId.ContainsKey(type) && _identifiersByTypeAndId[type].ContainsKey(id))
                Cancel(type, id);

            var notification = CreateNotification(configs.Notification, data.FireTime, data.TitleParams, data.DescriptionParams);
            var identifier = AndroidNotificationCenter.SendNotification(notification, configs.Channel.Id);

            AddIdentifier(identifier, type, id);

            Logging.Log(LoggingChannel.Notification, $"Added notification: data.Id {id.ToString()} identifier {identifier.ToString()} FireTime {data.FireTime.ToString()} type {type}");
        }

        protected override void CancelAllDisplayed()
        {
            AndroidNotificationCenter.CancelAllDisplayedNotifications();
        }

        protected override void Cancel(Type type, int id)
        {
            if (!_identifiersByTypeAndId.ContainsKey(type))
            {
                Logging.LogWarning(LoggingChannel.Notification, $"There is no scheduled notifications with type {type}");
                return;
            }

            var identifiersById = _identifiersByTypeAndId[type];
            if (!identifiersById.ContainsKey(id))
            {
                Logging.LogWarning(LoggingChannel.Notification, $"There is no scheduled notifications with data id {id.ToString()}");
                return;
            }

            var identifier = identifiersById[id];
            identifiersById.Remove(id);

            Logging.LogWarning(LoggingChannel.Notification, $"data.Id {id.ToString()} identifier {identifier.ToString()} type {type}");
            AndroidNotificationCenter.CancelNotification(identifier);
        }

        protected override void CancelAll()
        {
            AndroidNotificationCenter.CancelAllNotifications();
        }

        protected override bool VerifyPlatform()
        {
#if (UNITY_ANDROID && !UNITY_EDITOR)
            return true;
#endif
            return false;
        }

        protected override bool OpenAppWithNotificationCheck()
        {
            return AndroidNotificationCenter.GetLastNotificationIntent() != null;
        }

        private void RegisterChannels()
        {
            Array.ForEach(_channels, AndroidNotificationCenter.RegisterNotificationChannel);
        }

        private void CreateChannels()
        {
            var i = 0;
            foreach (var configs in _settings.Configs)
            {
                var channel = new AndroidNotificationChannel
                {
                    Id = configs.Channel.Id,
                    Name = configs.Channel.Name,
                    Importance = (Unity.Notifications.Android.Importance) configs.Channel.Importance,
                    Description = configs.Channel.Description
                };

                _channels[i] = channel;
                i++;
            }
        }

        private static void OnNotificationReceived(AndroidNotificationIntentData data)
        {
            Logging.LogWarning(LoggingChannel.Notification, $"Notification received: Id {data.Id} Title {data.Notification.Title})");
            AndroidNotificationCenter.CancelNotification(data.Id);
        }

        private AndroidNotification CreateNotification(
            AndroidNotificationDataConfig config,
            int fireTime,
            IReadOnlyCollection<object> titleParams,
            IReadOnlyCollection<object> descriptionParams)
        {
            var notification = new AndroidNotification
            {
                Title = CreateNotificationTitle(config.Title, titleParams),
                Text = CreateNotificationDescription(config.Descriptions, descriptionParams),
                Style = (NotificationStyle) config.Style,
                Color = config.Color,
                RepeatInterval = config.RepeatInterval,
                ShouldAutoCancel = true,
                FireTime = DateTime.Now.AddSeconds(fireTime)
            };

            return notification;
        }

        private void AddIdentifier(int identifier, Type type, int dataId)
        {
            if (_identifiersByTypeAndId.ContainsKey(type))
            {
                var identifiersById = _identifiersByTypeAndId[type];
                if (identifiersById.ContainsKey(dataId))
                    identifiersById[dataId] = identifier;
                else
                    identifiersById.Add(dataId, identifier);
            }
            else
            {
                var identifiersById = new Dictionary<int, int> {{dataId, identifier}};
                _identifiersByTypeAndId.Add(type, identifiersById);
            }
        }
    }
}
#endif