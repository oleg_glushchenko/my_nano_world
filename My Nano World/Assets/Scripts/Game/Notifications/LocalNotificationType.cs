﻿namespace NanoReality.Game.Notifications
{
    public enum LocalNotificationType
    {
        None = 0,
        BuildingConstructed = 1,
        BuildingUpgraded = 2,
        // = 3,
        // = 4,
        // = 5,
        TimeToCook = 6,
        FoodIsReady = 7,
        Auction = 8,
        AuctionPurchase = 9,
        StorageFull = 10,
        FriendVisit = 11,
        LongAgoNotPlay = 12,
        ProductionComplete = 13,
        HarvestReady = 14,
        TaxesReady = 15
    }
}
