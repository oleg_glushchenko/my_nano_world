﻿using System;
using System.Collections.Generic;
using System.Linq;
using I2.Loc;
using NanoReality.GameLogic.Settings.GameSettings.Models.Api;

namespace NanoReality.Game.Notifications
{
    public abstract class LocalNotificationService : ILocalNotificationService
    {
        [Inject] public IGameSettings jGameSettings { get; set; }
        [Inject] public INotificationRegistrationService jNotificationRegistrationService { get; set; }
        
        [Inject] public CancelAllPushNotificationSignal jCancelAllPushNotificationSignal { get; set; }
        [Inject] public NotificationInitSignal jNotificationInitSignal { get; set; }

        protected abstract void Cancel(Type type, int id);
        protected abstract void SendNotification(NotificationData data);
        protected abstract bool VerifyPlatform();
        protected abstract void CancelAll();
        protected abstract void CancelAllDisplayed();
        protected abstract bool OpenAppWithNotificationCheck();
  
        [PostConstruct]
        public void PostConstruct()
        {
            CancelAllDisplayed();
        }

        public virtual void Init()
        {
            jNotificationRegistrationService.AddPushNotification += OnAddPushNotification;
            jNotificationRegistrationService.CancelPushNotification += OnCancelPushNotification;
            
            jCancelAllPushNotificationSignal.AddListener(OnCancelAllPushNotification);

            jNotificationInitSignal.Dispatch(OpenAppWithNotificationCheck());
        }

        public virtual void Dispose()
        {
            jNotificationRegistrationService.AddPushNotification -= OnAddPushNotification;
            jNotificationRegistrationService.CancelPushNotification -= OnCancelPushNotification;

            jCancelAllPushNotificationSignal.RemoveListener(OnCancelAllPushNotification);
        }

        protected string CreateNotificationTitle(string titleKey, IReadOnlyCollection<object> titleParams)
        {
            var title = GetStringByKey(titleKey);
            title = titleParams != null
                ? string.Format(title, titleParams.ToArray())
                : title;

            return ApplyLocalizationConverter(title);
        }

        protected string CreateNotificationDescription(string[] descriptionKeys, IReadOnlyCollection<object> descriptionParams)
        {
            var descriptionKey = descriptionKeys[UnityEngine.Random.Range(0, descriptionKeys.Length - 1)];
            var description = GetStringByKey(descriptionKey);

            description = descriptionParams == null ? description : string.Format(description, descriptionParams.ToArray());

            return ApplyLocalizationConverter(description);
        }
        
        protected string GetStringByKey(string key)
        {
            return LocalizationManager.GetTranslation ($"Notifications/{key}");
        }
        
        private void OnAddPushNotification(NotificationData data)
        {
            if (IsVerificationSuccessful())
                SendNotification(data);
        }

        private void OnCancelPushNotification(Type type, int id)
        {
            if(IsVerificationSuccessful())
                Cancel(type, id);
        }

        private bool IsVerificationSuccessful()
        {
            if (!VerifyPlatform())
                return false;

            if (!jGameSettings.IsPushNotificationsEnabled)
                return false;

            return true;
        }

        private void OnCancelAllPushNotification()
        {
            CancelAll();
        }

        private string ApplyLocalizationConverter(string text)
        {
            if (LocalizationMLS.Instance.IsRtlLanguage)
            {
                var reverseText = MultiLanguageSystem.Utility.ReverseText(text);
                return reverseText;
            }

            return text;
        }
    }
}
