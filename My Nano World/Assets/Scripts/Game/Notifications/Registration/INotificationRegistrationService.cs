﻿using System;
using NanoReality.Game.Services;

namespace NanoReality.Game.Notifications
{
	public interface INotificationRegistrationService : IGameService
	{
		event Action<NotificationData> AddPushNotification;
		event Action<Type, int> CancelPushNotification;
	}
}