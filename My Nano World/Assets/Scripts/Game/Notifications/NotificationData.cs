﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace NanoReality.Game.Notifications
{
    public struct NotificationData
    {
        public Type SourceType { get; }
        public int Id { get; }
        public LocalNotificationType Type { get; }
        public int FireTime { get; }

        public IReadOnlyCollection<object> TitleParams => Array.AsReadOnly(_titleParams);
        public IReadOnlyCollection<object> DescriptionParams => Array.AsReadOnly(_descriptionParams);

        private object[] _titleParams;
        private object[] _descriptionParams;
        

        public NotificationData(int id, Type sourceType, LocalNotificationType type, float fireTime)
        {
            Id = id;
            Type = type;
            FireTime = Mathf.FloorToInt(fireTime);
            _titleParams = null;
            _descriptionParams = null;
            SourceType = sourceType;
        }

        public void SetTitleParams(params object[] titles)
        {
            _titleParams = titles;
        }
        
        public void SetDescriptionParams(params object[] descriptions)
        {
            _descriptionParams = descriptions;
        }
    }
}
