﻿using System.Collections.Generic;
using NanoReality.Game.Notifications.iOS.Local.Configs;
using Sirenix.OdinInspector;
using UnityEngine;

namespace NanoReality.Game.Notifications.iOS.Local.Settings
{
    [CreateAssetMenu(fileName = "Settings", menuName = "NanoReality/Notifications/iOS/Settings", order = 10)]
    [ShowOdinSerializedPropertiesInInspector]
    public class IOSLocalNotificationSettings : SerializedScriptableObject
    {
        [SerializeField] private Dictionary<LocalNotificationType, IOSNotificationDataConfig> _configPerType =
            new Dictionary<LocalNotificationType, IOSNotificationDataConfig>();

        public Dictionary<LocalNotificationType, IOSNotificationDataConfig> ConfigPerType => _configPerType;
    }
}
