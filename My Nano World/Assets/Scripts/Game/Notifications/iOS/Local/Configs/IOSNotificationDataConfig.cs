﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace NanoReality.Game.Notifications.iOS.Local.Configs
{
    [CreateAssetMenu(fileName = "DataConfig", menuName = "NanoReality/Notifications/iOS/DataConfig", order = 10)]
    [ShowOdinSerializedPropertiesInInspector]
    public class IOSNotificationDataConfig : SerializedScriptableObject
    {
        [SerializeField] private string _identifier;
        [SerializeField] private string _title;
        [SerializeField] private string[] subTitles;
        [SerializeField] private bool _repeat;

        public string Identifier => _identifier;

        public string Title => _title;

        public string[] SubTitles => subTitles;

        public bool Repeat => _repeat;
    }
}
