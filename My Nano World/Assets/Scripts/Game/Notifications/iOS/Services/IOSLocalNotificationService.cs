﻿#if UNITY_IOS
using System;
using System.Collections.Generic;
using NanoLib.Core.Logging;
using NanoReality.Game.Notifications.iOS.Local.Configs;
using NanoReality.Game.Notifications.iOS.Local.Settings;
using Unity.Notifications.iOS;
using UnityEngine;

namespace NanoReality.Game.Notifications.iOS.Services
{
    public class IOSLocalNotificationService : LocalNotificationService
    {
        private IOSLocalNotificationSettings _settings;

        private readonly Dictionary<Type, Dictionary<int, string>> _identifiersByTypeAndId = new Dictionary<Type, Dictionary<int, string>>();

        public override void Init()
        {
            base.Init();

            iOSNotificationCenter.OnNotificationReceived += OnNotificationReceived;

            _settings = Resources.Load<IOSLocalNotificationSettings>("Notifications/iOS/iOSNotificationSettings");
        }

        public override void Dispose()
        {
            base.Dispose();

            iOSNotificationCenter.OnNotificationReceived -= OnNotificationReceived;
        }

        protected override void SendNotification(NotificationData data)
        {
            if (!_settings.ConfigPerType.ContainsKey(data.Type))
            {
                $"Notification type {data.Type} is not set".LogError(LoggingChannel.Notification);
                return;
            }

            var config = _settings.ConfigPerType[data.Type];
            var notification = CreateNotification(config, data.FireTime, data.TitleParams, data.DescriptionParams);

            var isAdded = AddIdentifier(notification.Identifier, data.SourceType, data.Id);
            if (!isAdded)
                return;

            $"SendNotification id {data.Id} Identifier {notification.Identifier}".Log(LoggingChannel.Notification);
            iOSNotificationCenter.ScheduleNotification(notification);
        }

        protected override void Cancel(Type type, int id)
        {
            if (!_identifiersByTypeAndId.ContainsKey(type))
            {
                $"There is no scheduled notifications with type {type.FullName}".LogWarning(LoggingChannel.Notification);
                return;
            }

            var identifiersById = _identifiersByTypeAndId[type];
            if (!identifiersById.ContainsKey(id))
            {
                $"There is no scheduled notifications with id {id.ToString()}".LogWarning(LoggingChannel.Notification);
                return;
            }

            var identifier = identifiersById[id];
            iOSNotificationCenter.RemoveScheduledNotification(identifier);
        }

        protected override void CancelAll()
        {
            iOSNotificationCenter.RemoveAllScheduledNotifications();
        }

        protected override void CancelAllDisplayed()
        {
            iOSNotificationCenter.RemoveAllDeliveredNotifications();
        }

        protected override bool VerifyPlatform()
        {
#if UNITY_IOS && !UNITY_EDITOR
            return true;
#endif
            return false;
        }

        protected override bool OpenAppWithNotificationCheck()
        {
            return iOSNotificationCenter.GetLastRespondedNotification() != null;
        }

        private static void OnNotificationReceived(iOSNotification notification)
        {
            $"Notification received: Identifier {notification.Identifier}".LogWarning(LoggingChannel.Notification);

            iOSNotificationCenter.RemoveDeliveredNotification(notification.Identifier);
        }

        private iOSNotification CreateNotification(IOSNotificationDataConfig config, double fireTime, IReadOnlyCollection<object> titleParams, IReadOnlyCollection<object> descrParams)
        {
            var timeTrigger = new iOSNotificationTimeIntervalTrigger
            {
                TimeInterval = TimeSpan.FromSeconds(fireTime),
                Repeats = config.Repeat
            };

            var notification = new iOSNotification
            {
                Identifier = config.Identifier,
                Title = CreateNotificationTitle(config.Title, titleParams),
                Body = CreateNotificationDescription(config.SubTitles, descrParams),
                ShowInForeground = false,
                Trigger = timeTrigger,
            };

            return notification;
        }

        private bool AddIdentifier(string identifier, Type type, int dataId)
        {
            if (_identifiersByTypeAndId.ContainsKey(type))
            {
                var identifiersById = _identifiersByTypeAndId[type];
                if (identifiersById.ContainsKey(dataId))
                {
                    $"Push notification {identifier} with Id {dataId} is already registered!".LogWarning(LoggingChannel.Notification);
                    return false;
                }

                identifiersById.Add(dataId, identifier);
            }
            else
            {
                var identifiersById = new Dictionary<int, string> {{dataId, identifier}};
                _identifiersByTypeAndId.Add(type, identifiersById);
            }

            return true;
        }
    }
}
#endif