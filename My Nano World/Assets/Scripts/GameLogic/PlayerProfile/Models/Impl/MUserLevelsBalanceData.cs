﻿using System;
using System.Collections.Generic;
using Assets.NanoLib.Utilities.DataManager.Models.api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Impl;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.StartApplicationSequence.Models.api;
using Newtonsoft.Json;

public class MUserLevelsBalanceData : IUserLevelsData
{
    [JsonProperty("config")]
    public int LevelToOpenHappiness { get; set; }

    [JsonProperty("levels")]
    public List<UserLevel> Levels { get; set; }

	public UserLevel GetLevel(int level)
	{
		if (level <= Levels.Count && level >= 0)
		{
			return Levels[level];
		}

		if (level < 0 && Levels.Count > 0)
		{
			return Levels[0];
		}

		return Levels.Count > 0 ? Levels[Levels.Count - 1] : null;
	}

    #region IGameBalanceData

    public string DataHash { get; set; }

    private Action<IGameBalanceData> _gameBalanceUpdateCallback;

    public void LoadData(IServerCommunicator serverCommunicator, ILocalDataManager localDataManager, Action<IGameBalanceData> callback)
    {
        _gameBalanceUpdateCallback = callback;


       serverCommunicator.LoadUserLevels(this, OnLevelsLoadedFromServer);
    }

    private void OnLevelsLoadedFromServer(IUserLevelsData data)
    {
        Levels = data.Levels;
        LevelToOpenHappiness = data.LevelToOpenHappiness;

		for (int j = 0, LevelsCount = Levels.Count; j < LevelsCount; j++)
		{
			var level = Levels [j];
			for (int i = 0, levelawardsCount = level.awards.Count; i < levelawardsCount; i++)
			{
				var award = level.awards [i];
				award.InitServerData ();
			}
		}

        if (_gameBalanceUpdateCallback != null)
            _gameBalanceUpdateCallback(this);
    }

    #endregion
}