﻿using System.Collections.Generic;
using NanoReality.GameLogic.Quests;
using Newtonsoft.Json;

namespace NanoReality.GameLogic.PlayerProfile.Models.Impl
{
    /// <summary>
    /// Player's level objects class
    /// </summary>
    public class UserLevel : MLevel
    {
		[JsonProperty("award_actions")]
		public List<IAwardAction> awards { get; set; }
    }
}