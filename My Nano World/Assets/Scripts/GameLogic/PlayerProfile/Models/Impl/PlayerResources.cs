﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.UtilitesEngine.CustomDebugging.Models.api;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Extensions;
using NanoLib.Core.Logging;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.OrderDesk;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using UnityEngine;

namespace NanoReality.GameLogic.PlayerProfile.Models.Impl
{
    public class PlayerResources : IPlayerResources
    {
        [Obsolete]
        public enum CurencyType
        {
            Unknown,
            Coins,
            NanoGems,
            Bucks,
            Gold,
            Lanterns
        }

        #region Injects

        [Inject] public IDebug jDebug { get; set; }
        [Inject] public IHintTutorial jHintTutorial { get; set; }
        [Inject] public IHardTutorial jHardTutorial { get; set; }
        [Inject] public SignalOnSoftCurrencyStateChanged jSignalOnSoftCurrencyStateChanged { get; set; }
        [Inject] public SignalOnLanternCurrencyStateChanged jSignalOnLanternCurrencyStateChanged { get; set; }
        [Inject] public SignalOnHardCurrencyStateChanged jSignalOnHardCurrencyStateChanged { get; set; }
        [Inject] public SignalOnGoldCurrencyStateChanged jSignalOnGoldCurrencyStateChanged { get; set; }
        [Inject] public SignalOnHardCurrencyConsumed jSignalOnHardCurrencyConsumed { get; set; }
        [Inject] public SignalOnNanoGemsStateChanged jSignalOnNanoGemsStateChanged { get; set; }
        [Inject] public SignalOnProductsStateChanged jSignalOnProductsStateChanged { get; set; }
        [Inject] public SignalWarehouseProductsAmountChanged jSignalWarehouseProductsAmountChanged { get; set; }
        [Inject] public SignalOnCurrencyStateChanged jSignalOnCurrencyStateChanged { get; set; }
        
        #endregion
        
        public int SoftCurrency
        {
            get { return _softCurrency; }
            private set
            {
                _softCurrency = value;
                jSignalOnSoftCurrencyStateChanged.Dispatch(_softCurrency);
            }
        }

        public int LanternCurrency
        {
            get => _lanternCurrency;
            private set
            {
                _lanternCurrency = value;
                jSignalOnLanternCurrencyStateChanged.Dispatch(_lanternCurrency);
            }
        }

        public int HardCurrency
        {
            get { return _hardCurrency; }
            private set
            {
                if (value < _hardCurrency)
                {
                    jSignalOnHardCurrencyConsumed.Dispatch(_hardCurrency - value);
                }

                _hardCurrency = value;
                jSignalOnHardCurrencyStateChanged.Dispatch(_hardCurrency);
            }
        }

        public int NanoGems
        {
            get { return _nanoGemsCurrency; }
            set
            {
                _nanoGemsCurrency = value;
            }
        }

        public int GoldCurrency
        {
            get { return _goldCurrency; }
            set
            {
                _goldCurrency = value;
                jSignalOnGoldCurrencyStateChanged.Dispatch(_goldCurrency);
            }
        }

        public int WarehouseCapacity { get; private set; }
        public bool IsWarehouseFull
        {
            get { return ProductsCount >= WarehouseCapacity; }
        }

        public int ProductsCount
        {
            get { return Products.Sum(p => p.Value); }
        }

        private Dictionary<Id<Product>, int> Products { get; set; }

        private int _softCurrency;
        private int _hardCurrency;
        private int _nanoGemsCurrency;
        private int _goldCurrency;
        private int _lanternCurrency;

        /// <summary>
        /// Returns list of all player products.
        /// </summary>
        public IEnumerable<KeyValuePair<Id<Product>, int>> PlayerProducts => Products;
        
        [PostConstruct]
        public void PostConstruct()
        {
            Products = new Dictionary<Id<Product>, int>();
        }

        public void InitPlayerResources(int soft, int hard, int gems, int gold, int lanterns, Dictionary<Id<Product>, int> products)
        {
            _softCurrency = soft;
            _hardCurrency = hard;
            _nanoGemsCurrency = gems;
            _goldCurrency = gold;
            _lanternCurrency = lanterns;

            Products = products;
        }

        /// <summary>
        /// Инициализация продуктов в ресурсах
        /// </summary>
        /// <param name="products">
        /// Словарь продуктов. Если передать сюда null, словарь продуктов обновляться не будет
        /// (сделано для того, что бы при необходимости обновлять только <see cref="WarehouseCapacity"/>)
        /// </param>
        /// <param name="capacity">Емкость варехауса</param>
        public void InitProducts(Dictionary<Id<Product>, int> products, int capacity)
        {
            WarehouseCapacity = capacity;

            if (products != null)
                Products = products;
            else
            {
                jSignalWarehouseProductsAmountChanged.Dispatch();
            }
        }

        /// <summary>
        /// Можем ли поместить на склад указанное число товара
        /// </summary>
        /// <param name="count">Количество товара</param>
        /// <returns>True, если можно поместить весь товар в склад</returns>
        public bool IsCanPlaceToWarehouse(int count)
        {
            return (ProductsCount + count) <= WarehouseCapacity || !jHardTutorial.IsTutorialCompleted || jHintTutorial.IsHintTutorialActive;
        }

        /// <summary>
        /// Buy any object in game, evaluate final currencies
        /// </summary>
        /// <param name="itemPrice"></param>
        public bool BuyItem(Price itemPrice)
        {
            if (itemPrice != null && IsEnoughtForBuy(itemPrice))
            {
                if (itemPrice.IsNeedSoft)
                {
                    SoftCurrency -= itemPrice.SoftPrice;
                    jSignalOnCurrencyStateChanged.Dispatch(-1 * itemPrice.SoftPrice, PriceType.Soft);
                }

                if (itemPrice.IsNeedHard)
                {
                    HardCurrency -= itemPrice.HardPrice;
                    jSignalOnCurrencyStateChanged.Dispatch(-1 * itemPrice.HardPrice, PriceType.Hard);
                }

                if (itemPrice.IsNeedProducts)
                {
                    RemoveProducts(itemPrice.ProductsPrice);
                }

                if (itemPrice.IsNeedGold)
                {
                    GoldCurrency -= itemPrice.GoldPrice;
                    jSignalOnCurrencyStateChanged.Dispatch(-1 * itemPrice.GoldPrice, PriceType.Gold);
                }

                return true;
            }

            "Not enough resources to remove.".LogError();
            return false;
        }

        /// <summary>
        /// Покупка за премиум валюту
        /// </summary>
        /// <param name="hardCount">кол-во премиум валюты >0</param>
        /// <returns></returns>
        public bool BuyHardCurrency(int hardCount)
        {
            if (hardCount < 0)
            {
                throw new ArgumentException("Hard count is less zero!");
            }
            if (HardCurrency < hardCount)
            {
                throw  new ArgumentException("Not enought hard currency!");
            }
            
            HardCurrency -= hardCount;
            jSignalOnCurrencyStateChanged.Dispatch(-1 * hardCount, PriceType.Hard);
            return true;
        }

        /// <summary>
        /// Покупка за софт валюту
        /// </summary>
        /// <param name="softCount">кол-во софт валюты</param>
        /// <returns></returns>
        public bool BuySoftCurrency(int softCount)
        {
            if (softCount < 0)
            {
                jDebug.LogError("soft count is less zero!");
                return false;
            }
            if (SoftCurrency < softCount)
            {
                jDebug.LogError("user doesn't have enought soft");
            }
            
            SoftCurrency -= softCount;
            jSignalOnCurrencyStateChanged.Dispatch(-1 * softCount, PriceType.Soft);
            
            return true;
        }

        public bool BuyNanoGems(int gemsCount)
        {
            if (gemsCount < 0)
            {
                jDebug.LogError("Nano gems count is less zero!");
                return false;
            }
            if (NanoGems < gemsCount)
            {
                jDebug.LogError("user doesn't have enought nano gems");
            }
            NanoGems -= gemsCount;
            jSignalOnNanoGemsStateChanged.Dispatch(NanoGems);

            return true;
        }

        public void AddSoftCurrency(int softCount)
        {
            if (softCount < 0)
            {
                jDebug.LogError("soft count is less zero!");
            }

            SoftCurrency += softCount;
            jSignalOnCurrencyStateChanged.Dispatch(softCount, PriceType.Soft);
        }

        public void AddHardCurrency(int hardCount)
        {
            if (hardCount < 0)
            {
                jDebug.LogError("hard count is less zero!");
            }
            HardCurrency += hardCount;
            
            jSignalOnCurrencyStateChanged.Dispatch(hardCount, PriceType.Hard);
        }

        public void AddGoldCurrency(int goldAmount)
        {
            if (goldAmount < 0)
            {
                jDebug.LogError("gold count is less zero!");
                return;
            }
            GoldCurrency += goldAmount;
            
            jSignalOnCurrencyStateChanged.Dispatch(goldAmount, PriceType.Gold);
        }

        public void AddLanternCurrency(int lanternAmount)
        {
            LanternCurrency += lanternAmount;
            jSignalOnLanternCurrencyStateChanged.Dispatch(lanternAmount);
        }

        public void RemoveGoldCurrency(int gold)
        {
            if (gold < 0)
            {
                jDebug.LogError("gold count is less than zero!");
                return;
            }
            
            GoldCurrency -= gold;
            jSignalOnCurrencyStateChanged.Dispatch(-1 * gold, PriceType.Gold);
        }

        public void RemoveHardCurrency(int hardCurrency)
        {
            if (hardCurrency < 0)
            {
                jDebug.LogError("hard count is less than zero!");
                return;
            }

            HardCurrency -= hardCurrency;

            jSignalOnCurrencyStateChanged.Dispatch(-1 * hardCurrency, PriceType.Hard);
        }

        public void RemoveLanternCurrency(int lanternCurrency)
        {
            if (lanternCurrency < 0)
            {
                "lantern count is less than zero!".LogError();
                return;
            }

            LanternCurrency -= lanternCurrency;

            jSignalOnCurrencyStateChanged.Dispatch(-1 * lanternCurrency, PriceType.Lantern);
        }

        public void AddNanoGems(int gemsCount, AwardSourcePlace sourcePlace)
        {
            if (gemsCount < 0)
            {
                jDebug.LogError("Nano gems count is less zero!");
            }

            NanoGems += gemsCount;
            jSignalOnNanoGemsStateChanged.Dispatch(NanoGems);
        }

        /// <summary>
		/// Возвращается количество конкретного продукта
		/// </summary>
		public int GetProductCount(Id<Product> productId)
		{
			return Products.ContainsKey(productId) ? Products[productId] : 0;
		}

		/// <summary>
		/// Возвращается количество разновидностей продукта
		/// </summary>
		public int GetProductsTypesCount()
		{
			return Products.Count;
		}
        
		/// <summary>
		/// Возвращается пару id товара - количество
		/// </summary>
		public KeyValuePair<Id<Product>, int> GetProductByIndex(int productId)
		{
			if (productId < Products.Count)
				return Products.ElementAt(productId);
			return new KeyValuePair<Id<Product>, int>(-1,-1);
		}

        /// <summary>
        /// Checks if enought currencies for buy
        /// </summary>
        /// <param name="itemPrice"></param>
        /// <returns></returns>
        public bool IsEnoughtForBuy(Price itemPrice)
        {
            bool result = true;

            if (itemPrice.IsNeedSoft)
                result = SoftCurrency >= itemPrice.SoftPrice;

            if (itemPrice.IsNeedHard)
                result = result && (HardCurrency >= itemPrice.HardPrice);

            if (itemPrice.IsNeedProducts)
                result = result && (CheckProductsEnought(itemPrice.ProductsPrice));

            if (itemPrice.IsNeedGold)
                result = result && GoldCurrency >= itemPrice.GoldPrice;

            return result;
        }


		public bool IsEnoughtForBuyOnlyGold(Price itemPrice)
		{
			bool result = true;

			if (itemPrice.IsNeedSoft)
				result = SoftCurrency >= itemPrice.SoftPrice;

			if (itemPrice.IsNeedHard)
				result = result && (HardCurrency >= itemPrice.HardPrice);

			return result;
		}

        public bool HasEnoughProductsForUpgrade(List<IProductForUpgrade> products)
        {
            if (products != null)
            {
                for (int i = 0; i < products.Count; i++)
                {
                    var product = products[i];
                    if (!product.IsLoaded && !HasEnoughProduct(product.ProductID, product.ProductsCount))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public bool HasEnoughProduct(Id<Product> id, int amount)
        {
            return Products.ContainsKey(id) && Products[id] >= amount;
        }
        
        public bool CheckProductsEnought(Dictionary<Id<Product>, int> products)
        {
            return products == null || products.All(resource => Products.ContainsKey(resource.Key) && Products[resource.Key] >= resource.Value);
        }        
        
        public bool CheckProductsEnoughZero(Dictionary<Id<Product>, int> products)
        {
            return products == null || products.All(resource => resource.Value == 0);
        }

        public void RemoveProducts(Dictionary<Id<Product>, int> products)
        {
            if (products == null)
            {
                return;
            }

            if (CheckProductsEnought(products))
            {
                foreach (var resource in products)
                {
                    Products[resource.Key] -= resource.Value;
                }
				jSignalOnProductsStateChanged.Dispatch(Products);
                jDebug.Log($"Warehouse: Remove Products: <b>[{products.GetValuesString()}]</b> CurrentProducts: <b>{Products.GetValuesString()}</b>");
            }
            else if (CheckProductsEnoughZero(products))
            {
                jSignalOnProductsStateChanged.Dispatch(Products);
            }
            else
            {
                jDebug.LogError($"Warehouse: Player doesn't have enough products [{products.GetValuesString()}]");
            }
            
            jSignalWarehouseProductsAmountChanged.Dispatch();
        }

        public void RemoveProducts(List<ProductData> products)
        {
            var dictionary = new Dictionary<Id<Product>, int>(products.Count);
            foreach (ProductData productData in products)
            {
                dictionary.Add(productData.ProductId, productData.Count);
            }
            RemoveProducts(dictionary);
        }

        /// <summary>
        /// Удаляет 1 продукт у юзера
        /// </summary>
        /// <param name="productId">айди продукта</param>
        /// <param name="amount">его кол-во</param>
        public void RemoveProduct(Id<Product> productId, int amount)
        {
            if (Products == null)
                Products = new Dictionary<Id<Product>, int>();

            if (Products.ContainsKey(productId) && Products[productId] >= amount)
            {
                var currentCount = Products[productId];
                var newCount = currentCount - amount;
				jSignalOnProductsStateChanged.Dispatch(Products);
                jSignalWarehouseProductsAmountChanged.Dispatch();

                Products[productId] = newCount;
                if (newCount == 0)
                {
                    Products.Remove(productId);
                }
                jDebug.Log($"Warehouse: RemoveProduct: <b>[{productId.Value}:{amount}]</b> CurrentProducts: <b>{Products.GetValuesString()}</b>");
            }
            else
            {
                jDebug.LogError($"Warehouse: Player doesn't have enough products <b>[{productId}:{amount}]</b>");
            }
        }
       
        public void AddProducts(Dictionary<Id<Product>, int> products)
        {
            if (Products == null)
                Products = new Dictionary<Id<Product>, int>();

            foreach (var product in products)
            {
                if (Products.ContainsKey(product.Key))
                {
                    Products[product.Key] += product.Value;
                }
                else
                {
                    Products.Add(product.Key, product.Value);
                }
            }
            
            jDebug.Log($"Warehouse: AddProducts: <b>[{products.GetValuesString()}]</b> CurrentProducts: <b>{Products.GetValuesString()}</b>");
            
			jSignalOnProductsStateChanged.Dispatch(Products);
            jSignalWarehouseProductsAmountChanged.Dispatch();
        }

        public void AddProduct(Id<Product> productId, int amount, bool withNotification = true)
        {
            if (Products == null)
                Products = new Dictionary<Id<Product>, int>();

            if (Products.ContainsKey(productId))
            {
                Products[productId] += amount;
            }
            else
            {
                Products.Add(productId, amount);
            }

            jDebug.Log($"Warehouse: AddProduct: <b>[{productId}:{amount}]</b> CurrentProducts: <b>{Products.GetValuesString()}</b>");
            if (!withNotification)
            {
                return;
            }
            
            jSignalOnProductsStateChanged.Dispatch(Products);
            jSignalWarehouseProductsAmountChanged.Dispatch();
        }
    }
}