﻿using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.Game.Tutorial;
using Newtonsoft.Json;

namespace NanoReality.GameLogic.PlayerProfile.Models.Api
{
    [JsonConverter(typeof(SerializationConverters.ProfileConverter))]
    public interface IPlayerProfile
    {
	    Dictionary<AdTypePanel, AdStructData> AdData { get; set; }
	    IPlayerExperience CurrentLevelData { get; set; }
	    IPlayerResources PlayerResources { get; set; }
	    Id<IPlayerProfile> Id { get; set; }
	    int DailyBonusDay { get; set; }
	    int CurrentLevel { get; set; }
	    int CurrentExp { get; set; }
	    bool IsPremiumUser { get; set; }
	    bool IsDailyBonusCollectedToday { get; }
	    StepType TutorialStep { get; set; }
	    IUserCity UserCity { get; set; }
	    Dictionary<string, string> SocialAccounts { get; set; }
        
        void Init();
        
        void ResetDailyBonusTimer();
    }
}