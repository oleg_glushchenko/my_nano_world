﻿using System.Collections.Generic;
using NanoReality.GameLogic.PlayerProfile.Models.Impl;
using NanoReality.GameLogic.StartApplicationSequence.Models.api;

namespace NanoReality.GameLogic.PlayerProfile.Models.Api
{
    /// <summary>
    /// Provides data with player-levels storage
    /// </summary>
    public interface IUserLevelsData : IGameBalanceData
    {
        /// <summary>
        /// List of available levels with needed experience
        /// </summary>
        List<UserLevel> Levels { get; set; }

        int LevelToOpenHappiness { get; set; }

        UserLevel GetLevel(int level);
    }
}