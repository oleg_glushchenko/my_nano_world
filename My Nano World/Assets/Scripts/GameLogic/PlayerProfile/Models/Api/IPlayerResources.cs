﻿using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.OrderDesk;

namespace NanoReality.GameLogic.PlayerProfile.Models.Api
{
    /// <summary>
    /// Представляет объект хранящий данные о ресурсах игрока (деньги, товары, и т.д.)
    /// </summary>
    public interface IPlayerResources
    {
	    int SoftCurrency { get; }
        int HardCurrency { get; }
        int GoldCurrency { get; }
        int NanoGems { get; }
        int LanternCurrency { get; }
        
        int WarehouseCapacity { get; }
        
	    bool IsWarehouseFull { get; }

	    int ProductsCount { get; }
	    
	    IEnumerable<KeyValuePair<Id<Product>, int>> PlayerProducts { get; }
	    
	    void InitPlayerResources(int soft, int hard, int gems, int gold, int lanterns, Dictionary<Id<Product>, int> products);

	    void InitProducts(Dictionary<Id<Product>, int> products, int capacity);
	    
		int GetProductCount(Id<Product> productId);
		
		KeyValuePair<Id<Product>, int> GetProductByIndex(int productId);
		
		int GetProductsTypesCount();
		
        bool IsCanPlaceToWarehouse(int count);
        
        bool BuyItem(Price itemPrice);
        
        bool BuyHardCurrency(int hardCount);

        bool BuySoftCurrency(int softCount);
        
        bool BuyNanoGems(int gemsCount);
        
        void AddSoftCurrency(int softCount);
        
        void AddHardCurrency(int hardCount);

        void AddGoldCurrency(int goldAmount);

        void AddLanternCurrency(int lanternAmount);
        
        void RemoveGoldCurrency(int gold);
        
        void RemoveHardCurrency(int hardCurrency);

        void RemoveLanternCurrency(int lanternCurrency);
        
        void AddNanoGems(int gemsCount, AwardSourcePlace sourcePlace);
        
        bool IsEnoughtForBuy(Price itemPrice);
        
		bool IsEnoughtForBuyOnlyGold(Price itemPrice);
        
	    bool HasEnoughProductsForUpgrade(List<IProductForUpgrade> products);
	    
	    bool HasEnoughProduct(Id<Product> id, int amount);

        bool CheckProductsEnought(Dictionary<Id<Product>, int> products);
        
        void AddProducts(Dictionary<Id<Product>, int> products);
        
        /// <summary>
        /// Add products to player warehouse.
        /// Possible to add it without listeners notify.
        /// </summary>
        /// <param name="productId">Target Product Id</param>
        /// <param name="amount">Products Count</param>
        /// <param name="withNotification">Will warehouse send notification about change it?</param>
        void AddProduct(Id<Product> productId, int amount, bool withNotification = true);
        
        void RemoveProducts(Dictionary<Id<Product>, int> products);

        void RemoveProducts(List<ProductData> products);
        
        void RemoveProduct(Id<Product> productId, int amount);
    }
}