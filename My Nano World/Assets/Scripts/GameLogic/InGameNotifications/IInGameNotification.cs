﻿using System;
using Assets.NanoLib.Utilities;

namespace NanoReality.GameLogic.InGameNotifications
{
    /// <summary>
    /// Внутриигровая нотификация
    /// </summary>
    public interface IInGameNotification
    {

        /// <summary>
        /// Айди нотификации
        /// </summary>
        Id<IInGameNotification> NotificationId { get; set; }

        /// <summary>
        /// Время получения нотификации (unix time)
        /// </summary>
        int RecivedUnixTime { get; set; }

        /// <summary>
        /// Время получение нотификации (конвертация из <seealso cref="RecivedUnixTime"/> )
        /// </summary>
        DateTime RecivedTime { get; }

        /// <summary>
        /// Прочитана ли юзером нотификация?
        /// </summary>
        bool IsRead { get; set; }

        /// <summary>
        /// Тип нотификации
        /// </summary>
        NotificationTypes NotificationType { get; }


        /// <summary>
        /// Использывать нотификацию
        /// </summary>
        void UseNotification();

        /// <summary>
        /// Вызывается после загрузки нотификации
        /// </summary>
        void OnNotificationLoad();

        void Send();
    }


    public enum NotificationTypes
    {
        System = 1,
        Pvp = 2,
        Gift = 4,
        Help = 5
    }
}

