﻿using System;
using Assets.NanoLib.UtilitesEngine;
using Assets.Scripts.Engine.Analytics.Model.api;
using NanoLib.Core.Logging;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Impl;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Impl;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using UnityEngine;

namespace GameLogic.Taxes.Service
{
    public sealed class CityTaxesService : ICityTaxesService
    {
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public ITimerManager jTimerManager { get; set; }
        [Inject] public INanoAnalytics jNanoAnalytics { get; set; }

        public event Action TaxesChanged;
        public event Action<int> TaxesCollected;

        public bool AnyTaxesAccrued => CityTaxes.CurrentCoins > 0;

        public CityTaxes CityTaxes { get; private set; } = new CityTaxes();

        private float _secondsPerCoin;
        private int _coinsBeforeTimerStart;
        private ITimer _timer;

        private long _stopCityHallTime;

        public void Init()
        {
            UpdateCityTaxes();
        }

        public void Dispose()
        {
            _timer?.CancelTimer();
            _timer = null;
        }

        public void Collect()
        {
            _timer?.CancelTimer();
            _timer = null;
            CityTaxes.CurrentCoins = 0;

            TaxesChanged?.Invoke();

            jServerCommunicator.CollectTaxes(OnTaxesCollected);
        }

        private void UpdateCityTaxes()
        {
            _timer?.CancelTimer();
            _timer = null;

            if (CityTaxes.LeftCityHallTime > 0)
            {
                _secondsPerCoin = 1 / CityTaxes.CoinsPerSecond;
                _coinsBeforeTimerStart = CityTaxes.CurrentCoins;

                StartTimer();
            }

            TaxesChanged?.Invoke();
        }

        public void LoadNewTaxes(Action onSuccess = null)
        {
            jServerCommunicator.GetTaxesInfo(cityTaxes =>
            {
                CityTaxes = cityTaxes;
                UpdateCityTaxes();
                onSuccess?.Invoke();
            });
        }

        private void StartTimer()
        {
            if (_timer != null)
            {
                Logging.LogError(LoggingChannel.Debug, "Can't start new timer while previous not disposed.");
                return;
            }

            _timer = jTimerManager.StartServerTimer(float.MaxValue, null, OnTickAction);
        }

        private void OnTickAction(float currentTime)
        {

            if (CityTaxes.LeftCityHallTime < currentTime)
            {
                ReachCityHallTime();
                return;
            }

            var coinsSinceTimerStart = currentTime / _secondsPerCoin;
            var currentCoins = _coinsBeforeTimerStart + (int) coinsSinceTimerStart;

            if (CityTaxes.CurrentCoins == currentCoins)
            {
                return;
            }

            CityTaxes.CurrentCoins = currentCoins;

            TaxesChanged?.Invoke();
        }

        private void ReachCityHallTime()
        {
            var coinsSinceTimerStart = CityTaxes.LeftCityHallTime / _secondsPerCoin;
            CityTaxes.CurrentCoins = Mathf.RoundToInt(_coinsBeforeTimerStart + coinsSinceTimerStart);
            _timer.CancelTimer();
            _timer = null;
            TaxesChanged?.Invoke();
        }

        private void OnTaxesCollected(int collectedCoins, CityTaxes cityTaxes)
        {
            TaxesCollected?.Invoke(collectedCoins);
            jPlayerProfile.PlayerResources.AddSoftCurrency(collectedCoins);
            CityTaxes = cityTaxes;
            jNanoAnalytics.OnPlayerCurrenciesIncrease(PlayerResources.CurencyType.Coins, collectedCoins, AwardSourcePlace.Tax, "");
            UpdateCityTaxes();
        }
    }
}