using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.ServerCommunication.Models;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using Assets.NanoLib.UtilitesEngine;
using Assets.NanoLib.Utilities.DataManager.Models.api;
using Assets.Scripts.Engine.Analytics.Model.api;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl;
using Assets.Scripts.GameLogic.Utilites;
using DG.Tweening;
using Engine.UI.Extensions.NextEventUpgradePanel;
using NanoLib.UI.Core;
using NanoReality.Engine.UI.Views.TheEvent;
using NanoReality.Engine.Utilities;
using NanoReality.Game.UI.BuildingsShopPanel;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.Constants;
using NanoReality.GameLogic.Data;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.IAP;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.Services;
using NanoReality.GameLogic.StartApplicationSequence.Models.api;
using NanoReality.GameLogic.TheEvent.Data;
using NanoReality.GameLogic.TheEvent.Signals;
using NanoReality.GameLogic.Utilites;
using strange.extensions.injector.api;
using UnityEngine;

namespace NanoReality.GameLogic.TheEvent
{
    public class TheEventService : ITheEventService
    {
        #region Injects

        [Inject] public ILocalDataManager jLocalDataManager { get; set; }
        [Inject] public IInjectionBinder jInjectionBinder { get; set; }
        [Inject] public IPopupManager jPopupManager { get; set; }
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public IIapService jIapService { get; set; }
        [Inject] public IMapObjectsGenerator jIMapObjectsGenerator { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public ITimerManager jTimerManager { get; set; }
        [Inject] public IIconProvider jIconProvider { get; set; }
        [Inject] public INanoAnalytics jNanoAnalytics { get; set; }
        [Inject] public IGameCamera jCamera { get; set; }
        [Inject] public IUIManager jUIManager { get; set; }
        [Inject] public SignalTheEventDataIsLoaded jDataIsLoaded { get; set; }
        [Inject] public SignalGameDataIsUpdated jSignalGameDataIsUpdated { get; set; }
        [Inject] public SignalEnableEventButton jSignalEnableEventButton { get; set; }
        [Inject] public SignalClickOnTheEventBuildingWhenCannotUpgrade jSignalClickOnTheEventBuildingWhenCannotUpgrade { get; set; }
        [Inject] public SignalOpenTheEventPanel jSignalOpenTheEventPanel { get; set; }
        [Inject] public SignalCheckUnclaimedStageRewards jSignalCheckUnclaimedStageRewards { get; set; }
        [Inject] public SignalOpenUpgradeWindowEventBuilding jSignalOpenUpgradeWindowEventBuilding { get; set; }
        [Inject] public SignalSetEventTimerOnHud jSignalSetEventTimerOnHud { get; set; }
        [Inject] public SignalHudIsInitialized jSignalHudIsInitialized { get; set; }
        [Inject] public SignalDisableAttentionViewOnEventBuilding jSignalDisableAttentionViewOnEventBuilding { get; set; }
        [Inject] public PlayerOpenedPanelSignal jSignalPlayerOpenedPanel { get; set; }
        [Inject] public PurchaseFinishedSignal jPurchaseFinishedSignal { get; set; }
        [Inject] public MapBuildingSyncSignal jMapBuildingSyncSignal { get; set; }
        [Inject] public ShowWindowSignal jShowWindowSignal { get; set; }
        [Inject] public SignalTheEventIsEnds jSignalTheEventIsEnds { get; set; }
        [Inject] public SignalOnHidePanel jSignalOnHidePanel { get; set; }
        [Inject] public SignalPreEventEndHintTrigger jSignalPreEventEndHintTrigger { get; set; }

        #endregion

        #region Private Variables

        private FinishEventData _finishData;
        private ITimer _hudTimer;
        private ITimer _popupTimer;
        private ITimer _nextDayTimer;
        private readonly List<IAwardAction> _displayRewardInfos = new List<IAwardAction>();
        private bool _buildingsDataIsUpdated;
        private bool _disappearEventBuildingAnimIsCompleted;
        private PurchaseEventPopupSetting _purchaseSettings;
        private bool _showPurchaseDelayed;
        /// <summary>
        /// As we don't receive from the server ID of final purchase, this increment used to avoid conflicts with common IAP products
        /// </summary>a
        private int _finalRewardIdIncrement = 100000;
        private int _fxAnimLength = 2;
        private float _delayShowPurchasePopup = 0.5f;
        private bool _purchased;

        #endregion

        [PostConstruct]
        public void PostConstruct()
        {
            jDataIsLoaded.AddListener(SetData);
        }

        #region Interface Implementration

        public BaseEventData EventDataBase { get; private set; }
        public bool OpenTimelineTab { get; set; }

        public void Init()
        {
            if (EventDataBase == null)
            {
                return;
            }

            jSignalClickOnTheEventBuildingWhenCannotUpgrade.AddListener(OnClickTheEventBuildingWhenCannotUpgrade);
            jSignalOpenTheEventPanel.AddListener(OnClickOpenEventPanelButton);
            jMapBuildingSyncSignal.AddListener(OnBuildEventBuilding);
            jSignalCheckUnclaimedStageRewards.AddListener(OnClickEventBuildingWhenCanUpgrade);
            jSignalHudIsInitialized.AddListener(OnHudInitialized);
            jNanoAnalytics.TheEventStartInfo(EventDataBase.EventName, EventDataBase.CurrentEventDay, GetEventBuildingCurrentStage());

            StartTimerToNextDay();
        }

        public void Dispose()
        {
            jSignalClickOnTheEventBuildingWhenCannotUpgrade.RemoveListener(OnClickTheEventBuildingWhenCannotUpgrade);
            jSignalOpenTheEventPanel.RemoveListener(OnClickOpenEventPanelButton);
            jMapBuildingSyncSignal.RemoveListener(OnBuildEventBuilding);
            jSignalCheckUnclaimedStageRewards.RemoveAllListeners();
            jSignalHudIsInitialized.RemoveAllListeners();
        }

        public void OpenEventPanel()
        {
            jShowWindowSignal.Dispatch(typeof(TheEventPanelView), null);
            jSignalPlayerOpenedPanel.Dispatch();
        }

        public bool MainStateOfEvent()
        {
            if (EventDataBase != null)
            {
                return EventIsStarted() && !EventIsEnded();
            }

            return false;
        }

        public void AddReward(RewardData[] rewards, int lootBoxType)
        {
            foreach (var reward in rewards)
            {
                Sprite sprite = null;
                int amount = reward.Amount;
                var actionType = AwardActionTypes.LevelUp;

                switch (reward.Type)
                {
                    case RewardType.Soft:

                        jPlayerProfile.PlayerResources.AddSoftCurrency(reward.Amount);
                        sprite = jIconProvider.GetCurrencySprite(PriceType.Soft);
                        actionType = AwardActionTypes.SoftCurrency;
                        break;

                    case RewardType.Hard:

                        jPlayerProfile.PlayerResources.AddHardCurrency(reward.Amount);
                        sprite = jIconProvider.GetCurrencySprite(PriceType.Hard);
                        actionType = AwardActionTypes.PremiumCurrency;
                        break;

                    case RewardType.Products:

                        jPlayerProfile.PlayerResources.AddProduct(reward.Id, reward.Amount);
                        sprite = jIconProvider.GetProductSprite(reward.Id);
                        actionType = AwardActionTypes.Resources;
                        break;

                    case RewardType.Gold:

                        jPlayerProfile.PlayerResources.AddGoldCurrency(reward.Amount);
                        sprite = jIconProvider.GetCurrencySprite(PriceType.Gold);
                        actionType = AwardActionTypes.GoldBars;
                        break;

                    case RewardType.Lanterns:

                        jPlayerProfile.PlayerResources.AddLanternCurrency(reward.Amount);
                        sprite = jIconProvider.GetCurrencySprite(PriceType.Lantern);
                        actionType = AwardActionTypes.Lantern;
                        break;
                }

                _displayRewardInfos.Add(new DisplayRewardInfo() { AwardCount = amount, Sprite = sprite, AwardType = actionType });
            }

            jPopupManager.Show(new RewardBoxPopupSettings(_displayRewardInfos, lootBoxType, true));
            _displayRewardInfos.Clear();
        }

        public MapObjectView GetEventBuildingView()
        {
            var model = jGameManager.FindMapObjectByType<IMapObject>(MapObjectintTypes.EventBuilding);
            var view = jGameManager.GetMapObjectView(model);
            return view;
        }

        public void OnGettingEventRewardBuilding()
        {
            DisableTheEvent();
            UpdateBuildingsInfo();
        }

        public void OnDisappearAnimEnds()
        {
            _disappearEventBuildingAnimIsCompleted = true;
            OpenBuildingShopRewardBuilding();
        }

        #endregion

        #region Private Methods

        private void SetData(BaseEventData data)
        {
            EventDataBase = data;
            jDataIsLoaded.RemoveListener(SetData);
        }

        private void OnEventEnding()
        {
            jServerCommunicator.GetEventFinishedData(endingData =>
            {
                _finishData = endingData;
                IStoreProduct finalBuilding = new StoreProductEvent(endingData.ProductStoreId,
                    UnityEngine.Purchasing.ProductType.Consumable, _finalRewardIdIncrement + endingData.Day);
                jIapService.AddProduct(finalBuilding);

                if (EventDataBase.ShowPurchasePopupOnce)
                {
                    if (!PlayerPrefs.HasKey("purchase_popup_shown"))
                    {
                        ShowPurchaseEventPopup(_finishData);
                        PlayerPrefs.SetInt("purchase_popup_shown", 1);
                    }
                }
                else
                {
                    ShowPurchaseEventPopup(_finishData);
                }
            });
        }

        private void OnPreEventTimerEnds()
        {
            GetEventBuilding().ReadyToUpgrade = true;
            _hudTimer.CancelTimer();
            jSignalPreEventEndHintTrigger.Dispatch();
            StartHudTimer(EventDataBase.FinishDate, OnEventTimerEnds);
        }

        private void OnEventTimerEnds()
        {
            jSignalTheEventIsEnds.Dispatch();
            _hudTimer.CancelTimer();
            jSignalDisableAttentionViewOnEventBuilding.Dispatch(true);

            if (jPlayerProfile.CurrentLevelData.CurrentLevel < EventDataBase.UnlockLevel)
            {
                DisableTheEvent();
            }
            else
            {
                GetEventBuilding().ReadyToUpgrade = false;
                StartHudTimer(EventDataBase.AfterEndDate, OnPostEventTimerEnds);
                OnEventEnding();
            }
        }

        private void OnPostEventTimerEnds()
        {
            _hudTimer.CancelTimer();
            DisableTheEvent();
        }

        private IEventBuilding GetEventBuilding()
        {
            var model = jGameManager.FindMapObjectByType<IMapObject>(MapObjectintTypes.EventBuilding);
            return model as IEventBuilding;
        }

        private int GetEventBuildingCurrentStage()
        {
            int stage = 0;

            for (int i = 0; i < EventDataBase.BuildingStages.Length; i++)
            {
                if (EventDataBase.BuildingStages[i].IsCompleted)
                {
                    stage = i;
                }
            }

            return stage;
        }
     
        private void OnUpdateBuildingsData(IGameBalanceData data)
        {
            if (data is IMapObjectsData)
            {
                _buildingsDataIsUpdated = true;
                OpenBuildingShopRewardBuilding();
                jSignalGameDataIsUpdated.RemoveListener(OnUpdateBuildingsData);
            }
        }

        private void OpenBuildingShopRewardBuilding()
        {
            if (_buildingsDataIsUpdated && _disappearEventBuildingAnimIsCompleted)
            {
                jShowWindowSignal.Dispatch(typeof(BuildingsShopPanelView), new BuildingShopData(BusinessSectorsTypes.Town, 
                    BuildingsCategories.Special, BuildingSubCategories.Common));
            }
        }

        private string GetDaysLeftToStartEvent(double startTime)
        {
            var dateTime = DateTimeUtils.UnixStartTime.AddSeconds(startTime).ToLocalTime();
            var secondsLeft = (dateTime - DateTime.Now).TotalSeconds;
            return UiUtilities.GetFormattedTimeFromSeconds((int) secondsLeft);
        }

        private void ShowPreEventPopup(bool hideTimer = false)
        {
            var settings = new PreEventPopupSettings();
            settings.OnPopupHide+=CancelTimer;

            if (jPlayerProfile.CurrentLevelData.CurrentLevel >= EventDataBase.UnlockLevel)
            {
                settings.LowLevel = false;
            }
            else
            {
                settings.LowLevel = true;
                settings.UnlockLevel = EventDataBase.UnlockLevel + 1;
            }

            settings.DisableTimer = hideTimer;

            jPopupManager.Show(settings);

            if (!hideTimer)
            {
                float secondsLeft = GetTimeLeft(EventDataBase.StartDate);
                
                _popupTimer?.CancelTimer();
                _popupTimer = null;
                _popupTimer = jTimerManager.StartServerTimer(secondsLeft, OnTimerEnds,
                    elapsed =>
                    {
                        settings.TimerTickAction(
                            UiUtilities.GetFormattedTimeFromSecondsShorted((int)(secondsLeft - elapsed)));
                    });

                settings.TimerTickAction(UiUtilities.GetFormattedTimeFromSecondsShorted((int)(secondsLeft)));

                void OnTimerEnds()
                {
                    CancelTimer();
                    settings.OnTimerEnds?.Invoke();
                }
            }

            void CancelTimer()
            {
                _popupTimer?.CancelTimer();
            }
        }

        private void UpdateBuildingsInfo()
        {
            var mapDataCashed = jLocalDataManager.GetData<LocalReuqestDataKeeper>().Items.FirstOrDefault(item => item.Url.Equals(@"/v1/game/building"));
            mapDataCashed.Hash = string.Empty;
            jIMapObjectsGenerator.Clear();
            var loadCommand = GetCommand<LoadGameDataCommand>();
            loadCommand.LoadMapData();
            jSignalGameDataIsUpdated.AddListener(OnUpdateBuildingsData);
        }

        private T GetCommand<T>() where T : new()
        {
            var instance = new T();
            jInjectionBinder.injector.Inject(instance);
            return instance;
        }

        private bool EventIsEnded()
        {
            var finishDate = DateTimeUtils.UnixStartTime.AddSeconds(EventDataBase.FinishDate).ToLocalTime();
            return DateTime.Now >= finishDate;
        }

        private bool EventIsStarted()
        {
            var startDate = DateTimeUtils.UnixStartTime.AddSeconds(EventDataBase.StartDate).ToLocalTime();
            return DateTime.Now >= startDate;
        }

        private bool EventIsCompletelyEnded()
        {
            var afterEndDate = DateTimeUtils.UnixStartTime.AddSeconds(EventDataBase.AfterEndDate).ToLocalTime();
            return DateTime.Now >= afterEndDate;
        }

        private void ShowPurchaseEventPopup(FinishEventData endingData)
        {
            _popupTimer?.CancelTimer();

            _purchaseSettings = new PurchaseEventPopupSetting()
            {
                Price = endingData.Price,
                OnDisappearAnimationEnds = OnDisappearAnimEnds,
                OnBuyEventBuilding = ActionOnClickBuy,
            };

            if (!jUIManager.IsNotDefaultViewVisible())
            {
                jPopupManager.Show(_purchaseSettings);
                float secondsLeft = GetTimeLeft(EventDataBase.AfterEndDate);
                
                _popupTimer?.CancelTimer();
                _popupTimer = null;
                _popupTimer = jTimerManager.StartServerTimer(secondsLeft, OnPurchaseTimerEnds,
                    elapsed =>
                    {
                        _purchaseSettings.TimerTickAction(UiUtilities.GetFormattedTimeFromSecondsShorted((int)(secondsLeft - elapsed)));
                    });

                _purchaseSettings.TimerTickAction(UiUtilities.GetFormattedTimeFromSecondsShorted((int)(secondsLeft)));
                jCamera.FocusOnMapObject(GetEventBuildingView().MapObjectModel);
            }
            else
            {
                jSignalOnHidePanel.AddListener(ShowDelayedPurchasePopup);
                _showPurchaseDelayed = true;
                jShowWindowSignal.Dispatch(typeof(FxPopUpPanelView), new FxPopUpPanelView.FxPopupData(
                    LocalizationUtils.LocalizeUI(LocalizationKeyConstants.EVENT_IS_OVER), _fxAnimLength, FxPanelType.Achievement));
            }

            void ActionOnClickBuy()
            {
                _popupTimer.CancelTimer();
                jPurchaseFinishedSignal.AddListener(OnPurchaseFinalBuilding);
                jIapService.BuyProduct(endingData.ProductStoreId);
            }

            void OnPurchaseTimerEnds()
            {
                _popupTimer.CancelTimer();
                _purchaseSettings.OnTimerEnds?.Invoke();
            }
        }

        private void ShowDelayedPurchasePopup(UIPanelView _)
        {
            DOVirtual.DelayedCall(_delayShowPurchasePopup, TryToShow);

            void TryToShow()
            {
                if (_showPurchaseDelayed && !jUIManager.IsNotDefaultViewVisible())
                {
                    ShowPurchaseEventPopup(_finishData);
                    _showPurchaseDelayed = false;
                    jSignalOnHidePanel.RemoveListener(ShowDelayedPurchasePopup);
                }
            }
        }

        private void OnPurchaseFinalBuilding(IStoreProduct prod, bool success)
        {
            if (success)
            {
                _purchased = true;
                OnGettingEventRewardBuilding();
            }

            _purchaseSettings.OnPurchase?.Invoke(success);
            jPurchaseFinishedSignal.RemoveListener(OnPurchaseFinalBuilding);
        }

        private void DisableTheEvent()
        {
            jSignalEnableEventButton.Dispatch(false);
        }

        private void OnHudInitialized()
        {
            if (EventIsEnded())
            {
                if (jPlayerProfile.CurrentLevelData.CurrentLevel >= EventDataBase.UnlockLevel)
                {
                    OnEventEnding();
                }
            }

            if (!EventIsStarted())
            {
                StartHudTimer(EventDataBase.StartDate, OnPreEventTimerEnds);
            }
            else if (EventIsStarted() && !EventIsEnded())
            {
                StartHudTimer(EventDataBase.FinishDate, OnEventTimerEnds);
            }
            else if (EventIsStarted() && EventIsEnded())
            {
                StartHudTimer(EventDataBase.AfterEndDate, OnPostEventTimerEnds);
            }

            jSignalEnableEventButton.Dispatch(true);
        }

        private void OnClickEventBuildingWhenCanUpgrade()
        {
            if (!MainStateOfEvent()) return;

            if (!HasUnclaimedStageRewards())
            {
                jSignalOpenUpgradeWindowEventBuilding.Dispatch();
            }
            else
            {
                OpenTimelineTab = true;
                OpenEventPanel();
            }
        }

        private double GetTimeLeftToTextDay()
        {
            var startDate = DateTimeUtils.UnixStartTime.AddSeconds(EventDataBase.StartDate);
            var nextDay = startDate.AddDays(EventDataBase.CurrentEventDay).ToLocalTime();
            var secondsLeft = (nextDay - DateTime.Now).TotalSeconds;

            return secondsLeft;
        }

        private void StartTimerToNextDay()
        {
            if (!MainStateOfEvent()) return;

            _nextDayTimer?.CancelTimer();
            _nextDayTimer = null;
            _nextDayTimer = jTimerManager.StartServerTimer((float)GetTimeLeftToTextDay(), OnTimerEnds);

            void OnTimerEnds()
            {
                _nextDayTimer?.CancelTimer();
                EventDataBase.CurrentEventDay++;
                GetEventBuilding().ReadyToUpgrade = true;
                jSignalDisableAttentionViewOnEventBuilding.Dispatch(false);
                StartTimerToNextDay();
            }
        }

        private void OnClickTheEventBuildingWhenCannotUpgrade()
        {
            if (EventIsStarted() && !EventIsEnded())
            {
                GetEventBuilding().SecondsToNextEventDay = (float) GetTimeLeftToTextDay();
                jShowWindowSignal.Dispatch(typeof(NextEventUpgradePanel), GetEventBuilding());
            }
            else if (EventIsEnded() && !EventIsCompletelyEnded()&& !_purchased)    
            {
                ShowPurchaseEventPopup(_finishData);
            }
            else if (!EventIsStarted())
            {
                ShowPreEventPopup();
            }
        }

        private void OnClickOpenEventPanelButton()
        {
            if (jPlayerProfile.CurrentLevelData.CurrentLevel < EventDataBase.UnlockLevel && EventIsStarted())
            {
                ShowPreEventPopup(true);
            }
            else
            {
                if (!EventIsStarted())
                {
                    ShowPreEventPopup();
                }
                else if (EventIsStarted() && !EventIsEnded())
                {
                    OpenEventPanel();
                }
                else if (EventIsEnded())
                {
                    ShowPurchaseEventPopup(_finishData);
                }
            }
        }

        private bool HasUnclaimedStageRewards()
        {
            for (int i = 0; i < EventDataBase.DailyRewards.Length; i++)
            {
                if (EventDataBase.BuildingStages[i].IsCompleted && !EventDataBase.BuildingStages[i].IsClaimed)
                {
                    return true;
                }
            }

            return false;
        }

        private void OnBuildEventBuilding(IMapObject obj)
        {
            if (obj is EventBuilding)
            {
                EventDataBase.BuildingStages[obj.Level - 1].IsCompleted = true;
                jNanoAnalytics.OnBuildStageBuilding(obj.Level);
                OpenTimelineTab = true;
                OpenEventPanel();
            }
        }

        private void StartHudTimer(double finishTime, Action onTimerEnds)
        {
            var secondsLeft = GetTimeLeft(finishTime);
            var startDate = DateTimeUtils.UnixStartTime.AddSeconds(EventDataBase.StartDate).ToLocalTime();
            _hudTimer?.CancelTimer();
            _hudTimer = null;
            _hudTimer = jTimerManager.StartServerTimer(secondsLeft, onTimerEnds,
                elapsed =>
                {
                    jSignalSetEventTimerOnHud.Dispatch(UiUtilities.GetFormattedTimeFromSecondsShorted((int)(secondsLeft - elapsed)));
                });

            jSignalSetEventTimerOnHud.Dispatch(UiUtilities.GetFormattedTimeFromSecondsShorted((int)(secondsLeft)));
        }

        private float GetTimeLeft(double finishTime)
        {
            var dateTime = DateTimeUtils.UnixStartTime.AddSeconds(finishTime).ToLocalTime();
            var secondsLeft = (dateTime - DateTime.Now).TotalSeconds;
            return (float) secondsLeft;
        }

        #endregion
    }
}