using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Game.Services;
using NanoReality.GameLogic.Data;
using NanoReality.GameLogic.TheEvent.Data;

namespace NanoReality.GameLogic.TheEvent
{
    public interface ITheEventService : IGameService
    {
        BaseEventData EventDataBase { get; }
        bool OpenTimelineTab { get; set;}
        MapObjectView GetEventBuildingView();
        void OnGettingEventRewardBuilding();
        void AddReward(RewardData[] rewards, int lootBoxType);
        void OnDisappearAnimEnds();
        void OpenEventPanel();
        bool MainStateOfEvent();
    }
}