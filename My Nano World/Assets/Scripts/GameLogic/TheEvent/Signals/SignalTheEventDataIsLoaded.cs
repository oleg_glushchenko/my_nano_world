using NanoReality.GameLogic.TheEvent.Data;
using strange.extensions.signal.impl;

namespace NanoReality.GameLogic.TheEvent.Signals
{
    public class SignalTheEventDataIsLoaded : Signal<BaseEventData> { }
}