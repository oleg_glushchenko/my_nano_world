using strange.extensions.signal.impl;

namespace NanoReality.GameLogic.TheEvent.Signals
{
    public class SignalEnableEventButton : Signal<bool> {}
}