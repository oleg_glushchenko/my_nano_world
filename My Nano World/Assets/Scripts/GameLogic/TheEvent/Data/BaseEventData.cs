using System;
using NanoReality.GameLogic.Data;
using Newtonsoft.Json;

namespace NanoReality.GameLogic.TheEvent.Data
{
    [Serializable]
    public class BaseEventData 
    {
        [JsonProperty("name")]
        public string EventName { get; set; }

        [JsonProperty("current_day")]
        public int CurrentEventDay { get; set; }

        [JsonProperty("unlock_level")]
        public int UnlockLevel { get; set; }

        // id of event building
        [JsonProperty("building_stage_id")]
        public int BuildingStageId { get; set; }   

        // this building the player will receive after completing the event 
        [JsonProperty("main_building_id")]
        public int MainBuildingId { get; set; }

        [JsonProperty("start_date")] 
        public long StartDate { get; set; }

        [JsonProperty("finish_date")]
        public long FinishDate { get; set; }

        [JsonProperty("date_after_end")]
        public long AfterEndDate { get; set; }

        [JsonProperty("show_end_only_once")]
        public bool ShowPurchasePopupOnce { get; set; }
        
        /// <summary>
        /// Each element in the array represents every events day. 0 index = first day.
        /// </summary>
        [JsonProperty("daily_reward")] 
        public DailyRewardInfo[] DailyRewards { get; set; }

        /// <summary>
        /// Each element in the array represents every events day. 0 index = first day.
        /// </summary>
        [JsonProperty("building_stage")]
        public BuildingStageInfo[] BuildingStages { get; set; }
    }

    [Serializable]
    public class DailyRewardInfo
    {
        [JsonProperty("common")]
        public RewardInfo CommonRewards { get; set; }

        [JsonProperty("premium")]
        public RewardInfo PremiumRewards { get; set; }
    }

    [Serializable]
    public class BuildingStageInfo
    {
        [JsonProperty("is_completed")]
        public bool IsCompleted { get; set; }

        [JsonProperty("is_claimed")]
        public bool IsClaimed { get; set; }

        public int Day;
    }

    [Serializable]
    public class RewardInfo
    {
        [JsonProperty("is_collected")]
        public bool IsClaimed { get; set; }

        [JsonProperty("reward_id")]
        public int RewardId { get; set; }

        [JsonProperty("ui_type")]
        public int UiType { get; set; }

        [JsonProperty("amount")]
        public int Amount { get; set; }

        [JsonProperty("loot_box_type")]
        public int LootBoxType { get; set; }

        [JsonProperty("reward")]
        public RewardData[] Rewards { get; set; }

        public int Day;

        public bool IsPremium;
    }
}