﻿using Assets.NanoLib.UI.Core.Signals;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using NanoReality.Engine.UI.Extensions.LoaderPanel;
using NanoReality.GameLogic.Configs;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.StartApplicationSequence.Controllers.Signals;
using NanoReality.StrangeIoC;
using UnityEngine;
using ThreadPriority = UnityEngine.ThreadPriority;

namespace NanoReality.GameLogic.Common
{
    public class ApplicationInitCommand : AGameCommand
    {
        private const string UserAgreementFlagKey = "UserAgreementFlagKey";
        
        [Inject] public IPopupManager jPopupManager { get; set; }
        [Inject] public IServerCommunicator ServerCommunicator { get; set; }
        [Inject] public ShowWindowSignal jShowWindowSignal { get;set; }
        [Inject] public SignalGameInitialized jSignalGameInitialized { get; set; }
        [Inject] public LinksConfigsHolder LinksHolder { get; set; }

        public override void Execute()
        {
            base.Execute();
            
            Application.targetFrameRate = 60;
            Application.backgroundLoadingPriority = ThreadPriority.Low;
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
            
            jShowWindowSignal.Dispatch(typeof(LoaderPanelView), null);

            if (PlayerPrefs.GetInt(UserAgreementFlagKey) <= 0)
            {
                jSignalGameInitialized.Dispatch();
                
                Retain();
                jPopupManager.Show(new AgreementPopupSettings(), OnAgreementPopupResult);
            }

            ServerCommunicator.LoadLinksConfigData(LinksHolder.SetLinks);
        }

        private void OnAgreementPopupResult(PopupResult result)
        {
            if (result == PopupResult.Ok)
            {
                PlayerPrefs.SetInt(UserAgreementFlagKey, 1);
                PlayerPrefs.Save();
                Release();
            }
        }
    }
}
