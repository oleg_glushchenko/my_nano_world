﻿using Assets.NanoLib.UtilitesEngine.CustomDebugging.Models.api;
using Assets.Scripts.Engine.Analytics;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.Settings.GamePrefs;
using NanoReality.GameLogic.SocialCommunicator.api;
using strange.extensions.command.impl;
using strange.extensions.signal.impl;

namespace NanoReality.GameLogic.StartApplicationSequence.Controllers.Commands
{
    public class SocialNetworkLogout : Signal
    {
    }

    public class OnFriendInvitedSignal : Signal
    {
    }

    public class OnFriendsUpdatedSignal : Signal<int>
    {
    }

    public class CommandOnAutorizeInSocialNetwork : Command
    {
        [Inject] public AuthorizeResult AuthorizeResult { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IServerCommunicator ServerCommunicator { get; set; }
        [Inject] public IDebug Debug { get; set; }
        [Inject] public IPopupManager jPopupManager { get; set; }
        [Inject] public SignalOnSocialConnectAction jSignalOnSocialConnectAction { get; set; }
        [Inject] public IGamePrefs jGamePrefs { get; set; }

        public override void Execute()
        {
            Retain();

            Debug.Log("CommandOnAuthorizeIsSocialNetwrokd", true);
            Debug.Log($"Is Authorized to {AuthorizeResult.SocialNetworkCommunicator.SocialNetworkName} resilt: {AuthorizeResult.IsAuthorized}");
            if (AuthorizeResult.IsAuthorized)
            {
                // отправляем запрос на привязку к соц. сети
                ServerCommunicator.BindSocialNetwork(AuthorizeResult.SocialNetworkCommunicator.SocialNetworkName,
                    AuthorizeResult.AuthorizedUser.id, SelectWorld.None, OnBindSocialNetwrork);

                jSignalOnSocialConnectAction.Dispatch(AuthorizeResult);
            }
        }

        private void OnBindSocialNetwrork(ShortCityData city)
        {
            // привязка к соц. сети успешна
            if (city == null || city.CityData == null)
            {
                if (
                    !jPlayerProfile.SocialAccounts.ContainsKey(AuthorizeResult.SocialNetworkCommunicator
                        .SocialNetworkName) ||
                    string.IsNullOrEmpty(
                        jPlayerProfile.SocialAccounts[AuthorizeResult.SocialNetworkCommunicator.SocialNetworkName])
                )
                {
                    jPopupManager.Show(new AccountBindingSuccessPopupSetting(AuthorizeResult.SocialNetworkCommunicator
                        .SocialNetworkType.ToString()));
                }

                jPlayerProfile.SocialAccounts[AuthorizeResult.SocialNetworkCommunicator.SocialNetworkName] =
                    jGamePrefs.DeviceId;

                Release();
            }
        }

        public enum SelectWorld
        {
            None,
            Current,
            Other
        }
    }
}