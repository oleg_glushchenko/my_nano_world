﻿using System;
using NanoLib.UI.Core;
using NanoReality.Engine.UI.Views.Panels;
using NanoReality.Engine.UI.Views.TheEvent;
using NanoReality.Game.UI;
using NanoReality.Game.UI.BuildingsShopPanel;
using NanoReality.GameLogic.MinePanel;
using NanoReality.GameLogic.Upgrade;
using UniRx;

namespace GameLogic.StartApplicationSequence.Controllers.Commands.LoadUiCommands
{
    public class LoadPanelPrefabsCommand 
    {
        [Inject] public IUIManager jUIManager { get; set; }

        private readonly Type[] _preLoadPanels =
        {
            typeof(MinePanelView),
            typeof(FactoryPanelView),
            typeof(BuildingsShopPanelView),
            typeof(UpgradeWithProductsPanelView),
            typeof(TheEventPanelView)

        };
        
        public AsyncSubject<Unit> GetObservableExecute()
        {
            var subject = new AsyncSubject<Unit>();

            subject.OnNext(Unit.Default);
            
            foreach (var preLoadPanel in _preLoadPanels)
            {
                jUIManager.LoadView(preLoadPanel);
            }
            
            subject.OnCompleted();
            return subject;
        }
    }
}
