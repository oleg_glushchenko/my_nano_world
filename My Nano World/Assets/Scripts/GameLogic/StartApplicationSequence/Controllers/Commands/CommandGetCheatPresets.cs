using NanoReality.Debugging.ConsoleCommands;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.StrangeIoC;

namespace NanoReality.GameLogic.StartApplicationSequence.Controllers.Commands
{
	public class CommandGetCheatPresets : AGameCommand
	{
		[Inject] public IServerCommunicator jServerCommunicator { get; set; }

		public override void Execute()
		{
			base.Execute();

			Retain();

#if !PRODUCTION
			jServerCommunicator.CheatGetPresets(presetsData =>
			{
				MobileConsoleContext.Instance.jDebugConsoleCommands.PresetData = presetsData;
				Release();
			});
#else
            Release();
#endif
		}
	}
}