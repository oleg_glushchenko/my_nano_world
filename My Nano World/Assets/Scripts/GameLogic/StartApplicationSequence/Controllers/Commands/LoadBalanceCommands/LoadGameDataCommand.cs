﻿using Assets.NanoLib.ServerCommunication.Models;
using Assets.NanoLib.Utilities.DataManager.Models.api;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.api;
using Assets.Scripts.GameLogic.DailyBonus.Api;
using NanoReality.GameLogic.Achievements.api;
using NanoReality.GameLogic.BuildingSystem.CityArea;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.Configs;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.SoftCurrencyStore;
using NanoReality.GameLogic.StartApplicationSequence.Models.api;
using Newtonsoft.Json;
using strange.extensions.signal.impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NanoLib.Core.Logging;
using UniRx;
using UnityEngine;

/// <summary>
/// Load Game Balance to Application (just add instance of IGameBalanceData to gameBalance list in postconstact
/// </summary>

public class GameDataLoadedSignal : Signal { }
public class SignalGameDataIsUpdated : Signal<IGameBalanceData> { }

public class LoadGameDataCommand
{
    [Inject] public GameDataLoadedSignal GameBalanceLoadedSignal { get; set; }
    [Inject] public SignalGameDataIsUpdated jSignalGameDataIsUpdated { get; set; }
    [Inject] public IServerCommunicator ServerCommunicator { get; set; }
    [Inject] public ILocalDataManager LocalDataManager { get; set; }

    #region Implementations of IGameBalanceData

    [Inject] public IUserLevelsData UserUserLevelsData { get; set; }
    [Inject] public IMapObjectsData MapObjectsData { get; set; }
    [Inject] public IProductsData ProductsData { get; set; }
    [Inject] public IQuestBalanceData QuestBalanceData { get; set; }
    [Inject] public IAchievementsData AchievementsData { get; set; }
    [Inject] public ISoftCurrencyBundlesModel SoftCurrencyBundlesModel { get; set; }
    [Inject] public ISkipIntervalsData jSkipIntervalsData { get; set; }
    [Inject] public IDailyBonusBalanceData jDailyBonusBalanceData { get; set; }
    [Inject] public IGameConfigData jGameConfigData { get; set; }
    [Inject] public ICityAreaData jCityAreaData { get; set; }
    [Inject] public IBuildingSlotsData jBuildingSlotsData { get; set; }
    #endregion

    private UrlEtags clientUrlEtags = new UrlEtags();
    private IDictionary<string, IGameBalanceData> _urlObjectData;

    private void Init()
    {
        _urlObjectData = new Dictionary<string, IGameBalanceData>()
        {
            ["v1/game/level"] = UserUserLevelsData,
            ["v1/game/product"] = ProductsData,
            ["v1/game/building"] = MapObjectsData,
            //["v1/trade/trade-config"] = AuctionConfigData,
            ["v1/game/quest"] = QuestBalanceData,
            ["v1/game/achieve"] = AchievementsData,
            ["v1/game/coin-package"] = SoftCurrencyBundlesModel,
            ["v1/game/skip"] = jSkipIntervalsData,
            //["v1/game/daily-bonus"] = jDailyBonusBalanceData,
            ["v1/game/config"] = jGameConfigData,
            ["v1/game/area"] = jCityAreaData,
            ["v1/game/factory-slots"] = jBuildingSlotsData,
        };

        LocalDataManager.Init(Application.persistentDataPath, "AppData.bin");
    }

    public AsyncSubject<Unit> GetObservableExecute()
    {
        var subject = new AsyncSubject<Unit>();

        Init();

        clientUrlEtags = new UrlEtags();
        var localRequestDataKeeper = LocalDataManager.GetData<LocalReuqestDataKeeper>();
        if (localRequestDataKeeper == null)
        {
            localRequestDataKeeper = new LocalReuqestDataKeeper();
            LocalDataManager.AddData(localRequestDataKeeper);

            foreach (var balanceUrlKeyPair in _urlObjectData)
            {
                clientUrlEtags.Etags.Add(balanceUrlKeyPair.Key, "");
            }
        }
        else
        {
            foreach (var balanceData in _urlObjectData)
            {
                var local = localRequestDataKeeper.GetLocalRequestData("/" + balanceData.Key);
                clientUrlEtags.Etags.Add(balanceData.Key, local != null ? local.Hash : "");
            }
        }

        subject.OnNext(Unit.Default);

        ServerCommunicator.LoadGameBalanceHashes(clientUrlEtags, etags =>
        {
            foreach (var serverEtag in etags.Etags)
            {
                var url = "/" + serverEtag.Key;
                var hash = serverEtag.Value.Replace("\"", "");

                var local = localRequestDataKeeper.GetLocalRequestData(url);
                if (local != null)
                {
                    local.PreloadedHash = hash;
                }
                else
                {
                    local = new LocalRequestData
                    {
                        Url = url,
                        PreloadedHash = hash
                    };
                    localRequestDataKeeper.Items.Add(local);
                }
            }

            LocalDataManager.SaveData();

            Log(localRequestDataKeeper);
            
            _urlObjectData.Values.Select(Load).WhenAll().Subscribe(x =>
            {
                LocalDataManager.SaveData();
                GameBalanceLoadedSignal.Dispatch();

                subject.OnCompleted();
            });
        });

        return subject;
    }

    private IObservable<Unit> Load(IGameBalanceData data)
    {
        var subject = new AsyncSubject<Unit>();
        subject.OnNext(Unit.Default);
        data.LoadData(ServerCommunicator, LocalDataManager, x =>
        {
            subject.OnCompleted();
            jSignalGameDataIsUpdated.Dispatch(x);
        });

        return subject;
    }

    public void LoadMapData()
    {
        Load(MapObjectsData);
    }

    private void Log(LocalReuqestDataKeeper localRequestDataKeeper)
    {
        var etagsLog = new StringBuilder();

        etagsLog.AppendLine("<color=magenta><b>Server ETags:</b></color>");

        foreach (var clientEtag in clientUrlEtags.Etags)
        {
            var url = "/" + clientEtag.Key;

            var local = localRequestDataKeeper.GetLocalRequestData(url);
            if (local != null)
            {
                if (!string.IsNullOrEmpty(local.Hash))
                {
                    if (local.Hash != local.PreloadedHash)
                    {
                        etagsLog.AppendLine(
                            $"<color=red><b>{url}</b> localHash: <b>{local.Hash}</b> serverHash: <b>{local.PreloadedHash}</b></color>");
                    }
                    else
                    {
                        etagsLog.AppendLine(
                            $"<color=green><b>{url}</b> localHash: <b>{local.Hash}</b> serverHash: <b>{local.PreloadedHash}</b></color>");
                    }
                }
                else
                {
                    etagsLog.AppendLine(
                        $"<color=magenta><b>{url}</b> serverHash: <b>{local.PreloadedHash}</b></color>");
                }
            }
            else
            {
                var hash = clientEtag.Value;
                etagsLog.AppendLine(
                    $"<color=#4169E1><b>{url}</b> localHash: <b>{hash}</b></color>");
            }
        }
        
        etagsLog.Log();
    }

    public class UrlEtags
    {
        [JsonProperty("etags")]
        public Dictionary<string, string> Etags = new Dictionary<string, string>();
    }
}