﻿using System;
using NanoLib.Core.Services.Sound;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UtilitesEngine;
using NanoLib.UI.Core;
using NanoReality.Core.Resources;
using NanoReality.Engine.UI.Extensions.LoaderPanel;
using NanoReality.Game.Analytics;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.Settings.GameSettings.Models.Api;
using NanoReality.GameLogic.StartApplicationSequence.Controllers.Commands;
using NanoReality.StrangeIoC;
using strange.extensions.signal.impl;
using UnityEngine;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.U2D;
using UnityEngine.U2D;
#endif

public class SignalReloadApplication : Signal { }

public class ReloadApplicationCommand : AGameCommand
{
    [Inject] public IGameManager jGameManager { get; set; }
    [Inject] public IMapObjectsGenerator jMapObjectsGenerator { get; set; }
    [Inject] public ISoundManager jSoundManager { get; set; }
    [Inject] public IServerCommunicator jServerCommunicator { get; set; }
    [Inject] public ShowWindowSignal jShowWindowSignal { get; set; }
    [Inject] public IUIManager jUiManager { get; set; }
    [Inject] public ITimerManager jTimerManager { get; set; }
    [Inject] public IGameSettings jGameSettings { get; set; }
    [Inject] public SignalDisposeGameServices jSignalDisposeGameServices { get; set; }
    
    public override void Execute()
    {
        base.Execute();

        jSoundManager.StopPlaylist();
        
        // Fix for avoid double open loader panel, if command was called when panel visible.
        var loaderPanelType = typeof(LoaderPanelView);
        if (!jUiManager.IsViewVisible(loaderPanelType))
        {
            Action reloadAction = OnReloadClearing;
            jShowWindowSignal.Dispatch(loaderPanelType, reloadAction);
        }
        else
        {
            OnReloadClearing();
        }
    }

    private void OnReloadClearing()
    {
        jSignalDisposeGameServices.Dispatch();
        jServerCommunicator.StopAndClearServerQueue();
        jGameManager.ClearAll();
        jMapObjectsGenerator.Clear();
        jTimerManager.Dispose();

        if (jGameSettings.GraphicWasChanged)
        {
            AssetBundleManager.Reset();
            AtlasManager.Reset();
        }
        else
        {
            AssetBundleManager.Unsubscribe();
            AtlasManager.Unsubscribe();
        }
        
        ScriptableObjectsManager.UnloadAll();
        Resources.UnloadUnusedAssets();
        GC.Collect();

        SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);

        RepackSpriteAtlases();

        Release();
    }

    private static void RepackSpriteAtlases() //Editor SpriteAtlases not reloads fix
    {
#if UNITY_EDITOR 
        var target = Application.platform == RuntimePlatform.OSXEditor ? BuildTarget.StandaloneOSX : BuildTarget.StandaloneWindows;
        SpriteAtlasUtility.PackAtlases(new[] {AssetDatabase.LoadAssetAtPath<SpriteAtlas>("Assets/Art/Textures/VFXSprites.spriteatlas")}, target);
#endif
    }
}