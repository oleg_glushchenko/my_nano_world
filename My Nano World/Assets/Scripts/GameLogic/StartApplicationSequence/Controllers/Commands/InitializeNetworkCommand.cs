using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Assets.Scripts.GameLogic.Utilites;
using NanoLib.Core.Logging;
using NanoReality.Engine.Settings.BuildSettings;
using NanoReality.Game.Data;
using NanoReality.StrangeIoC;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;

namespace NanoReality.GameLogic.StartApplicationSequence.Controllers.Commands
{
	public sealed class InitializeNetworkCommand : AGameCommand
	{
		[Inject] public IBuildSettings jBuildSettings { get; set; }
		[Inject] public IPopupManager jPopupManager { get; set; }
		[Inject] public NetworkSettings jNetworkSettings { get; set; }
		[Inject] public SignalReloadApplication jSignalReloadApplication { get; set; }

		private const string MASTER_SERVER_NAME = "TargetServer";

		public override void Execute()
		{
			base.Execute();

			if (jNetworkSettings.UseMasterServer)
			{
				Retain();
				ConnectToMasterServer();
			}
		}

		private void ConnectToMasterServer()
		{
			if (PlayerPrefs.HasKey(MASTER_SERVER_NAME))
			{
				var serverJson = PlayerPrefs.GetString(MASTER_SERVER_NAME);
				SetServerAddressAndRelease(serverJson);
			}
			else
			{
				SendRequest();
			}
		}

		private void SendRequest()
		{
			var url = jNetworkSettings.MasterServerUrl;
			if (string.IsNullOrEmpty(url))
			{
				"Master server URL is empty!".LogError(LoggingChannel.Network);
				Release();
			}
			else
			{
				var version = jBuildSettings.BundleVersion;
				var unityWebRequest = UnityWebRequest.Get(url + version).SendWebRequest();
				$"Requested master server by url: {url + version}".Log(LoggingChannel.Network);
				
				unityWebRequest.completed += asyncOperation =>
				{
					var responseString = unityWebRequest.webRequest.downloadHandler.text;
					if (unityWebRequest.webRequest.result == UnityWebRequest.Result.ConnectionError)
					{
						ShowNetworkErrorWindow();
					}
					else
					{
						$"Master server {responseString} saved".Log(LoggingChannel.Network);
						PlayerPrefs.SetString(MASTER_SERVER_NAME, responseString);
						PlayerPrefs.Save();
						
						SetServerAddressAndRelease(responseString);
					}
				};
			}
		}

		private void ShowNetworkErrorWindow()
		{
			var title = LocalizationMLS.Instance.GetText(LocalizationKeyConstants.POORCONNECTION_LABEL_ERROR);
			var message = $"Localization/{LocalizationKeyConstants.POORCONNECTION_MSG_RESTART_GAME}";
						
			var popupSettings = new NetworkErrorPopupSettings(title, message, SendRequest, OnReloadClicked);
			jPopupManager.Show(popupSettings);
		}

		private void SetServerAddressAndRelease(string jsonString)
		{
			var serverSettings = JsonConvert.DeserializeObject<ServerSettings>(jsonString);
			jNetworkSettings.ServerSettings = serverSettings;
            
			$"Set Target Server: {serverSettings.GameServerUrl}".Log(LoggingChannel.Network);
			Release();
		}
		
		private void OnReloadClicked()
		{
			jSignalReloadApplication.Dispatch();
		}
	}
}