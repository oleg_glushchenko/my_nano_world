﻿using System.Collections.Generic;
using NanoReality.Game.Services.BuildingsUnlockService;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using UniRx;
using UnityEngine;

namespace NanoReality.GameLogic.StartApplicationSequence.Controllers.Commands
{
    public class LoadPlayerBuildingsUnlockCommand
    {
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public IBuildingsUnlockService jBuildingsUnlockService { get; set; }

        private List<BuildingInstanceUnlockInfo> _instancesUnlockList;
        
        public AsyncSubject<Unit> GetObservableExecute()
        {
            var subject = new AsyncSubject<Unit>();
            subject.OnNext(Unit.Default);
            
            jServerCommunicator.LoadBuildingInstancesUnlockData((unlockData) =>
            {
                _instancesUnlockList = unlockData;
                TryToFinish();
            });

            void TryToFinish()
            {
                if (_instancesUnlockList != null)
                {
                    jBuildingsUnlockService.Init(_instancesUnlockList);
                    subject.OnCompleted();
                }
            }

            return subject;
        }
    }
}
