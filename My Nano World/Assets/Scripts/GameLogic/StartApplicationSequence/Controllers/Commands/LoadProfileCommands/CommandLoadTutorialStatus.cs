﻿using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.StrangeIoC;

namespace NanoReality.GameLogic.StartApplicationSequence.Controllers.Commands
{
    public class CommandLoadTutorialStatus : AGameCommand
    {
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public IHintTutorial jHintTutorial { get; set; }

        public override void Execute()
        {
            base.Execute();

            Retain();

            jServerCommunicator.GetHintTutorialStatus(callback =>
            {
                jHintTutorial.UpdateStepsStatus(callback);
                Release();
            });
        }
    }
}