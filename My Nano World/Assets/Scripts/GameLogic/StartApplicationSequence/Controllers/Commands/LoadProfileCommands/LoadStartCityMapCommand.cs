﻿using System.Collections.Generic;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.GameManager.Controllers;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using UniRx;

namespace NanoReality.GameLogic.StartApplicationSequence.Controllers.Commands
{
    public class LoadStartCityMapCommand
    {
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public SignalOnCityMapLoaded jSignalOnCityMapLoaded { get; set; }
        [Inject] public SignalLoadCityMapSequence jSignalLoadCityMapSequence { get; set; }

        public AsyncSubject<Unit> GetObservableExecute()
        {
            var subject = new AsyncSubject<Unit>();
            subject.OnNext(Unit.Default);
            
            jSignalOnCityMapLoaded.AddOnce((x,y,z) =>
            {
                subject.OnCompleted();
            });
                
            var data = new LoadCityMapCommandData
            {
                AvailableBusinessSectors = new[] {BusinessSectorsTypes.Town, BusinessSectorsTypes.Farm, BusinessSectorsTypes.HeavyIndustry},
                CityId = jPlayerProfile.UserCity.Id,
                BusinessSector = BusinessSectorsTypes.Town,
                PlayerProfileId = jPlayerProfile.Id,
                LoadedCityMaps = new Dictionary<BusinessSectorsTypes, ICityMap>()
            };
                
            jSignalLoadCityMapSequence.Dispatch(data);

            return subject;
        }
    }
}