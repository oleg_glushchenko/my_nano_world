﻿using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.StartApplicationSequence.Controllers.Signals;
using UniRx;

namespace NanoReality.GameLogic.StartApplicationSequence.Controllers.Commands
{
    public class LoadUserQuestDataCommand
    {
        [Inject] public IUserQuestData jUserQuestData { get; set; }

        [Inject] public SignalOnQuestsLoaded jSignalOnQuestsLoaded { get; set; }

        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        
        public AsyncSubject<Unit> GetObservableExecute()
        {
            var subject = new AsyncSubject<Unit>();
            
            jServerCommunicator.LoadUserQuestsData(jUserQuestData, questData =>
            {
                subject.OnNext(Unit.Default);
                jSignalOnQuestsLoaded.Dispatch();
                
                subject.OnCompleted();
            });

            return subject;
        }
    }
}
