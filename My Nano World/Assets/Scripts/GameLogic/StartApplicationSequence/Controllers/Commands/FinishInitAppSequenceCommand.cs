﻿using System;
using NanoLib.Core.Services.Sound;
using Assets.NanoLib.UtilitesEngine;
using Assets.NanoLib.Utilities;
using NanoReality.Engine.BuildingSystem.CityCamera;
using NanoReality.Game.Data;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.StartApplicationSequence.Controllers.Signals;
using NanoReality.StrangeIoC;
using UnityEngine;

namespace NanoReality.GameLogic.StartApplicationSequence.Controllers.Commands
{
    public class FinishInitAppSequenceCommand : AGameCommand
    {
        [Inject] public IBuildSettings jBuildSettings { get; set; }
        [Inject] public FinishApplicationInitSignal jFinishApplicationInitSignal { get; set; }
        [Inject] public HidePreloaderAfterGameLaunchSignal jHidePreloaderSignal { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public IGameCamera jGameCamera { get; set; }
        [Inject] public IGameCameraModel jGameCameraModel { get; set; }
        [Inject] public ISoundManager jSoundManager { get; set; }
        [Inject] public ITimerManager jTimerManager { get; set; }

        public override void Execute()
        {
            base.Execute();

            jSoundManager.PlayPlaylist(SoundtrackPlaylist.GameScenePlaylistName, true);
            // TODO: workaround disabled sound track for keep developers team ears.
            //jSoundManager.SetChannelActive(false, SoundChannel.Soundtrack);
            
            // Setting up game camera
            var cityHall = jGameManager.FindMapObjectByType<IMapObject>(MapObjectintTypes.CityHall);
            if (cityHall != null)
            {
                jGameCamera.ZoomTo(cityHall, CameraSwipeMode.Jump, jGameCameraModel.DefaultZoom, 5f, true);
            }
            else
            {
                var mapView = jGameManager.GetMapView(BusinessSectorsTypes.Town.GetBusinessSectorId());
                var mapPosition = mapView.transform.position;
                var position = new Vector2F(mapPosition.x, mapPosition.z);
                jGameCamera.ZoomTo(position, CameraSwipeMode.Jump, jGameCameraModel.DefaultZoom, 5f, true);
            }

            GC.Collect();
            Resources.UnloadUnusedAssets();
            jFinishApplicationInitSignal.Dispatch();
            jHidePreloaderSignal.Dispatch();
            
            jTimerManager.StartSyncServerTimeTimer();

            Screen.sleepTimeout = jBuildSettings.ScreenSleepTimeout;
        }
    }
}