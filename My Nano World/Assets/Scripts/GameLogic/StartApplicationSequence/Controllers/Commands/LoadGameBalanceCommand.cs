﻿using System;
using GameLogic.StartApplicationSequence.Controllers.Commands.LoadUiCommands;
using NanoReality.StrangeIoC;
using UniRx;

namespace NanoReality.GameLogic.StartApplicationSequence.Controllers.Commands
{
    public class LoadGameBalanceCommand : AGameCommand
    {
        public override void Execute()
        {
            base.Execute();
            Retain();

            Observable.WhenAll(
                LoadGameBalance(),
                LoadCityMap(),
                LoadUi())
                .Subscribe(_ => Release());
        }

        private AsyncSubject<Unit> LoadGameBalance()
        {
            var subject = new AsyncSubject<Unit>();

            var cmGameBalance = GetCommand<LoadGameDataCommand>();
            var cmPlayerBuildingsUnlock = GetCommand<LoadPlayerBuildingsUnlockCommand>();

            subject.OnNext(Unit.Default);

            IObservable<Unit>[] asyncBalanceCommandsList =
            {
                cmGameBalance.GetObservableExecute(),
                cmPlayerBuildingsUnlock.GetObservableExecute()
            };

            Observable.WhenAll(asyncBalanceCommandsList).Subscribe(y => subject.OnCompleted());
            
            return subject;
        }

        private AsyncSubject<Unit> LoadCityMap()
        {
            var subject = new AsyncSubject<Unit>();

            var cmCityMap = GetCommand<LoadStartCityMapCommand>();
            var cmUserQuestData = GetCommand<LoadUserQuestDataCommand>();
            var cmUserAchievements = GetCommand<LoadUserAchievementsCommand>();

            subject.OnNext(Unit.Default);

            Observable
                .WhenAll(cmCityMap.GetObservableExecute())
                .Subscribe(x => Observable
                    .WhenAll(cmUserQuestData.GetObservableExecute(), cmUserAchievements.GetObservableExecute())
                    .Subscribe(y => subject.OnCompleted()));

            return subject;
        }

        private AsyncSubject<Unit> LoadUi()
        {
            var subject = new AsyncSubject<Unit>();

            var cmCityMap = GetCommand<LoadPanelPrefabsCommand>();

            subject.OnNext(Unit.Default);

            Observable.WhenAll(
                    cmCityMap.GetObservableExecute())
                    .Subscribe(_ => subject.OnCompleted());

            return subject;
        }

        private T GetCommand<T>() where T : new()
        {
            var instance = new T();
            injectionBinder.injector.Inject(instance);

            return instance;
        }
    }
}