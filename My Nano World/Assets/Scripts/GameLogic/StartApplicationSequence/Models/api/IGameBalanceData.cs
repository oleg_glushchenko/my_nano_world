﻿using System;
using Assets.NanoLib.Utilities.DataManager.Models.api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;

namespace NanoReality.GameLogic.StartApplicationSequence.Models.api
{
    public interface IGameBalanceData
    {
        string DataHash { get; set; }

        void LoadData(IServerCommunicator serverCommunicator, ILocalDataManager localDataManager, Action<IGameBalanceData> callback);
    }
}