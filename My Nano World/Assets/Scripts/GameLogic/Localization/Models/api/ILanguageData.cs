﻿using System.Collections.Generic;
using NanoReality.GameLogic.Localization.Models.impl;

namespace NanoReality.GameLogic.Localization.Models.api
{
	public interface ILanguageData
	{
		string LanguageName { get; set; }

        int LanguageVersion { get; set; }

		List<LanguageObject> LanguageObjects { get; set; } 
	}
}
