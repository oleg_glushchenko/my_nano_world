﻿using System;
using System.Collections.Generic;
using NanoReality.GameLogic.Localization.Models.api;
using Newtonsoft.Json;

namespace NanoReality.GameLogic.Localization.Models.impl
{
	public class LanguageData : ILanguageData
	{
		[JsonProperty("lng")]
		public string LanguageName { get; set; }

	    [JsonProperty("ver")]
        public int LanguageVersion { get; set; }

	    [JsonProperty("objs")]
		public List<LanguageObject> LanguageObjects { get; set; }
	}


}
