﻿using Newtonsoft.Json;

namespace NanoReality.GameLogic.Localization.Models.impl
{
	public struct LanguageObject
	{
		[JsonProperty("type")] 
		public int Type;

		[JsonProperty("key")] 
		public string Key;

		[JsonProperty("val")] 
		public string Value;
	}
}