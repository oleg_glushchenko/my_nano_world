﻿using NanoReality.Game.Services;

namespace NanoReality.GameLogic.Services
{
    public interface ISocialNetworkService : IGameService
    {
        void InviteFriends();
    }
}