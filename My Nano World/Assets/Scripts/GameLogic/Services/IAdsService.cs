﻿using System;
using NanoReality.Game.Services;

namespace NanoReality.GameLogic.Services
{
    public interface IAdsService : IGameService
    {
        AdTypePanel PlayingAdType { get; }
        bool IsVideoLoaded { get; }
        
        event EventHandler VideoStarted;
        event EventHandler VideoLoaded;
        event EventHandler VideoSkipped;
        event EventHandler VideoWatched;

        bool HasAvailableAdsByPanelType(AdTypePanel adType);
        void ShowVideo(AdTypePanel adType);
        void LoadVideo();
    }
}
