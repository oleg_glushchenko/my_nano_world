﻿using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.UI.Views.Trade.Items;
using NanoReality.Game;
using NanoReality.Game.Services;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using strange.extensions.signal.impl;

namespace NanoReality.GameLogic.Services
{
    public interface IProductService : IGameService
    {
        Signal<IMapBuilding> OnGoToProductionBuilding { get; }
        
        Signal<IMapBuilding> OnGoToBuildingsShop { get; }

        Product GetProduct(Id<Product> productId);

        string GetProductName(Id<Product> productId);

        string GetProductDescription(Id<Product> productId);

        string GetProductGoToText(Id<Product> productId);

        void GoToBuildingByProduct(Id<Product> productId);

        int CalculateHardPrice(int productId, int productCount);
        
        void GoToShop(Id<Product> productId);

        bool CanGoToBuildingByProduct(Id<Product> productId);
    }
}
