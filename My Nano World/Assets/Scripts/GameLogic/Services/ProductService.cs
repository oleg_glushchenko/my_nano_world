﻿using System;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks.Utils;
using Assets.Scripts.Engine.UI.Views.Trade.Items;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.api;
using Assets.Scripts.GameLogic.Utilites;
using Engine.UI.Views.Panels.ProductionPanels;
using NanoLib.UI.Core;
using NanoReality.Engine.UI.Extensions.Agricultural;
using NanoReality.Game.UI;
using NanoReality.Game.UI.BuildingsShopPanel;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.MinePanel;
using NanoReality.GameLogic.Utilites;
using NanoReality.GameLogic.Utilities;
using strange.extensions.signal.impl;

namespace NanoReality.GameLogic.Services
{
    public class ProductService : IProductService
    {
        [Inject] public IProductsData jProductsData { get; set; }
        [Inject] public IMapObjectsData jMapObjectsData { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }

        [Inject] public IUIManager jUIManager { get; set; }
        [Inject] public IGameCamera jCamera { get; set; }
        [Inject] public ShowWindowSignal jShowWindowSignal { get; set; }
        [Inject] public SignalOnShownPanel jSignalOnShownPanel { get; set; }

        #region Implementation of IProductService

        public Signal<IMapBuilding> OnGoToProductionBuilding { get; private set; }

        public Signal<IMapBuilding> OnGoToBuildingsShop { get; private set; }

        public ProductService()
        {
            OnGoToProductionBuilding = new Signal<IMapBuilding>();
            OnGoToBuildingsShop = new Signal<IMapBuilding>();
        }

        public void Init()
        {
        }

        public void Dispose()
        {
        }

        public Product GetProduct(Id<Product> productId)
        {
            return jProductsData.GetProduct(productId);
        }

        public string GetProductName(Id<Product> productId)
        {
            return GetProduct(productId)?.ProductName;
        }

        public string GetProductDescription(Id<Product> productId)
        {            
            var location = FindProductLocation(productId);
            if (location.Location == ProductLocation.Production || location.Location == ProductLocation.BuildingsShop)
            {
                var product = GetProduct(productId);
                return LocalizationUtils.GetProductSourceText(product.BuildingTypeId);
            }
            return LocalizationMLS.Instance.GetText(LocalizationKeyConstants.SHARED_TOOLTIP_DESC_TRADE);
        }

        public string GetProductGoToText(Id<Product> productId)
        {
            return LocalizationUtils.LocalizeL2(LocalizationKeyConstants.INFO_BTN_GO_TO_BUILDING);
        }

        public void GoToBuildingByProduct(Id<Product> productId)
        {
            var location = FindProductLocation(productId);
            location.GoToLocation();            
        }

        public int CalculateHardPrice(int productId, int productCount)
        {
            return productCount * GetProduct(productId).HardPrice;
        }

        public void GoToShop(Id<Product> productId)
        {
            var product = jProductsData.GetProduct(productId);
            GoToShop(product);
        }

        public bool CanGoToBuildingByProduct(Id<Product> productId)
        {
            var product = jProductsData.GetProduct(productId);

            if (product == null) return false;
            
            var model = jGameManager.FindMapObjectByTypeId<IMapObject>(product.BuildingTypeId) ?? 
                        jGameManager.FindMapObjectByType<IMapObject>(MapObjectintTypes.TradingShop);
            return model != null || jMapObjectsData.IsPresentBuildingIdAndLevel(product.BuildingTypeId, 0);
        }

        #endregion

        private ProductLocationInfo FindProductLocation(Id<Product> productId)
        {
            var product = jProductsData.GetProduct(productId);
            var model = jGameManager.FindMapObjectByTypeId<IMapObject>(product.BuildingTypeId);
            MapObjectView view;
            
            if (model == null)
            {
                return GetNoProduceLocationInfo(product);
            }
            
            if (model.MapObjectType == MapObjectintTypes.TradingShop)
            {
                return GetShopLocationInfo(product);
            }
            
            
            if (model.MapObjectType == MapObjectintTypes.OrderDesk)
            {
                view = jGameManager.GetMapObjectView(model);
                return GetSeaportLocationInfo(product, view);
            }
            
            if (model.MapObjectType == MapObjectintTypes.AgriculturalField)
            {
                var agriculturalField = ProductProduceUtils
                    .FindAgriculturalFieldWithProductOrEmpty(jGameManager, product.SectorID, productId);
                
                view = jGameManager.GetMapObjectView(agriculturalField);
                
                return GetLocationOnMap(view, productId.Value);
            }
            
            view = jGameManager.GetMapObjectView(model);            
            if (view == null)
            {
                return GetNoProduceLocationInfo(product);
            }

            return GetLocationOnMap(view, productId.Value);
        }
        
        private ProductLocationInfo GetSeaportLocationInfo(Product product, MapObjectView view)
        {
            return new ProductLocationInfo(ProductLocation.Production, () =>
            {
                jUIManager.HideAll();
                jCamera.FocusOnMapObject(view.MapObjectModel);
                jShowWindowSignal.Dispatch(typeof(SeaportPanelView) , product);
                OnGoToProductionBuilding.Dispatch(view.MapObjectModel as IMapBuilding);
            });
        }
        
        private ProductLocationInfo GetLocationOnMap(MapObjectView view, int productId)
        {
            void SetProductId(UIPanelView panelView)
            {
                jSignalOnShownPanel.RemoveListener(SetProductId);
                
                if (panelView is ProductionPanelView)
                {
                    ((ProductionPanelView)panelView).SetRequirementGoToProductId(productId);
                }
                else if (panelView is AgroFieldPanelView)
                {
                    ((AgroFieldPanelView)panelView).SetToolTipGoToProductId(productId);
                }
               
            }
            return new ProductLocationInfo(ProductLocation.Production, () =>
            {
                jUIManager.HideAll();
                jCamera.FocusOnMapObject(view.MapObjectModel);
                jSignalOnShownPanel.AddListener(SetProductId);
                 view.OnMapObjectViewTap();
                 OnGoToProductionBuilding.Dispatch(view.MapObjectModel as IMapBuilding);
            });
        }

        private ProductLocationInfo GetNoProduceLocationInfo(Product product)
        {
            var prototype = jMapObjectsData.GetByBuildingIdAndLevel(product.BuildingTypeId, 0) as IMapBuilding;
            if (prototype != null)
            {                
                return new ProductLocationInfo(ProductLocation.BuildingsShop, ()=> GoToBuildingsShop(prototype));
            }

            return GetShopLocationInfo(product);
        }

        private ProductLocationInfo GetShopLocationInfo(Product product)
        {
            return new ProductLocationInfo(ProductLocation.Market, () => GoToShop(product));
        }

        private void GoToBuildingsShop(IMapBuilding building)
        {
            jShowWindowSignal.Dispatch(typeof(BuildingsShopPanelView), building);
            OnGoToBuildingsShop.Dispatch(building);
        }

        private void GoToShop(Product product)
        {
            var model = jGameManager.FindMapObjectByType<IMapObject>(MapObjectintTypes.TradingShop);
            var view = jGameManager.GetMapObjectView(model);
            
            jUIManager.HideAll();
            jCamera.FocusOnMapObject(view.MapObjectModel);
            view.OnMapObjectViewTap();
        }
    }

    internal class ProductLocationInfo
    {
        private readonly ProductLocation _location;

        public ProductLocation Location
        {
            get { return _location; }
        }

        private readonly Action _action;

        public ProductLocationInfo(ProductLocation location, Action action)
        {
            _location = location;
            _action = action;
        }

        public void GoToLocation()
        {
            _action.SafeRaise();            
        }
    }

    internal enum ProductLocation
    {
        Market,
        BuildingsShop,
        Production
    }
}
