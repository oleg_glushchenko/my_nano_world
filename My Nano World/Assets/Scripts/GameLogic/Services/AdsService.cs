﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Engine.Analytics.Model.api;
using NanoLib.Core.Logging;
using NanoReality.Game.Advertisements;
using NanoReality.Game.Analytics;
using NanoReality.Game.Data;
using NanoReality.Game.Services.Advertisements;
using NanoReality.GameLogic.IAP;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using Sirenix.Utilities;

namespace NanoReality.GameLogic.Services
{
    public class AdsService : IAdsService
    {
        private readonly Dictionary<AdsType, IAdsWrapper> _adsWrappers = new Dictionary<AdsType, IAdsWrapper>();
        private AdsType _adsType = AdsType.Unity;

        private IAdsWrapper AdsWrapper => _adsWrappers[_adsType];
        public AdTypePanel PlayingAdType { get; private set; }
        public bool IsVideoLoaded => AdsWrapper.IsVideoLoaded;
        
        public event EventHandler VideoStarted;
        public event EventHandler VideoLoaded;
        public event EventHandler VideoSkipped;
        public event EventHandler VideoWatched;

        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IBuildSettings jBuildSettings { get; set; }
        [Inject] public INanoAnalytics jNanoAnalytics { get; set; }
        [Inject] public IAnalyticsManager jAnalyticsManager { get; set; }
        [Inject] public AdsWatchFinishedSignal jAdsWatchFinishedSignal { get; set; }

        public void Init()
        {
            PlayingAdType = AdTypePanel.None;
 
            _adsWrappers.Add(AdsType.Unity, new UnityAdsWrapper(jBuildSettings.UseTestAds, jBuildSettings.UnityAdsPlacementId, UnityAdsId(), jBuildSettings.DebugAds, jAnalyticsManager.PermissionsGranted));
            _adsWrappers.Add(AdsType.Google, new GoogleAdsWrapper(GoogleAdsId(), GoogleDeviceIds()));

            _adsWrappers.ForEach(pair =>
            {
                pair.Value.Initialize();
                pair.Value.VideoWatched += OnVideoWatched;
                pair.Value.VideoLoaded += OnVideoLoaded;
                pair.Value.VideoSkipped += OnVideoSkipped;
                pair.Value.VideoStarted += OnVideoStarted;
            });
        }

        public bool HasAvailableAdsByPanelType(AdTypePanel adType)
        {
            if (jPlayerProfile.AdData.TryGetValue(adType, out var data))
            {
                return data.AvailableAdsCount > 0;
            }
            
            Logging.LogError(LoggingChannel.Ads, $"PlayerProfile.AdData does not contain ad type: {adType} ");
            
            return true;
        }

        public void ShowVideo(AdTypePanel adType)
        {
            PlayingAdType = adType;
            AdsWrapper.ShowVideo();
        }

        public void LoadVideo()
        {
            AdsWrapper.LoadVideo();
        }

        public void Dispose()
        {
            _adsWrappers.ForEach(pair =>
            {
                pair.Value.VideoWatched -= OnVideoWatched;
                pair.Value.VideoLoaded -= OnVideoLoaded;
                pair.Value.VideoSkipped -= OnVideoSkipped;
                pair.Value.VideoStarted -= OnVideoStarted;
                pair.Value.Dispose();
            });
        }

        private void OnVideoStarted(object sender, EventArgs e)
        {
            VideoStarted?.Invoke(this, e);
        }

        private void OnVideoSkipped(object sender, EventArgs e)
        {
            VideoSkipped?.Invoke(this, e);
        }

        private void OnVideoLoaded(object sender, EventArgs e)
        {
            VideoLoaded?.Invoke(this, e);
        }

        private void OnVideoWatched(object sender, EventArgs e)
        {
            DecreaseAdsCount(PlayingAdType);
            SwitchToNextAdsType();

            jNanoAnalytics.OnAdsWatched(PlayingAdType.ToString());
            VideoWatched?.Invoke(this, e);
            jAdsWatchFinishedSignal.Dispatch(PlayingAdType);
        }
        
        private void DecreaseAdsCount(AdTypePanel adType)
        {
            if (jPlayerProfile.AdData.TryGetValue(adType, out var data))
            {
                --data.AvailableAdsCount;
            }
            else
            {
                Logging.LogError(LoggingChannel.Ads, $"PlayerProfile.AdData does not contain ad type: {adType} ");
            }
        }

        private void SwitchToNextAdsType()
        {
            var typesCount = Enum.GetValues(typeof(AdsType)).Length;
            _adsType = (AdsType) (((int) _adsType + 1) % typesCount);
        }

        private string UnityAdsId()
        {
#if UNITY_IOS
            return jBuildSettings.UnityAdsIOSId;
#else
            return jBuildSettings.UnityAdsAndroidId;
#endif
        }

        private List<string> GoogleDeviceIds()
        {
            var deviceIds = new List<string>();
#if UNITY_IOS
            deviceIds.AddRange(jBuildSettings.IOSTestDeviceIds);
#elif UNITY_ANDROID
            deviceIds.AddRange(jBuildSettings.AndroidTestDeviceIds);
#endif
            return deviceIds;
        }
        
        private string GoogleAdsId()
        {
#if UNITY_EDITOR
            return "unused";
#elif UNITY_ANDROID
            return jBuildSettings.GoogleRewardedAdsAndroidId;
#elif UNITY_IOS
            return jBuildSettings.GoogleRewardedAdsIOSId;
#else
            return "unexpected_platform";
#endif
        }
    }

    public enum AdsType
    {
        Unity,
        Google
    }
}
