﻿using NanoReality.Engine.UI.Views.QuestEvents;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.Quests;
using UnityEngine;

namespace NanoReality.GameLogic.Services
{
    public interface IIconProvider
    {
        void SetQuestImageIcon(QuestImageIcon questImageIcon, IQuest quest);

        Sprite GetSpecialOrderIcon(SpecialOrderType specialOrderType);

        Sprite GetSpecialOrderIcon(BusinessSectorsTypes sectorType);

        Sprite GetRewardBoxSprite(int giftBoxType);

        Sprite GetSprite(IAwardAction awardAction);

        Sprite GetSprite(IAwardItem rewardItem);

        Sprite GetServiceSprite(MapObjectintTypes serviceBuildingType);

        Sprite GetBuildingSprite(MapObjectId id);
        
        Sprite GetProductSprite(int productId);

        Sprite GetCurrencySprite(PriceType priceType);
    }
}