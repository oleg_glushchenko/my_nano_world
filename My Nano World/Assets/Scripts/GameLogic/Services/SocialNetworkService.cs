﻿using System;
using System.Collections.Generic;
using Assets.NanoLib.UtilitesEngine.CustomDebugging.Models.api;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.ServerCommunication.Models;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.SocialCommunicator.api;
using NanoReality.GameLogic.StartApplicationSequence.Controllers.Commands;
using strange.extensions.injector.api;
using UnityEngine;
using UnityEngine.SocialPlatforms;

namespace NanoReality.GameLogic.Services
{
    public class SocialNetworkService : ISocialNetworkService
    {
        #region Injects

        [Inject] public IDebug jDebug { get; set; }

        [Inject] public IPlayerProfile jPlayerProfile { get; set; }

        [Inject(SocialNetworkCommunicatorTypes.Primary)]
        public ISocialNetworkCommunicator PrimaryCommunicator { get; set; }
        [Inject] public IFacebookController jFacebookController { get; set; }

      
        // Signals

        [Inject] public SocialNetworkLogout jSocialNetworkLogout { get; set; }

        [Inject] public OnFriendsUpdatedSignal OnFriendsUpdatedSignal { get; set; }

        [Inject] public OnFriendInvitedSignal OnFriendInvitedSignal { get; set; }

        #endregion

        private readonly List<IUserProfile> _userProfiles = new List<IUserProfile>();

        private bool _isUpdatingFriendsList;
        private bool _isInvitingFriends;

        public void UpdateFriendsList()
        {
            
        }

        public void InviteFriends()
        {
           
        }

        private void OnFriendsCountUpdated(bool result)
        {
            if (!result)
            {
                jDebug.LogError("Could not update social friends count on server!");
            }
        }

        public void Init()
        {
            // jSignalOnAuthorizeSocialProfile.AddListener(OnAuthorizeSocialProfile); //todo do not forget to RemoveListener
            PrimaryCommunicator.OnInitializedSignal.AddListener(OnSocialNetworkInitialized);
            jFacebookController.Init();
            PrimaryCommunicator.Init();
        }

        public void Dispose()
        {
            PrimaryCommunicator.OnInitializedSignal.RemoveListener(OnSocialNetworkInitialized);
        }

        private void OnAuthorizeSocialProfile(AuthorizeResult result)
        {
           
        }

        private void OnSocialNetworkInitialized(ISocialNetworkCommunicator communicator)
        {
            communicator.OnInitializedSignal.RemoveListener(OnSocialNetworkInitialized);
            
            VerifyPlayerProfileLink(communicator);
        }

        private void VerifyPlayerProfileLink(ISocialNetworkCommunicator communicator)
        {
            // if player is logged in to social network but profile is not linked to it or does not match id
            // then we must log out from this network
            if (communicator.IsUserAuthorized &&
                (!jPlayerProfile.SocialAccounts.ContainsKey(communicator.SocialNetworkName) || 
                 jPlayerProfile.SocialAccounts[communicator.SocialNetworkName] != communicator.ActiveProfile.id))
            {
                jDebug.LogWarning("<b>SocialNetworkService:</b> player was logged in to <b>" + 
                                  communicator.SocialNetworkName + 
                                  "</b> but the account was not linked to the profile. Logging out...");
                communicator.Logout((res, com) =>
                {
                    if (res)
                    {
                        jSocialNetworkLogout.Dispatch();
                    }
                    else
                    {
                        jDebug.LogWarning("<b>SocialNetworkService:</b> could not log out from <b>" + 
                                          com.SocialNetworkName + "</b>");
                    }
                });
            }
        }
    }
}
