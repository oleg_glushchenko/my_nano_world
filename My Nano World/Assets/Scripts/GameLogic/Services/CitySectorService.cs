using System.Collections.Generic;
using System.Linq;
using GameLogic.BuildingSystem.CityArea;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityArea;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.GameManager.Models.api;
using UnityEngine;

namespace NanoReality.GameLogic.Services
{
    public class CitySectorService : ICitySectorService
    {
        [Inject] public ICityAreaData jCityAreaData { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public SignalOnSubSectorsUpdated jSignalOnSubSectorsUpdated { get; set; }
        
        private readonly Dictionary<BusinessSectorsTypes, List<ISectorArea>> _areasBySectorType = 
            new Dictionary<BusinessSectorsTypes, List<ISectorArea>>();
        private Rect? _dragAreaLimits;

        public void Init()
        {
            InitAreaSectors();
            InitLockedSectors();
        }

        public void Dispose()
        {
            
        }

        public bool ContainsDisableAreas(BusinessSectorsTypes sectorType, Vector2Int position, int objectTypeId)
        {
            if (_areasBySectorType.TryGetValue(sectorType, out var list))
            {
                var count = list.Count;
                
                for (var i = 0; i < count; i++)
                {
                    ISectorArea area = list[i];

                    if (area.Contains(position) && !area.ContainsBuildingTypeId(objectTypeId))
                    {
                        return true;
                    }
                }
            }
            
            return false;
        }

        private bool ContainsDisableAreas(IReadOnlyList<ISectorArea> areas, Vector2Int position)
        {
            for (var i = 0; i < areas.Count; i++)
            {
                ISectorArea area = areas[i];
                if (area.Contains(position) && area.IsDisabled)
                {
                    return true;
                }
            }
            
            return false;
        }
        
        public bool ContainsDisabledAreas(Vector2Int leftBottomPos, Vector2Int dimension, IReadOnlyList<ISectorArea> areas, int objectTypeId)
        {
            if (areas == null)
            {
                return false;
            }
            
            bool enabledType = areas.Any(area => area.ContainsBuildingTypeId(objectTypeId));

            if (!enabledType)
            {
                foreach (var area in areas)
                {
                    if (area.Overlaps(leftBottomPos, dimension))
                        return true;
                }

                return false;
            }

            int enablePoints = 0;
            const int squarePoints = 4;
            
            foreach (var area in areas)
            {
                if (!area.ContainsBuildingTypeId(objectTypeId))
                    continue;
                
                if (area.Contains(leftBottomPos))
                    ++enablePoints;
                
                if(area.Contains(leftBottomPos + (dimension - Vector2Int.one)))
                    ++enablePoints;
                
                if(area.Contains(new Vector2Int(leftBottomPos.x + (dimension.x - 1), leftBottomPos.y)))
                    ++enablePoints;
                
                if(area.Contains(new Vector2Int(leftBottomPos.x, leftBottomPos.y + (dimension.y - 1))))
                    ++enablePoints;
            }

            return enablePoints < squarePoints;
        }
        
        public void UpdateLockedMap(MapSize mapSize, List<CityLockedSector> lockedSectors, BusinessSectorsTypes businessSectorsTypes)
        {
            if (lockedSectors.Count == 0) return;

            IReadOnlyList<ISectorArea> areas = SectorAreas(businessSectorsTypes);

            for (var index = 0; index < lockedSectors.Count; index++)
            {
                ILockedSector sector = lockedSectors[index].LockedSector;
                sector.Unlockable = false;

                UpdateLockedSector(sector, areas, mapSize, lockedSectors);
            }
        }
        
        public IReadOnlyList<ISectorArea> SectorAreas(BusinessSectorsTypes sectorType) =>
            _areasBySectorType.TryGetValue(sectorType, out var list) ? list.AsReadOnly() : null;

        public IReadOnlyList<CityLockedSector> LockedSectorsByCityType(BusinessSectorsTypes sectorType) =>
            jGameManager.CurrentUserCity.GetBusinessSectorById(sectorType).SubLockedMap.LockedSectors;

        private void InitAreaSectors()
        {
            foreach (ISectorArea area in jCityAreaData.Areas)
            {
                BusinessSectorsTypes sectorType = area.BusinessSectorId.ToBusinessSectorType();

                if (!_areasBySectorType.TryGetValue(sectorType, out List<ISectorArea> sectorAreas))
                {
                    sectorAreas = new List<ISectorArea>();
                    _areasBySectorType[sectorType] = sectorAreas;
                }
                
                area.Init();
                sectorAreas.Add(area);
            }
        }

        private void InitLockedSectors()
        {
            foreach (var map in jGameManager.CurrentUserCity.CityMaps)
            {
                UpdateLockedMap(map.MapSize, map.SubLockedMap.LockedSectors, map.BusinessSector);
            }
            jSignalOnSubSectorsUpdated.Dispatch();
        }

        private void UpdateLockedSector(ILockedSector sector, IReadOnlyList<ISectorArea> areas, MapSize mapSize, IEnumerable<CityLockedSector> lockedSectors)
        {
            for (var x = sector.X; x < sector.X + sector.Width; x++)
            {
                if (sector.Y > 0)
                {
                    var downGridPos = new Vector2Int(x, sector.Y - 1);

                    if (ContainsUnlockedEnabledGrid(downGridPos, areas, lockedSectors))
                    {
                        sector.Unlockable = true;
                        return;
                    }
                }
                    
                if (sector.Y + sector.Height >= mapSize.Height) continue;
                    
                var upGridPos = new Vector2Int(x, sector.Y + sector.Height);
                    
                if (ContainsUnlockedEnabledGrid(upGridPos, areas, lockedSectors))
                {
                    sector.Unlockable = true;
                    break;
                }
            }

            for (var y = sector.Y; y < sector.Y + sector.Height; y++)
            {
                if (sector.X > 0)
                {
                    var leftGridPos = new Vector2Int(sector.X - 1, y);
                        
                    if (ContainsUnlockedEnabledGrid(leftGridPos, areas, lockedSectors))
                    {
                        sector.Unlockable = true;
                        return;
                    }
                }

                if (sector.X + sector.Width >= mapSize.Width) continue;
                    
                var leftGridRight = new Vector2Int(sector.X + sector.Width, y);
                    
                if (ContainsUnlockedEnabledGrid(leftGridRight, areas, lockedSectors))
                {
                    sector.Unlockable = true;
                    return;
                }
            }
        }
        
        private bool ContainsUnlockedEnabledGrid(Vector2Int gridPos, IReadOnlyList<ISectorArea> areas, IEnumerable<CityLockedSector> lockedSectors)
        {
            if (ContainsDisableAreas(areas, gridPos))
                return false;
            
            if(lockedSectors.Any(ss => ss.LockedSector.Contains(gridPos)))
                return false;

            return true;
        }
        
        public void SetDragLimits(Rect? newLimits)
        {
            _dragAreaLimits = newLimits;
        }

        public bool IsInsideOfDragArea(Vector2Int position, Vector2Int dimensions)
        {
            if (_dragAreaLimits == null)
                return true;
			
            var rect = (Rect)_dragAreaLimits;
            return rect.Contains(position) && rect.Contains(position + dimensions - Vector2Int.one);
        }
    }
}