﻿using System;
using System.Collections.Generic;
using NanoLib.Core.Logging;
using NanoReality.Core.Resources;
using NanoReality.Engine.UI.Views.QuestEvents;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.Quests;
using NanoReality.StrangeIoC;
using UnityEngine;
using NanoReality.Game.Attentions;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using Sirenix.OdinInspector;
using Sirenix.Serialization;

namespace NanoReality.GameLogic.Services
{
    [ShowOdinSerializedPropertiesInInspector]
    public class IconProvider : AViewMediator, IIconProvider, ISerializationCallbackReceiver, ISupportsPrefabSerialization
    {
        [Header("Condition Icons:")] 
        [SerializeField] private List<ConditionIcon> _conditionIcons;
        [SerializeField] private Sprite _defaultConditionIcon;

        [Header("Service Building Type Icons:")]
        [OdinSerialize] private Dictionary<MapObjectintTypes, Sprite> _serviceBuildingTypeIcons;
        [SerializeField] private Sprite _defaultServiceBuildingTypeIcon;
        [SerializeField] private Sprite _warehouseIcon;

        [Header("Special Order Icons:")] 
        [SerializeField] private Sprite _specialOrderAwardFarmIcon;
        [SerializeField] private Sprite _specialOrderAwardHeavyIcon;
        [SerializeField] private Sprite _specialOrderAwardTownIcon;

        [Header("Reward Boxes:")] 
        [SerializeField] private Sprite[] _rewardBoxSprites;
        
        [Header("Award Icons:")]
        [SerializeField] private Sprite _softCurrencyAwardIcon;
        [SerializeField] private Sprite _hardCurrencyAwardIcon;
        [SerializeField] private Sprite _goldCurrencyIcon;
        [SerializeField] private Sprite _lanternCurrencyIcon;
        [SerializeField] private Sprite _experienceAwardIcon;
        [SerializeField] private Sprite _levelUpAwardIcon;
        [SerializeField] private Sprite _defaultAwardIcon;

        private Dictionary<ConditionTypes, Sprite>  _conditionIconsByType;
        
        [Inject] public IResourcesManager jResourcesManager { get; set; }
        [Inject] public IMapObjectsGenerator jMapObjectsGenerator { get; set; }
        [Inject] public ResourcesPolicy jResourcesPolicy { get; set; }

        protected override void PreRegister()
        {
            _conditionIconsByType = new Dictionary<ConditionTypes, Sprite>(_conditionIcons.Count);
            for (int i = 0; i < _conditionIcons.Count; i++)
            {
                var icon = _conditionIcons[i];
                _conditionIconsByType[icon.ConditionType] = icon.Icon;
            }
        }

        protected override void OnRegister()
        {
        }

        protected override void OnRemove()
        {
        }

        public void SetQuestImageIcon(QuestImageIcon questImageIcon, IQuest quest)
        {
            if (quest != null && quest.AchiveConditions != null && quest.AchiveConditions.Count > 0)
            {
                SetQuestImageIcon(questImageIcon, quest.AchiveConditions[0]);
            }
            else
            {
                questImageIcon.Set(_defaultConditionIcon);
            }
        }

        public void SetQuestImageIcon(QuestImageIcon questImageIcon, ICondition condition)
        {
            if (condition == null)
            {
                questImageIcon.Set(_defaultConditionIcon);
                return;
            }

            switch (condition.ConditionType)
            {
                case ConditionTypes.ProductProduced:
                case ConditionTypes.ProductsUniqueAmountProduced:
                    questImageIcon.Set(jResourcesManager.GetProductSprite(condition.GetConditionRequirement().Id));
                    break;
                case ConditionTypes.RepairBuilding:
                case ConditionTypes.HaveBuilding:
                case ConditionTypes.BuildingHaveById:
                case ConditionTypes.ConstructBuilding:
                case ConditionTypes.BuildingConstructById:
                case ConditionTypes.BuildingUnlocked:
                case ConditionTypes.CoverBuildingsWithAreaOfEffect:
                {
                    var requirementId = condition.GetConditionRequirement().Id;
                    //27 - warehouse, the virtual building which isn't present in the scene and has no configuration but can update
                    if (requirementId == 27)
                    {
                        questImageIcon.Set(_warehouseIcon);
                        break;
                    }

                    var sprite = GetBuildingSprite(new MapObjectId(requirementId, condition.Level));
                    questImageIcon.Set(sprite);
                    break;  
                }
                case ConditionTypes.CoverAmountBuildingsByService:
                {
                    int requirementId = condition.GetConditionRequirement().Id;
                    questImageIcon.Set(GetServiceSprite((MapObjectintTypes) requirementId));
                    break;
                }
                case ConditionTypes.BuildingConstructByType:
                case ConditionTypes.BuildingHaveByType:
                {
                    questImageIcon.Set(GetConditionSprite(ConditionTypes.ReachPopulation));
                    break;
                }
                default:
                    questImageIcon.Set(GetConditionSprite(condition.ConditionType));
                    break;
            }
        }

        public Sprite GetSpecialOrderIcon(SpecialOrderType specialOrderType)
        {
            switch (specialOrderType)
            {
                case SpecialOrderType.Farm:
                    return _specialOrderAwardFarmIcon;
                case SpecialOrderType.Heavy:
                    return _specialOrderAwardHeavyIcon;
                case SpecialOrderType.Town:
                    return _specialOrderAwardTownIcon;
            }
            return null;
        }

        public Sprite GetSpecialOrderIcon(BusinessSectorsTypes sectorType)
        {
            switch (sectorType)
            {
                case BusinessSectorsTypes.Farm:
                    return _specialOrderAwardFarmIcon;
                case BusinessSectorsTypes.HeavyIndustry:
                    return _specialOrderAwardHeavyIcon;
                case BusinessSectorsTypes.Town:
                    return _specialOrderAwardTownIcon;
            }
            return null;
        }

        private Sprite GetConditionSprite(ConditionTypes conditionType)
        {
            Sprite sprite;
            return _conditionIconsByType.TryGetValue(conditionType, out sprite) ? sprite : _defaultConditionIcon;
        }

        public Sprite GetRewardBoxSprite(int giftBoxType)
        {
            if (_rewardBoxSprites.Length > 0)
            {
                giftBoxType = Mathf.Clamp(giftBoxType, 0, _rewardBoxSprites.Length - 1);
                return _rewardBoxSprites[giftBoxType];
            }
            return null;
        }

        public Sprite GetSprite(IAwardAction awardAction)
        {
            switch (awardAction.AwardType)
            {
                case AwardActionTypes.Experience:
                    return _experienceAwardIcon;
                case AwardActionTypes.SoftCurrency:
                    return _softCurrencyAwardIcon;
                case AwardActionTypes.PremiumCurrency:
                    return _hardCurrencyAwardIcon;
                case AwardActionTypes.Resources:
                    var giverProductAction = awardAction as IGiveProductAward;
                    if (giverProductAction != null)
                    {
                        return jResourcesManager.GetProductSprite(giverProductAction.Product.Value);
                    }
                    break;
                case AwardActionTypes.LevelUp:
                    return _levelUpAwardIcon;
                case AwardActionTypes.GiveRandomProduct:
                    var giverRandomProductAction = awardAction as IGiveRandomProductAward;
                    if (giverRandomProductAction != null)
                    {
                        return jResourcesManager.GetProductSprite(giverRandomProductAction.Product.Value);
                    }
                    break;
                case AwardActionTypes.GoldBars:
                    return _goldCurrencyIcon;
            }
            return _defaultAwardIcon;
        }

        public Sprite GetSprite(IAwardItem rewardItem)
        {
            if (rewardItem.HaveCoins())
            {
                return _softCurrencyAwardIcon;
            }

            if (rewardItem.HaveNanoBucks())
            {
                return _hardCurrencyAwardIcon;
            }

            if (rewardItem.HaveGoldBars())
            {
                return _goldCurrencyIcon;
            }

            if (rewardItem.HaveProduct())
            {
                return jResourcesManager.GetProductSprite(rewardItem.Product.ProductId.Value);
            }

            return null;
        }

        public Sprite GetServiceSprite(MapObjectintTypes serviceBuildingType)
        {
            return _serviceBuildingTypeIcons.TryGetValue(serviceBuildingType, out Sprite sprite) ? sprite : _defaultServiceBuildingTypeIcon;
        }

        public Sprite GetBuildingSprite(MapObjectId id)
        {
            MapObjectId visualMapObjectId = jMapObjectsGenerator.CalculateBuildingVisualStyleLevel(id);
            return jResourcesManager.GetBuildingSprite(visualMapObjectId);
        }
        
        public Sprite GetProductSprite(int productId)
        {
            return jResourcesPolicy.GetProductSprite(productId);
        }

        public Sprite GetCurrencySprite(PriceType priceType)
        {
            Sprite currencySprite = null;
            switch (priceType)
            {
                case PriceType.Soft:
                {
                    currencySprite = _softCurrencyAwardIcon;
                    break;
                }
                case PriceType.Hard:
                {
                    currencySprite = _hardCurrencyAwardIcon;
                    break;
                }
                case PriceType.Gold:
                {
                    currencySprite = _goldCurrencyIcon;
                    break;
                }
                case PriceType.Lantern:
                {
                    currencySprite = _lanternCurrencyIcon;
                    break;
                }
                default:
                {
                    Logging.LogError($"Not valid price type. Game haven't icon for this price type [{priceType}]");
                    break;
                }
            }

            return currencySprite;
        }

        [Serializable]
        public class AttentionIcon
        {
            public AttentionsTypes AttentionsType;
            public Sprite Icon;
        }

        [Serializable]
        public class ConditionIcon
        {
            public ConditionTypes ConditionType;
            public Sprite Icon;
        }
        
        #region Odin Serialize implementation

        [SerializeField, HideInInspector]
        private SerializationData _serializationData;

        SerializationData ISupportsPrefabSerialization.SerializationData
        {
            get => _serializationData;
            set => _serializationData = value;
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            UnitySerializationUtility.DeserializeUnityObject(this, ref _serializationData);
        }

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
            UnitySerializationUtility.SerializeUnityObject(this, ref _serializationData);
        }
        
        #endregion
    }
}
