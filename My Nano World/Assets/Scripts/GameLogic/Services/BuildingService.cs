﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.UtilitesEngine.CustomDebugging.Models.api;
using Assets.Scripts.Engine.Analytics;
using Assets.Scripts.Engine.Analytics.Extensions.CurrencyInfo;
using Assets.Scripts.Engine.Analytics.Model.api;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl;
using NanoReality.Engine.UI.Extensions.EditModePanels;
using NanoReality.Engine.UI.Views.BuildingsShopPanel;
using NanoReality.Game.Services.BuildingsUnlockService;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using UnityEngine;

namespace NanoReality.GameLogic.Services
{
    public sealed class BuildingService : IBuildingService
    {
        [Inject] public SignalOnBuildStared jSignalOnBuildStared { get; set; }
        [Inject] public SignalAnalyticBuildStared jSignalAnalyticBuildStared { get; set; }
        [Inject] public SignalOnBuildStartedVerified jSignalOnBuildStartedVerified { get; set; }
        [Inject] public MapBuildingVerifiedSignal jMapBuildingVerifiedSignal { get; set; }
        [Inject] public SignalOnActionWithCurrency jSignalOnActionWithCurrency { get; set; }
        [Inject] public INanoAnalytics jNanoAnalytics { get; set; }
        [Inject] public IBuildingsUnlockService jBuildingsUnlockService { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public SignalOnMapObjectBuildFinished OnMapObjectBuildingFinishedSignal { get; set; }
        [Inject] public IEditModeService jEditModeService { get; set; }

        public void Init()
        {
            
        }

        public void Dispose()
        {
            
        }

        public BuildingsShopItemData GetBuildingShopItemData(IMapObject mapObject)
        {
            int buildingId = mapObject.ObjectTypeID;
            
            int playerLevel = jPlayerProfile.CurrentLevelData.CurrentLevel;
            int unlockLevel = jBuildingsUnlockService.GetBuildingUnlockLevel(buildingId);
            int currentBuildingsCount = jGameManager.GetMapObjectsCountInCity(buildingId);
            int buildingsCountLimit = jBuildingsUnlockService.GetMaxAvailableInstancesCount(buildingId, jPlayerProfile.CurrentLevelData.CurrentLevel);

            var state = BuildingState.CanBeBuild;

            if (buildingsCountLimit > 0 && currentBuildingsCount >= buildingsCountLimit)
            {
                if(!jBuildingsUnlockService.GetNextAvailableInstancesCount(buildingId, out int nextAvailableCount, out int nextPLayerLevel))
                {
                    state = BuildingState.MaxCount;
                }
                else
                {
                    unlockLevel = nextPLayerLevel;
                    state = BuildingState.UnlockAt;
                }
            }
            else if (playerLevel < unlockLevel)
            {
                state = BuildingState.UnlockAt;
            }
            else if(buildingsCountLimit == 0)
            {
                state = BuildingState.Locked;
            }

            var data = new BuildingsShopItemData
            {
                MapObjectData = mapObject,
                UnlockLevel = unlockLevel,
                CurrentBuildingsCount = currentBuildingsCount,
                LimitBuildingCount = buildingsCountLimit,
                BuildingState = state,
            };

            if (state == BuildingState.CanBeBuild)
            {
                data.NextInstancePrice = jBuildingsUnlockService.GetNewBuildingInstancePrice(buildingId);
            }

            return data;
        }
        
        public void SyncBuilding(MapBuilding mapBuilding, Action<SyncBuildingResponse> successCallback = null)
        {
            mapBuilding.IsVerificated = false;

            void OnComplete(SyncBuildingResponse response)
            {
                mapBuilding.IsVerificated = true;
                successCallback?.Invoke(response);
                
                jMapBuildingVerifiedSignal.Dispatch(mapBuilding);
            }

            jServerCommunicator.SyncBuilding(mapBuilding.MapID, OnComplete);
        }

        public void FinalizeMapObjectConstruction(IMapBuilding mapObject, Action<SyncBuildingResponse> successCallback = null)
        {
            mapObject.IsVerificated = false;

            void OnNetworkCallback(SyncBuildingResponse syncBuildingResponse)
            {
                int resultExperience = GetBuildingConstructionReward(mapObject.MapID);
                jPlayerProfile.CurrentLevelData.AddExperience(resultExperience, true);
                jNanoAnalytics.OnExperienceUpdate(AwardSourcePlace.FinishBuilding, mapObject.Id.ToString(), resultExperience);
                
                successCallback?.Invoke(syncBuildingResponse);
            }

            void OnWaitConstructedResponse(IMapObject constructedMapObject)
            {
                if (mapObject == constructedMapObject)
                {
                    jSignalOnBuildStartedVerified.RemoveListener(OnWaitConstructedResponse);
                    jServerCommunicator.SyncBuilding(mapObject.MapID, OnNetworkCallback);
                }
            }

            if (mapObject.MapID <= 0)
            {
                jSignalOnBuildStartedVerified.AddListener(OnWaitConstructedResponse);
            }
            else
            {
                jServerCommunicator.SyncBuilding(mapObject.MapID, OnNetworkCallback);
            }
        }

        public void ConstructMapObject(IMapObject mapObject, Action<int> successCallback)
        {
            var buildingId = mapObject.ObjectTypeID;
            
            IEnumerable<IMapObject> currentBuildingInstances = 
                jGameManager.GetMapObjectsById(buildingId).Where(c => c.MapID != mapObject.MapID);
            var newInstanceIndex = currentBuildingInstances.Count() + 1;

            Price price = jBuildingsUnlockService.GetBuildingInstancePrice(buildingId, newInstanceIndex);
            if (price != default)
            {
                jPlayerProfile.PlayerResources.BuyItem(price);
                jNanoAnalytics.OnPlayerCurrenciesDecrease(price,
                    CurrenciesSpendingPlace.ConstructMapObject, mapObject.Id.Id.ToString());
                jSignalOnActionWithCurrency.Dispatch(new SoftInfo(CurrencyConsumeActions.Build, price, mapObject.Name));
            }

            mapObject.InstanceIndex = newInstanceIndex;

            int constructionTime = GetBuildingConstructionTime(buildingId, 0, newInstanceIndex);
            
            mapObject.IsVerificated = false;
            mapObject.StartConstruction(constructionTime);
            
            void OnComplete(int buildingMapId)
            {
                mapObject.IsVerificated = true;
                mapObject.CommitBuilding(buildingMapId);

                jSignalOnBuildStartedVerified.Dispatch(mapObject);
                successCallback?.Invoke(buildingMapId);
            }

            var buildingPosition = new Vector2Int(mapObject.MapPosition.intX, mapObject.MapPosition.intY);
            
            jServerCommunicator.ConstructMapObject(mapObject.SectorId.Value, buildingId, buildingPosition,  OnComplete);

            jSignalOnBuildStared.Dispatch(mapObject);
            jSignalAnalyticBuildStared.Dispatch(mapObject, price.PriceType);
        }

        public void ConstructRoad(int businessSectorId, int type, List<IMapObject> roadsList, Action<CreateRoadResponse> onSuccess)
        {
            List<Vector2Int> roadPositions = roadsList.Select(c => c.MapPosition.ToVector2Int()).ToList();

            foreach (IMapObject roadObject in roadsList)
            {
                roadObject.StartConstruction(0);
            }

            OnMapObjectBuildingFinishedSignal.Dispatch(roadsList.Last());
           
            if (jEditModeService.IsEditModeEnable)
            {
                jEditModeService.AddRoad(roadPositions, businessSectorId.ToString(), onSuccess);
            }
            else
            {
                jServerCommunicator.ConstructRoad(businessSectorId, type, roadPositions, onSuccess);
            }
        }

        public void UpgradeBuilding(IMapObject upgradedBuilding)
        {
            
        }

        public int GetBuildingConstructionTime(int buildingId, int buildingLevel, int instanceIndex)
        {
            int constructionTime = jBuildingsUnlockService.GetBuildingConstructionTime(
                buildingId, buildingLevel, instanceIndex);

            return constructionTime;
        }

        public int GetBuildingConstructionTime(int buildingMapId)
        {
            var mapBuilding = jGameManager.FindMapObjectById<IMapBuilding>(buildingMapId);

            var buildingId = mapBuilding.ObjectTypeID.Value;
            var level = mapBuilding.Level + 1;
            var constructionTime = GetBuildingConstructionTime(buildingId, level, mapBuilding.InstanceIndex);
            
            // Debug.LogError($"Construction time for building [{buildingMapId}] Default: [{constructionTime}] Multiplier: [{timeMultiplier}] " +
            //                $"Result: [{(int)(constructionTime * timeMultiplier)}]");
            
            return constructionTime;
        }

        public int GetBuildingConstructionReward(int buildingMapId)
        {
            IMapBuilding buildingInstance = jGameManager.FindMapObjectById<IMapBuilding>(buildingMapId);
            var buildingId = buildingInstance.ObjectTypeID;
            var buildingLevel = buildingInstance.Level;
            var instanceIndex = buildingInstance.InstanceIndex;

            var experienceReward = jBuildingsUnlockService.GetExperienceReward(buildingId, buildingLevel, instanceIndex);
            return experienceReward;
        }
        
        public int GetConstructionTime(IMapObject mapObject)
        {
            var buildingId = mapObject.ObjectTypeID;
            var buildingLevel = mapObject.Level;
            var instanceIndex = mapObject.InstanceIndex;

            return jBuildingsUnlockService.GetBuildingConstructionTime(buildingId, buildingLevel, instanceIndex);;
        }
    }
}
