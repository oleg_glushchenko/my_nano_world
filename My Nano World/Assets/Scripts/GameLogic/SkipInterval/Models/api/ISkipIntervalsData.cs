﻿using NanoReality.GameLogic.StartApplicationSequence.Models.api;

public interface ISkipIntervalsData : IGameBalanceData
{
    int GetCurrencyForTime(int timeInSeconds);

    int GetCurrencyForTime(float timeInSeconds);
}