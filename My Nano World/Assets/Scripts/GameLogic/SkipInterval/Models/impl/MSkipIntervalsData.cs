﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.Utilities.DataManager.Models.api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.StartApplicationSequence.Models.api;
using Newtonsoft.Json;
using UnityEngine;

public class MSkipIntervalsData : ISkipIntervalsData
{
    [JsonProperty("skip_intervals")]
    public List<SkipIntervalPrice> SkipIntervals { get; set; }
    
    public int GetCurrencyForTime(int timeInSeconds)
    {
        // пробегаемся по всем временным интервалам
        foreach (var skipInterval in SkipIntervals)
        {
            // стоимость выбирается в пользу большего времени
            // (если время 6 секунд, при предыдущем интервале 5,
            // и следующем - 10, выбираем стоимость за 10
            if (timeInSeconds < skipInterval.TimeSkip)
            {
                return skipInterval.Currency;
            }
        }

        // если мы попали сюда, значит дошли до самого последнего
        // (максимального по времени) элемента, и тот оказался
        // меньшим чем текущее время. В таком случае возвращаем
        // максимальный, то есть последний:
        return SkipIntervals[SkipIntervals.Count - 1].Currency;
    }

    /// <summary>
    /// Rounds to int float value of seconds
    /// </summary>
    /// <param name="timeInSeconds">Seconds</param>
    /// <returns></returns>
    public int GetCurrencyForTime(float timeInSeconds)
    {
        return GetCurrencyForTime(Mathf.RoundToInt(timeInSeconds));
    }

    public string DataHash { get; set; }

    public void LoadData(IServerCommunicator serverCommunicator, ILocalDataManager localDataManager,
        Action<IGameBalanceData> callback)
    {
        serverCommunicator.LoadSkipIntervalsBalanceData(OnIntervalsLoadedFromServer);
        
        void OnIntervalsLoadedFromServer(SkipIntervalsResponse data)
        {
            SkipIntervals = data.SkipIntervals.OrderBy(interval => interval.TimeSkip).ToList();
            callback?.Invoke(this);
        }
    }
}

public sealed class SkipIntervalsResponse
{
    [JsonProperty("skip_intervals")] 
    public List<SkipIntervalPrice> SkipIntervals;
}