﻿using Newtonsoft.Json;

public sealed class SkipIntervalPrice
{
    [JsonProperty("time_skip")]
    public int TimeSkip;

    [JsonProperty("price")]
    public int Currency;

    public SkipIntervalPrice()
    {

    }

    public SkipIntervalPrice(int timeSkip, int currency)
    {
        TimeSkip = timeSkip;
        Currency = currency;
    }
}