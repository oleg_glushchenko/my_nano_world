﻿using System;
using NanoLib.Core.Services.Sound;
using NanoReality.Game.Notifications;
using NanoReality.Game.UI.SettingsPanel;
using NanoReality.GameLogic.Settings.GamePrefs;

namespace NanoReality.GameLogic.Settings.GameSettings.Models.Api
{
    public class GameSettings : IGameSettings
    {
        [Inject] public IGamePrefs jGamePrefs { get; set; }
        [Inject] public ISoundManager jSoundManager { get; set; }
        [Inject] public CancelAllPushNotificationSignal jCancelAllPushNotificationSignal { get; set; }

        private ToggleSettings _toggleSettings;

        private const string GameSettingsKey = "PlayerGameSettings";

        [PostConstruct]
        public void PostConstruct()
        {
            _toggleSettings = jGamePrefs.GetObject<ToggleSettings>(GameSettingsKey);

            SwitchMusic(_toggleSettings.IsMusicEnabled);
            SwitchEffects(_toggleSettings.IsEffectsEnabled);
            SwitchPushes(_toggleSettings.IsPushNotificationsEnabled);
        }

        private ToggleSettings Settings
        {
            get
            {
                if (_toggleSettings != null)
                {
                    return _toggleSettings;
                }

                _toggleSettings = jGamePrefs.GetObject<ToggleSettings>(GameSettingsKey);

                SwitchMusic(_toggleSettings.IsMusicEnabled);
                SwitchEffects(_toggleSettings.IsEffectsEnabled);
                SwitchPushes(_toggleSettings.IsPushNotificationsEnabled);

                return _toggleSettings;
            }
        }

        public bool IsMusicEnabled
        {
            get => Settings.IsMusicEnabled;
            set
            {
                Settings.IsMusicEnabled = value;
                SaveSettings();
                SwitchMusic(value);
            }
        }

        public bool IsEffectsEnabled
        {
            get => Settings.IsEffectsEnabled;
            set
            {
                Settings.IsEffectsEnabled = value;
                SaveSettings();
                SwitchEffects(value);
            }
        }

        public bool IsPushNotificationsEnabled
        {
            get => Settings.IsPushNotificationsEnabled;
            set
            {
                Settings.IsPushNotificationsEnabled = value;
                SaveSettings();
                SwitchPushes(value);
            }
        }

        public bool IsHighQualityGraphic
        {
            get => Settings.IsHighQualityGraphic;
            set
            {
                if (Settings.IsHighQualityGraphic != value)
                    GraphicWasChanged = true;
                
                Settings.IsHighQualityGraphic = value;
                SaveSettings();
            }
        }
        
        public bool GraphicWasChanged
        {
            get => Settings.GraphicWasChanged;
            set
            {
                Settings.GraphicWasChanged = value;
                SaveSettings();
            }
        }

        public bool IsEnabled(SettingsToggleType type)
        {
            switch (type)
            {
                case SettingsToggleType.Music: return IsMusicEnabled;
                case SettingsToggleType.Effects: return IsEffectsEnabled;
                case SettingsToggleType.Notifications: return IsPushNotificationsEnabled;
                case SettingsToggleType.GraphicQuality: return IsHighQualityGraphic;
                default: return false;
            }
        }

        public void SetEnabled(SettingsToggleType type, bool value)
        {
            switch (type)
            {
                case SettingsToggleType.Music:
                    IsMusicEnabled = value;
                    break;

                case SettingsToggleType.Effects:
                    IsEffectsEnabled = value;
                    break;

                case SettingsToggleType.Notifications:
                    IsPushNotificationsEnabled = value;
                    break;

                case SettingsToggleType.GraphicQuality:
                    IsHighQualityGraphic = value;
                    break;
            }
        }

        private void SaveSettings()
        {
            jGamePrefs.SetObject(GameSettingsKey, Settings);
            jGamePrefs.Save();
        }

        private void SwitchMusic(bool isOn)
        {
            jSoundManager.SetChannelActive(isOn, SoundChannel.Music);
        }

        private void SwitchEffects(bool isOn)
        {
            jSoundManager.SetChannelActive(isOn, SoundChannel.SoundFX);
        }

        private void SwitchPushes(bool isOn)
        {
            if (!isOn)
                jCancelAllPushNotificationSignal.Dispatch();
        }

        [Serializable]
        private class ToggleSettings
        {
            public bool IsMusicEnabled = true;
            public bool IsEffectsEnabled = true;
            public bool IsPushNotificationsEnabled = true;
            public bool IsHighQualityGraphic;
            public bool GraphicWasChanged;
        }
    }
}