﻿using System;
using System.Collections.Generic;
using Assets.Scripts.GameLogic.DailyBonus.Impl;
using NanoReality.GameLogic.StartApplicationSequence.Models.api;

namespace Assets.Scripts.GameLogic.DailyBonus.Api
{
    public interface IDailyBonusBalanceData : IGameBalanceData
    {
        List<DailyBonusData> DailyBonusDatas { get; set; }

        void CollectDailyBonus(DailyBonusData dailyBonusData, Action onSuccess);

        void CollectDailyBonus(int day, Action onSuccess);

        DailyBonusData GetDailyBonusDataByDay(int day);
    }
}
