﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NanoReality.GameLogic.Quests;

namespace Assets.Scripts.GameLogic.DailyBonus.Api
{
    public interface IAwardActionData
    {
        AwardActionTypes ActionType { get; set; }

        List<DefaultActionParam> ActionParams { get; set; } 
    }

    public struct DefaultActionParam
    {

        public string AttributeName { get; set; }

        public int AttributeValue { get; set; }
    }
}
