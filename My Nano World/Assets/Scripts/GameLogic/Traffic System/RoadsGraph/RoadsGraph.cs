﻿using NanoReality.Engine.BuildingSystem.MapObjects;
using UnityEngine;
using NanoReality.GameLogic.TrafficSystem.Road;
using System.Collections.Generic;

namespace NanoReality.GameLogic.TrafficSystem
{
    public class RoadsGraph : GraphBase<RoadGraphNode, RoadGraphEdge>
	{
        #region implemented abstract members of GraphBase

        public override string GraphTargetElementName { get { return "Road"; } }

        protected override RoadGraphNode CreateVertex(RoadMapObjectView roadView, 
            Vector3 offset, Directions direction)
        {
            return new RoadGraphNode(roadView, offset, direction);
        }

        protected override RoadGraphEdge CreateEdge(RoadGraphNode start, RoadGraphNode end)
        {
            return new RoadGraphEdge(start, end);
        }

        protected override RoadTileNodeConfig GetTileConfig(TrafficConfig config, RoadTileType roadType)
        {
            return config.GetRoadTileNodeConfig(roadType);
        }

        protected override void AddInnerEdges(RoadMapObjectView roadView)
        {
            var tile = VerticesByPosition[roadView.MapObjectModel.MapPosition];
            switch (roadView.RoadTileType)
            {
                case RoadTileType.CityEnter:
                case RoadTileType.Intersection:
                case RoadTileType.Tside_LB_LT_RB:
                case RoadTileType.Tside_LT_LB_RT:
                case RoadTileType.Tside_RB_LB_RT:
                case RoadTileType.Tside_RT_LT_RB:
                    AddDirectionalEdge(tile[Directions.LeftTop], tile[Directions.LeftBottom]);
                    AddDirectionalEdge(tile[Directions.LeftBottom], tile[Directions.RightBottom]);
                    AddDirectionalEdge(tile[Directions.RightBottom], tile[Directions.RightTop]);
                    AddDirectionalEdge(tile[Directions.RightTop], tile[Directions.LeftTop]);
                    break;

                case RoadTileType.Straight_LB_RT:
                    AddDirectionalEdge(tile[Directions.LeftBottom], tile[Directions.RightBottom]);
                    AddDirectionalEdge(tile[Directions.RightTop], tile[Directions.LeftTop]);
                    break;

                case RoadTileType.Straight_LT_RB:
                    AddDirectionalEdge(tile[Directions.LeftTop], tile[Directions.LeftBottom]);
                    AddDirectionalEdge(tile[Directions.RightBottom], tile[Directions.RightTop]);
                    break;

                case RoadTileType.Deadlock_LB:
                    AddDirectionalEdge(tile[Directions.LeftBottom], tile[Directions.RightBottom]);
                    AddDirectionalEdge(tile[Directions.RightBottom], tile[Directions.RightTop]);
                    AddDirectionalEdge(tile[Directions.RightTop], tile[Directions.LeftTop]);
                    break;

                case RoadTileType.Deadlock_RT:
                    AddDirectionalEdge(tile[Directions.RightTop], tile[Directions.LeftTop]);
                    AddDirectionalEdge(tile[Directions.LeftTop], tile[Directions.LeftBottom]);
                    AddDirectionalEdge(tile[Directions.LeftBottom], tile[Directions.RightBottom]);
                    break;

                case RoadTileType.Deadlock_LT:
                    AddDirectionalEdge(tile[Directions.LeftTop], tile[Directions.LeftBottom]);
                    AddDirectionalEdge(tile[Directions.LeftBottom], tile[Directions.RightBottom]);
                    AddDirectionalEdge(tile[Directions.RightBottom], tile[Directions.RightTop]);
                    break;

                case RoadTileType.Deadlock_RB:
                    AddDirectionalEdge(tile[Directions.RightBottom], tile[Directions.RightTop]);
                    AddDirectionalEdge(tile[Directions.RightTop], tile[Directions.LeftTop]);
                    AddDirectionalEdge(tile[Directions.LeftTop], tile[Directions.LeftBottom]);
                    break;

                case RoadTileType.Turn_LB_LT:
                    AddDirectionalEdge(tile[Directions.LeftBottom], tile[Directions.RightBottom]);
                    AddDirectionalEdge(tile[Directions.RightBottom], tile[Directions.RightTop]);
                    break;

                case RoadTileType.Turn_LB_RB:
                    AddDirectionalEdge(tile[Directions.RightBottom], tile[Directions.RightTop]);
                    AddDirectionalEdge(tile[Directions.RightTop], tile[Directions.LeftTop]);
                    break;

                case RoadTileType.Turn_LT_RT:
                    AddDirectionalEdge(tile[Directions.LeftTop], tile[Directions.LeftBottom]);
                    AddDirectionalEdge(tile[Directions.LeftBottom], tile[Directions.RightBottom]);
                    break;

                case RoadTileType.Turn_RB_RT:
                    AddDirectionalEdge(tile[Directions.RightTop], tile[Directions.LeftTop]);
                    AddDirectionalEdge(tile[Directions.LeftTop], tile[Directions.LeftBottom]);
                    break;
            }
        }

        protected override void AddAdjacentEdges(RoadMapObjectView roadView, Directions direction)
        {
            RoadMapObjectView adjacentRoadView;

            if (!roadView.Adjacent.TryGetValue(direction, out adjacentRoadView))
            {
                return;
            }

            var tile = VerticesByPosition[roadView.MapObjectModel.MapPosition];
            var adjacent = VerticesByPosition[adjacentRoadView.MapObjectModel.MapPosition];

            switch (direction)
            {
                case Directions.LeftTop:
                    AddDirectionalEdge(tile[Directions.RightTop], adjacent[Directions.RightBottom]);
                    break;
                case Directions.LeftBottom:
                    AddDirectionalEdge(tile[Directions.LeftTop], adjacent[Directions.RightTop]);
                    break;
                case Directions.RightTop:
                    AddDirectionalEdge(tile[Directions.RightBottom], adjacent[Directions.LeftBottom]);
                    break;
                case Directions.RightBottom:
                    AddDirectionalEdge(tile[Directions.LeftBottom], adjacent[Directions.LeftTop]);
                    break;
            }
        }

        #endregion
    }
}