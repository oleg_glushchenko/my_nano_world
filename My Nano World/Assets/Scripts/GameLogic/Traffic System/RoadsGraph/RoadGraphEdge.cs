﻿using UnityEngine;
using NanoReality.GameLogic.TrafficSystem.Graph;
using NanoReality.GameLogic.TrafficSystem.Road;

namespace NanoReality.GameLogic.TrafficSystem
{
    public class RoadGraphEdge  : EdgeBase<RoadGraphNode>
	{
		public RoadGraphEdge(RoadGraphNode start, RoadGraphNode end)
            : base(start, end)
        {
            
        }

	    public bool IsCrossroad
	    {
	        get { return Start.RoadView == End.RoadView && Start.RoadView.RoadTileType.IsCrossroads(); }
	    }
    }
}

