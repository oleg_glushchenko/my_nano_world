﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NanoReality.GameLogic.TrafficSystem.Domain;

namespace NanoReality.GameLogic.TrafficSystem.Citizens
{
    [RequireComponent(typeof(MeshFilter))]
    public class CitizenModelView : MonoBehaviour
    {
        #region new class AnimationData
        [Serializable]
        public class AnimationData
        {
            public List<Mesh> Meshes;
            public Mesh IdleMesh;
            [Tooltip("Time in seconds to go through all meshes")]
            public float Duration = 1f;
            [Tooltip("Distance covered during one cycle of animation")]
            public float Distance = 0.2f;
        }
        #endregion

        [SerializeField]
        private AnimationData _leftBottom;
        [SerializeField]
        private AnimationData _leftTop;
        [SerializeField]
        private AnimationData _rightBottom;
        [SerializeField]
        private AnimationData _rightTop;

        private MeshFilter _meshFilter;
        private Coroutine _coroutine;
        private DirectionVectorType _direction;
        private Dictionary<DirectionVectorType, AnimationData> _mapDirectionToAnimData;

        public bool IsWalking { get { return _coroutine != null; } }

        private void Awake()
        {
            _meshFilter = GetComponent<MeshFilter>();
            _direction = DirectionVectorType.Zero;

            _mapDirectionToAnimData = new Dictionary<DirectionVectorType, AnimationData>();
            _mapDirectionToAnimData.Add(DirectionVectorType.Right, _rightTop);
            _mapDirectionToAnimData.Add(DirectionVectorType.Up, _leftTop);
            _mapDirectionToAnimData.Add(DirectionVectorType.Left, _leftBottom);
            _mapDirectionToAnimData.Add(DirectionVectorType.Down, _rightBottom);
        }

        public void StopWalking()
        {
            if (_coroutine != null)
            {
                StopCoroutine(_coroutine);
            }
            _meshFilter.mesh = GetAnimationData().IdleMesh;
          
            _coroutine = null;
            _direction = DirectionVectorType.Zero;
        }
      
        public void PlayWalking(DirectionVectorType direction, float speed)
        {
            if (_direction == direction)
            {
                return;
            }

            StopWalking();

            _direction = direction;

            _coroutine = StartCoroutine(PlayAnimation(GetAnimationData(), speed));
        }

        private IEnumerator PlayAnimation(AnimationData anim, float speed)
        {
            int count = anim.Meshes.Count;

            if (count <= 0)
            {
                yield break;
            }

            int index = UnityEngine.Random.Range(0, count);
            float time = anim.Duration / count / (speed / anim.Distance);
            var waitForNextFrame = new WaitForSeconds(time);

            while (true)
            {
                _meshFilter.mesh = anim.Meshes[index];

                yield return waitForNextFrame;

                index++;

                if (index >= count)
                {
                    index = 0;
                }
            }
        }

        private AnimationData GetAnimationData()
        {
            if (_mapDirectionToAnimData.ContainsKey(_direction))
            {
                return _mapDirectionToAnimData[_direction];
            }
            return _rightTop;
        }
    }
}