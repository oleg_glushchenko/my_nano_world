﻿using NanoReality.Engine.BuildingSystem.MapObjects;
using UnityEngine;
using NanoReality.GameLogic.TrafficSystem.Road;

namespace NanoReality.GameLogic.TrafficSystem.Citizens
{
    public class SidewalkGraph : GraphBase<SidewalkVertex, SidewalkEdge>
    {
        #region implemented abstract members of GraphBase

        public override string GraphTargetElementName { get { return "Sidewalk"; } }

        protected override SidewalkVertex CreateVertex(RoadMapObjectView roadView, 
            Vector3 offset, Directions direction)
        {
            return new SidewalkVertex(roadView, offset, direction);
        }

        protected override SidewalkEdge CreateEdge(SidewalkVertex start, SidewalkVertex end)
        {
            return new SidewalkEdge(start, end);
        }

        protected override RoadTileNodeConfig GetTileConfig(TrafficConfig config, RoadTileType roadType)
        {
            return config.GetSidewalkTileConfig(roadType);
        }

        protected override void AddInnerEdges(RoadMapObjectView roadView)
        {
            var tile = VerticesByPosition[roadView.MapObjectModel.MapPosition];
            switch (roadView.RoadTileType)
            {
                case RoadTileType.CityEnter:
                case RoadTileType.Intersection:
                case RoadTileType.Tside_LB_LT_RB:
                case RoadTileType.Tside_LT_LB_RT:
                case RoadTileType.Tside_RB_LB_RT:
                case RoadTileType.Tside_RT_LT_RB:
                    AddBidirectionalEdge(tile[Directions.LeftTop], tile[Directions.LeftBottom]);
                    AddBidirectionalEdge(tile[Directions.LeftBottom], tile[Directions.RightBottom]);
                    AddBidirectionalEdge(tile[Directions.RightBottom], tile[Directions.RightTop]);
                    AddBidirectionalEdge(tile[Directions.RightTop], tile[Directions.LeftTop]);
                    break;

                case RoadTileType.Straight_LB_RT:
                    AddBidirectionalEdge(tile[Directions.LeftTop], tile[Directions.RightTop]);
                    AddBidirectionalEdge(tile[Directions.LeftBottom], tile[Directions.RightBottom]);
                    break;

                case RoadTileType.Straight_LT_RB:
                    AddBidirectionalEdge(tile[Directions.LeftTop], tile[Directions.LeftBottom]);
                    AddBidirectionalEdge(tile[Directions.RightTop], tile[Directions.RightBottom]);
                    break;

                case RoadTileType.Deadlock_LB:
                    AddBidirectionalEdge(tile[Directions.LeftBottom], tile[Directions.RightBottom]);
                    AddBidirectionalEdge(tile[Directions.RightBottom], tile[Directions.RightTop]);
                    AddBidirectionalEdge(tile[Directions.RightTop], tile[Directions.LeftTop]);
                    break;

                case RoadTileType.Deadlock_RT:
                    AddBidirectionalEdge(tile[Directions.RightTop], tile[Directions.LeftTop]);
                    AddBidirectionalEdge(tile[Directions.LeftTop], tile[Directions.LeftBottom]);
                    AddBidirectionalEdge(tile[Directions.LeftBottom], tile[Directions.RightBottom]);
                    break;

                case RoadTileType.Deadlock_LT:
                    AddBidirectionalEdge(tile[Directions.LeftTop], tile[Directions.LeftBottom]);
                    AddBidirectionalEdge(tile[Directions.LeftBottom], tile[Directions.RightBottom]);
                    AddBidirectionalEdge(tile[Directions.RightBottom], tile[Directions.RightTop]);
                    break;

                case RoadTileType.Deadlock_RB:
                    AddBidirectionalEdge(tile[Directions.RightBottom], tile[Directions.RightTop]);
                    AddBidirectionalEdge(tile[Directions.RightTop], tile[Directions.LeftTop]);
                    AddBidirectionalEdge(tile[Directions.LeftTop], tile[Directions.LeftBottom]);
                    break;

                case RoadTileType.Turn_LB_LT:
                    AddBidirectionalEdge(tile[Directions.LeftBottom], tile[Directions.RightBottom]);
                    AddBidirectionalEdge(tile[Directions.RightBottom], tile[Directions.RightTop]);
                    break;

                case RoadTileType.Turn_LB_RB:
                    AddBidirectionalEdge(tile[Directions.RightBottom], tile[Directions.RightTop]);
                    AddBidirectionalEdge(tile[Directions.RightTop], tile[Directions.LeftTop]);
                    break;

                case RoadTileType.Turn_LT_RT:
                    AddBidirectionalEdge(tile[Directions.LeftTop], tile[Directions.LeftBottom]);
                    AddBidirectionalEdge(tile[Directions.LeftBottom], tile[Directions.RightBottom]);
                    break;

                case RoadTileType.Turn_RB_RT:
                    AddBidirectionalEdge(tile[Directions.RightTop], tile[Directions.LeftTop]);
                    AddBidirectionalEdge(tile[Directions.LeftTop], tile[Directions.LeftBottom]);
                    break;
            }
        }

        protected override void AddAdjacentEdges(RoadMapObjectView roadView, Directions direction)
        {
            RoadMapObjectView adjacentRoadView;

            if (!roadView.Adjacent.TryGetValue(direction, out adjacentRoadView))
            {
                return;
            }

            var tile = VerticesByPosition[roadView.MapObjectModel.MapPosition];
            var adjacent = VerticesByPosition[adjacentRoadView.MapObjectModel.MapPosition];

            switch (direction)
            {
                case Directions.LeftTop:
                    AddDirectionalEdge(tile[Directions.LeftTop], adjacent[Directions.LeftBottom]);
                    AddDirectionalEdge(tile[Directions.RightTop], adjacent[Directions.RightBottom]);
                    break;
                case Directions.LeftBottom:
                    AddDirectionalEdge(tile[Directions.LeftTop], adjacent[Directions.RightTop]);
                    AddDirectionalEdge(tile[Directions.LeftBottom], adjacent[Directions.RightBottom]);
                    break;
                case Directions.RightTop:
                    AddDirectionalEdge(tile[Directions.RightTop], adjacent[Directions.LeftTop]);
                    AddDirectionalEdge(tile[Directions.RightBottom], adjacent[Directions.LeftBottom]);
                    break;
                case Directions.RightBottom:
                    AddDirectionalEdge(tile[Directions.LeftBottom], adjacent[Directions.LeftTop]);
                    AddDirectionalEdge(tile[Directions.RightBottom], adjacent[Directions.RightTop]);
                    break;
            }
        }

        #endregion

        private void AddBidirectionalEdge(SidewalkVertex a, SidewalkVertex b)
        {
            AddDirectionalEdge(a, b);
            AddDirectionalEdge(b, a);
        }
    }
}