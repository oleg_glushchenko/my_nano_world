﻿using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.TrafficSystem.Graph;
using UnityEngine;

namespace NanoReality.GameLogic.TrafficSystem.Citizens
{
    public class SidewalkVertex : VertexBase
    {
        public SidewalkVertex(RoadMapObjectView roadView, Vector3 offset, Directions direction)
            : base(roadView, offset, direction)
        {
        }
    }
}