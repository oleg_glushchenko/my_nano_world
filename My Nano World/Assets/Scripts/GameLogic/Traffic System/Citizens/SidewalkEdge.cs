﻿using NanoReality.GameLogic.TrafficSystem.Graph;
using NanoReality.GameLogic.TrafficSystem.Road;
using UnityEngine;

namespace NanoReality.GameLogic.TrafficSystem.Citizens
{
    public class SidewalkEdge : EdgeBase<SidewalkVertex>
    {
        public SidewalkEdge(SidewalkVertex start, SidewalkVertex end)
            : base(start, end)
        {
            IsCrossing = start.RoadView == end.RoadView && start.RoadView.RoadTileType.IsCrossroads() &&
                         start.RoadView.Adjacent.ContainsKey(start.Direction.ToAdjacentRoadToEdge(end.Direction));
        }

        public bool IsCrossing { get; private set; }

        public bool IsBlocked
        {
            get
            {
                return IsCrossing && (Start.IsBlocked || End.IsBlocked);
            }
        }
    }
}