﻿using strange.extensions.signal.impl;
using NanoReality.GameLogic.TrafficSystem;
using Assets.NanoLib.Utilities;
using System;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.Engine.BuildingSystem.MapObjects;
using System.Collections.Generic;
using UnityEngine;

namespace NanoReality.GameLogic.TrafficSystem
{  
    public class MoveVehicleSignal : Signal<LocomotiveMoveParametrs> { }
    public class MoveCitizenSignal : Signal<CitizenMoveParametrs> { }
   
    public class SignalOnMovedVehicle : Signal<BusinessSectorsTypes, int, MovedByPathEventsType> {}
    public class SignalOnMovedCitizen : Signal<BusinessSectorsTypes, int, MovedByPathEventsType> {}

    #region new types declarations

    public enum MovedByPathEventsType
    {
        Started,
        InNextPoint,
        Comleted
    }

    [Serializable]
    public struct PathPoint
    {
        public readonly Vector2Int Position;
        public readonly Directions Direction;
        public readonly float Delay;

        public PathPoint(Vector2Int position, Directions direction, float delay)
        {
            this.Position = position;
            this.Direction = direction;
            this.Delay = delay;
        }
    }

    public abstract class BaseMoveParametrs
    {
        public readonly BusinessSectorsTypes SectorType;
        public readonly List<PathPoint> Path;
        public readonly bool CreateNewInFirstPathPoint = false;
        /// <summary>
        /// bool - is succes applyng moving
        /// int - id of target object
        /// </summary>
        public readonly Action<bool, int> Callback;

       public BaseMoveParametrs(BusinessSectorsTypes sectorType, List<PathPoint> path,
            bool createNewInFirstPathPoint, Action<bool, int> callback)
        {
            this.SectorType = sectorType;
            this.Path = path;
            this.CreateNewInFirstPathPoint = createNewInFirstPathPoint;
            this.Callback = callback;
        }
    }

    public class LocomotiveMoveParametrs : BaseMoveParametrs
    {
        public LocomotorType Type;

        public LocomotiveMoveParametrs(BusinessSectorsTypes sectorType,
            List<PathPoint> path, bool createNewInFirstPathPoint,
            Action<bool, int> callback, LocomotorType type) 
            : base(sectorType, path, createNewInFirstPathPoint, callback)
        {
            this.Type = type;
        }
    }

    public class CitizenMoveParametrs : BaseMoveParametrs
    {
        public CitizenMoveParametrs(BusinessSectorsTypes sectorType,
            List<PathPoint> path, bool createNewInFirstPathPoint, 
            Action<bool, int> callback)
            : base(sectorType, path, createNewInFirstPathPoint, callback)
        {
        }
        
    }
    #endregion
}