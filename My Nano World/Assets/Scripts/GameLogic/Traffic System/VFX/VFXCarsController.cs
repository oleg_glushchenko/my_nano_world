﻿using System;
using UnityEngine;
using System.Collections;
using strange.extensions.mediation.impl;
using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using Assets.NanoLib.Utilities.Pulls;
using strange.extensions.injector.api;
using NanoReality.GameLogic.GameManager.Controllers;
using Random = UnityEngine.Random;

namespace NanoReality.GameLogic.TrafficSystem.VFX
{
	/// <summary>
	/// Контроллер машинок проезжающих по автостраде, мимо города
	/// </summary>
	public class VFXCarsController : View
	{
		/// <summary>
		/// Префабы машин
		/// </summary>
		public List<VFXCar> CarsPrefabs = new List<VFXCar>();

		/// <summary>
		/// Максимальное кол-во времени между спавном новых машинок
		/// </summary>
		public float MaxSecondsBetweenNewCars = 60;

		/// <summary>
		/// Максимальное кол-во машин на дороге
		/// </summary>
		public int MaxCarsOnRoad = 25;

		#region Inject

		[Inject]
		public IInjectionBinder Binder { get; set; }

	    [Inject]
        public ITrafficManager jTrafficManager { get; set; }

		[Inject]
		public SignalOnFinishLoadCityMapSequence jSignalOnFinishLoadCityMapSequence {get; set;}

		#endregion

		/// <summary>
		/// Текущие машины на дороге
		/// </summary>
		private List<VFXCar> _currentLocomotors = new List<VFXCar>();


		/// <summary>
		/// Пулл машин
		/// </summary>
		private List<IObjectsPull> _locomotorPulls = new List<IObjectsPull>();

		/// <summary>
		/// Включен ли таймер спавна машин
		/// </summary>
		private bool _isEnabledNewCarTimer = false;

		/// <summary>
		/// Цикл спавна машин
		/// </summary>
		private Coroutine jSpawnRutine;

        /// <summary>
        /// Cached action to avoid extra allocations
        /// </summary>
	    private Action<VFXCar> _onCarAnimationEndAction;

        protected override void Start ()
		{
			base.Start ();

		    _onCarAnimationEndAction = OnCarAnimationEnd;
            
            foreach (var prefab in CarsPrefabs) 
			{
				var pull = Binder.GetInstance<IObjectsPull> ();
				pull.InstanitePull (prefab);
				_locomotorPulls.Add (pull);
			}
			jSignalOnFinishLoadCityMapSequence.AddListener (OnFinishLoadCity);
		}

		private void OnFinishLoadCity(LoadCityMapCommandData data)
		{
			foreach (var car in _currentLocomotors) {
				car.FreeObject ();
			}
			_currentLocomotors.Clear ();
			StartSpawnCycle ();
		}



		private void StartSpawnCycle()
		{
			if (!_isEnabledNewCarTimer) 
			{
				_isEnabledNewCarTimer = true;
				if (jSpawnRutine != null)
					StopCoroutine (jSpawnRutine);
				jSpawnRutine = StartCoroutine (SpawnNewCarTimer ());
			}
		}

		private IEnumerator SpawnNewCarTimer()
		{
			while (_isEnabledNewCarTimer) 
			{
				yield return new WaitForSeconds (Random.Range (2, MaxSecondsBetweenNewCars));
				SpawnNewCar ();
			}
		}


		private void SpawnNewCar()
		{
			if (_currentLocomotors.Count <= MaxCarsOnRoad) 
			{
				var vfxCar = (VFXCar)_locomotorPulls [Random.Range (0, _locomotorPulls.Count)].GetInstance ();
				_currentLocomotors.Add (vfxCar);
				vfxCar.SignalOnAnimationEnd.AddListener (_onCarAnimationEndAction);
				vfxCar.transform.SetParent (transform);
			    vfxCar.transform.localScale = Vector3.one * jTrafficManager.TrafficConfig.LocomotorScaleFactor;
                vfxCar.PlayRandomAnimation();
			}

		}

		private void OnCarAnimationEnd(VFXCar sender)
		{
			_currentLocomotors.Remove (sender);
			sender.FreeObject ();
		}

	}
}

