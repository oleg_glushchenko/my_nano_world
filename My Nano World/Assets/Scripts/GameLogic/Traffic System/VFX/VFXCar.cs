﻿using UnityEngine;
using Assets.NanoLib.Utilities.Pulls;
using strange.extensions.signal.impl;
using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using Random = UnityEngine.Random;

namespace NanoReality.GameLogic.TrafficSystem.VFX
{
	/// <summary>
	/// Машинока проезжающая по автостраде, мимо города
	/// </summary>
	public class VFXCar : GameObjectPullable
	{
		/// <summary>
		/// Список анимаций для каждого сектора
		/// </summary>
		public List<AnimationClip> Animations = new List<AnimationClip>();

		public CacheMonobehavior CacheMonobehavior {get {return _cacheMonobehavior ?? (_cacheMonobehavior = new CacheMonobehavior (this));}}
		private CacheMonobehavior _cacheMonobehavior;

		/// <summary>
		/// Сигнал вызывается по окончаню анимации движения
		/// </summary>
		public Signal<VFXCar> SignalOnAnimationEnd = new Signal<VFXCar> ();

		public void PlayRandomAnimation()
		{
		    AnimationClip clip = null;

            if (Animations.Count > 0)
            {
				clip = Animations[Random.Range (0, Animations.Count)];
			}

		    if (clip != null)
		    {
		        CacheMonobehavior.GetCacheComponent<Animation>().Play(clip.name, PlayMode.StopAll);
		    }
		    else
		    {
		        OnAnimationEnd();
            }
		}

		public override void OnPush ()
		{
			CacheMonobehavior.GetCacheComponent<Animation> ().Stop ();
			SignalOnAnimationEnd.RemoveAllListeners ();
			SignalOnAnimationEnd.RemoveAllOnceListeners ();
			base.OnPush ();
		}


		public void OnAnimationEnd()
		{
			SignalOnAnimationEnd.Dispatch (this);
		}
	}
}
