﻿using System;

namespace NanoReality.GameLogic.TrafficSystem.Graph
{
    public interface INeedRefreshPathFinder
    {
        event Action GraphChanged;
    }
}