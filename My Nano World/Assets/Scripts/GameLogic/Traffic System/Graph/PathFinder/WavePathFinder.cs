﻿using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using System;
using System.Text;
using NanoLib.Utilities;
using UnityEngine;

namespace NanoReality.GameLogic.TrafficSystem.Graph
{
    public class WavePathFinder<TVertex, TEdge> : IPathFinder<TVertex, TEdge> , IDisposable
        where TVertex : VertexBase where TEdge : IEdge<TVertex>
    {
        private const int CanMove = 0;
        private const int CannotMove = 1;
        private const int Start = 2;
        private const int Finish = 3;

        private const int Dimension_X = 0;
        private const int Dimension_Y = 1;

        private const int StartRationValue = 1;
        private const int NoRationValue = 0;

        private readonly List<Vector2F> _allDirections;
        private readonly Dictionary<Vector2F, List<TVertex>> _mapVerteces;
        private readonly INeedRefreshPathFinder _refreshController;

        private int[,] _map;
        private int[,] _rations;

        private Vector2F _target;
        private Vector2F _size;
        private bool _useLastCachGraph;
        private TVertex _startPoint;
        private TVertex _previusStartPoint;
        private TVertex _previusEndPoint;

        private IGraph<TVertex, TEdge> _graph;

        public WavePathFinder(INeedRefreshPathFinder refreshController)
        {
            _allDirections = new List<Vector2F>(new[]
                { Vector2F.down, Vector2F.up,
                    Vector2F.right, Vector2F.left
                });
            _mapVerteces = new Dictionary<Vector2F, List<TVertex>>();
            _refreshController = refreshController;
            _refreshController.GraphChanged += OnNeedRefreshGrap;
        }

        public WavePathFinder(INeedRefreshPathFinder refreshController, IGraph<TVertex, TEdge> graph)
            : this(refreshController)
        {
            CreateAndCachMatrix(graph);
        }
      
        #region IPathFinder implementation

        IList<TVertex> IPathFinder<TVertex, TEdge>.FindPath(TVertex start, TVertex end, IGraph<TVertex, TEdge> graph)
        {
            IList<TVertex> result = new List<TVertex>();
            _startPoint = start;
            if (!_useLastCachGraph)
            {
                CreateAndCachMatrix(graph);
            }
            SetStartAndEndPoints(start, end);

            Editor_Testing_ProfilerSamle("find path");
            var targetPath = FindResult();
            Editor_Testing_EndProfilerSample();
            result.Add(start);

            Editor_Testing_ProfilerSamle("find points in detected path");
            foreach (var item in targetPath)
            {
                var nexPathPart = GetPathBetweenNeigboards(result[result.Count - 1], item);
                if (nexPathPart == null)
                {
                    #if UNITY_EDITOR
                    var log = new StringBuilder();

                    log.Append("Problems with algoritm step " + targetPath.IndexOf(item)
                        + ". value: " + targetPath);
                    log.Append("\n Full path: ");
                    foreach (var itemForLog in targetPath)
                    {
                        log.Append(" -> " + itemForLog);
                    }
                    Debug.LogError(log.ToString());
                    #endif
                    return result;
                }
                result.AddRange(nexPathPart);
            }
            Editor_Testing_EndProfilerSample();

            return result;
        }

        #endregion

        #region IDisposable implementation

        void IDisposable.Dispose()
        {
            _refreshController.GraphChanged -= OnNeedRefreshGrap;
        }

        #endregion

        public void CreateAndCachMatrix(IGraph<TVertex, TEdge> graph)
        {
            _graph = graph;
            Editor_Testing_ProfilerSamle("detect size");
            DetectMatrixSize();
            Editor_Testing_EndProfilerSample();

            Editor_Testing_ProfilerSamle("find verticles");
            FindAllVerticles();
            Editor_Testing_EndProfilerSample();

            Editor_Testing_ProfilerSamle("create matrix");
            CreateEmptyMatrix();
            Editor_Testing_EndProfilerSample();

            Editor_Testing_ProfilerSamle("set cannot move cells");
            SetCannotMoveCells();
            Editor_Testing_EndProfilerSample();

            _useLastCachGraph = true;
        }

        private void DetectMatrixSize()
        {
            _size = new Vector2F();
            foreach (var item in _graph.Vertices)
            {
                if (item.MatrixPosition.intX > _size.intX)
                {
                    _size.intX = item.MatrixPosition.intX;
                }
                if (item.MatrixPosition.intY > _size.intY)
                {
                    _size.intY = item.MatrixPosition.intY;
                }
            }
            _size += Vector2F.one;
        }

        private void FindAllVerticles()
        {
            _mapVerteces.Clear();
            foreach (var item in _graph.Vertices)
            {
                if (!_mapVerteces.ContainsKey(item.MatrixPosition))
                {
                    _mapVerteces.Add(item.MatrixPosition, new List<TVertex>());
                }
                _mapVerteces[item.MatrixPosition].Add(item);
            }
        }

        private void SetStartAndEndPoints(TVertex start, TVertex finish)
        {
            MakeFreePreviusStartAndEndPoints();
            _map[start.MatrixPosition.intX, start.MatrixPosition.intY] = Start;
            _map[finish.MatrixPosition.intX, finish.MatrixPosition.intY] = Finish;
            _previusStartPoint = start;
            _previusEndPoint = finish;
        }

        private void MakeFreePreviusStartAndEndPoints()
        {
            if (_previusStartPoint != null)
            {
                var start = _previusStartPoint.MatrixPosition;
                _map[start.intX, start.intY] = CanMove;
            }
            if (_previusEndPoint != null)
            {
                var end = _previusEndPoint.MatrixPosition;
                _map[end.intX, end.intY] = CanMove;
            }
        }

        private void OnNeedRefreshGrap()
        {
            _useLastCachGraph = false;
        }

        private List<TVertex> GetPathBetweenNeigboards(TVertex start, Vector2F end)
        {
            List<TVertex> result = null;
            var targetVertices = _mapVerteces[start.MatrixPosition];
            var edges = new List<TEdge>();
            var endEdges = new List<TEdge>();
            var targetVerticesCount = targetVertices.Count;

            for (int vertexIndex = 0; vertexIndex < targetVerticesCount; vertexIndex++)
            {
                var newEdges = _graph.GetEdges(targetVertices[vertexIndex]);
                var newEdgesCount = newEdges.Count;
                for (int edgeIndex = 0; edgeIndex < newEdgesCount; edgeIndex++)
                {
                    var edgeItem = newEdges[edgeIndex];
                    if (edgeItem.End.MatrixPosition == end)
                    {
                        endEdges.Add(edgeItem);
                    }
                }
                edges.AddRange(newEdges);
            }

            var endEdgesCount = endEdges.Count;
            for(int i = 0; i < endEdgesCount; i++)
            {
                Editor_Testing_ProfilerSamle("GetEdgesBetweenOneElementPoints");
                var itemResult = GetEdgesBetweenOneElementPoints(start, endEdges[i], edges);
                Editor_Testing_EndProfilerSample();
                if (itemResult != null && (result == null || itemResult.Count < result.Count))
                {
                    result = itemResult;
                }
            }
            return result;
        }

        private List<TVertex> GetEdgesBetweenOneElementPoints(TVertex start, TEdge end,
            List<TEdge> allEdges, int depth = 0)
        {
            const int MaxEdgesInRoad = 4;
            if (depth > MaxEdgesInRoad)
                return null;
            var result = new List<TVertex>();
            if (depth == 0)
            {
                result.Add(end.End);
            }
            depth++;
            if (end.Start == start)
            {
                return result;
            }
            else
            {
                result.Add(end.Start);
            }
            var allEdgesCount = allEdges.Count;
            var lastResultItem = result[result.Count - 1];
            for (int i = 0; i < allEdgesCount; i++)
            {
                var item = allEdges[i];
                if (item.End == lastResultItem)
                {
                    if (item.Start == start)
                    {
                        result.Reverse();
                        return result;
                    }
                    var newResult = GetEdgesBetweenOneElementPoints(start, item, allEdges, depth);
                    if (newResult != null)
                    {
                        result.Reverse();
                        newResult.AddRange(result);
                        return newResult;
                    }
                }
            }
            return null;
        }

        private void SetCannotMoveCells()
        {
            for (int x = 0; x < _map.GetLength(Dimension_X); x++)
            {
                for (int y = 0; y < _map.GetLength(Dimension_Y); y++)
                {
                    if (!_mapVerteces.ContainsKey(new Vector2F(x, y)))
                    {
                        _map[x, y] = CannotMove;
                    }
                }
            }
        }

        private void CreateEmptyMatrix()
        {
            _map = new int[_size.intX, _size.intY];
        }

        private List<Vector2F> FindResult()
        {
            var result = new List<Vector2F>();
            _rations = new int[_map.GetLength(Dimension_X), _map.GetLength(Dimension_Y)];
            for (int x = 0; x < _map.GetLength(Dimension_X); x++)
            {
                for (int y = 0; y < _map.GetLength(Dimension_Y); y++)
                {
                    _rations[x, y] = 0;
                    if (_map[x, y] == Start)
                    {
                        _rations[x, y] = StartRationValue;
                    }
                    else if (_map[x, y] == Finish)
                    {
                        _target = new Vector2F(x, y);
                    }
                }
            }
            var targetStep = SetRation(StartRationValue);
            if (targetStep > 0)
            {
                var neigboard = _target;

                while (targetStep > 1)
                {
                    result.Add(neigboard);
                    neigboard = GetOneTargetCell(targetStep, neigboard);
                    targetStep--;
                }
                result.Add(neigboard);

                result.Reverse();
            }
            return result;
        }

        private int SetRation(int step)
        {
            var isStillHasFreeCells = false;
            for (int x = 0; x < _rations.GetLength(Dimension_X); x++)
            {
                for (int y = 0; y < _rations.GetLength(Dimension_Y); y++)
                {
                    if (_rations[x, y] == step)
                    {
                        if (TryMakeNeigboardRation(x, y, step))
                        {
                            isStillHasFreeCells = true;
                        }
                    }
                }
            }
            if (_rations[_target.intX, _target.intY] != 0)
            {
                return step;
            }
            else
            {
                if (isStillHasFreeCells)
                {
                    return SetRation(++step);
                }
                else
                {
                    return -1;
                }
            }
        }

        private bool TryMakeNeigboardRation(int x, int y, int step)
        {
            var result = false;
            var targetValue = step + 1;

            foreach (var item in _allDirections)
            {
                var target = item + new Vector2F(x, y);
                if (ValidateForMove(target, true))
                {
                    if (step != StartRationValue ||
                        GetPathBetweenNeigboards(_startPoint, target) != null)
                    {
                        result = true;
                        _rations[target.intX, target.intY] = targetValue;
                    }
                }
            }

            return result;
        }

        private void LogMatrix(string header, int[,] matrix)
        {
            var log = new StringBuilder();
            log.AppendLine(header);
            for (int y = 0; y < matrix.GetLength(Dimension_Y); y++)
            {
                var line = string.Empty;
                var isNeedAddLine = false;
                for (int x = 0; x < matrix.GetLength(Dimension_X); x++)
                {
                    if (matrix[x, y] != 0)
                    {
                        line += matrix[x, y];
                        isNeedAddLine = true;
                    }
                    else
                    {
                        line += " ";
                    }
                }
                if (isNeedAddLine)
                {
                    log.AppendLine(line);
                }
            }
            Debug.LogWarning(log.ToString());
        }

        private bool ValidateForMove(Vector2F direciton, bool isUseRatioCheck)
        {
            int x = direciton.intX;
            int y = direciton.intY;
            return x >= 0 && x < _map.GetLength(Dimension_X)
                && y >= 0 && y < _map.GetLength(Dimension_Y)
                && (_map[x, y] == CanMove || _map[x, y] == Finish)
                && (!isUseRatioCheck || _rations[x, y] == NoRationValue);
        }

        private Vector2F GetOneTargetCell(int ration, Vector2F neigboard)
        {
            foreach (var item in _allDirections)
            {
                var targetDirection = item + neigboard;
                var isValidate = ValidateForMove(targetDirection, false);
                if (isValidate &&
                    _rations[targetDirection.intX, targetDirection.intY] == ration)
                {
                    return targetDirection;
                }
            }
            throw new System.NotImplementedException();
        }
        #region using Unity profiler
        private bool _isNeedUseProfiling = true;

        private void Editor_Testing_ProfilerSamle(string mesasge )
        {
            if (_isNeedUseProfiling)
            {
                UnityEngine.Profiling.Profiler.BeginSample("WawePathFinder: " + mesasge);
            }
        }

        private void Editor_Testing_EndProfilerSample()
        {
            if (_isNeedUseProfiling)
            {
                UnityEngine.Profiling.Profiler.EndSample();
            }
        }
        #endregion
    }
}