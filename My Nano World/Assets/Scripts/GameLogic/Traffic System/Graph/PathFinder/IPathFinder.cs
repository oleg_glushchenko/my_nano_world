﻿using System.Collections.Generic;

namespace NanoReality.GameLogic.TrafficSystem.Graph
{
    public interface IPathFinder<TVertex, TEdge> where TVertex : IVertex where TEdge : IEdge<TVertex>
    {
        IList<TVertex> FindPath(TVertex start, TVertex end, IGraph<TVertex, TEdge> graph);
    }
}
