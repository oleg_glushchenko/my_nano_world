﻿using Assets.NanoLib.Utilities;
using Assets.NanoLib.Utilities.Pulls;
using UnityEngine;
using System;
using NanoReality.GameLogic.TrafficSystem.Graph;
using System.Collections.Generic;
using NanoReality.GameLogic.Utilities;
using Random = UnityEngine.Random;
using strange.extensions.signal.impl;
using Assets.NanoLib.UtilitesEngine;
using NanoReality.GameLogic.TrafficSystem.Domain;

namespace NanoReality.GameLogic.TrafficSystem
{
    public abstract class GraphMovePersonBase<TVertex, TEdge, TGraph>
        : MonoBehaviour, IPullableObject, IGraphMove
        where TVertex : VertexBase
        where TEdge : EdgeBase<TVertex>
        where TGraph : GraphBase<TVertex, TEdge>
    {
        #region IPullableObject implementation

        void IPullableObject.OnPop()
        {
            _destinationStartedCount = 0;
            PauseTime = 0;
            _pathData = new List<KeyValuePair<TVertex, float>>();
            IsAnimationPlaying = false;
            IsHasDestination = false;
            CacheMonobehavior.CachedGameObject.SetActive(true);
        }

        void IPullableObject.OnPush()
        {
            SetCurrentVertex(null);
            
            CacheMonobehavior.CachedGameObject.SetActive(false);
            SignalForPathMoving.RemoveAllListeners();
            OnDeactivating();
        }

        void IPullableObject.FreeObject()
        {
            if (pullSource != null)
            {
                pullSource.PushInstance(this);
            }
            else
            {
                (this as IPullableObject).DestroyObject();
            }
        }

        void IPullableObject.DestroyObject()
        {
            Destroy(gameObject);
        }

        public IObjectsPull pullSource { get; set; }

        #endregion

        #region ICloneable implementation

        object ICloneable.Clone()
        {
            return Instantiate (this);
        }

        #endregion

        #region move by path signals

        public Signal<int, MovedByPathEventsType> SignalForPathMoving = new Signal<int, MovedByPathEventsType>();
       
        #endregion

        [SerializeField]
        protected float BaseSpeed = 1f;
        [SerializeField]
        private float SpeedCoeffForSmooth = 0.01f;

        private TVertex _lastCurrentVertex;
        private TVertex _currentVertex;
        private CacheMonobehavior _cacheMonobehavior;
        private List<KeyValuePair<TVertex, float>> _pathData;
        private int _destinationStartedCount;
        private IPathFinder<TVertex, TEdge> _shortestPathFinder;
        private IPathFinder<TVertex, TEdge> _randomPathFinder;
        
        protected TrafficConfig Config;
        protected DirectionVectorType LastDirection;
        protected TVertex DestinationVertex;
        protected TVertex NextVertex;
        protected TEdge CurrentEdge;
        protected IList<TVertex> CurrentPath = new List<TVertex> ();
        protected bool IsAnimationPlaying;

        public Vector3 CurrentPosition { get; private set; }
        public bool IsMovingByPath { get { return _pathData.Count > 0; } }
       
        protected TVertex CurrentVertex 
        {
            get
            {
                return _currentVertex;
            }

            private set
            {
                if (value != null)
                {
                    _lastCurrentVertex = value;
                }
                _currentVertex = value;
            }
        }
        protected float PauseTime { get; private set; }

        protected TGraph Graph { get; private set; }

        protected float CurrentSpeed { get; private set; }
      
        public int Id { get; private set; }

        public CacheMonobehavior CacheMonobehavior
        {
            get
            {
                return _cacheMonobehavior ?? (_cacheMonobehavior = new CacheMonobehavior(this));
            }
        }

        public bool IsHasDestination { get; private set; }

        public virtual void Init(TrafficConfig config, TGraph graph, 
            IPathFinder<TVertex, TEdge> pathFinderForRandomMoving,
            IPathFinder<TVertex, TEdge> pathFinderForMovingByPath,
            int id)
        {
            Config = config;
            Id = id;
            Graph = graph;
            _randomPathFinder = pathFinderForRandomMoving;
            _shortestPathFinder = pathFinderForMovingByPath;
            CurrentSpeed = BaseSpeed;
            CurrentEdge = null;
        }

        public void MoveByPath(List<KeyValuePair<TVertex, float>> pathData, bool fastSetInFirstPoint)
        {
            if (pathData.Count > 1 || (fastSetInFirstPoint == false && pathData.Count > 0))
            {
                IsAnimationPlaying = false;
                _destinationStartedCount = 0;
                _pathData.Clear();
                if (!fastSetInFirstPoint)
                {
                    _pathData.Add(new KeyValuePair<TVertex, float>(_lastCurrentVertex, 0));
                }
                _pathData.AddRange(pathData.ToArray());
                SetCurrentVertex(_pathData[0].Key, fastSetInFirstPoint);
                MoveToNextPathPoint();
            }
            else
            {
                Debug.LogError("Not correct parametrs");
                SetCurrentVertex(GetRandomReachableVertex());
                SetDestination(GetRandomReachableVertex());
            }
        }

        #region abstracts methods

        protected abstract string NameForLog { get; }

        protected abstract bool UseSmoothStop { get; }

        /// <summary>
        /// Calls when object move to pull
        /// </summary>
        protected abstract void OnDeactivating();
        /// <summary>
        /// Change visual side in world after direction chaging
        /// </summary>
        /// <param name="direction">Direction.</param>
        protected abstract void SetSide(DirectionVectorType direction);

        protected abstract void SetNextVertex(TVertex vertex);

        protected abstract void DefaultInTargetLogicUpdate();

        protected abstract bool CanMoveToVertex(TVertex vertex);

        protected abstract void StoppingLogicUpdate();

        protected abstract bool SuitableForStay(TVertex vertex);

        #endregion

        private bool IsNodeReached(TVertex vertex)
        {
            var offsetNode = vertex.WorldPosition;
            var distance = Vector3.Distance(offsetNode, CurrentPosition);
            return  distance <= float.Epsilon;
        }

        protected DirectionVectorType GetDirection(TVertex vertex)
        {
            return GetDirection(CurrentPosition, vertex.WorldPosition);
        }

        protected DirectionVectorType GetDirection(Vector3 start, Vector3 end)
        {
            var direction = end - start;
            
            // avoid diagonal directions, prefer horizontal over vertical
            if (Mathf.Abs(direction.x) >= Mathf.Abs(direction.z))
            {
                return direction.x < 0 ? DirectionVectorType.Left : DirectionVectorType.Right;
            }
            
            return direction.z < 0 ? DirectionVectorType.Down : DirectionVectorType.Up;
        }

        protected virtual void UpdateSprite(TVertex vertex)
        {
            if (vertex == null)
            {
                return;
            }

            var direction = GetDirection(vertex);
            SetSide(direction);
            LastDirection = direction;
        }

        protected void SetCurrentVertex(TVertex vertex, bool updatePosition = true)
        {
            SetNextVertex(null);

            OnOccupiedVertexChanging(CurrentVertex, vertex);

            CurrentVertex = vertex;

            if (CurrentVertex != null && updatePosition)
            {
                SetPosition(CurrentVertex.WorldPosition, CurrentVertex.RoadView.Depth);
            }
        }

        private void UpdateNextVertex()
        {
            if (NextVertex == null)
            {
                var node = GetPathVertex();
                SetNextVertex(node);
            }
        }

        protected TVertex GetPathVertex(int afterCurrentPointIndex = 1)
        {
            if (CurrentPath == null)
            {
                return null;
            }

            if (CurrentVertex == null)
            {
                return CurrentPath[0];
            }

            if (CurrentVertex == DestinationVertex)
            {
                return null;
            }

            var currentNodePathIndex = CurrentPath.IndexOf(CurrentVertex);
            var targetPathIndex = currentNodePathIndex + afterCurrentPointIndex;

            if (currentNodePathIndex < 0 || targetPathIndex >= CurrentPath.Count)
            {
                return null;
            }
            else
            {
                return CurrentPath[targetPathIndex];
            }
        }

        private void SetPosition(Vector3 position, float depth)
        {
            CurrentPosition = position;

            CacheMonobehavior.CachedTransform.position = position;

            SortObject(depth, 30);
        }

        private void ChangeCurrentSpeedLevel(int level)
        {
            var speedDelta = SpeedCoeffForSmooth * BaseSpeed;
            CurrentSpeed += level * speedDelta;
            if (CurrentSpeed < speedDelta * Config.MinSpeedLevel)
            {
                CurrentSpeed = speedDelta * Config.MinSpeedLevel;
            }
            else if (CurrentSpeed > BaseSpeed)
            {
                CurrentSpeed = BaseSpeed;
            }
        }
       
        private void MoveTo(TVertex vertex)
        {
            if (UseSmoothStop)
            {
                var nextPoint = GetPathVertex(2);
                if (nextPoint == null && IsMovingByPath)
                {
                    ChangeCurrentSpeedLevel(Config.SmoothRunningLevel);
                }
                if (nextPoint != null)
                {
                    ChangeCurrentSpeedLevel(SuitableForStay(nextPoint) ? 
                        Config.SmoothRunningLevel : Config.SmoothStoppingLevel);
                }
            }

            var newPosition = Vector3.MoveTowards(CurrentPosition, vertex.WorldPosition,
                CurrentSpeed * Time.deltaTime);

            // select the lowest depth from nodes to avoid sorting problems
            // locomotor must stay in most cases behind objects
            var depth = CurrentVertex.RoadView.Depth < vertex.RoadView.Depth
                ? CurrentVertex.RoadView.Depth
                : vertex.RoadView.Depth;

            SetPosition(newPosition, depth);
            OnOccupiedVertexChanging(CurrentVertex, vertex);
        }

        private void BuildPathToDestination()
        {
            UnityEngine.Profiling.Profiler.BeginSample(NameForLog + " Find Path");
          
            TVertex startNode = _lastCurrentVertex;
            if (startNode == DestinationVertex) 
            {
                CurrentPath.Add (startNode);
            }
            else
            {
                var pathFinder = IsMovingByPath ? _shortestPathFinder : _randomPathFinder;
                var newPath = pathFinder.FindPath (startNode, DestinationVertex, Graph);
                if (newPath.Count == 0)
                {
                    throw new Exception(NameForLog + " path not found. Unreacheble node : "
                        + DestinationVertex.MatrixPosition);
                }
                CurrentPath = newPath;
            }
            UnityEngine.Profiling.Profiler.EndSample();
        }

        protected void SetRandomDestination()
        {
            SetDestination(GetRandomReachableVertex());
        }

        protected TVertex GetRandomReachableVertex()
        {
            if (Graph.ReachableVertices.Count == 0)
                throw new ArgumentException (NameForLog + "graph doesn't contains reachable vertex");
            return Graph.ReachableVertices[Random.Range(0, Graph.ReachableVertices.Count)];
        }

        protected void SetDestination(TVertex vertex)
        {
            DestinationVertex = vertex;
            BuildPathToDestination();
            IsHasDestination = true;
        }

        protected float GetDistanceToVertex(TVertex vertex)
        {
            return Vector3.Distance(CurrentPosition, vertex.WorldPosition);
        }

        private void MoveToNextPathPoint()
        {
            if (_destinationStartedCount != 0)
            {
                _pathData.RemoveAt(0);
                SignalForPathMoving.Dispatch(Id, MovedByPathEventsType.InNextPoint);
            }
            if (_pathData.Count > 0)
            {
                PauseTime = _pathData[0].Value;
            }
            if (_pathData.Count > 1)
            {
                SetDestination( _pathData[1].Key);
            }
            else
            {
                _pathData.Clear();
                SignalForPathMoving.Dispatch(Id, MovedByPathEventsType.Comleted);
                SetDestination(Graph.CityExitVertex);
            }
        }

        /// <summary>
        /// Raises the occupied vertex changing event.
        /// </summary>
        /// <param name="previusOccupied">Previus occupied.</param>
        /// <param name="newOccupied">New occupied.</param>
        protected virtual void OnOccupiedVertexChanging(TVertex previusOccupied,
            TVertex newOccupied)
        {
        }

        private void Update()
        {
            if (PauseTime > 0)
            {
                StoppingLogicUpdate();
                PauseTime -= Time.deltaTime;
                return;
            }
            if (!IsHasDestination || IsAnimationPlaying)
            {
                return;
            }

            UpdateNextVertex();
            var isReachedLastPoint = NextVertex == null;
            if (isReachedLastPoint)
            {
                InTargetLogicUpdate();
            }
            else if (IsNodeReached(NextVertex))
            {
                SetCurrentVertex(NextVertex, false);
                LastDirection = DirectionVectorType.Zero;
            }
            else if (CanMoveToVertex(NextVertex))
            {
                if (IsMovingByPath && _destinationStartedCount == 0)
                {
                    _destinationStartedCount++;
                    SignalForPathMoving.Dispatch(Id, MovedByPathEventsType.Started);
                }
                UpdateSprite(NextVertex);
                MoveTo(NextVertex);
            }
            else
            {
                UpdateSprite(NextVertex);
                StoppingLogicUpdate();
            }
            if (!isReachedLastPoint && NextVertex == null)
            {
                var node = GetPathVertex();
                if (node == null)
                {
                    InTargetLogicUpdate();
                }
            }
        }

        private void SortObject(float depth, float cameraDegrees)
        {
            // +2 to count ground and roads layers
            depth += 2;

            var izoPosition = CacheMonobehavior.CachedTransform.position;
            var depthOffset = MapObjectGeometry.GetHeightOffset(depth, cameraDegrees);

            izoPosition.x -= depthOffset;
            izoPosition.z -= depthOffset;
            izoPosition.y += depth;

            CacheMonobehavior.CachedTransform.localPosition = izoPosition;
        }

        private void InTargetLogicUpdate()
        {
            SetPosition(CurrentVertex.WorldPosition, CurrentVertex.RoadView.Depth);

            _destinationStartedCount++;

            if (IsMovingByPath)
            {
                MoveToNextPathPoint();
            }
            else
            {
                DefaultInTargetLogicUpdate();
            }
        }
    }
}