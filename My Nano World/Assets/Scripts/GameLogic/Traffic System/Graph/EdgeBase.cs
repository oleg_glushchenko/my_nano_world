﻿using NanoReality.GameLogic.TrafficSystem.Graph;
using UnityEngine;

namespace NanoReality.GameLogic.TrafficSystem.Graph
{
    public abstract class EdgeBase<TVertex> : IEdge<TVertex> 
        where TVertex : VertexBase
    {
        public EdgeBase(TVertex start, TVertex end)
        {
            Start = start;
            End = end;
            Length = Vector3.Distance(start.WorldPosition, end.WorldPosition);
            IsHorizontal = Mathf.Abs(start.MapPosition.y - end.MapPosition.y) < float.Epsilon;
        }

        #region Implementation of IEdge<out SidewalkVertex>

        public TVertex Start { get; private set; }
        public TVertex End { get; private set; }
        public float Length { get; private set; }

        #endregion

        public bool IsHorizontal { get; private set; }
    }
}