﻿using System.Collections.Generic;

namespace NanoReality.GameLogic.TrafficSystem.Graph
{
    public interface IGraph<TVertex, TEdge> 
        where TVertex : IVertex 
        where TEdge : IEdge<TVertex>
    {
        IEnumerable<TVertex> Vertices { get; }

        IEnumerable<TEdge> Edges { get; }

        IList<TEdge> GetEdges(TVertex start);

        TEdge GetEdge(TVertex start, TVertex end);
    }
}
