﻿using System;
using UnityEngine;

namespace NanoReality.GameLogic.TrafficSystem.Graph
{
    public interface IGraphMove
    {
        Vector3 CurrentPosition { get; }
        bool IsMovingByPath { get; }
    }
}