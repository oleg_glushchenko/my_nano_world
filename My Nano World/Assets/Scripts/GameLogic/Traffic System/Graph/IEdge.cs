﻿namespace NanoReality.GameLogic.TrafficSystem.Graph
{
    public interface IEdge<out TVertex> where TVertex : IVertex
    {
        TVertex Start { get; }
        TVertex End { get; }
        float Length { get; }
    }
}
