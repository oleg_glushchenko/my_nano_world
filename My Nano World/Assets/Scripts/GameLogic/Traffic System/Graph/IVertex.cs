﻿using UnityEngine;
using Assets.NanoLib.Utilities;

namespace NanoReality.GameLogic.TrafficSystem.Graph
{
    public interface IVertex
    {
        Vector3 WorldPosition { get; }
        Vector2F MatrixPosition { get; }
    }
}