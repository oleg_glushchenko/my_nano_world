﻿using System.Collections.Generic;
using Assets.Scripts.Engine.BuildingSystem.CityMap;
using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.TrafficSystem.Graph;
using NanoReality.GameLogic.TrafficSystem.Road;
using UnityEngine;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.GameManager.Models.api;

namespace NanoReality.GameLogic.TrafficSystem
{
    public abstract class GraphBase<TVertex, TEdge> : IGraph<TVertex, TEdge>
        where TVertex : VertexBase 
        where TEdge : EdgeBase<TVertex>
    {
        private readonly HashSet<TVertex> _vertices = new HashSet<TVertex>();
        private readonly HashSet<TEdge> _edges = new HashSet<TEdge>();
        private readonly Dictionary<TVertex, List<TEdge>> _edgesByVertex = new Dictionary<TVertex, List<TEdge>>();
        private readonly List<TVertex> _reachableVertices = new List<TVertex>();
        private readonly HashSet<RoadMapObjectView> _visitedRoadViews = new HashSet<RoadMapObjectView>();

        private TVertex _cityExitVertex;
        private TVertex _cityEnterVertex;
        private TrafficConfig _trafficConfig;

        protected readonly Dictionary<Vector2F, Dictionary<Directions, TVertex>> VerticesByPosition = new Dictionary<Vector2F, Dictionary<Directions, TVertex>>();

        #region Implementation of IGraph<TVertex, TEdge>

        public IEnumerable<TVertex> Vertices { get { return _vertices; } }

        public IEnumerable<TEdge> Edges { get { return _edges; } }

        public IList<TEdge> GetEdges(TVertex start)
        {
            List<TEdge> edges;
            return _edgesByVertex.TryGetValue(start, out edges) ? edges : null;
        }

        public TEdge GetEdge(TVertex start, TVertex end)
        {
            List<TEdge> edges;
            if (_edgesByVertex.TryGetValue(start, out edges))
            {
                var edgeCount = edges.Count;
                for (int i = 0; i < edgeCount; i++)
                {
                    if (edges[i].End == end)
                    {
                        return edges[i];
                    }
                }
            }

            return null;
        }

        #endregion

        #region abstract methods

        public abstract string GraphTargetElementName { get; }

        protected abstract TVertex CreateVertex(RoadMapObjectView roadView, Vector3 offset, Directions direction);

        protected abstract TEdge CreateEdge(TVertex start, TVertex end);

        protected abstract RoadTileNodeConfig GetTileConfig(TrafficConfig config, RoadTileType roadType);

        protected abstract void AddInnerEdges(RoadMapObjectView roadView);

        protected abstract void AddAdjacentEdges(RoadMapObjectView roadView, Directions direction);


        #endregion

        public List<TVertex> ReachableVertices { get { return _reachableVertices; } }
        public TVertex CityExitVertex { get { return _cityExitVertex; } }
        public TVertex CityEnterVertex { get { return _cityEnterVertex; } }
        public bool HasEnoughReachableVertices { get { return ReachableVertices.Count > 4; } }
        
        [Inject] public IGameManager jGameManager { get; set; }

        public void Build(RoadViewGrid grid, TrafficConfig trafficConfig)
        {
            BeginSample("Build {0} Graph");

            _trafficConfig = trafficConfig;

            Clear();

            foreach (var roadView in grid.Roads)
            {
                AddVertices(roadView);
            }

            foreach (var roadView in grid.Roads)
            {
                AddInnerEdges(roadView);
                AddOuterEdges(roadView);
            }

            SearchReachableVertices(_cityExitVertex);

            EndSample();
        }

        public void Clear()
        {
            _vertices.Clear();
            _edges.Clear();

            VerticesByPosition.Clear();
            _edgesByVertex.Clear();

            _reachableVertices.Clear();
            _visitedRoadViews.Clear();

            _cityExitVertex = null;
            _cityEnterVertex = null;
        }

        public void Refresh()
        {
            BeginSample("Refresh {0} Graph");

            SearchReachableVertices(_cityExitVertex);

            EndSample();
        }

        public void AddRoadView(RoadMapObjectView roadView)
        {
            if (VerticesByPosition.ContainsKey(roadView.MapObjectModel.MapPosition))
            {
                return;
            }

            BeginSample("{0} Graph Add Road View");

            AddVertices(roadView);
            AddInnerEdges(roadView);
            AddOuterEdges(roadView);
            UpdateAdjacentRoadViews(roadView);

            EndSample();
        }

        public void RemoveRoadView(RoadMapObjectView roadView)
        {
            if (!VerticesByPosition.ContainsKey(roadView.MapObjectModel.MapPosition))
            {
                return;
            }

            BeginSample("{0} Graph Remove Road View");

            RemoveEdges(roadView);
            RemoveVertices(roadView);
            UpdateAdjacentRoadViews(roadView);

            UnityEngine.Profiling.Profiler.EndSample();
        }


        public IDictionary<Directions, TVertex> GetVerticesByMapPosition(Vector2F mapPosition)
        {
            Dictionary<Directions, TVertex> result;
            return VerticesByPosition.TryGetValue(mapPosition, out result) ? result : null;
        }

        public TVertex GetVerticesByMapPosition(Vector2F mapPosition, Directions dir)
        {
            var vertice = GetVerticesByMapPosition(mapPosition);
            return vertice != null && vertice.ContainsKey(dir) ? vertice[dir] : null;
        }

        public TVertex GetVerticle(RoadMapObjectView roadView, Directions dir)
        {
            var count = _reachableVertices.Count;
            for (int i = 0; i < count; i++)
            {
                var item = _reachableVertices[i];
                if (item.RoadView.Equals(roadView) && item.Direction == dir)
                {
                    return item;
                }
            }
            return null;
        }

        private void RemoveEdges(RoadMapObjectView roadView)
        {
            var vertices = GetVerticesByMapPosition(roadView.MapObjectModel.MapPosition);
            foreach (var pair in vertices)
            {
                List<TEdge> edges;
                if (_edgesByVertex.TryGetValue(pair.Value, out edges))
                {
                    foreach (var edge in edges)
                    {
                        _edges.Remove(edge);
                    }
                    _edgesByVertex.Remove(pair.Value);
                }
            }
        }

        private void RemoveVertices(RoadMapObjectView roadView)
        {
            var vertices = GetVerticesByMapPosition(roadView.MapObjectModel.MapPosition);
            if (vertices != null)
            {
                foreach (var pair in vertices)
                {
                    _vertices.Remove(pair.Value);
                }
                VerticesByPosition.Remove(roadView.MapObjectModel.MapPosition);
            }
        }

        private void UpdateAdjacentRoadViews(RoadMapObjectView roadView)
        {
            foreach (var adjacent in roadView.Adjacent)
            {
                RemoveEdges(adjacent.Value);
                UpdateVertices(adjacent.Value);
                AddInnerEdges(adjacent.Value);
                AddOuterEdges(adjacent.Value);
            }
        }

        private void UpdateVertices(RoadMapObjectView roadView)
        {
            var config = GetTileConfig(_trafficConfig, roadView.RoadTileType);
            var vertices = GetVerticesByMapPosition(roadView.MapObjectModel.MapPosition);
            foreach (var pair in vertices)
            {
                var offset = config.GetOffset(pair.Key);
                pair.Value.SetOffset(offset);
            }
        }

        private void AddVertices(RoadMapObjectView roadView)
        {
            var config = GetTileConfig(_trafficConfig, roadView.RoadTileType);

            var leftBottom = CreateVertex(roadView, config.LeftBottom, Directions.LeftBottom);
            _vertices.Add(leftBottom);

            var rightBottom = CreateVertex(roadView, config.RightBottom, Directions.RightBottom);
            _vertices.Add(rightBottom);

            var leftTop = CreateVertex(roadView, config.LeftTop, Directions.LeftTop);
            _vertices.Add(leftTop);

            var rightTop = CreateVertex(roadView, config.RightTop, Directions.RightTop);
            _vertices.Add(rightTop);

            var verticles =
                new Dictionary<Directions, TVertex>(new DirectionsComparer())
            {
                {Directions.LeftBottom, leftBottom},
                {Directions.RightBottom, rightBottom},
                {Directions.LeftTop, leftTop},
                {Directions.RightTop, rightTop}
            };

            VerticesByPosition.Add(roadView.MapObjectModel.MapPosition, verticles);

            if (roadView.MapObjectModel.Level != 200)
                return;

            int xPos = Mathf.RoundToInt(roadView.GridPosition.x);
            int yPos = Mathf.RoundToInt(roadView.GridPosition.y);
            // TODO: commented for code compilation.
//            var cityMap = jGameManager.GetMap(roadView.MapObjectModel.SectorId);
//
//            if (xPos == 0)
//            {
//                _cityExitVertex = leftTop;
//                _cityEnterVertex = leftBottom;
//            }
//            else if (yPos == 0)
//            {
//                _cityExitVertex = leftBottom;
//                _cityEnterVertex = rightBottom;
//            }
//            else if (xPos == cityMap.MapSize.Width - 1)
//            {
//                _cityExitVertex = rightBottom;
//                _cityEnterVertex = rightTop;
//            }
//            else if (yPos == cityMap.MapSize.Height - 1)
//            {
//                _cityExitVertex = rightTop;
//                _cityEnterVertex = leftTop;
//            }
        }

        private void AddOuterEdges(RoadMapObjectView roadView)
        {
            switch (roadView.RoadTileType)
            {
                case RoadTileType.Deadlock_LB:
                    AddAdjacentEdges(roadView, Directions.LeftBottom);
                    break;

                case RoadTileType.Deadlock_RT:
                    AddAdjacentEdges(roadView, Directions.RightTop);
                    break;

                case RoadTileType.Deadlock_LT:
                    AddAdjacentEdges(roadView, Directions.LeftTop);
                    break;

                case RoadTileType.Deadlock_RB:
                    AddAdjacentEdges(roadView, Directions.RightBottom);
                    break;

                case RoadTileType.Straight_LB_RT:
                    AddAdjacentEdges(roadView, Directions.LeftBottom);
                    AddAdjacentEdges(roadView, Directions.RightTop);
                    break;

                case RoadTileType.Straight_LT_RB:
                    AddAdjacentEdges(roadView, Directions.LeftTop);
                    AddAdjacentEdges(roadView, Directions.RightBottom);
                    break;

                case RoadTileType.Intersection:
                    AddAdjacentEdges(roadView, Directions.LeftTop);
                    AddAdjacentEdges(roadView, Directions.LeftBottom);
                    AddAdjacentEdges(roadView, Directions.RightTop);
                    AddAdjacentEdges(roadView, Directions.RightBottom);
                    break;

                case RoadTileType.CityEnter:
                    AddAdjacentEdges(roadView, Directions.LeftTop);
                    AddAdjacentEdges(roadView, Directions.LeftBottom);
                    AddAdjacentEdges(roadView, Directions.RightTop);
                    AddAdjacentEdges(roadView, Directions.RightBottom);
                    break;

                case RoadTileType.Tside_LB_LT_RB:
                    AddAdjacentEdges(roadView, Directions.LeftBottom);
                    AddAdjacentEdges(roadView, Directions.LeftTop);
                    AddAdjacentEdges(roadView, Directions.RightBottom);
                    break;

                case RoadTileType.Tside_LT_LB_RT:
                    AddAdjacentEdges(roadView, Directions.LeftTop);
                    AddAdjacentEdges(roadView, Directions.LeftBottom);
                    AddAdjacentEdges(roadView, Directions.RightTop);
                    break;

                case RoadTileType.Tside_RB_LB_RT:
                    AddAdjacentEdges(roadView, Directions.RightBottom);
                    AddAdjacentEdges(roadView, Directions.LeftBottom);
                    AddAdjacentEdges(roadView, Directions.RightTop);
                    break;

                case RoadTileType.Tside_RT_LT_RB:
                    AddAdjacentEdges(roadView, Directions.RightTop);
                    AddAdjacentEdges(roadView, Directions.LeftTop);
                    AddAdjacentEdges(roadView, Directions.RightBottom);
                    break;

                case RoadTileType.Turn_LB_RB:
                    AddAdjacentEdges(roadView, Directions.LeftBottom);
                    AddAdjacentEdges(roadView, Directions.RightBottom);
                    break;

                case RoadTileType.Turn_LT_RT:
                    AddAdjacentEdges(roadView, Directions.LeftTop);
                    AddAdjacentEdges(roadView, Directions.RightTop);
                    break;

                case RoadTileType.Turn_RB_RT:
                    AddAdjacentEdges(roadView, Directions.RightBottom);
                    AddAdjacentEdges(roadView, Directions.RightTop);
                    break;

                case RoadTileType.Turn_LB_LT:
                    AddAdjacentEdges(roadView, Directions.LeftBottom);
                    AddAdjacentEdges(roadView, Directions.LeftTop);
                    break;
            }
        }

        protected void AddDirectionalEdge(TVertex start, TVertex end)
        {
            var edge = CreateEdge(start, end);
            _edges.Add(edge);

            List<TEdge> vertexEdges;
            if (!_edgesByVertex.TryGetValue(start, out vertexEdges))
            {
                vertexEdges = new List<TEdge>();
                _edgesByVertex[start] = vertexEdges;
            }

            vertexEdges.Add(edge);
        }

        private void SearchReachableVertices(TVertex start)
        {
            _reachableVertices.Clear();
            _visitedRoadViews.Clear();

            if (start == null)
            {
                return;
            }

            var roadView = start.RoadView;

            AddReachableVertices(roadView, _visitedRoadViews);
        }

        private void AddReachableVertices(RoadMapObjectView roadView, ICollection<RoadMapObjectView> visited)
        {
            if (roadView == null || visited.Contains(roadView))
            {
                return;
            }

            foreach (var pair in VerticesByPosition[roadView.MapObjectModel.MapPosition])
            {
                _reachableVertices.Add(pair.Value);
            }

            visited.Add(roadView);

            foreach (var pair in roadView.Adjacent)
            {
                AddReachableVertices(pair.Value, visited);
            }
        }

        private void BeginSample(string messageFormat)
        {
            UnityEngine.Profiling.Profiler.BeginSample(
                string.Format(messageFormat, GraphTargetElementName));
        }

        private void EndSample()
        {
            UnityEngine.Profiling.Profiler.EndSample();
        }
    }
}
