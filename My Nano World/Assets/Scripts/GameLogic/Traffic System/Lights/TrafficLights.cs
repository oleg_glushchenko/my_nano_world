﻿using System;
using System.Collections;
using System.Collections.Generic;
using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.TrafficSystem.Citizens;
using UniRx;
using UnityEngine;
using Random = UnityEngine.Random;

namespace NanoReality.GameLogic.TrafficSystem.Lights
{
    public class TrafficLights
    {
        private readonly RoadMapObjectView _view;
        private readonly List<RoadGraphNode> _roadNodes = new List<RoadGraphNode>();
        private readonly List<SidewalkVertex> _sidewalkVertices = new List<SidewalkVertex>();

        private IDisposable _disposable;

        public TrafficLights(RoadMapObjectView view)
        {
            _view = view;
        }

        public void AddRoadNode(RoadGraphNode node)
        {
            _roadNodes.Add(node);
        }

        public void AddSidewalkVertex(SidewalkVertex vertex)
        {
            _sidewalkVertices.Add(vertex);
        }

        public void Start(TrafficConfig trafficConfig)
        {
            Stop();
            _disposable = Observable.FromCoroutine(() => StartSwitchingLights(trafficConfig)).Subscribe();
        }

        public void Stop()
        {
            _disposable?.Dispose();
            
            SetTrafficLights(true);
            SetPedestrianLights(true);
        }

        private IEnumerator StartSwitchingLights(TrafficConfig trafficConfig)
        {
            var count = _roadNodes.Count;

            if (count == 0)
            {
                yield break;
            }

            var currentNode = Random.Range(0, count);
            var waitForGreenLight = new WaitForSeconds(trafficConfig.GreenLightDuration);
            var waitForPedestrians = new WaitForSeconds(trafficConfig.PedestrianLightDuration);
            
            // set all lights red
            SetTrafficLights(false);

            // wait a bit to make all lights a bit random
            yield return new WaitForSeconds(Random.Range(0f, trafficConfig.GreenLightDuration));

            while (true)
            {
                SetPedestrianLights(false);

                SetLight(_roadNodes[currentNode], true);

                yield return waitForGreenLight;
                
                SetLight(_roadNodes[currentNode], false);

                SetPedestrianLights(true);

                yield return waitForPedestrians;

                currentNode++;

                if (currentNode >= count)
                {
                    currentNode = 0;
                }
            }
        }

        private void SetLight(RoadGraphNode node, bool isGreen)
        {
            node.IsBlocked = !isGreen;
        }

        private void SetTrafficLights(bool isGreen)
        {
            var count = _roadNodes.Count;
            for (var i = 0; i < count; i++)
            {
                SetLight(_roadNodes[i], isGreen);
            }
        }

        private void SetPedestrianLights(bool isGreen)
        {
            var count = _sidewalkVertices.Count;
            for (var i = 0; i < count; i++)
            {
                var vertex = _sidewalkVertices[i];
                vertex.IsBlocked = !isGreen;
            }
        }
    }
}
