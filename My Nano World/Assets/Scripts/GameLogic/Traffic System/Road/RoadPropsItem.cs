﻿using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.Utilities;
using UnityEngine;

namespace NanoReality.GameLogic.TrafficSystem.Road
{
    public class RoadPropsItem : MonoBehaviour
    {
        [SerializeField]
        private MeshFilter _meshFilter;
        [SerializeField]
        private MeshRenderer _meshRenderer;
        
        private Transform _transform;
        private Vector3 _offset;

        public MeshFilter MeshFilter
        {
            get { return _meshFilter; }
        }

        public MeshRenderer Renderer
        {
            get { return _meshRenderer; }
        }

        protected virtual void Awake()
        {
            _transform = GetComponent<Transform>();
            _meshFilter = GetComponent<MeshFilter>();
            _offset = Vector3.zero;
        }

        public void Set(RoadPropsParams entry, Directions direction)
        {
            switch (direction)
            {
                case Directions.LeftTop:
                    Set(entry.LeftTop.Offset, entry.LeftTop.Rotation);
                    break;
                case Directions.LeftBottom:
                    Set(entry.LeftBottom.Offset, entry.LeftBottom.Rotation);
                    break;
                case Directions.RightTop:
                    Set(entry.RightTop.Offset, entry.RightTop.Rotation);
                    break;
                case Directions.RightBottom:
                    Set(entry.RightBottom.Offset, entry.RightBottom.Rotation);
                    break;
            }
        }

        public void Set(Vector3 offset, Vector3 rotation)
        {
            _transform.localPosition = offset;
            _transform.localRotation = Quaternion.Euler(rotation);

            _offset = offset;
        }

        public void SortObject(float depth, float cameraDegrees)
        {
            _transform.localPosition = _offset;
            
            var izoPosition = _transform.position;
            var depthOffset = MapObjectGeometry.GetHeightOffset(depth, cameraDegrees);

            izoPosition.x -= depthOffset;
            izoPosition.z -= depthOffset;
            izoPosition.y += depth;

            _transform.position = izoPosition;
        }
    }
}
