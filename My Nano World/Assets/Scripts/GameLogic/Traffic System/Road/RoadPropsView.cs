﻿using System.Collections.Generic;
using NanoReality.Engine.BuildingSystem.MapObjects;
using UnityEngine;

namespace NanoReality.GameLogic.TrafficSystem.Road
{
    public class RoadPropsView : MonoBehaviour
    {
        [SerializeField]
        private RoadPropsConfig _roadPropsConfig;

        private readonly List<RoadPropsItem> _items = new List<RoadPropsItem>();

        private readonly Dictionary<Directions, RoadPropsItem> _trafficLightItems = new Dictionary<Directions, RoadPropsItem>(new DirectionsComparer());

        public bool IsShown { get { return _items.Count > 0; } }

        private bool CanAddRandomProps { get { return Random.value > 0.5f; } }
        
        public Material Material
        {
            set
            {
                foreach (var roadPropsItem in _items)
                {
                    roadPropsItem.Renderer.material = value;
                }
            }
        }

        public void Clear()
        {
            _trafficLightItems.Clear();
            foreach (var roadPropsItem in _items)
            {
                Destroy(roadPropsItem.gameObject);
            }
            _items.Clear();
        }

        public void UpdateRoadProps(RoadMapObjectView view)
        {
            Clear();

            // check for traffic lights
            
            if (view.RoadTileType.IsCrossroads())
            {
                var entry = _roadPropsConfig.GetRandomTrafficLightParams();

                foreach (var pair in view.Adjacent)
                {
                    // avoid traffic lights for adjacent crossroads
                    if (!pair.Value.RoadTileType.IsCrossroads())
                    {
                        AddTrafficLight(pair.Key, entry);
                    }
                }

                return;
            }

            // check for signs
            
            // limit tiles to only those that have no adjacent crossroads and are straight
            if (CanAddRandomProps && !view.HasAdjacentCrossroads && view.RoadTileType.IsStraight())
            {
                foreach (var pair in view.Adjacent)
                {
                    // put a sign only when adjacent tile has adjacent crossroads
                    if (pair.Value.HasAdjacentCrossroads)
                    {
                        var entry = _roadPropsConfig.GetRandomSignParams();
                        if (entry != null)
                        {
                            AddPropsItem(entry.Prefab).Set(entry, pair.Key);
                        }

                        return;
                    }
                }
            }

            // check for lamps

            // add a lamp only when adjacent tiles have neither adjacent crossroads nor props
            if (!view.HasAdjacentCrossroads && !view.HasAdjacentProps)
            {
                var entry = _roadPropsConfig.GetLampParams(view.RoadTileType, view.RoadMarkingsType);
                if (entry != null)
                {
                    AddPropsItem(entry.Prefab).Set(entry.Transform.Offset, entry.Transform.Rotation);
                }
            }
        }

        public void SortObject(float depth, float cameraDegrees)
        {
            foreach (var roadPropsItem in _items)
            {
                roadPropsItem.SortObject(depth, cameraDegrees);
            }
        }

        public RoadPropsItem GetTrafficLightItem(Directions direction)
        {
            RoadPropsItem item;
            return _trafficLightItems.TryGetValue(direction, out item) ? item : null;
        }

        private RoadPropsItem AddPropsItem(GameObject prefab)
        {
            //TODO: think of using object pool here
            var item = Instantiate(prefab, transform, false).GetComponent<RoadPropsItem>();
            _items.Add(item);
            return item;
        }

        private void AddTrafficLight(Directions direction, RoadPropsParams entry)
        {
            var item = AddPropsItem(entry.Prefab);
            item.Set(entry, direction);
            _trafficLightItems.Add(direction, item);
        }
    }
}
