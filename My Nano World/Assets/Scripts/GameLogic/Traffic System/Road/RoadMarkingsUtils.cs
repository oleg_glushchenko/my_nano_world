﻿using System;
using System.Collections.Generic;
using NanoReality.Engine.BuildingSystem.MapObjects;

namespace NanoReality.GameLogic.TrafficSystem.Road
{
    public static class RoadMarkingsUtils
    {
        /// <summary>
        /// Set of visited roads. Used in markings randomization logic.
        /// </summary>
        private static readonly HashSet<RoadMapObjectView> VisitedRoads = new HashSet<RoadMapObjectView>();

        /// <summary>
        /// Returns road markings type based on road tile type and adjacent tiles of given road map object view.
        /// </summary>
        /// <param name="view"></param>
        /// <returns></returns>
        public static RoadMarkingsType GetRoadTileMarkings(RoadMapObjectView view)
        {
            var tileShape = view.RoadTileType.ToShape();

            switch (tileShape)
            {
                case RoadTileShape.Crossroads:
                    return RoadMarkingsType.Zebra;
                case RoadTileShape.Straight:
                    return GetStraightTileMarkings(view);
                case RoadTileShape.Deadlock:
                    return GetDeadlockMarkings(view);
                case RoadTileShape.Turn:
                    return GetTurnMarkings(view);
            }

            throw new Exception("Unsupported tile shape: " + tileShape);
        }

        private static RoadMarkingsType GetStraightTileMarkings(RoadMapObjectView view)
        {
            var adjacent = view.Adjacent;

            if (adjacent.ContainsKey(Directions.LeftBottom) && adjacent.ContainsKey(Directions.RightTop))
            {
                var leftBottom = adjacent[Directions.LeftBottom];
                var rightTop = adjacent[Directions.RightTop];

                return GetStraightTileMarkings(view, leftBottom, rightTop);
            }

            if (adjacent.ContainsKey(Directions.LeftTop) && adjacent.ContainsKey(Directions.RightBottom))
            {
                var leftTop = adjacent[Directions.LeftTop];
                var rightBottom = adjacent[Directions.RightBottom];

                return GetStraightTileMarkings(view, leftTop, rightBottom);
            }

            // An edge case when road tile leads out of city
            foreach (var pair in adjacent)
            {
                var tile = pair.Value;
                if (tile.RoadTileType.IsCrossroads())
                {
                    return GetRandomMarkingsWithoutDivider();
                }
                if (tile.IsViewUpdated)
                {
                    return tile.RoadMarkingsType;
                }
            }

            return GetRandomMarkingsWithoutDivider();
        }

        private static RoadMarkingsType GetStraightTileMarkings(RoadMapObjectView current, RoadMapObjectView left, RoadMapObjectView right)
        {
            if (left.RoadTileType.IsCrossroads() && right.RoadTileType.IsCrossroads())
            {
                return GetRandomMarkingsWithoutDivider();
            }
            if (left.RoadTileType.IsCrossroads())
            {
                if (!right.IsViewUpdated)
                {
                    return DoesRoadEndWithTurn(left, current) ? GetRandomMarkingsWithoutDivider() : GetRandomMarkings(true);
                }
                if (right.RoadMarkingsType.IsDivider())
                {
                    return RoadMarkingsType.DividerEnd;
                }
                return right.RoadMarkingsType;
            }
            if (right.RoadTileType.IsCrossroads())
            {
                if (!left.IsViewUpdated)
                {
                    return DoesRoadEndWithTurn(right, current) ? GetRandomMarkingsWithoutDivider() : GetRandomMarkings(true);
                }
                if (left.RoadMarkingsType.IsDivider())
                {
                    return RoadMarkingsType.DividerEnd;
                }
                return left.RoadMarkingsType;
            }
            if (left.IsViewUpdated)
            {
                if (left.RoadMarkingsType.IsDivider())
                {
                    return RoadMarkingsType.DividerFull;
                }
                return left.RoadMarkingsType;
            }
            if (right.IsViewUpdated)
            {
                if (right.RoadMarkingsType.IsDivider())
                {
                    return RoadMarkingsType.DividerFull;
                }
                return right.RoadMarkingsType;
            }
            return GetRandomMarkingsWithoutDivider();
        }

        private static bool DoesRoadEndWithTurn(RoadMapObjectView crossroads, RoadMapObjectView current)
        {
            VisitedRoads.Clear();
            VisitedRoads.Add(crossroads);
            return DoesRoadSegmentEndWithTurn(crossroads, current);
        }

        private static bool DoesRoadSegmentEndWithTurn(RoadMapObjectView prev, RoadMapObjectView current)
        {
            VisitedRoads.Add(current);

            if (current.RoadTileType.IsCrossroads())
            {
                return prev.RoadTileType.IsTurn();
            }
            if (current.RoadTileType.IsDeadlock())
            {
                return false;
            }

            foreach (var pair in current.Adjacent)
            {
                var next = pair.Value;
                if (!VisitedRoads.Contains(next))
                {
                    return DoesRoadEndWithTurn(current, next);
                }
            }

            return false;
        }

        private static RoadMarkingsType GetDeadlockMarkings(RoadMapObjectView view)
        {
            if (view.Adjacent.Count != 1)
            {
                throw new Exception("Deadlock tile must have one adjacent tile!");
            }

            var enumerator = view.Adjacent.GetEnumerator();
            enumerator.MoveNext();
            var tile = enumerator.Current.Value;
            enumerator.Dispose();

            if (tile.RoadTileType.IsCrossroads())
            {
                return GetRandomMarkingsWithoutDivider();
            }
            if (tile.IsViewUpdated)
            {
                if (tile.RoadMarkingsType.IsDivider())
                {
                    return RoadMarkingsType.DividerEnd;
                }
                return tile.RoadMarkingsType;
            }
            return GetRandomMarkings(true);
        }

        private static RoadMarkingsType GetTurnMarkings(RoadMapObjectView view)
        {
            var adjacent = view.Adjacent;

            if (adjacent.ContainsKey(Directions.LeftBottom) && adjacent.ContainsKey(Directions.LeftTop))
            {
                var leftBottom = adjacent[Directions.LeftBottom];
                var leftTop = adjacent[Directions.LeftTop];

                return GetTurnMarkings(leftBottom, leftTop);
            }
            if (adjacent.ContainsKey(Directions.LeftBottom) && adjacent.ContainsKey(Directions.RightBottom))
            {
                var leftBottom = adjacent[Directions.LeftBottom];
                var rightBottom = adjacent[Directions.RightBottom];

                return GetTurnMarkings(leftBottom, rightBottom);
            }
            if (adjacent.ContainsKey(Directions.LeftTop) && adjacent.ContainsKey(Directions.RightTop))
            {
                var leftTop = adjacent[Directions.LeftTop];
                var rightTop = adjacent[Directions.RightTop];

                return GetTurnMarkings(leftTop, rightTop);
            }
            if (adjacent.ContainsKey(Directions.RightBottom) && adjacent.ContainsKey(Directions.RightTop))
            {
                var rightBottom = adjacent[Directions.RightBottom];
                var rightTop = adjacent[Directions.RightTop];

                return GetTurnMarkings(rightBottom, rightTop);
            }

            throw new Exception("Turn tile must have two adjacent tiles!");
        }

        private static RoadMarkingsType GetTurnMarkings(RoadMapObjectView a, RoadMapObjectView b)
        {
            if (a.RoadTileType.IsCrossroads() && b.RoadTileType.IsCrossroads())
            {
                return GetRandomMarkingsWithoutDivider();
            }
            if (a.RoadTileType.IsCrossroads())
            {
                if (b.IsViewUpdated)
                {
                    if (b.RoadMarkingsType.IsDivider())
                    {
                        return RoadMarkingsType.DividerFull;
                    }
                    return b.RoadMarkingsType;
                }
                return GetRandomMarkingsWithoutDivider();
            }
            if (b.RoadTileType.IsCrossroads())
            {
                if (a.IsViewUpdated)
                {
                    if (a.RoadMarkingsType.IsDivider())
                    {
                        return RoadMarkingsType.DividerFull;
                    }
                    return a.RoadMarkingsType;
                }
                return GetRandomMarkingsWithoutDivider();
            }
            if (a.IsViewUpdated)
            {
                if (a.RoadMarkingsType.IsDivider())
                {
                    return RoadMarkingsType.DividerFull;
                }
                return a.RoadMarkingsType;
            }
            if (b.IsViewUpdated)
            {
                if (b.RoadMarkingsType.IsDivider())
                {
                    return RoadMarkingsType.DividerFull;
                }
                return b.RoadMarkingsType;
            }
            return GetRandomMarkingsWithoutDivider();
        }

        private static RoadMarkingsType GetRandomMarkingsWithoutDivider()
        {
            return (RoadMarkingsType)2; // Random causes blinking while road editing
            return (RoadMarkingsType)UnityEngine.Random.Range(1, 3);
        }

        private static RoadMarkingsType GetRandomMarkings(bool endLine = false)
        {
            return (RoadMarkingsType)4; // Random causes blinking while road editing
            int value = UnityEngine.Random.Range(1, 4);
            if (endLine && value == 3) value++;
            return (RoadMarkingsType)value;
        }
    }
}
