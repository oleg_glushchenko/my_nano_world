﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace NanoReality.GameLogic.TrafficSystem.Road
{
    [CreateAssetMenu(fileName = "RoadTileConfig", menuName = "Traffic System/Road Tile Config", order = 999)]
    public class RoadTileConfig : ScriptableObject
    {
        [Serializable]
        public class MeshEntry
        {
            public Mesh Mesh;
            public List<RoadTileType> TileTypes;
            public RoadMarkingsType MarkingsType;
        }

        [Serializable]
        public class RotationEntry
        {
            public Vector3 Rotation;
            public List<RoadTileType> TileTypes;
        }

        [SerializeField]
        private List<MeshEntry> _meshEntries;

        [SerializeField]
        private List<RotationEntry> _rotationEntries;

        [SerializeField]
        private Mesh _defaultMesh;

        [SerializeField]
        private Vector3 _defaultRotation;

        public Mesh GetTileMesh(RoadTileType tileType, RoadMarkingsType markingsType)
        {
            foreach (var meshEntry in _meshEntries)
            {
                if (meshEntry.TileTypes.Contains(tileType) && meshEntry.MarkingsType == markingsType)
                {
                    return meshEntry.Mesh;
                }
            }

            Debug.LogErrorFormat("Could not find a road tile mesh! Type: {0} Markings: {1}", tileType, markingsType);

            return _defaultMesh;
        }

        public Vector3 GetTileRotation(RoadTileType tileType)
        {
            foreach (var rotationEntry in _rotationEntries)
            {
                if (rotationEntry.TileTypes.Contains(tileType))
                {
                    return rotationEntry.Rotation;
                }
            }
            return _defaultRotation;
        }
    }
}
