﻿using System;
using System.Collections.Generic;
using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.TrafficSystem.Road;
using UnityEngine;

namespace NanoReality.GameLogic.TrafficSystem
{
    [CreateAssetMenu(fileName = "TrafficConfig", menuName = "Traffic System/Traffic Config", order = 999)]
    public class TrafficConfig : ScriptableObject
    {
        [Header("Vehicles")]
        
        [SerializeField]
        [Range(1f, 2f)]
        private float _locomotorScaleFactor = 1f;

        [SerializeField]
        [Tooltip("Min speed level for moving")]
        private float _minSpeedLevel = 4;

        [SerializeField]
        [Tooltip("Gradually slowing down vehicles before obstacle")]
        private bool _stopVehicleGradually = true;

        [SerializeField]
        [Range(-30, -1)]
        [Tooltip("Smooth level deceleration for end move")]
        private int _smoothStoppingLevel = -3;

        [SerializeField]
        [Range(1, 30)]
        [Tooltip("Smooth level acceleration for start move")]
        private int _smoothRunningLevel = 10;

        [SerializeField]
        [Range(0, 1)]
        [Tooltip("Percentage of edge length for turning visuals")]
        private float _turningDistance = 0.1f;
        
        [Tooltip("Amount of random generated routs for vehicle before leaving sector")]
        public int _minRoutsBeforeExit = 5;
        
        [Tooltip("Amount of random generated routs for vehicle before leaving sector")]
        public int _maxRoutsBeforeExit = 10;

        [Header("Pedestrians")]
        
        [SerializeField]
        [Range(1f, 2f)]
        private float _citizenScaleFactor = 1f;

        [Header("Traffic Lights")]
        
        [SerializeField]
        [Tooltip("Duration is in seconds")]
        [Range(1f, 100f)]
        private float _greenLightDuration = 5f;

        [SerializeField]
        [Tooltip("Duration is in seconds")]
        [Range(1f, 100f)]
        private float _pedestrianLightDuration = 3f;
        
        [Header("Road Tiles")]

        [SerializeField]
        private List<RoadTileNodeConfig> _roadTileNodeConfigs;

        [SerializeField]
        [Tooltip("Tile type is ignored")]
        private RoadTileNodeConfig _defaultRoadTileNodeConfig;

        [SerializeField]
        private List<RoadTileNodeConfig> _sidewalkTileConfigs;

        [SerializeField]
        [Tooltip("Tile type is ignored")]
        private RoadTileNodeConfig _defaultSidewalkTileConfig;

        public float LocomotorScaleFactor { get { return _locomotorScaleFactor; } }

        public float CitizenScaleFactor { get { return _citizenScaleFactor; } }

        public float GreenLightDuration { get { return _greenLightDuration; } }

        public float PedestrianLightDuration { get { return _pedestrianLightDuration; } }

        public float MinSpeedLevel { get { return _minSpeedLevel; } }

        public int SmoothStoppingLevel { get { return _smoothStoppingLevel; } }

        public int SmoothRunningLevel { get { return _smoothRunningLevel; } }

        public float TurningDistance { get { return _turningDistance; } }
        
        public bool StopVehicleGradually {get { return _stopVehicleGradually; } }

        public RoadTileNodeConfig GetRoadTileNodeConfig(RoadTileType tileType)
        {
            foreach (var roadTileNodeConfig in _roadTileNodeConfigs)
            {
                if (roadTileNodeConfig.TileType == tileType)
                {
                    return roadTileNodeConfig;
                }
            }

            return _defaultRoadTileNodeConfig;
        }

        public RoadTileNodeConfig GetSidewalkTileConfig(RoadTileType tileType)
        {
            foreach (var sidewalkTileConfig in _sidewalkTileConfigs)
            {
                if (sidewalkTileConfig.TileType == tileType)
                {
                    return sidewalkTileConfig;
                }
            }

            return _defaultSidewalkTileConfig;
        }

        public int GetRandomRoutsAmount()
        {
            return UnityEngine.Random.Range(_minRoutsBeforeExit, _maxRoutsBeforeExit + 1);
        }
    }

    [Serializable]
    public class RoadTileNodeConfig
    {
        public RoadTileType TileType;
        public Vector3 LeftTop;
        public Vector3 LeftBottom;
        public Vector3 RightTop;
        public Vector3 RightBottom;

        public Vector3 GetOffset(Directions direction)
        {
            switch (direction)
            {
                case Directions.LeftTop:
                    return LeftTop;
                case Directions.LeftBottom:
                    return LeftBottom;
                case Directions.RightTop:
                    return RightTop;
                case Directions.RightBottom:
                    return RightBottom;
                default:
                    return Vector3.zero;
            }
        }
    }
}
