﻿using System.Collections;
using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using Assets.NanoLib.Utilities.Pulls;
using Assets.Scripts.Engine.BuildingSystem.CityMap;
using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.GameManager.Controllers;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.TrafficSystem.Citizens;
using NanoReality.GameLogic.TrafficSystem.Graph;
using NanoReality.GameLogic.TrafficSystem.Lights;
using NanoReality.GameLogic.TrafficSystem.Road;
using NanoReality.StrangeIoC;
using strange.extensions.injector.api;
using UnityEngine;
using NanoReality.GameLogic.Utilities;

namespace NanoReality.GameLogic.TrafficSystem
{
    public class TrafficController : AViewMediator, INeedRefreshPathFinder
    {
        private static int UnitCreatedCount = 0;

        [SerializeField]
        private BusinessSectorsTypes _sectorType = BusinessSectorsTypes.Town;
        
        #region Locomotors

        [SerializeField]
        private List<Locomotor> _locomotorPrefabs;
        [SerializeField]
        private Vector2 _locomotorSpawnIntervalRange = new Vector2(2f, 60f);
        [SerializeField]
        private float _locomotorsPerRoadTile = 0.1f; // 1 car per 10 road tiles
        [SerializeField]
        private float _locomotorsPerPeople = 0.2f; // 1 car per 5 people

        private readonly List<IObjectPullWithPeek> _locomotorPools = new List<IObjectPullWithPeek>();
        private readonly List<Locomotor> _currentLocomotors = new List<Locomotor>();

        private RoadsGraph _roadsGraph;
        private IPathFinder<RoadGraphNode, RoadGraphEdge> _roadPathRandomFinder;
        private IPathFinder<RoadGraphNode, RoadGraphEdge> _roadWavePathFinder;
        private Coroutine _locomotorSpawnRutine;

        #endregion

        #region Citizens

        [SerializeField]
        private List<CitizenView> _citizenPrefabs;
        [SerializeField]
        [Range(0f, 5f)]
        private float _citizensWalkingPerRoadTile = 0.2f; // 1 citizen per 5 road tiles
        [SerializeField]
        [Range(0f, 1f)]
        private float _citizensWalkingPercent = 0.1f; // 1 citizen per 10 citizens

        private readonly List<IObjectsPull> _citizenPools = new List<IObjectsPull>();
        private readonly List<CitizenView> _currentCitizens = new List<CitizenView>();

        private SidewalkGraph _sidewalkGraph;
        private IPathFinder<SidewalkVertex, SidewalkEdge> _sidewalkRandomPathFinder;
        private IPathFinder<SidewalkVertex, SidewalkEdge> _sidewalkWaveFinder;

        #endregion

        #region Traffic Lights
        
        private readonly List<TrafficLights> _trafficLights = new List<TrafficLights>();

        #endregion

        #region INeedRefreshPathFinder implementation

        public event System.Action GraphChanged;

        #endregion

        private CityMapView _mapView;
        private TrafficConfig _trafficConfig;
        private CacheMonobehavior _cacheMonobehaviour;
        private bool _hasRoadChanged;
        private bool _isTrafficGenerated;
        
        private readonly List<Directions> _allDirections = new List<Directions>(new[]
        {
            Directions.LeftBottom, Directions.LeftTop, 
            Directions.RightTop, Directions.RightBottom
        });

        public Id<IBusinessSector> BusinessSectorId
        {
            get { return _sectorType.GetBusinessSectorId(); }
        }

        private bool HasRoadChanged
        {
            set
            {
                if (HasRoadChanged != value)
                {
                    _hasRoadChanged = value;
                    if (HasRoadChanged)
                    {
                        GraphChanged.SafeRaise();
                    }
                }
            }
            get
            {
                return _hasRoadChanged;
            }
        }

        #region Injects

        [Inject]
        public IInjectionBinder jBinder { get; set; }

        [Inject]
        public IGameManager jGameManager { get; set; }

        [Inject]
        public IConstructionController jConstractController { get; set; }

        [Inject]
        public ICityPopulation jCityPopulation { get; set; }

        [Inject]
        public ITrafficManager jTrafficManager { get; set; }
        
        [Inject]
        public SignalLoadCityMapSequence jSignalLoadCityMapSequence { get; set; }

        [Inject]
        public SignalOnFinishLoadCityMapSequence jSignalOnFinishLoadCityMapSequence { get; set; }

        [Inject]
        public SignalOnConstructControllerChangeState jSignalOnConstructControllerChangeState { get; set; }

        [Inject]
        public MoveVehicleSignal jMoveVehicleSignal { get; set; }

        [Inject]
        public MoveCitizenSignal jMoveCitizenSignal { get; set; }

        [Inject]
        public SignalOnMovedVehicle jSignalOnMovedVehicle { get; set; }

        [Inject]
        public SignalOnMovedCitizen jSignalOnMovedCitizen { get; set; }

        #endregion

        #region Overrides of AViewMediator

        protected override void PreRegister()
        {
            _cacheMonobehaviour = new CacheMonobehavior(this);

            _roadsGraph = new RoadsGraph();
            _roadPathRandomFinder = new RandomPathFinder<RoadGraphNode, RoadGraphEdge>();
            _roadWavePathFinder = new WavePathFinder<RoadGraphNode, RoadGraphEdge>(this);

            _sidewalkGraph = new SidewalkGraph();
            _sidewalkRandomPathFinder = new RandomPathFinder<SidewalkVertex, SidewalkEdge>();
            _sidewalkWaveFinder = new WavePathFinder<SidewalkVertex, SidewalkEdge>(this);
            HasRoadChanged = true;
            _isTrafficGenerated = false;
        }

        protected override void OnRegister()
        {
            _trafficConfig = jTrafficManager.TrafficConfig;

            _locomotorPrefabs.ForEach(CreatePrefabPool);
            _citizenPrefabs.ForEach(CreatePrefabPool);

            jSignalLoadCityMapSequence.AddListener(OnLoadCityMapSequence);
            jSignalOnFinishLoadCityMapSequence.AddListener(OnFinishLoadCityMapSequence);
            jSignalOnConstructControllerChangeState.AddListener(OnConstractControllerChangeState);
            jMoveCitizenSignal.AddListener(OnNeedMoveCitizen);
            jMoveVehicleSignal.AddListener(OnNeedMoveVehicle);
        }

        protected override void OnRemove()
        {
            ClearTraffic();
            
            if (_mapView != null && _mapView.RoadGrid != null)
            {
                _mapView.RoadGrid.OnRoadViewAddedSignal.RemoveListener(OnRoadViewAdded);
                _mapView.RoadGrid.OnRoadViewRemovedSignal.RemoveListener(OnRoadViewRemoved);
            }

            jSignalLoadCityMapSequence.RemoveListener(OnLoadCityMapSequence);
            jSignalOnFinishLoadCityMapSequence.RemoveListener(OnFinishLoadCityMapSequence);
            jSignalOnConstructControllerChangeState.RemoveListener(OnConstractControllerChangeState);
            jMoveCitizenSignal.RemoveListener(OnNeedMoveCitizen);
            jMoveVehicleSignal.RemoveListener(OnNeedMoveVehicle);
        }

        #endregion

        private void OnLoadCityMapSequence(LoadCityMapCommandData data)
        {
            _mapView = jGameManager.GetMapView(BusinessSectorId);

            transform.position = _mapView.transform.position;
        }

        private void OnFinishLoadCityMapSequence(LoadCityMapCommandData data)
        {
            _mapView.RoadGrid.OnRoadViewAddedSignal.AddListener(OnRoadViewAdded);
            _mapView.RoadGrid.OnRoadViewRemovedSignal.AddListener(OnRoadViewRemoved);

            BuildGraphs();
            GenerateTraffic();
        }

        private void OnConstractControllerChangeState(bool state)
        {
            if (state)
            {
                ClearTraffic();
            }
            else
            {
                UpdateGraphs();
                GenerateTraffic();
            }
        }

        private List<KeyValuePair<T, float>> GetPathData<T>(List<PathPoint> points)
            where T : VertexBase
        {
            List<KeyValuePair<T, float>> result = new List<KeyValuePair<T, float>>();
            foreach (var point in points)
            {
                T verticle = null;
                if (typeof(T) == typeof(RoadGraphNode))
                {
                    verticle = _roadsGraph.GetVerticesByMapPosition(point.Position, point.Direction) as T;
                }
                else if (typeof(T) == typeof(SidewalkVertex))
                {
                    verticle = _sidewalkGraph.GetVerticesByMapPosition(point.Position, point.Direction) as T;
                }
                if (verticle == null)
                {
                    Debug.LogError("Cannot find verticle in " + point.Position 
                        + ", dir " + point.Direction);
                    return null;
                }
                result.Add(new KeyValuePair<T, float>(verticle, point.Delay));
            }
            return result;
        }

        private void OnNeedMoveVehicle(LocomotiveMoveParametrs locomotorParametrs)
        {
            if (locomotorParametrs.SectorType == _sectorType)
            {
                bool isSucces = false;
                int returnId = 0;
                var path = GetPathData<RoadGraphNode>(locomotorParametrs.Path);
                if (path != null)
                {
                    Locomotor locomotor = null;
                    var isNewCreated = locomotorParametrs.CreateNewInFirstPathPoint;
                    if (!isNewCreated)
                    {
                        locomotor = GetMostCloseLocomotor(path[0].Key.WorldPosition, true, locomotorParametrs.Type);
                    }
                    if(locomotor == null)
                    {
                        isNewCreated = true;
                        locomotor = CreateRandomLocomotor(locomotorParametrs.Type);
                    }
                    locomotor.MoveByPath(path, isNewCreated);
                    returnId = locomotor.Id;
                    isSucces = true;
                }
                locomotorParametrs.Callback.SafeRaise(isSucces, returnId);
            }
        }

        private void OnNeedMoveCitizen(CitizenMoveParametrs citizenParameters)
        {
            if (citizenParameters.SectorType == _sectorType)
            {
                bool isSucces = false;
                int returnId = 0;

                UnityEngine.Profiling.Profiler.BeginSample("Get path data in trafficController");
                var path = GetPathData<SidewalkVertex>(citizenParameters.Path);
                UnityEngine.Profiling.Profiler.EndSample();
                if (path != null)
                {
                    CitizenView citizen = null;
                    var isNewCreated = citizenParameters.CreateNewInFirstPathPoint;
                    if (!isNewCreated)
                    {
                        citizen = GetMostCloseCitizen(path[0].Key.WorldPosition);
                    }
                    if (citizen == null)
                    {
                        isNewCreated = true;
                        citizen = GetRandomCitizenView();
                    }
                    citizen.MoveByPath(path, isNewCreated);
                    returnId = citizen.Id;
                    isSucces = true;
                }
                citizenParameters.Callback.SafeRaise(isSucces, returnId);
            }
        }

        private void OnRoadViewAdded(RoadMapObjectView roadView)
        {
            _roadsGraph.AddRoadView(roadView);
            _sidewalkGraph.AddRoadView(roadView);

            HasRoadChanged = true;
        }

        private void OnRoadViewRemoved(RoadMapObjectView roadView)
        {
            _roadsGraph.RemoveRoadView(roadView);
            _sidewalkGraph.RemoveRoadView(roadView);

            HasRoadChanged = true;
        }

        private void BuildGraphs()
        {
            ClearTraffic();

            _roadsGraph.Build(_mapView.RoadGrid, _trafficConfig);
            _sidewalkGraph.Build(_mapView.RoadGrid, _trafficConfig);

            UpdateTrafficLights();

            HasRoadChanged = false;
        }

        private void UpdateGraphs()
        {
            if (HasRoadChanged)
            {
                _roadsGraph.Refresh();
                _sidewalkGraph.Refresh();
                
                UpdateTrafficLights();

                HasRoadChanged = false;
            }
        }

        private void GenerateTraffic()
        {
            if (!_isTrafficGenerated)
            {
                GenerateLocomotors();
                GenerateCitizens();

                _isTrafficGenerated = true;
            }
        }

        private void ClearTraffic()
        {
            if (_isTrafficGenerated)
            {
                FreeAllCurrentLocomotors();
                FreeAllCurrentCitizens();

                _isTrafficGenerated = false;
            }
        }

        #region Pool manipulations

        private void FreePool<T>(List<T> pool)
            where T : IPullableObject
        {
            int count = pool.Count;
            for (int i = 0; i < count; i++)
            {
                pool[i].FreeObject();
            }
            pool.Clear();
        }

        private void CreatePrefabPool<T>(T prefab)
            where T : IPullableObject
        {
            var pull = jBinder.GetInstance<IObjectPullWithPeek>();
            pull.InstanitePull(prefab);
            if (prefab is Locomotor)
            {
                _locomotorPools.Add(pull);
            }
            else if (prefab is CitizenView)
            {
                _citizenPools.Add(pull);
            }
            else
            {
                Debug.LogError("TrafficController.IncreasePool() not correct type");
            }
        }
        #endregion

        #region Locomotors

        private void FreeAllCurrentLocomotors()
        {
            FreePool(_currentLocomotors);
            if (_locomotorSpawnRutine != null)
            {
                StopCoroutine(_locomotorSpawnRutine);
                _locomotorSpawnRutine = null;
            }
        }

        private void GenerateLocomotors()
        {
            if (!_roadsGraph.HasEnoughReachableVertices)
            {
                return;
            }
            var locomotorCount = GetNewLocomotorsOnRoadCount();
            for (int i = 0; i < locomotorCount; i++)
            {
                var locomotor = CreateRandomLocomotor();
                locomotor.SetOnRandomPoint();
            }
            _locomotorSpawnRutine = StartCoroutine(StartLocomotorSpawnRoutine());
        }

        private int GetNewLocomotorsOnRoadCount()
        {
            var maxLocomotorsOnRoad = EvaluateMaxLocomotorsOnRoad();
            int newLocomotorsOnRoad = Mathf.RoundToInt(Random.Range(1, maxLocomotorsOnRoad));
            return newLocomotorsOnRoad;
        }

        private float EvaluateMaxLocomotorsOnRoad()
        {
            var roadsCount = jGameManager.GetMap(BusinessSectorId).FindAllMapObjects(MapObjectintTypes.Road).Count;
            var maxCarsPerRoad = roadsCount * _locomotorsPerRoadTile;
            var maxCarsPerPeople = jCityPopulation.GetTotalCityPopulation() * _locomotorsPerPeople;
            var maxLocomotorsOnRoad = Mathf.Min(maxCarsPerPeople, maxCarsPerRoad);
            return maxLocomotorsOnRoad;
        }

        /// <summary>
        /// Clear locomotor from scene
        /// </summary>
        /// <param name="sender">Sender.</param>
        private void OnLocomotorExitFormSector(Locomotor sender)
        {
            _currentLocomotors.Remove(sender);
            (sender as IPullableObject).FreeObject();
        }

        private void OnLocomotoMoveByPath(int sender, MovedByPathEventsType actionType)
        {
            jSignalOnMovedVehicle.Dispatch(_sectorType, sender, actionType);
        }

        private void OnCitizenMoveByPath(int sender, MovedByPathEventsType actionType)
        {
            jSignalOnMovedCitizen.Dispatch(_sectorType, sender, actionType);
        }

        private IEnumerator StartLocomotorSpawnRoutine()
        {
            while (true)
            {
                yield return new WaitForSeconds(Random.Range(_locomotorSpawnIntervalRange.x, _locomotorSpawnIntervalRange.y));
                SpawnLocomotor();
            }
        }

        private void SpawnLocomotor()
        {
            // если в городе нет дорог с выездом из города или включен режим постройки, машинки не спавним
            if (!_roadsGraph.HasEnoughReachableVertices || jConstractController.IsBuildModeEnabled)
            {
                return;
            }

            if (EvaluateMaxLocomotorsOnRoad() > _currentLocomotors.Count)
            {
                var locomotor = CreateRandomLocomotor(LocomotorType.Car);
                locomotor.SetOnRoadStart(BusinessSectorId);
            }
        }

        private Locomotor CreateRandomLocomotor(params LocomotorType[] types)
        {
            List<IObjectPullWithPeek> targetPool = null;
            if (types.Length > 0)
            {
                var targetTypes = new List<LocomotorType>(types);
                targetPool = _locomotorPools.FindAll
                    (item => targetTypes.Contains((item.Peek() as Locomotor).Type));
            }
            else
            {
                targetPool = _locomotorPools;
            }
            var locomotor = (Locomotor)targetPool[Random.Range(0, targetPool.Count)].GetInstance();
            locomotor.Init(_trafficConfig, _roadsGraph, _roadPathRandomFinder,
                _roadWavePathFinder, UnitCreatedCount);
            UnitCreatedCount++;
            locomotor.BusinessSectorId = BusinessSectorId;
            locomotor.transform.SetParent(_cacheMonobehaviour.CachedTransform);
            locomotor.CacheMonobehavior.CachedTransform.localScale = Vector3.one * _trafficConfig.LocomotorScaleFactor;
            locomotor.SignalOnExitFromCity.AddListener(OnLocomotorExitFormSector);
            locomotor.SignalForPathMoving.AddListener(OnLocomotoMoveByPath);
            _currentLocomotors.Add(locomotor);

            return locomotor;
        }

        private Locomotor GetMostCloseLocomotor(Vector3 position, bool notMovingInPath = true, params LocomotorType[] types)
        {
            var targetTypes = new List<LocomotorType>(types);
            var targetLocomotorList = _currentLocomotors.FindAll(item => 
                targetTypes.Count == 0 || targetTypes.Contains(item.Type));
            return GetMostCloseUnit(targetLocomotorList, position, notMovingInPath) as Locomotor;
        }

        #endregion

        #region Citizens

        private void FreeAllCurrentCitizens()
        {
            FreePool(_currentCitizens);
        }

        private void GenerateCitizens()
        {
            if (!_sidewalkGraph.HasEnoughReachableVertices)
                return;

            UnityEngine.Profiling.Profiler.BeginSample("Generate citizens");

            var count = EvaluateMaxCitizensWalking();

            for (int i = 0; i < count; i++)
            {
                var citizenView = GetRandomCitizenView();
                citizenView.SetRandomInCity();
            }

            UnityEngine.Profiling.Profiler.EndSample();
        }

        private CitizenView GetRandomCitizenView()
        {
            var citizenView = (CitizenView)_citizenPools[Random.Range(0, _citizenPools.Count)].GetInstance();

            citizenView.CacheMonobehavior.CachedTransform.SetParent(transform);
            citizenView.CacheMonobehavior.CachedTransform.localScale = Vector3.one * _trafficConfig.CitizenScaleFactor;
            citizenView.Init(_trafficConfig, _sidewalkGraph, _sidewalkRandomPathFinder,
                _sidewalkWaveFinder, UnitCreatedCount);
            citizenView.SignalForPathMoving.AddListener(OnCitizenMoveByPath);
            UnitCreatedCount++;
            _currentCitizens.Add(citizenView);

            return citizenView;
        }

        private float EvaluateMaxCitizensWalking()
        {
            var roadsCount = jGameManager.GetMap(BusinessSectorId).FindAllMapObjects(MapObjectintTypes.Road).Count;
            var maxCarsPerRoad = roadsCount * _citizensWalkingPerRoadTile;
            var maxCarsPerPeople = jCityPopulation.GetTotalCityPopulation() * _citizensWalkingPercent;
            var maxCitizensWalking = Mathf.Min(maxCarsPerPeople, maxCarsPerRoad);
            return maxCitizensWalking;
        }

        private CitizenView GetMostCloseCitizen(Vector3 position, bool notMovingInPath = true, params LocomotorType[] types)
        {
            return GetMostCloseUnit(_currentCitizens, position, notMovingInPath) as CitizenView;
        }

        #endregion

        #region Traffic Lights

        private void UpdateTrafficLights()
        {
            UnityEngine.Profiling.Profiler.BeginSample("Update Traffic Lights");

            ClearTrafficLights();

            var crossroads = _mapView.RoadGrid.Crossroads;

            if (crossroads == null)
            {
                return;
            }

            // adding traffic lights to crossroads
            foreach (var roadView in crossroads)
            {
                var adjacentCrossroadsCount = roadView.AdjacentCrossroadsCount;
                // avoiding crossroads surrounded only by other crossroads
                if (adjacentCrossroadsCount == 4 || (adjacentCrossroadsCount == 3 && roadView.RoadTileType != RoadTileType.Intersection))
                {
                    continue;
                }

                var trafficLights = new TrafficLights(roadView);

                var roadVertices = _roadsGraph.GetVerticesByMapPosition(roadView.MapObjectModel.MapPosition);
                var sidewalkVertices = _sidewalkGraph.GetVerticesByMapPosition(roadView.MapObjectModel.MapPosition);

                var allDirections = _allDirections;
                var allDirectionsCount = allDirections.Count;
                for (int i = 0; i < allDirectionsCount; i++)
                {
                    var direction = allDirections[i];
                    if (roadView.Adjacent.ContainsKey(direction) && !roadView.Adjacent[direction].RoadTileType.IsCrossroads())
                    {
                        trafficLights.AddRoadNode(roadVertices[direction]);
                        trafficLights.AddSidewalkVertex(sidewalkVertices[direction]);
                    }
                }

                _trafficLights.Add(trafficLights);
                trafficLights.Start(_trafficConfig);
            }

            UnityEngine.Profiling.Profiler.EndSample();
        }

        public void ClearTrafficLights()
        {
            var count = _trafficLights.Count;
            for( int i = 0; i < count; i++)
            {
                _trafficLights[i].Stop();
            }
            _trafficLights.Clear();
        }

        #endregion

        private IGraphMove GetMostCloseUnit<TUnit>(
            List<TUnit> targetList, Vector3 position, bool notMovingInPath = true)
            where TUnit : IGraphMove
        {
            TUnit result = default(TUnit);
            var mostCloseDistance = float.MaxValue;
            var count = targetList.Count;
            for( int i = 0; i < count; i++)
            {
                var item = targetList[i];
                if(!notMovingInPath || !item.IsMovingByPath)
                {
                    var distance = Vector2.Distance(item.CurrentPosition, position);
                    if (distance < mostCloseDistance)
                    {
                        mostCloseDistance = distance;
                        result = item;
                    }
                }
            }
            return result;
        }

        private void OnDrawGizmosSelected()
        {
            if (!Application.isPlaying)
            {
                return;
            }

            var nodeOffset = transform.position;
            var edgeOffset = new Vector3(0, 0.2f, 0f) + nodeOffset;

            // roads

            foreach (var node in _roadsGraph.Vertices)
            {
                Gizmos.color = new Color(1f, 0.1f, 0.1f, 0.6f);
                Gizmos.DrawWireSphere(node.WorldPosition + edgeOffset, 0.05f);
            }

            foreach (var edge in _roadsGraph.Edges) // рисуем ребра
            {
                Gizmos.color = new Color(0f, 1f, 0.1f, 0.6f);
                var start = edge.Start.WorldPosition + edgeOffset;
                var end = edge.End.WorldPosition + edgeOffset;
                Gizmos.DrawLine(start, end);
                Gizmos.DrawCube(end, new Vector3(0.025f, 0.025f, 0.025f));
            }

            Gizmos.color = new Color(1f, 1f, 0f, 0.6f);
            Gizmos.DrawWireSphere(_roadsGraph.CityExitVertex.WorldPosition + edgeOffset, 0.1f);

            Gizmos.color = new Color(0f, 0.5f, 1f, 0.6f);
            Gizmos.DrawWireSphere(_roadsGraph.CityEnterVertex.WorldPosition + edgeOffset, 0.1f);

            // sidewalk

            foreach (var vertex in _sidewalkGraph.Vertices)
            {
                Gizmos.color = new Color(0f, 0.9f, 0f, 0.9f);
                Gizmos.DrawWireSphere(vertex.WorldPosition + nodeOffset, 0.05f);
            }

            foreach (var edge in _sidewalkGraph.Edges)
            {
                Gizmos.color = new Color(1f, 0.1f, 0.1f, 0.9f);
                var start = edge.Start.WorldPosition + nodeOffset;
                var end = edge.End.WorldPosition + nodeOffset;
                Gizmos.DrawLine(start, end);
                Gizmos.DrawCube(end, new Vector3(0.025f, 0.025f, 0.025f));
            }

            Gizmos.color = new Color(1f, 1f, 0f, 0.6f);
            Gizmos.DrawWireSphere(_sidewalkGraph.CityExitVertex.WorldPosition + nodeOffset, 0.1f);

            Gizmos.color = new Color(0f, 0.5f, 1f, 0.6f);
            Gizmos.DrawWireSphere(_sidewalkGraph.CityEnterVertex.WorldPosition + nodeOffset, 0.1f);
        }
    }
}