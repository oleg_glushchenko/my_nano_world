﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace NanoReality.GameLogic.TrafficSystem.Domain
{
    public enum DirectionVectorType
    {
        Zero,
        Left,
        Up,
        Right,
        Down,
        LeftToUp,
        LeftToDown,
        UpToLeft,
        UpToRight,
        RightToUp,
        RightToDown,
        DownToLeft,
        DownToRight
    }

    public static class DirectionVectorTypeExtentions
    {
        private const int SecondDirecationCoeff = 2;

        private static readonly Dictionary<DirectionVectorType, Vector3> _typeToVector;
        private static readonly Dictionary<Vector3, DirectionVectorType> _vectorToType;

        static DirectionVectorTypeExtentions()
        {
            _typeToVector = new Dictionary<DirectionVectorType, Vector3>();
            _typeToVector.Add(DirectionVectorType.Zero, Vector3.zero);
            _typeToVector.Add(DirectionVectorType.Left, Vector3.left);
            _typeToVector.Add(DirectionVectorType.Up, Vector3.forward);
            _typeToVector.Add(DirectionVectorType.Right, Vector3.right);
            _typeToVector.Add(DirectionVectorType.Down, Vector3.back);
            _typeToVector.Add(DirectionVectorType.LeftToUp,
                GetCombo(DirectionVectorType.Left, DirectionVectorType.Up));
            _typeToVector.Add(DirectionVectorType.LeftToDown, 
                GetCombo(DirectionVectorType.Left, DirectionVectorType.Down));
            _typeToVector.Add(DirectionVectorType.UpToLeft, 
                GetCombo(DirectionVectorType.Up, DirectionVectorType.Left));
            _typeToVector.Add(DirectionVectorType.UpToRight, 
                GetCombo(DirectionVectorType.Up, DirectionVectorType.Right));
            _typeToVector.Add(DirectionVectorType.RightToUp, 
                GetCombo(DirectionVectorType.Right, DirectionVectorType.Up));
            _typeToVector.Add(DirectionVectorType.RightToDown, 
                GetCombo(DirectionVectorType.Right, DirectionVectorType.Down));
            _typeToVector.Add(DirectionVectorType.DownToLeft, 
                GetCombo(DirectionVectorType.Down, DirectionVectorType.Left));
            _typeToVector.Add(DirectionVectorType.DownToRight, 
                GetCombo(DirectionVectorType.Down, DirectionVectorType.Right));

            _vectorToType = new Dictionary<Vector3, DirectionVectorType>();
            foreach (var key in _typeToVector.Keys)
            {
                _vectorToType.Add(_typeToVector[key], key);
            }
        }

        private static Vector3 GetCombo(DirectionVectorType firstDir, DirectionVectorType secondDir)
        {
            return _typeToVector[firstDir] + _typeToVector[secondDir] * SecondDirecationCoeff;
        }

        public static Vector3 GetVector3(this DirectionVectorType target)
        {
            return _typeToVector[target];
        }

        public static DirectionVectorType AddDirection(this DirectionVectorType first, 
            DirectionVectorType second)
        {
            var targetVector = first.GetVector3() + second.GetVector3() * SecondDirecationCoeff;
            var intVector = new Vector3(Mathf.RoundToInt(targetVector.x), 0, Mathf.RoundToInt(targetVector.z));
            if (_vectorToType.ContainsKey(intVector))
            {
                return _vectorToType[intVector];
            }
            Debug.LogWarning("Cannot correct get direction type for " + targetVector);
            return DirectionVectorType.Zero;
        }

        //does not include what direction was previously, so in this 
        //method RightToUp == UpToRight
        public static DirectionVectorType GetDirectionType(Vector3 target)
        {
            var dirX = GetValueDirectionInt(target.x);
            var dirZ = GetValueDirectionInt(target.z);
            DirectionVectorType result = DirectionVectorType.Zero;
            if (dirX == 0)
            {
                result = dirZ == 0 ? DirectionVectorType.Zero :
                    dirZ > 0 ? DirectionVectorType.Up :
                    DirectionVectorType.Down;
            }
            if (dirX > 0)
            {
                result = dirZ == 0 ? DirectionVectorType.Right :
                    dirZ > 0 ? DirectionVectorType.RightToUp :
                    DirectionVectorType.RightToDown;
            }
            if (dirX < 0)
            {
                result = dirZ == 0 ? DirectionVectorType.Left :
                    dirZ > 0 ? DirectionVectorType.LeftToUp :
                    DirectionVectorType.LeftToDown;
            }
            return result;
        }

        public static bool IsNotZero(this DirectionVectorType target)
        {
            return target != DirectionVectorType.Zero;
        }

        private static int GetValueDirectionInt(float value)
        {
            const float MinValueForDetectingDirection = 0.01f;
            if (Mathf.Abs(value) < MinValueForDetectingDirection)
            {
                return 0;
            }
            return value > 0 ? 1 : -1;
        }
    }
}