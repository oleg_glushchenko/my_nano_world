using UniRx;

namespace NanoReality.GameLogic.Common
{
    public class GameLoadingModel
    {
        public ReactiveProperty<GameLoadingState> LoadingState = new ReactiveProperty<GameLoadingState>(GameLoadingState.Startup);
    }
}