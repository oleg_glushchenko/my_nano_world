﻿using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
 using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
 using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
 using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
 using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Impl;
using strange.extensions.signal.impl;
using UnityEngine;


namespace NanoReality.GameLogic.Common.Controllers.Signals
{
    /// <summary>
    /// Вызывается когда у игрока изменяется список секторов
    /// </summary>
    public class SignalOnUserBusinessSectorsUpdate : Signal<IEnumerable<BusinessSectorsTypes>>{ }
    public class SignalOnSectorUpdate : Signal { }

    public class SignalOnSoftCurrencyStateChanged : Signal<int> { }

    public class SignalOnLanternCurrencyStateChanged : Signal<int> { }

    public class SignalOnUserInvited : Signal { }
    /// <summary>
    /// Сигнал получения нано гемов (от сервера)
    /// </summary>
    public class SignalOnNanoGemsDropped : Signal<int> { }

    public class SignalHudIsInitialized : Signal { }

    public class SignalWarehouseProductsAmountChanged : Signal { } 

    public class SignalOnHardCurrencyStateChanged : Signal<int> { }

    public class SignalOnGoldCurrencyStateChanged : Signal<int> { }
    public class SignalOnCurrencyStateChanged : Signal<int, PriceType> { }

    /// <summary>
    /// Dispatches when user consume some hard coins (param - hard currency delta)
    /// </summary>
    public class SignalOnHardCurrencyConsumed : Signal<int> { }

    public class SignalOnNanoGemsStateChanged : Signal<int> { }
    public class SignalAnalyticBuildStared : Signal<IMapObject, PriceType> { }

    public class SignalOnProductsStateChanged : Signal<Dictionary<Id<Product>, int>> { }

    public class SignalOnPopulationSatisfactionStateChanged : Signal { }

    public class SignalOnBuildingUnlocked : Signal<Id_IMapObjectType> { }

    public class SignalOnBuildingShipAward : Signal<Id_IMapObjectType, int> { }

    public class SignalOnSubSectorUnlocked : Signal<CityLockedSector> { }
    
    public class SignalOnSubSectorsUpdated : Signal { }
    
    public class SignalOnSubSectorUnlockedVerified : Signal<ILockedSector> { }

    public class SignalOnFireworksStart : Signal { }

    public class SignalOnProductTexturesLoadEnd : Signal { }

    public class SignalOnBuildingRepaired : Signal<Id_IMapObjectType, Id_IMapObject> { }

    public class StartSpentProductsAnimationSignal : Signal<Vector3, Dictionary<Id<Product>, int>> { }

    public class SignalOffersUpdated : Signal { }

    public class DisableOffersButtonSignal : Signal { }
    public class SignalLoadingSpinner : Signal<bool> { }
}

