﻿using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using NanoReality.Engine.UI.Extensions.DailyBonusPanel;
using NanoReality.StrangeIoC;

namespace NanoReality.GameLogic.PostLoadingSequence
{
    public class PostLoadingActionDaily : AGameCommand
    {
        [Inject] public DailyBonusContext jDailyBonusContext { get; set; }
        [Inject] public ShowWindowSignal jShowWindowSignal { get; set; }
        [Inject] public SignalOnShownPanel jSignalOnShownPanel { get; set; }
        [Inject] public SignalOnHidePanel jSignalOnHidePanel { get; set; }

        public override void Execute()
        {
            base.Execute();

            //TODO: turn Daily on when you need it
            return;

            if (!jDailyBonusContext.ShouldPanelBeShown) return;
            Retain();
            jSignalOnShownPanel.AddListener(OnPanelShown);
            jShowWindowSignal.Dispatch(typeof(DailyBonusView), null);

            void OnPanelShown(UIPanelView panelView)
            {
                jSignalOnShownPanel.RemoveListener(OnPanelShown);
                if (!(panelView is DailyBonusView)) return;
                jSignalOnHidePanel.AddListener(OnRelease);
                
                void OnRelease(UIPanelView subPanelView)
                {
                    if (!(subPanelView is DailyBonusView)) return;
                    jSignalOnHidePanel.RemoveListener(OnRelease);
                    Release();
                }
            }
        }
    }
}