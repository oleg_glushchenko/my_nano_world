using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using NanoReality.GameLogic.Quests;
using NanoReality.StrangeIoC;

namespace NanoReality.GameLogic.PostLoadingSequence
{
    public class PostLoadingActionQuests : AGameCommand
    {
        [Inject] public IUserQuestData jUserQuestData { get; set; }
        [Inject] public SignalOnHidePanel jSignalOnHidePanel { get; set; }
        [Inject] public SignalOnShownPanel jSignalOnShownPanel { get; set; }

        public override void Execute()
        {
            base.Execute();

            jUserQuestData.OnQuestsInitialized += OnQuestsInitializedHandler;
            jSignalOnHidePanel.AddListener(OnFxPopupHide);
            jSignalOnShownPanel.AddListener(OnFxPopupShow);
            jUserQuestData.InitUserQuest();
            Retain();
            
            void OnFxPopupShow(UIPanelView panelView)
            {
                jUserQuestData.OnQuestsInitialized -= OnQuestsInitializedHandler;
                if (panelView is FxPopUpPanelView fxPopup && fxPopup.FxType != FxPanelType.Quest) return;
                jSignalOnShownPanel.RemoveListener(OnFxPopupShow);
            }

            void OnFxPopupHide(UIPanelView panelView)
            {
                if (panelView is FxPopUpPanelView)
                {
                    jSignalOnHidePanel.RemoveListener(OnFxPopupHide);
                    Release();
                }
            }

            void OnQuestsInitializedHandler()
            {
                jUserQuestData.OnQuestsInitialized -= OnQuestsInitializedHandler;
                Release();
            }
        }
    }
}