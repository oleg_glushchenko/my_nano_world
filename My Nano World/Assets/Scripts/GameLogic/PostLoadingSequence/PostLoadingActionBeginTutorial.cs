﻿using NanoReality.Game.Data;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.StrangeIoC;

namespace GameLogic.PostLoadingSequence
{
    public sealed class PostLoadingActionBeginTutorial : AGameCommand
    {
        [Inject] public IHardTutorial jHardTutorial { get; set; }
        [Inject] public IHintTutorial jHintTutorial { get; set; }
        [Inject] public IBuildSettings jBuildSettings { get; set; }

        public override void Execute()
        {
            base.Execute();

            StartHardTutorial();
            StartHintTutorial();
        }

        private void StartHardTutorial()
        {
            if (jBuildSettings.DisableHardTutorial)
            {
                jHardTutorial.SkipTutorial();
                return;
            }
          
            jHardTutorial.StartTutorial();
        }
        
        private void StartHintTutorial()
        {
            if (jBuildSettings.DisableSoftTutorials)
            {
                jHintTutorial.SkipTutorial();
                return;
            }
            
            jHintTutorial.StartTutorial();
        }
    }
}