using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UI.Core.Views;
using NanoReality.GameLogic.Achievements;
using NanoReality.StrangeIoC;

namespace NanoReality.GameLogic.PostLoadingSequence
{
    public class PostLoadingActionAchievement : AGameCommand
    {
        [Inject] public IUserAchievementsData jUserAchievementsData { get; set; }
        [Inject] public SignalOnHidePanel jSignalOnHidePanel { get; set; }
        [Inject] public SignalOnShownPanel jSignalOnShownPanel { get; set; }

        public override void Execute()
        {
            base.Execute();

            jUserAchievementsData.OnAchievementsInitialize += OnAchievementsInitializeHandler;
            jSignalOnHidePanel.AddListener(OnFxPopupHide);
            jSignalOnShownPanel.AddListener(OnFxPopupShow);
            jUserAchievementsData.Init();
            Retain();
        }

        public override void Release()
        {
            base.Release();
            
            jSignalOnHidePanel.RemoveListener(OnFxPopupHide);
            jSignalOnShownPanel.RemoveListener(OnFxPopupShow);
        }

        private void OnFxPopupShow(UIPanelView panelView)
        {
            jUserAchievementsData.OnAchievementsInitialize -= OnAchievementsInitializeHandler;
            if (panelView is FxPopUpPanelView fxPopup && fxPopup.FxType != FxPanelType.Achievement)
                return;
            jSignalOnShownPanel.RemoveListener(OnFxPopupShow);
        }

        private void OnFxPopupHide(UIPanelView panelView)
        {
            if (panelView is FxPopUpPanelView)
            {
                jSignalOnHidePanel.RemoveListener(OnFxPopupHide);
                Release();
            }
        }

        private void OnAchievementsInitializeHandler()
        {
            jUserAchievementsData.OnAchievementsInitialize -= OnAchievementsInitializeHandler;
            Release();
        }
    }
}