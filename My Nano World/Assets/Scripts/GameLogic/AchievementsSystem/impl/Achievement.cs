﻿using System.Collections.Generic;
using Assets.Scripts.GameLogic.Utilites;
using I2.Loc;
using NanoLib.Core;
using NanoReality.GameLogic.Quests;
using Newtonsoft.Json;
using UnityEngine;

namespace NanoReality.GameLogic.Achievements.impl
{
    public class Achievement 
    {
        [JsonProperty("id")]
        public IntValue AchievementID { get; set; }
        
        [JsonProperty("unlock_conditions")]
        public List<ICondition> AchiveConditions { get; set; }
        
        [JsonProperty("award_actions")]
        public List<IAwardAction> AwardActions { get; set; }
        
		public string Title => 
            LocalizationManager.GetTranslation(string.Format(LocalizationKeyConstants.Objects.ACHIEVEMENT, AchievementID));
		
		public string Description => 
            LocalizationManager.GetTranslation(string.Format(LocalizationKeyConstants.Objects.ACHIEVEMENT_DSCR, AchievementID));
        
        
        public void InitServerData()
        {
            if (AchiveConditions != null)
                foreach (var achiveCondition in AchiveConditions)
                    achiveCondition.InitServerBalanceData();

            if (AwardActions != null)
                foreach (var awardAction in AwardActions)
                    awardAction.InitServerData();
        }

        public Requirement GetConditionRequirement(int index)
        {
            ICondition condition = AchiveConditions[index];
            if (IsAchievedBuildingCondition(condition))
            {
                if (condition.IsAchived)
                {
                    return new Requirement(0, 1, 1);
                }
                else
                {
                    return new Requirement(0, 1, 0);
                }
            }

            return AchiveConditions[0].GetConditionRequirement();
        }
        
        private bool IsAchievedBuildingCondition(ICondition condition)
        {
            if (condition is UserLevelAchivedCondition)
            {
                var userLevelCond = (UserLevelAchivedCondition) condition;
                int targetUserLevel = userLevelCond.TargetUserLevel;
                
                if (targetUserLevel == MUserAchievementsData.BazaarUnlockLevel
                    || targetUserLevel == MUserAchievementsData.MetroUnlockLevel
                    || targetUserLevel == MUserAchievementsData.SeaportUnlockLevel 
                    || targetUserLevel == MUserAchievementsData.RestaurantUnlockLevel)
                {
                    return true;
                }
            }
            
            return false;
        }
    }
}