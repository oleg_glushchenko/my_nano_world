﻿using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.Analytics;
using Assets.Scripts.Engine.Analytics.Extensions.CurrencyInfo;
using NanoReality.GameLogic.Achievements.api;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using Newtonsoft.Json;
using System;
using NanoReality.GameLogic.Achievements.impl;
using System.Collections.Generic;
using NanoLib.Core;
using NanoLib.Core.Services.Sound;
using UnityEngine;

namespace NanoReality.GameLogic.Achievements
{
    public sealed class UserAchievement : IUserAchievement
    {
	    [JsonProperty("achieve_id")]
        public IntValue BalanceID { get; set; }
        
        [JsonProperty("status")]
        public int AwardTaken { get; set; }
        
        public bool IsEarned { get; set; }
        
        public int GroupKey { get; set; }
        
		public List<ICondition> CurrentConditions => GetBalanceAchievementObject().AchiveConditions;

		[Inject] public IAchievementsData jAchievementsData { get; set; }

		[Inject] public IServerCommunicator jServerCommunicator { get; set; }

		[Inject] public SignalOnActionWithCurrency jSignalOnActionWithCurrency { get; set; }
		[Inject] public SignalOnUserAchievementUnlocked jSignalOnUserAchievementUnlocked { get; set; }
		[Inject] public AnalyticsAchievementUnlock jAnalyticsAchievementUnlock { get; set; }
		[Inject] public SignalOnAwardTaken jSignalOnAwardTaken { get; set; }
		[Inject] public ISoundManager jSoundManager { get; set; }

        public Achievement GetBalanceAchievementObject()
        {
			return _currentBalanceAchievement;
        }
		private Achievement _currentBalanceAchievement;
        
        public void InitWithProgress(Achievement achievement)
        {
	        _currentBalanceAchievement = achievement;
	        
			if (CurrentConditions.Count > 1)
			{
				Debug.LogError("Achievement must contain only one condition");
				return;
			}
            
			foreach (var condition in _currentBalanceAchievement.AchiveConditions)
            {
                condition.InitServerBalanceData();
                condition.InitLocalStateData();

                if (!IsEarned)
                {
                    condition.SignalOnAchieved.AddListener(OnUnlockConditionStateChange);
                    condition.StartEvaluateCondition();
                }
                else
                {
                    condition.IsAchived = true;
                }
            }
        }

        public bool IsCompleted()
        {
	        return AwardTaken >= 1;
        }
        
        public void OnUnlockConditionStateChange(ICondition condition)
        {
            bool result = true;

            foreach (var unlockConditionsState in GetBalanceAchievementObject().AchiveConditions)
                result &= unlockConditionsState.IsAchived;

			if (result) 
			{
				jServerCommunicator.VerifyAchievementUnlocked (BalanceID, () => {});
				foreach (var unlockConditionsState in GetBalanceAchievementObject().AchiveConditions)
					unlockConditionsState.SignalOnAchieved.RemoveListener(OnUnlockConditionStateChange);

				IsEarned = true;

				jSignalOnUserAchievementUnlocked.Dispatch();
				jSoundManager.Play(SfxSoundTypes.AchievementEarned, SoundChannel.SoundFX);
				jAnalyticsAchievementUnlock.Dispatch(this);
			}
        }
        
		public void TakeAward(Action successCallback, Action failCallback)
        {
			if (IsCompleted() || !IsEarned) {
				failCallback?.Invoke();
				return;
			}
            jServerCommunicator.TakeAchievementAward(BalanceID, awards =>
            {
                
				// *************** НАГРАДУ НАЧИСЛЯЕМ ПОСЛЕ ОТВЕТА С СЕРВЕРА, т.е. там может быть дополнительные рандомные награды
                foreach (var a in awards)
                {
                    a.InitServerData();
                    a.Execute(AwardSourcePlace.AchievementReward, BalanceID.Value.ToString());

                    jSignalOnActionWithCurrency.Dispatch(CurrencyInfo.GetInfoFromAwardType(a,
                        CurrencyIncomesActions.Achievements, jAchievementsData.GetAchievementByID(BalanceID).Title));
                }
				// *****************		
                
            });
			//**********  Не ждем ответа от сервера, сразу же сигналим о сборе награды *************
			AwardTaken = 1;
			var awardsLocal = GetBalanceAchievementObject().AwardActions;
			jSignalOnAwardTaken.Dispatch(this, awardsLocal);
        }
    }
}
