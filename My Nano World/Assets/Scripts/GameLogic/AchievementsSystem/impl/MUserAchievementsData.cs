﻿using System;
using System.Collections.Generic;
using GameLogic.Quests.Models.api.Extensions;
using NanoLib.Core;
using NanoReality.Game.Services.BuildingsUnlockService;
using NanoReality.GameLogic.Achievements.api;
using NanoReality.GameLogic.Achievements.impl;
using NanoReality.GameLogic.Quests;
using Newtonsoft.Json;
using strange.extensions.injector.api;
using UnityEngine;

namespace NanoReality.GameLogic.Achievements
{
    public class MUserAchievementsData : IUserAchievementsData
    {
        public Dictionary<int, List<IUserAchievement>> UserAchievements => _userAchievements;
        
        public static int MetroUnlockLevel;
        public static int BazaarUnlockLevel;
        public static int SeaportUnlockLevel;
        public static int RestaurantUnlockLevel;
         
        
        [JsonProperty("completed")] 
        public List<IUserAchievement> EarnedAchievements { get; set; }
        
        public int EarnedAchievementsNumber
        {
            get
            {
                int number = 0;
                
                foreach (var entry in _userAchievements)
                {
                    foreach (var userAchievement in entry.Value)
                    {
                        if (userAchievement == null)
                            continue;
                    
                        if(!userAchievement.IsEarned)
                            continue;
                    
                        if (userAchievement.IsCompleted())
                            continue;
                    
                        number++;
                    }
                }

                return number;
            }
        }
        
        private readonly Dictionary<int, List<IUserAchievement>> _userAchievements = new Dictionary<int, List<IUserAchievement>>();
        
        public event Action OnAchievementsInitialize;
        
        [Inject] public IAchievementsData jAchievementsData { get; set; }
        [Inject] public IInjectionBinder jInjectionBinder { get; set; }
        [Inject] public SignalOnUserAchievmentsInit jSignalOnUserAchievmentsInit { get; set; }
        [Inject] public IBuildingsUnlockService jBuildingsesUnlockService { get; set; }

        public void Init()
        {
            foreach (Achievement achievement in jAchievementsData.Achievements)
            {
                IUserAchievement userAchievement = EarnedAchievements.Find(earned => earned.BalanceID == achievement.AchievementID);
                if (userAchievement == null)
                {
                    userAchievement = jInjectionBinder.GetInstance<IUserAchievement>();
                    userAchievement.BalanceID = achievement.AchievementID;
                    userAchievement.AwardTaken = 0;
                    userAchievement.IsEarned = false;
                    userAchievement.InitWithProgress(achievement);
                } else
                {
                    userAchievement.IsEarned = true;
                    userAchievement.InitWithProgress(achievement);
                }

                int groupKey = GetGroupKey(userAchievement.GetBalanceAchievementObject().AchiveConditions[0], userAchievement.BalanceID);
                userAchievement.GroupKey = groupKey;

                if (_userAchievements.ContainsKey(groupKey))
                {
                    _userAchievements[groupKey].Add(userAchievement);
                } else
                {
                    _userAchievements.Add(groupKey, new List<IUserAchievement> {userAchievement});
                }
            }
            
            jSignalOnUserAchievmentsInit.Dispatch();
            OnAchievementsInitialize?.Invoke();
        }

        private int GetGroupKey(ICondition condition, IntValue balanceId)
        {
            if (condition.ConditionType == ConditionTypes.ProductProduced)
            {
                var castCondition = (IProductsProducedCondition) condition;
                return ((int) condition.ConditionType * 1000) + castCondition.ProductType;
            }

            if (condition.ConditionType == ConditionTypes.UserLevelAchived)
            {
                var castCondition = (IUserLevelAchievedCondition) condition;
                
                const int metroBuildingId = 135;
                const int bazaarBuildingId = 156;
                const int seaportBuildingId = 140;
                const int restaurantBalanceId = 57;
                
                MetroUnlockLevel = jBuildingsesUnlockService.GetBuildingUnlockLevel(metroBuildingId);
                BazaarUnlockLevel = jBuildingsesUnlockService.GetBuildingUnlockLevel(bazaarBuildingId);
                SeaportUnlockLevel = jBuildingsesUnlockService.GetBuildingUnlockLevel(seaportBuildingId);
                RestaurantUnlockLevel = jBuildingsesUnlockService.GetBuildingUnlockLevel(restaurantBalanceId);
                

                if (castCondition.TargetUserLevel == MetroUnlockLevel
                 || castCondition.TargetUserLevel == BazaarUnlockLevel
                 || castCondition.TargetUserLevel == SeaportUnlockLevel
                 || castCondition.TargetUserLevel == RestaurantUnlockLevel)
                {
                    return ((int) condition.ConditionType * 1000) + balanceId;
                }
            }

            if (condition.ConditionType == ConditionTypes.CoverAmountBuildingsByService)
            {
                var castCondition = (ICoverAmountBuildingsByServiceCondition) condition;

                return ((int) condition.ConditionType * 1000) + (int) castCondition.ServiceType;
            }

            if (condition.ConditionType == ConditionTypes.ReachPopulation)
            {
                return (balanceId / 10) * 10000;
            }

            return (int) condition.ConditionType;
        }
    }
}
