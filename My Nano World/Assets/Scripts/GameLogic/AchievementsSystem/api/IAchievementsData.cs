﻿using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.Achievements.impl;
using NanoReality.GameLogic.StartApplicationSequence.Models.api;

namespace NanoReality.GameLogic.Achievements.api
{
    /// <summary>
    /// Контейнер для всех балансовых ачивментов, синглтон
    /// </summary>
    public interface IAchievementsData : IGameBalanceData
    {
        /// <summary>
        /// Список балансовых ачивментов
        /// </summary>
        List<Achievement> Achievements { get; set; }

        /// <summary>
        /// Возвращает объект ачивмента по ID
        /// </summary>
        /// <param name="ID">ID необходимого ачивмента</param>
        /// <returns>Объект ачивмента</returns>
        Achievement GetAchievementByID(Id<Achievement> ID);
    }
}