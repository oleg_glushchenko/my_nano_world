﻿using System;
using System.Collections.Generic;

namespace NanoReality.GameLogic.Achievements
{
    public interface IUserAchievementsData
    {
        Dictionary<int, List<IUserAchievement>> UserAchievements { get; }
	    int EarnedAchievementsNumber { get; }

	    event Action OnAchievementsInitialize;
	    
        void Init();
    }
}