﻿using System;
using NanoReality.GameLogic.Achievements.impl;
using NanoReality.GameLogic.Quests;
using System.Collections.Generic;
using NanoLib.Core;

namespace NanoReality.GameLogic.Achievements
{
    public interface IUserAchievement
    {
        IntValue BalanceID { get; set;}
        
        bool IsEarned { get; set; }
        
        int AwardTaken { get; set;}
        
        int GroupKey { get; set; }
        
        List<ICondition> CurrentConditions { get; }
        
        Achievement GetBalanceAchievementObject();
        
        void InitWithProgress(Achievement achievement);

        bool IsCompleted();

        void TakeAward(Action succesCallback, Action failCallback);
    }
}