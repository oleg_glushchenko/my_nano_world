﻿using NanoReality.GameLogic.Achievements;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.StartApplicationSequence.Controllers.Signals;
using UniRx;

public class LoadUserAchievementsCommand
{
    #region Injects

    [Inject] public IServerCommunicator jServerCommunicator { get; set; }

    [Inject] public IUserAchievementsData jUserAchievementsData { get; set; }
    
    [Inject] public SignalOnUserAchievementsDataLoaded jUserAchievementsLoadedSignal { get; set; }

    #endregion

    public AsyncSubject<Unit> GetObservableExecute()
    {
        var subject = new AsyncSubject<Unit>();

        jServerCommunicator.LoadUserAchievementsData(
            data =>
        {
            jUserAchievementsLoadedSignal.Dispatch(data);
            
            subject.OnNext(Unit.Default);
            subject.OnCompleted();
        });

        return subject;
    }
}
