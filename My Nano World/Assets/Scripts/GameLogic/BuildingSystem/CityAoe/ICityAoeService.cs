﻿using System;
using NanoReality.Game.Services;

namespace GameLogic.BuildingSystem.CityAoe
{
    public interface ICityAoeService : IGameService
    {
        event Action<CityAoeData> OnCityAoeDataReceived;

        void LoadAoeData(Action onSuccess = null);
        void LoadAoeDataWithDelay(float delay);
    }
}