﻿using System;
using System.Collections.Generic;
using Assets.NanoLib.UI.Core.Signals;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl;
using GameLogic.Taxes.Service;
using NanoReality.Engine.UI.Extensions.EditModePanels;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.Happiness;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.ServiceBuildings;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using UniRx;

namespace GameLogic.BuildingSystem.CityAoe
{
    public class CityAoeService : ICityAoeService
    {
        private IDisposable _disposableLoadRequestDelay;
        
        public event Action<CityAoeData> OnCityAoeDataReceived;

        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public SignalOnBuildingElectricityChanged jSignalOnBuildingElectricityChanged { get; set; }
        [Inject] public SignalOnMountObjects jMountObjectsSignal { get; set; }
        [Inject] public SignalOnDeleteRoads jDeleteRoadSignal { get; set; }
        [Inject] public UserLevelUpSignal UserLevelUpSignal { get; set; }
        [Inject] public IHappinessData jHappinessData { get; set; }
        [Inject] public ICityTaxesService jCityTaxesService { get; set; }
        [Inject] public IEditModeService jEditModeService { get; set; }

        public void Init()
        {
            jMountObjectsSignal.AddListener(OnNewObjectsMounted);
            jDeleteRoadSignal.AddListener(OnRoadRemoved);
            UserLevelUpSignal.AddListener(OnLevelUp);
        }

        public void Dispose()
        {
            _disposableLoadRequestDelay?.Dispose();
            jMountObjectsSignal.RemoveListener(OnNewObjectsMounted);
            jDeleteRoadSignal.RemoveListener(OnRoadRemoved);
            UserLevelUpSignal.RemoveListener(OnLevelUp);
        }

        public void LoadAoeData(Action onSuccess = null)
        {
            if (jEditModeService.IsEditModeEnable)
            {
                onSuccess?.Invoke();
                return;
            }
            
            jServerCommunicator.LoadAoeDataNew((aoeData) =>
            {
                onSuccess?.Invoke();
                OnAoeDataLoadedCallback(aoeData);
            });
        }

        public void LoadAoeDataWithDelay(float delay)
        {
            _disposableLoadRequestDelay?.Dispose();
            _disposableLoadRequestDelay = Observable.Timer(TimeSpan.FromSeconds(delay)).Subscribe(OnLoadAoeDataAfterDelay);
        }

        private void OnLoadAoeDataAfterDelay(long time)
        {
            _disposableLoadRequestDelay?.Dispose();
            _disposableLoadRequestDelay = null;
            
            LoadAoeData();
        }

        private void OnAoeDataLoadedCallback(CityAoeData cityAoeData)
        {
            jHappinessData.UpdateData(cityAoeData.Happiness);

            if (cityAoeData.AoeData != null)
            {
                foreach (KeyValuePair<int,List<ServiceBuildingType>> affectedBuildings in cityAoeData.AoeData)
                {
                    var targetBuildingId = affectedBuildings.Key;
                    var affectedServices = affectedBuildings.Value;
                    var targetBuilding = jGameManager.FindMapObjectById<IMapBuilding>(targetBuildingId);
                    if (targetBuilding is MDwellingHouseMapObject targetDwelling)
                    {
                        if (targetDwelling.IsFunctionalConstructionComplete)
                        {
                            targetDwelling.ClearServicesStatus();
                            targetDwelling.SetActiveServices(affectedServices);
                        }

                        targetDwelling.IsVerificated = true;
                        continue;
                    }

                    if (targetBuilding is IBuildingWithElectricity buildingWithElectricity)
                    {
                        bool previousEnergy = buildingWithElectricity.IsEnabledEnergy;
                        bool newEnergy =  affectedServices.Count > 0;
                        if (previousEnergy != newEnergy)
                        {
                            buildingWithElectricity.IsEnabledEnergy = newEnergy;
                            jSignalOnBuildingElectricityChanged.Dispatch(buildingWithElectricity, buildingWithElectricity.IsEnabledEnergy);
                        }
                        buildingWithElectricity.IsVerificated = true;
                    }
                }
            }
            
            if (cityAoeData.ConnectedToServices != null)
            {
                foreach (KeyValuePair<int, List<int>> connectedToServicesIds in cityAoeData.ConnectedToServices)
                {
                    int aoeBuildingId = connectedToServicesIds.Key;
                    var targetBuilding = jGameManager.FindMapObjectById<IAoeBuilding>(aoeBuildingId);
                    targetBuilding.AoeConnected = connectedToServicesIds.Value;
                }
            }

            jCityTaxesService.LoadNewTaxes();
            
            OnCityAoeDataReceived?.Invoke(cityAoeData);
        }
        
        private void OnNewObjectsMounted(List<IMapObject> obj)
        {
            LoadAoeData();
        }
        
        private void OnRoadRemoved(List<Id_IMapObject> obj)
        {
            LoadAoeData();
        }
        
        private void OnLevelUp()
        {
            LoadAoeData();
        }
    }
}