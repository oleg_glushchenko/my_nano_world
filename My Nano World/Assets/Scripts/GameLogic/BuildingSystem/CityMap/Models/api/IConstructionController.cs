﻿using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using strange.extensions.signal.impl;
using UnityEngine;

namespace NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api
{
	public class SignalOnConstructControllerChangeState : Signal<bool> { }

	public interface IConstructionController
	{
		/// <summary>
		/// Is road building mode enabled
		/// </summary>
		bool IsRoadBuildingMode { get; }
		/// <summary>
		//Is Construction controller enabled
		/// </summary>
		bool IsBuildModeEnabled { get; }

		/// <summary>
		/// Is any MapView selected for construction
		/// </summary>
		bool IsCurrentlySelectObject { get; }

		/// <summary>
		//Current selected view
		/// </summary>
		MapObjectView SelectedObjectView { get; }

		/// <summary>
		//Current selected model
		/// </summary>
		IMapObject SelectedModel { get; }

		/// <summary>
		/// Is road building mode in creation mode
		/// </summary>
		bool IsCreateRoad { get; }

		/// <summary>
		/// Is currently building object placed in valid area
		/// </summary>
		bool IsConstructionAvailable { get; }


		/// <summary>
		/// Create new map object
		/// </summary>
		/// <param name="id">Type id</param>
		/// <param name="position">Object's initial position</param>
		/// <returns>Is object was created</returns>
		bool CreateMapObject(Id_IMapObjectType id, BusinessSectorsTypes sectorID,
	        Vector3F position = default(Vector3F), BuildingSubCategories buildingSubCategories = BuildingSubCategories.Common);
		
		bool RestoreMapObject(IMapObject obj, Vector3F position = default);

		/// <summary>
		/// Set position for currently selected object
		/// </summary>
		/// <param name="newPosition"></param>
		void PlaceMapObject(Vector2Int newPosition);
		
		/// <summary>
		/// Confirm building
		/// </summary>
		void ConfirmChangeObject();

		/// <summary>
		/// Cancel building
		/// </summary>
		void CancelChangeObject();

		void ActivateBuildingMode(MapObjectView view);
	}
}
