﻿using System;
using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using NanoLib.GeneratedTileMap;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Impl;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using UnityEngine;


namespace NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api
{
    /// <summary>
    /// Represent base city grid interface
    /// </summary>
    public interface ICityGrid
    {
        #region properties
        
        event Action<Vector2Int, Vector2Int, CellTileType> OnMapCellsChanged;
        /// <summary>
        /// Grid width (Y coordinate)
        /// </summary>
        int Width { get; }

        /// <summary>
        /// Grid height (X coordinate)
        /// </summary>
        int Height { get; }

        /// <summary>
        /// Matrix of grid cells
        /// </summary>
        List<List<CityGridCell>> CityGridCells { get; set; }

        Vector2Int Dimensions { get; }

        void CreateGrid(int width, int height);

        /// <summary>
        /// Grid row
        /// </summary>
        /// <param name="index">row (Y coordinate)</param>
        /// <returns></returns>
        List<CityGridCell> this[int index] { get; set; }

        /// <summary>
        /// Grid Cell
        /// </summary>
        /// <param name="index1">Y</param>
        /// <param name="index2">X</param>
        /// <returns></returns>
        CityGridCell this[int index1, int index2] { get; set; }

        #endregion


        /// <summary>
        /// Search a map object using grid coords
        /// </summary>
        /// <param name="position">grid cell coordinate</param>
        /// <returns>object on map</returns>
        IMapObject GetMapObject(Vector2F position);

        /// <summary>
        /// Set object on grid
        /// </summary>
        /// <param name="position">object start cell coordinate</param>
        /// <param name="mapObject">map object</param>
        void SetMapObject(Vector2Int position, IMapObject mapObject);

        /// <summary>
        /// Move object on grid
        /// </summary>
        /// <param name="position">object start cell coordinate</param>
        /// <param name="mapObject">map object</param>
        void MoveMapObject(Vector2Int oldPosition, Vector2Int newPosition, IMapObject mapObject);

        /// <summary>
        /// Removes map object from a grid
        /// </summary>
        /// <param name="position">grid cell coordinate</param>
        /// <param name="mapObject"></param>
        void RemoveMapObject(Vector2Int position, IMapObject mapObject);

        /// <summary>
        /// Remove all objects from grid
        /// </summary>
        void ClearGrid();

        void CacheGrid();

        List<IMapObject> RestoreGrid();

        /// <summary>
        /// Checks if a grids of mapId contains any cells that belongs other map objects
        /// </summary>
        /// <param name="startCell"></param>
        /// <param name="endCell"></param>
        /// <param name="mapId"></param>
        /// <returns></returns>
        bool ContainsOtherMapObjects(Vector2Int startCell, Vector2Int endCell, int mapId);

        bool Contains(int x, int y);
        bool Contains(Vector2Int leftBottomPos, Vector2Int dimension);
    }
}
