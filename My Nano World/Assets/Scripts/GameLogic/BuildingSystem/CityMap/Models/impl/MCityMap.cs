﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.UtilitesEngine.CustomDebugging.Models.api;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.Analytics;
using NanoLib.Core.Services.Sound;
using NanoReality.Engine.UI.Extensions.EditModePanels;
using NanoReality.Game.Data;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using Newtonsoft.Json;
using UnityEngine;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.Services;
using UniRx;

namespace NanoReality.GameLogic.BuildingSystem.CityMap.Models.Impl
{
    /// <summary>
    /// Класс карты 
    /// </summary>
    public sealed class MCityMap : ICityMap
    {
        #region JsonFields

        /// <summary>
        /// Список объектов на карте
        /// </summary>
        [JsonProperty("map_buildings")]
        public List<IMapObject> MapObjects { get; set; }

        [JsonProperty("map_size")]
        public MapSize MapSize { get; set; }

        [JsonProperty("happiness_zones")]
        public List<AoeBuildingZone> AoeBuildingZones;
        
        [JsonProperty("electricity_zones")]
        public List<AoeBuildingZone> ElectricBuildingZones;

        #endregion

        #region JsonIgnoreFields
        
        /// <summary>
        /// City ID where map located
        /// </summary>
        public Id<IUserCity> UserCityId { get; set; }

        /// <summary>
        /// Business sector where map located
        /// </summary>
        public BusinessSectorsTypes BusinessSector { get; set; }

        public CitySubLockedMap SubLockedMap { get; set; }

        #endregion

        #region Injects

        [Inject] public ISoundManager jSoundManager { get; set; }

        [Inject] [JsonIgnore] public IDebug jDebug { get; set; }

        [Inject] [JsonIgnore] public IMapObjectsGenerator jMapObjectsGenerator { get; set; }
        
        [Inject] [JsonIgnore] public ICityGrid jCityGrid { get; set; }

        [Inject] [JsonIgnore] public MapObjectAddedSignal MapObjectAddedSignal { get; set; }

        [Inject] [JsonIgnore] public StartCityMapInitializationSignal jStartCityMapInitializationSignal { get; set; }

        [Inject] public FinishCityMapInitSignal jFinishCityMapInitSignal { get; set; }

        [Inject] public SignalOnMapObjectUpgradedOnMap jSignalOnMapObjectUpgradedOnMap { get; set; }

        [Inject] public IServerCommunicator ServerCommunicator { get; set; }

        [Inject] public IBuildingService jBuildingService { get; set; }

        [Inject]  public SignalOnActionWithCurrency jSignalOnActionWithCurrency { get; set; }
        [Inject] [JsonIgnore] public SignalOnFullClearMap SignalOnFullClearMap { get; set; }
        [Inject] public SignalOnMountObjects SignalOnMountObjects { get; set; }
        [Inject] public SignalOnDeleteRoads SignalOnDeleteRoads { get; set; }
        [Inject] public LockedSectorAddedSignal jLockedSectorAddedSignal { get; set; }
        [Inject] public BuildingConstructionStartedSignal jBuildingConstructionStarted { get; set; }

        [Inject] public IGameCamera jGameCamera { get; set; }
        [Inject] public IMapObjectsData jMapObjectsData { get; set; }
        [Inject] public IEditModeService jEditModeService { get; set; }
        [Inject] public SignalMoveMapObjectView jSignalMoveMapObjectView { get; set; }
        
        #endregion

        private readonly Dictionary<Id_IMapObject, IMapObject> _mapObjectsById;
        private readonly Dictionary<MapObjectintTypes, List<IMapObject>> _mapObjectsByType;
        private readonly Dictionary<Id_IMapObjectType, List<IMapObject>> _mapObjectsByTypeId;

        public MCityMap()
        {
            const int initialCollectionSize = 100;
            SubLockedMap = new CitySubLockedMap();
            MapObjects = new List<IMapObject>(initialCollectionSize);
            _mapObjectsById = new Dictionary<Id_IMapObject, IMapObject>(initialCollectionSize);
            _mapObjectsByType = new Dictionary<MapObjectintTypes, List<IMapObject>>(initialCollectionSize);
            _mapObjectsByTypeId = new Dictionary<Id_IMapObjectType, List<IMapObject>>(initialCollectionSize);
        }

        public Vector2F GetAverageBuildingsPosition()
        {
            var resultPosition = new Vector2F(0, 0);
            var buildingsCount = 0;

            foreach (var mapObject in MapObjects)
            {
                if (mapObject.MapObjectType == MapObjectintTypes.Road)
                    continue;

                resultPosition += jGameCamera.GetMapObjectPosition(mapObject);
                buildingsCount++;
            }

            //todo: should calculate as its done in a buildingconstructor class
            return buildingsCount > 0 ? resultPosition / buildingsCount : resultPosition;
        }

        public IMapObject FindMapObject(MapObjectintTypes type) =>
            _mapObjectsByType.TryGetValue(type, out var list) ? list.FirstOrDefault() : null;

        public IMapObject FindMapObject(Id_IMapObjectType type) =>
            _mapObjectsByTypeId.TryGetValue(type, out var list) ? list.FirstOrDefault() : null;

        public List<IMapObject> FindAllMapObjects(MapObjectintTypes type) =>
            _mapObjectsByType.TryGetValue(type, out var list) ? list : null;


        public List<IMapObject> FindAllMapObjects(Id_IMapObjectType type) =>
            _mapObjectsByTypeId.TryGetValue(type, out var list) ? list : null;

        /// <summary>
        /// Search a map object using grid coords
        /// </summary>
        /// <param name="position">grid cell coordinate</param>
        /// <returns>object on map</returns>
        public IMapObject GetMapObject(Vector2F position) => jCityGrid.GetMapObject(position);

        /// <summary>
        /// Search a map object using object ID
        /// </summary>
        /// <param name="mapId">grid cell coordinate</param>
        /// <returns>object on map</returns>
        public IMapObject GetMapObject(Id_IMapObject mapId)
        {
            return _mapObjectsById.TryGetValue(mapId, out IMapObject mapObject) ? mapObject : null;
        }

        private bool _isStartInit;

        /// <summary>
        /// Map initialization
        /// </summary>
        /// <param name="onInitCallback"></param>
        public void InitMap(Action onInitCallback = null)
        {
            if (_isStartInit)
            {
                onInitCallback?.Invoke();
                return;
            }
            
            _isStartInit = true;
            
            jCityGrid.CreateGrid(MapSize.Width, MapSize.Height);
            jStartCityMapInitializationSignal.Dispatch(this);

            Observable.FromCoroutine(() => EInitMap(onInitCallback)).Subscribe();
        }
        
        public void RestoreMap(Action onInitCallback = null)
        {
            _isStartInit = true;
            Observable.FromCoroutine(() => ERestoreMap(onInitCallback)).Subscribe();
        }
        
        private IEnumerator ERestoreMap(Action onInitCallback = null)
        {
            int instancesPerFrame = 1;
            int currentInstancesCount = 0;
            int mapObjectCount = 0;

            ClearCache();
            
            foreach (var mapObject in MapObjects)
            {
                currentInstancesCount++;
                if (currentInstancesCount > instancesPerFrame)
                {
                    yield return new WaitForSeconds(0.05f);
                    currentInstancesCount = 0;
                }
                
                var prototype = jMapObjectsGenerator.GetMapObjectModel(mapObject.ObjectTypeID, mapObject.Level);
                mapObject.OnInitOnMap(prototype);
                mapObject.SectorId = BusinessSector.GetBusinessSectorId();

                CacheMapObject(mapObject);
                
                var gridPos = new Vector2Int(mapObject.MapPosition.intX, mapObject.MapPosition.intY);
                jCityGrid.SetMapObject(gridPos, mapObject);

                MapObjectAddedSignal.Dispatch(gridPos, mapObject, () =>
                {
                    mapObject.MapPosition = new Vector2F(gridPos.x, gridPos.y);
                    jSignalMoveMapObjectView.Dispatch(mapObject.MapPosition.ToVector2Int(), mapObject, () =>
                    {
                        mapObject.OnPostInitObject();
                        if (++mapObjectCount == MapObjects.Count)
                        {
                            onInitCallback?.Invoke();
                        }
                    });
                });
            }
        }

        private IEnumerator EInitMap(Action onInitCallback = null)
        {
            int instancesPerFrame = 1;
            int currentInstancesCount = 0;

            ClearCache();
            
            foreach (var mapObject in MapObjects)
            {
                currentInstancesCount++;
                if (currentInstancesCount > instancesPerFrame)
                {
                    yield return new WaitForSeconds(0.05f);
                    currentInstancesCount = 0;
                }
                
                var prototype = jMapObjectsGenerator.GetMapObjectModel(mapObject.ObjectTypeID, mapObject.Level);
    
                mapObject.OnInitOnMap(prototype);
                mapObject.SectorId = BusinessSector.GetBusinessSectorId();

                CacheMapObject(mapObject);
                
                var gridPos = new Vector2Int(mapObject.MapPosition.intX, mapObject.MapPosition.intY);
                jCityGrid.SetMapObject(gridPos, mapObject);
                
                MapObjectAddedSignal.Dispatch(gridPos, mapObject, null);
            }
            
            foreach (var mapObject in MapObjects)
            {
                mapObject.OnPostInitObject();
            }
            
            foreach (var lockedSector in SubLockedMap.LockedSectors)
            {
                jLockedSectorAddedSignal.Dispatch(lockedSector);
                
                currentInstancesCount++;
                if (currentInstancesCount > instancesPerFrame)
                {
                    yield return new WaitForSeconds(0.05f);
                    currentInstancesCount = 0;
                }
            }

            jFinishCityMapInitSignal.Dispatch(this);
            onInitCallback?.Invoke();
        }

        private void ClearCache()
        {
            _mapObjectsById.Clear();
            _mapObjectsByType.Clear();
            _mapObjectsByTypeId.Clear();
        }

        private void CacheMapObject(IMapObject mapObject)
        {
            if (!_mapObjectsByType.TryGetValue(mapObject.MapObjectType, out var list))
            {
                list = new List<IMapObject>();
                _mapObjectsByType.Add(mapObject.MapObjectType, list);
            }
            list.Add(mapObject);
            
            if (!_mapObjectsByTypeId.TryGetValue(mapObject.ObjectTypeID, out list))
            {
                list = new List<IMapObject>();
                _mapObjectsByTypeId.Add(mapObject.ObjectTypeID, list);
            }
            list.Add(mapObject);

            _mapObjectsById[mapObject.MapID] = mapObject;

            if (mapObject.MapID.Value < 0)
            {
                mapObject.EventOnCommitBuilding.AddOnce((oldId, obj) =>
                {
                    var objectById = GetMapObjectById(oldId);
                    if (objectById != null && objectById == obj)
                    {
                        _mapObjectsById.Remove(oldId);
                    }

                    _mapObjectsById[obj.MapID] = obj;
                });
            }
        }

        private void RemoveFromCache(IMapObject mapObject)
        {
            _mapObjectsById.Remove(mapObject.MapID);

            if (_mapObjectsByType.TryGetValue(mapObject.MapObjectType, out var list))
            {
                list.Remove(mapObject);
            }

            if (_mapObjectsByTypeId.TryGetValue(mapObject.ObjectTypeID, out list))
            {
                list.Remove(mapObject);
            }
        }

        /// <summary>
        /// Ищет объект по его айди
        /// </summary>
        /// <param name="mapId">айди объекта который нужно найти на карте</param>
        /// <returns></returns>
        public IMapObject GetMapObjectById(Id_IMapObject mapId) => GetMapObject(mapId);
        
        public T GetMapObjectById<T>(int buildingMapId) where T : IMapObject
        {
            return (T)GetMapObjectById(buildingMapId);
        }

        public int GetMapObjectsCountByType(MapObjectintTypes mapObjectType) =>
            FindAllMapObjects(mapObjectType)?.Count ?? 0;

        /// <summary>
        /// Remove all objects from a map
        /// </summary>
        public void ClearMap()
        {
            ClearCache();

            foreach (var mapObject in MapObjects)
            {
                mapObject.RemoveMapObject();
            }
            MapObjects.Clear();
        }
        
        public void ClearMapView()
        {
            SignalOnFullClearMap.Dispatch(this);
            jCityGrid.ClearGrid();
            _isStartInit = false;
        } 

        public void MoveMapObject(Vector2Int oldPosition, Vector2Int newPosition, MMapObject mapObject)
        {
            jCityGrid.MoveMapObject(oldPosition, newPosition, mapObject);
            mapObject.MoveMapObject(oldPosition, newPosition);

            if (jEditModeService.IsEditModeEnable)
            {
                jEditModeService.MoveBuilding(mapObject,(() =>
                {
                    var model = new MoveMapObjectResponce();
                    model.MapObject = mapObject;
                    mapObject.MoveMapObjectVerificated(model);
                    jSoundManager.Play(SfxSoundTypes.MoveBuildings, SoundChannel.SoundFX);
                    
                }));
                return;
            }

            ServerCommunicator.MoveMapObject(mapObject.GetMapIDString, mapObject.MapPosition, result =>
             {
                 jSoundManager.Play(SfxSoundTypes.MoveBuildings, SoundChannel.SoundFX);
                 mapObject.MoveMapObjectVerificated(result);
             });
         }

        private void AddObject(IMapObject mapObject, Vector2Int toVector2F)
        {   
            jCityGrid.SetMapObject(toVector2F, mapObject);                    
            MapObjects.Add(mapObject);                                        
            CacheMapObject(mapObject);
        }

        public void MountObject(IMapObject mapObject, Vector2Int toVector2F)
        {
            var buildingId = mapObject.ObjectTypeID;
            
            jCityGrid.SetMapObject(toVector2F, mapObject);

            MapObjects.Add(mapObject);                                        
            CacheMapObject(mapObject);
            
            IMapObject mapObjectData = jMapObjectsData.GetMapObjectData<IMapObject>(buildingId, mapObject.Level);
            mapObject.OnInitOnMap(mapObjectData);
            
            jBuildingService.ConstructMapObject(mapObject);
            jBuildingConstructionStarted.Dispatch(mapObject);
        }
        
        public void RestoreObject(IMapObject mapObject, Vector2Int toVector2F, Action callback)
        {
            jCityGrid.SetMapObject(toVector2F, mapObject);                                     
            CacheMapObject(mapObject);
            
            IMapObject mapObjectData = jMapObjectsData.GetMapObjectData<IMapObject>(mapObject.ObjectTypeID, mapObject.Level);
            mapObject.OnInitOnMap(mapObjectData);
            callback.Invoke();
        }
        
        public void MountRoads(List<IMapObject> mapObject)
        {
            // removing objects that have already been sent to the server from the list
            mapObject.RemoveAll(o => jCityGrid.GetMapObject(o.MapPosition) != null);
            
            if (mapObject.Count < 1) return;
            
            // adding buildings to the map immediately, without waiting for a server response 
            foreach (var b in mapObject)
            {
                var gridPos = new Vector2Int(b.MapPosition.intX, b.MapPosition.intY);
                AddObject(b, gridPos);
            }

            jSoundManager.Play(SfxSoundTypes.BuildRoad, SoundChannel.SoundFX);

            int roadId = mapObject[0].ObjectTypeID;
            int sectorId = mapObject[0].SectorId.Value;

            void OnRoadConstructed(CreateRoadResponse data)
            {
                for (int i = 0; i < data.Roads.Count; i++)
                {
                    mapObject[i].CommitBuilding(data.Roads[i]);
                }

                foreach (Id_IMapObject id in data.Buildings.ExitToMainRoadBuildings)
                {
                    IMapObject obj = GetMapObjectById(id);
                    if (obj == null)
                    {
                        jDebug.LogWarning($"MapObject with id [{id}] not found in map objects list for sector [{BusinessSector}]");
                        continue;
                    }

                    obj.IsConnectedToGeneralRoad = true;
                }

                foreach (var id in data.Buildings.NotExitToMainRoadBuildings)
                {
                    IMapObject obj = GetMapObjectById(id);
                    if (obj != null)
                        obj.IsConnectedToGeneralRoad = false;
                }
            }
            
            jBuildingService.ConstructRoad(sectorId, roadId, mapObject, OnRoadConstructed);

			SignalOnMountObjects.Dispatch(mapObject);
        }

        public void ReastoreRoads()
        {
            
        }

        public void DeleteRoad(List<Id_IMapObject> roads, Action complete)
        {
            void OnComplete(DeleteRoadResponse result)
            {
                if (result.ExitToMainRoadBuildings != null)
                {
                    foreach (var id in result.ExitToMainRoadBuildings)
                    {
                        var obj = GetMapObjectById(id);
                        if(obj!= null)
                            obj.IsConnectedToGeneralRoad = true;
                    }
                }

                if (result.NotExitToMainRoadBuildings != null)
                {  
                    foreach (var id in result.NotExitToMainRoadBuildings)
                    {
                        var obj = GetMapObjectById(id);
                        if(obj!= null)
                            obj.IsConnectedToGeneralRoad = false;
                    }
                }

                complete();
                SignalOnDeleteRoads.Dispatch(roads);
            }
            
            jSoundManager.Play(SfxSoundTypes.RemoveRoad, SoundChannel.SoundFX);

            if (jEditModeService.IsEditModeEnable)
            {
                jEditModeService.RemoveRoad(roads, BusinessSector.GetBusinessSectorId().ToString(), OnComplete);
            }
            else
            {
                ServerCommunicator.DeleteRoad(roads, OnComplete);
            }
        }
        
        public void UpgradeObject(Id_IMapObject mapId, bool withProducts = false)
        {
            IMapObject mapObject = GetMapObjectById(mapId);

            int constructionTime = jBuildingService.GetBuildingConstructionTime(mapId);
            mapObject.UpgradeObject(constructionTime);
            
            var prototype = jMapObjectsGenerator.GetMapObjectModel(mapObject.ObjectTypeID, mapObject.Level);
            mapObject.OnInitOnMap(prototype);
            mapObject.SectorId = BusinessSector.GetBusinessSectorId();

            jSignalOnMapObjectUpgradedOnMap.Dispatch(mapObject);

            // TODO: need to handle this case.
            // if (mapObject is IObjectWithPrice objWithPrice)
            //     jSignalOnActionWithCurrency.Dispatch(new SoftInfo(CurrencyConsumeActions.UpgradeBuilding, objWithPrice.Price, mapObject.Name));
        }

        /// <summary>
        /// Removes map object from a grid
        /// </summary>
        /// <param name="mapObject">map object ID</param>
        public void RemoveMapObjectById(Id_IMapObject mapObject)
        {
            var obj = GetMapObjectById(mapObject);
            RemoveMapObject(obj);
        }

        /// <summary>
        /// Removes map object from a grid
        /// </summary>
        /// <param name="position">grid cell coordinate</param>
        public void RemoveMapObjectOnCell(Vector2F position)
        {
            var obj = GetMapObject(position);
            RemoveMapObject(obj);            
        }

        /// <summary>
        /// Удаялет объект с карты
        /// </summary>
        /// <param name="mapObject">Объект который нужно удалить</param>
        public void RemoveMapObject(IMapObject mapObject)
        {
            if (mapObject == null)
            {
                return;
            }

            RemoveFromCache(mapObject);

            MapObjects.Remove(mapObject);
            jCityGrid.RemoveMapObject(new Vector2Int(mapObject.MapPosition.intX, mapObject.MapPosition.intY), mapObject);

            mapObject.RemoveMapObject();            
        }
    }
}
