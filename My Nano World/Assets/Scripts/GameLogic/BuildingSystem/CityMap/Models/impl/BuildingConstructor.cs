﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using UnityEngine;
using NanoLib.Services.InputService;
using Assets.Scripts.Engine.BuildingSystem.MapObjects.Components;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Assets.Scripts.GameLogic.Utilites;
using GameLogic.BuildingSystem.MapObjects.Models.api.Core;
using NanoLib.Core.Logging;
using NanoLib.UI.Core;
using NanoReality.Engine.BuildingSystem.CityCamera;
using NanoReality.Engine.BuildingSystem.MapObjects.Components;
using NanoReality.Engine.UI.Extensions.EditModePanels;
using NanoReality.Game.Services.BuildingServices;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
using NanoReality.GameLogic.Utilities;

namespace NanoReality.GameLogic.BuildingSystem
{
	public class BuildingConstructor : BaseConstructor
	{
		private int _deltaMapId = 1;
		
		#region Injects
        
		[Inject] public IUIManager jUIManager { get; set; }
		[Inject] public IGameCamera jGameCamera { get; set; }
		[Inject] public IGameCameraModel jGameCameraModel { get; set; }
		[Inject] public IMapObjectPlacementService jMapObjectPlacementService { get; set; }
		[Inject] public IInputController jInputController { get; set; }     
        [Inject] public SignalMoveMapObjectView SignalMoveMapObjectView { get; set; }
        [Inject] public SignalSimulateLongTap jSignalSimulateLongTap { get; set; }
        [Inject] public IHintTutorial jHintTutorial { get; set; }
        [Inject] public IEditModeService jEditModeService { get; set; }

        #endregion

		public override bool IsCurrentlySelectObject { get; set; }

		public override bool IsBuildModeEnabled
		{
			get => isBuildModeEnabled;
			set
			{
				bool stateChanged = isBuildModeEnabled != value;
				isBuildModeEnabled = value;

				OnSelectMapObject(jGameManager.CurrentSelectedObjectView, true);
				
				if (stateChanged)
				{
					jSignalOnConstructControllerChangeState.Dispatch(jGameManager.CurrentSelectedObjectView != null && isBuildModeEnabled);
					
					if (!isBuildModeEnabled)
					{
						SwitchOffBuildingMode();
					}
				}
			}
		}

		public override bool IsBuildNewBuilding { get; protected set; }

		public override MapObjectView SelectedObjectView { get; set; }
		public override IMapObject SelectedModel { get; set; }

		public override bool IsConstructionAvailable
		{
			get
			{
				if (SelectedModel == null || SelectedObjectView == null)
				{
					return false;
				}
				
				Vector2Int position = SelectedObjectView.DragComponent.CurrentGridPosition;

				// low-level dependency, should be fixed
				if (!jHardTutorial.IsBuildingApprovedByTutorial(position) || !jHintTutorial.IsBuildingApprovedByHintTutorial(position))
				{
					return false;
				}
				
				jMapObjectPlacementService.SetContextData(SelectedModel);

				return jMapObjectPlacementService.Constructable(position);
			}
		}

	    public void ActivateBuildingMode(MapObjectView pressedObject)
		{
			if (!pressedObject.IsMovable || IsCurrentlySelectObject)
				return;

			jUIManager.HideAll();
			pressedObject.SelectComponent.ChangeSelect(true);
			IsBuildModeEnabled = true;
		}

	    private void SetMapObjectAndSelectIt(IMapObject mapObject, Vector3F position)
        {
	        if (!IsBuildModeEnabled)
		        return;
	        
	        Vector2Int gridPos = ConvertToGridPos(mapObject, position);
            MapObjectAddedSignal.Dispatch(gridPos, mapObject, () =>
            {
	            if (SelectedObjectView != null)
	            {
		            DeselectObject();
	            }
	            mapObject.MapPosition = new Vector2F(gridPos.x, gridPos.y);
	            
	            SelectedObjectView = jGameManager.GetMapView(mapObject.SectorId).GetMapObjectById(mapObject.MapID);
	            SelectedModel = mapObject;
	            SelectedObjectView.SelectComponent.ChangeSelect(true);
	            //SelectedObjectView turn on visibble

	            if(position != default)
	            {
		            SwipeCameraToSyncInputPosAndMapViewPos();
		            jSignalSimulateLongTap.Dispatch(SelectedObjectView, position.ToVector3());
		            jSignalOnConstructControllerChangeState.Dispatch(true);
	            }
	            else
	            {
		            jGameCamera.ZoomTo(SelectedModel, CameraSwipeMode.Fly, jGameCameraModel.DefaultZoomProperty.Value, hardAutoSwipe: true);
		            jSignalOnConstructControllerChangeState.Dispatch(true);
	            }

	            MapObjectSemiTransparentComponent mapObjectSemiTransparentComponent = SelectedObjectView.SemiTransparentComponent;
	            mapObjectSemiTransparentComponent.IsSemiTransparentActive = false;
            });
        }

        private void SwipeCameraToSyncInputPosAndMapViewPos()
        {
	        if (jHintTutorial.IsHintTutorialActive || !jHardTutorial.IsTutorialCompleted) return;

	        Vector3 fingerPos = jGameCamera.ScreenToWorldPoint(jInputController.CurrentFingerPosition);
	        Vector3 deltaCam = jGameCameraModel.Position.ToVector3() - fingerPos;
	        Vector3 worldPosition = SelectedObjectView.WorldPosition + deltaCam;
	        float depthOffset = MapObjectGeometry.GetHeightOffset(worldPosition.y, jGameCameraModel.AngleDegrees);

	        worldPosition.x += depthOffset;
	        worldPosition.z += depthOffset;

	        var posToSwipe = new Vector2F(worldPosition.x, worldPosition.z);
	        jGameCamera.SwipeTo(posToSwipe, CameraSwipeMode.Jump, 10f, true);
        }

        private Vector2Int ConvertToGridPos(IMapObject mapObject, Vector3F position)
        {
	        jMapObjectPlacementService.SetContextData(mapObject);
	        if (position == default)
	        {
		        Vector3 screenPoint = position == Vector3F.Zero
			        ? Camera.main.WorldToScreenPoint(jGameManager.CurrentGameCamera.Position.ToVector3())
			        : position.ToVector3();

		        RaycastHit hit = RaycastGround(screenPoint.ToVector3F());
		        Vector2Int gridPos = jGameManager.GetMapView(SelectedModel.SectorId).ConvertToGridPos(hit.point);
		        return jMapObjectPlacementService.FindConstructPosition(gridPos);
	        }
	        else
	        {
		        RaycastHit hit = RaycastGround(position);
		        Vector2Int gridPos = jGameManager.GetMapView(SelectedModel.SectorId).ConvertToGridPos(hit.point);
		        return jMapObjectPlacementService.FindConstructPosition(gridPos);
	        }
        }

        public void PreMoveMapObject(Vector2Int newPosition)
		{
			if (IsBuildModeEnabled)
			{
				SignalMoveMapObjectView.Dispatch(newPosition, SelectedModel, null);
			}
		}

		public override void ConfirmChangeObject()
		{
			switch (IsBuildNewBuilding, jEditModeService.IsEditModeEnable)
			{
				case (true, true):
					CityMap.RestoreObject(SelectedModel, SelectedObjectView.DragComponent.CurrentGridPosition, () =>
					{
						jEditModeService.RestoreBuilding(SelectedModel, SelectedObjectView.DragComponent.CurrentGridPosition);
					});
					break;
				case (true, false): 
					CityMap.MountObject(SelectedModel, SelectedObjectView.DragComponent.CurrentGridPosition);
					break;
				default:
					if (SelectedModel is IMovableObject)
					{
						var oldGridPos = new Vector2Int(SelectedModel.MapPosition.intX, SelectedModel.MapPosition.intY);
						CityMap.MoveMapObject(oldGridPos, SelectedObjectView.DragComponent.CurrentGridPosition, (MMapObject) SelectedModel);
					}
					break;
			}

			DeselectObject();
			IsBuildModeEnabled = false;
		}

		public override void CancelChangeObject()
		{
				if (SelectedModel == null) return;

				if (IsBuildNewBuilding) 
				{
					jSignalRemoveMapObjectView.Dispatch(SelectedModel);
					SelectedModel.FreeObject();
				}
				else if (SelectedObjectView && SelectedModel is IMovableObject)
				{
					var gridPos = new Vector2Int(SelectedModel.MapPosition.intX, SelectedModel.MapPosition.intY);
					PreMoveMapObject(gridPos);
				}

				DeselectObject();
				IsBuildModeEnabled = false;
		}

		public override bool CreateMapObject(Id_IMapObjectType id, BusinessSectorsTypes sectorID, Vector3F position = default, BuildingSubCategories buildingSubCategories = BuildingSubCategories.None)
		{
			if (IsBuildNewBuilding)
				return false;
			
			IMapObject obj = MapObjectsGenerator.GetMapObjectModel(id, 0);
			
			var instancesCount = jGameManager.GetMapObjectsCountInCity(id);
			var newInstanceIndex = instancesCount + 1;

			obj.MapID = DefaultBuildingMapId - _deltaMapId++;
            obj.SectorId = sectorID.GetBusinessSectorId();
            obj.InstanceIndex = newInstanceIndex;
            obj.SubCategory = buildingSubCategories;

            SelectedModel = obj;

            IsBuildModeEnabled = true;
            IsBuildNewBuilding = true;

            SetMapObjectAndSelectIt(obj, position);

            return true;
		}
		
		public override bool RestoreMapObject(IMapObject obj, Vector3F position = default)
		{
			if (IsBuildNewBuilding) return false;

			SelectedModel = obj;
			IsBuildModeEnabled = true;
			IsBuildNewBuilding = true;
			SetMapObjectAndSelectIt(obj, position);
			return true;
		}

		private void DeselectObject()
		{
			SelectedObjectView.SelectComponent.ChangeSelect(false);

			SelectedModel = null;
			SelectedObjectView = null;
			IsCurrentlySelectObject = false;
		}

		public override void OnSelectMapObject(MapObjectView view, bool isSelected)
		{
			if (!IsBuildModeEnabled || IsCurrentlySelectObject || !isSelected)
				return;
			
			if (view != null)
			{
				IsCurrentlySelectObject = true;
				SelectedObjectView = view;
				SelectedModel = view.MapObjectModel;
			}
			
			if (IsBuildNewBuilding && SelectedModel != null && SelectedModel.MapID.Value > 0 && !jEditModeService.IsEditModeEnable)
				IsBuildNewBuilding = false;
		}
	}
}