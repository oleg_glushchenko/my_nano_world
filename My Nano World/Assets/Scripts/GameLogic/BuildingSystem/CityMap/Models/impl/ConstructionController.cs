﻿using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.Core.RoadConstructor;
using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using UnityEngine;

namespace NanoReality.GameLogic.BuildingSystem
{
	public class ConstructionController : IConstructionController
	{
		private BaseConstructor _currentConstructor;

		[Inject] public BuildingConstructor jBuildingConstructor { get; set; }
		[Inject] public RoadConstructor jRoadConstructor { get; set; }
		[Inject] public SignalOnMapObjectChangedSelect jSignalOnMapObjectChangedSelect { get; set; }
		[Inject] public RoadConstructSetActiveSignal jRoadConstructSetActiveSignal { get; set; }

		public bool IsRoadBuildingMode => jRoadConstructor.IsRoadBuildingMode;

		public bool IsCreateRoad => jRoadConstructor.IsCreateRoad;

		public bool IsCurrentlySelectObject => _currentConstructor.IsCurrentlySelectObject;

		public bool IsBuildModeEnabled => _currentConstructor.IsBuildModeEnabled;

		public MapObjectView SelectedObjectView => _currentConstructor.SelectedObjectView;

		public IMapObject SelectedModel => _currentConstructor.SelectedModel;

		public bool IsConstructionAvailable => _currentConstructor.IsConstructionAvailable;
		
		[PostConstruct]
		public void PostConstruct()
		{
			jSignalOnMapObjectChangedSelect.AddListener(OnSelectMapObject);
			jRoadConstructSetActiveSignal.AddListener(BuildRoadModeChanged);

			SetNewConstructor(jBuildingConstructor);
		}

		[Deconstruct]
		public void Deconstruct()
		{
			jSignalOnMapObjectChangedSelect.RemoveListener(OnSelectMapObject);
			jRoadConstructSetActiveSignal.RemoveListener(BuildRoadModeChanged);

			SetNewConstructor(null);
		}

	    public bool CreateMapObject(Id_IMapObjectType id, BusinessSectorsTypes sectorId, Vector3F position = default, BuildingSubCategories buildingSubCategories = BuildingSubCategories.None)
	    {
	        if (!_currentConstructor.IsBuildNewBuilding || IsCreateRoad)
	        {
		        return _currentConstructor.CreateMapObject(id, sectorId, position, buildingSubCategories);
	        }

	        return false;
	    }

	    public bool RestoreMapObject(IMapObject obj, Vector3F position = default)
	    {
		    return _currentConstructor.RestoreMapObject(obj, position);
	    }

	    public void PlaceMapObject(Vector2Int newPosition)
		{
		    if (jRoadConstructor.IsBuildModeEnabled)
		    {
		        return;
		    }

            SetNewConstructor(jBuildingConstructor);
            
			jBuildingConstructor.PreMoveMapObject(newPosition);
		}

		public void ConfirmChangeObject()
		{
			_currentConstructor.ConfirmChangeObject();
		}

		public void CancelChangeObject()
		{
			_currentConstructor.CancelChangeObject();
		}

		public void ActivateBuildingMode(MapObjectView view)
		{
			if (jBuildingConstructor.IsBuildModeEnabled || jRoadConstructor.IsBuildModeEnabled)
				return;

			SetNewConstructor(jBuildingConstructor);
			
			jBuildingConstructor.ActivateBuildingMode(view);
		}

		private void OnSelectMapObject(MapObjectView sender, bool isSelected)
		{
			if (!isSelected)
			{
				return;
			}

			if (sender.MapObjectModel.MapObjectType == MapObjectintTypes.Road)
			{
				SetNewConstructor(jRoadConstructor);
			}
			else
			{
				SetNewConstructor(jBuildingConstructor);
			}

			_currentConstructor.OnSelectMapObject(sender, true);
		}

		private void BuildRoadModeChanged(bool state)
		{
			CancelChangeObject();
			SwitchOffBuildingMode();

			if (state)
			{
				SetNewConstructor(jRoadConstructor);
				_currentConstructor.IsBuildModeEnabled = true;
			}
			else
			{
				SetNewConstructor(jBuildingConstructor);
				_currentConstructor.IsBuildModeEnabled = false;
			}
		}

		private void SwitchOffBuildingMode()
		{
			jBuildingConstructor.SwitchOffBuildingMode();
			jRoadConstructor.SwitchOffBuildingMode();
		}

		private void SetNewConstructor(BaseConstructor constructor)
		{
			_currentConstructor = constructor;
		}
	}
}