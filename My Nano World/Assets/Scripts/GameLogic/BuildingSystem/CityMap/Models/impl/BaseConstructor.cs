﻿using NanoLib.Services.InputService;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UtilitesEngine.CustomDebugging.Models.api;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.Game.Tutorial;
using UnityEngine;

namespace NanoReality.GameLogic.BuildingSystem
{
	public abstract class BaseConstructor
	{
		public static Id_IMapObject DefaultBuildingMapId = new Id_IMapObject(-1);

		protected bool isBuildModeEnabled;

		#region Injects

		[Inject] public IMapObjectsGenerator MapObjectsGenerator { get; set; }
		[Inject] public IGameManager jGameManager { get; set; }
		[Inject] public IHardTutorial jHardTutorial { get; set; }

        #endregion

        #region Signals

        #region Dispatched
        [Inject]
		public SignalRemoveMapObjectView jSignalRemoveMapObjectView { get; set; }

		[Inject]
		public SignalOnConstructControllerChangeState jSignalOnConstructControllerChangeState { get; set; }

		[Inject]
		public MapObjectAddedSignal MapObjectAddedSignal { get; set; }

		#endregion

		#endregion

		/// <summary>
		/// Is some object seleted for building
		/// </summary>
		public abstract bool IsCurrentlySelectObject { get; set; }

		/// <summary>
		/// Is constructor enabled
		/// </summary>
		public abstract bool IsBuildModeEnabled { get; set; }

		/// <summary>
		/// Is new building selected (false if selected existing building)
		/// </summary>
		public abstract bool IsBuildNewBuilding { get; protected set; }

		public abstract MapObjectView SelectedObjectView { get; set; }

		public abstract IMapObject SelectedModel { get; set; }

		public bool HasSelection { get; set; }

	    /// <summary>
	    /// Returns city map based on sector id of selected map object model.
	    /// </summary>
        public ICityMap CityMap
	    {
	        get
	        {
	            if (SelectedModel != null)
	            {
	                return jGameManager.GetMap(SelectedModel.SectorId);
	            }
	            return null;
	        }
	    }
		
		/// <summary>
		/// Is currently building object placed in valid area
		/// </summary>
		public virtual bool IsConstructionAvailable
		{
			get; protected set;
		}

		public abstract bool CreateMapObject(Id_IMapObjectType id, BusinessSectorsTypes sectorID, Vector3F position = default(Vector3F),  BuildingSubCategories buildingSubCategories = BuildingSubCategories.Common);
		public abstract bool RestoreMapObject(IMapObject obj, Vector3F position = default);

		public abstract void OnSelectMapObject(MapObjectView sender, bool isSelected);

		public abstract void ConfirmChangeObject();

		public abstract void CancelChangeObject();

		public virtual void SwitchOffBuildingMode()
		{
			IsBuildNewBuilding = false;
			IsBuildModeEnabled = false;
		}

        /// <summary>
        /// Raycasts from screen point to ground layer and returns hit info.
        /// </summary>
        /// <param name="screenPoint"></param>
        /// <returns></returns>
	    protected RaycastHit RaycastGround(Vector3F screenPoint)
	    {
	        var layer = 1 << LayerMask.NameToLayer("Ground");
	        return InputTools.RaycastScene(screenPoint.ToVector3(), layer);
	    }

        /// <summary>
        /// Raycasts from screen point to ground layer setting hit info and city map.
        /// Returns true if city map is hit, otherwise returns false.
        /// </summary>
        /// <param name="screenPoint"></param>
        /// <param name="hit"></param>
        /// <param name="cityMap"></param>
        /// <returns></returns>
	    protected bool RaycastCityMap(Vector3F screenPoint, out RaycastHit hit, out ICityMap cityMap)
	    {
	        hit = RaycastGround(screenPoint);

	        if (hit.collider == null)
	        {
	            cityMap = null;
	            return false;
	        }

            cityMap = jGameManager.GetMap(hit.point);
            return cityMap != null;
	    }
	}

}
