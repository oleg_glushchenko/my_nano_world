﻿using System;
using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using NanoLib.GeneratedTileMap;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Settings.BuildSettings.Models.Api;
using strange.extensions.injector.api;
using UnityEngine;

namespace NanoReality.GameLogic.BuildingSystem.CityMap.Models.Impl
{
    public class MCityGrid : ICityGrid
    {
        #region Injections

        [Inject] public IInjectionBinder injectionBinder { get; set; }
        [Inject] public ICityMapSettings jCityMapSettings { get; set; }

        #endregion

        #region Properties

        public event Action<Vector2Int, Vector2Int, CellTileType> OnMapCellsChanged;

        public Vector2Int Dimensions { get; private set; }

        /// <summary>
        /// Grid width (X coordinate)
        /// </summary>
        public int Width { get; private set; }

        /// <summary>
        /// Grid height (Y coordinate)
        /// </summary>
        public int Height { get; private set; }

        private float _tileSize;

        /// <summary>
        /// Matrix of grid cells
        /// </summary>
        public List<List<CityGridCell>> CityGridCells { get; set; }
        private Dictionary<IMapObject, Vector2F> CacheMapObjectPositions = new Dictionary<IMapObject, Vector2F>();

        /// <summary>
        /// Grid row
        /// </summary>
        /// <param name="index">row (Y coordinate)</param>
        /// <returns></returns>
        public List<CityGridCell> this[int index]
        {
            get => CityGridCells[index];
            set => CityGridCells[index] = value;
        }

        /// <summary>
        /// Grid Cell
        /// </summary>
        /// <param name="index1">Y</param>
        /// <param name="index2">X</param>
        /// <returns></returns>
        public CityGridCell this[int index1, int index2]
        {
            get
            {
                return CityGridCells[index1][index2];
            }
            set => CityGridCells[index1][index2] = value;
        }

        #endregion

        #region Functions

        public void CreateGrid(int width, int height)
        {
            Width = width;
            Height = height;
            
            Dimensions = new Vector2Int(width, height);

            CreateGrid();
        }

        [PostConstruct]
        public void PostConstruct()
        {
            _tileSize = jCityMapSettings.GridCellScale;
        }

        /// <summary>
        /// Search a map object using grid coords
        /// </summary>
        /// <param name="position">grid cell coordinate</param>
        /// <returns>object on map</returns>
        public IMapObject GetMapObject(Vector2F position)
        {
            return this[position.intX, position.intY].Value;
            
        }
        private void CreateGrid()
        {
            CityGridCells = new List<List<CityGridCell>>(Width);
            for (int i = 0; i < Width; i++)
            {
                var row = new List<CityGridCell>(Height);
                for (int j = 0; j < Height; j++)
                {
                    var cell = new CityGridCell(_tileSize * new Vector2F(i,j));
                    row.Add(cell);
                }
                CityGridCells.Add(row);
            }
        }

        public List<IMapObject> RestoreGrid()
        {
            List<IMapObject> cacheBuildings = new List<IMapObject>();
            ClearGrid();
            foreach (var cacheMapObjectPosition in CacheMapObjectPositions)
            {
                SetMapObject(cacheMapObjectPosition.Value.ToVector2Int(), cacheMapObjectPosition.Key);
                cacheBuildings.Add(cacheMapObjectPosition.Key);
            }

            return cacheBuildings;
        }

        public void CacheGrid()
        {
            CacheMapObjectPositions.Clear();
            
            foreach (var cityGridCellList in CityGridCells)
            {
                foreach (var cityGridCell in cityGridCellList)
                {
                    if (cityGridCell.Value != null && !CacheMapObjectPositions.ContainsKey(cityGridCell.Value))
                    {
                        CacheMapObjectPositions.Add(cityGridCell.Value , cityGridCell.Value.MapPosition);
                    }
                }
            }
        }

        /// <summary>
        /// Set object on grid
        /// </summary>
        /// <param name="position">object start cell coordinate</param>
        /// <param name="mapObject">map object</param>
        public void SetMapObject(Vector2Int position, IMapObject mapObject)
        {
            Vector2Int startCell = position;
            Vector2Int lastCell = startCell + new Vector2Int(mapObject.Dimensions.intX, mapObject.Dimensions.intY);
            
            for (int i = startCell.x; i < lastCell.x; i++)
            {
                for (int j = startCell.y; j < lastCell.y; j++)
                {
                    //Debug.LogError($"{i}, {j} {Width} {Height}");
                    this[i, j].Value = mapObject;
                }
            }

            OnMapCellsChanged?.Invoke(startCell, lastCell, CellTileType.Closed);
            mapObject.MapPosition = ((Vector2) startCell).ToVector2F();
        }
        
        /// <summary>
        ///  Move object on grid
        /// </summary>
        /// <param name="oldPosition"></param>
        /// <param name="newPosition"></param>
        /// <param name="mapObject"></param>
        /// <returns></returns>
        public void MoveMapObject(Vector2Int oldPosition, Vector2Int newPosition, IMapObject mapObject)
        {
            RemoveMapObject(oldPosition, mapObject);
            SetMapObject(newPosition, mapObject);
        }

        /// <summary>
        /// Removes map object from a grid
        /// </summary>
        /// <param name="position">grid cell coordinate</param>
        /// <param name="mapObject"></param>
        public void RemoveMapObject(Vector2Int position, IMapObject mapObject)
        {
            Vector2Int startCell = position;
            Vector2Int lastCell = startCell + new Vector2Int(mapObject.Dimensions.intX, mapObject.Dimensions.intY);
            
            for (int i = startCell.x; i < lastCell.x; i++)
            {
                for (int j = startCell.y; j < lastCell.y; j++)
                {
                    this[i, j].Value = null;
                }
            }
            
            OnMapCellsChanged?.Invoke(startCell, lastCell, CellTileType.Free);
        }


        /// <summary>
        /// Remove all objects from grid
        /// </summary>
        public void ClearGrid()
        {
            foreach (var row in CityGridCells)
            {
                foreach (var cell in row)
                {
                    cell.Value = null;
                }
            }
        }

        public bool Contains(int x, int y)
        {
            if (x < 0)
                return false;

            if (x >= Width)
                return false;

            if (y < 0)
                return false;

            if (y >= Height)
                return false;

            return true;
        }
        
        public bool Contains(Vector2Int leftBottomPos, Vector2Int dimension)
        {
            if (leftBottomPos.x < Vector2Int.zero.x)
                return false;

            if (leftBottomPos.x >= Width - (dimension.x - 1))
                return false;
                
            if (leftBottomPos.y < Vector2Int.zero.y)
                return false;

            if (leftBottomPos.y >= Height - (dimension.y - 1))
                return false;

            return true;
        }

        public bool ContainsOtherMapObjects(Vector2Int startCell, Vector2Int endCell, int mapId)
        {
            for (var i = startCell.x; i <= endCell.x; i++)
            {
                for (var j = startCell.y; j <= endCell.y; j++)
                {
                    try
                    {
                        var cellObject = this[i, j].Value;
                        if (cellObject == null) continue;
                        if (cellObject.MapID == mapId) continue;
                    
                        // check if the object is a building, not road
                        if (cellObject.MapObjectType != MapObjectintTypes.Road) return true;
                        // allow roads in offset tiles

                        return true;
                    }
                    catch (Exception e)
                    {
                        Debug.LogError($"Try to get [{startCell}] [{endCell}] [{mapId}]");
                    }
                    
                }
            }

            return false;
        }

        #endregion
    }
}
