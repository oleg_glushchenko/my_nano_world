﻿using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;

namespace NanoReality.GameLogic.BuildingSystem.CityMap.Models.Impl
{
    public class CityGridCell
    {
        /// <summary>
        /// Cell value, if null - cell is empty
        /// </summary>
        public IMapObject Value { get; set; }
        
        public Vector2F GlobalPosition { get; set; }
        
        public CityGridCell()
        {
            
        }

        public CityGridCell(Vector2F globalPosition)
        {
            GlobalPosition = globalPosition;
        }
    }
}