using System.Collections.Generic;
using UnityEngine;

namespace NanoReality.GameLogic.Settings.BuildSettings.Models.Api
{
    public interface ICityMapSettings
    {
        #region Terrain

        float GridCellScale { get; }
	    
        float BuildingViewScale { get; }
	    
        float BuildingViewPositionOffset { get; }
        float CellSize { get; }
        
        float RoadViewScale { get; }
        List<GameObject> LockedSectorsPrefabs { get; }

        #endregion
    }
}