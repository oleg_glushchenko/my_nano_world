﻿using System;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoLib.GeneratedTileMap;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
using strange.extensions.signal.impl;
using UnityEngine;

namespace NanoReality.GameLogic.BuildingSystem.CityMap.Controllers
{
    /// <summary>
    /// Сигнал вызывается когда модель карты проинициализирована и готова к использованию
    /// </summary>
    public class StartCityMapInitializationSignal : Signal<ICityMap> { }

    /// <summary>
    /// Сигнал вызывается когда на модель карты установленны все здания
    /// </summary>
    public class FinishCityMapInitSignal : Signal<ICityMap> { }


    /// <summary>
    /// Сигнал вызываеся когда модель карты очищается от объектов
    /// </summary>
    public class SignalOnFullClearMap : Signal<ICityMap> { }

    /// <summary>
    /// Сигнал вызывается когда на карте происходит апгрейд объекта
    /// </summary>
    public class SignalOnMapObjectUpgradedOnMap : Signal<IMapObject> { }

    public class SignalOnFastFinishButtonActivated : Signal<MapObjectView> { }
    
    public class SignalOnEditModeStateChange : Signal<bool> { }
    public class SignalOnEditModeEraseStateChange : Signal<bool> { }

    /// <summary>
    /// Вызывается, когда включается/выключается режим удаления дороги
    /// </summary>
    public class RoadConstructSetActiveSignal : Signal<Boolean> { }

    /// <summary>
    /// Сигнал вызывается когда начинается строительство дороги (параметр - кол-во построенных тайлов дорог)
    /// </summary>
    public class SignalOnStartRoadCreation : Signal<int> { }


    /// <summary>
    /// Вызывается для передвижения вьюшки на новую позицию
    /// </summary>
    public class SignalMoveMapObjectView : Signal<Vector2Int, IMapObject, Action>{ }

    public class TileOfRoadEditedSignal : Signal
    {
    }

    /// <summary>
    /// Вызывается когда было передвинуто здание
    /// </summary>
    public class SignalOnMovedMapObjectOnServer : Signal<IMapObject> { }


    /// <summary>
    /// Вызывается когда здание удаляется с карты
    /// </summary>
	public class SignalOnMapObjectRemoved : Signal<IMapObject> { }

    /// <summary>
    /// Вызывается когда здание было установлено на карту
    /// </summary>
    public class MapObjectAddedSignal : Signal<Vector2Int, IMapObject, Action> { }
    
    public class LockedSectorAddedSignal : Signal<CityLockedSector> { }

    /// <summary>
    /// Вызывается при начале постройки
    /// </summary>
    public class SignalOnBuildStared : Signal<IMapObject> { }
    
    /// <summary>
    /// Called when there was a success response from server when building started
    /// </summary>
    public class SignalOnBuildStartedVerified : Signal<IMapObject> { }

    /// <summary>
    /// Вызывается для удаления объекта вьюшки с карты
    /// </summary>
    public class SignalRemoveMapObjectView : Signal<IMapObject> { }
}
