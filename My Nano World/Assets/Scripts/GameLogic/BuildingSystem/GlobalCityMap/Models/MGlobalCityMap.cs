﻿using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.UtilitesEngine.CustomDebugging.Models.api;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.Analytics;
using Assets.Scripts.Engine.Analytics.Extensions.CurrencyInfo;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.GlobalCityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Services;
using Newtonsoft.Json;

namespace NanoReality.GameLogic.BuildingSystem.GlobalCityMap.Models
{
    public class MGlobalCityMap : IGlobalCityMap
    {
        private readonly Dictionary<Id_IMapObject, IMapObject> _mapObjectsById = new Dictionary<Id_IMapObject, IMapObject>();
        private readonly Dictionary<MapObjectintTypes, List<IMapObject>> _mapObjectsByType = new Dictionary<MapObjectintTypes, List<IMapObject>>();
        private readonly Dictionary<Id_IMapObjectType, List<IMapObject>> _mapObjectsByTypeId = new Dictionary<Id_IMapObjectType, List<IMapObject>>();

        [JsonProperty("map_buildings")]
        public List<IMapObject> MapObjects { get; private set; }

        public Id<IPlayerProfile> UserId { get; private set; }

        public Id<IUserCity> UserCityId { get; private set; }

        public Id<IBusinessSector> BusinessSectorId { get; private set; }
        
        [Inject] public IDebug jDebug { get; set; }
        [Inject] public IMapObjectsGenerator jObjectsGenerator { get; set; }
        [Inject] public SignalOnInitGlobalCityMap jSignalOnInitGlobalCityMap { get; set; }
        [Inject] public SignalOnMapObjectUpgradedOnMap jSignalOnMapObjectUpgradedOnMap { get; set; }
        [Inject] public SignalOnActionWithCurrency jSignalOnActionWithCurrency { get; set; }

        [Inject] public IBuildingService jBuildingService { get; set; }

        #region Implementation of IGlobalCityMap

        private void CacheMapObject(IMapObject mapObject)
        {
            List<IMapObject> list;
            
            if (!_mapObjectsByType.TryGetValue(mapObject.MapObjectType, out list))
            {
                list = new List<IMapObject>();
                _mapObjectsByType.Add(mapObject.MapObjectType, list);
            }
            list.Add(mapObject);
            
            if (!_mapObjectsByTypeId.TryGetValue(mapObject.ObjectTypeID, out list))
            {
                list = new List<IMapObject>();
                _mapObjectsByTypeId.Add(mapObject.ObjectTypeID, list);
            }
            list.Add(mapObject);

            _mapObjectsById[mapObject.MapID] = mapObject;
        }

        private void ClearCache()
        {
            _mapObjectsById.Clear();
            _mapObjectsByType.Clear();
            _mapObjectsByTypeId.Clear();
        }

        public void Init(Id<IPlayerProfile> userId, Id<IUserCity> userCityId, Id<IBusinessSector> businessSectorId)
        {
            UserId = userId;
            UserCityId = userCityId;
            BusinessSectorId = businessSectorId;

            ClearCache();
            
            //TODO:  Remove Excess Warehouses from server but not here
            RemoveExcessWarehouses();

            for (var i = 0; i < MapObjects.Count; i++)
            {
                var mapObject = MapObjects[i];

                var prototype = jObjectsGenerator.GetMapObjectModel(mapObject.ObjectTypeID, mapObject.Level);
                mapObject.OnInitOnMap(prototype);
                mapObject.SectorId = BusinessSectorId;
                
                CacheMapObject(mapObject);
            }
            
            for (var i = 0; i < MapObjects.Count; i++)
            {
                var mapObject = MapObjects[i];
                mapObject.OnPostInitObject();
            }

            jSignalOnInitGlobalCityMap.Dispatch(this);
        }

        private void RemoveExcessWarehouses()
        {
            var firstWarehouse = MapObjects.FirstOrDefault(x => x.Id.Id == 27);
            MapObjects.RemoveAll(x => x.Id.Id == 27);
            MapObjects.Add(firstWarehouse);
        }

        public void UpgradeObject(Id_IMapObject mapId)
        {
            IMapObject mapObject;
            if (!_mapObjectsById.TryGetValue(mapId, out mapObject))
            {
                jDebug.LogError("Failed to upgrade object in global city map by id: " + mapId);
                return;
            }
            
            mapObject.UpgradeObject(jBuildingService.GetBuildingConstructionTime(mapId));

            var prototype = jObjectsGenerator.GetMapObjectModel(mapObject.ObjectTypeID, mapObject.Level);

            mapObject.OnInitOnMap(prototype);
            mapObject.SectorId = BusinessSectorId;

            jSignalOnMapObjectUpgradedOnMap.Dispatch(mapObject);

            // var objWithPrice = mapObject as IObjectWithPrice;
            // if (objWithPrice != null)
            // {
            //     jSignalOnActionWithCurrency.Dispatch(new SoftInfo(CurrencyConsumeActions.UpgradeBuilding, objWithPrice.Price, mapObject.Name));
            // }
        }

        public IMapObject FindMapObject(Id_IMapObject id)
        {
            return _mapObjectsById.TryGetValue(id, out IMapObject mapObject) ? mapObject : null;
        }

        public IMapObject FindMapObject(MapObjectintTypes objectType)
        {
            List<IMapObject> list;
            if (_mapObjectsByType.TryGetValue(objectType, out list))
            {
                return list.FirstOrDefault();
            }
            return null;
        }

        public List<IMapObject> FindAllMapObjects(MapObjectintTypes objectType)
        {
            List<IMapObject> list;
            if (_mapObjectsByType.TryGetValue(objectType, out list))
            {
                return list;
            }
            return null;
        }

        public List<IMapObject> FindAllMapObjects(Id_IMapObjectType objectTypeid)
        {
            List<IMapObject> list;
            if (_mapObjectsByTypeId.TryGetValue(objectTypeid, out list))
            {
                return list;
            }
            return null;
        }

        public IMapObject FindMapObject(Id_IMapObjectType objectTypeId)
        {
            List<IMapObject> list;
            if (_mapObjectsByTypeId.TryGetValue(objectTypeId, out list))
            {
                return list.FirstOrDefault();
            }
            return null;
        }

        public void Clear()
        {
            foreach (var mapObject in MapObjects)
            {
                mapObject.RemoveMapObject();
            }
            MapObjects.Clear();
            
            ClearCache();
        }

        #endregion
    }
}
