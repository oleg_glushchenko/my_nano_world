﻿using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;

namespace NanoReality.GameLogic.BuildingSystem.GlobalCityMap.Models
{
    public interface IGlobalCityMap
    {
        List<IMapObject> MapObjects { get; }

        Id<IPlayerProfile> UserId { get; }

        Id<IUserCity> UserCityId { get; }
        
        Id<IBusinessSector> BusinessSectorId { get; }

        void Init(Id<IPlayerProfile> userId, Id<IUserCity> userCityId, Id<IBusinessSector> businessSectorId);
        
        void UpgradeObject(Id_IMapObject mapId);
        
        IMapObject FindMapObject(Id_IMapObject id);
        
        IMapObject FindMapObject(MapObjectintTypes objectType);
        
        IMapObject FindMapObject(Id_IMapObjectType objectTypeId);
        
        List<IMapObject> FindAllMapObjects(MapObjectintTypes objectType);
        
        List<IMapObject> FindAllMapObjects(Id_IMapObjectType objectTypeid);
        
        void Clear();
    }
}
