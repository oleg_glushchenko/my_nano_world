﻿using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
using Newtonsoft.Json;
using UnityEngine;

namespace GameLogic.BuildingSystem.CityArea
{
    public abstract class CitySector : ICitySector
    {
        [JsonProperty("id")]
        public Id<ILockedSector> Id { get; set; }

        [JsonProperty("bss_id")]
        public Id<IBusinessSector> BusinessSectorId { get; set; }

        [JsonProperty("x")]
        public int X { get; set; }

        [JsonProperty("y")]
        public int Y { get; set; }

        [JsonProperty("width")]
        public int Width { get; set; }

        [JsonProperty("height")]
        public int Height { get; set; }

        public Vector2Int LeftBottomGridPos { get; private set; }

        public Vector2Int GridDimensions { get; private set; }

        public bool Contains(Vector2Int point)
        {
            if (point.x < X)
                return false;

            if (point.x >= X + Width)
                return false;

            if (point.y < Y)
                return false;

            if (point.y >= Y + Height)
                return false;

            return true;
        }

        public bool Overlaps(Vector2Int leftBottomPos, Vector2Int dimension)
        {
            if (Contains(leftBottomPos))
                return true;
                
            if (Contains(leftBottomPos + (dimension - Vector2Int.one)))
                return true;
                
            if (Contains(new Vector2Int(leftBottomPos.x + (dimension.x - 1), leftBottomPos.y)))
                return true;
                
            if (Contains(new Vector2Int(leftBottomPos.x , leftBottomPos.y + (dimension.y - 1))))
                return true;

            return false;
        }

        public void Init()
        {
            GridDimensions = new Vector2Int(Width, Height);
            LeftBottomGridPos = new Vector2Int(X, Y);
        }
    }
}
