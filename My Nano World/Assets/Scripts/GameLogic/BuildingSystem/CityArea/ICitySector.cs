﻿using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
using UnityEngine;

namespace GameLogic.BuildingSystem.CityArea
{
    public interface ICitySector
    {
        Id<ILockedSector> Id { get; }
        Id<IBusinessSector> BusinessSectorId { get; }
        
        Vector2Int LeftBottomGridPos { get; }
        
        Vector2Int GridDimensions { get; }

        bool Contains(Vector2Int point);
    }
}
