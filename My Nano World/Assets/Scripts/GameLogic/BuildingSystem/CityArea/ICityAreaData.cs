using System.Collections.Generic;
using GameLogic.BuildingSystem.CityArea;
using NanoReality.GameLogic.StartApplicationSequence.Models.api;

namespace NanoReality.GameLogic.BuildingSystem.CityArea
{
    public interface ICityAreaData : IGameBalanceData
    {
        List<ISectorArea> Areas { get; }
    }
}
