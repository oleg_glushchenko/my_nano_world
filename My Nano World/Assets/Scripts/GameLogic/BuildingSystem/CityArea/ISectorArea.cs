using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using UnityEngine;

namespace GameLogic.BuildingSystem.CityArea
{
    public interface ISectorArea
    {
        Id<IBusinessSector> BusinessSectorId { get; }
        bool IsDisabled { get; }

        bool ContainsBuildingTypeId(Id_IMapObjectType type);

        bool Contains(Vector2Int point);
        
        Vector2Int LeftBottomGridPos { get; }

        Vector2Int GridDimensions { get; }

        void Init();
        bool Overlaps(Vector2Int leftBottomPos, Vector2Int dimension);
    }
}
