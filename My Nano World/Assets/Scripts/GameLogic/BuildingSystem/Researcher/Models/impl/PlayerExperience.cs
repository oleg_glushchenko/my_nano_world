﻿using System;
using Assets.NanoLib.UI.Core.Signals;
using NanoReality.Engine.UI.Views.LevelUpPanel;
using NanoReality.GameLogic.Common;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.UI.Components;
using UnityEngine;

public sealed class PlayerExperience : IPlayerExperience
{
    public int CurrentLevel { get; set; }

    public int CurrentExperience { get; set; }
    
    [Inject] public SignalExperienceUpdate jSignalExperienceUpdate { get; set; }
    [Inject] public ShowWindowSignal jShowWindowSignal { get; set; }
    [Inject] public UserLevelUpSignal jUserLevelUpSignal { get; set; }
    [Inject] public IUserLevelsData ObjectUserLevelsesBalanceData { get; set; }
    [Inject] public GameLoadingModel jGameLoadingModel { get; set; }
    
    public event Action<int> LevelChanged;
    public event Action<int> ExperienceChanged;
    
    public void Init()
    {
        
    }

    public void AddExperience(int experienceToAdd, bool autoLevelUp)
    {
        Debug.LogWarning("AddExperience: " + experienceToAdd);
        if (!autoLevelUp)
        {
            CurrentExperience += experienceToAdd;
            return;
        }

        int nextLevel = GetNextLevel(experienceToAdd);

        if (nextLevel > CurrentLevel)
        {
            CurrentLevel = nextLevel;
            UserLevelUp();
        }
        
        jSignalExperienceUpdate.Dispatch();
    }

    private int GetNextLevel(int experienceToAdd)
    {
        int levelCount = ObjectUserLevelsesBalanceData.Levels.Count - 1;
        
        for(int next = CurrentLevel; next < levelCount; next++)
        {
            int nextLevelExp = ObjectUserLevelsesBalanceData.Levels[next].Experience;

            if (CurrentExperience + experienceToAdd >= nextLevelExp && next < levelCount - 1)
            {
                experienceToAdd -= (nextLevelExp - CurrentExperience);
                CurrentExperience = 0;
            }
            else
            {
                if (CurrentExperience + experienceToAdd > nextLevelExp)
                {
                    CurrentExperience = nextLevelExp;
                }
                else
                {
                    CurrentExperience += experienceToAdd;
                }

                return next;
            }
        }

        return CurrentLevel;
    }

    public void LevelUp(int levelCount)
    {
        var nextLevel = Mathf.Clamp(CurrentLevel + levelCount, 1, ObjectUserLevelsesBalanceData.Levels.Count - 1);
        CurrentLevel = nextLevel;
        UserLevelUp();
    }

    private void UserLevelUp()
    {
        jUserLevelUpSignal.Dispatch();
        LevelChanged?.Invoke(CurrentLevel);
        ExperienceChanged?.Invoke(CurrentExperience);
        
        if (jGameLoadingModel.LoadingState.Value == GameLoadingState.PostLoadingActions)
        {
            return;
        }

        jShowWindowSignal.Dispatch(typeof(LevelUpPanelView), null);
    }
}


//TODO: Рома, где это должно быть?
public enum AwardSourcePlace
{
    None = -1,
    FinishBuilding = 0,
    BuildingRepaired = 1,
    SeaportReward = 2,
    OrderDeskReward = 3,
    AgriculturalHarvest = 4,
    FinishProduction = 5,
    QuestsAwards = 6,
    DailyBonus = 7,
    AchievementReward = 8,
    CinemaReward = 9,
    LevelUp = 10,
    ChestsReward = 11,
    TradeMarketLot = 12,
    RestaurantReward = 13,
    RestaurantReturnBet = 14,
    FoodSupplyReward = 15,
    BuyCurrency = 16,
    MetroReward = 17,
    Tax = 18,
    Purchase = 19,
    Cheating = 100
}

public enum CurrenciesSpendingPlace
{
    None,
    RepairBuilding,
    UpgradeBuilding,
    UnlockSector,
    SkipOrderWaiting,
    ConstructMapObject,
    OrderDeskCompleteOrder,
    GetRestaurantGame,
    SkipRestaurantCooldown,
    BuyoutAuctionLot,
    CloseAuctionLot,
    AddNewAuctionSlot,
    WarehouseBuyProduct,
    SkipBuilding,
    SkipMineSlot,
    SkipFactorySlot,
    UnlockMineSlot,
    UnlockFactorySlot,
    SkipShuffleBazaar,
    SkipShuffleSeaport,
    SkipShuffleMetro,
    SkipWaitingMetro,
    NotEnoughtResourcesBuy,
    BuySoftCurrency,
    SkipGrowingTimer,
    BuyChests,
    BuyBazaarProducts,
}