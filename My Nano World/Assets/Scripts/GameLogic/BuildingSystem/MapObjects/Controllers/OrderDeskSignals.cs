﻿using NanoReality.Game.Services.OrderDesk.SeaportOrderDesk;
using strange.extensions.signal.impl;

namespace NanoReality.GameLogic.Orders
{
    public class SignalSeaportOrderSent : Signal<ISeaportOrderSlot> { }

    public class SignalOnCityOrdersLoadedSignal : Signal
    {
    }

    public class SignalOnOrderTruckSlotLoaded : Signal
    {
    }

    public class SignalOnOrderTruckTripFinished : Signal<int>
    {
    }

    public class SignalOnNeedToGetNewOrder : Signal<int>
    {
    }

    public class SignalOnCityOrderAwardTaken : Signal
    {
    }

    public class SignalOnShowCarInTripElementsEvent : Signal
    {
    }

    public class SignalOnHideCarInTripElementsEvent : Signal
    {
    }

    public class SignalOnResultArrivedAnimationEndsEvent : Signal
    {
    }

    public class SignalOnGetNewOrderAnimationEndsEvent : Signal
    {
    }

    public class SignalOnHideOrderDeskPanelView : Signal
    {
    }
}