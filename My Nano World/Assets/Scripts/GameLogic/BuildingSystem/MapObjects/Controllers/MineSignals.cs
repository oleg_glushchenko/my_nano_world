﻿using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using strange.extensions.signal.impl;

namespace Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Controllers
{
    /// <summary>
    /// Вызывается при отгрузке продукта из слота в склад
    /// </summary>
    public class SignalMineShipSlot : Signal<Id_IMapObject, IProduceSlot> { }

    /// <summary>
    /// Вызывается при окончании разработки продукта
    /// </summary>
    public class SignalOnMineProduceSlotFinished : Signal<Id_IMapObject> { }
}
