﻿using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using strange.extensions.signal.impl;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Controllers.Agricultural
{
	public class OnChangeGrowingState : Signal<IAgroFieldBuilding>
	{
	}

	public class AgriculturalPlantSelectedSignal : Signal<IProducingItemData>
	{
	}

    /// <summary>
    /// Вызывается после сбора продукта (с заводов и шахт тоже)
    /// </summary>
	public class OnProductProduceAndCollectSignal : Signal<IMapObject, Id<Product>, int>  { }


    /// <summary>
    /// Вызывается, когда начинается перетаскивание продукта 
    /// (используется для избежания выделения объекта на карте)
    /// </summary>
    public class OnItemDragStartedSignal : Signal { }

    /// <summary>
    /// Вызывается, когда заканчивается перетаскивание продукта 
    /// (используется для избежания выделения объекта на карте)
    /// </summary>
    public class OnItemDragEndedSignal : Signal { }
}