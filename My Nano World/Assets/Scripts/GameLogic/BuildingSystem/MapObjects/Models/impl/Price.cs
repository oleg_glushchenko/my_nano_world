﻿using System;
using System.Collections.Generic;
using System.Text;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using Newtonsoft.Json;
using UnityEngine;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api
{
    [Serializable]
    public class Price
    {
        [JsonProperty("cost_soft")]
        public int SoftPrice { get; set; }

        [JsonProperty("cost_hard")]
        public int HardPrice { get; set; }
        
        [JsonProperty("cost_gold")]
        public int GoldPrice { get; set; }
        
        [JsonProperty("cost_resources")]
        public Dictionary<Id<Product>, int> ProductsPrice { get; set; }

        [JsonProperty("cost_type")] 
        public PriceType PriceType{ get; set; }

        /// <summary>
        /// If Price use soft currency
        /// </summary>
        public bool IsNeedSoft => PriceType.HasFlag(PriceType.Soft);
    

        /// <summary>
        /// If Price use hard currency
        /// </summary>
        public bool IsNeedHard => PriceType.HasFlag(PriceType.Hard);

        /// <summary>
        /// If Price use product currency
        /// </summary>
        public bool IsNeedProducts => PriceType.HasFlag(PriceType.Resources) || PriceType.HasFlag(PriceType.ListResources);
        
        public bool IsNeedGold => PriceType.HasFlag(PriceType.Gold);

        public int GetPriceValue()
        {
            int resultValue;
            switch (PriceType)
            {
                case PriceType.Soft:
                    resultValue = SoftPrice;
                    break;
                case PriceType.Hard:
                    resultValue = HardPrice;
                    break;
                case PriceType.Gold:
                    resultValue = GoldPrice;
                    break;
                default:
                    Debug.LogWarning($"Wrong usage of Price.GetPrice. PriceType must be only in currency, current: [{PriceType}]");
                    resultValue = 0;
                    break;
            }

            return resultValue;
        }

        public override string ToString()
        {
            var builder = new StringBuilder($"PriceType: {PriceType} Soft: {SoftPrice} Hard: {HardPrice} Gold: {GoldPrice}");
            foreach (KeyValuePair<Id<Product>,int> productsPair in ProductsPrice)
            {
                builder.Append($" Id:{productsPair.Key.Value} Count:{productsPair.Value} ");
            }
            return builder.ToString();
        }
    }
}
