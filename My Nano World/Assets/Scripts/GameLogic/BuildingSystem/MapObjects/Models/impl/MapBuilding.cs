﻿using System;
using Assets.Scripts.Engine.Analytics;
using Assets.Scripts.Engine.Analytics.Extensions.CurrencyInfo;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using Newtonsoft.Json;
using NanoReality.GameLogic.GameManager.Models.api;
using Assets.Scripts.Engine.Analytics.Model;
using System.Collections.Generic;
using Assets.Scripts.Engine.Analytics.Model.api;
using NanoLib.Core.Logging;
using NanoReality.Game.CitizenGifts;
using NanoReality.Game.Services.BuildingsUnlockService;
using NanoReality.GameLogic.PlayerProfile.Models.Impl;
using NanoReality.GameLogic.Services;
using strange.extensions.signal.impl;
using UnityEngine;

namespace Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl
{
    public sealed class MapBuildingSyncSignal : Signal<IMapObject> { }

    public class MapBuilding : MMapObject, IMapBuilding
    {
        #region Injects

        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IBuildingsUnlockService jBuildingsUnlockService { get; set; }
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public ISkipIntervalsData jSkipIntervalsData { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public IBuildingService jBuildingService { get; set; }

        [Inject] public SignalOnActionWithCurrency jSignalOnActionWithCurrency { get; set; }

        [Inject] public SignalOnMovedMapObjectOnServer jSignalOnMovedMapObject { get; set; }
        [Inject] public SignalOnMapBuildingsUpgradeStarted jSignalOnMapBuildingsUpgradeStarted { get; set; }
        [Inject] public SignalOnSkipBuilding jSignalOnSkipBuilding { get; set; }
        [Inject] public SignalOnMapBuildingUpgradeVerificated jSignalOnMapBuildingUpgradeVerificated { get; set; }

        [Inject] public MapBuildingSyncSignal jMapBuildingSyncSignal { get; set; }
        [Inject] public INanoAnalytics jNanoAnalytics { get; set; }

        #endregion

        #region Fields
        
        public override bool IsNeedRoad => false;
        public override bool IsSkipped { get; set; }
        
        /// <summary>
        /// Is Upgrade available for current player.
        /// </summary>
        public virtual bool IsUpgradable => jBuildingsUnlockService.IsUpgradeAvailable(ObjectTypeID, Level + 1, InstanceIndex);

        public virtual bool CanBeUpgraded => IsUpgradable;

        [JsonProperty("lock")]
        public bool IsLocked { get; set; }

        [JsonProperty("products_for_upgrade")]
        public List<IProductForUpgrade> ProductsForUpgrade { get; set; }

        [JsonProperty("unlock_products")]
        public IProductsToUnlock ProductsToUnlock { get; set; }

        [JsonProperty("user_exp_for_up")]
        public int UserExpForConstruct { get; set; }

        public int BuildingUnlockLevel => jBuildingsUnlockService.GetBuildingUnlockLevel(ObjectTypeID);

        public int CurrentBuildTime => currentBuildTime;

        private List<CitizenGift> _citizenGifts;
        public List<CitizenGift> AvailableGifts
        {
            get => _citizenGifts;
            set
            {
                _citizenGifts = value;
                CitizenGiftChanged?.Invoke();
            }
        }

        public event Action CitizenGiftChanged;
        
        #endregion

        #region Methods

        public override object Clone()
        {
            var result = Binder.GetInstance<IMapBuilding>();
            CopyMembersTo(result);
            return result;
        }

        public override void MoveMapObjectVerificated(MoveMapObjectResponce result)
        {
            SyncServerModel(result.MapObject);
            jSignalOnMovedMapObject.Dispatch (this);
        }
        
        public override void UpgradeObject(int upgradeTime)
        {
            MapBuilding nextLevelBuildingModel = jMapObjectsData.GetNextBuildingLevelData(this);
            if (nextLevelBuildingModel == null)
            {
                return;
            }
            
            Level++;

            if (MapObjectType != MapObjectintTypes.DwellingHouse && MapObjectType != MapObjectintTypes.EventBuilding)
            {
                jServerCommunicator.UpgradeMapObject(MapID, OnUpgradeMapObjectSuccess);

                var upgradePrice = jBuildingsUnlockService.GetUpgradeBuildingPrice(ObjectTypeID, Level, InstanceIndex);
                if (upgradePrice != null)
                {
                    jPlayerProfile.PlayerResources.BuyItem(upgradePrice);
                }
            }

            base.UpgradeObject(upgradeTime);
            jSignalOnMapBuildingsUpgradeStarted.Dispatch(nextLevelBuildingModel);
        }

        private void OnUpgradeMapObjectSuccess()
        {
            jSignalOnMapBuildingUpgradeVerificated.Dispatch(this);
        }
        
        protected override void FinishBuildingLocal()
        {
            jBuildingService.FinalizeMapObjectConstruction(this, BuildFinishVerified);
            base.FinishBuildingLocal();
        }

        protected override void BuildFinishVerified(SyncBuildingResponse result)
        {
            SyncServerModel(result.Building);
            base.BuildFinishVerified(result);
        }

        public void VerifyBuilding()
        {
            if (!IsVerificated)
            {
                Debug.LogWarning("Building is verifying right now. Can't verify it twice.");
                return;
            }

            jBuildingService.SyncBuilding(this, OnBuildingVerified);
        }

        protected virtual void OnBuildingVerified(SyncBuildingResponse result)
        {
            try
            {
                SyncServerModel(result.Building);
            }
            catch (Exception e)
            {
                Logging.LogException(e);
            }
        }
        
        public void SkipConstructionTimer()
        {
            int skipPrice = jSkipIntervalsData.GetCurrencyForTime(currentBuildTime);
            
            //уведомляем сервер о скипе
            jServerCommunicator.BuildingSkip(GetMapIDString, () => { });

            // не ждем ответа от сервера, для оптимизации задержек
            jPlayerProfile.PlayerResources.BuyHardCurrency(skipPrice);
            jNanoAnalytics.OnPlayerCurrenciesDecrease(PlayerResources.CurencyType.Bucks, skipPrice, CurrenciesSpendingPlace.SkipBuilding,
                ObjectTypeID.Value.ToString());

            jSignalOnActionWithCurrency.Dispatch(new HardInfo(CurrencyConsumeActions.SkipBuilding,
                new Price {HardPrice = skipPrice}, Name));

            IsSkipped = true;

            BuildTimer?.CancelTimer();
            BuildTimer = null;

            // если строительство не закончилось по таймеру 
            if (currentBuildTime > 0)
            {
                // производим окончание строительства
                FinishBuildingLocal();
            }

            // аналитика 
            jSignalOnSkipBuilding.Dispatch(new SkipBuildingInfo(BalanceName, ObjectTypeID.Value, Level, skipPrice));
        }

        /// <summary>
        /// Возвращает true, если все условия для работы здания соблюдены
        /// </summary>
        /// <returns>true, если все условия для работы здания соблюдены</returns>
        public virtual bool IsFunctional()
        {
            return IsVerificated && IsConstructFinished && (!IsNeedRoad || (IsNeedRoad && IsConnectedToGeneralRoad)) &&
                   jPlayerProfile.CurrentLevelData.CurrentLevel >= BuildingUnlockLevel;
        }

        protected void SyncServerModel(IMapObject model)
        {
            if (model is IMapBuilding mapBuilding)
            {
                SyncServerModel(mapBuilding);
                jMapBuildingSyncSignal.Dispatch(model);
            }
            else
            {
                Debug.LogError("SyncServerModel has failed due to incorrect model class type: " + model);
            }
        }

        protected virtual void SyncServerModel(IMapBuilding model)
        {
            if (model == null)
            {
                return;
            }

            IsVerificated = true;
            IsLocked = model.IsLocked;
            ProductsToUnlock = model.ProductsToUnlock;
            ProductsForUpgrade = model.ProductsForUpgrade;
            IsConnectedToGeneralRoad = model.IsConnectedToGeneralRoad;
        }

        public override void MoveMapObject(Vector2Int oldPosition, Vector2Int newPosition)
        {
            base.MoveMapObject(oldPosition, newPosition);
            IsVerificated = false;
        }

        #endregion
    }
}
