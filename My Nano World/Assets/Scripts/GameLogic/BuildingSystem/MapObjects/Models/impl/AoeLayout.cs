namespace NanoReality.GameLogic.BuildingSystem.MapObjects
{
    /// <summary>
    /// Лаяуты расположения эффекта по площади относительно здания источника
    /// </summary>
    public enum AoeLayout
    {
        None = 0,
        TopLeft = 4,
        BottomLeft = 3,
        TopRight = 2,
        BottomRight = 1,
        Center = 5
    }
}