using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.TheEvent.Signals;
using Newtonsoft.Json;

public class EventBuilding : MapBuilding, IEventBuilding
{
    [Inject] public SignalDisableAttentionViewOnEventBuilding jSignalDisableAttentionViewOnEventBuilding { get; set; }

    [JsonProperty("slot_skip_price")]
    public int LanternPrice { get; set; }

    [JsonProperty("can_upgrade")]
    public bool ReadyToUpgrade { get; set; }

    public float SecondsToNextEventDay { get; set; }

    protected override void SyncServerModel(IMapBuilding model)
    {
        base.SyncServerModel(model);
        IEventBuilding updatedEventBuilding = (IEventBuilding)model;
        ReadyToUpgrade = updatedEventBuilding.ReadyToUpgrade;
        LanternPrice = updatedEventBuilding.LanternPrice;
        if (!ReadyToUpgrade)
        {
            jSignalDisableAttentionViewOnEventBuilding.Dispatch(true);
        }
    }
}
