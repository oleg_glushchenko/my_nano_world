﻿using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Quests;

namespace Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl
{
    public class MQuestsBuilding : MapBuilding, IQuestsBuilding
    {
        [Inject]
        public IUserQuestData jUserQuestData { get; set; }
        
        public override MapObjectintTypes MapObjectType
        {
            get { return MapObjectintTypes.Quests; }
            set { }
        }

        public BusinessSectorsTypes QuestsSectorType
        {
            get
            {
                var sectorType = SectorId.ToBusinessSectorType();
                // There are no quests marked for global so far, so we try to get other supported sector type
                if (sectorType == BusinessSectorsTypes.Global)
                {
                    for (int i = 0; i < SectorIds.Count; i++)
                    {
                        sectorType = SectorIds[i];
                        if (sectorType != BusinessSectorsTypes.Global)
                        {
                            return sectorType;
                        }
                    }
                }
                
                return sectorType;
            }
        }

        public int AchievedQuestsCount
        {
            get { return jUserQuestData.GetAchievedQuestsCount(QuestsSectorType); }
        }

        public int ActiveQuestsCount
        {
            get { return jUserQuestData.GetActiveQuestsCount(QuestsSectorType); }
        }

        public int FirstAchievedQuestGiftBoxType
        {
            get
            {
                return jUserQuestData.FirstAchievedQuestGiftBoxType(QuestsSectorType);                
            }
            
        }

        public override object Clone()
        {
            var museum = Binder.GetInstance<IQuestsBuilding>();
            CopyMembersTo(museum);
            return museum;
        }
    }
}
