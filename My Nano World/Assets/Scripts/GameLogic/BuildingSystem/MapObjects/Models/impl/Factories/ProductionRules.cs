﻿using System.Collections.Generic;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using Newtonsoft.Json;

// ReSharper disable once CheckNamespace

namespace NanoReality.GameLogic.Production
{
    /// <summary>
    /// Правила продакшна
    /// </summary>
    public class ProductionRules 
    {


        /// <summary>
        /// Доступные для изготовления продукты
        /// </summary>
        [JsonProperty("available_producing_goods")]
        public List<IProducingItemData> AvailableProducingGoods { get; set; }

        /// <summary>
        /// Цена премиум покупки слота
        /// </summary>
        [JsonProperty("slot_hard_cost")]
        public int SlotPremiumPrice { get; set; }

        [JsonProperty("available_module")]
        public List<string> AvailableModule { get; set; }

        public ProductionRules()
        {

            AvailableProducingGoods = new List<IProducingItemData>();

            AvailableModule = new List<string>();
        }
    }
}