﻿using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Impl
{
    public class MRoad : MapBuilding, IRoad
    {
        public override MapObjectintTypes MapObjectType
        {
            get => MapObjectintTypes.Road;
            set { }
        }

        public override bool IsUpgradable { get; } = false;
        public override bool CanBeUpgraded { get; } = false;

        protected override void FinishBuildingLocal()
        {
            FinishBuilding();
        }

        protected override void FinishBuilding()
        {
            EventOnMapObjectBuildFinished.Dispatch(this);
        }

        public override object Clone()
        {
            var result = Binder.GetInstance<IRoad>();
            CopyMembersTo(result);

            return result;
        }
    }
}