﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.Utilities.DataManager.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.StartApplicationSequence.Models.api;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Models.impl
{
    /// <summary>
    /// Гейм. баланс данные о скипе времени постройки за премиум
    /// </summary>
    public class MapBuildingsSkipData : IMapBuildingsSkipData
    {
        /// <summary>
        /// Словарь временных точек скипа ключ время, значение - стоимость скипа в премиуме
        /// </summary>
        public Dictionary<int, int> SkipData { get; set; }

        public int GetNearesSkipPrice(float time)
        {
            if (SkipData == null) return 0;
            foreach (var i in SkipData)
            {
                if (time <= i.Key)
                    return i.Value;
            }
            return SkipData.Last().Value;
        }

        public string DataHash { get; set; }

        public void LoadData(IServerCommunicator serverCommunicator, ILocalDataManager localDataManager, Action<IGameBalanceData> callback)
        {
            serverCommunicator.LoadMapBuildingsPremiumSkipData(this, data =>
            {
                SkipData = data.SkipData;
                DataHash = data.DataHash;
                callback(this);
            });
        }
    }
}
