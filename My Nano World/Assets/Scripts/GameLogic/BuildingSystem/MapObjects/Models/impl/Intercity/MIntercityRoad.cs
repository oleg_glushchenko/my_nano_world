﻿using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Models.impl
{
    public class MIntercityRoad : MapBuilding, IIntercityRoad
    {
        #region Overrides of MMapObject

        public override MapObjectintTypes MapObjectType
        {
            get { return MapObjectintTypes.IntercityRoad; }
            set { }
        }

        #endregion

        #region Overrides of MapBuilding

        public override object Clone()
        {
            var result = Binder.GetInstance<IIntercityRoad>();
            CopyMembersTo(result);
            return result;
        }

        #endregion
    }
}