﻿using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using Newtonsoft.Json;

namespace Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.api.OrderDesks
{
    public class OrderDeskMapBuilding : MapBuilding, IOrderDeskMapBuilding
    {
        [JsonIgnore]
        public override bool IsNeedRoad => false;

        [JsonIgnore]
        public override MapObjectintTypes MapObjectType => MapObjectintTypes.OrderDesk;

        public BusinessSectorsTypes OrderDeskSectorType
        {
            get
            {
                var sectorType = SectorId.ToBusinessSectorType();
                if (sectorType != BusinessSectorsTypes.Global) return sectorType;
                // HACK! There are no order desks marked for global so far, so we try to get other supported sector type
                for (var i = 0; i < SectorIds.Count; i++)
                {
                    sectorType = SectorIds[i];
                    if (sectorType != BusinessSectorsTypes.Global)
                    {
                        return sectorType;
                    }
                }
                return sectorType;
            }
        }

        public override object Clone()
        {
            var result = Binder.GetInstance<IOrderDeskMapBuilding>();
            CopyMembersTo(result);
            return result;
        }

        public override bool IsFunctional()
        {
            return true;
        }
    }
}
