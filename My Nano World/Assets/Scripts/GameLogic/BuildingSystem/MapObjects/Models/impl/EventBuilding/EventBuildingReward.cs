using System;
using System.Linq;
using Assets.NanoLib.UtilitesEngine;
using Assets.Scripts.Engine.Analytics.Model.api;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl;
using NanoReality.Engine.Utilities;
using NanoReality.Game.CitizenGifts;
using NanoReality.Game.CityStatusEffects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using Newtonsoft.Json;
using UnityEngine;

public class EventBuildingReward : MapBuilding, IEventBuildingReward
{
    [JsonProperty("next_reward_time")]
    public long NextRewardTime { get; set; }

    [JsonProperty("status_effects")]
    public EventStatusEffect[] Effects { get; set; }

    [Inject] public ICityGiftsService jCityGiftsService { get; set; }
    [Inject] public INanoAnalytics jNanoAnalytics { get; set; }

    private ITimer _timer;

    public override void OnInitOnMap(IMapObject prototype)
    {
        base.OnInitOnMap(prototype);
        jMapBuildingSyncSignal.AddListener(OnSync);
    }

    public int TaxBonus()
    {
        if (Effects == null)
        {
            return 0;
        }

        EventStatusEffect effect = Effects.First(eventStatusEffect => eventStatusEffect.Influence == StatusEffectInfluence.Increase);
        return effect.Value;
    }
 
    protected override void BuildFinishVerified(SyncBuildingResponse result)
    {
        base.BuildFinishVerified(result);
        jNanoAnalytics.OnPlaceOnTheMapRewardedBuilding();
        jCityGiftsService.LoadAvailableGifts(null);
    }

    private void StartTimerToUpdateGifts(long nextRewardTime)
    {
        _timer?.CancelTimer();
        var dateTime = DateTimeUtils.UnixStartTime.AddSeconds(nextRewardTime).ToLocalTime();
        var secondsLeft = (dateTime - DateTime.Now).TotalSeconds;
        _timer = jTimerManager.StartServerTimer((float)secondsLeft, OnTimerFinish, null);
    }

    private void OnTimerFinish()
    {
        _timer?.CancelTimer();
        jCityGiftsService.LoadAvailableGifts(null);
    }

    private void OnSync(IMapObject model)
    {
        if (model is IEventBuildingReward buildingReward)
        {
            NextRewardTime = buildingReward.NextRewardTime;
            Effects ??= buildingReward.Effects;
            StartTimerToUpdateGifts(buildingReward.NextRewardTime);
        }
    }

    public override object Clone()
    {
        var result = Binder.GetInstance<IEventBuildingReward>();
        CopyMembersTo(result);
        return result;
    }

    public override void RemoveMapObject()
    {
        base.RemoveMapObject();
        jMapBuildingSyncSignal.RemoveListener(OnSync);
    }

    [SerializeField]
    public class EventStatusEffect
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("effect_type")]
        public StatusEffect StatusEffect { get; set; }

        [JsonProperty("influence_type")]
        public StatusEffectInfluence Influence { get; set; }

        [JsonProperty("value")]
        public int Value { get; set; }
    }
}
