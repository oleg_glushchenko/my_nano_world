﻿using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using Newtonsoft.Json;

public class ProductForUpgrade : IProductForUpgrade
{
    [JsonProperty("id")]
    public Id<Product> ProductID { get; set; }

    [JsonProperty("count")]
    public int ProductsCount { get; set; }

    [JsonProperty("done")]
    public bool IsLoaded { get; set; }
}