﻿using System;
using System.Collections.Generic;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl;
using GameLogic.BuildingSystem.CityAoe;
using NanoLib.Core.Services.Sound;
using NanoReality.Engine.UI.Extensions.EditModePanels;
using NanoReality.GameLogic.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using Newtonsoft.Json;
using UniRx;
using UnityEngine;

namespace GameLogic.BuildingSystem.MapObjects.Models.impl
{
    public abstract class AoeBuilding : MapBuilding, IAoeBuilding
    {
        private IDisposable _disposableLayoutRequestDelay;

        [JsonProperty("aoe_size")] 
        public Vector2 AoeSize { get; set; }
        [JsonProperty("aoe_type")] 
        public AoeLayout AoeLayoutType { get; set; }

        [JsonProperty("aoe_connect")]
        public List<int> AoeConnected { get; set; }
        
        public override bool IsNeedRoad => true;
        protected abstract bool IsEnergy { get; }

        public event Action<IAoeBuilding> AoeLayoutChanged;
        
        [Inject] public ICityAoeService jCityAoeService { get; set; }
        [Inject] public SignalOnBuildingElectricityChanged jSignalOnBuildingElectricityChanged { get; set; }
        [Inject] public IEditModeService jEditModeService { get; set; }

        public abstract bool IsCoveringBuilding(IMapObject mapObject);
        
        public override void OnInitOnMap(IMapObject prototype)
        {
            base.OnInitOnMap(prototype);

            if (AoeConnected == null)
            {
                AoeConnected = new List<int>();
            }
        }

        public void SwitchAoeLayout(AoeLayout newType)
        {
            AoeLayoutType = newType;
            jSoundManager.Play(SfxSoundTypes.ChangeAoeType, SoundChannel.SoundFX);
            _disposableLayoutRequestDelay?.Dispose();
            _disposableLayoutRequestDelay = Observable.Timer(TimeSpan.FromSeconds(1.0f)).Subscribe(SendAoeLayoutRequest);
            
            AoeLayoutChanged?.Invoke(this);
        }

        public override void MoveMapObjectVerificated(MoveMapObjectResponce result)
        {
            base.MoveMapObjectVerificated(result);
            jCityAoeService.LoadAoeDataWithDelay(1.0f);
        }
        
        public override void InitFromPrototype(IMapObject prototype)
        {
            base.InitFromPrototype(prototype);
            
            var casted = (AoeBuilding)prototype;
            AoeSize = casted.AoeSize;
        }

        protected override void CopyMembersTo(IMapObject result)
        {
            base.CopyMembersTo(result);

            var casted = (AoeBuilding)result;
            casted.AoeLayoutType = AoeLayoutType;
            casted.AoeSize = AoeSize;
        }

        protected override void OnBuildingVerified(SyncBuildingResponse result)
        {
            base.OnBuildingVerified(result);
            IsVerificated = false;
            jCityAoeService.LoadAoeData(() =>
            {
                IsVerificated = true;
            });
        }
        
        protected override void SyncServerModel(IMapBuilding model)
        {
            base.SyncServerModel(model);
            
            var building = (AoeBuilding)model;
            
            AoeSize = building.AoeSize;
            AoeConnected = building.AoeConnected;
            
            if (_disposableLayoutRequestDelay == null)
            {
                if (building.AoeLayoutType != AoeLayoutType)
                {
                    Debug.LogWarning($"SyncServerModel [{MapID.Value}] server AOE type different from client. Current: {AoeLayoutType}, Server: {building.AoeLayoutType}");
                }
                
                AoeLayoutType = building.AoeLayoutType;
                if (building.AoeLayoutType == AoeLayout.None)
                {
                    Debug.LogError("Not valid Aoe Type.");
                }
            }
        }

        protected override void BuildFinishVerified(SyncBuildingResponse result)
        {
            base.BuildFinishVerified(result);
            jCityAoeService.LoadAoeData();
        }

        private void SendAoeLayoutRequest(long time)
        {
            _disposableLayoutRequestDelay?.Dispose();
            _disposableLayoutRequestDelay = null;

            IsVerificated = false;

            if (jEditModeService.IsEditModeEnable)
            {
                jEditModeService.ChangeAOE(MapID, (int) AoeLayoutType, (d) =>
                {
                    MoveMapObjectResponce moveMapObject = new MoveMapObjectResponce {MapObject = this};
                    AoeSwitched(moveMapObject);
                });
                return;
            }

            jServerCommunicator.SwitchAoeLayoutType(GetMapIDString, AoeLayoutType, IsEnergy, AoeSwitched);
        }

        private void AoeSwitched(MoveMapObjectResponce response)
        {
            foreach (int connectedBuilding in AoeConnected)
            {
                var affectedBuilding = jGameManager.FindMapObjectById<IMapBuilding>(connectedBuilding);
                if (affectedBuilding is IBuildingWithElectricity productionBuilding)
                {
                    productionBuilding.IsEnabledEnergy = false;
                    jSignalOnBuildingElectricityChanged.Dispatch(productionBuilding, false);
                }
            }
            jSignalOnMovedMapObject.Dispatch(this);
            SyncServerModel(response.MapObject);
            IsVerificated = false;
            jCityAoeService.LoadAoeData(() =>
            {
                    IsVerificated = true;
            });
        }
    }
}
