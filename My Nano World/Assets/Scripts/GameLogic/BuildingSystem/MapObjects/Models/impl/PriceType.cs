﻿using System;
using Newtonsoft.Json;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api
{
    [Flags]
    [JsonConverter(typeof(SerializationConverters.PriceTypeConverter))]
    public enum PriceType
    {
        None = 0,
        Soft = 1, 
        Hard = 2, 
        Resources=4, 
        ListResources = 8, //TODO: wtf?
        Gold = 16,
        Lantern = 17
    }
}