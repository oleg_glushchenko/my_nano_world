﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.Analytics;
using Assets.Scripts.Engine.Analytics.Extensions.CurrencyInfo;
using Assets.Scripts.Engine.Analytics.Model;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.api;
using GameLogic.BuildingSystem.CityAoe;
using NanoLib.Core.Logging;
using NanoLib.Core.Services.Sound;
using NanoReality.Game.Production;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Controllers.Agricultural;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Controllers.Factories;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using NanoReality.GameLogic.Configs;
using NanoReality.GameLogic.PlayerProfile.Models.Impl;
using NanoReality.GameLogic.Production;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using Newtonsoft.Json;
using UnityEngine;

namespace GameLogic.BuildingSystem.MapObjects.Models.impl
{
    public abstract class ProductionBuilding : MapBuilding
    {
        private const int InitializeCollectionSize = 10;
        
        [JsonProperty("produce_slot")]
        public List<IProduceSlot> CurrentProduceSlots { get; set; }
        
        [JsonProperty("slots_available")]
        public int AvailableSlotsCount { get; set; }

        [JsonProperty("rules")]
        public ProductionRules ProductionRules = new ProductionRules();

        [JsonProperty("free_slots")]
        public int FreeSlotsCount { get; set; }

        public List<IProducingItemData> AvailableProducingGoods
        {
            get => ProductionRules.AvailableProducingGoods;
            set => ProductionRules.AvailableProducingGoods = value;
        }
        
        public override bool IsNeedRoad => true;

        public int MaximalSlotsCount { get; set; }
        
        public List<SlotData> SlotData { get; private set; }
        
        public bool IsProductionEmpty() => CurrentProduceSlots.All(slot => slot.IsEmpty);
        public bool IsProduced() => CurrentProduceSlots.Any(x => x.CurrentProducingTime > 0);

        public bool HasProductsToCollect() => CurrentProduceSlots.Any(slot => slot.IsFinishedProduce);
        public bool HasEmptySlot() => CurrentProduceSlots.Any(s => s.IsEmpty && s.IsUnlocked && !s.IsWaitingResponse);

        public event Action<IProduceSlot, IProducingItemData> StartedProduction;
        public event Action<IProduceSlot, IProducingItemData> OnConfirmFinishProduction;
        public event Action<IProduceSlot, IProducingItemData> OnFinishProduction;
        public event Action<IProduceSlot, IProducingItemData> OnSlotShipped;

        [Inject] public IBuildingSlotsData jBuildingSlotsData { get; set; }
        [Inject] public IProductionService jProductionService { get; set; }
        [Inject] public ICityAoeService jCityAoeService { get; set; }
        [Inject] public IProductsData jProductsData { get; set; }
        [Inject] public ProductReadySignal jProductReadySignal { get; set; }
        [Inject] public SignalOnBuildingElectricityChanged jSignalOnBuildingElectricityChanged { get; set; }
        [Inject] public OnProductProduceAndCollectSignal jOnProductProduceAndCollectSignal { get; set; }
        [Inject] public SignalOnStartProduceGoodOnFactory jSignalOnStartProduceGoodOnFactory {get; set;}
        [Inject] public SignalOnSkipProduction jSignalOnSkipProduction { get; set; }
        [Inject] public StartProductionSignal jStartProductionSignal { get; set; }

        public ProductionBuilding()
        {
            CurrentProduceSlots = new List<IProduceSlot>(InitializeCollectionSize);
            AvailableProducingGoods = new List<IProducingItemData>(InitializeCollectionSize);
        }
        
        public override void InitializeBuildingTimers()
        {
            AddMissingSlots();

            base.InitializeBuildingTimers();

            foreach (IProduceSlot slot in CurrentProduceSlots)
            {
                if (slot.ResumeProduceGood())
                {
                    slot.OnFinishProduceGood.AddListener(OnFinishProduceGoodLocal);
                    StartedProduction?.Invoke(slot, slot.ItemDataIsProducing);
                }
                bool isPostInitProduceEnded = slot.CurrentProducingTime <= 0 && !slot.IsFinishedProduce && slot.ItemDataIsProducing != null;
                if (isPostInitProduceEnded)
                {
                    VerifyProduceGood(slot, slot.ItemDataIsProducing);
                }
            }
        }
        
        public void StartProduceGood(IProducingItemData producingItemData, IProduceSlot produceSlot, Action<int> onSuccess = null)
        {
            if (produceSlot == null || produceSlot.IsWaitingResponse)
            {
                return;
            }

            if (!HasEmptySlot())
            {
                Logging.LogError($"StartProduceGood failed, there is no available slots, SlotId {produceSlot.SlotId}");
                return;
            }
            
            var newProducingData = (IProducingItemData)producingItemData.Clone();

            produceSlot.IsWaitingResponse = true;

            RemoveResources(newProducingData);
            
            float time = SectorId.Value == 5
                ? Mathf.CeilToInt(producingItemData.ProducingTime)
                : jProductionService.GetProductionTimeMultiplier(newProducingData);
            
            StartProduceGoodLocal(newProducingData, time + QueueProductionTime(), produceSlot);

            jServerCommunicator.StartProduceGood(GetMapIDString, produceSlot.SlotId, newProducingData.OutcomingProducts,
                (id, t) =>
                {
                    produceSlot.Id = id;
                    produceSlot.IsWaitingResponse = false;
                    jStartProductionSignal.Dispatch(produceSlot);
                    onSuccess?.Invoke(MapID);
                });
            
            jSignalOnStartProduceGoodOnFactory.Dispatch(new ProductionStartInfo(ObjectTypeID.Value, BalanceName, newProducingData));
            StartedProduction?.Invoke(produceSlot, newProducingData);
        }

        public virtual void SkipSlot(IProduceSlot produceSlot, Action<IProduceSlot> callback)
        {
            if (produceSlot.CurrentProducingTime <= 0 || produceSlot.Id == 0)
            {
                return;
            }

            if (produceSlot.IsWaitingResponse)
            {
                Logging.LogWarning($"Can't skip  slot, it's waiting network response. SlotId {produceSlot.SlotId}");
                return;
            }
            
            produceSlot.IsWaitingResponse = true;

            int price = jHardTutorial.IsTutorialCompleted ? 
                jSkipIntervalsData.GetCurrencyForTime((int) produceSlot.CurrentProducingTime) : 0;
            jPlayerProfile.PlayerResources.BuyHardCurrency(price);
            jNanoAnalytics.OnPlayerCurrenciesDecrease(PlayerResources.CurencyType.Bucks, price, CurrenciesSpendingPlace.SkipFactorySlot,
                produceSlot.SlotId.ToString());

            produceSlot.OnFinishProduceGood.RemoveListener(OnFinishProduceGoodLocal);
            produceSlot.SkipTimer();

            IProducingItemData producedProduct = produceSlot.ItemDataIsProducing;
            int producedProductId = producedProduct.OutcomingProducts;
            jPlayerProfile.PlayerResources.AddProduct(producedProductId, producedProduct.Amount);

            int experienceToAdd = jProductsData.GetProduct(producedProductId).ExperienceUp;
            jPlayerProfile.CurrentLevelData.AddExperience(experienceToAdd, true);
            jNanoAnalytics.OnExperienceUpdate(AwardSourcePlace.FinishProduction, produceSlot.SlotId.Value.ToString(), experienceToAdd);

            jServerCommunicator.SkipFactoryProduction(GetMapIDString, produceSlot.GetSlotId, _ =>
            {
                produceSlot.IsWaitingResponse = false;
                produceSlot.Id = default;
                callback?.Invoke(produceSlot);
            });
            
            var skipInfo = new SkipProductionInfo(BalanceName, ObjectTypeID.Value.ToString(),
                producedProduct, price);

            OnSlotShipped?.Invoke(produceSlot, producedProduct);
            jSignalOnSkipProduction.Dispatch(skipInfo);
            
            produceSlot.ItemDataIsProducing = null;
            jOnProductProduceAndCollectSignal.Dispatch(this, producedProductId, producedProduct.Amount);
        }

        public void ShipProduct(IProduceSlot produceSlot, Action<IProduceSlot> onSuccess)
        {
            produceSlot.IsWaitingResponse = true;

            IProducingItemData producingData = produceSlot.ItemDataIsProducing;
            int productId = producingData.OutcomingProducts;
            int productAmount = producingData.Amount;
            
            jPlayerProfile.PlayerResources.AddProduct(productId, productAmount);

            int experienceToAdd = jProductsData.GetProduct(productId).ExperienceUp;
            jPlayerProfile.CurrentLevelData.AddExperience(experienceToAdd, true);
            jNanoAnalytics.OnExperienceUpdate(AwardSourcePlace.FinishProduction, 
                produceSlot.SlotId.Value.ToString(), experienceToAdd);

            jServerCommunicator.ShipProductFromFactory(produceSlot.GetId, () =>
            {
                produceSlot.IsWaitingResponse = false;
                produceSlot.Id = default;
                onSuccess?.Invoke(produceSlot);
            });
            
            //shipoSound
            OnSlotShipped?.Invoke(produceSlot, producingData);
            
            produceSlot.ItemDataIsProducing = null;
            jOnProductProduceAndCollectSignal.Dispatch(this, productId, productAmount);
        }
        
        public virtual void UnlockSlot(IProduceSlot produceSlot)
        {
            jServerCommunicator.UnlockFactorySlot(GetMapIDString, slotsCount => { });
            jSoundManager.Play(SfxSoundTypes.AddNewSlot, SoundChannel.SoundFX);
            AvailableSlotsCount++;
            produceSlot.IsUnlocked = true;

            AddMissingSlots();
            jPlayerProfile.PlayerResources.BuyHardCurrency(produceSlot.UnlockPremiumPrice);
            jNanoAnalytics.OnPlayerCurrenciesDecrease(PlayerResources.CurencyType.Bucks, produceSlot.UnlockPremiumPrice,
                CurrenciesSpendingPlace.UnlockFactorySlot, produceSlot.SlotId.Value.ToString());
            jNanoAnalytics.UnlockSlotEvent(MapObjectType.ToString(), produceSlot.SlotId.Value, ObjectTypeID.Value.ToString());
            jSignalOnActionWithCurrency.Dispatch(new HardInfo(CurrencyConsumeActions.SlotUnlock, 
                new Price {HardPrice = produceSlot.UnlockPremiumPrice}, Name));
        }

        protected void OnFinishProduceGoodLocal(IProduceSlot sender, IProducingItemData producingItemData)
        {
            VerifyProduceGood(sender, producingItemData);
        }

        public override void MoveMapObjectVerificated(MoveMapObjectResponce result)
        {
            base.MoveMapObjectVerificated(result);
            jCityAoeService.LoadAoeDataWithDelay(1.0f);
        }

        protected abstract float QueueProductionTime();

        protected abstract void FinishVerifyProduceGood(IProduceSlot sender);

        protected virtual void RemoveResources(IProducingItemData cachedStartProducingItemData)
        {
            jPlayerProfile.PlayerResources.RemoveProducts(cachedStartProducingItemData.IncomingProducts);
        }
        
        protected void StartProduceGoodLocal(IProducingItemData producingData, float timeToProduce, IProduceSlot produceSlot)
        {
            produceSlot.OnFinishProduceGood.AddListener(OnFinishProduceGoodLocal);
            produceSlot.IsFinishedProduceConfirm = false;

            produceSlot.StartProduceGood(producingData, timeToProduce);
            produceSlot.MakeFirstFakeTick();
        }

        protected void VerifyProduceGood(IProduceSlot sender, IProducingItemData producingItemData)
        {
            jServerCommunicator.VerifyProduceGood(sender.GetId,
                time =>
                {
                    // For cases, when game try to verify already collected on client side slot products.
                    if (sender.Id == 0)
                    {
                        return;
                    }
                    
                    if (time > 0)
                    {
                        Logging.LogError($"SlotTime Server:{time} Client:{sender.CurrentProducingTime}.");

                        sender.CurrentProducingTime = time;
                        sender.ItemDataIsProducing = producingItemData;
                        sender.ResumeProduceGood();
                    }
                    else
                    {
                        sender.IsFinishedProduceConfirm = true;
                        sender.CurrentProducingTime = 0;
                        sender.OnFinishProduceGood.RemoveListener(OnFinishProduceGoodLocal);
                        sender.ClearSlot();

                        FinishVerifyProduceGood(sender);
                        jSoundManager.Play(SfxSoundTypes.ProductionFinished, SoundChannel.SoundFX);
                        OnConfirmFinishProduction?.Invoke(sender, producingItemData);
                    }
                });
            OnFinishProduction?.Invoke(sender, producingItemData);

            jProductReadySignal.Dispatch(sender, MapID);
        }

        private void AddMissingSlots()
        {
            for (int i = 1; i <= AvailableSlotsCount; i++)
            {
                if (CurrentProduceSlots.Any(s => s.SlotId == i))
                    continue;

                CurrentProduceSlots.Add(CreateSlot(i));
            }
            if (AvailableSlotsCount < MaximalSlotsCount)
            {
                int id = AvailableSlotsCount + 1;
                if (CurrentProduceSlots.Any(s => s.SlotId == id))
                    return;
                
                IProduceSlot slot = CreateSlot(id, false);
                slot.UnlockPremiumPrice = CalculateSlotUnlockPrice();
                slot.IsUnlocked = false;
                CurrentProduceSlots.Add(slot);
            }
        }
        
        public int CalculateSlotUnlockPrice()
        {
            return AvailableSlotsCount < SlotData.Count ? SlotData[AvailableSlotsCount].Price : -1;
        }

        protected override void FinishBuilding()
        {
            base.FinishBuilding();

            if (Level != 0)
            {
                IProduceSlot lastCurrentProduceSlot = CurrentProduceSlots[CurrentProduceSlots.Count - 1];
                AvailableSlotsCount = Mathf.Clamp(AvailableSlotsCount + FreeSlotsCount, 0, MaximalSlotsCount);

                if (lastCurrentProduceSlot.IsUnlocked && CurrentProduceSlots.Count < MaximalSlotsCount)
                {
                    var newSlot = Binder.GetInstance<IProduceSlot>();
                    newSlot.SlotId = new Id<IProduceSlot>(AvailableSlotsCount);
                    newSlot.IsUnlocked = true;

                    CurrentProduceSlots.Add(newSlot);
                }
                else if (!lastCurrentProduceSlot.IsUnlocked)
                {
                    IProduceSlot produceSlot = CurrentProduceSlots.FirstOrDefault(s => !s.IsUnlocked);
                    if(produceSlot == null) return;
                    produceSlot.IsUnlocked = true;

                    AddMissingSlots();
                }
            }
            else
            {
                AddMissingSlots();
            }
        }
       
        public override void InitFromPrototype(IMapObject prototype)
        {
            base.InitFromPrototype(prototype);

            var buildingsData = jBuildingSlotsData.BuildingSlotsDataList;
            foreach (var buildingSlotData in buildingsData.Where(buildingSlotData => buildingSlotData.BuildingId == ObjectTypeID))
            {
                SlotData = buildingSlotData.SlotData;
            }
                
            ProductionBuilding productionBuilding = (ProductionBuilding)prototype;
            MaximalSlotsCount = SlotData.Count;
            ProductionRules.SlotPremiumPrice = productionBuilding.ProductionRules.SlotPremiumPrice;
            AvailableProducingGoods = productionBuilding.AvailableProducingGoods;
            FreeSlotsCount = productionBuilding.FreeSlotsCount;
	        
            AvailableProducingGoods.Sort(CompareProducingItemData);
        }
        
        protected override void CopyMembersTo(IMapObject result)
        {
            base.CopyMembersTo(result);
            
            var buildingsData = jBuildingSlotsData.BuildingSlotsDataList;
            foreach (var buildingSlotData in buildingsData.Where(buildingSlotData => buildingSlotData.BuildingId == ObjectTypeID))
            {
                SlotData = buildingSlotData.SlotData;
            }
            
            var productionBuilding = (ProductionBuilding) result;
            productionBuilding.ProductionRules = ProductionRules;
            productionBuilding.MaximalSlotsCount = SlotData.Count;
            productionBuilding.AvailableSlotsCount = AvailableSlotsCount = 1; // first slot always unlocked
            productionBuilding.CurrentProduceSlots = new List<IProduceSlot>(CurrentProduceSlots);
            productionBuilding.AvailableProducingGoods = new List<IProducingItemData>(AvailableProducingGoods);
            productionBuilding.FreeSlotsCount = FreeSlotsCount;
	        
	        AvailableProducingGoods.Sort(CompareProducingItemData);
        }
        
        protected override void BuildFinishVerified(SyncBuildingResponse result)
        {
            base.BuildFinishVerified(result);
            jCityAoeService.LoadAoeData();
        }
        
        protected override void OnBuildingVerified(SyncBuildingResponse result)
        {
            base.OnBuildingVerified(result);

            jCityAoeService.LoadAoeData();
        }
        
        protected override void SyncServerModel(IMapBuilding model)
        {
            base.SyncServerModel(model);

            var productionBuilding = (ProductionBuilding)model;
            
            AvailableSlotsCount = productionBuilding.AvailableSlotsCount;

            // Hack! This is a bugfix for extra slot is added after upgrade NAN-5205
            if (CurrentProduceSlots.Count > AvailableSlotsCount)
            {
                var slots = new List<IProduceSlot>(AvailableSlotsCount);

                for (int i = 0; i < CurrentProduceSlots.Count; i++)
                {
                    IProduceSlot slot = CurrentProduceSlots[i];
                    int id = slot.SlotId.Value;
                    //if redundant slot added on building upgrade, both last and penultimate slots have same id, that's why we check i additionaly
                    if (id <= AvailableSlotsCount && i < AvailableSlotsCount)
                    {
                        slots.Add(slot);
                    }
                    else if ((i > AvailableSlotsCount || id > AvailableSlotsCount) && slot.IsUnlocked)
                    {
                        slot.IsUnlocked = false;
                    }
                }

                CurrentProduceSlots = slots;
                AddMissingSlots();
            }
            
            for (int i = 0; i < CurrentProduceSlots.Count; i++)
            {
                CurrentProduceSlots[i].IsUnlocked = i < AvailableSlotsCount;
            }
            
            AddMissingSlots();
        }

        private IProduceSlot CreateSlot(int id, bool isUnlocked = true)
        {
            IProduceSlot slot = Binder.GetInstance<IProduceSlot>();
            slot.SlotId = new Id<IProduceSlot>(id);
            slot.IsUnlocked = isUnlocked;

            return slot;
        }

        private int CompareProducingItemData(IProducingItemData a, IProducingItemData b)
        {
            return jProductsData.GetProduct(a.OutcomingProducts).UnlockLevel.CompareTo(
                jProductsData.GetProduct(b.OutcomingProducts).UnlockLevel);
        }
    }
}
