﻿using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Impl
{
    public class GroundBlock : MMapObject, IGroundBlock
    {

        public GroundBlock()
        {
            Dimensions = new Vector2F(1,1);
        }

        public override MapObjectintTypes MapObjectType
        {
            get {return MapObjectintTypes.GroundBlock;}
            set { } 
        }


        public override object Clone()
        {
            var result = Binder.GetInstance<IGroundBlock>(); 
            CopyMembersTo(result);
            return result;
        }
    }
}
