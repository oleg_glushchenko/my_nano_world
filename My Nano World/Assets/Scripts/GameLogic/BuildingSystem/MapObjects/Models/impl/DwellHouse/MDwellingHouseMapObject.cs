﻿using NanoReality.Game.Services.BuildingsUnlockService;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.Happiness;

namespace Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl
{
    public class MDwellingHouseMapObject : BuildingNeedHappiness, IDwellingHouseMapObject
    {
        /// <summary>
        /// Building Population.
        /// Sets from building instance config and can have different values
        /// for different instances of the same building id.
        /// </summary>
        public int MaxPopulation { get; set; }

        public override bool IsNeedRoad => true;
        public override MapObjectintTypes MapObjectType => MapObjectintTypes.DwellingHouse;
        
        [Inject] public IBuildingsUnlockService jBuildingUnlockService { get; set; }

        public override bool CanBeUpgraded
        {
            get
            {
                if (base.CanBeUpgraded)
                {
                    return IsEnoughPopulationLimit();
                }
                
                return false;
            }
        }

        #region Overrides of BuildingNeedHappiness

        public override void InitFromPrototype(IMapObject prototype)
        {
            base.InitFromPrototype(prototype);

            var house = (IDwellingHouseMapObject)prototype;
            MaxPopulation = jBuildingUnlockService.GetPopulationForDwelling(ObjectTypeID, Level, InstanceIndex);
        }
        
        public override object Clone()
        {
            var result = Binder.GetInstance<IDwellingHouseMapObject>();
            CopyMembersTo(result);
            return result;
        }

        protected override void CopyMembersTo(IMapObject result)
        {
            base.CopyMembersTo(result);
            var obj = (MDwellingHouseMapObject)result;
            obj.MaxPopulation = MaxPopulation;
        }

        public override void OnInitOnMap(IMapObject prototype)
        {
            base.OnInitOnMap(prototype);
            MaxPopulation = GetPopulationForBuilding();
        }

        public override void UpgradeObject(int upgradeTime)
        {
            base.UpgradeObject(upgradeTime);

            if (IsUpgradingNow)
            {
                MaxPopulation = GetPopulationForBuilding();
            }
        }
        
        #endregion

        private bool IsEnoughPopulationLimit()
        {
            var cityHall = jGameManager.FindMapObjectByType<ICityHall>(MapObjectintTypes.CityHall);

            if (cityHall != null)
            {
                var nextLevel = jMapObjectsData.GetNextBuildingLevelData(this);

                if (nextLevel == null) return false;
                        
                if (cityHall.IsUpgradingNow)
                {
                    cityHall = jMapObjectsData.GetPreviousBuildingLevelData(cityHall);
                }

                return cityHall.CanFitMorePeople(nextLevel.MaxPopulation - MaxPopulation);
            }

            return true;
        }

        private int GetPopulationForBuilding()
        {
            return jBuildingUnlockService.GetPopulationForDwelling(ObjectTypeID, Level, InstanceIndex);
        }
    }
}
