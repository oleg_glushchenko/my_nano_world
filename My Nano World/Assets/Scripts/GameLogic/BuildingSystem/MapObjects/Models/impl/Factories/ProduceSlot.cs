﻿using System;
using Assets.NanoLib.UtilitesEngine;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using Newtonsoft.Json;
using strange.extensions.signal.impl;
using UnityEngine;

namespace Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl.Factories
{
    public class ProduceSlot : IProduceSlot
	{
        public bool IsWaitingResponse
        {
            get => _isWaitingResponse;
            set => _isWaitingResponse = value;
        }

        private bool _timerStarted;

        /// <summary>
        /// Открыт ли слот
        /// </summary>
        public bool IsUnlocked { get; set; }

        private int _unlockPremiumPrice;
        
        public int UnlockPremiumPrice
        {
            get => _unlockPremiumPrice;
            set => _unlockPremiumPrice = value;
        }

        public bool IsFinishedProduce => IsUnlocked && IsFinishedProduceConfirm  && ItemDataIsProducing != null;
        public bool IsFinishedProduceConfirm{ get; set; }
        
        public bool IsEmpty => ItemDataIsProducing == null;

        [JsonProperty("time_left")]
        public float CurrentProducingTime { get; set; }

        public float TimeToProduce { get; protected set; }

        /// <summary>
        /// Good is producing. If null - slot is free
        /// </summary>
        [JsonProperty("producing_item")]
        public IProducingItemData ItemDataIsProducing { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("slot_id")]
        public Id<IProduceSlot> SlotId { get; set; }

        public Signal<IProduceSlot, IProducingItemData> OnFinishProduceGood { get; } = new Signal<IProduceSlot, IProducingItemData>();

        public Signal OnPayedOperationNotActual { get; } = new Signal();
        public event Action OnTimerTick;

        private ITimer _timer;
        private bool _isWaitingResponse;
    
        [Inject] public ITimerManager jTimerManager { get; set; }

        public ProduceSlot()
        {
            IsUnlocked = true;
        }

		public void StartProduceGood(IProducingItemData itemData, float timeToProduce)
        {
            ItemDataIsProducing = itemData;
            TimeToProduce = timeToProduce;
            _timer = jTimerManager.StartServerTimer(TimeToProduce, OnFinishProduce, OnProduceTick);
        }

        public bool ResumeProduceGood()
        {
            TimeToProduce = CurrentProducingTime;
            if (CurrentProducingTime > 0 && ItemDataIsProducing != null)
            {
                _timer = jTimerManager.StartServerTimer(CurrentProducingTime, OnFinishProduce, OnProduceTick);
                return true;
            }
            return false;
        }

        public virtual void ClearSlot()
        {
            CurrentProducingTime = 0;
        }

        protected void OnFinishProduce()
        {
            _timer = null;
            _timerStarted = false;
            CurrentProducingTime = 0;
            OnFinishProduceGood.Dispatch(this, ItemDataIsProducing);

	        if (OnPayedOperationNotActual != null)
	        {
		        OnPayedOperationNotActual.Dispatch();
				OnPayedOperationNotActual.RemoveAllOnceListeners();
	        }

		}

        /// <summary>
        /// Пересчитывает переменные времени, необходимо вызывать после буста (изменения CurrentProducingTime)
        /// </summary>
        public void RefreshTimerTimeout()
        {
            if (_timer == null)
                return;

            TimeToProduce = CurrentProducingTime;
            _timer.CancelTimer();
            _timer = jTimerManager.StartServerTimer(CurrentProducingTime, OnFinishProduce, OnProduceTick);
        }

        public void SkipTimer()
        {
            if (_timer == null) return;
            
            _timer.CancelTimer();
            _timer = null;
            OnFinishProduce();
        }
        
        public string GetSlotId()
        {
            return SlotId.Value.ToString();
        }
        
        public string GetId()
        {
            return Id.ToString();
        }

        protected void OnProduceTick(float time)
        {
            CurrentProducingTime = TimeToProduce - time;
            OnTimerTick?.Invoke();
            if (!_timerStarted)
            {
                _timerStarted = true;
            }
        }

        public void MakeFirstFakeTick()
        {
            OnProduceTick(0);
        }
	}
}