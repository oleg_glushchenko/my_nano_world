﻿using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using Newtonsoft.Json;
using System.Collections.Generic;
using GameLogic.BuildingSystem.CityArea;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using UnityEngine;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Impl
{
    public class LockedSector : CitySector, ILockedSector
    {
        [JsonProperty("cost")]
        public Price Price { get; set; }

        public bool Unlockable { get; set; }
    }

    public class CityLockedSector
    {
        [JsonProperty("city_id")]
        public Id<IUserCity> CityId { get; set; }

        [JsonProperty("bss_id")]
        public Id<IBusinessSector> BusinessSectorId { get; set; }

        [JsonProperty("map_sub_sector")]
        public LockedSector LockedSector { get; set; }
    }

    public class CitySubLockedMap
    {
        [JsonProperty("locked_sectors")]
        public List<CityLockedSector> LockedSectors { get; set; }

        public bool ContainsLockedSectors(Vector2Int leftBottom, Vector2Int dimension)
        {
            if (LockedSectors == null)
            {
                return false;
            }
            
            foreach (var sector in LockedSectors)
            {
                if (sector.LockedSector.Overlaps(leftBottom, dimension))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
