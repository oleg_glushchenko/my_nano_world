﻿using System.Linq;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl;
using NanoReality.Game.FoodSupply;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Models.SupplyFood
{
    public sealed class FoodSupplyBuilding : MapBuilding, IFoodSupplyBuilding
    {
        public override bool IsNeedRoad => true;

        public override MapObjectintTypes MapObjectType
        {
            get => MapObjectintTypes.FoodSupply;
            set { }
        }
        
        [Inject] public IPlayerExperience jPlayerExperience { get; set; }
        [Inject] public IFoodSupplyService jFoodSupplyService { get; set; }

        public override void OnInitOnMap(IMapObject prototype)
        {
            base.OnInitOnMap(prototype);

            jPlayerExperience.LevelChanged += OnUserLevelChanged;
        }

        public override void RemoveMapObject()
        {
            base.RemoveMapObject();
            
            jPlayerExperience.LevelChanged -= OnUserLevelChanged;
        }

        public override object Clone()
        {
            var result = Binder.GetInstance<IFoodSupplyBuilding>();
            CopyMembersTo(result);
            return result;
        }
        
        private void OnUserLevelChanged(int newUserLevel)
        {
            var slotUnlockedOnThisLevel = jFoodSupplyService.GetSlotsData().Any(c => c.UnlockLevel == newUserLevel);
            if (slotUnlockedOnThisLevel)
            {
                VerifyBuilding();
            }
        }
    }
}