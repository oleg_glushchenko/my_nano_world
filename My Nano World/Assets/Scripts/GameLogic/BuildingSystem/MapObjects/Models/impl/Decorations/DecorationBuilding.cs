﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;

public class DecorationBuilding : MapBuilding, IDecorationMapObject
{
    public override MapObjectintTypes MapObjectType
    {
        get { return MapObjectintTypes.Decoration; }
        set { }
    }

    public override object Clone()
    {
        var result = Binder.GetInstance<IDecorationMapObject>();
        CopyMembersTo(result);
        return result;
    }
}