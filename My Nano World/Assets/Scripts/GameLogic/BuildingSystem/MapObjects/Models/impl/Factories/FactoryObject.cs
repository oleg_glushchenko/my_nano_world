﻿using System;
using System.Linq;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Controllers.Factories;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using UnityEngine;
using GameLogic.BuildingSystem.MapObjects.Models.impl;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Models.impl.Factories
{
    public class FactoryObject : ProductionBuilding, IFactoryObject
    {
	    public override MapObjectintTypes MapObjectType
        {
            get => MapObjectintTypes.Factory;
            set { }
        }

        [Inject] public SignalFactoryProduceSlotFinished jSignalFactoryProduceSlotFinished { get; set; }

        public override object Clone()
        {
	        IFactoryObject result = Binder.GetInstance<IFactoryObject>();
            CopyMembersTo(result);
            return result;
        }
        
        protected override float QueueProductionTime()
		{
			var queueTime = 0f;
			for (int i = 0; i < CurrentProduceSlots.Count; i++)
			{
				if (CurrentProduceSlots[i].ItemDataIsProducing != null &&
				    CurrentProduceSlots[i].CurrentProducingTime > queueTime)
					queueTime = CurrentProduceSlots[i].CurrentProducingTime;
			}

			return queueTime;
		}

        public override void InitializeBuildingTimers()
        {
	        base.InitializeBuildingTimers();
	        foreach (IProduceSlot produceSlot in CurrentProduceSlots)
	        {
		        produceSlot.OnFinishProduceGood.AddListener(OnSlotProducingFinished);
	        }
        }

        public override void SkipSlot(IProduceSlot produceSlot, Action<IProduceSlot> callback)
        {
	        base.SkipSlot(produceSlot, callback);
	        RefreshNextSlotTimeout(produceSlot);
        }

        public override void UnlockSlot(IProduceSlot produceSlot)
        {
	        base.UnlockSlot(produceSlot);
	        produceSlot.OnFinishProduceGood.AddListener(OnSlotProducingFinished);
        }

        private void OnSlotProducingFinished(IProduceSlot slot, IProducingItemData product)
        {
	        RefreshNextSlotTimeout(slot);
        }

        protected override void FinishVerifyProduceGood(IProduceSlot sender)
        {
	        jSignalFactoryProduceSlotFinished.Dispatch (MapID);
        }

        private float GetProductProducingTimeData(IProduceSlot nextSlot)
        {
	        IProducingItemData data = AvailableProducingGoods.Find(good =>
		        good.OutcomingProducts == nextSlot.ItemDataIsProducing.OutcomingProducts);

	        if (SectorId.Value == 5)
	        {
		        // For agrosector we don't use any time multipliers.
		        return Mathf.CeilToInt(data.ProducingTime);
	        }

	        return jProductionService.GetProductionTimeMultiplier(data);
        }

		private void RefreshNextSlotTimeout(IProduceSlot finishedSlot)
		{
			IProduceSlot nextSlot = CurrentProduceSlots.Where(s =>
					s.SlotId != finishedSlot.SlotId &&
					s.ItemDataIsProducing != null &&
					s.CurrentProducingTime > 0)
				.OrderBy(s => s.CurrentProducingTime)
				.FirstOrDefault();

	        if (nextSlot == null)
		        return;

	        float producingTime = GetProductProducingTimeData(nextSlot);
	        Debug.Log("producingTimeNew: " + producingTime + " producingTimeOld: " + nextSlot.CurrentProducingTime);
	        nextSlot.ItemDataIsProducing.ProducingTime = producingTime;
	        nextSlot.CurrentProducingTime = producingTime;
	        nextSlot.RefreshTimerTimeout();
        }
    }
}
