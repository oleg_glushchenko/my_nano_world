﻿namespace NanoReality.GameLogic.BuildingSystem.MapObjects
{
    public interface IBuildingWithProduction
    {
        bool IsPruductionEmpty { get; }
    }
}