﻿using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.StartApplicationSequence.Models.api;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Api
{
    /// <summary>
    /// Contains balancing related data for all map objects (buildings, roads etc).
    /// Each entry is a level of map object starting from 0
    /// </summary>
    public interface IMapObjectsData : IGameBalanceData
    {
        /// <summary>
        /// List of available map objects with balancing data (must never be modified!!!)
        /// </summary>
        List<IMapObject> MapObjects { get; }

        /// <summary>
        /// Returns true if there is balancing entry for given building id and level
        /// </summary>
        /// <param name="buildingId"></param>
        /// <param name="levelId"></param>
        /// <returns></returns>
        bool IsPresentBuildingIdAndLevel(Id_IMapObjectType buildingId, int levelId);

        /// <summary>
        /// Returns balancing data of givent building id and level. If not present, returns null
        /// </summary>
        /// <param name="buildingId"></param>
        /// <param name="levelId"></param>
        /// <returns></returns>
        IMapObject GetByBuildingIdAndLevel(Id_IMapObjectType buildingId, int levelId);

        IMapObject GetByBuildingId(Id_IMapObjectType buildingId);
        
        /// <summary>
        /// Return first encountered balacing data of the given building type, otherwise returns null
        /// </summary>
        /// <param name="buildingType"></param>
        /// <returns></returns>
        IMapObject GetByBuildingType(MapObjectintTypes buildingType);

        IMapObject GetByBuildingTypeAndSubCategory(MapObjectintTypes buildingType, BuildingSubCategories subCategories);

        /// <summary>
        /// Returns all map objects that can be constructed in specific business sector
        /// </summary>
        /// <param name="sectorId">specific business sector id</param>
        /// <returns>List of map objects that can be constructed in the given sector</returns>
        List<IMapObject> GetAllMapObjectsForSector(Id<IBusinessSector> sectorId);
        
        /// <summary>
        /// Returns all map objects by type
        /// </summary>
        /// <param name="buildingType"></param>
        /// <returns></returns>
        List<IMapObject> GetAllMapObjectsByBuildingType(MapObjectintTypes buildingType);

        BuildingSubCategories GetSubCategoryByBuildingId(Id_IMapObjectType buildingId);
        
        /// <summary>
        /// Returns a prototype object (must never be modified!!!)
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        T GetMapObjectData<T>(Id_IMapObjectType ID, int level) where T: class, IMapObject;
        
        T GetMapObjectData<T>(T building, int level) where T: class, IMapObject;

        /// <summary>
        /// Returns a previous building level prototype (must never be modified!!!)
        /// </summary>
        /// <param name="building"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        T GetPreviousBuildingLevelData<T>(T building) where T : class, IMapBuilding;

        /// <summary>
        /// Returns a next building level prototype (must never be modified!!!)
        /// </summary>
        /// <param name="building"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        T GetNextBuildingLevelData<T>(T building) where T : class, IMapBuilding;
    }
}
