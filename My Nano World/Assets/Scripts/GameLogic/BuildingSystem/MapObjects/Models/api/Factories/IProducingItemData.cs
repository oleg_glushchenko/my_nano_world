﻿using System;
using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using NanoLib.Core;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories
{
    /// <summary>
    /// Represent structure of production good on the factory
    /// </summary>
    public interface IProducingItemData : ICloneable
    {
        /// <summary>
        /// Dictionary of goods needed to produce new good
        /// </summary>
        Dictionary<Id<Product>, int> IncomingProducts { get; set; }

        /// <summary>
        /// Good to produce
        /// </summary>
        IntValue OutcomingProducts { get; set; }

        /// <summary>
        /// Count to produce
        /// </summary>
        int Amount { get; set; }

        /// <summary>
        /// Time to producing
        /// </summary>
        float ProducingTime { get; set; }

        /// <summary>
        /// Возвращает список продуктов, которых не хватает для производства
        /// </summary>
        /// <returns>список продуктов, которых не хватает для производства</returns>
        Dictionary<Id<Product>, int> GetNotEnoughProductsList();
    }
}
