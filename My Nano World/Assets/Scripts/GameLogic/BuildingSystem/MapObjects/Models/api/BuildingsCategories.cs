﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Категории зданий (для UI)
/// </summary>
public enum BuildingsCategories 
{
    None = 0,
    HQs = 1, 
	Residential = 2,
    FoodSupply = 3,
	Entertaiment = 4,  
	Energy = 5,
	Crops = 6,
	Animals = 7,
	Factories = 8, 
	Stone = 9, 
	Metal = 10, 
	Vehicle = 11,
    Special = 12,
    Service = 13
}

public enum BuildingSubCategories
{
    None = 0,
    Common = 1,
    Vip = 2,
    Luxury = 3
}

public static class BuildingsCategoriesExtentions
{
    private static readonly Dictionary<BuildingsCategories, string> _localizationKeys;

    static BuildingsCategoriesExtentions()
    {
        _localizationKeys = new Dictionary<BuildingsCategories, string>
        {
            {BuildingsCategories.HQs, "BUILDINGSHOP_LABEL_HQ"},
            {BuildingsCategories.Residential, "BUILDINGSHOP_LABEL_RESIDETIAL"},
            {BuildingsCategories.FoodSupply, "TRADE_FILTER_SUPPLY"},
            {BuildingsCategories.Entertaiment, "BUILDINGSHOP_LABEL_ENTERTAIMENT"},
            {BuildingsCategories.Energy, "SHARED_LABEL_ENERGY"},
            {BuildingsCategories.Crops, "BUILDINGSHOP_LABEL_CROPS"},
            {BuildingsCategories.Animals, "BUILDINGSHOP_LABEL_ANIMALS"},
            {BuildingsCategories.Factories, "BUILDINGSHOP_LABEL_FACTORIES"},
            {BuildingsCategories.Stone, "BUILDINGSHOP_LABEL_STONE"},
            {BuildingsCategories.Metal, "BUILDINGSHOP_LABEL_METAL"},
            {BuildingsCategories.Vehicle, "BUILDINGSHOP_LABEL_VEHICLE"},
            {BuildingsCategories.Special, "BUILDINGSHOP_LABEL_SPECIAL"},
            {BuildingsCategories.Service, "BUILDINGSHOP_LABEL_SERVICE"},
        };
    }

    public static string ToLocalizationString(this BuildingsCategories type)
    {

        if (_localizationKeys.ContainsKey(type))
        {
            return LocalizationMLS.Instance.GetText(_localizationKeys[type]);
        }

        Debug.LogWarning("The are no localization key for " + type);
        return string.Empty;

    }
}
