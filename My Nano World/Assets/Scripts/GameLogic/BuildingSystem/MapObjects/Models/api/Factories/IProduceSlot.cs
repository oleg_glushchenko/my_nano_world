﻿using System;
using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using strange.extensions.signal.impl;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories
{
    /// <summary>
    /// Represent prodaction line on the factory
    /// </summary>
    public interface IProduceSlot
    {
        bool IsWaitingResponse { get; set; }

        /// <summary>
        /// Открыт ли слот
        /// </summary>
        bool IsUnlocked { get; set; }

        /// <summary>
        /// if true - product is finished produce, and waiting for shipping
        /// </summary>
        bool IsFinishedProduce { get; }

        bool IsFinishedProduceConfirm { get; set; }
        
        bool IsEmpty { get; }

        /// <summary>
        /// Current prodaction time
        /// </summary>
        float CurrentProducingTime { get; set; }
        
        /// <summary>
        /// Product produce time (depends from work amount and equipment)
        /// </summary>
        float TimeToProduce { get; }

        /// <summary>
        /// Стоимость открытия слота за премиум валюту. Устанавливается объектом фабрики
        /// </summary>
        int UnlockPremiumPrice { get; set; }


        /// <summary>
        /// Good is producing. If null - slot is free
        /// </summary>
        IProducingItemData ItemDataIsProducing { get; set; }

        /// <summary>
        /// Идентификатор текущего производственного слота
        /// Каждый раз при старте производства этот идентификатор меняется
        /// </summary>
        int Id { get; set; }

	    /// <summary>
	    /// Production slot id, starts from 1 (not 0)
	    /// </summary>
        Id<IProduceSlot> SlotId { get; set; }

        /// <summary>
        /// Starting local timer to produce good
        /// </summary>
        /// <param name="itemData"></param>
        /// <param name="timeToProduce"></param>
        void StartProduceGood(IProducingItemData itemData, float timeToProduce);

        /// <summary>
        /// Resuming produce good
        /// </summary>
        bool ResumeProduceGood();

        /// <summary>
        /// Скип таймера. Необходимо использовать при скипе производства
        /// </summary>
        void SkipTimer();

        /// <summary>
        /// Calls when local producing finished
        /// </summary>
        Signal<IProduceSlot, IProducingItemData> OnFinishProduceGood { get; }

        event Action OnTimerTick;

        /// <summary>
        /// Clearing slot and prepeare it to next using
        /// </summary>
        void ClearSlot();
        
        void RefreshTimerTimeout();

        void MakeFirstFakeTick();

        string GetSlotId();
        
        string GetId();

		/// <summary>
		/// Signal dispatched on slot finished, to let Buy popup know if payed operation is not actual anymore
		/// </summary>
	    Signal OnPayedOperationNotActual { get; }
    }

    public class SlotPack: ISlotPack
    {
        public List<IProduceSlot> slots { get; set; }
    
    }

    public interface ISlotPack
    {
         List<IProduceSlot> slots { get; set; }
    }
}
