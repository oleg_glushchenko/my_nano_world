﻿using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using Assets.NanoLib.Utilities.Pulls;
using NanoLib.Core;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using strange.extensions.signal.impl;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using UnityEngine;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Api
{
	/// <summary>
    /// Model of any entity on game map.
    /// </summary>
    public interface IMapObject : IPullableObject 
    {
        #region JSON DATA

        /// <summary>
        /// Object Type ID (unique for every map object type in game)
        /// </summary>
        Id_IMapObjectType ObjectTypeID { get; set; }

        /// <summary>
        /// Object logic type id (unique for every group of buildings)
        /// </summary>
        MapObjectintTypes MapObjectType { get; set; }
        
        /// <summary>
        /// Level of upgrade of map object
        /// </summary>
        int Level { get; set; }
        
        /// <summary>
        /// The serial number of the building of this type built on the map.
        /// Count starts from 1.
        /// </summary>
        int InstanceIndex { get; set; }

        /// <summary>
        /// Object map ID (unique for every object on map)
        /// </summary>
        Id_IMapObject MapID { get; set; }

        /// <summary>
        /// Айди сектора в котором находится здание
        /// </summary>
        Id<IBusinessSector> SectorId { get; set; }

        /// <summary>
	    /// Name that is set in balancing table (not localized)
	    /// </summary>
	    string BalanceName { get; set; }

        /// <summary>
        /// Object name
        /// </summary>
        string Name { get; }

        string Description { get; }

        /// <summary>
        /// List of sectors where object can build
        /// </summary>
        List<BusinessSectorsTypes> SectorIds { get; set; }

        List<BuildingsCategories> Categories { get; set; }
        
        BuildingSubCategories SubCategory { get; set; }

        /// <summary>
        /// Grid cell start position
        /// </summary>
        Vector2F MapPosition { get; set; }

        /// <summary>
        /// Object dementions (X - height, Y - widht)
        /// </summary>
        Vector2F Dimensions { get; set; }

	    /// <summary>
	    /// Amount of tiles around the map object that cannot be occupied by other map objects (except roads)
	    /// </summary>
	    int ConstructionOffset { get; set; }

	    /// <summary>
        /// Is building currently upgrading
        /// </summary>
        bool IsUpgradingNow { get; }

        /// <summary>
        /// Current constuction time in seconds (upgrade or build) 
        /// </summary>
        int CurrentConstructTime { get; }

        /// <summary>
        /// Название звука, проигрывающегося при открытии панели здания
        /// </summary>
        string OpenPopupSound { get; set; }

        /// <summary>
        /// Подведена ли к зданию дорога, соединенная с магистралью
        /// </summary>
        bool IsConnectedToGeneralRoad { get; set; }

        #endregion
      
        bool IsVerificated { get; set; }

        bool IsNeedRoad { get; }

        /// <summary>
        /// used for visual effects check whether upgrade/building was skipped and which animation to play
        /// </summary>
        bool IsSkipped { get; set; }

         /// <summary>
        /// Построенно ли здание
        /// </summary>
        bool IsConstructed { get; }
	    bool IsConstructFinished { get; }
	    
	    /// <summary>
	    /// Is construction of functional object completed
	    /// </summary>
	    bool IsFunctionalConstructionComplete { get; }

	    MapObjectId Id { get; }
	    
	    object Rules { get; set; }

		#region Events
	    
		/// <summary>
		/// Происходит при старте постройки/улучшения здания
		/// </summary>
		Signal<IMapObject> EventOnMapObjectBuildStarted { get; }

		/// <summary>
		/// Вызывается при окончании постройки/улучшения здания
		/// </summary>
		Signal<IMapObject> EventOnMapObjectBuildFinished { get; }

        /// <summary>
		/// Вызывается при окончании перемещения здания
		/// </summary>
		Signal<IMapObject> EventOnMapObjectBuildMoved { get; }

        /// <summary>
        /// Вызывается при изменении состояния подключения к дороге
        /// </summary>
        Signal<IMapObject> EventOnBuildingRoadConnectionChanged { get; }

        /// <summary>
        /// Вызывается при получения id от сервера.
        /// Параметры:
        /// 1. Id_IMapObject - старый фективный id
        /// 2. IMapObject - объект с обновленным id
        /// </summary>
        Signal<Id_IMapObject, IMapObject> EventOnCommitBuilding { get; }

		#endregion

        /// <summary>
        /// Инициализирует здание установленное на карте
        /// </summary>
        /// <param name="prototype"></param>
        void OnInitOnMap(IMapObject prototype);

	    /// <summary>
	    /// Called when all other map objects of the same map were initialized
	    /// </summary>
	    void OnPostInitObject();

        /// <summary>
        /// Инициализирует таймеры для здания
        ///  </summary>
        void InitializeBuildingTimers();

	    /// <summary>
	    /// Called when map object view for the object is added on map
	    /// </summary>
	    void OnMapObjectViewAdded();
	    
        void StartConstruction(int constructionTime);
       
        void CommitBuilding(Id_IMapObject id);

        void UpgradeObject(int upgradeTime);
        
        void RemoveMapObject();

        string GetMapIDString();

        void MoveMapObjectVerificated(MoveMapObjectResponce result);
    }
	
    /// <summary>
    /// Decorative interface especially for id ObjectTypeID
    /// </summary>
    public interface IMapObjectType
    {
    }
}
