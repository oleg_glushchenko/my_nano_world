﻿using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api
{
    public interface IPowerPlantBuilding : IAoeBuilding
    {
        int AoeAreaX { get; set; }
        int AoeAreaY { get; set; }
    }
}