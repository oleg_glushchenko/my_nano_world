﻿using System;
using System.Collections.Generic;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using strange.extensions.signal.impl;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects
{
    public interface IAgroFieldBuilding : IMapBuilding
    {
        IProducingItemData CurrentGrowingPlant { get; set; }

        AgriculturalFieldState CurrentState { get; }

        float GrowingTimeLeft { get; set; }

        List<IProducingItemData> AvailableAgroProducts { get; set; }

        bool IsHarvesting { get; set; }

        event Action<IAgroFieldBuilding> OnGrowingStart;

        event Action<IAgroFieldBuilding> OnGrowingStartVerified;

        Signal OnHarvestStarted { get; }

        Signal<bool> OnHarvestEnded { get; }

        Signal<bool> OnHarvestEndedVerified { get; }

        Signal<IProducingItemData> OnFinishGrowing { get; }

        Signal<IProducingItemData> OnFinishGrowingVerified { get; }

        void SkipGrowingTimer(int hardCost);

        void StartHarvesting();

        void StartGrowing(IProducingItemData plant);
    }
}