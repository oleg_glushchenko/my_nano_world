﻿using UnityEngine;

namespace GameLogic.BuildingSystem.MapObjects.Models.api.Core
{
    public interface IMovableObject
    {
        /// <summary>
        /// Двигает здание
        /// </summary>
        /// <param name="oldPosition">предыдущая позиция на карте</param>
        /// <param name="newPosition">новая позиция на карте</param>
        /// <returns>Успешно ли перемещено здание</returns>
        void MoveMapObject(Vector2Int oldPosition, Vector2Int newPosition);
    }
}
