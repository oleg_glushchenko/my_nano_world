﻿using NanoReality.GameLogic.BuildingSystem.MapObjects.Controllers.Agricultural;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Production;

namespace Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.api.Mines
{
    public interface IMineObject : IProductionBuilding
    {
        OnProductProduceAndCollectSignal jOnProductProduceAndCollectSignal { get; set; }
    }
}