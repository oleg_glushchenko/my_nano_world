﻿using System;
using System.Collections.Generic;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using UnityEngine;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Api
{
    public interface IAoeBuilding : IMapBuilding
    {
        AoeLayout AoeLayoutType { get; set; }

        Vector2 AoeSize { get; }
        
        List<int> AoeConnected { get; set; }
        
        event Action<IAoeBuilding> AoeLayoutChanged;
        
        void SwitchAoeLayout(AoeLayout newType);
        
        bool IsCoveringBuilding(IMapObject mapObject);
    }
}