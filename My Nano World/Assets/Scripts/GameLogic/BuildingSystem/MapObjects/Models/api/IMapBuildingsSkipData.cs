﻿using System.Collections.Generic;
using NanoReality.GameLogic.StartApplicationSequence.Models.api;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api
{
    /// <summary>
    /// Гейм. баланс данные о скипе времени постройки за премиум
    /// </summary>
    public interface IMapBuildingsSkipData : IGameBalanceData
    {
        /// <summary>
        /// Словарь временных точек скипа ключ время, значение - стоимость скипа в премиуме
        /// </summary>
        Dictionary<int, int> SkipData { get; set; }

        /// <summary>
        /// Возвращает ближайшую цену скипа
        /// </summary>
        /// <param name="time">текущее время постройки</param>
        /// <returns></returns>
        int GetNearesSkipPrice(float time);

    }
}


