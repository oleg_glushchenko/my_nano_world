﻿using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using Newtonsoft.Json;

public class AwardProduct
{
    [JsonProperty("product_id")]
    public Id<Product> ProductId { get; private set; } = -1;
    
    [JsonProperty("count")]
    public int Count { get; private set; }
}