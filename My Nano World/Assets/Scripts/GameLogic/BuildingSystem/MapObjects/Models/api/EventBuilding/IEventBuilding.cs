using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

public interface IEventBuilding : IMapBuilding
{
    bool ReadyToUpgrade { get; set; }
    int LanternPrice { get; set; }
    float SecondsToNextEventDay { get; set; }
}
