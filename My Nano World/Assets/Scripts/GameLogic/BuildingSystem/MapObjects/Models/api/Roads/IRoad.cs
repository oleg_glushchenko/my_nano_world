﻿namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Api
{
    /// <summary>
    /// Представляет класс дороги, дорога имеет цену
    /// </summary>
    public interface IRoad : IMapObject
    {
    }

    public enum RoadLevelType
    {
        Simple = 0,
        EnterPoint = 200,
        Bridge = 100,

        /// <summary>
        /// Дорога, которую пользователю не разрешается редактировать
        /// </summary>
        HardRoad = 150,

        /// <summary>
        /// Выезд для машин в ордер деске (запрещен для редактирования игроком)
        /// </summary>
        OrderDeskCrossExit = 151,

        /// <summary>
        /// Выезд для машин из ордер деска на глобальную дорогу (запрещен для редактирования игроком)
        /// </summary>
        OrderDeskExitToFakeMainRoad = 152
    }
}
