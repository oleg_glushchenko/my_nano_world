namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Api
{
    public struct MapObjectId
    {
        public readonly int Id;
        public readonly int Level;

        public MapObjectId(int id, int level = 0)
        {
            Id = id;
            Level = level;
        }

        public override string ToString()
        {
            return $"Id: {Id}, Level: {Level}";
        }
        
        public static bool operator ==(MapObjectId v1, MapObjectId v2)
        {
            return v1.Equals(v2);
        }

        public static bool operator !=(MapObjectId v1, MapObjectId v2)
        {
            return !v1.Equals(v2);
        }

        public bool Equals(MapObjectId other)
        {
            return Id == other.Id && Level == other.Level;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Id * 397) ^ Level;
            }
        }
    }
}