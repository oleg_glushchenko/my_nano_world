﻿namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api
{
    public interface IWarehouseMapObject : IMapBuilding
    {
        int MaxCapacity { get; }
        int CurrentCapacity { get; }
    }
}