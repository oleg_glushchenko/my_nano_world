﻿using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using strange.extensions.signal.impl;

public interface IBuildingWithElectricity : IMapBuilding
{
    bool IsEnabledEnergy { get; set; }
}

/// <summary>
/// Сигнал символизирующий о том что изменилось состояние электричества в здании
/// </summary>
public class SignalOnBuildingElectricityChanged : Signal<IBuildingWithElectricity, bool> { }
