﻿using Newtonsoft.Json;

public sealed class ProductRequirementItem
{
    [JsonProperty("product_id")]
    public int ProductID { get; set; }
    
    [JsonProperty("count")]
    public int Count { get; set; }
}