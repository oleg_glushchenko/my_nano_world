﻿using Newtonsoft.Json;

public sealed class IAwardItem
{
    [JsonProperty("nano_coins")]
    public int Coins { get; set; }

    [JsonProperty("nano_bucks")]
    public int NanoBucks { get; set; }

    [JsonProperty("gold")]
    public int GoldBars { get; set; }

    [JsonProperty("exp")]
    public int Experience { get; set; }

    [JsonProperty("product")]
    public AwardProduct Product { get; set; }

    public bool HaveCoins() => Coins > 0;
    
    public bool HaveExperience() => Experience > 0;

    public bool HaveNanoBucks() => NanoBucks > 0;

    public bool HaveGoldBars() => GoldBars > 0;

    public bool HaveProduct() => Product != null;

    public override bool Equals(object obj)
    {
        var castedObj = (IAwardItem) obj;
        if (castedObj == null)
        {
            return false;
        }

        return Coins == castedObj.Coins && NanoBucks == castedObj.NanoBucks && GoldBars == castedObj.GoldBars && Experience == castedObj.Experience
               && Equals(Product, castedObj.Product);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            int hashCode = Coins;
            hashCode = (hashCode * 397) ^ NanoBucks;
            hashCode = (hashCode * 397) ^ GoldBars;
            hashCode = (hashCode * 397) ^ Experience;
            hashCode = (hashCode * 397) ^ (Product != null ? Product.GetHashCode() : 0);
            return hashCode;
        }
    }
}