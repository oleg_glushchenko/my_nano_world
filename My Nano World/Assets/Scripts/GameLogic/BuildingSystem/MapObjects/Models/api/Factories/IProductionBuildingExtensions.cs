namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Production
{
    public static class IProductionBuildingExtensions
    {
        public static bool IsMaximumSlotsUnlocked(this IProductionBuilding building)
        {
            return building.AvailableSlotsCount == building.MaximalSlotsCount;
        }
    }
}