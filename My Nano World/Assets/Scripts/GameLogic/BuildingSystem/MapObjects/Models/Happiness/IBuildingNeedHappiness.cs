﻿using System;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.ServiceBuildings;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Models.Happiness
{
    public interface IBuildingNeedHappiness : IMapObject
    {
        event Action<SatisfactionState> OnHappinessChanged;
        
        SatisfactionState IsHappinessConnected { get; }

        bool HasService(ServiceBuildingType serviceType);
    }
}