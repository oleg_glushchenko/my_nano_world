using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Models.Happiness
{
    public class PoliceBuilding : HappinessBuilding
    {
        public override MapObjectintTypes MapObjectType => MapObjectintTypes.Police;
        
        public override object Clone()
        {
            var result = Binder.GetInstance<PoliceBuilding>();
            CopyMembersTo(result);
            return result;
        }
    }
    
    public class HospitalBuilding : HappinessBuilding
    {
        public override MapObjectintTypes MapObjectType => MapObjectintTypes.Hospital;
        
        public override object Clone()
        {
            var result = Binder.GetInstance<HospitalBuilding>();
            CopyMembersTo(result);
            return result;
        }
    }
    
    public class SchoolBuilding : HappinessBuilding
    {
        public override MapObjectintTypes MapObjectType => MapObjectintTypes.School;
        
        public override object Clone()
        {
            var result = Binder.GetInstance<SchoolBuilding>();
            CopyMembersTo(result);
            return result;
        }
    }
}