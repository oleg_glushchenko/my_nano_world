﻿using System.Collections.Generic;
using NanoReality.Game.Services.BuildingsUnlockService;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.ServiceBuildings;
using UnityEngine;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Models.Happiness
{
    public class ServicesBuildingsService
    {
        private readonly Dictionary<ServiceBuildingType, int> _serviceBuildings = new Dictionary<ServiceBuildingType, int>
        {
            {ServiceBuildingType.School, 69},
            {ServiceBuildingType.Hospital, 58}
        };
        
        [Inject] public IPlayerExperience jPlayerExperience { get; set; }
        [Inject] public IBuildingsUnlockService jBuildingsUnlockService { get; set; }

        public bool IsServiceUnlocked(ServiceBuildingType serviceType)
        {
            return jBuildingsUnlockService.GetBuildingUnlockLevel(_serviceBuildings[serviceType])
                   <= jPlayerExperience.CurrentLevel;
        }
    }
}