﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.impl;
using GameLogic.BuildingSystem.CityAoe;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.ServiceBuildings;
using NanoReality.GameLogic.ServerCommunication.Models.Api;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Models.Happiness
{
    public class BuildingNeedHappiness : MapBuilding, IBuildingNeedHappiness
    {
        [Inject] public ServicesBuildingsService jServicesBuildingsService { get; set; }
        [Inject] public ICityAoeService jCityAoeService { get; set; }

        private readonly Dictionary<ServiceBuildingType, bool> _affectedServices = new Dictionary<ServiceBuildingType, bool>
        {
            {ServiceBuildingType.School, false},
            {ServiceBuildingType.Hospital, false}
        };

        public event Action<SatisfactionState> OnHappinessChanged;

        private SatisfactionState _isHappinessConnected;
        public SatisfactionState IsHappinessConnected => _isHappinessConnected;
        
        public override void MoveMapObjectVerificated(MoveMapObjectResponce result)
        {
            base.MoveMapObjectVerificated(result);
            jCityAoeService.LoadAoeDataWithDelay(1.0f);
        }
        
        protected override void BuildFinishVerified(SyncBuildingResponse result)
        {
            base.BuildFinishVerified(result);
            
            jCityAoeService.LoadAoeData();
        }

        protected override void OnBuildingVerified(SyncBuildingResponse result)
        {
            base.OnBuildingVerified(result);

            jCityAoeService.LoadAoeData();
        }

        public void ClearServicesStatus()
        {
            foreach (var service in new Dictionary<ServiceBuildingType, bool>(_affectedServices))
                _affectedServices[service.Key] = false;
        }

        public void SetActiveServices(IEnumerable<ServiceBuildingType> activeServices)
        {
            foreach (var service in activeServices)
                if (_affectedServices.ContainsKey(service))
                    _affectedServices[service] = true;
            
            RecalculateHappiness();
        }

        public bool HasService(ServiceBuildingType serviceType)
        {
            return _affectedServices[serviceType];
        }

        private void RecalculateHappiness()
        {
            var newHappiness = SatisfactionState.Sad;
            var unlockedServicesCount = 0;
            var affectedServicesCount = 0;
            foreach (var affectedService in _affectedServices)
            {
                var isUnlocked = jServicesBuildingsService.IsServiceUnlocked(affectedService.Key);
                if (isUnlocked)
                {
                    unlockedServicesCount += 1;
                    if (affectedService.Value)
                    {
                        affectedServicesCount += 1;
                    }
                }
            }

            newHappiness = NewSatisfactionState(affectedServicesCount, unlockedServicesCount);

            if (newHappiness == _isHappinessConnected)
                return;

            _isHappinessConnected = newHappiness;
            OnHappinessChanged?.Invoke(newHappiness);
        }

        private SatisfactionState NewSatisfactionState(int affectedServicesCount, int unlockedServicesCount)
        {
            var resultState = SatisfactionState.Sad;
            
            if (affectedServicesCount == unlockedServicesCount)
            {
                resultState = SatisfactionState.Happy;
            }
            else if (affectedServicesCount > 0 && affectedServicesCount < unlockedServicesCount)
            {
                resultState = SatisfactionState.Unhappy;
            }
            else if (affectedServicesCount == 0)
            {
                resultState = SatisfactionState.Sad;
            }

            return resultState;
        }
    }
}