﻿using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Models.Happiness
{
    public interface IBuildingProduceHappiness : IAoeBuilding
    {
        float AoeAreaX { get; set; }
        float AoeAreaY { get; set; }
    }
}