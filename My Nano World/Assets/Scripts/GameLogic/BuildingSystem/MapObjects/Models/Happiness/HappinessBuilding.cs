﻿using System;
using GameLogic.BuildingSystem.MapObjects.Models.impl;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using Newtonsoft.Json;
using UnityEngine;

namespace NanoReality.GameLogic.BuildingSystem.MapObjects.Models.Happiness
{
    public class HappinessBuilding : AoeBuilding, IBuildingProduceHappiness
    {
        [JsonProperty("happiness_distance_x")] 
        public float AoeAreaX { get; set; }

        [JsonProperty("happiness_distance_y")] 
        public float AoeAreaY { get; set; }
        
        protected override bool IsEnergy => false;

        public override bool IsCoveringBuilding(IMapObject mapObject)
        {
            return mapObject is IBuildingNeedHappiness;
        }
        public override void InitFromPrototype(IMapObject prototype)
        {
            base.InitFromPrototype(prototype);

            var casted = (HappinessBuilding)prototype;
            AoeAreaX = Convert.ToInt32(casted.AoeSize.x);
            AoeAreaY = Convert.ToInt32(casted.AoeSize.y);
        }
        
        public override object Clone()
        {
            var result = Binder.GetInstance<IBuildingProduceHappiness>();
            CopyMembersTo(result);
            return result;
        }

        protected override void CopyMembersTo(IMapObject result)
        {
            base.CopyMembersTo(result);

            var casted = (HappinessBuilding)result;
            casted.AoeAreaX = AoeAreaX;
            casted.AoeAreaY = AoeAreaY;
        }

        protected override void SyncServerModel(IMapBuilding model)
        {
            base.SyncServerModel(model);
            
            var happinessBuilding = (HappinessBuilding)model;

            AoeAreaX = happinessBuilding.AoeAreaX;
            AoeAreaY = happinessBuilding.AoeAreaY;
        }
        
        public override void MoveMapObject(Vector2Int oldPosition, Vector2Int newPosition)
        {
            base.MoveMapObject(oldPosition, newPosition);
            foreach (int connectedBuildingId in AoeConnected)
            {
                var affectedBuilding = jGameManager.FindMapObjectById<IDwellingHouseMapObject>(connectedBuildingId);
                affectedBuilding.IsVerificated = false;
            }
        }
    }
}