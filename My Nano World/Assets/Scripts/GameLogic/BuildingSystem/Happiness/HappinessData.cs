﻿using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.ServerCommunication.Controllers;
using Assets.NanoLib.UI.Core.Signals;
using NanoReality.Game.CityStatusEffects;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using strange.extensions.signal.impl;

namespace NanoReality.GameLogic.BuildingSystem.Happiness
{
    //TODO move this signal to common place
    public class SignalUpdateHappiness : Signal { }
    public class HappinessData : IHappinessData
    {
        [Inject]
        public IPlayerProfile jPlayerProfile { get; set; }

        [Inject]
        public IServerCommunicator jServerCommunicator { get; set; }

        [Inject]
        public UpdatePopulationSatisfactionSignal jUpdatePopulationSatisfactionSignal { get; set; }

        [Inject]
        public UserLevelUpSignal jUserLevelUpSignal { get; set; }
        
        [Inject] 
        public SignalUpdateHappiness jSignalUpdateHappiness { get;set; }
        
        [Inject] public ICityStatusEffectService jCityStatusEffectService { get; set; }

        //TODO move this logic to proper place
        public int TotalHappinessPercent
        {
            get => jPlayerProfile.UserCity.Happiness;

            set
            {
                PreviousHappinessPercent = jPlayerProfile.UserCity.Happiness;
                jPlayerProfile.UserCity.Happiness = value;
                
                jUpdatePopulationSatisfactionSignal.Dispatch(this);
                jSignalUpdateHappiness.Dispatch();
            }
        }

        public int PreviousHappinessPercent { get; private set; }

        [PostConstruct]
        public void PostConstruct()
        {
            jUserLevelUpSignal.AddListener(OnUserLevelUp);
            jCityStatusEffectService.StatusEffectsChanged += OnStatusEffectsChanged;
        }

        private void OnStatusEffectsChanged()
        {
            UpdateHappiness();
        }

        public void UpdateData(int happinessPercent)
        {
            TotalHappinessPercent = happinessPercent;
        }

        private void OnUserLevelUp()
        {
            UpdateHappiness();
        }

        private void UpdateHappiness()
        {
            jServerCommunicator.GetHappinessInfo((happinessValue) =>
            {
                TotalHappinessPercent = happinessValue;
            });
        }
    }
}
