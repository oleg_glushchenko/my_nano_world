using System.Collections.Generic;
using Assets.NanoLib.Utilities;

namespace NanoReality.GameLogic.BuildingSystem.City.Models.Api
{
    public static class BusinessSectorExtensions
    {
        private static readonly List<BusinessSectorsTypes> BusinessSectors =
            new List<BusinessSectorsTypes>
            {
                BusinessSectorsTypes.Global,
                BusinessSectorsTypes.Town,
                BusinessSectorsTypes.HeavyIndustry,
                BusinessSectorsTypes.Farm
            };

        public static Id<IBusinessSector> GetBusinessSectorId(this BusinessSectorsTypes sectorType)
        {
            return (int)sectorType;
        }

        public static BusinessSectorsTypes ToBusinessSectorType(this Id<IBusinessSector> sectorId)
        {
            return (BusinessSectorsTypes)sectorId.Value;
        }

        public static bool IsSupported(this BusinessSectorsTypes sectorType)
        {
            return BusinessSectors.Contains(sectorType);
        }

        public static List<BusinessSectorsTypes> GetBuildingSectors()
        {
            return BusinessSectors;
        }
    }
}