﻿using strange.extensions.signal.impl;


namespace NanoReality.GameLogic.BuildingSystem.City.Models.Api
{

    /// <summary>
    /// Вызывается когда происходит изменения населения
    /// </summary>
    public class SignalOnCityPopulationUpdated : Signal {}

    /// <summary>
    /// interface of the city population
    /// </summary>
    public interface ICityPopulation
    {
        /// <summary>
        /// Returns current population capacity
        /// </summary>
        /// <returns></returns>
        int GetPopulationCapacity();

        /// <summary>
        /// Returns total amount of people in the city (depends on citizen in dwellhouses)
        /// </summary>
        /// <returns></returns>
        int GetTotalCityPopulation();
    }
}