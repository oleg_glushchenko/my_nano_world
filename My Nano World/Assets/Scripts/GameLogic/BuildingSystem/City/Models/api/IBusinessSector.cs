﻿using Assets.NanoLib.Utilities;

namespace NanoReality.GameLogic.BuildingSystem.City.Models.Api
{
    /// <summary>
    /// Представляет бизнес сектор (локацию) в городе
    /// </summary>
    public interface IBusinessSector
    {
        /// <summary>
        /// contains type of the region
        /// </summary>
        Id<IBusinessSector> BusinessSectorType { get; set; }

        /// <summary>
        /// contains name of the current region
        /// </summary>
        string BusinessSectorName { get; set; }
    }
}
