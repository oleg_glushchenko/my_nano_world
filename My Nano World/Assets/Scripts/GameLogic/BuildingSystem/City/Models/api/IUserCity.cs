﻿using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using Newtonsoft.Json;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Impl;
using NanoReality.GameLogic.BuildingSystem.GlobalCityMap.Models;

namespace NanoReality.GameLogic.BuildingSystem.City.Models.Api
{
    public interface IUserCity
    {
        Id<IUserCity> Id { get; }
        
        string CityName { get; set; }
        
        CityStatistics CityStatistics { get; }
        
        /// <summary>
        /// Current happiness value.
        /// </summary>
        int Happiness { get; set; }
        
        List<ICityMap> CityMaps { get; set; }
        
        IGlobalCityMap GlobalCityMap { get; set; }

        ICityMap GetBusinessSectorById(BusinessSectorsTypes id);

        IMapObject GetMapObjectInCityByID(Id_IMapObject mapObjectID);

        IMapObject GetMapObjectInCityByType(MapObjectintTypes mapObjectType);
    }

    public class ShortCityData
    {
        [JsonProperty("object")]
        public CityData CityData;
    }

    public class CityData
    {
        [JsonProperty("city_name")]
        public string CityName { get; set; }

        [JsonProperty("city_id")]
        public Id<IUserCity> CityId { get; set; }

        [JsonProperty("level")]
        public int Level { get; set; }

        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("second_access_token")]
        public string SecondAccessToken { get; set; }

        [JsonProperty("device_id")]
        public string DeviceId { get; set; }

        [JsonProperty("population")]
        public int Population { get; set; }

        [JsonProperty("fb_id")]
        public string FacebookId { get; set; }

        [JsonProperty("game_center_id")]
        public string GameCenterId { get; set; }

        [JsonProperty("google_id")]
        public string GooglePlayId { get; set; }

        [JsonProperty("random_device_id")]
        public SecondAccountData SecondAccountData { get; set; }

        public override string ToString()
        {
            return string.Format("CityName: {0}, Level: {1}, Population: {2}, AccessToken: {3}, SecondAccessToken: {4}, DeviceId: {5}, Facebook: {6}, GC: {7}, GP: {8}",
                CityName ?? "null", Level, Population, AccessToken ?? "null",  SecondAccessToken ?? "null", DeviceId ?? "null", FacebookId ?? "null", GameCenterId ?? "null", GooglePlayId ?? "null");
        }
    }

    public class SecondAccountData
    {
        [JsonProperty("device_id")]
        public string DeviceId { get; set; }

        [JsonProperty("type")]
        public string DeviceType { get; set; }
    }
}
