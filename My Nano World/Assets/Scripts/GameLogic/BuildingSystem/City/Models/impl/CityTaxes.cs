﻿using Newtonsoft.Json;
using strange.extensions.signal.impl;

namespace NanoReality.GameLogic.BuildingSystem.CityMap.Models.Impl
{
    /// <summary>
    /// Вызывается когда происходит изменение налоговой ставки
    /// </summary>
    public class SignalOnCityTaxesChanged : Signal<CityTaxes> { }

    public class CityTaxes
    {
        [JsonProperty("max_taxes")]
        public int MaxTaxes { get; set; }

        [JsonProperty("current")]
        public int CurrentCoins { get; set; }
        
        [JsonProperty("peh")]
        public int PerHour { get; set; }

        [JsonProperty("max_cityhall")]
        public int MaxCityHall { get; set; }
        
        [JsonProperty("perSec")]
        public float CoinsPerSecond { get; set; }
        
        [JsonProperty("left_cityhall_time")]
        public float LeftCityHallTime { get; set; }
    }
}
