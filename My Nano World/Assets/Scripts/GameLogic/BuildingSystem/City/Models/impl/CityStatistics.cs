﻿using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using Newtonsoft.Json;

namespace NanoReality.GameLogic.BuildingSystem.CityMap.Models.Impl
{
    /// <summary>
    /// Объект статистики по городу
    /// </summary>
    public class CityStatistics 
    {
        [JsonProperty("produced_products")]
		public Dictionary<Id<Product>, int> ProducedProducts { get; set; }

        [JsonProperty("collected_coins")]
		public Dictionary<Id_IMapObjectType, int> CollectedCoinsData { get; set; }

        [JsonProperty("auction_earn_coins")]
        public int AuctionEarnedCoins { get; set; }

        [JsonProperty("auction_buy_lots")]
        public int AuctionPurchasedItemsCount { get; set; }

        [JsonProperty("auction_spend_coins")]
        public int AuctionSpendCoins { get; set; }

        [JsonProperty("auction_sell_lots")]
        public int AuctionSellItemsCount { get; set; }

        [JsonProperty("social_friends")]
        public int SocialFriendsInvites { get; set; }

        [JsonProperty("unlock_sub_sectors")]
        public int UnlockedSubSectors { get; set; }

        [JsonProperty("research")]
        public int ResearchCount { get; set; }

        public int CityRenamesCount { get; set; }

        public int TotalProducedProducts
        {
			get { return ProducedProducts.Sum(p => p.Value); }
        }

        public int TotalCollectCommercialBuilingCoins
        {
			get { return CollectedCoinsData.Sum(b => b.Value); }
        }

        public CityStatistics()
        {
			ProducedProducts = new Dictionary<Id<Product>, int> ();
			CollectedCoinsData = new Dictionary<Id_IMapObjectType, int> ();
        }
    }
}
