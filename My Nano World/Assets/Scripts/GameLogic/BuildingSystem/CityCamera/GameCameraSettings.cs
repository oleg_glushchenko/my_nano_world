﻿using UnityEngine;

namespace NanoReality.GameLogic.BuildingSystem.CityCamera
{
    [CreateAssetMenu(fileName = "GameCameraSettings", menuName = "NanoReality/GameCameraSettings", order = 999)]
    public sealed class GameCameraSettings : ScriptableObject
    {
	    #region Inspector

	    [Header("Zoom")] 
	    [SerializeField] private float _minZoom = 4f;
	    [SerializeField] private float _maxZoom = 10f;
	    [SerializeField] private float _defaultZoom;
	    [SerializeField] private float _minZoomRange = 4f;
	    [SerializeField] private float _maxZoomRange = 10f;
	    [SerializeField] private float _normalZoom = 10f;
	    

	    [SerializeField] private float _minZoomOverflow = 2f;
	    [SerializeField] private float _maxZoomOverflow = 15f;
	    [SerializeField] private float _zoomSensitivity = 1.2f;
	    [SerializeField] private float _autoZoomDefaultSpeed = 2f;
	    [SerializeField] private float _autoZoomSpeed = 4f;

	    [Header("Swipe")] 
	    [SerializeField] private float _swipeSensitivityMin = 5f;
	    [SerializeField] private float _swipeSensitivityMax = 20f;
	    [SerializeField] private float _autoSwipeSpeed = 3f;
	    
	    [Header("Object swipe")]
	    [SerializeField] private float _swipeObjectCoefficient;
	    [SerializeField] private Vector2 _minSwipeObjectBorder;
	    [SerializeField] private Vector2 _maxSwipeObjectBorder;
	    

	    [Header("Inertia")] 
	    [SerializeField] private float _inertialLag = 0.016f;
	    [SerializeField] private float _inertialDamp = 0.90f;
	    [SerializeField] private float _inertialThreshold = 0.02f;

	    #endregion

	    public float MinZoom => _minZoom;

	    public float MaxZoom => _maxZoom;
	    
	    public float MinZoomRange => _minZoomRange;
	    public float MaxZoomRange => _maxZoomRange;
	    public float NormalZoom => _normalZoom;


	    public float DefaultZoom => _defaultZoom;

	    public float MinZoomOverflow => _minZoomOverflow;

	    public float MaxZoomOverflow => _maxZoomOverflow;

	    public float AutoZoomDefaultSpeed => _autoZoomDefaultSpeed;

	    public float AutoZoomSpeed => _autoZoomSpeed;

	    public float ZoomSensitivity => _zoomSensitivity;

	    public float SwipeSensitivityMin => _swipeSensitivityMin;

	    public float SwipeSensitivityMax => _swipeSensitivityMax;

	    public float AutoSwipeSpeed => _autoSwipeSpeed;

	    public float SwipeObjectCoefficient => _swipeObjectCoefficient;
	    
	    public Vector2 MinSwipeObjectBorder => _minSwipeObjectBorder;
	    
	    public Vector2 MaxSwipeObjectBorder => _maxSwipeObjectBorder;

	    public float InertialLag => _inertialLag;

	    public float InertialDamp => _inertialDamp;

	    public float InertialThreshold => _inertialThreshold;
    }
}
