﻿using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using NanoReality.Engine.BuildingSystem.GlobalCityMap;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using UnityEngine;

namespace NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api
{
    /// <summary>
    /// Модель камеры, реализует позицию зум, свайп камеры
    /// </summary>
    public interface IGameCamera
    {
        void Init(CameraBounds bounds, Vector3F position, float angle, Camera camera);

        /// <summary>
        /// Enable or disable zoom
        /// </summary>
        /// <param name="state">true - enable zoom, false - disable zoom</param>
        void SetZoomStatus(bool state);

        /// <summary>
        /// calculate current zoom (taking into account sensivity)
        ///  </summary>
        void Zoom(float zoomStrenght);

        /// <summary>
        /// Calculate current auto zoom
        ///  </summary>
        /// <param name="hardAutoZoom">Если true, камера прекращает предыдущий зум и начинает новое, иначе запрос игнорируется</param> 
        void AutoZoom(float zoomStrenght, float speed = -1, bool hardAutoZoom = false);

        /// <summary>
        /// calculate swipe direction along with swipe sensivity
        ///  </summary>
        void Swipe(Vector3F swipeDirection);

        /// <summary>
        /// move camera to some position
        ///  </summary>
        /// <param name="hardAutoSwipe">Если true, камера прекращает предыдущее перемещение и начинает новое, иначе запрос игнорируется</param> 
        void SwipeTo(Vector3F swipeDirection, CameraSwipeMode mode, float speed = -1, bool hardAutoSwipe = false);

        /// <summary>
        /// Move camera to some position (object on map)
        /// </summary>
        /// <param name="hardAutoSwipe">Если true, камера прекращает предыдущее перемещение и начинает новое, иначе запрос игнорируется</param> 
        void SwipeTo(Vector2F swipePosition, CameraSwipeMode mode, float speed = -1, bool hardAutoSwipe = false);

        /// <summary>
        /// Move camera to some position and zoom it (object on map)
        /// </summary>
        /// <param name="hardAutoSwipe">Если true, камера прекращает предыдущее перемещение и начинает новое, иначе запрос игнорируется</param>
        void ZoomTo(Vector2F swipePosition, CameraSwipeMode mode, float zoomStrenght, float speed = -1,
            bool hardAutoSwipe = false);

        /// <summary>
        /// Center camera to some object with zoom percent
        /// </summary>
        /// <param name="target"></param>
        /// <param name="mode"></param>
        /// <param name="zoomPercent"></param>
        /// <param name="speed"></param>
        /// <param name="hardAutoSwipe">Если true, камера прекращает предыдущее перемещение и начинает новое, иначе запрос игнорируется</param>
        void ZoomTo(IMapObject target, CameraSwipeMode mode, float zoomStrenght, float speed = -1,
            bool hardAutoSwipe = false);

        /// <summary>
        /// Center camera to some object with zoom percent
        /// </summary>
        /// <param name="view"></param>
        /// <param name="mode"></param>
        /// <param name="zoomPercent"></param>
        /// <param name="speed"></param>
        /// <param name="hardAutoSwipe">Если true, камера прекращает предыдущее перемещение и начинает новое, иначе запрос игнорируется</param>
        void ZoomTo(AGlobalCityBuildingView view, CameraSwipeMode mode, float zoomPercent, float speed = -1,
            bool hardAutoSwipe = false);

        /// <summary>
        /// Center camera at a map object
        /// </summary>
        /// <param name="mapObject"></param>
        void FocusOnMapObject(IMapObject mapObject);

        /// <summary>
        /// Returns position of a map object on terrain
        /// </summary>
        /// <param name="mapObject"></param>
        /// <returns></returns>
        Vector2F GetMapObjectPosition(IMapObject mapObject);

        /// <summary>
        /// Align the camera to some position of city
        /// </summary>
        void AlignCamera(ICityMap cityMap, CameraAlignPosition align, CameraSwipeMode mode, float speed = -1);

        void SetGrayscaleActive(bool value);

        Vector3 WorldToScreenPoint(Vector3 position);

        Vector3 ScreenToWorldPoint(Vector3 position);
        Vector3 WorldToViewportPoint(Vector3 position);
        Vector3 ScreenToViewportPoint(Vector3 position);
        
        void SetActive(bool value);

        bool RenderTheWholeObject(Bounds bounds);
    }

    /// <summary>
    /// How to move camera
    /// </summary>
    public enum CameraSwipeMode
    {
        Jump,
        Fly
    }

    /// <summary>
    /// Destanation camera position
    /// </summary>
    public enum CameraAlignPosition
    {
        Top,
        Left,
        Right,
        Bottom,
        Center
    }
}