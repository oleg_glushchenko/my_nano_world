using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.StartApplicationSequence.Models.api;

namespace Assets.Scripts.GameLogic.BuildingSystem.Products.Models.api
{
    /// <summary>
    /// Provides information about products
    /// </summary>
    public interface IProductsData : IGameBalanceData
    {
        List<Product> Products { get; }

        Product GetProduct(Id<Product> productId);
        
        Product GetProduct(int productId);
    }
}
