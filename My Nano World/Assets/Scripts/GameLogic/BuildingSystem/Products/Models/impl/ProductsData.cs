﻿using System;
using System.Collections.Generic;
using Assets.NanoLib.UtilitesEngine.CustomDebugging.Models.api;
using Assets.NanoLib.Utilities;
using Assets.NanoLib.Utilities.DataManager.Models.api;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.api;
using NanoLib.Core.Logging;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.Settings.BuildSettings.Models.Api;
using NanoReality.GameLogic.StartApplicationSequence.Models.api;
using Newtonsoft.Json;

namespace Assets.Scripts.GameLogic.BuildingSystem.Products.Models.impl
{
    /// <summary>
    /// Provides information about products
    /// </summary>
    [Serializable]
    public class ProductsData : IProductsData
    {
        [JsonProperty("products")]
		public List<Product> Products { get; set; }

        private readonly Dictionary<int, Product> _productsDic;

        public ProductsData()
        {
			Products = new List<Product>();
            _productsDic = new Dictionary<int, Product>();
        }

        public Product GetProduct(Id<Product> productId)
        {
            return GetProduct(productId.Value);
        }
        
        public Product GetProduct(int productId)
        {
            if (_productsDic.TryGetValue(productId, out Product product))
            {
                return product;
            }
            
            Logging.LogWarning(LoggingChannel.Debug,"Not found product id: " + productId);

            return null;
        }

        #region IGameBalanceData

        [JsonIgnore]
        public string DataHash { get; set; }

        [JsonIgnore]
        private Action<IGameBalanceData> _gameBalanceUpdateCallback;


        public void LoadData(IServerCommunicator serverCommunicator, ILocalDataManager localDataManager, Action<IGameBalanceData> callback)
        {
            _gameBalanceUpdateCallback = callback;
            serverCommunicator.LoadProducts(this, OnUpdateProductsFromServer);
        }

        private void OnUpdateProductsFromServer(IProductsData data)
        {
            for (int i = 0; i < data.Products.Count; i++)
            {
                _productsDic.Add(Products[i].ProductTypeID,Products[i]);
            }

            _gameBalanceUpdateCallback?.Invoke(this);
        }
        #endregion
    }
}