﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Assets.NanoLib.ServerCommunication.Models;
using Assets.NanoLib.UtilitesEngine;
using Assets.NanoLib.UtilitesEngine.CustomDebugging.Models.api;
using Assets.NanoLib.Utilities;
using Assets.NanoLib.Utilities.Serializers;
using Assets.Scripts.Engine.Analytics.Model.api;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.api;
using Assets.Scripts.GameLogic.DailyBonus.Api;
using NanoReality.GameLogic.Achievements;
using NanoReality.GameLogic.Achievements.impl;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.OrderDesk;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.SoftCurrencyStore;
using NanoReality.GameLogic.StartApplicationSequence.Controllers.Commands;
using Newtonsoft.Json;
using UnityEngine;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Impl;
using NanoReality.GameLogic.BuildingSystem.MapObjects;
using NanoReality.Game.Tutorial;
using GameLogic.Bazaar.Model.impl;
using Assets.Scripts.GameLogic.DailyBonus.Impl;
using GameLogic.BuildingSystem.CityAoe;
using GameLogic.Taxes.Service;
using GameLogic.Utilites;
using JetBrains.Annotations;
using NanoLib.Core.Logging;
using NanoReality.Engine.Utilities;
using NanoReality.Game.CitizenGifts;
using NanoReality.Game.CityStatusEffects;
using NanoReality.Game.FoodSupply;
using NanoReality.Game.Production;
using NanoReality.Game.Services.BuildingsUnlockService;
using NanoReality.Game.Services.OrderDesk.SeaportOrderDesk;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.GameLogic.BuildingSystem.CityArea;
using NanoReality.GameLogic.BuildingSystem.GlobalCityMap.Models;
using NanoReality.GameLogic.Configs;
using NanoReality.GameLogic.GameManager.Controllers;
using NanoReality.GameLogic.IAP;
using NanoReality.GameLogic.Settings.GamePrefs;
using UnityEngine.Purchasing;
using Product = NanoReality.GameLogic.BuildingSystem.Products.Models.Api.Product;
using NanoReality.Debugging;
using NanoReality.Game.Data;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.TheEvent.Data;
using UnityEngine.Networking;

// ReSharper disable once CheckNamespace
// ReSharper disable HeuristicUnreachableCode

namespace NanoReality.GameLogic.ServerCommunication.Models.Api
{
    public partial class ServerCommunicator : IServerCommunicator
    {
        #region InjectsData

        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public IBuildSettings BuildSettings { get; set; }
        [Inject] public IGamePrefs jGamePrefs { get; set; }
        [Inject] public IDebug jDebug { get; set; }
        [Inject] public IGameManager GameManager { get; set; }
        [Inject] public IStringSerializer jStringSerializer { get; set; }
        [Inject] public IServerRequest jServerRequest { get; set; }
        [Inject] public ITimerManager jTimeManager { get; set; }
        [Inject] public INanoAnalytics jNanoAnalytics { get; set; }

        #endregion

        public bool IsAuthorized { get; private set; }
        public string AccessToken { get; set; }

        private SubCommunicatorBuildingBaseData _subCommunicatorBuildingBaseData;

        public event Action<IErrorData> ErrorReceived;

        #region Unsorted Methods

        [PostConstruct]
        public void PostConstruct()
        {
            _subCommunicatorBuildingBaseData = new SubCommunicatorBuildingBaseData(this);
        }

        public void OnRequestError(IErrorData errorData)
        {
            ErrorReceived?.Invoke(errorData);
        }

        public void StopAndClearServerQueue()
        {
            jServerRequest.StopAndClearRequestQueue();
        }

        private static string GetResponseValue(string response, string key)
        {
            try
            {
                var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(response);
                dict = SerializationConverters.DeserializeObjectWithoutConverters(response, dict.GetType()) as
                        Dictionary<string, string>;
                string value;
                return dict != null && dict.TryGetValue(key, out value) ? value : null;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Authorize device on server, using UDID
        /// </summary>
        /// <param name="callback"></param>
        public void Authorize(Action<Dictionary<string, string>> callback = null)
        {
            var deviceId = string.IsNullOrEmpty(BuildSettings.CustomUdid)
                ? jGamePrefs.DeviceId
                : BuildSettings.CustomUdid;

            RequestCompleteDelegate onAuthorizeCompleted = responseText =>
            {
                var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseText);
                dict =
                    SerializationConverters.DeserializeObjectWithoutConverters(responseText, dict.GetType()) as
                        Dictionary<string, string>;

                if (dict != null)
                {
                    dict.Add("isNewUser", deviceId);

                    AccessToken = dict["access_token"];
                    IsAuthorized = true;
                    callback?.Invoke(dict);
                }
                else
                {
                    jDebug.LogError(responseText);
                    jDebug.LogError("Could not deserialize json data!");
                }
            };

            var type = "android";

#if UNITY_IOS || UNITY_IPHONE
            type = "ios";
#endif
            var parameters = new Dictionary<string, string>
            {
                {"device_id", deviceId},
                {"type", type}
            };

            jServerRequest.SendRequest(RequestMethod.POST, parameters, "/v1/auth/login", onAuthorizeCompleted,
                OnRequestError);
        }

        public void Logout(Action callback)
        {
            void Complete(string responseText)
            {
                callback?.Invoke();
            }

            jServerRequest.SendRequest(RequestMethod.POST, "/v1/auth/logout", Complete, OnRequestError);
        }

        public void LoadGameBalanceHashes(LoadGameDataCommand.UrlEtags clientEtags, Action<LoadGameDataCommand.UrlEtags> callback = null)
        {
            void Complete(String responseText)
            {
                jStringSerializer.DeserializeAsync(responseText, callback);
            }

            var tags = jStringSerializer.Serialize(clientEtags.Etags);
            var parameters = new Dictionary<string, string>() { { "etags", tags } };

            jServerRequest.SendRequest(RequestMethod.GET, parameters, "/v1/game/cache", Complete, OnRequestError);
        }

        /// <summary>
        /// Bind Social Network ID to current account Id
        /// </summary>
        /// <param name="socialNetwrokType">social netwrok type</param>
        /// <param name="socialId">social id</param>
        /// <param name="world">принадлежность мира</param>
        /// <param name="callback">success callback</param>
        public void BindSocialNetwork(string socialNetwrokType, string socialId, CommandOnAutorizeInSocialNetwork.SelectWorld world, Action<ShortCityData> callback = null)
        {
            RequestCompleteDelegate complete = responseText =>
            {
                jDebug.Log("Add Social Network response: " + responseText, true);

                ShortCityData city = null;
                try
                {
                    city = jStringSerializer.Deserialize(responseText, typeof(ShortCityData)) as ShortCityData;
                    if (city == null || city.CityData == null)
                    {
                        AccessToken = GetAccessTokenFromResponce(responseText);
                    }

                    if (callback != null) callback(city);
                }
                catch (JsonException e)
                {
                    // ошибка десериализации свидетельствует о получении ответа не содержащего информации о старом прогрессе, вследствии его отсутствия 
                    AccessToken = GetAccessTokenFromResponce(responseText);
                    if (callback != null) callback(city);
                    jDebug.LogWarning(e.Message + "\n" + e.StackTrace);
                }
            };

            var type = "android";

#if UNITY_IOS || UNITY_IPHONE
            type = "ios";
#endif

            var data = new Dictionary<string, string>
            {
                {"device_type", type},
                {"type", socialNetwrokType},
                {"social_id", socialId}
            };

            if (world != CommandOnAutorizeInSocialNetwork.SelectWorld.None)
                data.Add("method", world == CommandOnAutorizeInSocialNetwork.SelectWorld.Current ? "current" : "other");

            jDebug.Log("Add Social Network: POST /v1/user/social-networking/" + type + socialNetwrokType + socialId, true);

            jServerRequest.SendRequest(RequestMethod.POST, data, "/v1/user/social-networking",
                complete, OnRequestError);
        }

        /// <summary>
        /// Get device id binded to social network ID
        /// </summary>
        /// <param name="socialNetwrokType">social network type</param>
        /// <param name="socialId">social id</param>
        /// <param name="callbackAction">success callback</param>
        public void BindDeviceIdToSocialNetwork(string socialNetwrokType, string socialId, Action callbackAction = null)
        {
            RequestCompleteDelegate complete = responseText =>
            {
                jDebug.Log("BindDeviceIdToSocialNetwork response: " + responseText, true);
                AccessToken = GetAccessTokenFromResponce(responseText);
            };

            var parameters = new Dictionary<string, string>
            {
                {"type", socialNetwrokType},
                {"device_id", jGamePrefs.DeviceId}
            };

            jDebug.Log("BindDeviceIdToSocialNetwork: PUT /v1/user/social-networking/" + parameters, true);

            jServerRequest.SendRequest(RequestMethod.PUT, parameters, "/v1/user/social-networking/" + socialId,
                complete, OnRequestError);
        }

        private string GetAccessTokenFromResponce(string data)
        {
            var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            dict =
            SerializationConverters.DeserializeObjectWithoutConverters(data, dict.GetType()) as
                Dictionary<string, string>;
            if (dict != null && dict.ContainsKey("access_token"))
            {
                return dict["access_token"];
            }

            return AccessToken;
        }

        public void LoadMapBuildingsPremiumSkipData(IMapBuildingsSkipData data, Action<IMapBuildingsSkipData> callback)
        {
            if (callback != null) callback(data);
        }

        public void GetHappinessInfo(Action<int> onSuccess)
        {
            string url = "/v1/user/building/option/0";
            RequestMethod method = RequestMethod.GET;

            void OnComplete(string jsonResponse)
            {
                jStringSerializer.DeserializeAsync(jsonResponse, onSuccess);
            }

            jServerRequest.SendRequest(method, url, OnComplete, OnRequestError);
        }

        #endregion

        #region Taxes

        public void GetTaxesInfo(Action<CityTaxes> callback)
        {
            RequestCompleteDelegate complete = responseText =>
            {
                jStringSerializer.DeserializeAsync(responseText, callback);
            };

            jServerRequest.SendRequest(RequestMethod.GET, null, "/v1/user/taxes", complete, OnRequestError);
        }

        public void CollectTaxes(Action<int, CityTaxes> callback)
        {
            void OnComplete(string responseText)
            {
                var deserializedResponse = JsonConvert.DeserializeObject<CollectTaxesNetworkResponse>(responseText);
                if (deserializedResponse != null)
                {
                    callback(deserializedResponse.CollectedCoins, deserializedResponse.CityTaxes);
                }
            }

            jServerRequest.SendRequest(RequestMethod.PUT, null, "/v1/user/taxes/1", OnComplete, OnRequestError);
        }
        
        #endregion

        #region EditMode

        public void GetEditModeLayouts(Action<List<EditModeLayout>> onComplete)
        {
            void OnComplete(string responseText)
            {
                if (jStringSerializer.Deserialize(responseText, typeof(List<EditModeLayout>)) is List<EditModeLayout> deserializedResponse)
                {
                    onComplete(deserializedResponse);
                }
            }

            jServerRequest.SendRequest(RequestMethod.GET, null, "/v1/user/layout", OnComplete, OnRequestError);
        }

        public void GetEditModeLayoutData(int layoutId, Action<EditModeLayoutData> onComplete)
        {
            void OnComplete(string responseText)
            {
                var deserializedResponse = JsonConvert.DeserializeObject<EditModeLayoutData>(responseText);
                if (deserializedResponse != null)
                {
                    onComplete(deserializedResponse);
                }
            }

            jServerRequest.SendRequest(RequestMethod.GET, null, "/v1/user/layout/edit/"+layoutId, OnComplete, OnRequestError);
        }

        public void EditModeClearMap(int layoutId, Action<EditModeLayoutData> onComplete)
        {
            void OnComplete(string responseText)
            {
                var deserializedResponse = JsonConvert.DeserializeObject<EditModeLayoutData>(responseText);
                if (deserializedResponse != null)
                {
                    onComplete(deserializedResponse);
                }
            }

            jServerRequest.SendRequest(RequestMethod.GET, null, "/v1/user/layout/clear/"+layoutId, OnComplete, OnRequestError);
        }

        public void EditModeRemoveBuilding(int layoutObjectId, Action<EditModeHappinessMap> onComplete)
        {
            void OnComplete(string responseText)
            {
                var deserializedResponse = JsonConvert.DeserializeObject<EditModeHappinessMap>(responseText);
                onComplete(deserializedResponse);
            }

            jServerRequest.SendRequest(RequestMethod.GET, null, "/v1/user/layout/remove/"+layoutObjectId, OnComplete, OnRequestError);
        }

        public void EditModeApplyLayout(int layoutId, string screenshot, Action<bool> onComplete)
        {
            void OnComplete(string responseText)
            {
                var deserializedResponse = JsonConvert.DeserializeObject<bool>(responseText);
                onComplete(deserializedResponse);
            }
            
            var parameters = new Dictionary<string, string>
            {
                {"image", screenshot},
            };

            jServerRequest.SendRequest(RequestMethod.POST, parameters, "/v1/user/layout/apply/"+layoutId, OnComplete, OnRequestError);
        }
        
        public void EditModeCancelLayout(int layoutId, string screenshot, Action<bool> onComplete)
        {
            void OnComplete(string responseText)
            {
                var deserializedResponse = JsonConvert.DeserializeObject<bool>(responseText);
                onComplete(deserializedResponse);
            }
            
            var parameters = new Dictionary<string, string>
            {
                {"image", screenshot},
            };

            jServerRequest.SendRequest(RequestMethod.POST, parameters, "/v1/user/layout/cancel/"+layoutId, OnComplete, OnRequestError);
        }

        public void EditModeChangeAOE(int layoutObjectId, int aoeType, Action<EditModeHappinessMap> onComplete)
        {
            void OnComplete(string responseText)
            {
                var deserializedResponse = JsonConvert.DeserializeObject<EditModeHappinessMap>(responseText);
                onComplete(deserializedResponse);
            }

            var parameters = new Dictionary<string, string>
            {
                {"aoe_type", aoeType.ToString()},
            };

            jServerRequest.SendRequest(RequestMethod.POST, parameters, "/v1/user/layout/change-aoe/"+layoutObjectId, OnComplete, OnRequestError);
        }

        public void EditModeMoveBuilding(int layoutObjectId, Vector2Int position, Action<EditModeHappinessMap> onComplete)
        {
            void OnComplete(string responseText)
            {
                var deserializedResponse = JsonConvert.DeserializeObject<EditModeHappinessMap>(responseText);
                if (deserializedResponse != null)
                {
                    onComplete(deserializedResponse);
                }
            }

            var parameters = new Dictionary<string, string>
            {
                {"x", position.x.ToString()},
                {"y", position.y.ToString()},
            };

            jServerRequest.SendRequest(RequestMethod.POST, parameters, "/v1/user/layout/move/"+layoutObjectId, OnComplete, OnRequestError);
        }

        public void EditModeRestoreBuilding(int layoutId, int mapId, Vector2Int position, Action<EditModeHappinessMap> onComplete)
        {
            void OnComplete(string responseText)
            {
                var deserializedResponse = JsonConvert.DeserializeObject<EditModeHappinessMap>(responseText);
                if (deserializedResponse != null)
                {
                    onComplete(deserializedResponse);
                }
            }

            var parameters = new Dictionary<string, string>
            {
                { "player_building_id", mapId.ToString() },
                { "x", position.x.ToString() },
                { "y", position.y.ToString() },
            };

            jServerRequest.SendRequest(RequestMethod.POST, parameters, "/v1/user/layout/add/" + layoutId, OnComplete, OnRequestError);
        }

        public void EditModeAddRoad(int layoutId, string bssId, List<Vector2Int> positions, Action<EditModeCreateRoadResponse> onComplete)
        {
            void OnComplete(string responseText)
            {
                var deserializedResponse = JsonConvert.DeserializeObject<EditModeCreateRoadResponse>(responseText);
                if (deserializedResponse != null)
                {
                    onComplete(deserializedResponse);
                }
            }

            List<Vector2> coordinates = new List<Vector2>();

            foreach (var position in positions)
            {
               coordinates.Add(new Vector2(position.x,position.y));
            }
            
            string coords = jStringSerializer.Serialize(coordinates);
            
            var parameters = new Dictionary<string, string>
            {
                {"business_sector_id", bssId},
                {"coordinates", coords}
            };

            jServerRequest.SendRequest(RequestMethod.POST, parameters, "/v1/user/layout/create-road/"+layoutId,
                OnComplete, OnRequestError);
        }

        public void EditModeRemoveRoad(int layoutId, string bssId, List<Id_IMapObject> roads, Action<EditModeDeleteRoadResponse> onComplete)
        {
            void OnComplete(string responseText)
            {
                var deserializedResponse = JsonConvert.DeserializeObject<EditModeDeleteRoadResponse>(responseText);
                if (deserializedResponse != null)
                {
                    onComplete(deserializedResponse);
                }
            }
            
            var idsList = roads.Select(road => road.ToString()).ToList();

            string ids = jStringSerializer.Serialize(idsList);
            var parameters = new Dictionary<string, string>
            {
                {"business_sector_id", bssId},
                {"ids", ids}
            };

            jServerRequest.SendRequest(RequestMethod.POST, parameters, "/v1/user/layout/delete-road/"+layoutId,
                OnComplete, OnRequestError);
        }

        #endregion

        #region BuildingBaseData

        /// <summary>
        /// Gives a list of all mapObjects that have been add in the admin panel
        /// </summary>
        public void LoadMapObjects(Action<IMapObjectsData> callback)
        {
            string url = "/v1/game/building";
            RequestMethod requestMethod = RequestMethod.GET;

            void OnLoadedComplete(string responseText)
            {
                void OnDeserializeComplete(MapObjectsData parsedData)
                {
                    callback?.Invoke(parsedData);
                }

                jStringSerializer.DeserializeAsync<MapObjectsData>(responseText, OnDeserializeComplete);
            }

            jServerRequest.SendRequestHash(requestMethod, null, url, OnLoadedComplete, OnRequestError);
        }

        public void ConstructMapObject(int businessSectorId, int buildingId, Vector2Int buildingPosition, Action<int> onSuccess)
        {
            string url = "/v1/user/building";
            RequestMethod method = RequestMethod.POST;

            int cityId = GameManager.CurrentUserCity.Id.Value;

            var parameters = new Dictionary<string, string>
            {
                {"building_id", buildingId.ToString()},
                {"business_sector_id", businessSectorId.ToString()},
                {"city_id", cityId.ToString()},
                {"x", buildingPosition.x.ToString()},
                {"y", buildingPosition.y.ToString()}
            };

            void Complete(string responseString)
            {
                int buildingMapId = Convert.ToInt32(responseString);
                onSuccess(buildingMapId);
            }

            jServerRequest.SendRequest(method, parameters, url, Complete, OnRequestError);
        }

        private void BuyProduct(int productId, string token, string orderId, Action<int, bool> callback, string url)
        {
            var parameters = new Dictionary<string, string>
            {
                {"token", token},
                {"orderId", orderId},
#if UNITY_EDITOR || UNITY_STANDALONE
                {"debug", "1"}  
#else
                {"debug", "0"}
#endif
            };

            void OnComplete(string jsonString)
            {
                var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonString);
                dict =
                    SerializationConverters.DeserializeObjectWithoutConverters(jsonString, dict.GetType()) as
                        Dictionary<string, string>;

                bool success = false;

                if (dict != null)
                {
                    if (dict["status"].Equals("success"))
                    {
                        success = true;
                    }
                    if (!success)
                    {
                        jNanoAnalytics.OnInAppPurchaseFail(orderId, dict["message"]);
                    }
                }

                callback?.Invoke(productId, success);
            }

            // void OnPurchaseRequestError(IErrorData errorData)
            // {
            //     const string TimeOutKey = "time_out";
            //
            //     var time = default(int);
            //
            //     var errorAnswer = JsonUtility.FromJson<ErrorResponse>(errorData.Response);
            //     var messageObject = MiniJson.JsonDecode(errorAnswer.message);
            //     if (messageObject != null)
            //     {
            //         var message = (Dictionary<string, object>)messageObject;
            //         if (message.ContainsKey(TimeOutKey))
            //         {
            //             time = Convert.ToInt32(message[TimeOutKey]);
            //         }
            //     }
            //
            //     callback?.Invoke(time, false);
            // }

            jServerRequest.SendRequest(RequestMethod.POST, parameters, url, OnComplete, OnRequestError);
        }

        public void ConstructRoad(int businessSectorId, int type, List<Vector2Int> positionsList, Action<CreateRoadResponse> onSuccess)
        {
            string url = "/v1/user/road";
            RequestMethod requestMethod = RequestMethod.POST;

            int cityId = GameManager.CurrentUserCity.Id.Value;
            string coords = jStringSerializer.Serialize(positionsList);

            var header = new Dictionary<string, string>
            {
                {"city-id", cityId.ToString()}
            };

            var parameters = new Dictionary<string, string>
            {
                {"city_id", cityId.ToString()},
                {"business_sector_id", businessSectorId.ToString()},
                {"coordinates", coords.Replace(".0", "")},
                {"building_id", type.ToString()}
            };

            void OnCompete(string jsonResponse)
            {
                jStringSerializer.DeserializeAsync(jsonResponse, onSuccess);
            }

            jServerRequest.SendRequest(requestMethod, parameters, url, OnCompete, OnRequestError, header);
        }

        public void SyncBuilding(int buildingMapId, Action<SyncBuildingResponse> onSuccess)
        {
            string url = $"/v1/user/building/{buildingMapId}";
            RequestMethod requestMethod = RequestMethod.GET;

            RequestCompleteDelegate complete = responceText =>
            {
                jStringSerializer.DeserializeAsync(responceText, onSuccess);
            };

            jServerRequest.SendRequest(requestMethod, url, complete, OnRequestError);
        }

        public void BuildingSkip(Func<string> GetMapId, Action onSuccess)
        {
            _subCommunicatorBuildingBaseData.BuildingSkip(GetMapId, onSuccess, OnRequestError);
        }

        public void UpgradeMapObject(int buildingMapId, Action onSuccess)
        {
            var url = $"/v1/user/building/{buildingMapId.ToString()}";
            var method = RequestMethod.PUT;
            var parameters = new Dictionary<string, string>
            {
                {"action", "upgrade"}
            };

            void OnComplete(string response)
            {
                onSuccess?.Invoke();
            }

            jServerRequest.SendRequest(method, parameters, url, OnComplete, OnRequestError);
        }

        public void PushUpgradeProduct(Func<string> mapId, Id<Product> productID, Action onSuccess)
        {
            _subCommunicatorBuildingBaseData.PushUpgradeProduct(mapId, productID, onSuccess, OnRequestError);
        }

        public void MoveMapObject(Func<string> mapId, Vector2F newPosition, Action<MoveMapObjectResponce> onSuccess)
        {
            _subCommunicatorBuildingBaseData.MoveMapObject(mapId, newPosition, onSuccess, OnRequestError);
        }

        public void SwitchAoeLayoutType(Func<string> mapId, AoeLayout aoeLayout, bool isEnegry, Action<MoveMapObjectResponce> onSuccsess)
        {
            _subCommunicatorBuildingBaseData.SwitchAoeLayout(mapId, aoeLayout, isEnegry, onSuccsess, OnRequestError);
        }

        public void DeleteRoad(List<Id_IMapObject> ids, Action<DeleteRoadResponse> onSuccess)
        {
            _subCommunicatorBuildingBaseData.DeleteRoad(ids, onSuccess, OnRequestError, jGameManager);
        }
        public void RepairBuilding(Id_IMapObject mapID, Action onServerConfirmedRepair)
        {
            RequestCompleteDelegate complete = responseText =>
            {
                onServerConfirmedRepair();
            };

            var parameters = new Dictionary<string, string>
            {
                {"id", mapID.Value.ToString()}
            };

            jServerRequest.SendRequest(RequestMethod.POST, parameters,
                "/v1/user/building-repair", complete,
                OnRequestError);
        }

        #endregion

        #region Achievements

        public void LoadAchievementsData(Action<List<Achievement>> onSuccess)
        {
            void Complete(string response)
            {
                jStringSerializer.DeserializeAsync(response, onSuccess);
            }

            jServerRequest.SendRequestHash(RequestMethod.GET, null, "/v1/game/achieve",
                Complete, OnRequestError);
        }

        public void LoadUserAchievementsData(Action<IUserAchievementsData> onSuccess)
        {
            void Complete(string response)
            {
                jStringSerializer.DeserializeAsync(response, onSuccess);
            }

            jServerRequest.SendRequest(RequestMethod.GET, null, "/v1/quest/player-achieve",
                Complete, OnRequestError);
        }

        public void VerifyAchievementUnlocked(Id<Achievement> id, Action onSuccess)
        {
            onSuccess();
        }

        public void TakeAchievementAward(Id<Achievement> id, Action<List<IAwardAction>> onSuccess)
        {
            RequestCompleteDelegate complete = responseText =>
            {
                jDebug.Log("TakeAchievementAward: " + responseText, true);

                var awards = jStringSerializer.Deserialize(responseText, typeof(List<IAwardAction>)) as List<IAwardAction>;

                onSuccess(awards);
            };

            var parameters = new Dictionary<string, string>
                {
                    {"id", id.Value.ToString()},
                    {"type", "achieve"}
                };

            jServerRequest.SendRequest(RequestMethod.PUT, parameters, "/v1/quest/player-award/" + id.Value, complete, OnRequestError);
        }

        #endregion

        #region Quests

        public void LoadQuestBalanceData(Action<IEnumerable<IQuest>> onSuccess)
        {
            void Complete(string response)
            {
                jStringSerializer.DeserializeAsync<List<IQuest>>(response, quests =>
                {
                    onSuccess?.Invoke(quests);
                });
            }

            jServerRequest.SendRequestHash(RequestMethod.GET, null, "/v1/game/quest", Complete, OnRequestError);
        }

        public void LoadUserQuestsData(IUserQuestData userQuestData, Action<IUserQuestData> onSuccess)
        {
            void OnComplete(string text)
            {
                userQuestData = jStringSerializer.Deserialize(text, userQuestData.GetType()) as IUserQuestData;
                onSuccess?.Invoke(userQuestData);
            }

            jServerRequest.SendRequest(RequestMethod.GET, null, "/v1/quest/player-quest", OnComplete, OnRequestError);
        }

        public void StartQuest(Id<IQuest> questId, Action onSuccess = null)
        {
            void OnComplete(string text)
            {
                onSuccess?.Invoke();
            }

            var parameters = new Dictionary<string, string>
            {
                { "quest_id", questId.Value.ToString() },
                { "offset", jTimeManager.CurrentUTC.ToString() },
            };

            jServerRequest.SendRequest(RequestMethod.POST, parameters, "/v1/quest/player-quest", OnComplete, OnRequestError);
        }

        public void CollectQuest(Id<IQuest> questId, Id<IUserCity> cityId, Action<List<IAwardAction>> onSuccess)
        {
            void OnComplete(string responseText)
            {
                var awards = jStringSerializer.Deserialize(responseText, typeof(List<IAwardAction>)) as List<IAwardAction>;

                onSuccess(awards);
            }

            var headers = new Dictionary<string, string>
            {
                {"city-id", cityId.Value.ToString()}
            };

            var parameters = new Dictionary<string, string>
            {
                { "id", questId.Value.ToString() }

            };

            jServerRequest.SendRequest(RequestMethod.PUT, parameters, "/v1/quest/player-award/" + questId.Value, OnComplete, OnRequestError, headers);
        }

        public void CollectQuestsAwards(List<int> questsId, Action<Dictionary<int, List<IAwardAction>>> onSuccess)
        {
            void OnComplete(string responseText)
            {
                var awards = jStringSerializer.Deserialize(responseText, typeof(Dictionary<int, List<IAwardAction>>)) as Dictionary<int, List<IAwardAction>>;

                onSuccess(awards);
            }

            var quests = jStringSerializer.Serialize(questsId);
            var parameters = new Dictionary<string, string> { { "quests", quests } };

            jServerRequest.SendRequest(RequestMethod.POST, parameters, "/v1/quest/player-award", OnComplete, OnRequestError);
        }

        public void VerifyQuestUnlocked(int questId, Action onSuccess = null)
        {
            void OnComplete(string text)
            {
                onSuccess?.Invoke();
            }

            jServerRequest.SendRequest(RequestMethod.GET, null, $"/v1/quest/player-quest/{questId}", OnComplete, OnRequestError);
        }

        public void VerifyQuestFinished(int questId, Action onSuccess = null)
        {
            void OnComplete(string text)
            {
                onSuccess?.Invoke();
            }

            var parameters = new Dictionary<string, string>
            {
                {"offset", jTimeManager.CurrentUTC.ToString()}
            };

            jServerRequest.SendRequest(RequestMethod.PUT, parameters, "/v1/quest/player-quest/" + questId, OnComplete, OnRequestError);
        }

        #endregion

        #region Monetization

        public void LoadInAppShopData(Action<InAppShopData> callback)
        {
            void OnDeserializeComplete(InAppShopData data)
            {
                var storeName = Application.platform == RuntimePlatform.IPhonePlayer ? "apple" : "google";
                var platformProductsData = new InAppShopData
                {
                    Products = data.Products.Where(product => product.StoreName == storeName).ToArray()
                };

                callback?.Invoke(platformProductsData);
            }

            void OnComplete(string jsonString)
            {
                jStringSerializer.DeserializeAsync<InAppShopData>(jsonString, OnDeserializeComplete);
            }

            jServerRequest.SendRequest(RequestMethod.GET, "/v1/purchase/offer/common", OnComplete, OnRequestError);
        }

        public void LoadInAppShopOfferData(Action<InAppShopOffersData> callback)
        {
            void OnDeserializeComplete(InAppShopOffersData data)
            {
                callback?.Invoke(data);
            }

            void OnComplete(string jsonString)
            {
                jStringSerializer.DeserializeAsync<InAppShopOffersData>(jsonString, OnDeserializeComplete);
            }

            jServerRequest.SendRequest(RequestMethod.GET, "/v1/purchase/offer/special", OnComplete, OnRequestError);
        }

        public void BuyInAppProduct(int productId, string orderId, string token, Action<int, bool> callback)
        {
            BuyProduct(productId, token, orderId, callback, $"/v1/purchase/offer/buy/{productId}");
        }

        public void LoadSoftCurrencyBundles(Action<ISoftCurrencyBundlesModel> onSuccess)
        {
            void OnComplete(string responseText)
            {
                responseText = responseText.WrapToJsonObjectWithField("SoftCurrencyBundles");
                var result = jStringSerializer.Deserialize<ISoftCurrencyBundlesModel>(responseText);

                onSuccess?.Invoke(result);
            }

            jServerRequest.SendRequestHash(RequestMethod.GET, null, "/v1/game/coin-package", OnComplete, OnRequestError);
        }

        public void PurchaseSoftCurrencyBundle(string bundleId, Action callback)
        {
            void OnComplete(string responseText)
            {
                jDebug.Log(responseText, true);
                callback?.Invoke();
            }

            var parameters = new Dictionary<string, string>()
            {
                {"id", bundleId},
                {"type", "1"}
            };

            jServerRequest.SendRequest(RequestMethod.PUT, parameters, "/v1/user/coin-package/" + bundleId, OnComplete, OnRequestError);
        }

        #endregion

        #region Shools

        /// <summary>
        /// Загрузка списка интервалов пропуска времени за премиум
        /// </summary>
        /// <param name="skipIntervalsData">класс-контейнер для загрузки</param>
        /// <param name="callback">отклик</param>
        public void LoadSkipIntervalsBalanceData(Action<SkipIntervalsResponse> callback)
        {
            void OnSkipIntervalsLoaded(string responseText)
            {
                jStringSerializer.DeserializeAsync(responseText, callback);
            }

            // Do a request
            string url = "/v1/game/skip";
            jServerRequest.SendRequestHash(RequestMethod.GET, null, url,
                OnSkipIntervalsLoaded, OnRequestError);
        }

        /// <summary>
        /// Начинает обучение в школе
        /// </summary>
        /// <param name="mapObject">айди школы</param>
        /// <param name="studentsCount">кол-во студентов для обучения</param>
        /// <param name="onSuccess">успешный колбек</param>
        /// <param name="onFailed">кол-бек ошибки</param>
		public void StartTeaching(Func<string> mapObject, int studentsCount, Action<int> onSuccess)
        {
            RequestCompleteDelegate onTeachingStarted = responceText =>
            {
                jDebug.Log(responceText, true);
                int time;
                int.TryParse(responceText, out time);
                onSuccess(time);
            };

            var parameters = new Dictionary<string, LazyParameter<string>>
            {
                {"id", new LazyParameter<string>(mapObject)},
                {"amount", new LazyParameter<string>(studentsCount.ToString())}
            };

            jServerRequest.SendRequest(RequestMethod.POST, "/v1/user/education", parameters,
                onTeachingStarted, OnRequestError);
        }

        /// <summary>
        /// Проверяет окончание таймера обучения
        /// </summary>
        /// <param name="mapObject">айди школы</param>
        /// <param name="onSuccess">успешный колбек</param>
		public void VerifyTeaching(Func<string> mapObject, Action<float> onSuccess)
        {
            RequestCompleteDelegate resp = responceText =>
            {
                jDebug.Log(responceText, true);
                float sec;
                if (float.TryParse(responceText, out sec))
                {
                    onSuccess(sec);
                }
            };

            jServerRequest.SendRequest(RequestMethod.GET, "/v1/user/education/", null,
                resp, OnRequestError, null, mapObject);
        }

        /// <summary>
        /// Заканчивает обучающий цикл
        /// </summary>
        /// <param name="mapObject">айди школы</param>
        /// <param name="onSuccess">успешный колбек</param>
		public void FinishTeaching(Func<string> mapObject, Action onSuccess)
        {
            RequestCompleteDelegate onTeachingFinished = responceText =>
            {
                jDebug.Log("FinishTeaching: " + responceText, true);
                onSuccess();
            };



            jServerRequest.SendRequest(RequestMethod.PUT, "/v1/user/education/", null,
                onTeachingFinished, OnRequestError, null, mapObject);
        }

        /// <summary>
        /// Пропускает обучающий цикл за премиум
        /// </summary>
        /// <param name="mapObject">айди школы</param>
        /// <param name="onSuccess">успешный колбек</param>
		public void SkipTeaching(Func<string> mapObject, Action onSuccess)
        {
            RequestCompleteDelegate onTeachingSkipped = responceText =>
            {
                jDebug.Log(responceText, true);
                onSuccess();
            };

            var parameters = new Dictionary<string, LazyParameter<string>>
            {
                {"id", new LazyParameter<string>(mapObject)},
                {"amount", new LazyParameter<string>("0")},
                {"action", new LazyParameter<string>("skip")}
            };

            jServerRequest.SendRequest(RequestMethod.POST, "/v1/user/education", parameters,
                onTeachingSkipped, OnRequestError);
        }

        #endregion

        #region Video AD

        private string Md5Sum(string strToEncrypt)
        {
            UTF8Encoding ue = new UTF8Encoding();
            byte[] bytes = ue.GetBytes(strToEncrypt);

            // encrypt bytes
            System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] hashBytes = md5.ComputeHash(bytes);

            // Convert the encrypted bytes back to a string (base 16)
            string hashString = "";

            for (int i = 0; i < hashBytes.Length; i++)
            {
                hashString += Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
            }

            return hashString.PadLeft(32, '0');
        }

        public void GetRewardInfoForVideo(Action<List<IAwardAction>> onSuccess = null)
        {
            RequestCompleteDelegate complete = responseText =>
            {
                var awards = jStringSerializer.Deserialize(responseText, typeof(List<IAwardAction>)) as List<IAwardAction>;

                if (awards != null)
                {
                    foreach (var award in awards)
                    {
                        award.InitServerData();
                    }

                    if (onSuccess != null) onSuccess(awards);
                }
            };

            jServerRequest.SendRequest(RequestMethod.GET, null, "/v1/gift/reward-video",
                complete, OnRequestError);
        }

        public void GetRewardItemForVideo(Action<List<IAwardAction>> onSuccess = null)
        {
            RequestCompleteDelegate complete = responseText =>
            {
                jDebug.Log("Get win Item For Video responce: " + responseText, true);

                var awards = jStringSerializer.Deserialize(responseText, typeof(List<IAwardAction>)) as List<IAwardAction>;

                if (awards != null)
                {
                    foreach (var award in awards)
                    {
                        award.InitServerData();
                    }

                    if (onSuccess != null) onSuccess(awards);
                }
            };

            jDebug.Log("Get win item For Video request: " + "/v1/gift/reward-video/1", true);

            var parameters = new Dictionary<string, string>
            {
                {"param", "1"}
            };

            jServerRequest.SendRequest(RequestMethod.PUT, parameters, "/v1/gift/reward-video/1",
                complete, OnRequestError);
        }

        public void ClimeRewardForVideo(string sekretKey, Action<List<IAwardAction>> onSuccess = null)
        {
            void OnComplete(string responseText)
            {
                if (jStringSerializer.Deserialize(responseText, typeof(List<IAwardAction>)) is List<IAwardAction> awards)
                {
                    foreach (IAwardAction award in awards)
                    {
                        award.InitServerData();
                    }

                    onSuccess?.Invoke(awards);
                }
            }

            var time = DateTime.UtcNow.TotalSeconds();
            var str = "{\"access-token\":\"" + AccessToken + "\",\"time\":" + time + "}";

            var hash = Md5Sum(str);
            hash = Md5Sum(hash + sekretKey);

            var parameters = new Dictionary<string, string>
            {
                {"time", time.ToString()},
                {"hash", hash}
            };

            jServerRequest.SendRequest(RequestMethod.POST, parameters, "/v1/gift/reward-video",
                OnComplete, OnRequestError);
        }


        public void ResetCasinoFreeSpinTime(Action onSuccess)
        {
            RequestCompleteDelegate complete = responseText =>
            {
                if (onSuccess != null)
                {
                    onSuccess();
                }
            };

            jServerRequest.SendRequest(RequestMethod.GET, new Dictionary<string, string>(), "/v1/user/casino/extra/0", complete, OnRequestError);
        }

        public void ClaimProductForVideo(int productId, Action onSuccess)
        {
            RequestCompleteDelegate complete = responseText =>
            {
                if (onSuccess != null)
                {
                    onSuccess();
                }
            };

            var param = new Dictionary<string, string>
            {
                {"product_id", productId.ToString()}
            };

            jServerRequest.SendRequest(RequestMethod.GET, param, "/v1/gift/reward-video/extra/0", complete, OnRequestError);
        }

        #endregion

        #region DailyBonus

        public void LoadDailyBonusBalanceData(IDailyBonusBalanceData dailyBalanceData, Action<IDailyBonusBalanceData> callback)
        {
            void OnComplete(string response)
            {
                void OnDeserialized(DailyBonusBalanceData data)
                {
                    callback?.Invoke(data);
                }

                var json = response.WrapToJsonObjectWithField("days");
                jStringSerializer.DeserializeAsync<DailyBonusBalanceData>(json, OnDeserialized);
            }

            jServerRequest.SendRequestHash(RequestMethod.GET, null, "/v1/game/daily-bonus", OnComplete, OnRequestError);
        }

        /// <summary>
        /// Собрать бонус
        /// </summary>
        /// <param name="day">день за который нужно собрать бонусы</param>
        /// <param name="onSucsess">вызывается после успешной загрузки с сервера</param>
        public void CollectDailyBonus(int day, Action<List<IAwardAction>> onSuccess)
        {

            RequestCompleteDelegate complete = (responseText) =>
            {
                var awards = jStringSerializer.Deserialize(responseText, typeof(List<IAwardAction>)) as List<IAwardAction>;

                if (awards != null)
                {
                    foreach (var award in awards)
                    {
                        award.InitServerData();
                    }

                    if (onSuccess != null) onSuccess(awards);
                }
            };

            var parameters = new Dictionary<string, string>
            {
                {"day", day.ToString()}
            };

            jServerRequest.SendRequestHash(RequestMethod.PUT, parameters, $"/v1/quest/daily-bonus/{day}", complete, OnRequestError);
        }

        #endregion

        #region Seaport Order Desk

        public void LoadSeaportOrders(Action<NetworkSeaportOrderDesk> resultCallback)
        {
            var url = "/v1/user/seaport";

            void OnOrderDeskLoaded(string responseText)
            {
                var orderDesk = (NetworkSeaportOrderDesk)jStringSerializer.Deserialize(responseText, typeof(NetworkSeaportOrderDesk));
                resultCallback?.Invoke(orderDesk);
            }

            jServerRequest.SendRequest(RequestMethod.GET, url, OnOrderDeskLoaded, OnRequestError);
        }

        public void ConfirmSkipShuffleOrder(int orderId, bool adsSkip, Action resultCallback)
        {
            string url = $"/v1/user/seaport/skip-wait/{orderId}";

            var parameters = new Dictionary<string, string>
            {
                { "id", orderId.ToString() },
                { "type", Convert.ToInt32(adsSkip).ToString() }
            };

            void OnOrderDeskLoaded(string responseText)
            {
                resultCallback?.Invoke();
            }

            jServerRequest.SendRequest(RequestMethod.POST, parameters, url, OnOrderDeskLoaded, OnRequestError);
        }

        public void ShuffleSeaportOrder(int orderId, Action<NetworkSeaportOrderDesk> resultCallback)
        {
            string url = $"/v1/user/seaport/extra/{orderId}";

            void OnOrderDeskLoaded(string responseText)
            {
                var orderDesk = (NetworkSeaportOrderDesk)jStringSerializer.Deserialize(responseText, typeof(NetworkSeaportOrderDesk));
                resultCallback?.Invoke(orderDesk);
            }

            jServerRequest.SendRequest(RequestMethod.GET, url, OnOrderDeskLoaded, OnRequestError);
        }

        public void SendSeaportOrder(int orderId, Action<NetworkSeaportOrderDesk> resultCallback)
        {
            string url = $"/v1/user/seaport/{orderId}";
            RequestMethod method = RequestMethod.PUT;

            void OnOrderDeskLoaded(string responseText)
            {
                var orderDesk = (NetworkSeaportOrderDesk)jStringSerializer.Deserialize(responseText, typeof(NetworkSeaportOrderDesk));
                resultCallback?.Invoke(orderDesk);
            }

            jServerRequest.SendRequest(method, url, OnOrderDeskLoaded, OnRequestError);
        }

        #endregion

        #region Metro Order Desk

        public void LoadMetroOrderDesk(Action<NetworkMetroOrderDesk> onSuccessCallback)
        {
            string url = "/v1/user/metro";
            RequestMethod method = RequestMethod.GET;

            void OnComplete(string jsonResponse)
            {
                jStringSerializer.DeserializeAsync(jsonResponse, onSuccessCallback);
            }

            jServerRequest.SendRequest(method, url, OnComplete, OnRequestError);
        }

        public void LoadMetroTutorialOrderDesk(bool isReward, Action<NetworkMetroOrderDesk> onSuccessCallback)
        {
            string url = "/v1/tutorial/metro";

            string reward = isReward ? "1" : "0";

            var parameters = new Dictionary<string, string>()
            {
                { "reward", reward }
            };

            RequestMethod method = RequestMethod.GET;

            void OnComplete(string jsonResponse)
            {
                jStringSerializer.DeserializeAsync(jsonResponse, onSuccessCallback);
            }

            jServerRequest.SendRequest(method, parameters, url, OnComplete, OnRequestError);
        }

        public void ShippingMetroOrder(int trainId, Action<NetworkMetroOrderDesk> onSuccessCallback)
        {
            string url = $"/v1/user/metro/send/{trainId}";
            RequestMethod method = RequestMethod.PUT;

            void OnComplete(string jsonResponse)
            {
                jStringSerializer.DeserializeAsync(jsonResponse, onSuccessCallback);
            }

            jServerRequest.SendRequest(method, url, OnComplete, OnRequestError);
        }

        public void ShippingMetroTutorialOrder(int trainId, Action<NetworkMetroOrderDesk> onSuccessCallback)
        {
            string url = $"/v1/tutorial/metro/send/{trainId}";
            RequestMethod method = RequestMethod.PUT;

            void OnComplete(string jsonResponse)
            {
                jStringSerializer.DeserializeAsync(jsonResponse, onSuccessCallback);
            }

            jServerRequest.SendRequest(method, url, OnComplete, OnRequestError);
        }

        public void SkipMetroOrderWaitingTime(int trainId, Action<NetworkMetroOrderDesk> onSuccessCallback)
        {
            string url = $"/v1/user/metro/skip-wait/{trainId}";
            RequestMethod method = RequestMethod.POST;

            void OnComplete(string jsonResponse)
            {
                jStringSerializer.DeserializeAsync(jsonResponse, onSuccessCallback);
            }

            var headers = new Dictionary<string, string>()
            {
                { "train-id", trainId.ToString() }
            };

            jServerRequest.SendRequest(method, url, OnComplete, OnRequestError, headers);
        }

        public void LoadMetroProductsToSlot(int slotId, Action<NetworkMetroOrderDesk> onSuccessCallback)
        {
            string url = $"/v1/user/metro-crate/extra/{slotId}";
            RequestMethod method = RequestMethod.GET;

            void OnComplete(string jsonResponse)
            {
                jStringSerializer.DeserializeAsync(jsonResponse, onSuccessCallback);
            }

            jServerRequest.SendRequest(method, url, OnComplete, OnRequestError);
        }

        public void LoadMetroTutorialProductsToSlot(int slotId, Action<NetworkMetroOrderDesk> onSuccessCallback)
        {
            string url = $"/v1/tutorial/metro-crate/extra/{slotId}";
            RequestMethod method = RequestMethod.GET;

            void OnComplete(string jsonResponse)
            {
                jStringSerializer.DeserializeAsync(jsonResponse, onSuccessCallback);
            }

            jServerRequest.SendRequest(method, url, OnComplete, OnRequestError);
        }

        public void ShuffleMetroOrder(int orderId, Action<NetworkMetroOrderDesk> onSuccessCallback)
        {
            string url = $"/v1/user/metro/shuffle/{orderId}";
            RequestMethod method = RequestMethod.POST;

            void OnComplete(string jsonResponse)
            {
                jStringSerializer.DeserializeAsync(jsonResponse, onSuccessCallback);
            }

            jServerRequest.SendRequest(method, url, OnComplete, OnRequestError);
        }

        public void SkipShuffleMetroOrder(int orderId, bool isPaid, Action<NetworkMetroOrderDesk> onSuccessCallback)
        {
            // 0 for skip by ad, 1 for skip by currency.
            int skipType = isPaid ? 1 : 0;

            string url = "/v1/user/metro/skip-shuffle";
            RequestMethod method = RequestMethod.POST;

            var parameters = new Dictionary<string, string>
            {
                {"train_id", orderId.ToString()},
                {"type", skipType.ToString()}
            };

            void OnComplete(string jsonResponse)
            {
                jStringSerializer.DeserializeAsync(jsonResponse, onSuccessCallback);
            }

            jServerRequest.SendRequest(method, parameters, url, OnComplete, OnRequestError);
        }

        #endregion

        #region Tutorial

        public void SoftTutorialStepCompleted(SoftTutorialType step)
        {
            void OnComplete(string responseText) { }

            var type = (int)step;
            jServerRequest.SendRequest(RequestMethod.GET, null, "/v1/user/tutorial/" + type, OnComplete, OnRequestError);
        }

        public void HintTutorialStepCompleted(HintType step, Action<List<HintStepStatus>> onSuccessCallback)
        {
            void OnComplete(string responseString)
            {
                jStringSerializer.DeserializeAsync(responseString, onSuccessCallback);
            }

            var type = (int)step;
            jServerRequest.SendRequest(RequestMethod.PUT, null, "/v1/tutorial/player-hints/" + type, OnComplete, OnRequestError);
        }

        public void GetHintTutorialStatus(Action<List<HintStepStatus>> onSuccessCallback)
        {
            void OnComplete(string responseString)
            {
                jStringSerializer.DeserializeAsync(responseString, onSuccessCallback);
            }

            jServerRequest.SendRequest(RequestMethod.GET, null, "/v1/tutorial/player-hints", OnComplete, OnRequestError);
        }

        public void HardTutorialStepCompleted(Action<StepType> onSuccessCallback)
        {
            void OnComplete(string responseText)
            {
                var parsedTutorialStep = (StepType)Enum.Parse(typeof(StepType), responseText);
                onSuccessCallback?.Invoke(parsedTutorialStep);
            }

            // parameters are mandatory, so something has to be sent
            var parameters = new Dictionary<string, string> { { "id", "1" } };

            jServerRequest.SendRequest(RequestMethod.POST, parameters, "/v1/user/tutorial", OnComplete, OnRequestError);
        }

        public void JumpHardTutorialStep(StepType step, Action successCallback)
        {
            void OnComplete(string responseText)
            {
                successCallback?.Invoke();
            }

            var intStep = (int)step;
            var parameters = new Dictionary<string, string> { { "id", intStep.ToString() } };

            jServerRequest.SendRequest(RequestMethod.PUT, parameters, "/v1/user/tutorial/" + intStep, OnComplete, OnRequestError);
        }

        public void CollectTutorialRewards(List<IAwardAction> awardActions, Action onSuccess)
        {
            if (awardActions == null || awardActions.Count <= 0)
            {
                jDebug.LogWarning("CollectTutorialRewards: cannot collect with empty award actions!");
                onSuccess?.Invoke();
                return;
            }

            void OnComplete(string responseText)
            {
                onSuccess?.Invoke();
            }

            var ids = new int[awardActions.Count];
            for (var i = 0; i < awardActions.Count; i++)
            {
                IAwardAction action = awardActions[i];
                ids[i] = action.AwardId;
            }

            var parameters = new Dictionary<string, string> { { "ids", jStringSerializer.Serialize(ids) } };

            jServerRequest.SendRequest(RequestMethod.GET, parameters, "/v1/user/tutorial/option/0", OnComplete, OnRequestError);
        }

        #endregion

        #region TheEvent

        public void PurchaseEventProduct(Id<Product> productId, int count, Action onSuccess = null)
        {
            RequestCompleteDelegate onProductsLoaded = responseText =>
            {
                if (onSuccess != null)
                {
                    onSuccess();
                }
            };

            var parameters = new Dictionary<string, string>
            {
                {"amount", count.ToString()}
            };

            jServerRequest.SendRequest(RequestMethod.POST, parameters, "/v1/event/product/" + productId.Value, onProductsLoaded, OnRequestError);
        }

        public void GetEventBaseData(Action<BaseEventData> onSucces, Action onError)
        {
            void OnDeserializeComplete(BaseEventData data)
            {
                if (DataIsValid(data))
                {
                    onSucces?.Invoke(data);
                }
                else
                {
                    "The Event data is not valid".LogError();
                    onError?.Invoke();
                }
            }

            void OnComplete(string jsonString)
            {
                if (!jsonString.Equals("[]"))
                {
                    jStringSerializer.DeserializeAsync<BaseEventData>(jsonString, OnDeserializeComplete);
                }
                else
                {
                    onError?.Invoke();
                }
            }

            jServerRequest.SendRequest(RequestMethod.GET, "/v1/event", OnComplete, OnRequestError);

            bool DataIsValid(BaseEventData data)
            {
                if (data.FinishDate == 0 || data.BuildingStageId == 0 || data.StartDate == 0 ||
                    string.IsNullOrEmpty(data.EventName) || data.MainBuildingId == 0 || data.DailyRewards == null ||
                    data.BuildingStages == null)
                {
                    return false;
                }

                foreach (var rewardInfo in data.DailyRewards)
                {
                    if (RewardInfoIsInvalid(rewardInfo.CommonRewards))
                    {
                        return false;
                    }

                    if (RewardInfoIsInvalid(rewardInfo.PremiumRewards))
                    {
                        return false;
                    }
                }

                return true;

                bool RewardInfoIsInvalid(RewardInfo rewardInfo)
                {
                    if (rewardInfo.UiType == 0 || rewardInfo.Amount == 0 ||
                        rewardInfo.RewardId == 0 || rewardInfo.Rewards == null || rewardInfo.Rewards.Length == 0)
                    {
                        return true;
                    }

                    foreach (var rewardInfoCommonReward in rewardInfo.Rewards)
                    {
                        if (rewardInfoCommonReward.Type == 0 || rewardInfoCommonReward.Amount == 0)
                        {
                            return true;
                        }
                    }

                    return false;
                }
            }
        }

        public void GetEventFinishedData(Action<FinishEventData> callback)
        {
            void OnDeserializeComplete(FinishEventData data)
            {
                callback?.Invoke(data);
            }

            void OnComplete(string jsonString)
            {
                jStringSerializer.DeserializeAsync<FinishEventData>(jsonString, OnDeserializeComplete);
            }

            jServerRequest.SendRequest(RequestMethod.GET, "/v1/event/status", OnComplete, OnRequestError);
        }

        public void ClaimDailyReward(int rewardId, Action callback)
        {
            void OnComplete(string jsonString)
            {
                callback?.Invoke();
            }

            jServerRequest.SendRequest(RequestMethod.GET, $"/v1/event/reward/claim/{rewardId}", OnComplete, OnRequestError);
        }

        public void ClaimStageReward(int stageId, Action<ClaimStageRewardData> callback)
        {
            void OnDeserializeComplete(ClaimStageRewardData data)
            {
                callback?.Invoke(data);
            }

            void OnComplete(string jsonString)
            {
                jStringSerializer.DeserializeAsync<ClaimStageRewardData>(jsonString, OnDeserializeComplete);
            }

            jServerRequest.SendRequest(RequestMethod.GET, $"/v1/event/reward/claim-stage/{stageId}", OnComplete, OnRequestError);
        }

        public void FinalizeEvent(Action onFinalize)
        {
            void OnComplete(string jsonString)
            {
                onFinalize?.Invoke();
            }

            jServerRequest.SendRequest(RequestMethod.GET, "/v1/event/finalise", OnComplete, OnRequestError);
        }

      

        public void BuyEventBuilding(int productId, string token, string orderId, Action<int,bool> callback)
        {
            BuyProduct( productId,  token,  orderId,  callback, "/v1/event/buy");
        }

        #endregion

        public void GetStatusEffectsBalanceData(Action<CityStatusEffectSettings[]> onComplete)
        {
            string url = "/v1/game/status-effect";
            RequestMethod requestMethod = RequestMethod.GET;

            void OnComplete(string responseString)
            {
                jStringSerializer.DeserializeAsync(responseString, onComplete);
            }

            jServerRequest.SendRequest(requestMethod, url, OnComplete, OnRequestError);
        }

        public void GetCityStatusEffects(Action<CityStatusEffect[]> onComplete)
        {
            string url = "/v1/user/status-effects";
            RequestMethod requestMethod = RequestMethod.GET;

            void OnComplete(string responseString)
            {
                jStringSerializer.DeserializeAsync(responseString, onComplete);
            }

            jServerRequest.SendRequest(requestMethod, url, OnComplete, OnRequestError);
        }

        public void GetProductionSlots(Action<List<BuildingProductionInfo>> activeProductionSlots)
        {
            string url = "/v1/user/status-effects/production";
            RequestMethod requestMethod = RequestMethod.GET;

            void OnComplete(string responseString)
            {
                jStringSerializer.DeserializeAsync(responseString, activeProductionSlots);
            }

            jServerRequest.SendRequest(requestMethod, url, OnComplete, OnRequestError);
        }

        public void LoadAvailableCitizensGifts(Action<CitizenGift[]> onComplete)
        {
            void OnComplete(string responseString)
            {
                jStringSerializer.DeserializeAsync(responseString, onComplete);
            }

            jServerRequest.SendRequest(RequestMethod.GET, "/v1/user/gift", OnComplete, OnRequestError);
        }

        public void ClaimCitizenGift(int giftId, Action onComplete)
        {
            void OnComplete(string responseString)
            {
                onComplete?.Invoke();
            }

            jServerRequest.SendRequest(RequestMethod.PUT, $"/v1/user/gift/{giftId}", OnComplete, OnRequestError);
        }

        #region Food Supply

        public void GetFoodSupplyCityInfo(Action<FoodSupplyCityInfo> onComplete)
        {
            void OnComplete(string responseText)
            {
                jStringSerializer.DeserializeAsync(responseText, onComplete);
            }

            jServerRequest.SendRequest(RequestMethod.GET, "/v1/user/food-supply/status", OnComplete, OnRequestError);
        }

        public void GetFoodSupplyData(Action<FoodSupplyData> onComplete)
        {
            void OnComplete(string responseText)
            {
                jStringSerializer.DeserializeAsync(responseText, onComplete);
            }

            jServerRequest.SendRequest(RequestMethod.GET, "/v1/game/food-supply", OnComplete, OnRequestError);
        }

        public void GetTutorialFoodSupplyData(Action<FoodSupplyData> onComplete)
        {
            void OnComplete(string responseText)
            {
                jStringSerializer.DeserializeAsync(responseText, onComplete);
            }

            jServerRequest.SendRequest(RequestMethod.GET, "/v1/tutorial/food-supply", OnComplete, OnRequestError);
        }

        public void CookFoodSupplyProducts(int recipeId, Action<FoodSupplyData> callback)
        {
            var parameters = new Dictionary<string, string>()
            {
                { "recipe_id", recipeId.ToString()}
            };

            void OnComplete(string responseText)
            {
                jStringSerializer.DeserializeAsync(responseText, callback);
            }

            jServerRequest.SendRequest(RequestMethod.POST, parameters, "/v1/user/food-supply/cooking", OnComplete, OnRequestError);
        }

        public void CookTutorialFoodSupplyProducts(int recipeId, Action<FoodSupplyData> callback)
        {
            var parameters = new Dictionary<string, string>()
            {
                { "recipe_id", recipeId.ToString()}
            };

            void OnComplete(string responseText)
            {
                jStringSerializer.DeserializeAsync(responseText, callback);
            }

            jServerRequest.SendRequest(RequestMethod.GET, parameters, "/v1/tutorial/food-supply/cooking", OnComplete, OnRequestError);
        }

        public void FoodSupplyFeedingProducts(int buildingId, Dictionary<int, int> productsInSlots, Action<FoodSupplyCityInfo> callback)
        {
            var parameters = new Dictionary<string, string>()
            {
                {"building_id", buildingId.ToString()},
                {"meals", JsonConvert.SerializeObject(productsInSlots)},
            };

            void OnComplete(string responseText)
            {
                jStringSerializer.DeserializeAsync(responseText, callback);
            }

            jServerRequest.SendRequest(RequestMethod.POST, parameters, "/v1/user/food-supply/feeding", OnComplete, OnRequestError);
        }

        public void UnlockFoodSupplySlot(int buildingId, int slotId, Action<FoodSupplyData> callback)
        {
            var parameters = new Dictionary<string, string>()
            {
                {"building_id", buildingId.ToString()},
                {"slot_id", slotId.ToString()}
            };

            void OnComplete(string responseText)
            {
                jStringSerializer.DeserializeAsync(responseText, callback);
            }

            jServerRequest.SendRequest(RequestMethod.POST, parameters, "/v1/user/food-supply/feeding/unlock-slot", OnComplete, OnRequestError);
        }

        #endregion

        #region GlobalCityMap

        public void LoadGlobalCityMap(Id<IUserCity> userCityId, Id<IPlayerProfile> userId, Id<IBusinessSector> businessSectorId, Action<IGlobalCityMap> successCallback)
        {
            RequestCompleteDelegate onCityMapLoaded = responseText =>
            {
                jStringSerializer.DeserializeAsync(responseText, successCallback);
            };

            jServerRequest.SendRequest(RequestMethod.GET, null,
                "/v1/user/map?type=" + businessSectorId.Value + "&player_id=" +
                userId.Value,
                onCityMapLoaded, OnRequestError);
        }

        #endregion

        #region Time

        public void GetServerTime(Action<long> callback)
        {
            RequestCompleteDelegate onComplete = responseText =>
            {
                long time;
                if (long.TryParse(responseText, out time))
                {
                    callback(time);
                }
                else
                {
                    jDebug.LogError("ServerCommunicator.GetServerTime: Cannot parse server time: " + responseText);
                }
            };

            jServerRequest.SendRequest(RequestMethod.GET, null, "/v1/user/0", onComplete, OnRequestError);
        }

        #endregion

        #region Cheats

        public void CheatGetPresets(Action<PresetData[]> onComplete)
        {
            void OnComplete(string responseString)
            {
                jStringSerializer.DeserializeAsync(responseString, onComplete);
            }

            jServerRequest.SendRequest(RequestMethod.GET, "/v1/user/cheating/preset", OnComplete, OnRequestError);
        }

        public void CheatSetPreset(int presetId, Action onComplete)
        {
            void OnComplete(string response)
            {
                onComplete();
            }

            jServerRequest.SendRequest(RequestMethod.PUT, $"/v1/user/cheating/run-preset/{presetId}", OnComplete, OnRequestError);
        }

        public void CheatAddValue(string actionType, int amount, Action callback)
        {
            void OnComplete(string responseText)
            {
                callback();
            }

            var parameters = new Dictionary<string, string>
            {
                {"action", actionType},
                {"value", amount.ToString()}
            };

            jServerRequest.SendRequest(RequestMethod.POST, parameters, "/v1/user/cheating", OnComplete, OnRequestError);
        }

        public void CheatAddAmount(string valueType, int amount, Action callback)
        {
            void OnComplete(string responseText)
            {
                callback();
            }

            jServerRequest.SendRequest(RequestMethod.POST, new Dictionary<string, string>(), $"/v1/user/cheating/{valueType.ToLower()}/{amount}", OnComplete, OnRequestError);
        }

        public void CheatAddProduct(int productId, int amount, Action callback)
        {
            void OnComplete(string responseText)
            {
                callback();
            }

            var parameters = new Dictionary<string, string>
            {
                {"count", amount.ToString()}
            };

            jServerRequest.SendRequest(RequestMethod.POST, parameters, "/v1/user/cheating/product/" + productId, OnComplete, OnRequestError);
        }

        #endregion

        #region Configs

        public void LoadGameConfigData(IGameConfigData gameConfigData, Action<IGameConfigData> onSuccess)
        {
            void OnComplete(string response)
            {
                void OnDeserialized(GameConfigData data)
                {
                    onSuccess?.Invoke(data);
                }

                jStringSerializer.DeserializeAsync<GameConfigData>(response, OnDeserialized);
            }

            jServerRequest.SendRequestHash(RequestMethod.GET, null, "/v1/game/config", OnComplete, OnRequestError);
        }

        public void LoadLinksConfigData(Action<LinksConfigData> onSuccess)
        {
            void OnComplete(string response)
            {
                void OnDeserialized(LinksConfigData data)
                {
                    onSuccess?.Invoke(data);
                }
                
                jStringSerializer.DeserializeAsync<LinksConfigData>(response, OnDeserialized);
            }
            
            jServerRequest.SendRequest(RequestMethod.GET, "/v1/auth/web-urls", OnComplete, OnRequestError);
        }
        
        public void LoadBuildingSlotsData(BuildingSlotsData buildingSlotsData, Action<List<BuildingSlotData>> callback)
        {
            void OnLoadedComplete(string responseText)
            {
                void OnDeserializeComplete(List<BuildingSlotData> parsedData)
                {
                    callback?.Invoke(parsedData);
                }

                jStringSerializer.DeserializeAsync<List<BuildingSlotData>>(responseText, OnDeserializeComplete);
            }
            
            jServerRequest.SendRequest(RequestMethod.GET, null, "/v1/game/factory-slots", OnLoadedComplete, OnRequestError);
        }

        #endregion

        public void LoadAoeData(Action<CityAoeData> onSuccess)
        {
            void OnComplete(string json)
            {
                jStringSerializer.DeserializeAsync(json, onSuccess);

            }
            jServerRequest.SendRequest(RequestMethod.GET, null, "/v1/user/service", OnComplete, OnRequestError);
        }

        public void LoadAoeDataNew(Action<CityAoeData> onSuccess)
        {
            void OnComplete(string json)
            {
                jStringSerializer.DeserializeAsync(json, onSuccess);

            }
            jServerRequest.SendRequest(RequestMethod.GET, null, "/v1/user/service/1", OnComplete, OnRequestError);
        }

        #region CityArea

        public void LoadCityAreaData(ICityAreaData cityAreaData, Action<ICityAreaData> onSuccess)
        {
            RequestCompleteDelegate complete = (response) =>
            {
                var data = jStringSerializer.Deserialize(response, cityAreaData.GetType()) as ICityAreaData;

                if (data == null)
                {
                    jDebug.LogError("Could not deserialize ICityAreaData!");
                    return;
                }

                if (onSuccess != null) onSuccess(data);
            };

            jServerRequest.SendRequestHash(RequestMethod.GET, null, "/v1/game/area", complete, OnRequestError);
        }

        #endregion

        #region Bazaar

        public void BuySoukLootbox(int lootboxIndex, Action<SoukData> onSuccessCallback)
        {
            string url = $"/v1/user/souk/buy-lootbox/{lootboxIndex}";
            RequestMethod method = RequestMethod.PUT;

            void OnComplete(string response)
            {
                jStringSerializer.DeserializeAsync(response, onSuccessCallback);
            }

            jServerRequest.SendRequest(method, url, OnComplete, OnRequestError);
        }

        #endregion

        #region Souk

        public void LoadSouk(Action<SoukData> callback)
        {
            var url = "/v1/user/souk";
            var method = RequestMethod.GET;

            void OnComplete(string jsonText)
            {
                jStringSerializer.DeserializeAsync(jsonText, callback);
            }

            jServerRequest.SendRequest(method, url, OnComplete, OnRequestError);
        }

        public void ShuffleSouk(Action<SoukData> callback)
        {
            var url = "/v1/user/souk/shuffle";
            var method = RequestMethod.POST;

            void OnComplete(string jsonText)
            {
                jStringSerializer.DeserializeAsync(jsonText, callback);
            }

            jServerRequest.SendRequest(method, url, OnComplete, OnRequestError);
        }

        public void UnlockTopSoukSlot(int slotId, Action<SoukData> callback)
        {
            var url = $"/v1/user/souk/unlock-top-slot/{slotId}";
            var method = RequestMethod.POST;

            void OnComplete(string jsonText)
            {
                jStringSerializer.DeserializeAsync(jsonText, callback);
            }

            jServerRequest.SendRequest(method, url, OnComplete, OnRequestError);
        }

        public void BuyTopSoukSlot(int slotId, Action<SoukData> callback)
        {
            var url = $"/v1/user/souk/buy-top-slot/{slotId}";
            var method = RequestMethod.PUT;

            void OnComplete(string jsonText)
            {
                jStringSerializer.DeserializeAsync(jsonText, callback);
            }

            jServerRequest.SendRequest(method, url, OnComplete, OnRequestError);
        }

        public void UnlockBottomSoukSlot(int slotId, Action<SoukData> callback)
        {
            var url = $"/v1/user/souk/unlock-bottom-slot/{slotId}";
            var method = RequestMethod.POST;

            void OnComplete(string jsonText)
            {
                jStringSerializer.DeserializeAsync(jsonText, callback);
            }

            jServerRequest.SendRequest(method, url, OnComplete, OnRequestError);
        }

        public void BuyBottomSoukSlot(int slotId, Action<SoukData> callback)
        {
            var url = $"/v1/user/souk/buy-bottom-slot/{slotId}";
            var method = RequestMethod.PUT;

            void OnComplete(string jsonText)
            {
                jStringSerializer.DeserializeAsync(jsonText, callback);
            }

            jServerRequest.SendRequest(method, url, OnComplete, OnRequestError);
        }

        public void SkipSoukShuffleTime(Action<SoukData> callback)
        {
            var url = $"/v1/user/souk/skip";
            var method = RequestMethod.POST;

            void OnComplete(string jsonText)
            {
                jStringSerializer.DeserializeAsync(jsonText, callback);
            }

            jServerRequest.SendRequest(method, url, OnComplete, OnRequestError);
        }

        #endregion

        #region AgriculturalData

        public void StartGrowing(Func<string> mapObjectId, Id<Product> productId, Action onSuccess)
        {
            string url = "/v1/factory/agricultural-field";
            RequestMethod requestMethod = RequestMethod.POST;

            RequestCompleteDelegate completeDelegate = responseText =>
            {
                if (onSuccess != null)
                    onSuccess();
            };

            var parameters = new Dictionary<string, LazyParameter<string>>
            {
                {"building_id", new LazyParameter<string>(mapObjectId)},
                {"product_id", new LazyParameter<string>(productId.Value.ToString())}
            };

            jServerRequest.SendRequest(requestMethod, url, parameters, completeDelegate, OnRequestError);
        }

        public void VerifyGrowing(Func<string> mapObjectId, Action<int> onSuccess)
        {
            RequestCompleteDelegate completeDelegate = responseText =>
            {
                int time;
                if (int.TryParse(responseText, out time))
                {
                    if (onSuccess != null)
                        onSuccess(time);
                }
            };

            jServerRequest.SendRequest(RequestMethod.GET, "/v1/factory/agricultural-field/", null,
                completeDelegate, OnRequestError, null, mapObjectId);
        }

        public void Harvesting(Func<string> mapObjectId, Id<IUserCity> userId, Action<int> onSuccess)
        {
            RequestCompleteDelegate completeDelegate = responseText =>
            {
                var nanoGems = GetResponseValue(responseText, "nano_gems");
                if (onSuccess != null)
                    onSuccess(nanoGems != null ? int.Parse(nanoGems) : 0);
            };

            var header = new Dictionary<string, string>
            {
                {"city-id", userId.Value.ToString()}
            };

            var parameters = new Dictionary<string, LazyParameter<string>>
            {
                {"id", new LazyParameter<string>(mapObjectId)}
            };

            jServerRequest.SendRequest(RequestMethod.PUT,
                "/v1/factory/agricultural-field/", parameters,
                completeDelegate, OnRequestError, header, mapObjectId);
        }

        public void SkipAgriculturalGrowing(Id<IUserCity> cityId, Func<string> mapId, Action<bool> onSuccess)
        {
            RequestCompleteDelegate completeDelegate = responseText =>
            {
                bool.TryParse(responseText, out bool res);
                onSuccess?.Invoke(res);
            };

            var header = new Dictionary<string, string>
            {
                {"city-id", cityId.Value.ToString()}
            };

            string url = "/v1/factory/agricultural-skip/extra/";
            RequestMethod method = RequestMethod.GET;
            jServerRequest.SendRequest(method, url, null, completeDelegate, OnRequestError, header, mapId);
        }

        public void FreeSkipAgriculturalGrowing(Id<IUserCity> cityId, Func<string> mapId, Action<bool> onSuccess)
        {
            RequestCompleteDelegate completeDelegate = responseText =>
            {
                if (onSuccess != null)
                {
                    bool res;
                    bool.TryParse(responseText, out res);
                    onSuccess(res);
                }
            };

            var header = new Dictionary<string, string>
            {
                {"city-id", cityId.Value.ToString()}
            };

            var parameters = new Dictionary<string, LazyParameter<string>>
            {
                {"free", new LazyParameter<string>("1")}
            };

            jServerRequest.SendRequest(RequestMethod.GET,
                "/v1/factory/agricultural-skip/extra/", parameters, completeDelegate, OnRequestError, header, mapId);
        }

        public void FertilizeAgriculturalGrowing(Id<IUserCity> cityId, Func<string> mapId, Action<int> onSuccess)
        {
            RequestCompleteDelegate completeDelegate = responseText =>
            {
                jDebug.Log("Fertilize growing timer server result: " + responseText, true);
                if (onSuccess != null)
                {
                    int res;
                    int.TryParse(responseText, out res);
                    onSuccess(res);
                }
            };

            var header = new Dictionary<string, string>
            {
                {"city-id", cityId.Value.ToString()}
            };

            var parameters = new Dictionary<string, LazyParameter<string>>
            {
                {"id", new LazyParameter<string>(mapId)}
            };

            jServerRequest.SendRequest(RequestMethod.PUT, "/v1/factory/agricultural-skip/", parameters,
                completeDelegate, errorData =>
                {
                    OnRequestError(errorData);

                    jDebug.LogError(
                        "Error getting agricultural types data: " + errorData.ErrorMessage);
                }, header, mapId);
        }

        #endregion

        #region BusinessSector

        public void LoadBusinessSectorMap(LoadCityMapResponseModel responseModel)
        {
            RequestCompleteDelegate onCityMapLoaded = responseText =>
            {
                jStringSerializer.DeserializeAsync(responseText,
                    (ICityMap cityMap) =>
                    {
                        responseModel.SuccessCallback(responseModel.UserCityId, cityMap, responseModel.UserId,
                            responseModel.BusinessSectorId);
                    });
            };

            jServerRequest.SendRequest(RequestMethod.GET, null,
                "/v1/user/map?type=" + (int)responseModel.BusinessSectorId + "&player_id=" +
                responseModel.UserId.Value,
                onCityMapLoaded, OnRequestError);
        }

        public void LoadSubSectors(Id<IUserCity> userCityId, Action<CitySubLockedMap> callback)
        {
            RequestCompleteDelegate onCityMapLoaded = responseText =>
            {
                jStringSerializer.DeserializeAsync(responseText, callback);
            };

            jServerRequest.SendRequest
            (
                RequestMethod.GET,
                null,
                "/v1/user/locked-sector/" + userCityId.Value,
                onCityMapLoaded,
                OnRequestError
            );
        }

        public void UnlockSubSector(Id<IUserCity> userCityId, Id<ILockedSector> subSectorId,
            Action<bool> successCallback)
        {
            RequestCompleteDelegate onCityMapLoaded = responseText =>
            {
                jDebug.Log("LoadSubSectors IAsyncResult: " + responseText, true);
                bool result;
                bool.TryParse(responseText, out result);
                successCallback(result);
            };

            var header = new Dictionary<string, string>
            {
                {"city-id", userCityId.Value.ToString()}
            };

            var parameters = new Dictionary<string, string>
            {
                {"sectorID", subSectorId.Value.ToString()}
            };

            // Do a request
            jServerRequest.SendRequest(RequestMethod.PUT, parameters,
                "/v1/user/locked-sector/" + subSectorId.Value,
                onCityMapLoaded, OnRequestError, header);
        }

        #endregion

        #region UserCity

        public void RenameCity(string cityName, Id<IUserCity> cityId, Action<bool> callback)
        {
            // Make delegate with success callback
            RequestCompleteDelegate onCityRenamed = responseText =>
            {
                if (callback != null) callback(true);
            };

            var parameters = new Dictionary<string, string>
            {
                {"name", cityName}
            };

            // Do a request
            jServerRequest.SendRequest(RequestMethod.PUT, parameters, "/v1/user/city/" + cityId.Value,
                onCityRenamed,
                data =>
                {
                    if (callback != null) callback(false);
                });
        }

        #endregion

        #region ProductData

        public void LoadProducts(IProductsData productsData, Action<IProductsData> callback = null)
        {
            RequestCompleteDelegate onProductsLoaded = (responseText) =>
            {
                void Callback(IProductsData data)
                {
                    callback?.Invoke(data);
                }

                jStringSerializer.DeserializeAsync<IProductsData>(responseText, Callback);
            };

            jServerRequest.SendRequestHash(RequestMethod.GET, null,
                "/v1/game/product",
                onProductsLoaded, OnRequestError);
        }


        public void PurchaseProduct(Id<IUserCity> userCityId, Id<Product> productId, int count, Action onSuccess = null)
        {
            RequestCompleteDelegate onProductsLoaded = responseText =>
            {
                if (onSuccess != null)
                {
                    onSuccess();
                }
            };

            var header = new Dictionary<string, string>
            {
                {"city-id", userCityId.Value.ToString()}
            };

            var parameters = new Dictionary<string, string>
            {
                {"id", productId.Value.ToString()},
                {"amount", count.ToString()}
            };
            jServerRequest.SendRequest(RequestMethod.PUT, parameters, "/v1/purchase/product/" + productId.Value,
                onProductsLoaded, OnRequestError, header);
        }

        public void PurchaseProducts(Id<IUserCity> userCityId, Dictionary<Id<Product>, int> products,
            Action onSuccess = null)
        {
            RequestCompleteDelegate onProductsLoaded = responseText =>
            {
                if (onSuccess != null)
                    onSuccess();
            };

            string _data = "{";
            foreach (KeyValuePair<Id<Product>, int> product in products)
                _data += "\"" + product.Key.Value + "\":" + product.Value + ",";
            _data = _data.Remove(_data.Length - 1);
            _data += "}";

            var header = new Dictionary<string, string>
            {
                {"city-id", userCityId.Value.ToString()}
            };

            var parameters = new Dictionary<string, string>
            {
                {"products", _data}
            };
            jServerRequest.SendRequest(RequestMethod.POST, parameters, "/v1/purchase/product",
                onProductsLoaded, OnRequestError, header);
        }

        #endregion

        #region Factory

        public void StartProduceGood(Func<string> mapObjectId, Id<IProduceSlot> produceSlotId, Id<Product> productId, Action<int, int> onSuccess)
        {
            void CompleteDelegate(string responseText)
            {
                var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseText);
                dict = SerializationConverters.DeserializeObjectWithoutConverters(responseText, dict.GetType()) as Dictionary<string, string>;

                if (dict != null && onSuccess != null)
                {
                    onSuccess(int.Parse(dict["id"]), int.Parse(dict["time_left"]));
                }
            }

            var parameters = new Dictionary<string, LazyParameter<string>>
            {
                {"building_id", new LazyParameter<string>( mapObjectId)},
                {"slot_id", new LazyParameter<string>( produceSlotId.Value.ToString())},
                {"product_id",new LazyParameter<string>(  productId.Value.ToString())}
            };

            jServerRequest.SendRequest(RequestMethod.POST, "/v1/factory/production", parameters,
                CompleteDelegate, OnRequestError);
        }

        public void VerifyProduceGood(Func<string> produceSlotId, Action<int> onSuccess)
        {
            RequestCompleteDelegate completeDelegate = responseText =>
            {
                var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseText);
                dict =
                    SerializationConverters.DeserializeObjectWithoutConverters(responseText, dict.GetType()) as
                        Dictionary<string, string>;

                if (onSuccess != null && dict != null)
                {
                    onSuccess(int.Parse(dict["time_left"]));
                }
            };

            var parameters = new Dictionary<string, LazyParameter<string>>
            {
                {"id", new LazyParameter<string>(produceSlotId)},
            };

            jServerRequest.SendRequest(RequestMethod.GET, "/v1/factory/production", parameters,
                completeDelegate, OnRequestError);
        }

        public void ShipProductFromFactory(Func<string> id, Action onSuccess)
        {
            RequestCompleteDelegate onShip = responseText =>
            {
                onSuccess();
            };

            jServerRequest.SendRequest(RequestMethod.PUT,
                "/v1/factory/warehouse/", null,
                onShip, (data =>
                {
                    if (data.HttpErrorCode == ServerResponseCodes.LogicNotEnoughSpaceWarehouse)
                    {

                    }
                    else
                    {
                        OnRequestError(data);
                    }
                }), null, id);
        }

        public void UnlockFactorySlot(Func<string> mapObjectId, Action<int> onSuccess)
        {
            RequestCompleteDelegate onUnlock = respoceText =>
            {
                int slotsCount;
                int.TryParse(respoceText, out slotsCount);
                onSuccess(slotsCount);
            };

            jServerRequest.SendRequest(RequestMethod.PUT,

                "/v1/user/product/", null,

                onUnlock, (data =>
                {
                    if (data.HttpErrorCode == ServerResponseCodes.LogicNotEnoughSpaceWarehouse)
                    {
                    }
                    else
                    {
                        OnRequestError(data);
                    }
                }), null, mapObjectId);
        }

        public void SkipFactoryProduction(Func<string> mapId, Func<string> slotId, Action<bool> onSuccess)
        {
            RequestCompleteDelegate completeDelegate = responceText =>
            {
                if (onSuccess != null)
                {
                    onSuccess(responceText != "false");
                }
            };

            var header = new Dictionary<string, string>
            {

                {"city-id", jGameManager.CurrentUserCity.Id.ToString()}
            };

            var parameters = new Dictionary<string, LazyParameter<string>>
            {
                {"slot", new LazyParameter<string>( slotId)},
                {"id", new LazyParameter<string>(mapId)}
            };

            jServerRequest.SendRequest(RequestMethod.GET,
                "/v1/factory/production-skip/extra/", parameters, completeDelegate, OnRequestError, header, mapId);
        }

        #endregion

        #region AssetBundles

        /// <summary>
        /// Loads asset bundles hashes.
        /// </summary>
        /// <param name="onSuccess">Returns list of current asset bundle hashes on remote server.</param>
        public void LoadAssetBundleHashes(Action<Dictionary<RuntimePlatform, uint>> onSuccess)
        {
            void OnComplete(string json)
            {
                jStringSerializer.DeserializeAsync(json, onSuccess);
            }

            var url = "/v1/game/asset-bundles";
            var requestMethod = RequestMethod.GET;

            jServerRequest.SendRequest(requestMethod, url, OnComplete, OnRequestError);
        }

        #endregion

        public void LoadBuildingInstancesUnlockData(Action<List<BuildingInstanceUnlockInfo>> callback)
        {
            var url = "/v1/game/building-instances-unlock";
            var method = RequestMethod.GET;

            void OnComplete(string jsonText)
            {
                jStringSerializer.DeserializeAsync(jsonText, callback);
            }

            jServerRequest.SendRequest(method, url, OnComplete, OnRequestError);
        }

        public void CheatAddTaxesToCollect(int coinsAmount)
        {
            var url = $"/v1/user/cheating/tax/{coinsAmount}";
            var method = RequestMethod.GET;

            jServerRequest.SendRequest(method, url, null, OnRequestError);
        }

        public void CheatSetFreeSkipPrices()
        {
            var url = $"/v1/user/cheating/free-skip";
            var method = RequestMethod.GET;

            jServerRequest.SendRequest(method, url, null, OnRequestError);
        }
    }
}