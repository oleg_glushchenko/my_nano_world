﻿using System;
using System.Collections.Generic;
using Assets.NanoLib.ServerCommunication.Models;
using Assets.NanoLib.Utilities;
using Assets.NanoLib.Utilities.Serializers;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.api;
using Assets.Scripts.GameLogic.DailyBonus.Api;
using GameLogic.Bazaar.Model.impl;
using GameLogic.BuildingSystem.CityAoe;
using NanoReality.Game.CitizenGifts;
using NanoReality.Game.CityStatusEffects;
using NanoReality.Game.FoodSupply;
using NanoReality.Game.Production;
using NanoReality.Game.Services.BuildingsUnlockService;
using NanoReality.Game.Services.OrderDesk.SeaportOrderDesk;
using NanoReality.GameLogic.Achievements;
using NanoReality.GameLogic.Achievements.impl;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityArea;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.OrderDesk;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.StartApplicationSequence.Controllers.Commands;
using NanoReality.GameLogic.BuildingSystem.GlobalCityMap.Models;
using NanoReality.GameLogic.BuildingSystem.MapObjects;
using NanoReality.GameLogic.Configs;
using NanoReality.GameLogic.GameManager.Controllers;
using NanoReality.Game.Tutorial;
using NanoReality.Game.Tutorial.HintTutorial;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Impl;
using NanoReality.GameLogic.IAP;
using NanoReality.GameLogic.SoftCurrencyStore;
using UnityEngine;
using NanoReality.Debugging;
using NanoReality.GameLogic.TheEvent.Data;

namespace NanoReality.GameLogic.ServerCommunication.Models.Api
{
	/// <summary>
	/// Interface to communicate with server
	/// </summary>
	public interface IServerCommunicator
	{
		/// <summary>
		/// Access Tocken, available after authorizetion
		/// </summary>
		string AccessToken { get; set; }

		bool IsAuthorized { get; }

	    /// <summary>
	    /// String serializer used to convert objects before sending typesData to server
	    /// </summary>
	    IStringSerializer jStringSerializer { get; set; }
        
	    IServerRequest jServerRequest { get; set; }

	    event Action<IErrorData> ErrorReceived;

        #region Methods

        void OnRequestError(IErrorData errorData);
	    
	    /// <summary>
	    /// Stops and clears all requests from server queue
	    /// </summary>
	    void StopAndClearServerQueue();

        /// <summary>
        /// Authorize device on server, using UDID
        /// </summary>
        /// <param name="callback">returns build version and player id</param>
        void Authorize(Action<Dictionary<string, string>> callback = null);

        void Logout(Action callback);

        void LoadGameBalanceHashes(LoadGameDataCommand.UrlEtags clientEtags,
            Action<LoadGameDataCommand.UrlEtags> callback = null);

        /// <summary>
        /// Bind Social Network ID to current device Id
        /// </summary>
        /// <param name="socialNetwrokType">social netwrok type</param>
        /// <param name="socialId">social id</param>
        /// <param name="world"></param>
        /// <param name="callback">success callback</param>
        void BindSocialNetwork(string socialNetwrokType, string socialId, CommandOnAutorizeInSocialNetwork.SelectWorld world, Action<ShortCityData> callback = null);

        /// <summary>
        /// Bind current device id to social network ID
        /// </summary>
        /// <param name="socialNetwrokType">social network type</param>
        /// <param name="socialId">social id</param>
        /// <param name="callbackAction">success callback</param>
        void BindDeviceIdToSocialNetwork(string socialNetwrokType, string socialId, Action callbackAction = null);

        /// <summary>
        /// Load user profile from the server
        /// </summary>
        void LoadUserProfile(IPlayerProfile profile, Action<IPlayerProfile> callback = null);

        /// <summary>
        /// Gives a list of all levels that have been add in the admin panel
        /// </summary>
        void LoadUserLevels(IUserLevelsData userLevelsData, Action<IUserLevelsData> callback = null);

        void RenameCity(string cityName, Id<IUserCity> cityId, Action<bool> callback);

        /// <summary>
        /// Gives a list of all products that have been add in the admin panel
        /// </summary>
        void LoadProducts(IProductsData productsData, Action<IProductsData> callback = null);

        void PurchaseProduct(Id<IUserCity> userCityId, Id<Product> productId, int count, Action onSuccess = null);

		void PurchaseProducts(Id<IUserCity> userCityId, Dictionary<Id<Product>, int> products, Action onSuccess = null);

        /// <summary>
        /// Gives a list of all mapObjects that have been add in the admin panel
        /// </summary>
        void LoadMapObjects(Action<IMapObjectsData> callback);

        /// <summary>
        /// Загружает данные гейм баланса о скипе постройки за премиум
        /// </summary>
        /// <param name="data"></param>
        /// <param name="callback"></param>
        void LoadMapBuildingsPremiumSkipData(IMapBuildingsSkipData data, Action<IMapBuildingsSkipData> callback);

        void LoadBusinessSectorMap(LoadCityMapResponseModel responseModel);

        void LoadSubSectors(Id<IUserCity> userCityId,  Action<CitySubLockedMap> callback);

        void UnlockSubSector(Id<IUserCity> userCityId, Id<ILockedSector> subSectorId, Action<bool> successCallback);

        /// <summary>
        /// Create new building instance in some map sector.
        /// </summary>
        /// <param name="businessSectorId">Map Sector.</param>
        /// <param name="buildingId">Building Type Id</param>
        /// <param name="buildingPosition">Placing position</param>
        /// <param name="onSuccess">Returns unique MapId of new building.</param>
        void ConstructMapObject(int businessSectorId, int buildingId, Vector2Int buildingPosition, Action<int> onSuccess);

        void ConstructRoad(int businessSectorId, int type, List<Vector2Int> positionsList, Action<CreateRoadResponse> onSuccess);

		void SyncBuilding(int buildingMapId, Action<SyncBuildingResponse> onSuccess);

		void BuildingSkip(Func<string> GetMapId, Action onSuccess);

		void UpgradeMapObject(int buildingMapId, Action onSuccess);

        void PushUpgradeProduct(Func<string> mapId, Id<Product> productID, Action onSuccess);
        void MoveMapObject(Func<string> mapId, Vector2F newPosition, Action<MoveMapObjectResponce> onSuccess);

        void SwitchAoeLayoutType(Func<string> mapId, AoeLayout aoeLayout, bool isEnergy, Action<MoveMapObjectResponce> onSuccsess);

        void DeleteRoad(List<Id_IMapObject> ids, Action<DeleteRoadResponse> onSuccess);

        void StartProduceGood(Func<string> mapObjectId, Id<IProduceSlot> produceSlotId, Id<Product> productId, Action<int, int> onSuccess);

		void VerifyProduceGood(Func<string> produceSlotId, Action<int> onSuccess);

		void ShipProductFromFactory(Func<string> id, Action  onSuccess);

		void UnlockFactorySlot(Func<string> mapObjectId, Action<int> onSuccess);

        void SkipFactoryProduction(Func<string> mapId, Func<string> slotId, Action<bool> onSuccess);

        /// <summary>
        /// Create server request for check current city happiness value.
        /// </summary>
        /// <param name="onSuccess">Callback that receive current happiness value</param>
        void GetHappinessInfo(Action<int> onSuccess);

        #region Agricultural

        void StartGrowing(Func<string> mapObjectId, Id<Product> productId, Action onSuccess);

		void VerifyGrowing(Func<string> mapObjectId, Action<int> onSuccess);

		void Harvesting(Func<string> mapObjectId, Id<IUserCity> userId, Action<int> onSuccess);

		void SkipAgriculturalGrowing(Id<IUserCity> cityId, Func<string> mapId, Action<bool> onSuccess);

        void FreeSkipAgriculturalGrowing(Id<IUserCity> cityId, Func<string> mapId, Action<bool> onSuccess);

        void FertilizeAgriculturalGrowing(Id<IUserCity> cityId, Func<string> mapId, Action<int> onSuccess);

        #endregion

        #region Metro Order Desk
        
        void LoadMetroOrderDesk(Action<NetworkMetroOrderDesk> onSuccessCallback);

        void LoadMetroTutorialOrderDesk(bool isReward, Action<NetworkMetroOrderDesk> onSuccessCallback);

        void ShuffleMetroOrder(int orderId, Action<NetworkMetroOrderDesk> onSuccessCallback);

        void SkipShuffleMetroOrder(int orderId, bool isPaid, Action<NetworkMetroOrderDesk> onSuccessCallback);

        void LoadMetroProductsToSlot(int slotId, Action<NetworkMetroOrderDesk> onSuccessCallback);

        void LoadMetroTutorialProductsToSlot(int slotId, Action<NetworkMetroOrderDesk> onSuccessCallback);

        void ShippingMetroTutorialOrder(int trainId, Action<NetworkMetroOrderDesk> onSuccessCallback);

        void ShippingMetroOrder(int orderId, Action<NetworkMetroOrderDesk> onSuccessCallback);

        void SkipMetroOrderWaitingTime(int orderId, Action<NetworkMetroOrderDesk> onSuccessCallback);

        #endregion
        
        #endregion

        #region Achievements

        void LoadAchievementsData(Action<List<Achievement>> onSuccess);

        void LoadUserAchievementsData(Action<IUserAchievementsData> onSuccess);

        void VerifyAchievementUnlocked(Id<Achievement> id, Action onSuccess);

		void TakeAchievementAward(Id<Achievement> id, Action<List<IAwardAction>> onSuccess);

        #endregion

        #region Monetization

        void LoadInAppShopData(Action<InAppShopData> callback);

        void LoadInAppShopOfferData(Action<InAppShopOffersData> callback);

        void BuyInAppProduct(int productId, string orderId, string token, Action<int, bool> callback);

        void LoadSoftCurrencyBundles(Action<ISoftCurrencyBundlesModel> onSuccess);

        void PurchaseSoftCurrencyBundle(string bundleId, Action callback);

        #endregion

        #region Quests

		/// <summary>
		/// Loads all quests data
		/// </summary>
		void LoadQuestBalanceData(Action<IEnumerable<IQuest>> onSuccess);

		/// <summary>
		/// Loads user's quests progress data
		/// </summary>
		void LoadUserQuestsData(IUserQuestData userQuestData, Action<IUserQuestData> onSuccess);
		
		void VerifyQuestUnlocked(int questId, Action onSuccess = null);
        void StartQuest(Id<IQuest> questId, Action onSuccess = null);
        void VerifyQuestFinished(int questId, Action onSuccess = null);
        void CollectQuest(Id<IQuest> questId, Id<IUserCity> cityId, Action<List<IAwardAction>> onSuccess);
        void CollectQuestsAwards(List<int> questsId, Action<Dictionary<int,List<IAwardAction>>> onSuccess);

        #endregion

        void LoadSkipIntervalsBalanceData(Action<SkipIntervalsResponse> callback = null);

        #region Video AD

        /// <summary>
        /// Получает список возможных наград за просмотр рекламы
        /// </summary>
        /// <param name="onSuccess"></param>
        void GetRewardInfoForVideo(Action<List<IAwardAction>> onSuccess = null);
        
       /// <summary>
       /// Получает информацию о выигранной награде за просмотр рекламы
       /// </summary>
       /// <param name="onSuccess"></param>
        void GetRewardItemForVideo(Action<List<IAwardAction>> onSuccess = null);

        /// <summary>
        /// Забирает выигранную награду за просмотр рекламы
        /// </summary>
        /// <param name="sekretKey"></param>
        /// <param name="onSuccess"></param>
        void ClimeRewardForVideo(string sekretKey, Action<List<IAwardAction>> onSuccess = null);
	    
	    /// <summary>
	    /// Resets casino free spin after video ad was watched
	    /// </summary>
	    /// <param name="onSuccess"></param>
	    /// <param name="onSuccess"></param>
	    void ResetCasinoFreeSpinTime(Action onSuccess);

	    #endregion

		#region DailyBonus

        /// <summary>
        /// Загружает данные дейли бонусов
        /// </summary>
        /// <param name="dailyBalanceData">объект куда загрузить данные</param>
        /// <param name="callback">вызывается после загрузки с сервера</param>
        void LoadDailyBonusBalanceData(IDailyBonusBalanceData dailyBalanceData, Action<IDailyBonusBalanceData> callback);

        /// <summary>
        /// Собрать бонус
        /// </summary>
        /// <param name="day">день за который нужно собрать бонусы</param>
        /// <param name="onSucsess">вызывается после успешной загрузки с сервера</param>
        void CollectDailyBonus(int day, Action<List<IAwardAction>> onSucsess);

        #endregion

        #region Tutorial
        
        void SoftTutorialStepCompleted(SoftTutorialType step);

        void HintTutorialStepCompleted(HintType step, Action<List<HintStepStatus>> onSuccessCallback);

        void GetHintTutorialStatus(Action<List<HintStepStatus>> onSuccessCallback);

        void HardTutorialStepCompleted(Action<StepType> successAction);

        void JumpHardTutorialStep(StepType step, Action successAction);

        #endregion

        #region Taxes
        
        void CollectTaxes(Action<int, CityTaxes> callback);

        #endregion

        #region Edit Mode

        void GetEditModeLayouts(Action<List<EditModeLayout>> onComplete);
        void GetEditModeLayoutData(int layoutId, Action<EditModeLayoutData> onComplete);
        void EditModeClearMap(int layoutId, Action<EditModeLayoutData> onComplete);
        void EditModeRemoveBuilding(int layoutObjectId, Action<EditModeHappinessMap> onComplete);
        void EditModeApplyLayout(int layoutId, string screenshot, Action<bool> onComplete);
        void EditModeCancelLayout(int layoutId, string screenshot, Action<bool> onComplete);
        void EditModeChangeAOE(int layoutObjectId, int aoeType, Action<EditModeHappinessMap> onComplete);
        void EditModeMoveBuilding(int layoutObjectId, Vector2Int position, Action<EditModeHappinessMap> onComplete);
        void EditModeRestoreBuilding(int layoutId, int mapId, Vector2Int position, Action<EditModeHappinessMap> onComplete); 
        void EditModeAddRoad(int layoutId, string bssId, List<Vector2Int> positions, Action<EditModeCreateRoadResponse> onComplete);
        void EditModeRemoveRoad(int layoutId, string bssId, List<Id_IMapObject> roads, Action<EditModeDeleteRoadResponse> onComplete);

        #endregion

        #region Food Supply

        void GetFoodSupplyCityInfo(Action<FoodSupplyCityInfo> onComplete);

        void GetFoodSupplyData(Action<FoodSupplyData> onComplete);
        
        void GetTutorialFoodSupplyData(Action<FoodSupplyData> onComplete);

        void CookFoodSupplyProducts(int recipeId, Action<FoodSupplyData> callback);

        void CookTutorialFoodSupplyProducts(int recipeId, Action<FoodSupplyData> callback);

        void FoodSupplyFeedingProducts(int buildingId, Dictionary<int, int> productIds, Action<FoodSupplyCityInfo> callback);

        void UnlockFoodSupplySlot(int buildingId, int slotId, Action<FoodSupplyData> callback);

        #endregion

        #region GlobalCityMap

        void LoadGlobalCityMap(Id<IUserCity> userCityId, Id<IPlayerProfile> userId, Id<IBusinessSector> businessSectorId, Action<IGlobalCityMap> successCallback);

        #endregion

        #region Time

        /// <summary>
        /// Requests current server time.
        /// </summary>
        /// <param name="callback"></param>
        void GetServerTime(Action<long> callback);

        #endregion

	    #region Cheats

	    void CheatAddValue(string actionType, int amount, Action callback);

	    void CheatAddAmount(string valueType, int amount, Action callback);

	    void CheatAddProduct(int productId, int amount, Action callback);

	    void CheatGetPresets(Action<PresetData[]> onComplete);

	    void CheatSetPreset(int presetId, Action onComplete);
	    
	    void CheatAddTaxesToCollect(int coinsAmount);

	    void CheatSetFreeSkipPrices();

        #endregion

        #region TheEvent

        void PurchaseEventProduct(Id<Product> productId, int count, Action onSuccess = null);
        void GetEventBaseData(Action<BaseEventData> onSucces, Action onError);
        void GetEventFinishedData(Action<FinishEventData> callback);
        void ClaimDailyReward(int rewardId, Action callback);
        void ClaimStageReward(int stageId, Action<ClaimStageRewardData> callback);
        void BuyEventBuilding(int productId, string token, string orderId, Action<int, bool> callback);
        void FinalizeEvent(Action onFinalize);

        #endregion

        void RepairBuilding(Id_IMapObject mapID, Action onServerConfirmedRepair);

	    #region Configs

	    void LoadGameConfigData(IGameConfigData data, Action<IGameConfigData> onSuccess);

	    void LoadLinksConfigData(Action<LinksConfigData> onSuccess);
	    
	    void LoadBuildingSlotsData(BuildingSlotsData buildingSlotsData, Action<List<BuildingSlotData>> callback);
	    
	    #endregion

	    void LoadAoeData(Action<CityAoeData> onSuccess);

	    #region CityArea

	    void LoadCityAreaData(ICityAreaData serviceBuildingsData, Action<ICityAreaData> onSuccess);

	    #endregion

	    void LoadSeaportOrders(Action<NetworkSeaportOrderDesk> resultCallback);
	    
	    void ShuffleSeaportOrder(int orderId, Action<NetworkSeaportOrderDesk> resultCallback);

	    /// <summary>
	    /// Skip shuffle waiting in Seaport order desk.
	    /// </summary>
	    /// <param name="orderId"></param>
	    /// <param name="adsSkip">Is skipped by wathing ads or currency.</param>
	    /// <param name="resultCallback"></param>
	    void ConfirmSkipShuffleOrder(int orderId, bool adsSkip, Action resultCallback);
	    void SendSeaportOrder(int orderId, Action<NetworkSeaportOrderDesk> resultCallback);

	    #region Souk implementation

	    void LoadSouk(Action<SoukData> callback);

	    void ShuffleSouk(Action<SoukData> callback);

	    void UnlockTopSoukSlot(int slotId, Action<SoukData> callback);

	    void UnlockBottomSoukSlot(int slotId, Action<SoukData> callback);

	    void BuyTopSoukSlot(int slotId, Action<SoukData> callback);

	    void BuyBottomSoukSlot(int slotId, Action<SoukData> callback);
	    
	    void SkipSoukShuffleTime(Action<SoukData> callback);

	    void BuySoukLootbox(int lootboxIndex, Action<SoukData> onSuccessCallback);

	    #endregion

	    /// <summary>
	    /// Requests list with balance data for city status effects buffs/de-buffs.
	    /// </summary>
	    /// <param name="onComplete"></param>
	    void GetStatusEffectsBalanceData(Action<CityStatusEffectSettings[]> onComplete);

	    /// <summary>
	    /// Returns list of current active status effects for player city.
	    /// </summary>
	    /// <param name="onComplete">Active status effects array.</param>
	    void GetCityStatusEffects(Action<CityStatusEffect[]> onComplete);

	    void LoadAvailableCitizensGifts(Action<CitizenGift[]> onComplete);

	    void ClaimCitizenGift(int buildingId, Action onComplete);

	    /// <summary>
	    /// Returns updates status of all production slots for all productions buildings.
	    /// </summary>
	    /// <param name="activeProductionSlots">Slots list.</param>
	    void GetProductionSlots(Action<List<BuildingProductionInfo>> activeProductionSlots);

	    void LoadAssetBundleHashes(Action<Dictionary<RuntimePlatform, uint>> onSuccess);

	    void LoadAoeDataNew(Action<CityAoeData> onSuccess);

	    void GetTaxesInfo(Action<CityTaxes> callback);

	    void LoadBuildingInstancesUnlockData(Action<List<BuildingInstanceUnlockInfo>> callback);
	}
}