﻿using System.Collections.Generic;

namespace NanoReality.GameLogic.ServerCommunication
{
    public static class ServerResponseCodes
    {
        public const int Continue = 100;
        
        // Success
        public const int SuccessOK = 200;
        public const int SuccessCreated = 201;
        
        // Redirection
        public const int RedirectionTooManyRequests = 302;
        public const int RedirectionNotModified = 304;

        // Client Error
        public const int ClientErrorBadRequest = 400;
        public const int ClientErrorUnauthorized = 401;
        public const int ClientErrorForbidden = 403;
        public const int ClientErrorNotFound = 404;
        public const int ClientErrorRequestTimeout = 408;
		
        // Logic Errors
        public const int LogicNotEnoughSoftMoney = 600;
        public const int LogicNotEnoughPremiumMoney = 601;
        public const int LogicNotEnoughResources = 602;
        public const int LogicInvalidObjectPositionOnMap = 603;
        public const int LogicNotAvailable = 604;
        public const int LogicIsAlreadyUpgrading = 605;
        public const int LogicObjectsIntersectOnMap = 606;
        public const int LogicNotEnoughLevelForUnlock = 607;
        public const int LogicTechnicalMaintenance = 608;
        public const int LogicNotEnoughSpaceWarehouse = 609;
        public const int LogicNotEnoughElectricPower = 610;
        public const int LogicNotEnoughSomething = 611;
        public const int LogicNotEnoughWorkers = 612;
        public const int LogicInAppPurchaseError = 613;
        public const int LogicAuctionClosed = 614;
        public const int LogicAuctionBetError = 615;
        public const int LogicGiftError = 616;
        public const int LogicWrongTime = 617;
        public const int LogicNotEnoughSkipFoodSupply = 618;
        public const int LogicNotEnoughEnergyBurger = 619;
        public const int LogicNotEnoughSkipWatchVideo = 620;
        public const int LogicNotValidateEmailByDNS = 621;
        public const int LogicDatabaseException = 622;
        
        // Payment Errors
        public const int PaymentNotConfirmed = 623;

        // Custom Errors
        public const int ResponseUnknownError = 0;
        public const int ResponseIsNotYetProcessed = -1;
        public const int ClientInternalException = -101;
        public const int PoorConnection = -102;
        public const int ResponseTimeout = -1001;
        public const int NoInternetConnection = -1009;
        
        private static readonly List<int> SuccessServerResponseCodes = new List<int>
        {
            SuccessOK,
            SuccessCreated,
            LogicGiftError,
            LogicAuctionBetError,
            LogicNotValidateEmailByDNS,
        };

        public static bool IsSuccess(int responseCode)
        {
            return SuccessServerResponseCodes.Contains(responseCode);
        }
    }
}
