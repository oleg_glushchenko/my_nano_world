﻿using System.Text;
using System.Text.RegularExpressions;

namespace GameLogic.Utilites
{
    public static class StringExtensions
    {
        public static string WrapToJsonObjectWithField(this string self, string fieldName)
        {
            if (string.IsNullOrEmpty(self) || string.IsNullOrEmpty(fieldName)) return self;
            
            var stringBuilder = new StringBuilder(self.Length + fieldName.Length + 10);
            stringBuilder.Append("{\"");
            stringBuilder.Append(fieldName);
            stringBuilder.Append("\": ");
            stringBuilder.Append(self);
            stringBuilder.Append("}");
            
            return stringBuilder.ToString();
        }

        public static bool IsEmailValid(string email)
        {
            const string emailPattern = @"^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$";
            return Regex.IsMatch(email, emailPattern);
        }
    }
}