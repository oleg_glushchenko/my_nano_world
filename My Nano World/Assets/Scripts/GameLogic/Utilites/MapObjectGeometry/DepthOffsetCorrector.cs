﻿using UnityEngine;
using strange.extensions.mediation.impl;

namespace NanoReality.GameLogic.Utilities
{
    public class DepthOffsetCorrector : View
    {
        Transform[] mapObjectsCollection; 

        [SerializeField] private float extraDepthShift = 0f;

        protected override void Start()
        {
            base.Start();
            mapObjectsCollection = transform.GetComponentsInChildren<Transform>(); 
            foreach (var mapObject in mapObjectsCollection)
            {
                if(mapObject!=transform)
                    mapObject.position = MapObjectGeometry.PlaceObjectInDepth(mapObject.position, extraDepthShift);
            }
          
        } 
    }
}
