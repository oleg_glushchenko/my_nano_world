﻿using UnityEngine;

namespace NanoReality.GameLogic.Utilities
{
    public class MapObjectGeometry
    { 
        /// <summary>
        /// returns new temp pos of map object according to the map
        /// </summary>
        /// <param name="position">position on a map</param>
        /// <param name="mapHeight">map height</param>
        /// <param name="mapWidth">map width</param>
        /// <returns></returns>
        public static float GetMapObjectPosY(Vector3 position, int mapHeight, int mapWidth )
        {
            return Mathf.Round(((mapHeight - position.x) + (mapWidth - position.z)) / 2);
        }

        public static float GetMapObjectPosY(Vector2 isoPosition, int mapHeight, int mapWidth)
        {
            return Mathf.Round(((mapHeight - isoPosition.x) + (mapWidth - isoPosition.y)) / 2);
        }

        public static int GetMapObjectRenderQue(Vector2 position, int mapHeight, int mapWidth)
        {
            return 2050+ Mathf.RoundToInt(((mapHeight*2 - position.x*2) + (mapWidth*2 - position.y*2)) / 2);
        }

        /// <summary>
        /// Returns the height offset 
        /// </summary>
        /// <param name="height"></param>
        /// <param name="cameraDegrees"></param>
        /// <returns></returns>
        public static float GetHeightOffset(float height, float cameraDegrees)
        {
            var gipotenuz = Mathf.Tan(Mathf.Deg2Rad * (90 - cameraDegrees)) * height;
            return Mathf.Sqrt(Mathf.Pow(gipotenuz, 2) / 2);
        }

        public static Vector3 PlaceObjectInDepth(Vector3 position, float depthShift)
        {  
            float devider = 1.5f;
            float shift = 100f;

            var m =  Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(30f,45f,0), Vector3.one) ;              
            m = Matrix4x4.Inverse(m); 
            m.m20 *= -1f;
            m.m21 *= -1f;
            m.m22 *= -1f;
            m.m23 *= -1f;
            
            Vector3 scrPosition = m.MultiplyPoint3x4(position);
            Vector3 scrCorectedPosition = new Vector3(scrPosition.x, scrPosition.y, -scrPosition.y / devider + depthShift + shift);

            var izoPosition = m.inverse.MultiplyPoint3x4(scrCorectedPosition);
            return izoPosition; 
        }

        public static bool TestPlanesAABBInside(Plane [] planes, Vector3 boundsMin, Vector3 boundsMax)
        {
            Vector3 vmin, vmax;

            for(int planeIndex = 0; planeIndex < planes.Length; planeIndex++)
            {
                var normal= planes [planeIndex].normal;
                var planeDistance= planes [planeIndex].distance;
 
                // X axis
                if(normal.x < 0)
                {
                    vmin.x = boundsMin.x;
                    vmax.x = boundsMax.x;
                }
                else
                {
                    vmin.x = boundsMax.x;
                    vmax.x = boundsMin.x;
                }
 
                // Y axis
                if (normal.y < 0)
                {
                    vmin.y = boundsMin.y;
                    vmax.y = boundsMax.y;
                }
                else
                {
                    vmin.y = boundsMax.y;
                    vmax.y = boundsMin.y;
                }
 
                // Z axis
                if (normal.z < 0)
                {
                    vmin.z = boundsMin.z;
                    vmax.z = boundsMax.z;
                }
                else
                {              
                    vmin.z = boundsMax.z;
                    vmax.z = boundsMin.z;
                }
 
                var dot1 = normal.x * vmin.x + normal.y * vmin.y + normal.z * vmin.z;
                if (dot1 + planeDistance < 0)
                    return false;
 
                var dot2 = normal.x * vmax.x + normal.y * vmax.y + normal.z * vmax.z;
                if (dot2 + planeDistance <= 0)
                    return false;
            }
 
            return true;
        }
    }
}