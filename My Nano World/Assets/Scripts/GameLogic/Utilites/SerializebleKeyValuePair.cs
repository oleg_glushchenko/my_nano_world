﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace NanoReality.GameLogic.Utilities
{
    /// <summary>
    /// Class for key value pair object serializeble for server
    /// </summary>
    public class SerializebleKeyValuePair
    {
        [JsonProperty("attribute_name")] 
        public string KeyName;


        [JsonProperty("value")] 
        public string KeyValue;

        public static string GetValue(List<SerializebleKeyValuePair> paramsArray, string key)
        {
            var result = paramsArray.Find(curr => curr.KeyName == key);
            return result == null ? "" : result.KeyValue;
        }
    }
}
