﻿using System;
using UnityEngine;

namespace NanoReality.GameLogic.Utilites
{
    public static class VectorExtensions
    {
        public static Vector3 ToVector3(this Vector2 pos, float z = 0f)
        {
            return new Vector3(pos.x, pos.y, z);
        }

        /// <summary>
        /// Swaps x and y of the given vector
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static Vector2 Rotate(this Vector2 v)
        {
            return new Vector2(v.y, v.x);
        }

        public static int GetIntX(this Vector2 vector)
        {
            return Convert.ToInt32(vector.x);
        }
        
        public static int GetIntY(this Vector2 vector)
        {
            return Convert.ToInt32(vector.y);
        }
    }
}
