﻿using System;
using System.Globalization;
using System.Text;

namespace NanoReality.GameLogic.Utilities
{
    public static class ActionExtensions
    {
        public static void SafeRaise(this Action action)
        {
            var a = action;
            if (a != null)
            {
                a.Invoke();
            }
        }

        public static void SafeRaise<T1, T2>(this Action<T1, T2> action, T1 arg1, T2 arg2)
        {
            var a = action;
            if (a != null)
            {
                a.Invoke(arg1, arg2);
            }
        }

        public static void SafeRaise<T1, T2, T3>(this Action<T1, T2, T3> action, T1 arg1, T2 arg2, T3 arg3)
        {
            var a = action;
            if (a != null)
            {
                a.Invoke(arg1, arg2, arg3);
            }
        }

        public static void SafeRaise<T1, T2, T3, T4>(this Action<T1, T2, T3, T4> action, T1 arg1, T2 arg2,
            T3 arg3, T4 arg4)
        {
            var a = action;
            if (a != null)
            {
                a.Invoke(arg1, arg2, arg3, arg4);
            }
        }
    }
}