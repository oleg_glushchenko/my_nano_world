﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR
public class ManualDepthSetterTool : EditorWindow
{
    private float HeightOffset = 0;
    private float HeightToDepthRatio = .39f;
    private float GroundShift = 5f;

    private GameObject[] _objects;

    // Add menu named "My Window" to the Window menu
    [MenuItem("Window/Manual Depth Setter Tool")]
    private static void Init()
    {
        // Get existing open window or if none, make a new one:
        var window = (ManualDepthSetterTool) GetWindow(typeof(ManualDepthSetterTool));
        window.Show();
    }

    private void OnSelectionChange()
    {
        _objects = Selection.gameObjects;
    }

    private void UpdateDepth()
    {
        foreach (GameObject go in _objects)
        {
            Vector3 localPosition = go.transform.localPosition;
            var groundShift = go.name.Contains("Ground") ? GroundShift : 0f;
            go.transform.localPosition = new Vector3(localPosition.x, localPosition.y,
                localPosition.y * HeightToDepthRatio + HeightOffset + groundShift);
        }
    }

    private void OnGUI()
    {
        GUILayout.Label("Base Settings", EditorStyles.boldLabel);

        HeightToDepthRatio = EditorGUILayout.Slider("HeightToDepthRatio", HeightToDepthRatio, .00001f, 3);
        HeightOffset = EditorGUILayout.Slider("HeightOffset", HeightOffset, -3, 3);
        GroundShift = EditorGUILayout.Slider("GroundShift", GroundShift, -10, 10);

        if (GUILayout.Button("Align Object"))
        {
            UpdateDepth();
        }

        EditorGUILayout.EndToggleGroup();
    }
}
#endif