﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.GameLogic.Utilites;
using I2.Loc;
using NanoReality.GameLogic.Utilites;
using UnityEngine;

namespace NanoReality.Game.Utility
{
#if UNITY_EDITOR
    public static class LocalizationConvertTool
    {

        //public static List<string> keys = new List<string>();

        [UnityEditor.MenuItem("NanoReality/Localization/Localization Convert Tool", priority = 1)]
        private static void Replace()
        {
            LocalizationMLS.Instance.ChangeLanguage("en");
            Dictionary<string, string> EnLocalization = new Dictionary<string, string>();

            for (int i = 125; i < 550; i++)
            {
                var fullkey = string.Format(LocalizationKeyConstants.Objects.PRODUCT, i);
               
                if (EnLocalization.ContainsKey(fullkey))
                {
                    continue;
                }
                var en = LocalizationMLS.Instance.GetText(fullkey);
                
                if(fullkey == en) continue;
                
                Debug.LogError($" add key: {fullkey} + EN {en}");

                EnLocalization.Add(fullkey, en);
            }

            LocalizationMLS.Instance.ChangeLanguage("ar");
            
            foreach (var key in EnLocalization)
            {
                var keyCached = key.Key;
                var ar = LocalizationMLS.Instance.GetText(key.Key);
                 Debug.LogError($"{key.Key} ||| {key.Value} ||| {ar}");
                var term = LocalizationManager.Sources.FirstOrDefault().AddTerm(keyCached);
                term.SetTranslation(0, key.Value);
                term.SetTranslation(1, ar);
            }

            Debug.LogError("Save;");
            LocalizationManager.Sources.FirstOrDefault().SaveLanguages(true);
            LocalizationManager.Sources.FirstOrDefault().Editor_SetDirty();
        }
    }
#endif
}