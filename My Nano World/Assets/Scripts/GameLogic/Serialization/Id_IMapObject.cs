using System;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;

[Serializable]
public sealed class Id_IMapObject : Id<IMapObject>
{
    public Id_IMapObject(int i) : base(i)
    {
    }

    public Id_IMapObject()
    {
    }

    public static implicit operator Id_IMapObject(Int64 x)
    {
        return new Id_IMapObject((int) x);
    }

    public static implicit operator int(Id_IMapObject obj)
    {
        return obj.Value;
    }

    public static implicit operator Id_IMapObject(string x)
    {
        int result;
        if (!int.TryParse(x, out result))
        {
            throw new Exception("failed to parse id from string '" + x + "'");
        }

        ;
        return new Id_IMapObject(result);
    }
}