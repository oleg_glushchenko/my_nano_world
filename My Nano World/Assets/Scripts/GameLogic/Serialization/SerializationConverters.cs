﻿using System;
using System.Collections.Generic;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.api.CityOrderDesks;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.api.Mines;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.api.OrderDesks;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.api;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.impl;
using Assets.Scripts.GameLogic.DailyBonus.Api;
using GameLogic.BuildingSystem.CityArea;
using GameLogic.Quests.Models.api.Extensions;
using GameLogic.Quests.Models.impl.Extensions;
using NanoLib.Core.Logging;
using NanoLib.Utilities;
using NanoReality;
using NanoReality.Engine.Utilities.Serialization;
using NanoReality.Game.Utilities;
using NanoReality.GameLogic.Achievements;
using NanoReality.GameLogic.Achievements.api;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.City.Models.Impl;
using NanoReality.GameLogic.BuildingSystem.CityArea;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Impl;
using NanoReality.GameLogic.BuildingSystem.GlobalCityMap.Models;
using NanoReality.GameLogic.BuildingSystem.MapObjects;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.impl.Factories;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.SoftCurrencyStore;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.Happiness;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.SupplyFood;
using NanoReality.GameLogic.Configs;

public class SerializationConverters
{
    private static List<JsonConverter> _converters;

    public static List<JsonConverter> Converters
    {
        get
        {
            if (_converters == null) CollectConverters();
            return _converters;
        }
    }

    private static void CollectConverters()
    {
		_converters = new List<JsonConverter> ();
		
        Type[] types = typeof(SerializationConverters).GetNestedTypes();

        foreach (Type curr in types)
        {
            object newOne = Activator.CreateInstance(curr);
            if (newOne is JsonConverter )
            {
                JsonConverter jsonConverter = (JsonConverter)newOne;
                _converters.Add(jsonConverter);
            }
            else
                throw new Exception("nested class in SerializationConverters which is not JsonConverter detected! " + curr.Name);
        }
    }

    public static JsonSerializerSettings Settings => _settings ?? (_settings = CreateSettings());
    private static JsonSerializerSettings _settings;

    private static JsonSerializerSettings CreateSettings()
    {
        var settings = new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.All,
            NullValueHandling = NullValueHandling.Ignore,
            ObjectCreationHandling = ObjectCreationHandling.Replace
        };
        settings.Converters.AddRange(Converters);

        return settings;
    }

    public static T DeserializeObject<T>(string str)
    {
        T result = default;
        try
        {
            result = JsonConvert.DeserializeObject<T>(str, Settings);
        }
        catch (Exception e)
        {
            throw e;
        }

        return result;
    }



    public static object DeserializeObject(string str, Type T)
    {

        object result = null;
        try
        {
            result = JsonConvert.DeserializeObject(str, T, Settings);

        }
        catch (Exception e)
        {
            ProcessError(e);
        }

        return result;
    }

   

    public static object DeserializeObjectWithoutConverters(string str, Type T)
    {
        object result = null;
        try
        {
            result = JsonConvert.DeserializeObject(str, T);
        }
        catch (Exception e)
        {
            ProcessError(e);
        }

        return result;
    }





    private static void ProcessError(Exception e)
    {
        if (e is JsonSerializationException)
        {
            if (e.Message.Contains("Type is an interface or abstract class and cannot be instantated."))
            {
                JsonSerializationException exc =
                    new JsonSerializationException("Looks like you forgot to write a converter for the interface named below in the SerializationConverters class.\n"
                                                   +
                                                   " You should create nested converter class inherited from JsonCreationConverter<InterfaceNamedFromBelow> in the SerializationConverters class. \n\n" +
                                                   e.Message);
                throw exc;
            }
        }
        throw e;
    }


    //***********************************************************************************************************************
    //***********************************************************************************************************************
    //***********************************************************************************************************************
    //***********************************************************************************************************************
    //***********************************************************************************************************************
    //***********************************************************************************************************************
    //***********************************************************************************************************************
    //***********************************************************************************************************************
    //***********************************************************************************************************************
    //***********************************************************************************************************************

    #region converters
    
    public class ProfileConverter :JsonCreationConverter<IPlayerProfile>
    {
        protected override IPlayerProfile Create(Type objectType, JObject jObject)
        {
            return NanoRealityGameContext.InjectionBinder.GetInstance<IPlayerProfile>();
        }
    }

    public class ConverterIMapObject : JsonCreationConverter<IMapObject>
    {
        protected override IMapObject Create(Type objectType, JObject jObject)
        {
            var type = jObject["building_logic_type"];
            if (type == null)
            {
                throw new Exception("MapObjectType is absent in " + jObject);
            }
            
            var intType = type.Value<int>();
            var objType = (MapObjectintTypes)intType;
            
            switch (objType)
            {
                case MapObjectintTypes.DwellingHouse:
                    return NanoRealityGameContext.InjectionBinder.GetInstance<IDwellingHouseMapObject>();
                case MapObjectintTypes.Warehouse:
                    return NanoRealityGameContext.InjectionBinder.GetInstance<IWarehouseMapObject>();               
                case MapObjectintTypes.Road:
                    return NanoRealityGameContext.InjectionBinder.GetInstance<IRoad>();
                case MapObjectintTypes.CityHall:
                    return NanoRealityGameContext.InjectionBinder.GetInstance<ICityHall>();
                case MapObjectintTypes.Factory:
                    return NanoRealityGameContext.InjectionBinder.GetInstance<IFactoryObject>();
                case MapObjectintTypes.PowerPlant:
                    return NanoRealityGameContext.InjectionBinder.GetInstance<IPowerPlantBuilding>();
                case MapObjectintTypes.Mine:
                    return NanoRealityGameContext.InjectionBinder.GetInstance<IMineObject>(); 
                case MapObjectintTypes.EntertainmentBuilding:
                    return NanoRealityGameContext.InjectionBinder.GetInstance<IBuildingProduceHappiness>();
                case MapObjectintTypes.AgriculturalField:
                    return NanoRealityGameContext.InjectionBinder.GetInstance<IAgroFieldBuilding>();
                case MapObjectintTypes.Decoration:
                    return NanoRealityGameContext.InjectionBinder.GetInstance<IDecorationMapObject>();
                case MapObjectintTypes.OrderDesk:
                    return NanoRealityGameContext.InjectionBinder.GetInstance<IOrderDeskMapBuilding>();
                case MapObjectintTypes.CityOrderDesk:
                    return NanoRealityGameContext.InjectionBinder.GetInstance<ICityOrderDeskMapBuilding>();
                case MapObjectintTypes.FoodSupply:
                    return NanoRealityGameContext.InjectionBinder.GetInstance<IFoodSupplyBuilding>();
                case MapObjectintTypes.TradingShop:
                    return NanoRealityGameContext.InjectionBinder.GetInstance<ITradingShopBuilding>();
                case MapObjectintTypes.TradingMarket:
                    return NanoRealityGameContext.InjectionBinder.GetInstance<ITradingMarketBuilding>();
                case MapObjectintTypes.IntercityRoad:
                    return NanoRealityGameContext.InjectionBinder.GetInstance<IIntercityRoad>();
                case MapObjectintTypes.Quests:
                    return NanoRealityGameContext.InjectionBinder.GetInstance<IQuestsBuilding>();
                case MapObjectintTypes.GroundBlock:
                    return NanoRealityGameContext.InjectionBinder.GetInstance<IGroundBlock>();
                case MapObjectintTypes.Police:
                    return NanoRealityGameContext.InjectionBinder.GetInstance<PoliceBuilding>();                
                case MapObjectintTypes.School:
                    return NanoRealityGameContext.InjectionBinder.GetInstance<SchoolBuilding>();
                case MapObjectintTypes.Hospital:
                    return NanoRealityGameContext.InjectionBinder.GetInstance<HospitalBuilding>();
                case MapObjectintTypes.EventBuilding:
                    return NanoRealityGameContext.InjectionBinder.GetInstance<IEventBuilding>();
                case MapObjectintTypes.EventBuildingReward:
                    return NanoRealityGameContext.InjectionBinder.GetInstance<IEventBuildingReward>();


                default:
                    $"{objType} parsed to a default building.".Log(LoggingChannel.Buildings);
                    return NanoRealityGameContext.InjectionBinder.GetInstance<IMapBuilding>();
            }
        }
    }

    public class MapObjectsDataConverter:JsonCreationConverter<IMapObjectsData>
    {
        protected override IMapObjectsData Create(Type objectType, JObject jObject)
        {
            if (NanoRealityGameContext.InjectionBinder != null)
                return NanoRealityGameContext.InjectionBinder.GetInstance<IMapObjectsData>();
            else
            {
                return new MapObjectsData();
            }
        }
    }

    public class ProductsDataContainerConverter : JsonCreationConverter<IProductsData>
    {
        protected override IProductsData Create(Type objectType, JObject jObject)
        {
            if (NanoRealityGameContext.InjectionBinder != null)
                return NanoRealityGameContext.InjectionBinder.GetInstance<IProductsData>();
            else
            {
                return new ProductsData();
            }
        }
    }

    /// <summary>
    /// Converter class for ILevel interface type serializing
    /// </summary>
    public class BusinessSectorsDataConverter : JsonCreationConverter<IBusinessSector>
    {
        /// <summary>
        /// Returns the interface-object for Converter
        /// </summary>
        /// <param name="objectType"></param>
        /// <param name="jObject"></param>
        /// <returns></returns>
        protected override IBusinessSector Create(Type objectType, JObject jObject)
        {
            if (NanoRealityGameContext.InjectionBinder != null)
                return NanoRealityGameContext.InjectionBinder.GetInstance<IBusinessSector>();
            else
            {
                return new BusinessSector();
            }
        }
    }

    public class CityMapConverter : JsonCreationConverter<ICityMap>
    {
        protected override ICityMap Create(Type objectType, JObject jObject)
        {
            if (NanoRealityGameContext.InjectionBinder != null)
                return NanoRealityGameContext.InjectionBinder.GetInstance<ICityMap>();
            else
            {
                return new MCityMap();
            }
        }
    }

    public class GlobalCityMapConverter : JsonCreationConverter<IGlobalCityMap>
    {
        protected override IGlobalCityMap Create(Type objectType, JObject jObject)
        {
            if (NanoRealityGameContext.InjectionBinder != null)
            {
                return NanoRealityGameContext.InjectionBinder.GetInstance<IGlobalCityMap>();
            }
            return new MGlobalCityMap();
        }
    }



    /// <summary>
    /// Converter class for ILevel interface type serializing
    /// </summary>
    public class CitiesDataConverter : JsonCreationConverter<IUserCity>
    {
        /// <summary>
        /// Returns the interface-object for Converter
        /// </summary>
        /// <param name="objectType"></param>
        /// <param name="jObject"></param>
        /// <returns></returns>
        protected override IUserCity Create(Type objectType, JObject jObject)
        {
            if (NanoRealityGameContext.InjectionBinder != null)
                return NanoRealityGameContext.InjectionBinder.GetInstance<IUserCity>(); //new MUserCity();
            else
            {
                return new MUserCity();
            }
        }
    }

    public class ProducingGoodConverter : JsonCreationConverter<IProducingItemData>
    {
        protected override IProducingItemData Create(Type objectType, JObject jObject)
        {
            if (NanoRealityGameContext.InjectionBinder != null)
                return NanoRealityGameContext.InjectionBinder.GetInstance<IProducingItemData>();
            else
            {
                return new ProducingItemData();
            }
        }
    }
  
    public class ProduceSlotConverter : StrangeIoCConvrter<IProduceSlot>
    {
         
    }

    public class ProductsForRepairItemConverter : StrangeIoCConvrter<IProductsToUnlockItem>
    {   
    }

    public class ProductForUpgradeConverter : StrangeIoCConvrter<IProductForUpgrade>
    {
    }

    
    public class ProductsForRepairConverter : StrangeIoCConvrter<IProductsToUnlock>
    {   
    }

    public class ConditionConverter : JsonCreationConverter<ICondition>
    {
        protected override ICondition Create(Type objectType, JObject jObject)
        {
            var type = jObject["condition_type"];

            if (type == null)
                throw new Exception("Condition type is absent in " + jObject);

            var objType = (ConditionTypes) type.Value<int>();

            switch (objType)
            {
                case ConditionTypes.QuestAchived:
                {
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IQuestsCompletedCondition>();
                    return new MQuestsCompletedCondition();
                }
                case ConditionTypes.UserLevelAchived:
                {
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IUserLevelAchievedCondition>();
                    return new UserLevelAchivedCondition();
                }
                case ConditionTypes.HaveBuilding:
                {
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IBuildingBuildCondition>();
                    return new HaveBuildingCondition();
                }
                case ConditionTypes.ConstructBuilding:
                {
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IBuildBuildingsCondition>();
                    return new ConstructBuildingCondition();
                }

                case ConditionTypes.ProductProduced:
                {
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IProductsProducedCondition>();
                    return new ProductsProducedCondition();
                }

                case ConditionTypes.BuildingUnlocked:
                {
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IBuildingUnlockedCondition>();
                    return new MBuildingUnlockedCondition();
                }

                case ConditionTypes.CoinsGained:
                {
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IGoldGainedCondition>();
                    return new MGoldGainedCondition();
                }

                case ConditionTypes.PremiumCoinsGained:
                {
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IPremiumGoldGainedCondition>();
                    return new MPremiumGoldGainedCondition();
                }

                case ConditionTypes.QuestsDoneCount:
                {
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IQuestsCompletedCondition>();
                    return new MQuestsCompletedCondition();
                }

                case ConditionTypes.ReachPopulation:
                {
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IPopulationAchivedCondition>();
                    return new PopulationAchivedCondition();
                }

                case ConditionTypes.SatisfactionLevel:
                {
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IPopulationSatisfactionCondition>();
                    return new MPopulationSatisfactionCondition();
                }

                case ConditionTypes.CertainBuildingTypeBuilded:
                {
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IBuildBuildingByTypeCondition>();
                    return new BuildBuildingByTypeCondition();
                }

                case ConditionTypes.CollectSoftCoinsFromCommercialBuildings:
                {
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return
                            NanoRealityGameContext.InjectionBinder
                                .GetInstance<ICollectCoinsFormCommercialBuildingsCondition>();
                    return new MCollectCoinsFromCommercialBuildingsCondition();
                }

                case ConditionTypes.ResearchingsAmount:
                {
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IResearchCondition>();
                    return new ResearchCondition();
                }

                case ConditionTypes.AuctionSales:
                {
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IAuctionSalesCondition>();
                    return new AuctionSalesCondition();
                }

                case ConditionTypes.AuctionPurchases:
                {
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IAuctionPurchaseCondition>();
                    return new AuctionPurchaseCondition();
                }

                case ConditionTypes.SpendCoinsAuction:
                {
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IAuctionSpendCoinsCondition>();
                    return new AuctionSpendCoinsCondition();
                }

                case ConditionTypes.GetCoinsFromAuction:
                {
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IAuctionGetCoinsCondition>();
                    return new AuctionGetCoinsCondition();
                }

                case ConditionTypes.InviteFriends:
                {
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IInviteFriendsCondition>();
                    return new InviteFriendsCondition();
                }

                case ConditionTypes.AmountFriends:
                {
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IAmountFriendsCondition>();
                    return new AmountFriendsCondition();
                }

                case ConditionTypes.ProductsUniqueAmountProduced:
                {
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IUniqueProductsAmountProducedCondition>();
                    return new MUniqueProductsAmountProducedCondition();
                }

                case ConditionTypes.UnlockSectorsForConstructions:
                {
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IUnlockSectorForConstruction>();
                    return new UnlockSectorForConstruction();
                }

                case ConditionTypes.ProductsTotalProduced:
                {
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IProductsTotalProducedCondition>();
                    return new MProductsTotalProducedCondition();
                }

                case ConditionTypes.CityRenamed:
                {
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<ICityRenamedCondition>();
                    return new CityRenamedCondition();
                }

                case ConditionTypes.RepairBuilding:
                {
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IRepairBuildingCondition>();
                    return new RepairBuildingCondition();
                }

                case ConditionTypes.CollectTaxes:
                {
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<ICollectTaxesCondition>();
                    return new CollectTaxesCondition();
                }

                case ConditionTypes.OrderDeskOrdersWithSectors:
                {
                    if(NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<ICompleteOrderDeskCondition>();
                    return new CompleteOrderDeskCondition();
                }

                case ConditionTypes.CertainQuestDone:
                {
                    if(NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<ICertainQuestDoneCondition>();
                    return new CertainQuestDoneCondition();
                }
                    
                case ConditionTypes.LoginSocialNetwork:
                    if(NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<ILoginSocialNetworkCondition>();
                    return new LoginSocialNetworkCondition();

                case ConditionTypes.UpgradePowerStation:
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IUpgradePowerStationCondition>();
                    return new UpgradePowerStationCondition();

                case ConditionTypes.ConnectBuildingToRoad:
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IConnectBuildingToRoadCondition>();
                    return new ConnectBuildingToRoadCondition();
                    
                case ConditionTypes.DateTime:
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IDateTimeCondition>();
                    return new DateTimeCondition();
                
                case ConditionTypes.CoverAmountBuildingsByService:
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<ICoverAmountBuildingsByServiceCondition>();
                    return new CoverAmountBuildingsByServiceCondition();
                
                case ConditionTypes.ShuffleOrderTable:
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IShuffleOrderTableCondition>();
                    return new ShuffleOrderTableCondition();
                
                case ConditionTypes.BazaarPurchaseLootBox:
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IBazaarPurchaseLootBoxCondition>();
                    return new BazaarPurchaseLootBoxCondition();
                
                case ConditionTypes.BazaarPurchaseProduct:
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IBazaarPurchaseProductCondition>();
                    return new BazaarPurchaseProductCondition();
                
                case ConditionTypes.BazaarSpendAmountGoldBars:
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IBazaarSpendGoldBarCondition>();
                    return new BazaarSpendGoldBarCondition();
                
                case ConditionTypes.FoodSupplyFillBar:
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IFoodSupplyFillBarCondition>();
                    return new FoodSupplyFillBarCondition();
                
                case ConditionTypes.FoodSupplyCookAmountOfMeals:
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IFoodSupplyCookMealsCondition>();
                    return new FoodSupplyCookMealsCondition();
                
                case ConditionTypes.FoodSupplyFillBarToFull:
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IFoodSupplyFillingBarCondition>();
                    return new FoodSupplyFillingBarCondition();
                
                case ConditionTypes.ReceiveItemFromOrderTable:
                    if (NanoRealityGameContext.InjectionBinder != null)
                        return NanoRealityGameContext.InjectionBinder.GetInstance<IReceiveProductFromOrderCondition>();
                    return new ReceiveProductFromOrderCondition();
                
                case ConditionTypes.BuildingConstructById:
                    return NanoRealityGameContext.InjectionBinder.GetInstance<IBuildingConstructByIdCondition>();
                
                case ConditionTypes.BuildingHaveById:
                    return NanoRealityGameContext.InjectionBinder.GetInstance<IBuildingHaveByIdCondition>();
                
                case ConditionTypes.BuildingConstructByType:
                    return NanoRealityGameContext.InjectionBinder.GetInstance<IBuildingConstructByTypeCondition>();
                
                case ConditionTypes.BuildingHaveByType:
                    return NanoRealityGameContext.InjectionBinder.GetInstance<IBuildingHaveByTypeCondition>();  
                
                case ConditionTypes.BuildingConstructBySubCategory:
                    return NanoRealityGameContext.InjectionBinder.GetInstance<IConstructBuildingWithSubCategoryCondition>();
                
                case ConditionTypes.BuildingHaveBySubCategory:
                    return NanoRealityGameContext.InjectionBinder.GetInstance<IBuildingHaveWithSubCategoryCondition>();              
                
                case ConditionTypes.SpendSpecificCurrency:
                    return NanoRealityGameContext.InjectionBinder.GetInstance<ISpendCurrencyCondition>();    
                
                case ConditionTypes.ObtainSpecificCurrency:
                    return NanoRealityGameContext.InjectionBinder.GetInstance<IObtainSpecificCurrencyCondition>();
            }

            Debug.LogWarning("Неизвестный кондишн! Возможно в SerializationConverters::ConditionConverter не хватает кейса для типа кондишна " + objType);
            
            if (NanoRealityGameContext.InjectionBinder != null)
                return NanoRealityGameContext.InjectionBinder.GetInstance<IUnknownCondition>();
            return new UnknownCondition();
        }
    }

    public class AwardActionsConverter : JsonCreationConverter<IAwardAction>
    {
        protected override IAwardAction Create(Type objectType, JObject jObject)
        {
            JToken type = jObject["action_type"];
            var objType = (AwardActionTypes) type.Value<int>();
            
            switch (objType)
            {
                case AwardActionTypes.LevelUp:
                {
                    return NanoRealityGameContext.InjectionBinder.GetInstance<ILevelUpAward>();
                }
                case AwardActionTypes.SoftCurrency:
                {
                    return NanoRealityGameContext.InjectionBinder.GetInstance<ISoftCurrencyAward>();
                }
                case AwardActionTypes.PremiumCurrency:
                {
                    return NanoRealityGameContext.InjectionBinder.GetInstance<IPremiumCurrencyAwardAction>();
                }
                case AwardActionTypes.Experience:
                {
                    return NanoRealityGameContext.InjectionBinder.GetInstance<IExperienceAward>();
                }
                case AwardActionTypes.Resources:
                {
                    return NanoRealityGameContext.InjectionBinder.GetInstance<IGiveProductAward>();
                }
                case AwardActionTypes.GiveRandomProduct:
                {
                    return NanoRealityGameContext.InjectionBinder.GetInstance<IGiveRandomProductAward>();
                }
                case AwardActionTypes.NanoGems:
                {
                    return NanoRealityGameContext.InjectionBinder.GetInstance<INanoGemsAward>();
                }
                case AwardActionTypes.Crystal:
                {
                    return null;
                }
                case AwardActionTypes.GoldBars:
                {
                    return NanoRealityGameContext.InjectionBinder.GetInstance<IGoldBarAwardAction>();
                }
                default:
                {
                    Logging.Log($"Not valid AwardActionType [{objectType}]");
                    return null;
                }
            }
        }
    }

    public class QuestDataConverter : StrangeIoCConvrter<IQuestBalanceData>
    {
        
    }

    public class DailyBonusBalanceDataConverter : StrangeIoCConvrter<IDailyBonusBalanceData>
    {

    }

    public class QuestConverter : StrangeIoCConvrter<IQuest>
    {
        
    }

    public class UserQuestDataConverter : StrangeIoCConvrter<IUserQuestData>
    {
        
    }

    public class SoftCurrencyBundlesDataConverter : StrangeIoCConvrter<ISoftCurrencyBundlesModel>
    {
  
    }

    #region Achievements

    public class AchievementsDataConverter : StrangeIoCConvrter<IAchievementsData>
    {
    }

    public class UserAchievementConverter : StrangeIoCConvrter<IUserAchievement>
    {
    }

    public class UserAchievementsDataConverter : StrangeIoCConvrter<IUserAchievementsData>
    {
    }

    public class SkipIntervalsDataConverter : StrangeIoCConvrter<ISkipIntervalsData>
    {
    }

    #endregion

    public class BoolConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(((bool)value) ? 1 : 0);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            bool boolean;
            if(bool.TryParse(reader.Value.ToString(), out  boolean))
                return boolean;
            return reader.Value.ToString() == "1";
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(bool);
        }
    }
    
    public class PriceTypeConverter : JsonConverter
    {
        private readonly Type _priceType = typeof(PriceType);

        public override bool CanConvert(Type objectType)
        {
            return objectType == _priceType;
        }
        
        public override object ReadJson(JsonReader reader,  Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType != JsonToken.Integer)
            {
                throw new NotImplementedException ("TokenType != Integer");
            }
            
            var intValue = Convert.ToInt32((long)reader.Value);
            return PriceUtility.GetPriceType(intValue);
        }
        
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException ("WriteJson");
            var flags = ((int) value).ToString();
            writer.WriteRawValue(flags);
        }
    }
    
    public class GameConfigDataContainerConverter : StrangeIoCConvrter<IGameConfigData> {}

    public class CityAreaDataContainerConverter : StrangeIoCConvrter<ICityAreaData> {}
    
    public class SectorAreaContainerConverter : StrangeIoCConvrter<ISectorArea> {}

    #endregion
}
