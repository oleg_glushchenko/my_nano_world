﻿using System;
using System.Globalization;
using NanoReality.GameLogic.Bank;
using Newtonsoft.Json;
using UnityEngine.Purchasing;

namespace NanoReality.GameLogic.IAP
{
	[Serializable]
	public sealed class InAppShopProduct : IBankProduct, IStoreProduct
	{
		[JsonProperty("id")] 
		public int Id { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("product_id")]
		public string StoreId { get; set; }
		
		[JsonProperty("store_name")]
		public string StoreName { get; set; }

		[JsonProperty("price")]
		public float StorePrice { get; set; }

		[JsonProperty("rewards")]
		public PurchaseReward Reward { get; set; }
		
		[JsonProperty("discount")] 
		public int DiscountField { get; set; }
		
		[JsonProperty("old_price")]
		public string OldPrice { get; set; }

		public string Price => StorePrice.ToString(CultureInfo.InvariantCulture);
		public string Amount => Reward.reward.ToString();
		public string BonusAmount => Reward.bonusReward > 0 ? Reward.bonusReward.ToString() : string.Empty;
		public string Discount => DiscountField.ToString();
		public ProductType ProductType => ProductType.Consumable;

		[Serializable]
		public class PurchaseReward
		{
			[JsonProperty("reward")]
			public int reward;
			
			[JsonProperty("bonus_reward")]
			public int bonusReward;
		}
	}
}