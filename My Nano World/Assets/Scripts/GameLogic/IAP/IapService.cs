﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.NanoLib.ServerCommunication.Models;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UtilitesEngine;
using Assets.NanoLib.Utilities.DataManager.Models.api;
using Assets.Scripts.Engine.Analytics.Model.api;
using NanoLib.Core.Logging;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.Constants;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Impl;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using UnityEngine;
using UnityEngine.Purchasing;

namespace NanoReality.GameLogic.IAP
{
    public class IapService : IIapService, IStoreListener
    {
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public ILocalDataManager jLocalDataManager { get; set; }
        [Inject] public IServerRequest jServerRequest { get; set; }
        [Inject] public ITimer jTimer { get; set; }
        [Inject] public PurchaseFinishedSignal jPurchaseFinishedSignal { get; set; }
        [Inject] public UserLevelUpSignal jUserLevelUpSignal { get; set; }
        [Inject] public SignalOffersUpdated jSignalOffersUpdated { get; set; }
        [Inject] public SignalLoadingSpinner jSignalLoadingSpinner { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public INanoAnalytics jNanoAnalytics { get; set; }

        public InAppShopData InAppShopProducts { get; private set; }
        public InAppShopOffersData InAppShopOffers { get; private set; }
        public bool OffersUpdating { get; private set; }

        private List<IStoreProduct> _allProducts;
        private IStoreController _storeController;
        private PendingPurchase _pendingPurchase;
        private ConfigurationBuilder _builder;

        #region IGameService implementation

        public void Init()
        {
            jUserLevelUpSignal.AddListener(UpdateOffers);
            
            jServerCommunicator.LoadInAppShopData(inAppData =>
            {
                InAppShopProducts = inAppData;

                if (InAppShopOffers == null)
                {
                    jServerCommunicator.LoadInAppShopOfferData(offersData =>
                    {
                        InAppShopOffers = offersData;
                        InitializeProducts();
                    });
                }
                else
                {
                        InitializeProducts();
                }
            });
        }

        public void UpdateOffers()
        {
            OffersUpdating = true;
            jServerCommunicator.LoadInAppShopOfferData(inAppData =>
            {
                InAppShopOffers = inAppData;
                jSignalOffersUpdated.Dispatch();
                InitializeProducts();
                OffersUpdating = false;
            });
        }

        public void InitOffersData(Action onComplete)
        {
            jServerCommunicator.LoadInAppShopOfferData(inAppData =>
            {
                InAppShopOffers = inAppData;
                onComplete?.Invoke();
            });
        }

        public virtual void Dispose()
        {
            jUserLevelUpSignal.RemoveListener(UpdateOffers);
            
            InAppShopProducts = null;
            InAppShopOffers = null;
            _storeController = null;
            _pendingPurchase = null;
        }

        #endregion

        private void InitializeProducts()
        {
             _builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
            _allProducts = InAppShopOffers.Products.Concat<IStoreProduct>(InAppShopProducts.Products).ToList();
            foreach (var product in _allProducts)
            {
                _builder.AddProduct(product.StoreId, product.ProductType);
                Logging.Log(LoggingChannel.Purchases, $"Register product {product.StoreId} as Consumable");
            }

            UnityPurchasing.Initialize(this, _builder);
        }

        public void BuyProduct(string productId)
        {
            jSignalLoadingSpinner.Dispatch(true);

            if (_pendingPurchase == null)
            {
                var product = _storeController.products.WithID(productId);
                if (product != null)
                {
                    if (product.availableToPurchase)
                    {
                        LogProductData(product);
                        _storeController.InitiatePurchase(product);
                    }
                    else
                    {
                        jSignalLoadingSpinner.Dispatch(false);
                        Logging.LogError(LoggingChannel.Purchases, $"Product {productId} not available!");
                    }
                }
                else
                {
                    jSignalLoadingSpinner.Dispatch(false);
                    Logging.LogError(LoggingChannel.Purchases, $"Product {productId} not found!");
                }
            }
            else
            {
                jSignalLoadingSpinner.Dispatch(false);
                Logging.LogError(LoggingChannel.Purchases,
                    $"Product {_pendingPurchase.id} is pending. Need to finish previous purchase first. Purchase token: {_pendingPurchase.token}");
            }
        }

        public void AddProduct(IStoreProduct product)
        {
            _allProducts.Add(product);
            _builder.AddProduct(product.StoreId, product.ProductType);
            UnityPurchasing.Initialize(this, _builder);
        }

        private void OnPurchaseFinished(Product product, bool isSuccess)
        {
            IStoreProduct shopProduct = null;
            if (product != null)
            {
                _storeController.ConfirmPendingPurchase(product);
                shopProduct = _allProducts.FirstOrDefault(productData => productData.StoreId == product.definition.id);
            }
            
            Logging.Log(LoggingChannel.Purchases, $"Product purchasing finished with result: <color={(isSuccess ? "green" : "red")}>{isSuccess}</color>");
            
            jLocalDataManager.Remove(_pendingPurchase);
            jLocalDataManager.SaveData();
            _pendingPurchase = null;
            jSignalLoadingSpinner.Dispatch(false);
            jPurchaseFinishedSignal.Dispatch(shopProduct, isSuccess);
        }

        private void OnServerResponseReceived(int intParam, bool isSuccess)
        {
            if (isSuccess)
            {
                var productId = intParam;
                var shopProduct = _allProducts.FirstOrDefault(productData => productData.Id == productId);
                if (shopProduct == null)
                {
                    Logging.LogError(LoggingChannel.Purchases, $"Shop product with Id {productId} not found!");
                    OnPurchaseFinished(null, false);
                    return;
                }

                var product = _pendingPurchase.product;
                OnPurchaseFinished(product, true);
            }
            else
            {
                Logging.LogError(LoggingChannel.Purchases, $"Purchase with id {intParam} is unsuccessful!");
                OnPurchaseFinished(null, false);
            }
        }

        private bool SendPurchasingConfirmRequest(Product product)
        {
            if (product ==null)
            {
                OnServerResponseReceived(-1, false);
                return false;
            }

            var shopData = _allProducts.FirstOrDefault(data => data.StoreId == product.definition.id);
            if (shopData != null)
            {
                var (orderId, token) = _pendingPurchase != null ? (_pendingPurchase.orderId, _pendingPurchase.token) :
#if UNITY_EDITOR || UNITY_STANDALONE
                    GetDebugReceiptData(product);
#elif UNITY_IOS
                    GetAppleStoreReceiptData(product);
#else
                    GetGooglePlayReceiptData(product);
#endif
                if (_pendingPurchase == null)
                {
                    _pendingPurchase = new PendingPurchase { id = product.definition.id, orderId = orderId, token = token, product = product };
                    jLocalDataManager.AddData(_pendingPurchase);
                }

                if (shopData is StoreProductEvent)
                {
                    jServerCommunicator.BuyEventBuilding(shopData.Id, token, orderId, OnServerResponseReceived);
                }
                else
                { 
                    jServerCommunicator.BuyInAppProduct(shopData.Id, orderId, token, OnServerResponseReceived);
                }

                return true;
            }

            return false;
        }

        #region IStoreListener imlementation

        public void OnInitialized(IStoreController storeController, IExtensionProvider extensionProvider)
        {
            _storeController = storeController;
            _pendingPurchase = jLocalDataManager.GetData<PendingPurchase>();
            if (_pendingPurchase != null)
            {
                var product = _storeController.products.WithID(_pendingPurchase.id);
                _pendingPurchase.product = product;

                jPurchaseFinishedSignal.AddListener(RestorePurchase);
                SendPurchasingConfirmRequest(product);
            }

            Logging.Log(LoggingChannel.Purchases, $"Unity IAP successfully initialized!");
        }

        private void RestorePurchase(IStoreProduct product, bool isSuccess)
        {
            if (isSuccess)
            {
                if (product is InAppShopProduct shopProduct)
                {
                    var amount = shopProduct.Reward.reward + shopProduct.Reward.bonusReward;
                    jPlayerProfile.PlayerResources.AddHardCurrency(amount);
                    jNanoAnalytics.OnInAppPurchase(shopProduct.StoreId, shopProduct.StorePrice);
                    jNanoAnalytics.OnPlayerCurrenciesIncrease(PlayerResources.CurencyType.Bucks, amount,
                        AwardSourcePlace.Purchase, shopProduct.StoreId);
                }
                else if (product is InAppShopOffer offer)
                {
                    jNanoAnalytics.OnInAppPurchase(offer.StoreId, offer.StorePrice);

                    for (int i = 0; i < offer.Reward.Length; i++)
                    {
                        switch (offer.Reward[i].Type)
                        {
                            case RewardType.Soft:
                                jPlayerProfile.PlayerResources.AddSoftCurrency(offer.Reward[i].Amount);
                                jNanoAnalytics.OnPlayerCurrenciesIncrease(PlayerResources.CurencyType.Coins,
                                    offer.Reward[i].Amount, AwardSourcePlace.Purchase, offer.StoreId);
                                break;

                            case RewardType.Hard:
                                jPlayerProfile.PlayerResources.AddHardCurrency(offer.Reward[i].Amount);
                                jNanoAnalytics.OnPlayerCurrenciesIncrease(PlayerResources.CurencyType.Bucks,
                                    offer.Reward[i].Amount, AwardSourcePlace.Purchase, offer.StoreId);
                                break;

                            case RewardType.Products:
                                jPlayerProfile.PlayerResources.AddProduct(offer.Reward[i].Id, offer.Reward[i].Amount);
                                break;

                            case RewardType.Gold:
                                jPlayerProfile.PlayerResources.AddGoldCurrency(offer.Reward[i].Amount);
                                jNanoAnalytics.OnPlayerCurrenciesIncrease(PlayerResources.CurencyType.Gold,
                                    offer.Reward[i].Amount, AwardSourcePlace.Purchase, offer.StoreId);
                                break;

                            case RewardType.Lanterns:
                                jPlayerProfile.PlayerResources.AddLanternCurrency(offer.Reward[i].Amount);
                                jNanoAnalytics.OnPlayerCurrenciesIncrease(PlayerResources.CurencyType.Lanterns,
                                    offer.Reward[i].Amount, AwardSourcePlace.Purchase, offer.StoreId);
                                break;

                            case RewardType.NanoPass:
                                jPlayerProfile.IsPremiumUser = true;
                                jNanoAnalytics.OnPurchaseNanoPass(offer.StoreId);
                                break;
                        }
                    }
                }

                UpdateOffers();
            }

            jPurchaseFinishedSignal.RemoveListener(RestorePurchase);
        }

        public void OnInitializeFailed(InitializationFailureReason error)
        {
            Logging.LogError(LoggingChannel.Purchases, $"Unity IAP initialization failed: {error}");
        }

        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs purchaseArgs)
        {
            if (_pendingPurchase != null)
                return PurchaseProcessingResult.Pending;
            
            var product = purchaseArgs.purchasedProduct;
            if (SendPurchasingConfirmRequest(product))
                return PurchaseProcessingResult.Pending;

            OnPurchaseFinished(product, false);
            
            return PurchaseProcessingResult.Complete;
        }

        public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            if (_pendingPurchase == null) //Not null pending purchase means that the purchase is processing by the server
                OnPurchaseFinished(product, false);

            Logging.LogError(LoggingChannel.Purchases, $"Product purchasing failed: '{product.definition.storeSpecificId}', reason: {failureReason}");
        }

        #endregion

        private static (string orderId, string token) GetDebugReceiptData(Product product)
        {
            return ("Debug order", product.transactionID);
        }

        private static (string orderId, string token) GetAppleStoreReceiptData(Product product)
        {
            var receipt = product.receipt;
            var wrapper = (Dictionary<string, object>) MiniJson.JsonDecode(receipt);
            var payload = (string) wrapper["Payload"];
            
            Logging.Log(LoggingChannel.Purchases, $"Apple Store purchasing receipt: {payload}");
            
            return (string.Empty, payload);
        }

        private static (string orderId, string token) GetGooglePlayReceiptData(Product product)
        {
            var receipt = product.receipt;
            var wrapper = (Dictionary<string, object>) MiniJson.JsonDecode(receipt);
            var payload = (string) wrapper["Payload"];
            var details = (Dictionary<string, object>) MiniJson.JsonDecode(payload);
            var receiptJson = (string) details["json"];
            
            Logging.Log(LoggingChannel.Purchases, $"Google purchasing receipt json: {receiptJson}");
            
            var receiptData = JsonUtility.FromJson<ReceiptData>(receiptJson);
            return (receiptData.orderId, receiptData.purchaseToken);
        }

        private static void LogProductData(Product product)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine($"Purchasing product {product.definition.id}");
            stringBuilder.AppendLine($"Transaction Id: {product.transactionID}");
            stringBuilder.AppendLine($"Localized price: {product.metadata.localizedPriceString}");
            stringBuilder.Log(LoggingChannel.Purchases);
        }

        [Serializable]
        public class PendingPurchase
        {
            public string id;
            public string orderId;
            public string token;
            [NonSerialized] public Product product;
        }
    }
}