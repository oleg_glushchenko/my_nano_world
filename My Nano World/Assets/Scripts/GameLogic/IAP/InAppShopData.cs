﻿using System;
using Newtonsoft.Json;

namespace NanoReality.GameLogic.IAP
{
	[Serializable]
	public sealed class InAppShopData
	{
		[JsonProperty("offers")]
		public InAppShopProduct[] Products;
	}
}