using UnityEngine.Purchasing;

namespace NanoReality.GameLogic.IAP
{
    public interface IStoreProduct
    {
        string StoreId { get; }
        ProductType ProductType { get; }
        int Id { get; }
        string Price { get; }
    }
}