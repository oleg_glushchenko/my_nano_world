using System;
using System.Globalization;
using NanoReality.GameLogic.Bank;
using NanoReality.GameLogic.Constants;
using NanoReality.GameLogic.Data;
using Newtonsoft.Json;
using UnityEngine.Purchasing;

namespace NanoReality.GameLogic.IAP
{
    [Serializable]
    public sealed class InAppShopOffer : IBankProduct, IStoreProduct
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("price")]
        public float StorePrice { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("old_price")]
        public string OldPrice { get; set; }

        [JsonProperty("discount")]
        public string DiscountField { get; set; }

        [JsonProperty("product_id")]
        public string StoreId { get; set; }

        [JsonProperty("product_type")]
        public int Type { get; set; }

        [JsonProperty("store_name")]
        public string StoreName { get; set; }

        [JsonProperty("start_time")]
        public long StartTime { get; set; }

        [JsonProperty("finish_time")]
        public long FinishTime { get; set; }

        [JsonProperty("is_new")]
        public int IsNew { get; set; }

        [JsonProperty("priority")]
        public int Priority { get; set; } 
        
        [JsonProperty("icon_style")]
        public int IconId { get; set; }

        [JsonProperty("ui_type")]
        public OfferStyle UiStyle { get; set; }

        [JsonProperty("rewards")]
        public RewardData[] Reward { get; set; }

        public string Price => StorePrice.ToString(CultureInfo.InvariantCulture);

        public string Discount => DiscountField;

        public ProductType ProductType => (ProductType)Type;

        public string Amount => throw new NotImplementedException();

        public string BonusAmount => throw new NotImplementedException();

    }
}