﻿using UnityEngine;

namespace NanoReality.Game.Visuals
{
    public class EnvironmentMatsInitializer : MonoBehaviour
    {
        #region Inspector

        [SerializeField] private Texture _cloudsTexture;  
        [SerializeField] private Color _cloudsColor;

        [SerializeField] private float _cloudUvX;
        [SerializeField] private float _cloudUvY;
        [SerializeField] private float _twinkleIntensity;
        [SerializeField] private float _twinkleScale;
        [SerializeField] private float _twinkleSpeed;
        [SerializeField] private float _wiggleScale;
        [SerializeField] private float _wiggleIntensity;
        [SerializeField] private float _cloudsSpeed;

        [SerializeField] private float _worldX;
        [SerializeField] private float _worldY;
        [SerializeField] private float _worldZ;
         
        #endregion

        private void Awake()
        {
            Shader.SetGlobalTexture("_CloudsTex",_cloudsTexture);
            Shader.SetGlobalColor("_CloudsCol",_cloudsColor );
            Shader.SetGlobalFloat("_CloudUvX",_cloudUvX);
            Shader.SetGlobalFloat("_CloudUvY",_cloudUvY);
            Shader.SetGlobalFloat("_CloudsTwinkleIntensity",_twinkleIntensity);
            Shader.SetGlobalFloat("_CloudsTwinkleScale",_twinkleScale);
            Shader.SetGlobalFloat("_CloudsTwinkleSpeed",_twinkleSpeed);
            Shader.SetGlobalFloat("_CloudsWiggleScale",_wiggleScale);
            Shader.SetGlobalFloat("_CloudsWiggleIntensity",_wiggleIntensity);
            Shader.SetGlobalFloat("_CloudsSpeed",_cloudsSpeed);
            Shader.SetGlobalFloat("_TurnX",_worldX);
            Shader.SetGlobalFloat("_TurnY",_worldY);
            Shader.SetGlobalFloat("_TurnZ",_worldZ); 
        }
    }
}