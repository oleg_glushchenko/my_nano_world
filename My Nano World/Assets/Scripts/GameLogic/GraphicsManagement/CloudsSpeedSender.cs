﻿using DG.Tweening;
using UnityEngine;

namespace NanoReality.Game.Visuals
{
    public class CloudsSpeedSender : MonoBehaviour
    {
        [SerializeField] private float _tweenDuration = 90f;

        private Tweener _offsetSpeedTween;

        private void Awake()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.playModeStateChanged += ReactToEditorExit;
#endif
        }

        private void Start()
        {
            _offsetSpeedTween = DOVirtual.Float(0f, -1f, _tweenDuration, OnSpeedChanged).SetLoops(-1);
        }

        private void OnSpeedChanged(float speedValue)
        {
            SetSpeed(speedValue);
        }

        private void OnDestroy()
        {
            #if UNITY_EDITOR
            UnityEditor.EditorApplication.playModeStateChanged -= ReactToEditorExit;
            #endif
            _offsetSpeedTween?.Kill();
        }

        private static void SetSpeed(float speed)
        {
            Shader.SetGlobalFloat("_OffsetProgress", speed);
        }

        private void OnApplicationQuit()
        {
            SetSpeed(0f);
        }

        #if UNITY_EDITOR
        
        private static void ReactToEditorExit(UnityEditor.PlayModeStateChange state)
        {
            if (state == UnityEditor.PlayModeStateChange.ExitingPlayMode)
            {
                SetSpeed(0f);
            }
        }
        
        #endif
    }
}