﻿using GameLogic.Bazaar.Model.impl;
using strange.extensions.signal.impl;

namespace GameLogic.Bazaar.Controllers.Signals
{
    public class BazaarBuyProductSignal : Signal<SoukTopSlot> { }
    public class BazaarBuyLootBoxSignal : Signal<BazaarLootBox> { }
}
