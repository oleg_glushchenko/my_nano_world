﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.NanoLib.UI.Core.Signals;
using Assets.NanoLib.UtilitesEngine;
using Assets.Scripts.Engine.Analytics.Model.api;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using Engine.UI;
using GameLogic.Bazaar.Controllers.Signals;
using GameLogic.Bazaar.Model.impl;
using NanoReality.Game.Services.BuildingsUnlockService;
using NanoReality.Game.UI.BazaarPanel;
using NanoReality.Game.UI.BazaarPanel.ProductView;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityCamera.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.Configs;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Impl;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.UI.Components;
using UnityEngine;

namespace GameLogic.Bazaar.Service
{
    public class BazaarService : IBazaarService
    {
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }

        [Inject] public ITimerManager jTimerManager { get; set; }
        [Inject] public BazaarBuyProductSignal jBazaarBuyProductSignal { get; set; }
        [Inject] public BazaarBuyLootBoxSignal jBazaarBuyLootBoxSignal { get; set; }
        [Inject] public UserLevelUpSignal jUserLevelUpSignal { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public IGameConfigData jGameConfigData { get; set; }
        [Inject] public IPopupManager jPopupManager { get; set; }
        [Inject] public INanoAnalytics jNanoAnalytics { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public IBuildingsUnlockService jBuildingsUnlockService { get; set; }

        public SoukData SoukData { get; private set; }
        public List<SoukTopSlot> RareProducts { get; } = new List<SoukTopSlot>();
        public List<SoukTopSlot> CommonProducts { get; } = new List<SoukTopSlot>();
        public SoukConfig SoukConfig => jGameConfigData.SoukConfig;

        public BazaarLootBox[] LootBoxes => SoukData.LootBoxes;
        public SoukTopTradeline TopTradeline => SoukData.TopTradeItems;
        public SoukBottomOffer[] BottomTradeline => SoukData.BottomTradeItems;
        public long OffersEndTime => SoukData.ExpireTime;

        public event Action BazaarCreated;

        private int _soukUnlockLevel;

        public void Init()
        {
            foreach (var product in SoukConfig.BazaarProducts)
            {
                if (product.IsRare)
                {
                    RareProducts.Add(product);
                    continue;
                }
                CommonProducts.Add(product);
            }

            jUserLevelUpSignal.AddListener(OnUserLevelUp);
            var targets = jGameManager.FindAllMapObjectsOnMapByType(BusinessSectorsTypes.Global, MapObjectintTypes.TradingShop);
            if (targets.Count == 0)
            {
                return;
            }
            
            _soukUnlockLevel = jBuildingsUnlockService.GetBuildingUnlockLevel(targets.First().Id.Id);
            if (jPlayerProfile.CurrentLevelData.CurrentLevel >= _soukUnlockLevel)
            {
                FetchBazaar();
            }
        }

        private void OnUserLevelUp()
        {
            if (jPlayerProfile.CurrentLevelData.CurrentLevel >= _soukUnlockLevel)
            {
                FetchBazaar();
            }
        }

        public void Dispose()
        {
            RareProducts.Clear();
            CommonProducts.Clear();
        }
        
        public void FetchBazaar()
        {
            jServerCommunicator.LoadSouk(bazaarData =>
            {
                SetBazaarData(bazaarData);
                BazaarCreated?.Invoke();
            });
        }

        public void ShuffleSouk(Action resultCallback)
        {
            jPlayerProfile.PlayerResources.BuyHardCurrency(SoukData.ShufflePrice);
            jServerCommunicator.ShuffleSouk(bazaarData =>
            {
                SetBazaarData(bazaarData);
                resultCallback?.Invoke();
                BazaarCreated?.Invoke();
            });
        }

        /// <summary>
        /// temp crunch to fix sort products bugs because of using list index as slot id
        /// </summary>
        private bool _buyingProduct = false;

        public bool BuyTopSlotProduct(SoukTopSlot soukTopSlot, SoukTopTradeItemView productView, Action resultCallback)
        {
            if (_buyingProduct)
                return false;
            
            _buyingProduct = true;

            jPlayerProfile.PlayerResources.RemoveGoldCurrency(soukTopSlot.Price);
            jNanoAnalytics.OnPlayerCurrenciesDecrease(PlayerResources.CurencyType.Gold, soukTopSlot.Price,
                CurrenciesSpendingPlace.BuyBazaarProducts, soukTopSlot.ProductId.ToString());
            jPlayerProfile.PlayerResources.AddProduct(soukTopSlot.ProductId, soukTopSlot.Count);

            jServerCommunicator.BuyTopSoukSlot(soukTopSlot.SlotId, bazaarData =>
            {
                jNanoAnalytics.BuySoukSlot("TopSlot", soukTopSlot.SlotId, soukTopSlot.ProductId + " ");
                jBazaarBuyProductSignal.Dispatch(soukTopSlot);
                if (bazaarData.BottomTradeItems == null) return;
                SetBazaarData(bazaarData);
                resultCallback?.Invoke();
                _buyingProduct = false;
            });

            return true;
        }
        
        public bool UnlockTopSlot(int slotId, Action resultCallback)
        {
            if (_buyingProduct)
                return false;

            _buyingProduct = true;

            var unlockSlotData = SoukConfig.TopTradeline.First(c => c.SlotId == slotId);
            jPlayerProfile.PlayerResources.RemoveHardCurrency(unlockSlotData.UnlockSlotPrice);

            jServerCommunicator.UnlockTopSoukSlot(slotId, bazaarData =>
            {
                jNanoAnalytics.UnlockSlotEvent("Souk", slotId, "TopSlot");
                if (bazaarData.BottomTradeItems == null) return;
                SetBazaarData(bazaarData);
                resultCallback?.Invoke();
                _buyingProduct = false;
            });

            return true;
        }

        public bool BuyChests(BazaarLootBox lootBox, BazaarChestView productView, Action resultCallback)
        {
            if (_buyingProduct)
                return false;

            _buyingProduct = true;

            jPlayerProfile.PlayerResources.RemoveGoldCurrency(lootBox.BoxPrice);
            jNanoAnalytics.OnPlayerCurrenciesDecrease(PlayerResources.CurencyType.Gold, lootBox.BoxPrice, CurrenciesSpendingPlace.BuyChests,
                lootBox.Id.ToString());

            List<IAwardAction> rewardActions = lootBox.BoxReward.ToAwardActions();
            jPopupManager.Show(new RewardBoxPopupSettings(rewardActions, lootBox.Id));
            
            StringBuilder rewards =new StringBuilder();

            foreach (var awardProduct in lootBox.BoxReward.Products)
            {
                rewards.Append(awardProduct.ProductId + " ");
                jPlayerProfile.PlayerResources.AddProduct(awardProduct.ProductId, awardProduct.Count);
            }

            jPlayerProfile.PlayerResources.AddSoftCurrency(lootBox.BoxReward.NanoCoins);
            jNanoAnalytics.OnPlayerCurrenciesIncrease(PlayerResources.CurencyType.Coins, lootBox.BoxReward.NanoCoins,
                AwardSourcePlace.ChestsReward, lootBox.Id.ToString());
                
            jPlayerProfile.PlayerResources.AddHardCurrency(lootBox.BoxReward.NanoBucks);
            jNanoAnalytics.OnPlayerCurrenciesIncrease(PlayerResources.CurencyType.Bucks, lootBox.BoxReward.NanoBucks,
                AwardSourcePlace.TradeMarketLot, lootBox.Id.ToString());
                
            jBazaarBuyLootBoxSignal.Dispatch(lootBox);
            
            jServerCommunicator.BuySoukLootbox(lootBox.Id, bazaarData =>
            {
                jNanoAnalytics.BuySoukSlot("Lootbox", lootBox.Id, rewards.ToString());
                SetBazaarData(bazaarData);
                resultCallback?.Invoke();
                _buyingProduct = false;
            });

            return true;
        }

        public void UnlockBottomSlot(int slotId, Action callback)
        {
            var unlockSlotConfig = SoukConfig.BottomTradeline.First(c => c.SlotId == slotId);
            jPlayerProfile.PlayerResources.BuyHardCurrency(unlockSlotConfig.UnlockSlotPrice);
            
            jServerCommunicator.UnlockBottomSoukSlot(slotId, soukData =>
            {
                jNanoAnalytics.UnlockSlotEvent("Souk", slotId, "BottomSlot");
                SetBazaarData(soukData);
                BazaarCreated?.Invoke();
                callback?.Invoke();
            });
        }

        public void BuyBottomProduct(int slotId, Action callback)
        {
            var targetSlot = SoukData.BottomTradeItems.First(c => c.Id == slotId);
            targetSlot.SoldOut = true;

            var slotPrice = targetSlot.GetPrice();
            jPlayerProfile.PlayerResources.BuyItem(slotPrice);
            jServerCommunicator.BuyBottomSoukSlot(slotId, soukData =>
            {
                StringBuilder rewards =new StringBuilder();
                
                foreach (KeyValuePair<int,int> targetSlotProduct in targetSlot.Products)
                {
                    jPlayerProfile.PlayerResources.AddProduct(targetSlotProduct.Key, targetSlotProduct.Value);
                    rewards.Append(targetSlotProduct.Key + " ");
                }
                
                jNanoAnalytics.BuySoukSlot("BottomSlot", slotId, rewards.ToString());
                SetBazaarData(soukData);
                BazaarCreated?.Invoke();
                callback?.Invoke();
            });
        }

        private void SetBazaarData(SoukData bazaarData)
        {
            SoukData = bazaarData;
            SoukData.BottomTradeItems = bazaarData.BottomTradeItems;
            SoukData.LootBoxes = bazaarData.LootBoxes;
        }
    }
}
