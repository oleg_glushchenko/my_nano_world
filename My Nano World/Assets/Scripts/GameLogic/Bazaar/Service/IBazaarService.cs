﻿using System;
using System.Collections.Generic;
using GameLogic.Bazaar.Model.impl;
using NanoReality.Game.Services;
using NanoReality.Game.UI.BazaarPanel;
using NanoReality.Game.UI.BazaarPanel.ProductView;
using NanoReality.GameLogic.Configs;

namespace GameLogic.Bazaar.Service
{
    public interface IBazaarService : IGameService
    {
        event Action BazaarCreated;
        List<SoukTopSlot> RareProducts { get; }
        List<SoukTopSlot> CommonProducts { get; }
        SoukConfig SoukConfig { get; }
        
        BazaarLootBox[] LootBoxes { get; }
        SoukTopTradeline TopTradeline { get; }
        SoukBottomOffer[] BottomTradeline { get; }
        long OffersEndTime { get; }
        
        SoukData SoukData { get; }
        
        void FetchBazaar();
        
        void ShuffleSouk(Action resultCallback);
        bool BuyTopSlotProduct(SoukTopSlot soukTopSlot, SoukTopTradeItemView productView, Action resultCallback);
        bool UnlockTopSlot(int slotId, Action resultCallback);
        bool BuyChests(BazaarLootBox bazaarLootBox, BazaarChestView productView, Action resultCallback);

        void UnlockBottomSlot(int slotId, Action callback);

        void BuyBottomProduct(int slotId, Action callback);
    }
}
