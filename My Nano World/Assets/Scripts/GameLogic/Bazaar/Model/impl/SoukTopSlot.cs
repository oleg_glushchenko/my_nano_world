﻿using Newtonsoft.Json;

namespace GameLogic.Bazaar.Model.impl
{
    public class SoukTopSlot
    {
        [JsonProperty("id")]
        public int SlotId{ get; set; }
        
        [JsonProperty("product_id")]
        public int ProductId { get; set; }
        
        [JsonProperty("product_count")]
        public int Count { get; set; }

        [JsonProperty("product_price")]
        public int Price { get; set; }

        [JsonProperty("is_rare")]
        public bool IsRare { get; set; }
        
        [JsonProperty("sold_out")]
        public bool SoldOut { get; set; }

    }
}
