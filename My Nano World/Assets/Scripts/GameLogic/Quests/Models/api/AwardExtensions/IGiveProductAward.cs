﻿using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;

namespace NanoReality.GameLogic.Quests
{
    /// <summary>
    /// Награда товаром
    /// </summary>
    public interface IGiveProductAward  : IAwardAction
    {
        /// <summary>
        /// Тип продукта для награды
        /// </summary>
        Id<Product> Product { get; set; }

        /// <summary>
        /// Кол-во продукта для нагады
        /// </summary>
        int Amount { get; set; }
    }
}