﻿namespace NanoReality.GameLogic.Quests
{
    public interface IPremiumCurrencyAwardAction : IAwardAction
    {
        int CurrencyCount { get; set; }
    }
}