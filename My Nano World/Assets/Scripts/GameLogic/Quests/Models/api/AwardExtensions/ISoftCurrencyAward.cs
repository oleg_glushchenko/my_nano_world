﻿namespace NanoReality.GameLogic.Quests
{
    /// <summary>
    /// Награда софт валюты
    /// </summary>
    public interface ISoftCurrencyAward : IAwardAction
    {
        /// <summary>
        /// Кол-во денег для софт валюты
        /// </summary>
        int SoftCurrencyCount { get; set; }
    }
}
