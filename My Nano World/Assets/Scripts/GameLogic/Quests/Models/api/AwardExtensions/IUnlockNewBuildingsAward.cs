﻿using NanoReality.GameLogic.Quests;

public interface IUnlockNewBuildingsAward : IAwardAction
{
    int BuildingID { get; set; }
}
