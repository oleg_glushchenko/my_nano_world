﻿namespace NanoReality.GameLogic.Quests
{
    /// <summary>
    /// Награда повышения уровня игрока
    /// </summary>
    public interface ILevelUpAward : IAwardAction
    {
        /// <summary>
        /// Сколько уровней получит игрок
        /// </summary>
        int LevelUpCount { get; set; }

    }
}
