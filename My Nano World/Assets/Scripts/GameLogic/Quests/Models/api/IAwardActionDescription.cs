﻿namespace NanoReality.GameLogic.Quests
{
    /// <summary>
    /// Объект "описание награды"
    /// </summary>
    public interface IAwardActionDescription 
    {
        /// <summary>
        /// Тип действия награды
        /// </summary>
        AwardActionTypes AwardActionType { get; set; }

        /// <summary>
        /// Его описание
        /// </summary>
        string Description { get; set; }
       
    }
}
