﻿using System;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using strange.extensions.signal.impl;

namespace NanoReality.GameLogic.Quests
{
    /// <summary>
    /// Условие, применяется в создании квестов и веток развития зданий
    /// </summary>
    public interface ICondition : ICloneable
    {
        /// <summary>
        /// Уникальный айди кондишена
        /// </summary>
        int ConditionId { get; set; }

        /// <summary>
        /// Тип условия
        /// </summary>
        ConditionTypes ConditionType { get; set; }

        /// <summary>
        /// Уровень здания
        /// </summary>
        int Level { get; set; }

        /// <summary>
        /// Тип здания
        /// </summary>
        MapObjectintTypes BuildingType { get; set; }
        
        BusinessSectorsTypes SectorsType { get; set; }

        /// <summary>
        /// Происходит когда условие меняет состояние
        /// </summary>
        Signal<ICondition> SignalOnAchieved { get; }

        /// <summary>
        /// Условие выполнено
        /// </summary>
        bool IsAchived { get; set; }
	    
	    /// <summary>
	    /// Returns localized description of the condition
	    /// </summary>
	    string Description { get; }

        /// <summary>
        /// Развертывает данные сериализированные с сервера, в переменные гейм. баланса кондишена
        /// </summary>
        void InitServerBalanceData();

        /// <summary>
        /// Развертывает данные сериализированные с сервера, в переменные текущего состояния кондишена
        /// </summary>
        void InitServerStatesData();

        /// <summary>
        /// Make variables of current state equal variables of condition balance and all conditions achieved
        /// </summary>
        void InitAchievedStateData();
        
        /// <summary>
        /// Подсчитывает прогресс кондишена проверяя локальные показатели (используется для достижений)
        /// </summary>
        void InitLocalStateData();

        /// <summary>
        /// Инициализирует данные баланса из прототипа
        /// </summary>
        /// <param name="condition"></param>
        void InitFromPrototype(ICondition condition);

        /// <summary>
        /// Входая точка вычисления условия
        /// </summary>
        void StartEvaluateCondition();

 

        /// <summary>
        /// Возвращает требование выполнения квеста
        /// </summary>
        Requirement GetConditionRequirement();
    }

    public class Requirement
    {
        public int Id;
        public int NeededCount;
        public int CurrentCount;

        public Requirement(int id, int neededCount, int currentCount)
        {
            Id = id;
            NeededCount = neededCount;
            CurrentCount = currentCount;
        }
    }



	/// <summary>
	/// Описывает все типы условий
	/// </summary>
	public enum ConditionTypes
	{
		/// <summary>
		/// Игрок выполнил квест №N
		/// </summary>
		QuestAchived = -1,

		/// <summary>
		/// Игрок должен быть N-го уровня
		/// </summary>
		UserLevelAchived = 1,

		/// <summary>
		/// У игрока построено одно здание уровня K
		/// </summary>
		HaveBuilding = 2,

		/// <summary>
		/// У игрока построено N зданий уровня K
		/// </summary>
		ConstructBuilding = 3,

		/// <summary>
		/// Игрок произвел N товара типа K
		/// </summary>
		ProductProduced = 4,

		/// <summary>
		/// Открыто здание
		/// </summary>
		BuildingUnlocked = 5,

		/// <summary>
		/// Количество софт валюты
		/// </summary>
		CoinsGained = 6,

		/// <summary>
		/// Количество хард валюты
		/// </summary>
		PremiumCoinsGained = 7,

		/// <summary>
		/// Количество выполненых квестов
		/// </summary>
		QuestsDoneCount = 8,

		/// <summary>
		/// Количество набранного населения
		/// </summary>
		ReachPopulation = 9,

		SatisfactionLevel = 10,

		/// <summary>
		/// Постройка конкретного типа здания в количестве N уровня L.
		/// </summary>
		CertainBuildingTypeBuilded = 11,

		GlobalUpgradeEquipmentToTheNextClass = 12,

		UpgradeEquipmentToTheNextClass = 13,

		GlobalUpgradeEquipmentToTheNextLevel = 14,

		UpgradeEquipmentToTheNextLevel = 15,

		EducateSchoolPeoples = 16,

		CollectSoftCoinsFromCommercialBuildings = 17,

		ResearchingsAmount = 18,

		CitiesOpened = 19,

		SectorsOpened = 20,

		ProductsTotalProduced = 21,

		CertainSectorOpened = 22,

		ProductsUniqueAmountProduced = 23,

		AuctionSales = 24,

		AuctionPurchases = 25,

		AmountFriends = 26,

		GetCoinsFromAuction = 27, 

		SpendCoinsAuction = 28,

		UnlockSectorsForConstructions = 29,

		InviteFriends = 30,

		OrderDeskOrders = 31,

		CityRenamed = 32,

        RepairBuilding = 33,

	    CollectTaxes = 34,

        OrderDeskOrdersWithSectors = 36,

		CertainQuestDone = 37,
		
		LoginSocialNetwork = 38,

        UpgradePowerStation = 39,

        WinBurgerGame = 40,
		
		CoverBuildingsWithAreaOfEffect = 41,
		
		UseCasinoFreeSpin = 42,
		
		ConnectBuildingToRoad = 43,
		
		DateTime = 44,
		
		ChangeAoeForCoverAmountBuildings = 45,
		
		ShuffleOrderTable = 46,
		
		CoverAmountBuildingsByService = 47,
		
		BazaarPurchaseLootBox = 48,
		
		BazaarPurchaseProduct = 49,
		
		BazaarSpendAmountGoldBars = 50,
		
		FoodSupplyCookAmountOfMeals = 51,
		
		FoodSupplyFillBar = 52,
		
		ReceiveItemFromOrderTable = 53,
		
		BuildingConstructById = 54,
		
		BuildingHaveById = 55,
		
		BuildingConstructByType = 56,
		
		BuildingHaveByType = 57,
		
		BuildingConstructBySubCategory = 58,
		
		BuildingHaveBySubCategory = 59,
		
		SpendSpecificCurrency = 60,
		
		ObtainSpecificCurrency = 61,
		
		FoodSupplyFillBarToFull = 62
    }
}
