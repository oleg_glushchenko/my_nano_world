﻿using System;
using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;

namespace NanoReality.GameLogic.Quests
{

    /// <summary>
    /// Объект контейнер хранящий список квестов игрока
    /// </summary>
    public interface IUserQuestData 
    {
        /// <summary>
        /// Список квестов игрока
        /// </summary>
        List<IQuest> UserQuestsData { get; set; }

        /// <summary>
        /// Входная точка просчета квестов у юзера
        /// </summary>
        void InitUserQuest();

        IQuest GetQuestById(Id<IQuest> id);

        int GetActiveQuestsCount(BusinessSectorsTypes sectorType);
        int GetAchievedQuestsCount(BusinessSectorsTypes sectorType);
        List<IQuest> GetAchievedQuests(BusinessSectorsTypes sectorType);
        int FirstAchievedQuestGiftBoxType(BusinessSectorsTypes sectorType);
        event Action OnQuestsInitialized;
    }
}