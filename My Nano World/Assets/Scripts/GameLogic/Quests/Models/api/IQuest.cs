﻿using System;
using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using NanoLib.Core;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using strange.extensions.signal.impl;
using UnityEngine;

namespace NanoReality.GameLogic.Quests
{
    /// <summary>
    /// Представляет собой описание квеста в игре
    /// </summary>
    public interface IQuest
    {
        #region GameBalanceData

        /// <summary>
        /// Айди квеста
        /// </summary>
        IntValue QuestId { get; }

        /// <summary>
        /// Бизнес сектор к которому относится квест
        /// </summary>
        BusinessSectorsTypes BusinessSector { get; }

        /// <summary>
        /// Должна ли отображаться реплика персонажа после выполнения квеста
        /// </summary>
        bool IsDialogShow { get; }

        /// <summary>
        /// Список условий для разблокировки квеста
        /// </summary>
        List<ICondition> UnlockConditions { get; }

        /// <summary>
        /// Список условий для выполнения квеста
        /// </summary>
        List<ICondition> AchiveConditions { get; }

        /// <summary>
        /// Список действий наград вызываемых после окончания квеста
        /// </summary>
        List<IAwardAction> AwardActions { get; }

        /// <summary>
        /// Type of a reward box that is displayed to player
        /// </summary>
        int GiftBoxType { get; }

        #endregion

        #region UserData

        /// <summary>
        /// Состояние данного квеста
        /// </summary>
        UserQuestStates QuestState { get; set; }

        /// <summary>
        /// Текущие состояния условий выполнения квеста
        /// </summary>
        List<ICondition> AchiveConditionsStates { get; }

        #endregion

        #region Signals

        /// <summary>
        /// Вызывается когда квест становится доступным для выполнения
        /// </summary>
        SignalOnQuestUnlock jSignalOnQuestUnlock { get; }

        /// <summary>
        /// Вызывается когда квест начинается
        /// </summary>
        SignalOnQuestStart jSignalOnQuestStart { get; }

        /// <summary>
        /// Вызывается когда квест выполнен
        /// </summary>
        SignalOnQuestAchive jSignalOnQuestAchive { get; }

        /// <summary>
        /// Вызывается когда собрана награда за квест
        /// </summary>
        SignalOnQuestComplete jSignalOnQuestComplete { get; }

        /// <summary>
        /// Called when quest reward was collected and verified by server
        /// </summary>
        SignalOnQuestCompletedOnServer jSignalOnQuestCompletedOnServer { get; }

        /// <summary>
        /// Called when quest was failed
        /// </summary>
        SignalOnQuestFailed jSignalOnQuestFailed { get; }

        #endregion

        #region Properties

        bool IsActive { get; }
        string Name { get; }


        #endregion

        #region Methods

        void InitServerData();

        void InitFromPrototype(IQuest prototype);

        void InitUserQuest();

        void CollectAward();

        #endregion
    }


    /// <summary>
    /// Состояния квестов игрока
    /// </summary>
    public enum UserQuestStates
    {
        /// <summary>
        /// Не доступен
        /// </summary>
        Unavailable = 1,

        /// <summary>
        /// Доступен но пока не выполняется
        /// </summary>
        Avaliable = 2,


        /// <summary>
        /// Доступен, в процессе выполнения
        /// </summary>
        InProgress = 3,

        /// <summary>
        /// Выполнен, но игрок еще не забрал награду
        /// </summary>
        Achived = 4,

        /// <summary>
        /// Квест выполнен, награда забрана
        /// </summary>
        Completed = 5,
	    
	    /// <summary>
	    /// Player failed to fulfill quest conditions on time
	    /// </summary>
	    Failed = 6,
    }

	/// <summary>
	/// Вызывается, когда квест был завершен на сервере
	/// </summary>
	public class SignalOnQuestCompletedOnServer : Signal<IQuest, List<IAwardAction>> {	}
}
