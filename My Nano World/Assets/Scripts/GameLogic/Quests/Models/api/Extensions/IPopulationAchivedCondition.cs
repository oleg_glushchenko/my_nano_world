﻿namespace NanoReality.GameLogic.Quests
{
    /// <summary>
    /// Условие общего кол-ва населения в городе
    /// </summary>
    public interface IPopulationAchivedCondition  : ICondition
    {
        /// <summary>
        /// Кол-во населения которое нужно достигнуть
        /// </summary>
        int PopulationCountToAchive { get; set; }
    }
}
