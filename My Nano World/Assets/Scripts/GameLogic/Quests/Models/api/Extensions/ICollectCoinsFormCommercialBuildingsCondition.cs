﻿using NanoReality.GameLogic.Quests;

public interface ICollectCoinsFormCommercialBuildingsCondition : ICondition
{
    /// <summary>
    /// Id здания которое должно быть построено
    /// </summary>
    Id_IMapObjectType BuildingTypeId { get; set; }

    int CoinsAmount { get; set; }
}
