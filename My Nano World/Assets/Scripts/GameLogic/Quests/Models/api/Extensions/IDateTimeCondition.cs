﻿namespace NanoReality.GameLogic.Quests
{
    public interface IDateTimeCondition : ICondition
    {
        /// <summary>
        /// Unix timestamp in seconds
        /// </summary>
        long Time { get; }
        
        /// <summary>
        /// Seconds left till achived state
        /// </summary>
        long TimeLeft { get; }
        
        /// <summary>
        /// Sets unix timestamp in seconds
        /// </summary>
        /// <param name="unixTimestamp"></param>
        void SetTime(long unixTimestamp);
    }
}
