﻿using System.Collections;
using System.Collections.Generic;
using NanoReality.GameLogic.Quests;
using UnityEngine;

/// <summary>
/// Условие логина в соц. сеть. Срабатывает когда юзер залогинился в соц. сеть
/// </summary>
public interface ILoginSocialNetworkCondition : ICondition {

	
}
