﻿namespace NanoReality.GameLogic.Quests
{
    public interface IConnectBuildingToRoadCondition : ICondition
    {
        Id_IMapObjectType BuildingTypeId { get; set; }
    }
}
