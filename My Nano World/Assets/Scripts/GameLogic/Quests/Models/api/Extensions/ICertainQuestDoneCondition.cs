﻿using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.Quests;


public interface ICertainQuestDoneCondition : ICondition
{
    Id<IQuest> QuestId { get; set; }
}
