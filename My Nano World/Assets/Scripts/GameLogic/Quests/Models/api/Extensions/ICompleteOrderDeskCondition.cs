﻿using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.Quests;

public interface ICompleteOrderDeskCondition : ICondition
{
    int RequiredCompletedOrdersAmount { get; set; }

    BusinessSectorsTypes BusinessSector { get; set; }
}