﻿namespace NanoReality.GameLogic.Quests
{
    /// <summary>
    /// Условие разблокировки по построенным зданиям в количестве N, левела L
    /// </summary>
    public interface IBuildBuildingsCondition : ICondition
    {
        /// <summary>
        /// Id здания которое должно быть построено
        /// </summary>
        Id_IMapObjectType BuildingId { get; set; }

        /// <summary>
        /// Количество зданий которые нужно построить
        /// </summary>
        int Amount { get; set; }
    }
}
