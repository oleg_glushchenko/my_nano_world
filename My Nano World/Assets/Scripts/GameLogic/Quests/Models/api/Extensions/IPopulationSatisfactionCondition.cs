﻿using NanoReality.GameLogic.Quests;

public interface IPopulationSatisfactionCondition : ICondition
{
    int SatisfyLevel { get; set; }
}
