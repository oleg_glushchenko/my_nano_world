﻿using NanoReality.GameLogic.Quests;

namespace GameLogic.Quests.Models.api.Extensions
{
    public interface IBuildingHaveByTypeCondition : ICondition
    {
        int RequiredNumber { get; set; }
    }
}
