﻿using NanoReality.GameLogic.Quests;

public interface IAuctionSalesCondition : ICondition
{
    int SalesCount { get; set; }
}
