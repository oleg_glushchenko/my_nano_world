﻿using NanoReality.GameLogic.Quests;

namespace GameLogic.Quests.Models.api.Extensions
{
    public interface IFoodSupplyFillBarCondition : ICondition
    {
        int RequiredFillBarProgress { get; set; }
    }
}
