﻿using NanoReality.GameLogic.Quests;
public interface IRepairBuildingCondition : ICondition
{
    Id_IMapObjectType BuildingToRepairID { get; set; }
}
