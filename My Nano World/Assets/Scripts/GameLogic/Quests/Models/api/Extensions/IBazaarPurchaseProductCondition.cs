﻿using NanoReality.GameLogic.Quests;

namespace GameLogic.Quests.Models.api.Extensions
{
    public interface IBazaarPurchaseProductCondition : ICondition
    {
        int RequiredProductAmount { get; set; }
        int RequiredProductId { get; set; }
    }
}