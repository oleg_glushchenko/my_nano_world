﻿using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;

namespace NanoReality.GameLogic.Quests
{
    public interface IConstructBuildingWithSubCategoryCondition : ICondition
    {
        BuildingSubCategories BuildingSubCategory { get; set; }
        int RequiredCount { get; set; }
    }
}
