﻿using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Quests;

namespace GameLogic.Quests.Models.api.Extensions
{
    public interface ICoverAmountBuildingsByServiceCondition : ICondition
    {
        MapObjectintTypes BuildingTypeId { get; set; }
        
        MapObjectintTypes ServiceType { get; set; }
        
        int RequiredCoveredBuildingAmount { get; set; }

        int ServiceObjectTypeId();
    }
}
