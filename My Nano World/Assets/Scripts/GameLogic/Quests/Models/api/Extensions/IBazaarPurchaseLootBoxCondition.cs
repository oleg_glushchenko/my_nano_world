﻿using NanoReality.GameLogic.Quests;

namespace GameLogic.Quests.Models.api.Extensions
{
    public interface IBazaarPurchaseLootBoxCondition : ICondition
    {
        int RequiredLootBoxesCount { get; set; }
    }
}
