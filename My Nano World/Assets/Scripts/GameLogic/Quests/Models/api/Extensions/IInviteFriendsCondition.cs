﻿using NanoReality.GameLogic.Quests;

public interface IInviteFriendsCondition : ICondition
{
    int InvitesCount { get; set; }
}
