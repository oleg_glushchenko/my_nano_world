﻿using NanoReality.GameLogic.Quests;

public interface IUniqueProductsAmountProducedCondition : ICondition
{
    /// <summary>
    /// Общее количество произведенных продуктов
    /// </summary>
    int UniqueProductsCountToAchieve { get; set; }
}
