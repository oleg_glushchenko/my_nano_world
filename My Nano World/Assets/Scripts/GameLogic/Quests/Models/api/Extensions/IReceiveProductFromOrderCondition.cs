﻿using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.Quests;

namespace GameLogic.Quests.Models.api.Extensions
{
    public interface IReceiveProductFromOrderCondition : ICondition
    {
        int RequiredProductAmount { get; set; }
        int RequiredProductId { get; set; }
        BusinessSectorsTypes BusinessSector { get; set; } 
    }
}
