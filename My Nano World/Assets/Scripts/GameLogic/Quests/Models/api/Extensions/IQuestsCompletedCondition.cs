﻿using NanoReality.GameLogic.Quests;

public interface IQuestsCompletedCondition : ICondition
{
    int CountOfCompletedQuests { get; set; }
}
