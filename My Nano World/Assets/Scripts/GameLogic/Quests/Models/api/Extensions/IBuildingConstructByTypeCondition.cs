﻿using NanoReality.GameLogic.Quests;

namespace GameLogic.Quests.Models.api.Extensions
{
    public interface IBuildingConstructByTypeCondition : ICondition
    {
        int RequiredNumber { get; set; }
    }
}
