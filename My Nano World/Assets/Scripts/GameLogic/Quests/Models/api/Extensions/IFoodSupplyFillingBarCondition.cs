namespace NanoReality.GameLogic.Quests
{
	public interface IFoodSupplyFillingBarCondition : ICondition
	{
		int TargetFilledBarCount { get; set; }
	}
}