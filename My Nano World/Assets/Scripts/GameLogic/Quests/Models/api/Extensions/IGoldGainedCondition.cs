﻿namespace NanoReality.GameLogic.Quests
{
    public interface IGoldGainedCondition : ICondition
    {
        int CountGoldToAchieve { get; set; }
    }
}