﻿using NanoReality.GameLogic.Quests;

public interface IProductsTotalProducedCondition : ICondition
{
    int CountProducedProducts { get; set; }
}
