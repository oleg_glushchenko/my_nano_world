﻿using Assets.Scripts.GameLogic.Quests.Models.api.Extensions;

namespace NanoReality.GameLogic.Quests
{
    /// <summary>
    /// Условие наличия построенного здания
    /// </summary>
    public interface IBuildingBuildCondition : IConditionWithProgress
    {
        /// <summary>
        /// Id здания которое должно быть построено
        /// </summary>
        Id_IMapObjectType BuildingTypeId { get; set; }
    }
}
