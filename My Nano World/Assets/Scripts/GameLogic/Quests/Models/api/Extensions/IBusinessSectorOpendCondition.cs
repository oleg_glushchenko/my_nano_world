﻿using NanoReality.GameLogic.BuildingSystem.City.Models.Api;

namespace NanoReality.GameLogic.Quests
{
    /// <summary>
    /// Условия открытия сектора у игрока
    /// </summary>
    public interface IBusinessSectorOpendCondition : ICondition
    {
        /// <summary>
        /// ID сектора который должен быть открыт у игрока
        /// </summary>
        BusinessSectorsTypes BusinessSectorId { get; set; }
    }
}
