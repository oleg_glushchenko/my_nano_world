﻿using NanoReality.GameLogic.Quests;

namespace GameLogic.Quests.Models.api.Extensions
{
    public interface IBazaarSpendGoldBarCondition : ICondition
    {
        int RequiredSpendGoldBarAmount { get; set; }
    }
}
