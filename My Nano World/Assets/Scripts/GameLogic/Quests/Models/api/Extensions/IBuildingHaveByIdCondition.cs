﻿using NanoReality.GameLogic.Quests;

namespace GameLogic.Quests.Models.api.Extensions
{
    public interface IBuildingHaveByIdCondition : ICondition
    {
        int BuildingId { get; set; }
        int RequiredNumber { get; set; }
    }
}
