﻿using NanoReality.GameLogic.Quests;

namespace Assets.Scripts.GameLogic.Quests.Models.api.Extensions
{
    public interface IConditionWithProgress : ICondition
    {
        bool IsInProgress { get; }
    }
}
