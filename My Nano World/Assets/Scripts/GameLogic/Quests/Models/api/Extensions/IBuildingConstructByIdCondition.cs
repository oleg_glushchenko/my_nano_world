﻿using NanoReality.GameLogic.Quests;

namespace GameLogic.Quests.Models.api.Extensions
{
    public interface IBuildingConstructByIdCondition : ICondition
    {
        int BuildingId { get; set; }
        
        int RequiredCount { get; set; }
    }
}
