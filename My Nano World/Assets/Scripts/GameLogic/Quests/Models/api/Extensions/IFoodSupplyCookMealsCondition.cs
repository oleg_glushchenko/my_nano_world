﻿using NanoReality.GameLogic.Quests;

namespace GameLogic.Quests.Models.api.Extensions
{
    public interface IFoodSupplyCookMealsCondition : ICondition
    {
        int RequiredCookMealsAmount { get; set; }
    }
}
