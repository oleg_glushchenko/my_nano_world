﻿using NanoReality.GameLogic.Quests;


public interface ICollectTaxesCondition : ICondition
{
    int CollectTaxesNeed { get; set; }
}
