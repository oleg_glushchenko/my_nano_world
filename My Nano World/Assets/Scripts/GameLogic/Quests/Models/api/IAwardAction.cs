﻿namespace NanoReality.GameLogic.Quests
{
    /// <summary>
    /// Объект действия (награды) вызываемого по окончанию квеста
    /// </summary>
    public interface IAwardAction 
    {
        /// <summary>
        /// Unique award id
        /// </summary>
        int AwardId { get; }
        
        /// <summary>
        /// Тип действия
        /// </summary>
        AwardActionTypes AwardType { get; }

        /// <summary>
        /// Количество награды
        /// </summary>
        int AwardCount { get; }

        /// <summary>
        /// Получает продукт награды
        /// </summary>
        AwardProduct GetAwardProduct { get; }        

        /// <summary>
        /// Инициализирует данные с сервера
        /// </summary>
        void InitServerData();

        /// <summary>
        /// Выполняет действие, дать награду
        /// </summary>
        void Execute(AwardSourcePlace sourcePlace, string source);

        /// <summary>
        /// Возвращает текст текущей награды сформированного с тейм.плейта описания награды
        /// </summary>
        /// <param name="baseDescription">теймплейт награды</param>
        /// <returns></returns>
        string GetAwardDescriptionText(IAwardActionDescription baseDescription);
    }

    /// <summary>
    /// Типы действий (наград) вызываемых по окончанию квестов
    /// </summary>
    public enum AwardActionTypes
    {
        LevelUp = 1,
        SoftCurrency = 2,
        PremiumCurrency = 3,
        Experience = 4,
        Resources = 5,
        GiveRandomProduct = 6,
        NanoGems = 7,
        Crystal = 8,
        GoldBars = 9,
        Lantern = 10
    }
}