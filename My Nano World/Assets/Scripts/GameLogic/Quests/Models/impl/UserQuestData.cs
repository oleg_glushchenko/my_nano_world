﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using Newtonsoft.Json;
using UnityEngine;

namespace NanoReality.GameLogic.Quests
{
    /// <summary>
    /// Объект контейнер хранящий список квестов игрока
    /// </summary>
    public class UserQuestData : IUserQuestData
    {
        [JsonProperty("available")]
        private List<IQuest> _avaliableQuests = new List<IQuest>();

        [JsonProperty("part_available")]
        private List<IQuest> _partAvaliableQuests = new List<IQuest>();
        
        private readonly Dictionary<Id<IQuest>, IQuest> _questsCache = new Dictionary<Id<IQuest>, IQuest>();

        public event Action OnQuestsInitialized;
        
        public UserQuestData()
        {
            UserQuestsData = new List<IQuest>();
        }

        /// <summary>
        /// Список квестов игрока
        /// </summary>
        public List<IQuest> UserQuestsData { get; set; }
        
        [Inject]
        public IQuestBalanceData QuestBalanceData { get; set; }

        /// <summary>
        /// Входная точка просчета квестов у юзера
        /// </summary>
        public void InitUserQuest()
        {
            UserQuestsData.Clear();
            _questsCache.Clear();

            for (int i = 0; i < _avaliableQuests.Count; i++)
            {
                var quest = _avaliableQuests[i];
                AddQuest(quest);
            }

            foreach (var partAvailableQuest in _partAvaliableQuests)
            {
                partAvailableQuest.QuestState = UserQuestStates.Unavailable;

                var quest = GetQuestById(partAvailableQuest.QuestId.Value);
                
                if (quest == null)
                {
                    AddQuest(partAvailableQuest);
                }
                else
                {
                    quest.UnlockConditions.AddRange(partAvailableQuest.UnlockConditions);
                }
            }

            foreach (var quest in UserQuestsData)
            {
                var prototype = QuestBalanceData.QuestBalance.Find(curr => quest.QuestId == curr.QuestId);
                if (prototype != null) //костыль на случай добавления новых квестов (еще не появились данные в QuestBalanceData) - NAN-3509
                {
                    quest.InitFromPrototype(prototype);
                }
            }

            foreach (var quest in QuestBalanceData.QuestBalance)
            {
                var find = GetQuestById(quest.QuestId.Value);
                if (find != null) continue;
                quest.QuestState = UserQuestStates.Unavailable;
                AddQuest(quest);
            }

            foreach (var userQuest in UserQuestsData)
            {
                try
                {
                    userQuest.InitUserQuest();
                }
                catch (Exception e)
                {
                    Debug.LogError($"Error while initialize Quest [{userQuest.QuestId}]. Exception: {e}");
                }

            }

            OnQuestsInitialized?.Invoke();
        }

        private void AddQuest(IQuest quest)
        {
            UserQuestsData.Add(quest);
            _questsCache[quest.QuestId.Value] = quest;
        }

        public IQuest GetQuestById(Id<IQuest> id)
        {
            IQuest quest;
            if (_questsCache.TryGetValue(id, out quest))
            {
                return quest;
            }
            return null;
        }

        public int GetActiveQuestsCount(BusinessSectorsTypes sectorType)
        {
            int count = 0;

            if (UserQuestsData != null)
            {
                for (int i = 0; i < UserQuestsData.Count; i++)
                {
                    var quest = UserQuestsData[i];
                    if (quest.BusinessSector == sectorType && 
                        (quest.QuestState == UserQuestStates.Avaliable ||
                         quest.QuestState == UserQuestStates.InProgress))
                    {
                        count++;
                    }
                }
            }
            
            return count;
        }

        public int GetAchievedQuestsCount(BusinessSectorsTypes sectorType)
        {
            int count = 0;

            if (UserQuestsData != null)
            {
                for (int i = 0; i < UserQuestsData.Count; i++)
                {
                    var quest = UserQuestsData[i];
                    if (quest.BusinessSector == sectorType && 
                        quest.QuestState == UserQuestStates.Achived)
                    {
                        count++;
                    }
                }
            }
            
            return count;
        }

        public List<IQuest> GetAchievedQuests(BusinessSectorsTypes sectorType)
        {           
            if (UserQuestsData != null)
            {
                return UserQuestsData.FindAll(
                    item => item.BusinessSector == sectorType && item.QuestState == UserQuestStates.Achived);
            }
            return null;
        }

        public int FirstAchievedQuestGiftBoxType(BusinessSectorsTypes sectorType)
        {
            var achievedQuests = GetAchievedQuests(sectorType);
            if (achievedQuests != null)
            {
                var quest = achievedQuests.FirstOrDefault();
                if (quest != null)
                {
                    return quest.GiftBoxType;
                }
            }
            return 0;
        }
    }
}