﻿namespace NanoReality.GameLogic.Quests
{
    /// <summary>
    /// Объект "описание награды"
    /// </summary>
    public class AwardActionDescription : IAwardActionDescription
    {

        /// <summary>
        /// Тип действия награды
        /// </summary>
        public AwardActionTypes AwardActionType { get; set; }

        /// <summary>
        /// Его описание
        /// </summary>
        public string Description { get; set; }
    }
}
