﻿using System.Collections.Generic;
using I2.Loc;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.Utilities;
using Newtonsoft.Json;
using strange.extensions.injector.api;
using strange.extensions.signal.impl;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Impl;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;

namespace NanoReality.GameLogic.Quests
{
    public abstract  class Condition : ICondition
    {
        [JsonProperty("condition_id")]
        public int ConditionId { get; set; }
        
        [JsonProperty("condition_type")]
        public ConditionTypes ConditionType { get; set; }
        
        [JsonProperty("status")]
        public ConditionStates ConditionState { get; set; }
        
        [JsonProperty("level")]
        public int Level { get; set; }
        
        [JsonProperty("building_type")]
        public MapObjectintTypes BuildingType { get; set; }
        
        // TODO: workaround. sets from quest sector.
        public BusinessSectorsTypes SectorsType { get; set; }

        [JsonProperty("params")] public List<SerializebleKeyValuePair> ServerParams { get; set; } = new List<SerializebleKeyValuePair>();

        public CityStatistics CityStatistics => jPlayerProfile.UserCity.CityStatistics;

        [Inject]
		public IPlayerProfile jPlayerProfile { get; set; }
        
        [Inject]
        [JsonIgnore]
        public IInjectionBinder jInjectionBinder { get; set; }
        
        [JsonIgnore]
        public Signal<ICondition> SignalOnAchieved { get; set; }
        
        private bool _isAchieved;
        
        public virtual bool IsAchived
        {
            get => _isAchieved;
            set
            {
                _isAchieved = value;
                if(_isAchieved)
                    SignalOnAchieved.Dispatch(this);
            }
        }
        
        public Condition()
        {
            SignalOnAchieved = new Signal<ICondition>();
            _isAchieved = false;
        }

        #region Abstract

        /// <summary>
        /// Returns localized description.
        /// </summary>
        public virtual string Description => LocalizationManager.GetTranslation($"Quests/OBJECT_CONDITION_DSCR_{(int) ConditionType}");
        
        public abstract void InitServerBalanceData();

        public abstract Requirement GetConditionRequirement();
        
        public abstract void StartEvaluateCondition();

        public abstract void InitLocalStateData();
        
        #endregion

        #region Virtual

        /// <summary>
        /// Updates current conditions progress from the server
        /// </summary>
        public virtual void InitServerStatesData()
        {
            _isAchieved = ConditionState == ConditionStates.Achieved;
        }
        
        /// <summary>
        /// Make variables of current state equal variables of condition balance and all conditions achieved
        /// </summary>
        public virtual void InitAchievedStateData()
        {
            ConditionState = ConditionStates.Achieved;
            _isAchieved = true;
        }

        public virtual void InitFromPrototype(ICondition condition)
        {
            ConditionType = condition.ConditionType;
            SectorsType = condition.SectorsType;
        }

        protected virtual ICondition GetNewConditionInstance => jInjectionBinder.GetInstance<ICondition>();

        public virtual object Clone()
        {
            var clone = GetNewConditionInstance;
            CopyMembers(clone);
            return clone;
        }

        protected virtual void CopyMembers(ICondition target)
        {
            target.IsAchived = IsAchived;
            target.SectorsType = SectorsType;
        }

        #endregion
        
        protected string GetParameterValue(string key)
        {
            var result = ServerParams.Find(curr => curr.KeyName == key);
            return result == null ? string.Empty : result.KeyValue;
        }
        
        /// <summary>
        /// Condition state on the server
        /// </summary>
        public enum ConditionStates
        {
            NotAchieved = 3,
            Achieved = 4
        }
    }
}