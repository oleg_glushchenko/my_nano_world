﻿namespace NanoReality.GameLogic.Quests
{
    public class AwardProduct
    {

        public AwardProduct(AwardActionTypes _type, string _name){
            AwardType = _type;
            AwardName = _name;

        }

        public AwardActionTypes AwardType;

        public string AwardName;


    }
}
