﻿using System.Collections.Generic;
using Assets.Scripts.Engine.UI.Views.Popups;
using Assets.Scripts.Engine.UI.Views.Popups.Settings;
using NanoLib.Core;
using NanoLib.Core.Logging;
using NanoLib.Core.Services.Sound;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using Newtonsoft.Json;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.Utilites;
using UnityEngine;

namespace NanoReality.GameLogic.Quests
{
    /// <summary>
    /// Представляет собой описание квеста в игре
    /// </summary>
    public class Quest : IQuest
    {
        #region Injects

        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public ISoundManager jSoundManager { get; set; }
        [Inject] public IPopupManager jPopupManager { get; set; }
        [Inject] public SignalOnQuestUnlock jSignalOnQuestUnlock { get; set; }
        [Inject] public SignalOnQuestStart jSignalOnQuestStart { get; set; }
        [Inject] public SignalOnQuestAchive jSignalOnQuestAchive { get; set; }
        [Inject] public SignalOnQuestComplete jSignalOnQuestComplete { get; set; }
        [Inject] public SignalOnQuestCompletedOnServer jSignalOnQuestCompletedOnServer { get; set; }
        [Inject] public SignalOnQuestFailed jSignalOnQuestFailed { get; set; }

        #endregion

        #region Game Balance Data

        [JsonProperty("quest_id")]
        public IntValue QuestId { get; private set; }

        [JsonProperty("title")]
        public string Name { get; private set; }

        [JsonProperty("bss_id")]
        public BusinessSectorsTypes BusinessSector { get; private set; }

		/// <summary>
		/// Должна ли отображаться реплика персонажа после выполнения квеста
		/// </summary>
		[JsonProperty("show")]
        public bool IsDialogShow { get; private set; }

        /// <summary>
        /// Список условий для разблокировки квеста
        /// </summary>
        [JsonProperty("conditions_unlock")]
        public List<ICondition> UnlockConditions { get; private set; }

        /// <summary>
        /// Список условий для выполнения квеста
        /// </summary>
        [JsonProperty("conditions_achieve")]
        public List<ICondition> AchiveConditions { get; private set; }

        /// <summary>
        /// Список действий наград вызываемых после окончания квеста
        /// </summary>
        [JsonProperty("award_actions")]
        public List<IAwardAction> AwardActions { get; private set; }

        [JsonProperty("gift_box_type")]
        public int GiftBoxType { get; private set; }

        #endregion

        #region User Data
        /// <summary>
        /// Состояние данного квеста
        /// </summary>
        [JsonProperty("status")]
        public UserQuestStates QuestState { get; set; }

        /// <summary>
        /// Текущие состояния условий выполнения квеста
        /// </summary>
        [JsonProperty("conditions_current_achieve")]
        public List<ICondition> AchiveConditionsStates { get; private set; }

        #endregion

        #region Properties

        public bool IsActive
        {
            get { return QuestState == UserQuestStates.Avaliable || QuestState == UserQuestStates.InProgress; }
        }

        #endregion

        public Quest()
        {
            AchiveConditionsStates = new List<ICondition>();
            AchiveConditions = new List<ICondition>();
            UnlockConditions = new List<ICondition>();
        }

        #region Methods

        public void InitFromPrototype(IQuest prototype)
        {
            QuestId = prototype.QuestId;
            Name = prototype.Name;
            UnlockConditions = prototype.UnlockConditions;
            AchiveConditions = prototype.AchiveConditions;
            AwardActions = prototype.AwardActions;
            BusinessSector = prototype.BusinessSector;
            IsDialogShow = prototype.IsDialogShow;
            GiftBoxType = prototype.GiftBoxType;
        }

        public void InitServerData()
        {
            if (UnlockConditions != null)
            {
                foreach (var unlockCondition in UnlockConditions)
                {
                    unlockCondition.SectorsType = BusinessSector;
                    unlockCondition.InitServerBalanceData();
                }
            }
            if (AchiveConditions != null)
            {
                foreach (var achiveCondition in AchiveConditions)
                {
                    achiveCondition.SectorsType = BusinessSector;
                    achiveCondition.InitServerBalanceData();
                }
            }
            if (AwardActions != null)
            {
                foreach (var awardAction in AwardActions)
                {
                    awardAction.InitServerData();
                }
            }
        }
        
        public void InitUserQuest()
        {
            // добавляем отсутсвующие кондишены в список состояний кондишенов квеста
            if (AchiveConditionsStates.Count < AchiveConditions.Count)
            {
                foreach (var achiveCondition in AchiveConditions)
                {
                    var find = AchiveConditionsStates.Find(curr => curr.ConditionId == achiveCondition.ConditionId);
                    if (find == null)
                    {
                        AchiveConditionsStates.Add(achiveCondition);
                    }
                }
            }

            var isAllConditionsCompleted = true;
            // инициализируем данные кондишенов
            foreach (var achiveConditionsState in AchiveConditionsStates)
            {
                var prototype = AchiveConditions.Find(curr => achiveConditionsState.ConditionId == curr.ConditionId);
                achiveConditionsState.InitFromPrototype(prototype);
                
                // если квест запущен, восстанавливаем прогресс по квесту
                if (QuestState == UserQuestStates.InProgress)
                {
                    achiveConditionsState.InitServerStatesData();
                    achiveConditionsState.SignalOnAchieved.AddListener(OnAchieveConditionStateChanged);
                    achiveConditionsState.StartEvaluateCondition();
                } else
                if (QuestState == UserQuestStates.Achived)
                {
                    achiveConditionsState.InitAchievedStateData();
                }

                isAllConditionsCompleted &= achiveConditionsState.IsAchived;
            }

            // для недоступных квестов
            if (QuestState == UserQuestStates.Unavailable)
            {
                // для каждого условия доступности квеста
                foreach (var unlockCondition in UnlockConditions)
                {
                    // слушаем сигнал изменения состояния квеста
                    unlockCondition.SignalOnAchieved.AddListener(OnUnlockConditionStateChanged);
                    // включаем ожидание сигналов выполнения условия
                    unlockCondition.StartEvaluateCondition();
                    // восстанавливаем состояние условия квеста
                    unlockCondition.InitLocalStateData();
                }
            }

            if (QuestState == UserQuestStates.InProgress)
            {
                if (isAllConditionsCompleted)
                {
                    VerifyQuestFinished();
                }
            }

            StartQuest();
        }

        /// <summary>
        /// Начинает выполнение квеста
        /// </summary>
        private void StartQuest()
        {
            if (QuestState == UserQuestStates.Avaliable)
            {
				// ПОПЫТКА ПОФИКСИТЬ БАГ!!!
				// ВЫЗОВ СЕРВАКА ДОЛЖЕН ИДТИ ВПЕРЕДИ ВСЕХ СИГНАЛОВ, ЧТО Б ЗАПРОСЫ ПРАВИЛЬНО СТРОИЛИСЬ В ОЧЕРЕДЬ!!!!!
				//уведомляем сервер о старте квеста
				jServerCommunicator.StartQuest(QuestId.Value, () => {  });

				QuestState = UserQuestStates.InProgress;
				StartListeningAchiveConditions();
				jSignalOnQuestStart.Dispatch(this);
            }
        }

        public void CollectAward()
        {
            QuestState = UserQuestStates.Completed;
            
            jPopupManager.Show(new RewardBoxPopupSettings(AwardActions, GiftBoxType), () =>
            {
                jServerCommunicator.CollectQuest(QuestId.Value, jPlayerProfile.UserCity.Id, awards =>
                {
                    foreach (var award in awards)
                    {
                        award.InitServerData();
                        award.Execute(AwardSourcePlace.QuestsAwards, QuestId.Value.ToString());
                    }

                    jSignalOnQuestComplete.Dispatch(this);
                    jSignalOnQuestCompletedOnServer.Dispatch(this, awards);
                });
            });
        }

        /// <summary>
        /// Квест начинает слушать ивенты выполнения
        /// </summary>
        private void StartListeningAchiveConditions()
        {
            foreach (var condition in AchiveConditionsStates)
            {
                condition.SignalOnAchieved.AddListener(OnAchieveConditionStateChanged);
                condition.StartEvaluateCondition();
            }
        }
        
        private void OnAchieveConditionStateChanged(ICondition sender)
        {
            if (QuestState != UserQuestStates.InProgress) return;

            var isAchieved = true;
            foreach (var condition in AchiveConditionsStates)
            {
                isAchieved &= condition.IsAchived;
            }

            if (isAchieved)
            {
                VerifyQuestFinished();
            }
        }

        /// <summary>
        /// Коллбек когда условие анлока квеста было изменено
        /// </summary>
        /// <param name="sender"></param>
       private void OnUnlockConditionStateChanged(ICondition sender)
        {
            if (QuestState != UserQuestStates.Unavailable) return;
            bool result = true;
            foreach (var unlockCondition in UnlockConditions)
            {
                result &= unlockCondition.IsAchived;
            }
            if (result)
            {
                // уведомляем сервак о анлоке квеста, но ответ не ждем
                jServerCommunicator.VerifyQuestUnlocked(QuestId, () => { });

                QuestState = UserQuestStates.Avaliable;
                foreach (var unlockCondition in UnlockConditions)
                {
                    unlockCondition.SignalOnAchieved.RemoveListener(OnUnlockConditionStateChanged);
                }
                jSignalOnQuestUnlock.Dispatch(this);

                // автоматический запуск квеста
                StartQuest();
            }
        }

        private void VerifyQuestFinished()
        {
            $"{QuestId} is complete!".Log();

            jServerCommunicator.VerifyQuestFinished(QuestId);
            jSoundManager.Play(SfxSoundTypes.CompletedQuest, SoundChannel.SoundFX);
            QuestState = UserQuestStates.Achived;
            StopListenAchieveConditions();
            
            jSignalOnQuestAchive.Dispatch(this);
        }
        
        private void StopListenAchieveConditions()
        {
            foreach (var condition in AchiveConditionsStates)
            {
                condition.SignalOnAchieved.RemoveListener(OnAchieveConditionStateChanged);
            }
        }

        public override string ToString()
        {
            return $"{LocalizationUtils.GetQuestTitle(QuestId.Value)} (id: {QuestId})";
        }

        #endregion
    }
}
