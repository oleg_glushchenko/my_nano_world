﻿using Assets.NanoLib.UI.Core.Signals;
using NanoReality.GameLogic.Achievements;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Utilites;
using NanoReality.GameLogic.Utilities;

namespace NanoReality.GameLogic.Quests
{
    /// <summary>
    /// Условие набора уровня игрока
    /// </summary>
    public class UserLevelAchivedCondition : Condition, IUserLevelAchievedCondition
    {
        /// <summary>
        /// Уровень который нужно набрать
        /// </summary>
        public int TargetUserLevel { get; set; }

        private int _currentLevel;

		[Inject] public AchievementConditionUpdateSignal jAchievementConditionUpdateSignal { get; set; }
        [Inject] public UserLevelUpSignal jUserLevelUpSignal { get; set; }
        
	    public override string Description
	    {
		    get { return LocalizationUtils.SafeFormat(base.Description, TargetUserLevel + 1); }
	    }
        
        public override void InitServerBalanceData()
        {
            int userLevel;
            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out userLevel))
                TargetUserLevel = userLevel;
        }

        public override void InitServerStatesData()
        {
            CheckForAchiev();
        }

        public override void InitAchievedStateData()
        {
	        base.InitAchievedStateData();

	        _currentLevel = TargetUserLevel;
        }

        /// <summary>
        /// Инициализирует данные баланса из прототипа
        /// </summary>
        /// <param name="condition"></param>
        public override void InitFromPrototype(ICondition condition)
        {
            base.InitFromPrototype(condition);
            var buildCondition = condition as UserLevelAchivedCondition;
            if (buildCondition == null) return; // копируем данные гейм. баланса из прототипа
            TargetUserLevel = buildCondition.TargetUserLevel;
        }

        /// <summary>
        /// Входая точка вычисления условия
        /// </summary>
        public override void StartEvaluateCondition()
        {
            jUserLevelUpSignal.AddListener(OnUserLevelUp);
            CheckForAchiev();
        }

        public override Requirement GetConditionRequirement()
        {
            return new Requirement(0, TargetUserLevel + 1, _currentLevel + 1);
        }

        /// <summary>
        /// Вызывается по сигналу лвл-апа юзера
        /// </summary>
        void OnUserLevelUp()
        {
			if (CheckForAchiev())
				jAchievementConditionUpdateSignal.Dispatch();
		}

		private bool CheckForAchiev()
		{
			_currentLevel = jPlayerProfile.CurrentLevelData.CurrentLevel;

			if (_currentLevel >= TargetUserLevel)
			{
				_currentLevel = TargetUserLevel;
				IsAchived = true;
				jUserLevelUpSignal.RemoveListener(OnUserLevelUp);
				return true;
			}
			return false;
				
		}

        /// <summary>
        /// Возвращает новый инстанс кондишена
        /// </summary>
        protected override ICondition GetNewConditionInstance
        { get { return jInjectionBinder.GetInstance<IUserLevelAchievedCondition>(); } }

        /// <summary>
        /// Копирует переменные в копию объекта
        /// </summary>
        /// <param name="target"></param>
        protected override void CopyMembers(ICondition target)
        {
            base.CopyMembers(target);
            var userCond = target as IUserLevelAchievedCondition;
            if (userCond != null) userCond.TargetUserLevel = TargetUserLevel;
        }

        /// <summary>
        /// Подсчитывает прогресс кондишена проверяя локальные показатели (используется для достижений)
        /// </summary>
        public override void InitLocalStateData()
        {
            CheckForAchiev();
        }
    }
}
