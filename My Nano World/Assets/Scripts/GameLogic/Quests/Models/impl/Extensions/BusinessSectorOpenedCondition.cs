﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.Achievements;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.PlayerProfile.Models.Api;

namespace NanoReality.GameLogic.Quests
{
    /// <summary>
    /// Условия открытия сектора у игрока
    /// </summary>
    public class BusinessSectorOpenedCondition : Condition, IBusinessSectorOpendCondition
    {
        /// <summary>
        /// ID сектора который должен быть открыт у игрока
        /// </summary>
        public BusinessSectorsTypes BusinessSectorId { get; set; }

        /// <summary>
        /// Ссылка на сигнал оповещающий что список доступных секторов изменился
        /// </summary>
        [Inject]
        public SignalOnUserBusinessSectorsUpdate SignalOnUserBusinessSectorsUpdate { get; set; }

        /// <summary>
        /// Вызывется когда у юзера обновляется список секторов
        /// </summary>
        /// <param name="sectorsList"></param>
        void OnSectorsUpdate(IEnumerable<BusinessSectorsTypes> sectorsList)
        {
            if (sectorsList.Contains(BusinessSectorId))
            {
                IsAchived = true;
                SignalOnUserBusinessSectorsUpdate.RemoveListener(OnSectorsUpdate);
            }
        }

        /// <summary>
        /// Инициализирует данные баланса из прототипа
        /// </summary>
        /// <param name="condition"></param>
        public override void InitFromPrototype(ICondition condition)
        {
            base.InitFromPrototype(condition);
            var sectorCondition = condition as BusinessSectorOpenedCondition;
            if (sectorCondition == null) return; // копируем данные гейм. баланса из прототипа
            BusinessSectorId = sectorCondition.BusinessSectorId;
        }

        /// <summary>
        /// Входая точка вычисления условия
        /// </summary>
        public override void StartEvaluateCondition()
        {
            SignalOnUserBusinessSectorsUpdate.AddListener(OnSectorsUpdate);
        }
        
        public override void InitServerBalanceData()
        {
			throw new NotImplementedException("GetAwardDescriptionText");
        }

        /// <summary>
        /// Возвращает новый инстанс кондишена
        /// </summary>
        protected override ICondition GetNewConditionInstance { get { return jInjectionBinder.GetInstance<IBusinessSectorOpendCondition>(); } }

        protected override void CopyMembers(ICondition target)
        {
            base.CopyMembers(target);
            var sectorCondition = target as IBusinessSectorOpendCondition;
            if (sectorCondition != null) sectorCondition.BusinessSectorId = BusinessSectorId;
        }


        public override Requirement GetConditionRequirement()
        {
            return new Requirement((int)BusinessSectorId, 1, 0);
        }

        public override void InitLocalStateData()
        {

        }
    }
}
