﻿using GameLogic.Quests.Models.api.Extensions;
using NanoReality.Game.Services.OrderDesk.SeaportOrderDesk;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.OrderDesk;
using NanoReality.GameLogic.Orders;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilites;
using NanoReality.GameLogic.Utilities;
using UniRx;

namespace GameLogic.Quests.Models.impl.Extensions
{
    public class ShuffleOrderTableCondition : Condition, IShuffleOrderTableCondition
    {
        [Inject] public MetroOrderShuffledSignal jMetroOrderShuffleSignal { get; set; }
        [Inject] public ISeaportOrderDeskService jSeaportOrderDeskService  { get; set; }
        
        [Inject] public IMapObjectsData jMapObjectsData { get; set; }
        
        public int BuildingTypeId { get; set; }
        public BusinessSectorsTypes BusinessSector { get; set; }

        public override string Description => LocalizationUtils.SafeFormat(base.Description, LocalizationUtils.GetBuildingText(BuildingTypeId));

        public override void InitServerBalanceData()
        {
            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out int buildingId))
            {
                BuildingTypeId = buildingId;
            }
        }

        public override Requirement GetConditionRequirement()
        {
            return new Requirement(BuildingTypeId, 1, IsAchived ? 1 : 0);
        }

        public override void StartEvaluateCondition()
        {
            SetBusinessSector();
            
            switch (BusinessSector)
            {
                case BusinessSectorsTypes.Town:
                    jMetroOrderShuffleSignal.AddOnce(OnMetroOrderShuffle);
                    break;
                case BusinessSectorsTypes.HeavyIndustry:
                    jSeaportOrderDeskService.OnSeaportSlotShuffleUpdated += OnSeaportShuffleOrder;
                    break;
            }
        }

        public override void InitLocalStateData()
        {
        }
        
        public override void InitFromPrototype(ICondition condition)
        {
            base.InitFromPrototype(condition);
            var castedConditions = (IShuffleOrderTableCondition) condition;
            BuildingTypeId = castedConditions.BuildingTypeId;
        }
        
        protected override ICondition GetNewConditionInstance => jInjectionBinder.GetInstance<IShuffleOrderTableCondition>();
        
        protected override void CopyMembers(ICondition target)
        {
            base.CopyMembers(target);
            var castedTarget = (IShuffleOrderTableCondition) target;
            castedTarget.BuildingTypeId = BuildingTypeId;
        }

        private void SetBusinessSector()
        {
            IMapObject requiredObjectModel = jMapObjectsData.GetByBuildingIdAndLevel(BuildingTypeId, 0);
            
            if (requiredObjectModel.SectorIds.Contains(BusinessSectorsTypes.Town))
            {
                BusinessSector = BusinessSectorsTypes.Town;
            }
            else if(requiredObjectModel.SectorIds.Contains(BusinessSectorsTypes.HeavyIndustry))
            {
                BusinessSector = BusinessSectorsTypes.HeavyIndustry;
            }
        }
        
        private void OnMetroOrderShuffle(int orderId)
        {
            IsAchived = true;
        }
        
        private void OnSeaportShuffleOrder(ISeaportOrderSlot seaportOrderSlot)
        {
            IsAchived = true;
            jSeaportOrderDeskService.OnSeaportSlotShuffleUpdated -= OnSeaportShuffleOrder;
        }
    }
}
