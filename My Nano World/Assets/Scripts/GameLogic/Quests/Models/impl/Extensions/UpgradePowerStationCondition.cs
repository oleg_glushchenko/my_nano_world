﻿using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilities;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.GameManager.Models.api;

public class UpgradePowerStationCondition : Condition, IUpgradePowerStationCondition
{
    #region Injections

    /// <summary>
    /// GlobalCityMap для доступа к PowerStation
    /// </summary>
    [Inject]
    public IGameManager jGameManager { get; set; }

    /// <summary>
    /// Ссылка на сигнал который происходит при постройке здания
    /// </summary>
    [Inject]
    public SignalOnMapObjectBuildFinished jOnMapObjectBuildingFinishedSignal { get; set; }

    /// <summary>
    /// Сигнал необходимости обновить состояние ачивок
    /// </summary>
    [Inject] public AchievementConditionUpdateSignal jAchievementConditionUpdateSignal { get; set; }

    #endregion

    #region Fields

    /// <summary>
    /// Возвращает новый инстанс кондишена
    /// </summary>
    protected override ICondition GetNewConditionInstance
    {
        get
        {
            return jInjectionBinder.GetInstance<IUpgradePowerStationCondition>();
        }
    }

    #endregion

    #region Methods

    /// <summary>
    /// Инициализирует локальное положение вещей
    /// </summary>
    public override void InitLocalStateData()
    {
        CheckForAchieve();
    }

    /// <summary>
    /// Инициализирует балансовые данные кондишна
    /// </summary>
    public override void InitServerBalanceData()
    {
        int tmp;
        if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out tmp))
            Level = tmp;
    }

    /// <summary>
    /// Ицинициализация кондишна из прототипа с копированием переменных
    /// </summary>
    /// <param name="condition"></param>
    public override void InitFromPrototype(ICondition condition)
    {
        base.InitFromPrototype(condition);
        var upgradeCondition = condition as IUpgradePowerStationCondition;
        if (upgradeCondition != null)
            Level = upgradeCondition.Level;
    }


    /// <summary>
    /// Копирует переменные в копию объекта
    /// </summary>
    /// <param name="target">целевой объект, в который будут копироваться переменные</param>
    protected override void CopyMembers(ICondition target)
    {
        base.CopyMembers(target);
        var buildCond = target as IUpgradePowerStationCondition;
        if (buildCond == null)
            return;

        buildCond.Level = Level;
    }

    /// <summary>
    /// Возвращает прогресс кондишна, второе поле - необходимый уровень, третье - полученный текущий
    /// </summary>
    public override Requirement GetConditionRequirement()
    {
        return new Requirement(0, Level, 0);
    }

    /// <summary>
    /// Входная точка вычисления условия
    /// </summary>
    public override void StartEvaluateCondition()
    {
        if (!CheckForAchieve())
        {
            jOnMapObjectBuildingFinishedSignal.RemoveListener(OnBuidingUpdate);
            jOnMapObjectBuildingFinishedSignal.AddListener(OnBuidingUpdate);
        }
    }

    /// <summary>
    /// Вызывается при обновлении состояния здания (постройки или апгрейда)
    /// </summary>
    /// <param name="mapObject">Объект, состояние которго обновилось</param>
    private void OnBuidingUpdate(IMapObject mapObject)
    {
        if (mapObject.MapObjectType == MapObjectintTypes.PowerStation && mapObject.Level >= Level)
        {
            if (!CheckForAchieve())
            {
                jAchievementConditionUpdateSignal.Dispatch();
            }
        }
    }

    /// <summary>
    /// Проверка кондишна на выполненость
    /// </summary>
    private bool CheckForAchieve()
    {
        return false;
    }

    #endregion
}
