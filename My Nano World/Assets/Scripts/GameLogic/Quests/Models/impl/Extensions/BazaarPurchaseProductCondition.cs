﻿using GameLogic.Bazaar.Controllers.Signals;
using GameLogic.Bazaar.Model.impl;
using GameLogic.Quests.Models.api.Extensions;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilites;
using NanoReality.GameLogic.Utilities;
using UnityEngine;

namespace GameLogic.Quests.Models.impl.Extensions
{
    public class BazaarPurchaseProductCondition : Condition, IBazaarPurchaseProductCondition
    {
        [Inject] public BazaarBuyProductSignal jBazaarBuyProductSignal { get; set; }
        
        public int RequiredProductAmount { get; set; }
        public int RequiredProductId { get; set; }
        
        public override string Description =>
            LocalizationUtils.SafeFormat(base.Description, 
                Mathf.Clamp(_currentProductAmount, 0, RequiredProductAmount).ToString(),
                RequiredProductAmount.ToString(), 
                LocalizationUtils.GetProductText(RequiredProductId));
        
        private int _currentProductAmount;
        
        public override void InitServerBalanceData()
        {
            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out int value1))
            {
                RequiredProductId = value1;
            }

            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_2"), out int value2))
            {
                RequiredProductAmount = value2;
            }
        }

        public override void InitServerStatesData()
        {
            base.InitServerStatesData();
            
            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out int value1))
            {
                RequiredProductId = value1;
            }

            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_2"), out int value2))
            {
                _currentProductAmount = value2;
            }
        }

        public override Requirement GetConditionRequirement()
        {
            return new Requirement(RequiredProductId, RequiredProductAmount, _currentProductAmount);
        }

        public override void StartEvaluateCondition()
        {
            if (AchievementCheck())
            {
                IsAchived = true;
            }
            else
            {
                AddListeners();
            }
        }
        
        public override void InitAchievedStateData()
        {
            base.InitAchievedStateData();

            _currentProductAmount = RequiredProductAmount;
        }

        public override void InitLocalStateData()
        {
        }
        
        public override void InitFromPrototype(ICondition condition)
        {
            base.InitFromPrototype(condition);
            var castedConditions = (IBazaarPurchaseProductCondition) condition;
            RequiredProductId = castedConditions.RequiredProductId;
            RequiredProductAmount = castedConditions.RequiredProductAmount;
        }
        
        protected override ICondition GetNewConditionInstance => jInjectionBinder.GetInstance<IBazaarPurchaseProductCondition>();
        
        protected override void CopyMembers(ICondition target)
        {
            base.CopyMembers(target);
            var castedTarget = (IBazaarPurchaseProductCondition) target;
            castedTarget.RequiredProductId = RequiredProductId;
            castedTarget.RequiredProductAmount = RequiredProductAmount;
        }
        
        private bool AchievementCheck()
        {
            return _currentProductAmount >= RequiredProductAmount;
        }
        
        private void AddListeners()
        {
            jBazaarBuyProductSignal.AddListener(OnBazaarBuyProduct);
        }
        
        private void RemoveListeners()
        {
            jBazaarBuyProductSignal.RemoveListener(OnBazaarBuyProduct);
        }

        private void OnBazaarBuyProduct(SoukTopSlot soukTopSlot)
        {
            if (soukTopSlot.ProductId != RequiredProductId)
                return;

            _currentProductAmount += soukTopSlot.Count;
            if (AchievementCheck())
            {
                IsAchived = true;
                RemoveListeners();
            }
        }
    }
}
