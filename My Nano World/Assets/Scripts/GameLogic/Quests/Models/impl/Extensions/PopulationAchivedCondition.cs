﻿using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Utilites;
using NanoReality.GameLogic.Utilities;
using UnityEngine;

namespace NanoReality.GameLogic.Quests
{
    /// <summary>
    /// Условие общего кол-ва населения в городе
    /// </summary>
    public class PopulationAchivedCondition : Condition, IPopulationAchivedCondition
    {
        #region Injects
        [Inject] public SignalOnCityPopulationUpdated jSignalOnCityPopulationUpdated { get; set; }

        [Inject] public AchievementConditionUpdateSignal jAchievementConditionUpdateSignal { get; set; }

		[Inject] public ICityPopulation jCityPopulation { get; set; }
        #endregion

        /// <summary>
        /// Кол-во населения которое нужно достигнуть
        /// </summary>
        public int PopulationCountToAchive { get; set; }

        private int _cachedPopulationCount = 0;

        public override string Description
        {
            get { return LocalizationUtils.SafeFormat(base.Description, jCityPopulation.GetTotalCityPopulation(), PopulationCountToAchive); }
        }

        /// <summary>
        /// Инициализирует данные баланса из прототипа
        /// </summary>
        /// <param name="condition"></param>
        public override void InitFromPrototype(ICondition condition)
        {
            var populationAchivedCondition = condition as PopulationAchivedCondition;
            if (populationAchivedCondition == null) return; // копируем данные гейм. баланса из прототипа
            PopulationCountToAchive = populationAchivedCondition.PopulationCountToAchive;
        }

        /// <summary>
        /// Возвращает новый инстанс кондишена
        /// </summary>
        protected override ICondition GetNewConditionInstance { get { return jInjectionBinder.GetInstance<IPopulationAchivedCondition>(); } }

        /// <summary>
        /// Копирует переменные в копию объекта
        /// </summary>
        /// <param name="target"></param>
        protected override void CopyMembers(ICondition target)
        {
            base.CopyMembers(target);
            var populationCondition = target as IPopulationAchivedCondition;
            if (populationCondition != null) populationCondition.PopulationCountToAchive = PopulationCountToAchive;
        }

        public override Requirement GetConditionRequirement()
        {
            var currentPopulationCount = IsAchived ? PopulationCountToAchive : jCityPopulation.GetTotalCityPopulation();
            return new Requirement(0, PopulationCountToAchive, currentPopulationCount);
        }

        public override void StartEvaluateCondition()
        {
            //if (IsAchived) return;

            var curr = jCityPopulation.GetTotalCityPopulation();
            if (curr >= PopulationCountToAchive)
            {
                IsAchived = true;
            }
            else
            {
                jSignalOnCityPopulationUpdated.AddListener(OnCityPopulationUpdated);
            }
        }

        private void OnCityPopulationUpdated()
        {
            CheckAchive();
        }

        private void CheckAchive()
        {
            var curr = jCityPopulation.GetTotalCityPopulation();
            if (curr >= PopulationCountToAchive)
            {
                IsAchived = true;
                jSignalOnCityPopulationUpdated.RemoveListener(OnCityPopulationUpdated);
            }

            // send achievement update signal when achievement data changed or when achievement completed
            if (_cachedPopulationCount != curr) //do not send update if current value hasn't changed since last check
            {
                _cachedPopulationCount = curr;
                jAchievementConditionUpdateSignal.Dispatch();
            }
        }

        public override void InitServerBalanceData()
        {
            int populationCountToAchive;
            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out populationCountToAchive))
                PopulationCountToAchive = populationCountToAchive;
        }

        public override void InitLocalStateData()
        {

        }
    }
}