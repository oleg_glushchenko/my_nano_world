﻿using NanoReality.GameLogic.BuildingSystem.CityMap.Controllers;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Utilites;
using NanoReality.GameLogic.Utilities;

namespace NanoReality.GameLogic.Quests
{
    public class ConnectBuildingToRoadCondition : Condition, IConnectBuildingToRoadCondition
    {
        public Id_IMapObjectType BuildingTypeId { get; set; }
        
        [Inject]
        public SignalOnBuildingRoadConnectionChanged jSignalOnBuildingRoadConnectionChanged { get; set; }
        
        public override string Description
        {
            get { return LocalizationUtils.SafeFormat(base.Description, LocalizationUtils.GetBuildingText(BuildingTypeId)); }
        }
        
        public override void InitServerBalanceData()
        {
            int buildingId;
            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out buildingId))
            {
                BuildingTypeId = buildingId;
            }
        }

        public override void InitFromPrototype(ICondition condition)
        {
            base.InitFromPrototype(condition);
            
            var prototype = condition as IConnectBuildingToRoadCondition;

            if (prototype != null)
            {
                BuildingTypeId = prototype.BuildingTypeId;
            }
        }
        
        protected override ICondition GetNewConditionInstance
        {
            get { return jInjectionBinder.GetInstance<IConnectBuildingToRoadCondition>(); }
        }

        public override Requirement GetConditionRequirement()
        {
            return new Requirement(BuildingTypeId.Value, 1, IsAchived ? 1 : 0);
        }
        
        protected override void CopyMembers(ICondition target)
        {
            base.CopyMembers(target);

            var repairBuildingCondition = target as IConnectBuildingToRoadCondition;
            if (repairBuildingCondition == null)
                return;

            repairBuildingCondition.BuildingTypeId = BuildingTypeId;
        }
        
        public override void StartEvaluateCondition()
        {
            jSignalOnBuildingRoadConnectionChanged.AddListener(OnChangedMapObjectOnServer);
        }

        public override void InitLocalStateData()
        {
        }

        private void OnChangedMapObjectOnServer(IMapObject obj)
        {
            var building = obj as IMapBuilding;
            if (building != null && building.ObjectTypeID == BuildingTypeId)
            {
                if (building.IsConnectedToGeneralRoad)
                {
                    jSignalOnBuildingRoadConnectionChanged.RemoveListener(OnChangedMapObjectOnServer);
                    IsAchived = true;
                }
            }
        }
    }
}
