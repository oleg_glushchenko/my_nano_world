﻿using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilites;
using NanoReality.GameLogic.Utilities;

/// <summary>
/// Условие разблокирования N секторов
/// </summary>
public class UnlockSectorForConstruction : Condition, IUnlockSectorForConstruction
{
    public int Amount { get; set; }

	[Inject] public AchievementConditionUpdateSignal jAchievementConditionUpdateSignal { get; set; }
    [Inject] public SignalOnSubSectorUnlocked jSignalOnSubSectorUnlocked { get; set; }

    private int _currentUnlockedSubSectors;
    
    public override string Description
    {
        get { return LocalizationUtils.SafeFormat(base.Description, _currentUnlockedSubSectors, Amount); }
    }

    public override void InitServerBalanceData()
    {
        int countToAchieve;
        if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out countToAchieve))
        {
            Amount = countToAchieve;
        }
    }

    public override void InitServerStatesData()
    {
        base.InitServerStatesData();
        int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out _currentUnlockedSubSectors);
    }

    public override void InitAchievedStateData()
    {
        base.InitAchievedStateData();

        _currentUnlockedSubSectors = Amount;
    }

    public override void InitFromPrototype(ICondition condition)
    {
        base.InitFromPrototype(condition);
        var prot = condition as IUnlockSectorForConstruction;

        if (prot != null)
            Amount = prot.Amount;
    }

    protected override ICondition GetNewConditionInstance { get { return jInjectionBinder.GetInstance<IUnlockSectorForConstruction>(); } }

    protected override void CopyMembers(ICondition target)
    {
        base.CopyMembers(target);
        var prot = target as IUnlockSectorForConstruction;
        if (prot == null) return;
        prot.Amount = Amount;
    }
    
    public override Requirement GetConditionRequirement()
    {
        return new Requirement(0, Amount, _currentUnlockedSubSectors);
    }

    public override void StartEvaluateCondition()
    {
        jSignalOnSubSectorUnlocked.AddListener(SubSectorUnlocked);
    }

    private void SubSectorUnlocked(CityLockedSector unlockedCitySector)
    {
        _currentUnlockedSubSectors++;
		if (!CheckForUnlock())
			jAchievementConditionUpdateSignal.Dispatch();
    }

	private bool CheckForUnlock()
    {
        if (_currentUnlockedSubSectors >= Amount)
        {
            _currentUnlockedSubSectors = Amount;
            IsAchived = true;
            jSignalOnSubSectorUnlocked.RemoveListener(SubSectorUnlocked);
			return true;
        }
		return false;
    }

    public override void InitLocalStateData()
    {
        _currentUnlockedSubSectors = CityStatistics.UnlockedSubSectors;
        CheckForUnlock();
    }
}