﻿using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilites;
using NanoReality.GameLogic.Utilities;

/// <summary>
/// Условие разблокирования здания. Используется для доступности квеста
/// </summary>
public class MBuildingUnlockedCondition : Condition, IBuildingUnlockedCondition
{
    public Id_IMapObjectType UnlockedBuildingID { get; set; }

    [Inject]
    public IMapObjectsData MapObjectsData { get; set; }

    [Inject]
    public SignalOnBuildingUnlocked SignalOnBuildingUnlocked { get; set; }
    
    public override string Description
    {
        get { return LocalizationUtils.SafeFormat(base.Description, LocalizationUtils.GetBuildingText(UnlockedBuildingID)); }
    }

    public override void InitServerBalanceData()
    {
        int tmp;
        if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out tmp))
            UnlockedBuildingID = tmp;
    }

    public override void InitFromPrototype(ICondition condition)
    {
        base.InitFromPrototype(condition);
        var prototype = condition as IBuildingUnlockedCondition;

        if (prototype != null)
            UnlockedBuildingID = prototype.UnlockedBuildingID;
    }

    /// <summary>
    /// Возвращает новый инстанс кондишена
    /// </summary>
    protected override ICondition GetNewConditionInstance
    {
        get { return jInjectionBinder.GetInstance<IBuildingUnlockedCondition>(); }
    }

    /// <summary>
    /// Копирует переменные в копию объекта
    /// </summary>
    /// <param name="target"></param>
    protected override void CopyMembers(ICondition target)
    {
        base.CopyMembers(target);
        var buildCond = target as IBuildingUnlockedCondition;

        if (buildCond == null)
            return;

        buildCond.UnlockedBuildingID = UnlockedBuildingID;
    }

    public override Requirement GetConditionRequirement()
    {
        return new Requirement(UnlockedBuildingID.Value, 1, IsAchived ? 1 : 0);
    }

    public override void StartEvaluateCondition()
    {
        SignalOnBuildingUnlocked.AddListener(OnBuildingUnlocked);
    }

    private void OnBuildingUnlocked(Id_IMapObjectType id)
    {
        if (id == UnlockedBuildingID)
        {
            IsAchived = true;
            SignalOnBuildingUnlocked.RemoveListener(OnBuildingUnlocked);
        }
    }

    public override void InitLocalStateData()
    {
    }
}
