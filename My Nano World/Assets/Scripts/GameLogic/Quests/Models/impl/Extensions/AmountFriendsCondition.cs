﻿using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.SocialCommunicator.api;
using NanoReality.GameLogic.StartApplicationSequence.Controllers.Commands;
using NanoReality.GameLogic.Utilities;
using System.Collections.Generic;
using NanoReality.GameLogic.Utilites;
using UnityEngine.SocialPlatforms;

public class AmountFriendsCondition : Condition, IAmountFriendsCondition
{
    [Inject] public OnFriendsUpdatedSignal jOnFriendsUpdatedSignal { get; set; }
    [Inject] public AchievementConditionUpdateSignal jAchievementConditionUpdateSignal { get; set; }

    [Inject(SocialNetworkCommunicatorTypes.Secondary)]
    public ISocialNetworkCommunicator jSocialNetworkCommunicator { get; set; }

    [Inject] public IServerCommunicator jServerCommunicator { get; set; }

    public int AmountFriends { get; set; }

    private int _friendsCount;
    
    public override string Description
    {
        get { return LocalizationUtils.SafeFormat(base.Description, _friendsCount, AmountFriends); }
    }

    public override void InitServerBalanceData()
    {
        int countToAchieve;
        if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out countToAchieve))
        {
            AmountFriends = countToAchieve;
        }
    }

    public override void InitLocalStateData()
    {
        _friendsCount = 0;

        if (jSocialNetworkCommunicator.IsUserAuthorized)
        {
            jSocialNetworkCommunicator.LoadUserFriends(OnLoadedUserFriends);
        }

        jAchievementConditionUpdateSignal.Dispatch();
    }

    private void OnLoadedUserFriends(bool loadResult, IUserProfile[] profiles, ISocialNetworkCommunicator social)
    {
        if (!loadResult) return;
    }

    public override void InitFromPrototype(ICondition condition)
    {
        base.InitFromPrototype(condition);
        var prot = condition as IAmountFriendsCondition;

        if (prot != null)
            AmountFriends = prot.AmountFriends;
    }

    protected override ICondition GetNewConditionInstance { get { return jInjectionBinder.GetInstance<IAmountFriendsCondition>(); } }

    protected override void CopyMembers(ICondition target)
    {
        base.CopyMembers(target);
        var prot = target as IAmountFriendsCondition;
        if (prot == null) return;
        prot.AmountFriends = AmountFriends;
    }

    public override Requirement GetConditionRequirement()
    {
        return new Requirement(0, AmountFriends, IsAchived ? AmountFriends : _friendsCount);
    }

    public override void StartEvaluateCondition()
    {
        jOnFriendsUpdatedSignal.AddListener(OnFriendsUpdated);
    }

    private void OnFriendsUpdated(int friendsCount)
    {
        if (_friendsCount == friendsCount) return;
        
        _friendsCount = friendsCount;

        if (_friendsCount >= AmountFriends)
        {
            _friendsCount = AmountFriends;
            jOnFriendsUpdatedSignal.RemoveListener(OnFriendsUpdated);
            IsAchived = true;
        }
        else 
        {
            jAchievementConditionUpdateSignal.Dispatch();
        }
    }
}
