﻿using NanoReality.GameLogic.Achievements;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilities;
using System.Linq;

/// <summary>
/// Условие сбора N софт валюты с зданий M
/// </summary>
public class MCollectCoinsFromCommercialBuildingsCondition : Condition,  ICollectCoinsFormCommercialBuildingsCondition
{
	[Inject] public AchievementConditionUpdateSignal jAchievementConditionUpdateSignal { get; set; }
    [Inject] public SignalOnBuildingShipAward jSignalOnBuildingShipAward { get; set; }
    [Inject] public IMapObjectsData MapObjectsData { get; set; }
    
    public Id_IMapObjectType BuildingTypeId { get; set; }

    public int CoinsAmount { get; set; }

    private int _earnedGold; 

    public override void InitServerBalanceData()
    {
        int value;

        if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out value))
        {
            CoinsAmount = value;
        }
        if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_2"), out value))
        {
            BuildingTypeId = value;
        }
    }

    public override void InitServerStatesData()
    {
        base.InitServerStatesData();
        int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out _earnedGold);
    }

    public override void InitAchievedStateData()
    {
        base.InitAchievedStateData();

        _earnedGold = CoinsAmount;
    }

    public override void InitFromPrototype(ICondition condition)
    {
        base.InitFromPrototype(condition);
        var goldProt = condition as ICollectCoinsFormCommercialBuildingsCondition;

        if (goldProt != null)
        {
            CoinsAmount = goldProt.CoinsAmount;
            BuildingTypeId = goldProt.BuildingTypeId;
        }
    }

    /// <summary>
    /// Возвращает новый инстанс кондишена
    /// </summary>
    protected override ICondition GetNewConditionInstance { get { return jInjectionBinder.GetInstance<ICollectCoinsFormCommercialBuildingsCondition>(); } }

    /// <summary>
    /// Копирует переменные в копию объекта
    /// </summary>
    /// <param name="target"></param>
    protected override void CopyMembers(ICondition target)
    {
        base.CopyMembers(target);
        var goldProt = target as ICollectCoinsFormCommercialBuildingsCondition;
        if (goldProt == null) return;
        goldProt.CoinsAmount = CoinsAmount;
        goldProt.BuildingTypeId = BuildingTypeId;
    }

    public override Requirement GetConditionRequirement()
    {
        return new Requirement(0, CoinsAmount, _earnedGold);
    }

    public void OnCollectCoinsFromCommercialBuilding(Id_IMapObjectType building, int earnedGold)
    {
        if (BuildingTypeId.Value > 0 && BuildingTypeId != building) return;
		if (earnedGold == 0)
			return; 
        _earnedGold += earnedGold;

        if (_earnedGold >= CoinsAmount)
        {
            IsAchived = true;
            _earnedGold = CoinsAmount;
            jSignalOnBuildingShipAward.RemoveListener(OnCollectCoinsFromCommercialBuilding);
        }
		
        jAchievementConditionUpdateSignal.Dispatch ();
    }

    /// <summary>
    /// Входная точка вычисления условия
    /// </summary>
    public override void StartEvaluateCondition()
    {
        jSignalOnBuildingShipAward.AddListener(OnCollectCoinsFromCommercialBuilding);
    }

    public override void InitLocalStateData()
    {
        int coins;

        if (BuildingTypeId.Value > 0)
        {
			CityStatistics.CollectedCoinsData.TryGetValue(BuildingTypeId, out coins);
        }
        else
        {
            coins = CityStatistics.TotalCollectCommercialBuilingCoins;
        }
 
        OnCollectCoinsFromCommercialBuilding(BuildingTypeId, coins);
    }
}
