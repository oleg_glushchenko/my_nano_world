﻿using System;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilites;
using NanoReality.GameLogic.Utilities;

public class AuctionSpendCoinsCondition : Condition, IAuctionSpendCoinsCondition
{
    [Inject] public AchievementConditionUpdateSignal jAchievementConditionUpdateSignal { get; set; }

    public int SpendCoinsCount { get; set; }

    private int _spendCount;
    
    public override string Description
    {
        get { return LocalizationUtils.SafeFormat(base.Description, _spendCount, SpendCoinsCount); }
    }

    public override void InitServerBalanceData()
    {
        int countToAchieve;
        if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out countToAchieve))
        {
            SpendCoinsCount = countToAchieve;
        }
    }

    public override void InitServerStatesData()
    {
        base.InitServerStatesData();
        int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out _spendCount);
        if(_spendCount > SpendCoinsCount)
            _spendCount = SpendCoinsCount;
    }

    public override void InitAchievedStateData()
    {
        base.InitAchievedStateData();

        _spendCount = SpendCoinsCount;
    }

    public override void InitFromPrototype(ICondition condition)
    {
        base.InitFromPrototype(condition);
        var prot = condition as IAuctionSpendCoinsCondition;

        if (prot != null)
            SpendCoinsCount = prot.SpendCoinsCount;
    }

    protected override ICondition GetNewConditionInstance { get { return jInjectionBinder.GetInstance<IAuctionSpendCoinsCondition>(); } }

    protected override void CopyMembers(ICondition target)
    {
        base.CopyMembers(target);
        var prot = target as IAuctionSpendCoinsCondition;
        if (prot == null) return;
        prot.SpendCoinsCount = SpendCoinsCount;
    }

    public override Requirement GetConditionRequirement()
    {
        return new Requirement(0, SpendCoinsCount, _spendCount);
    }

    public override void StartEvaluateCondition()
    {
        throw new NotImplementedException();
    }
    
    private void CheckForComplete()
    {
        if (_spendCount >= SpendCoinsCount)
        {
            _spendCount = SpendCoinsCount;
            IsAchived = true;
        }
		
        jAchievementConditionUpdateSignal.Dispatch();
    }

    public override void InitLocalStateData()
    {
        _spendCount += CityStatistics.AuctionSpendCoins;
        CheckForComplete();
    }
}