﻿using GameLogic.Quests.Models.api.Extensions;
using NanoReality.Game.FoodSupply;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilites;
using NanoReality.GameLogic.Utilities;
using UnityEngine;

namespace GameLogic.Quests.Models.impl.Extensions
{
    public class FoodSupplyFillBarCondition : Condition, IFoodSupplyFillBarCondition
    {
        private int _achievedFillBarProgress;
        public int RequiredFillBarProgress { get; set; }
        
        public override string Description =>
            LocalizationUtils.SafeFormat(base.Description, 
                Mathf.Clamp(_achievedFillBarProgress, 0, RequiredFillBarProgress).ToString(),
                RequiredFillBarProgress.ToString());
        
        [Inject] public IFoodSupplyService jFoodSupplyService { get; set; }
        
        public override void InitServerBalanceData()
        {
            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out int value))
            {
                RequiredFillBarProgress = value;
            }
        }

        public override void InitServerStatesData()
        {
            base.InitServerStatesData();
            
            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out int value))
            {
                _achievedFillBarProgress = value;
            }
        }
        
        public override void InitAchievedStateData()
        {
            base.InitAchievedStateData();

            _achievedFillBarProgress = RequiredFillBarProgress;
        }
        
        public override Requirement GetConditionRequirement()
        {
            return new Requirement(0, RequiredFillBarProgress, _achievedFillBarProgress);
        }

        public override void StartEvaluateCondition()
        {
            if (AchievementCheck())
            {
                IsAchived = true;
            }
            else
            {
                jFoodSupplyService.FoodSupplyAmountChanged += OnFoodSupplyAmountChanged;
            }
        }

        public override void InitLocalStateData()
        {
        }
        
        public override void InitFromPrototype(ICondition condition)
        {
            base.InitFromPrototype(condition);
            var castedConditions = (IFoodSupplyFillBarCondition) condition;
            RequiredFillBarProgress = castedConditions.RequiredFillBarProgress;
        }

        protected override ICondition GetNewConditionInstance => jInjectionBinder.GetInstance<IFoodSupplyFillBarCondition>();

        protected override void CopyMembers(ICondition target)
        {
            base.CopyMembers(target);
            var castedTarget = (IFoodSupplyFillBarCondition) target;
            castedTarget.RequiredFillBarProgress = RequiredFillBarProgress;
        }

        private bool AchievementCheck()
        {
            return _achievedFillBarProgress >= RequiredFillBarProgress;
        }
        
        private void OnFoodSupplyAmountChanged(float amount)
        {
            var currentFillBarProgress = Mathf.CeilToInt(amount * 100);
            if (currentFillBarProgress > _achievedFillBarProgress)
            {
                _achievedFillBarProgress = currentFillBarProgress;
            }
            
            if (AchievementCheck())
            {
                IsAchived = true;
                jFoodSupplyService.FoodSupplyAmountChanged -= OnFoodSupplyAmountChanged;
            }
        }
    }
}
