﻿using System;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilities;

/// <summary>
/// Условие общего количества проданных лотов на аукционе
/// </summary>
public class AuctionSalesCondition : Condition, IAuctionSalesCondition
{
    [Inject] public AchievementConditionUpdateSignal jAchievementConditionUpdateSignal { get; set; }

    public int SalesCount { get; set; }

    private int _salesCount;

    public override void InitServerBalanceData()
    {
        int countToAchieve;
        if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out countToAchieve))
        {
            SalesCount = countToAchieve;
        }
    }

    public override void InitServerStatesData()
    {
        base.InitServerStatesData();
        int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out _salesCount);
        if(_salesCount > SalesCount)
            _salesCount = SalesCount;
    }

    public override void InitAchievedStateData()
    {
        base.InitAchievedStateData();

        _salesCount = SalesCount;
    }

    public override void InitFromPrototype(ICondition condition)
    {
        base.InitFromPrototype(condition);
        var prot = condition as IAuctionSalesCondition;

        if (prot != null)
            SalesCount = prot.SalesCount;
    }

    protected override ICondition GetNewConditionInstance { get { return jInjectionBinder.GetInstance<IAuctionSalesCondition>(); } }

    protected override void CopyMembers(ICondition target)
    {
        base.CopyMembers(target);
        var prot = target as IAuctionSalesCondition;
        if (prot == null) return;
        prot.SalesCount = SalesCount;
    }

    public override Requirement GetConditionRequirement()
    {
        return new Requirement(0, SalesCount, _salesCount);
    }

    public override void StartEvaluateCondition()
    {
        throw new NotImplementedException();
    }

	private void CheckForUnlock()
    {
	    if (_salesCount >= SalesCount)
	    {
	        _salesCount = SalesCount;
	        IsAchived = true;
	    }
    }

    public override void InitLocalStateData()
    {
        _salesCount = CityStatistics.AuctionSellItemsCount;
        CheckForUnlock();
    }
}