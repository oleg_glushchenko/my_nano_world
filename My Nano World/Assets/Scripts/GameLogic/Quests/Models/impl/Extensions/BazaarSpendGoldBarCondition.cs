﻿using GameLogic.Bazaar.Controllers.Signals;
using GameLogic.Bazaar.Model.impl;
using GameLogic.Quests.Models.api.Extensions;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilites;
using NanoReality.GameLogic.Utilities;
using UnityEngine;

namespace GameLogic.Quests.Models.impl.Extensions
{
    public class BazaarSpendGoldBarCondition : Condition, IBazaarSpendGoldBarCondition
    {
        [Inject] public BazaarBuyProductSignal jBazaarBuyProductSignal { get; set; }
        [Inject] public BazaarBuyLootBoxSignal jBazaarBuyLootBoxSignal { get; set; }
        
        public int RequiredSpendGoldBarAmount { get; set; }
        
        public override string Description =>
            LocalizationUtils.SafeFormat(base.Description,
                Mathf.Clamp(_currentSpendGoldBarAmount, 0, RequiredSpendGoldBarAmount).ToString(),
                RequiredSpendGoldBarAmount.ToString());
        
        private int _currentSpendGoldBarAmount;
        
        public override void InitServerBalanceData()
        {
            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out int value))
            {
                RequiredSpendGoldBarAmount = value;
            }
        }

        public override void InitServerStatesData()
        {
            base.InitServerStatesData();

            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out int value))
            {
                _currentSpendGoldBarAmount = value;
            }
        }

        public override Requirement GetConditionRequirement()
        {
            return new Requirement(0, RequiredSpendGoldBarAmount, _currentSpendGoldBarAmount);
        }

        public override void StartEvaluateCondition()
        {
            if (AchievementCheck())
            {
                IsAchived = true;
            }
            else
            {
                AddListeners();
            }
        }
        
        public override void InitAchievedStateData()
        {
            base.InitAchievedStateData();

            _currentSpendGoldBarAmount = RequiredSpendGoldBarAmount;
        }

        public override void InitLocalStateData()
        {
        }
        
        public override void InitFromPrototype(ICondition condition)
        {
            base.InitFromPrototype(condition);
            var castedConditions = (IBazaarSpendGoldBarCondition) condition;
            RequiredSpendGoldBarAmount = castedConditions.RequiredSpendGoldBarAmount;
        }

        protected override ICondition GetNewConditionInstance => jInjectionBinder.GetInstance<IBazaarSpendGoldBarCondition>();

        protected override void CopyMembers(ICondition target)
        {
            base.CopyMembers(target);
            var castedTarget = (IBazaarSpendGoldBarCondition) target;
            castedTarget.RequiredSpendGoldBarAmount = RequiredSpendGoldBarAmount;
        }

        private bool AchievementCheck()
        {
            return _currentSpendGoldBarAmount >= RequiredSpendGoldBarAmount;
        }
        
        private void RemoveListeners()
        {
            jBazaarBuyProductSignal.RemoveListener(OnBazaarBuyProduct);
            jBazaarBuyLootBoxSignal.RemoveListener(OnBazaarBuyLootBox);
        }
        
        private void AddListeners()
        {
            jBazaarBuyProductSignal.AddListener(OnBazaarBuyProduct);
            jBazaarBuyLootBoxSignal.AddListener(OnBazaarBuyLootBox);
        }

        private void OnBazaarBuyLootBox(BazaarLootBox bazaarLootBox)
        {
            _currentSpendGoldBarAmount += bazaarLootBox.BoxPrice;

            if (AchievementCheck())
            {
                IsAchived = true;
                RemoveListeners();
            }
        }

        private void OnBazaarBuyProduct(SoukTopSlot soukTopSlot)
        {
            _currentSpendGoldBarAmount += soukTopSlot.Price;
            
            if (AchievementCheck())
            {
                IsAchived = true;
                RemoveListeners();
            }
        }
    }
}
