﻿using System.Linq;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.Utilites;

namespace NanoReality.GameLogic.Quests
{
    public class ConstructBuildingWithSubCategoryCondition : Condition, IConstructBuildingWithSubCategoryCondition
    {
        public BuildingSubCategories BuildingSubCategory { get; set; }
        public int RequiredCount { get; set; }
        public int CurrentNumber { get; private set; }
        public override string Description => GetDescription();


        [Inject] public SignalBuildFinishVerificated jSignalBuildFinishVerificated { get; set; }
        [Inject] public AchievementConditionUpdateSignal jAchievementConditionUpdateSignal { get; set; }
        [Inject] public IMapObjectsData jMapObjectsData { get; set; }

        public override void InitServerBalanceData()
        {
            if (int.TryParse(GetParameterValue("value_1"), out int tmp))
                BuildingSubCategory = (BuildingSubCategories) tmp;

            if (int.TryParse(GetParameterValue("value_2"), out tmp))
                Level = tmp;

            if (int.TryParse(GetParameterValue("value_3"), out tmp))
                RequiredCount = tmp;
        }

        public override void InitServerStatesData()
        {
            base.InitServerStatesData();

            if (int.TryParse(GetParameterValue("value_1"), out int tmp))
                BuildingSubCategory = (BuildingSubCategories) tmp;

            if (int.TryParse(GetParameterValue("value_2"), out tmp))
                Level = tmp;

            if (int.TryParse(GetParameterValue("value_3"), out tmp))
                CurrentNumber = tmp;
        }

        public override void InitLocalStateData()
        {
        }

        public override void InitAchievedStateData()
        {
            base.InitAchievedStateData();

            CurrentNumber = RequiredCount;
        }

        public override Requirement GetConditionRequirement()
        {
            return new Requirement((int) BuildingSubCategory, RequiredCount, CurrentNumber);
        }

        public override void StartEvaluateCondition()
        {
            if (AchievementCheck())
            {
                IsAchived = true;
            }
            else
            {
                jSignalBuildFinishVerificated.AddListener(OnBuildFinishVerificated);
            }
        }

        public override void InitFromPrototype(ICondition condition)
        {
            base.InitFromPrototype(condition);
            var castedConditions = (IConstructBuildingWithSubCategoryCondition) condition;
            RequiredCount = castedConditions.RequiredCount;
            BuildingSubCategory = castedConditions.BuildingSubCategory;
            Level = castedConditions.Level;
        }

        protected override ICondition GetNewConditionInstance => jInjectionBinder.GetInstance<IConstructBuildingWithSubCategoryCondition>();

        protected override void CopyMembers(ICondition target)
        {
            base.CopyMembers(target);
            var castedTarget = (IConstructBuildingWithSubCategoryCondition) target;
            castedTarget.RequiredCount = RequiredCount;
            castedTarget.BuildingSubCategory = BuildingSubCategory;
            castedTarget.Level = Level;
        }

        private void OnBuildFinishVerificated(IMapObject mapObject)
        {
            if (jMapObjectsData.GetSubCategoryByBuildingId(mapObject.ObjectTypeID) == BuildingSubCategory && mapObject.IsConstructed &&
                mapObject.Level == Level)
            {
                CurrentNumber++;

                if (AchievementCheck())
                {
                    IsAchived = true;
                    jSignalBuildFinishVerificated.RemoveListener(OnBuildFinishVerificated);
                }
                else
                {
                    jAchievementConditionUpdateSignal.Dispatch();
                }
            }
        }

        private bool AchievementCheck()
        {
            return CurrentNumber >= RequiredCount;
        }

        private string GetDescription()
        {
            int conditionTextType = Level < 2 ? 0 : 1;

            var baseDescription =
                LocalizationUtils.GetQuestsConditionText((int) ConditionType, conditionTextType);
            
           return LocalizationUtils.SafeFormat(baseDescription,
                CurrentNumber > RequiredCount ? RequiredCount : CurrentNumber,
                RequiredCount.ToString(),
                LocalizationUtils.GetMapObjectSubCategoryKey(BuildingSubCategory), Level);
        }
    }
}