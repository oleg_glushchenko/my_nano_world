﻿using System.Linq;
using GameLogic.Quests.Models.api.Extensions;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilites;

namespace GameLogic.Quests.Models.impl.Extensions
{
    public class BuildingHaveWithSubCategoryCondition : Condition, IBuildingHaveWithSubCategoryCondition
    {
        public BuildingSubCategories BuildingSubCategory { get; set; }
        public int RequiredCount { get; set; }
        public int CurrentNumber { get; private set; }

        public override string Description => GetDescription();

        [Inject] public SignalBuildFinishVerificated jSignalBuildFinishVerificated { get; set; }
        [Inject] public AchievementConditionUpdateSignal jAchievementConditionUpdateSignal { get; set; }
        [Inject] public IMapObjectsData jMapObjectsData { get; set; }

        public override void InitServerBalanceData()
        {
            if (int.TryParse(GetParameterValue("value_1"), out int tmp))
                BuildingSubCategory = (BuildingSubCategories) tmp;

            if (int.TryParse(GetParameterValue("value_2"), out tmp))
                Level = tmp;

            if (int.TryParse(GetParameterValue("value_3"), out tmp))
                RequiredCount = tmp;
        }

        public override void InitServerStatesData()
        {
            base.InitServerStatesData();

            if (int.TryParse(GetParameterValue("value_1"), out int tmp))
                BuildingSubCategory = (BuildingSubCategories) tmp;

            if (int.TryParse(GetParameterValue("value_2"), out tmp))
                Level = tmp;

            if (int.TryParse(GetParameterValue("value_3"), out tmp))
                CurrentNumber = tmp;
        }

        public override void InitLocalStateData()
        {
        }

        public override void InitAchievedStateData()
        {
            base.InitAchievedStateData();

            CurrentNumber = RequiredCount;
        }

        public override Requirement GetConditionRequirement()
        {
            return new Requirement((int) BuildingSubCategory, RequiredCount, CurrentNumber);
        }

        public override void StartEvaluateCondition()
        {
            RecalculateCurrentCount();
            if (AchievementCheck())
            {
                IsAchived = true;
            }
            else
            {
                jSignalBuildFinishVerificated.AddListener(OnBuildFinishVerificated);
            }
        }

        public override void InitFromPrototype(ICondition condition)
        {
            base.InitFromPrototype(condition);
            var castedConditions = (IBuildingHaveWithSubCategoryCondition) condition;
            RequiredCount = castedConditions.RequiredCount;
            BuildingSubCategory = castedConditions.BuildingSubCategory;
            Level = castedConditions.Level;
        }

        protected override ICondition GetNewConditionInstance => jInjectionBinder.GetInstance<IBuildingHaveWithSubCategoryCondition>();

        protected override void CopyMembers(ICondition target)
        {
            base.CopyMembers(target);
            var castedTarget = (IBuildingHaveWithSubCategoryCondition) target;
            castedTarget.RequiredCount = RequiredCount;
            castedTarget.BuildingSubCategory = BuildingSubCategory;
            castedTarget.Level = Level;
        }

        private void OnBuildFinishVerificated(IMapObject mapObject)
        {
            if (!mapObject.IsConstructed || jMapObjectsData.GetSubCategoryByBuildingId(mapObject.ObjectTypeID) != BuildingSubCategory )
            {
                return;
            }
            
            RecalculateCurrentCount();
                
            if (AchievementCheck())
            {
                IsAchived = true;
                jSignalBuildFinishVerificated.RemoveListener(OnBuildFinishVerificated);
            }
            else
            {
                jAchievementConditionUpdateSignal.Dispatch();
            }
        }

        private bool AchievementCheck()
        {
            return CurrentNumber >= RequiredCount;
        }

        private void RecalculateCurrentCount()
        {
            CurrentNumber = 0;

            foreach (ICityMap map in jPlayerProfile.UserCity.CityMaps)
            {
                int count = map.MapObjects.Count(b =>
                    jMapObjectsData.GetSubCategoryByBuildingId(b.ObjectTypeID) == BuildingSubCategory && b.Level >= Level && b.IsConstructed);
                CurrentNumber += count;
            }
        }
        
        private string GetDescription()
        {
            return LocalizationUtils.SafeFormat(base.Description,
                CurrentNumber > RequiredCount ? RequiredCount : CurrentNumber,
                RequiredCount.ToString(),
                LocalizationUtils.GetMapObjectSubCategoryKey(BuildingSubCategory),
                Level);
        }
    }
}