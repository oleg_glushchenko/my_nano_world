﻿using System;
using System.Collections.Generic;
using GameLogic.BuildingSystem.CityAoe;
using GameLogic.Quests.Models.api.Extensions;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilites;
using NanoReality.GameLogic.Utilities;
using UnityEngine;

namespace GameLogic.Quests.Models.impl.Extensions
{
    public class CoverAmountBuildingsByServiceCondition : Condition, ICoverAmountBuildingsByServiceCondition
    {
        private int _coveredBuildings;
        
        public MapObjectintTypes BuildingTypeId { get; set; }
        public MapObjectintTypes ServiceType { get; set; }
        public int RequiredCoveredBuildingAmount { get; set; }
        
        public override string Description =>
            LocalizationUtils.SafeFormat(base.Description,
                Mathf.Clamp(_coveredBuildings, 0, RequiredCoveredBuildingAmount).ToString(),
                RequiredCoveredBuildingAmount.ToString(),
                LocalizationUtils.GetMapObjectCategoryText(BuildingTypeId),
                LocalizationUtils.GetMapObjectTypeText(ServiceType));
        
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public ICityAoeService jCityAoeService { get; set; }
        
        public override void InitServerBalanceData()
        {
            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out int buildingId))
            {
                BuildingTypeId = (MapObjectintTypes)buildingId;
            }

            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_2"), out int coverBuildingType))
            {
                if (coverBuildingType == 4)
                {
                    ServiceType = MapObjectintTypes.PowerPlant;
                }
                if (coverBuildingType == 3)
                    ServiceType = MapObjectintTypes.School;
                else if(coverBuildingType == 2)
                {
                    ServiceType = MapObjectintTypes.Police;
                } else if (coverBuildingType == 1)
                {
                    ServiceType = MapObjectintTypes.Hospital;
                }
            }

            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_3"), out int coverBuildingAmount))
            {
                RequiredCoveredBuildingAmount = coverBuildingAmount;
            }
        }
    
        public override void InitServerStatesData()
        {
            base.InitServerStatesData();

            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out int buildingId))
            {
                BuildingTypeId = (MapObjectintTypes)buildingId;
            }

            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_2"), out int coverBuildingType))
            {
                if (coverBuildingType == 4)
                {
                    ServiceType = MapObjectintTypes.PowerPlant;
                }
                if (coverBuildingType == 3)
                    ServiceType = MapObjectintTypes.School;
                else if(coverBuildingType == 2)
                {
                    ServiceType = MapObjectintTypes.Police;
                } else if (coverBuildingType == 1)
                {
                    ServiceType = MapObjectintTypes.Hospital;
                }
            }

            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_3"), out int coveredBuildings))
            {
                _coveredBuildings = coveredBuildings;
            }
        }

        public override Requirement GetConditionRequirement()
        {
            return new Requirement((int)ServiceType, RequiredCoveredBuildingAmount, _coveredBuildings);
        }

        public override void StartEvaluateCondition()
        {
            if (AchievementCheck())
            {
                IsAchived = true;
            }
            else
            {
                AddListeners();
            }
        }

        public int ServiceObjectTypeId()
        {
            List<IMapObject> mapObjects = jGameManager.FindAllMapObjectsOnMapByType(SectorsType, ServiceType);
            if (mapObjects.Count > 0)
            {
                return mapObjects[0].ObjectTypeID;
            }

            return 0;
        }
        
        public override void InitAchievedStateData()
        {
            base.InitAchievedStateData();

            _coveredBuildings = RequiredCoveredBuildingAmount;
        }

        public override void InitLocalStateData()
        {
        }
        
        public override void InitFromPrototype(ICondition condition)
        {
            base.InitFromPrototype(condition);
            var castedConditions = (ICoverAmountBuildingsByServiceCondition) condition;
            BuildingTypeId = castedConditions.BuildingTypeId;
            ServiceType = castedConditions.ServiceType;
            RequiredCoveredBuildingAmount = castedConditions.RequiredCoveredBuildingAmount;
        }
        
        protected override ICondition GetNewConditionInstance => jInjectionBinder.GetInstance<ICoverAmountBuildingsByServiceCondition>();
        
        protected override void CopyMembers(ICondition target)
        {
            base.CopyMembers(target);
            var castedTarget = (ICoverAmountBuildingsByServiceCondition) target;
            castedTarget.ServiceType = ServiceType;
            castedTarget.BuildingTypeId = BuildingTypeId;
            castedTarget.RequiredCoveredBuildingAmount = RequiredCoveredBuildingAmount;
        }

        private bool AchievementCheck()
        {
            List<IMapObject> mapObjects = jGameManager.FindAllMapObjectsOnMapByType(SectorsType, ServiceType);
            
            if (mapObjects == null) return false;

            bool isCovered = false;
            foreach (var mapObject in mapObjects)
            {
                switch (ServiceType)
                {
                    case MapObjectintTypes.PowerPlant:
                    {
                        isCovered = RequiredCoveringCheck(NonDwellingsCoverageCheck, mapObject);
                        break;
                    }
                    case MapObjectintTypes.Police:
                    case MapObjectintTypes.Hospital:
                    case MapObjectintTypes.School:
                    {
                        isCovered = RequiredCoveringCheck(DwellingsCoverageCheck, mapObject);
                        break;
                    }
                }

                if (isCovered)
                    break;
            }

            return isCovered;
        }

        private bool RequiredCoveringCheck(Func<IMapBuilding, bool> coverageCheck, IMapObject requiredMapObject )
        {
            IAoeBuilding serviceMapObject = (IAoeBuilding) requiredMapObject;
            _coveredBuildings = 0;
            
            if (!serviceMapObject.IsFunctional())
            {
                return false;
            }

            if (serviceMapObject.AoeConnected != null)
            {
                // TODO: need optimize list enumerate.
                List<int> aoeConnected = serviceMapObject.AoeConnected;
                for (int i = 0; i < aoeConnected.Count; i++)
                {
                    int affectedBuildingId = aoeConnected[i];
                    var mapBuilding = jGameManager.FindMapObjectById<IMapBuilding>(affectedBuildingId);

                    if (coverageCheck(mapBuilding))
                    {
                        _coveredBuildings++;
                    }
                }
            }
            
            return _coveredBuildings >= RequiredCoveredBuildingAmount;
        }

        private bool DwellingsCoverageCheck(IMapBuilding mapBuilding)
        {
            return NonDwellingsCoverageCheck(mapBuilding) &&
                   mapBuilding.Level > 0;
        }
        
        private bool NonDwellingsCoverageCheck(IMapBuilding mapBuilding)
        {
            return mapBuilding.MapObjectType == BuildingTypeId &&
                   mapBuilding.IsFunctional();
        }

        private void AddListeners()
        {
            jCityAoeService.OnCityAoeDataReceived += OnCityAoeDataReceived;
        }

        private void RemoveListeners()
        {
            jCityAoeService.OnCityAoeDataReceived -= OnCityAoeDataReceived;
        }

        private void OnCityAoeDataReceived(CityAoeData obj)
        {
            if (AchievementCheck())
            {
                IsAchived = true;
                RemoveListeners();
            }
        }
    }
}
