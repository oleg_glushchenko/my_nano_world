﻿using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.StartApplicationSequence.Controllers.Commands;
using NanoReality.GameLogic.Utilites;
using NanoReality.GameLogic.Utilities;

public class InviteFriendsCondition : Condition, IInviteFriendsCondition
{
	public int InvitesCount { get; set; }

    [Inject] public OnFriendInvitedSignal jOnFriendInvitedSignal  { get; set; }

	[Inject] public AchievementConditionUpdateSignal jAchievementConditionUpdateSignal { get; set; }
    
	private int _invitesCount;
    
    public override string Description
    {
        get { return LocalizationUtils.SafeFormat(base.Description, _invitesCount, InvitesCount); }
    }

    public override void InitServerBalanceData()
    {
        int countToAchieve;
        if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out countToAchieve))
        {
            InvitesCount = countToAchieve;
        }
    }

    public override void InitServerStatesData()
    {
        base.InitServerStatesData();
        int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out _invitesCount);
    }

    public override void InitAchievedStateData()
    {
        base.InitAchievedStateData();

        _invitesCount = InvitesCount;
    }

    public override void InitFromPrototype(ICondition condition)
    {
        base.InitFromPrototype(condition);
        var prot = condition as IInviteFriendsCondition;

        if (prot != null)
        {
            InvitesCount = prot.InvitesCount;
        }
    }

    protected override ICondition GetNewConditionInstance { get { return jInjectionBinder.GetInstance<IInviteFriendsCondition>(); } }

    protected override void CopyMembers(ICondition target)
    {
        base.CopyMembers(target);
        var prot = target as IInviteFriendsCondition;
        if (prot == null) return;
        prot.InvitesCount = InvitesCount;
    }


    public override Requirement GetConditionRequirement()
    {
        return new Requirement(0, InvitesCount, _invitesCount);
    }

    public override void StartEvaluateCondition()
    {
        jOnFriendInvitedSignal.AddListener(InviteComplete);
    }

    private void InviteComplete()
    {
        _invitesCount++;
        
        if (CheckForUnlock())
        {
            jAchievementConditionUpdateSignal.Dispatch();
        }
    }

    private bool CheckForUnlock()
    {
        if (_invitesCount >= InvitesCount)
        {
            _invitesCount = InvitesCount;
            IsAchived = true;
            jOnFriendInvitedSignal.RemoveListener(InviteComplete);
			return true;
        }
		return false;
    }

    public override void InitLocalStateData()
    {
        _invitesCount = CityStatistics.SocialFriendsInvites;
        CheckForUnlock();
    }
}
