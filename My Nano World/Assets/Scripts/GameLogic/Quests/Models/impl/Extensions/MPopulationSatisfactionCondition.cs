﻿using NanoReality.GameLogic.BuildingSystem.Happiness;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.ServerCommunication.Controllers;
using NanoReality.GameLogic.Utilites;
using NanoReality.GameLogic.Utilities;

/// <summary>
/// Условие удовлетворенности населения с значением N
/// </summary>
public class MPopulationSatisfactionCondition : Condition, IPopulationSatisfactionCondition
{
    [Inject]
    public UpdatePopulationSatisfactionSignal jUpdatePopulationSatisfactionSignal { get; set; }

    [Inject]
    public IHappinessData jHappinessData { get; set; }

    public int SatisfyLevel { get; set; }

    public override string Description
    {
        get { return LocalizationUtils.SafeFormat(base.Description, 
            jHappinessData.TotalHappinessPercent >= SatisfyLevel || IsAchived
            ? SatisfyLevel
            : jHappinessData.TotalHappinessPercent, 
            SatisfyLevel); }
    }

    public override void InitServerBalanceData()
    {
        int satisfyLevel;
        if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out satisfyLevel))
        {
            SatisfyLevel = satisfyLevel;
        }
    }

    public override void InitFromPrototype(ICondition condition)
    {
        base.InitFromPrototype(condition);
        var goldProt = condition as IPopulationSatisfactionCondition;

        if (goldProt != null)
            SatisfyLevel = goldProt.SatisfyLevel;
    }

    /// <summary>
    /// Возвращает новый инстанс кондишена
    /// </summary>
    protected override ICondition GetNewConditionInstance { get { return jInjectionBinder.GetInstance<IPopulationSatisfactionCondition>(); } }

    /// <summary>
    /// Копирует переменные в копию объекта
    /// </summary>
    /// <param name="target"></param>
    protected override void CopyMembers(ICondition target)
    {
        base.CopyMembers(target);
        var goldProt = target as IPopulationSatisfactionCondition;
        if (goldProt == null) return;
        goldProt.SatisfyLevel = SatisfyLevel;
    }

    public override Requirement GetConditionRequirement()
    {
        return new Requirement(0, SatisfyLevel,
            jHappinessData.TotalHappinessPercent >= SatisfyLevel || IsAchived
                ? SatisfyLevel
                : jHappinessData.TotalHappinessPercent);
    }

    private void OnSatisfyChanged(IHappinessData happiness)
    {
        if (jHappinessData.TotalHappinessPercent >= SatisfyLevel || IsAchived)
        {
            IsAchived = true;
            jUpdatePopulationSatisfactionSignal.RemoveListener(OnSatisfyChanged);
        }
    }

    /// <summary>
    /// Входая точка вычисления условия
    /// </summary>
    public override void StartEvaluateCondition()
    {
        jUpdatePopulationSatisfactionSignal.AddListener(OnSatisfyChanged);
        OnSatisfyChanged(null);
    }

    public override void InitLocalStateData()
    {
        
    }
}