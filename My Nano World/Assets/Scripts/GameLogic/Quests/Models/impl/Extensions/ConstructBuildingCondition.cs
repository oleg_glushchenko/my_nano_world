﻿using System.Linq;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Utilites;
using NanoReality.GameLogic.Utilities;

namespace NanoReality.GameLogic.Quests
{
    public class ConstructBuildingCondition : Condition, IBuildBuildingsCondition
    {
        public Id_IMapObjectType BuildingId { get; set; }
        
        public int Amount { get; set; }
        
        public int CurrentCount { get; private set; }
        
        [Inject] public IMapObjectsData jMapObjectsData { get; set; }
        [Inject] public AchievementConditionUpdateSignal jAchievementConditionUpdateSignal { get; set; }
        [Inject] public SignalBuildFinishVerificated jSignalBuildFinishVerificated { get; set; }

        public override string Description
        {
            get
            {
                return LocalizationUtils.SafeFormat(
                    base.Description,
                    CurrentCount.ToString(),
                    Amount.ToString(),
                    LocalizationUtils.GetBuildingText(BuildingId),
                    (Level + jMapObjectsData.GetByBuildingIdAndLevel(BuildingId, Level).MapObjectType == MapObjectintTypes.DwellingHouse ? 0 : 1).ToString());
            }
        }

        public override void InitServerBalanceData()
        {
            int tmp;
            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out tmp))
                BuildingId = tmp;
            
            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_2"), out tmp))
                Level = tmp;


            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_3"), out tmp))
                Amount = tmp;
            
        }

        public override void InitServerStatesData()
        {
            base.InitServerStatesData();
            int tmp;
            if(int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_3"), out tmp))
            {
                CurrentCount = tmp;
            }
        }

        public override void InitAchievedStateData()
        {
            base.InitAchievedStateData();

            CurrentCount = Amount;
        }

        public override void InitFromPrototype(ICondition condition)
        {
            base.InitFromPrototype(condition);
            var buildingProt = condition as IBuildBuildingsCondition;
            if (buildingProt != null)
            {
                BuildingId = buildingProt.BuildingId;
                Level = buildingProt.Level;
                Amount = buildingProt.Amount;
            }
        }
        
        protected override ICondition GetNewConditionInstance { get { return jInjectionBinder.GetInstance<IBuildBuildingsCondition>(); } }

        protected override void CopyMembers(ICondition target)
        {
            base.CopyMembers(target);
            var buildCond = target as IBuildBuildingsCondition;
            if (buildCond == null) return;
            buildCond.BuildingId = BuildingId;
            buildCond.Level = Level;
            buildCond.Amount = Amount;
        }

        public override Requirement GetConditionRequirement()
        {
            return new Requirement(BuildingId.Value, Amount, CurrentCount);
        }
        
        public override void StartEvaluateCondition()
        {
            InitLocalStateData();
            jSignalBuildFinishVerificated.AddListener(OnBuildFinishVerificated);
        }
        
        private void OnBuildFinishVerificated(IMapObject mapObject)
        {
            RecalculateCurrentCount();
            if (mapObject.ObjectTypeID == BuildingId && mapObject.Level >= Level)
            {
                CurrentCount++;
				if (!CheckForAchiev())
					jAchievementConditionUpdateSignal.Dispatch(); 
            }
        }

        private bool CheckForAchiev()
        {
            if (CurrentCount >= Amount)
            {
                CurrentCount = Amount;
                IsAchived = true;
                jSignalBuildFinishVerificated.RemoveListener(OnBuildFinishVerificated);
				return true;
            }
			return false;
        }

        public override void InitLocalStateData()
        {
            RecalculateCurrentCount();
            CheckForAchiev();
        }

        private void RecalculateCurrentCount()
        {
            CurrentCount = 0;
            foreach (ICityMap map in jPlayerProfile.UserCity.CityMaps)
            {
                int count = map.MapObjects.Count(b => b.ObjectTypeID == BuildingId && b.Level >= Level && b.IsConstructed);
                CurrentCount += count;
            }
        }
    }
}
