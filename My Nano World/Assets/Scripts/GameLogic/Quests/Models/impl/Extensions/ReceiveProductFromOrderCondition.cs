﻿using Assets.NanoLib.Utilities;
using GameLogic.Quests.Models.api.Extensions;
using NanoReality.Game.Services.OrderDesk.SeaportOrderDesk;
using NanoReality.Game.UI;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.OrderDesk;
using NanoReality.GameLogic.Orders;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilites;
using NanoReality.GameLogic.Utilities;
using UnityEngine;

namespace GameLogic.Quests.Models.impl.Extensions
{
    public class ReceiveProductFromOrderCondition : Condition, IReceiveProductFromOrderCondition
    {
        [Inject] public SignalSeaportOrderSent jSignalSeaportOrderSent { get; set; }
        [Inject] public MetroOrderShippingSignal jMetroOrderShippingSignal { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        
        public int RequiredProductAmount { get; set; }
        public int RequiredProductId { get; set; }
        public BusinessSectorsTypes BusinessSector { get; set; }
        
        public override string Description =>
            LocalizationUtils.SafeFormat(base.Description, 
                Mathf.Clamp(_currentProductAmount, 0, RequiredProductAmount),
                RequiredProductAmount, 
                LocalizationUtils.GetProductText(RequiredProductId),
                LocalizationUtils.GetBuildingText(GetObjectTypeID()));
        
        private int _currentProductAmount;
        
        public override void InitServerBalanceData()
        {
            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out var value1))
            {
                BusinessSector = (BusinessSectorsTypes)value1;
            }
            
            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_2"), out int value2))
            {
                RequiredProductId = value2;
            }

            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_3"), out int value3))
            {
                RequiredProductAmount = value3;
            }
        }

        public override void InitServerStatesData()
        {
            base.InitServerStatesData();
            
            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out var value1))
            {
                BusinessSector = (BusinessSectorsTypes)value1;
            }
            
            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_2"), out int value2))
            {
                RequiredProductId = value2;
            }

            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_3"), out int value3))
            {
                _currentProductAmount = value3;
            }
        }

        public override void StartEvaluateCondition()
        {
            if (AchievementCheck())
            {
                IsAchived = true;
            }
            else
            {
                AddListeners();
            }
        }
        
        public override void InitAchievedStateData()
        {
            base.InitAchievedStateData();

            _currentProductAmount = RequiredProductAmount;
        }

        public override void InitLocalStateData() { }
        
        public override Requirement GetConditionRequirement()
        {
            return new Requirement(RequiredProductId, RequiredProductAmount, _currentProductAmount);
        }
        
        public override void InitFromPrototype(ICondition condition)
        {
            base.InitFromPrototype(condition);
            var castedConditions = (IReceiveProductFromOrderCondition) condition;
            RequiredProductId = castedConditions.RequiredProductId;
            RequiredProductAmount = castedConditions.RequiredProductAmount;
            BusinessSector = castedConditions.BusinessSector;
        }
        
        protected override ICondition GetNewConditionInstance => jInjectionBinder.GetInstance<IReceiveProductFromOrderCondition>();
        
        protected override void CopyMembers(ICondition target)
        {
            base.CopyMembers(target);
            var castedTarget = (IReceiveProductFromOrderCondition) target;
            castedTarget.RequiredProductId = RequiredProductId;
            castedTarget.RequiredProductAmount = RequiredProductAmount;
            castedTarget.BusinessSector = BusinessSector;
        }
        
        private bool AchievementCheck()
        {
            return _currentProductAmount >= RequiredProductAmount;
        }
        
        private Id_IMapObjectType GetObjectTypeID()
        {
            switch (BusinessSector)
            {
                case BusinessSectorsTypes.HeavyIndustry:
                    return jGameManager.FindMapObjectOnMapByType(BusinessSector, MapObjectintTypes.OrderDesk).ObjectTypeID;
                case BusinessSectorsTypes.Town:
                    return jGameManager.FindMapObjectOnMapByType(BusinessSector, MapObjectintTypes.CityOrderDesk).ObjectTypeID;
            }

            return -1;
        }
        
        private void SeaportOrderComplete(ISeaportOrderSlot order)
        {
            if (order.Awards.Product?.ProductId.Value == RequiredProductId)
            {
                _currentProductAmount += order.Awards.Product.Count;
                if (AchievementCheck())
                {
                    IsAchived = true;
                    RemoveListeners();
                }

                return;
            }

            if (order.Awards.Products != null)
            {
                foreach (var product in order.Awards.Products)
                {
                    if (product.ProductId.Value == RequiredProductId)
                    {
                        _currentProductAmount += product.Count;
                        if (AchievementCheck())
                        {
                            IsAchived = true;
                            RemoveListeners();
                        }
                    }
                }
            }


        }
    
        private void OnMetroOrderShipping(int trainId, NetworkMetroOrderTrain train)
        {
            if (train.Reward.ProductId != RequiredProductId)
                return;

            _currentProductAmount += train.Reward.Count;
            if (AchievementCheck())
            {
                IsAchived = true;
                RemoveListeners();
            }
        }
        
        private void AddListeners()
        {
            switch (BusinessSector)
            {
                case BusinessSectorsTypes.HeavyIndustry:
                    jSignalSeaportOrderSent.AddListener(SeaportOrderComplete);
                    break;
                case BusinessSectorsTypes.Town:
                    jMetroOrderShippingSignal.AddListener(OnMetroOrderShipping);
                    break;
            }
        }

        private void RemoveListeners()
        {
            switch (BusinessSector)
            {
                case BusinessSectorsTypes.HeavyIndustry:
                    jSignalSeaportOrderSent.RemoveListener(SeaportOrderComplete);
                    break;
                case BusinessSectorsTypes.Town:
                    jMetroOrderShippingSignal.RemoveListener(OnMetroOrderShipping);
                    break;
            }
        }
    }
}
