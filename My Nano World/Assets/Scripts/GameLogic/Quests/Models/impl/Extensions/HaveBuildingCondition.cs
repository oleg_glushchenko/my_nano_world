﻿using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.BuildingSystem.CityMap;
using Assets.Scripts.GameLogic.Utilites;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.Utilites;
using NanoReality.GameLogic.Utilities;
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace NanoReality.GameLogic.Quests
{
    /// <summary>
    /// Условие наличия построенного здания уровня L
    /// </summary>
    public class HaveBuildingCondition : Condition, IBuildingBuildCondition
    {
        /// <summary>
        /// Id здания которое должно быть построено
        /// </summary>
        public Id_IMapObjectType BuildingTypeId { get; set; }

        public bool IsInProgress { get; private set; }

        /// <summary>
        /// Ссылка на сигнал который происходит при постройке здания
        /// </summary>
        [Inject]
        public SignalBuildFinishVerificated jSignalBuildFinishVerificated { get; set; }

        [Inject]
        public BuildingConstructionStartedSignal JBuildingConstructionStartedSignal { get; set; }
        
        [Inject] public IMapObjectsData jMapObjectsData { get; set; }

        [Inject]
        public IGameManager jGameManager { get; set; }

        public override string Description
        {
            get
            {
                const string upgradeLocKey = "UPGRADE";
                var format = base.Description;
                var building = jMapObjectsData.GetByBuildingIdAndLevel(BuildingTypeId, Level);

                int adaptedLevel = AdaptMapObjectLevel(building);

                if (adaptedLevel > 1)
                {
                    format = LocalizationMLS.Instance.GetText(
                        string.Format(LocalizationKeyConstants.Objects.CONDTITION_DSCR_ALT, 
                            ((int) ConditionType).ToString(), upgradeLocKey));
                }
                
                return LocalizationUtils.SafeFormat(format, LocalizationUtils.GetBuildingText(BuildingTypeId), Level.ToString());
            }
        }

        /// <summary>
        /// Развертывает данные сериализированные с сервера, в переменные объекта
        /// </summary>
        public override void InitServerBalanceData()
        {
            int buildingId;
            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out buildingId))
            {
                BuildingTypeId = buildingId;
            }

            int level;
            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_2"), out level))
            {
                Level = level;
            }
        }

        public override void InitServerStatesData()
        {
            base.InitServerStatesData();
            
            int buildingId;
            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out buildingId))
            {
                BuildingTypeId = buildingId;
            }

            int level;
            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_2"), out level))
            {
                Level = level;
            }
        }

        public override void InitLocalStateData()
        {
        }

        /// <summary>
        /// Инициализирует данные баланса из прототипа
        /// </summary>
        /// <param name="condition"></param>
        public override void InitFromPrototype(ICondition condition)
        {
            base.InitFromPrototype(condition);
            var buildCondition = condition as HaveBuildingCondition;
            if (buildCondition == null) return; // копируем данные гейм. баланса из прототипа
            BuildingTypeId = buildCondition.BuildingTypeId;
            Level = buildCondition.Level;
        }

        /// <summary>
        /// Возвращает новый инстанс кондишена
        /// </summary>
        protected override ICondition GetNewConditionInstance { get { return jInjectionBinder.GetInstance<IBuildingBuildCondition>(); } }

        /// <summary>
        /// Копирует переменные в копию объекта
        /// </summary>
        /// <param name="target"></param>
        protected override void CopyMembers(ICondition target)
        {
            base.CopyMembers(target);
            var buildingBuildCond = target as IBuildingBuildCondition;
            if (buildingBuildCond == null) return;
            buildingBuildCond.BuildingTypeId = BuildingTypeId;
            buildingBuildCond.Level = Level;
        }
        
        public override Requirement GetConditionRequirement()
        {
            if(BuildingTypeId == null) // костыль, иногда BuildingTypeId == null
                InitServerBalanceData();

            if (TryFindBuiltMapObject(out var mapObject))
            {
                return new Requirement(BuildingTypeId.Value, Level + 1, AdaptMapObjectLevel(mapObject)+1);
            }
            else
            {
                return new Requirement(BuildingTypeId.Value, Level + 1, 1);
            }
        }
        
        /// <summary>
        /// Входая точка вычисления условия
        /// </summary>
        public override void StartEvaluateCondition()
        {
            if (!TryFindBuiltMapObject(out var mapObject))
            {
                jSignalBuildFinishVerificated.AddListener(OnBuildFinishVerificated);
                JBuildingConstructionStartedSignal.AddListener(OnMapObjectBuildStarted);
                
                return;
            }
            
            int adaptedLevel = AdaptMapObjectLevel(mapObject);

            if (mapObject.Level <= 0 && adaptedLevel >= Level)
            {
                IsInProgress = true;
                jSignalBuildFinishVerificated.AddListener(OnBuildFinishVerificated);
            } else
            if (adaptedLevel >= Level)
            {
                if (mapObject.IsConstructed)
                {
                    IsAchived = true;
                }

                IsInProgress = true;
                jSignalBuildFinishVerificated.AddListener(OnBuildFinishVerificated);
            }
            else
            {
                jSignalBuildFinishVerificated.AddListener(OnBuildFinishVerificated);
                JBuildingConstructionStartedSignal.AddListener(OnMapObjectBuildStarted);
            }
        }

        private int AdaptMapObjectLevel(IMapObject mapObject)
        {
            if (mapObject.MapObjectType != MapObjectintTypes.DwellingHouse)
                return mapObject.Level;

            return mapObject.Level == 0 ? 1 : mapObject.Level;
        }

        private void OnMapObjectBuildStarted(IMapObject mapObject)
        {
            if (mapObject.ObjectTypeID != BuildingTypeId)
                return;

            int adaptedLevel = AdaptMapObjectLevel(mapObject);
            
            if (adaptedLevel >= Level)
            {
                JBuildingConstructionStartedSignal.RemoveListener(OnMapObjectBuildStarted);
                IsInProgress = true;
            }
        }

        /// <summary>
        /// Вызывается при постройке или апгрейде здания в игре
        /// </summary>
        void OnBuildFinishVerificated(IMapObject mapObject)
        {
            if (mapObject.ObjectTypeID != BuildingTypeId)
                return;

            if (mapObject.Level >= Level)
            {
                JBuildingConstructionStartedSignal.RemoveListener(OnMapObjectBuildStarted);
                jSignalBuildFinishVerificated.RemoveListener(OnBuildFinishVerificated);
                IsAchived = true;
            }
        }

		public static bool operator ==(HaveBuildingCondition a, HaveBuildingCondition b)
		{
			// If both are null, or both are same instance, return true.
			if (ReferenceEquals(a, b))
			{
				return true;
			}

			// If one is null, but not both, return false.
			if (((object)a == null) || ((object)b == null))
			{
				return false;
			}

			// Return true if the fields match:
			return (a.BuildingTypeId == b.BuildingTypeId) && (a.Level == b.Level);
		}

		public static bool operator !=(HaveBuildingCondition a, HaveBuildingCondition b)
		{
			return !(a == b);
		}

        public override bool Equals(object obj)
        {

            var objCond = obj as HaveBuildingCondition;
            if (objCond == null) return false; // if its not current type

            // If both are null, or both are same instance, return true.
            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            // Return true if the fields match:
            return (BuildingTypeId == objCond.BuildingTypeId) && (Level == objCond.Level);
        }

        public override int GetHashCode()
        {
            return Level.GetHashCode() ^ GetType().GetHashCode() ^ (BuildingTypeId != null? BuildingTypeId.GetHashCode() : 1);
        }

        private bool TryFindBuiltMapObject(out IMapObject foundMapObject)
        {
            foreach (KeyValuePair<BusinessSectorsTypes,CityMapView> keyValue in jGameManager.CityMapViews)
            {
                CityMapView cityMapView = jGameManager.GetMapView(keyValue.Key);

                foreach (IMapObject mapObject in cityMapView.CityMapModel.MapObjects)
                {
                    if (mapObject.ObjectTypeID == BuildingTypeId)
                    {
                        foundMapObject = mapObject;
                        return true;
                    }
                }
            }

            foreach (IMapObject mapObject in jGameManager.GlobalCityMap.MapObjects)
            {
                if (mapObject.ObjectTypeID == BuildingTypeId)
                {
                    foundMapObject = mapObject;
                    return true;
                }
            }

            foundMapObject = null;
            return false;
        }
    }
}
