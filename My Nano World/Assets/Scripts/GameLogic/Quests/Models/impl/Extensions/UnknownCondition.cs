﻿using Assets.NanoLib.UtilitesEngine.CustomDebugging.Models.api;

namespace NanoReality.GameLogic.Quests
{
    public class UnknownCondition : Condition, IUnknownCondition
    {
        [Inject]
        public IDebug JDebug { get; set; }
        
        public override void InitServerBalanceData()
        {
        }

        public override Requirement GetConditionRequirement()
        {
            return new Requirement(0, 0, 0);
        }

        public override void StartEvaluateCondition()
        {
            JDebug.LogWarning(
                string.Format("UnknownCondition.StartEvaluateCondition ConditionId: {0}, ConditionType:{1}, ConditionState: {2}", 
                    ConditionId, ConditionType, ConditionState));
        }

        public override void InitLocalStateData()
        {
        }
    }
}