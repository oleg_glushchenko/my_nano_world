﻿using GameLogic.Taxes.Service;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilites;
using NanoReality.GameLogic.Utilities;


public class CollectTaxesCondition : Condition,  ICollectTaxesCondition
{
    [Inject] public AchievementConditionUpdateSignal jAchievementConditionUpdateSignal { get; set; }
    
    [Inject] public ICityTaxesService jCityTaxesService { get; set; }

    public int CollectTaxesNeed { get; set; }

    private int _collectTaxesCount;
    
    public override string Description
    {
        get { return LocalizationUtils.SafeFormat(base.Description, _collectTaxesCount, CollectTaxesNeed); }
    }

    public override void InitServerBalanceData()
    {
        int value;
        if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out value))
        {
            CollectTaxesNeed = value;
        }
    }

    public override void InitServerStatesData()
    {
        base.InitServerStatesData();
        int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out _collectTaxesCount);
        if(_collectTaxesCount > CollectTaxesNeed) //костыль на отображение текущего значения, со стороны сервера сделать не вышло
            _collectTaxesCount = CollectTaxesNeed;
    }

    public override void InitAchievedStateData()
    {
        base.InitAchievedStateData();

        _collectTaxesCount = CollectTaxesNeed;
    }

    public override void InitFromPrototype(ICondition condition)
    {
        base.InitFromPrototype(condition);
        var prot = condition as ICollectTaxesCondition;

        if (prot != null)
        {
            CollectTaxesNeed = prot.CollectTaxesNeed;
        }

    }

    protected override ICondition GetNewConditionInstance
    {
        get { return jInjectionBinder.GetInstance<ICollectTaxesCondition>(); }
    }

    protected override void CopyMembers(ICondition target)
    {
        base.CopyMembers(target);
        var prot = target as ICollectTaxesCondition;
        if (prot == null) return;
        prot.CollectTaxesNeed = CollectTaxesNeed;
    }

    public override Requirement GetConditionRequirement()
    {
        return new Requirement(0, CollectTaxesNeed, _collectTaxesCount);
    }

    public override void StartEvaluateCondition()
    {
        jCityTaxesService.TaxesCollected += OnTaxesCountCollected;
    }

    private void OnTaxesCountCollected(int coins)
    {
        _collectTaxesCount += coins;
        CheckForComplete();
    }

    private void CheckForComplete()
    {
        if (_collectTaxesCount >= CollectTaxesNeed)
        {
            _collectTaxesCount = CollectTaxesNeed;
            IsAchived = true;
            jCityTaxesService.TaxesCollected -= OnTaxesCountCollected;
        }

        jAchievementConditionUpdateSignal.Dispatch();
    }

    public override void InitLocalStateData()
    {
        _collectTaxesCount = 0;
        CheckForComplete();
    }
}