﻿using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilites;
using NanoReality.GameLogic.Utilities;


public class CertainQuestDoneCondition : Condition, ICertainQuestDoneCondition
{
    public Id<IQuest> QuestId { get; set; }

    [Inject] public IUserQuestData jUserQuestData { get; set; }
    [Inject] public AchievementConditionUpdateSignal jAchievementConditionUpdateSignal { get; set; }

    /// <summary>
    /// Вызывается когда квест выполнен
    /// </summary>
    [Inject] public SignalOnQuestAchive SignalOnQuestAchive { get; set; }
    
    public override string Description
    {
        get { return LocalizationUtils.SafeFormat(base.Description, LocalizationUtils.GetQuestText(QuestId.Value)); }
    }

    public override void InitServerBalanceData()
    {
        int questId;
        if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out questId))
            QuestId = new Id<IQuest>(questId);
    }

    #region Instance

    public override void InitFromPrototype(ICondition condition)
    {
        base.InitFromPrototype(condition);
        var prototype = condition as ICertainQuestDoneCondition;

        if (prototype != null)
            QuestId = prototype.QuestId;
    }

    protected override void CopyMembers(ICondition target)
    {
        base.CopyMembers(target);
        var prototype = target as ICertainQuestDoneCondition;
        if (prototype == null) return;
        prototype.QuestId = QuestId;
    }

    protected override ICondition GetNewConditionInstance { get { return jInjectionBinder.GetInstance<ICertainQuestDoneCondition>(); } }

    #endregion

    public override Requirement GetConditionRequirement()
    {
        return new Requirement(QuestId.Value, 1, IsAchived ? 1 : 0);
    }

    public override void StartEvaluateCondition()
    {
        SignalOnQuestAchive.AddListener(OnQuestAchive);
    }

    private void OnQuestAchive(IQuest arg1)
    {
        var certainQuest = jUserQuestData.UserQuestsData.Find(q => q.QuestId == QuestId.Value);
        IsAchived = certainQuest != null &&
                    (certainQuest.QuestState == UserQuestStates.Achived || certainQuest.QuestState == UserQuestStates.Completed);

        if (IsAchived)
        {
            SignalOnQuestAchive.RemoveListener(OnQuestAchive);
        }

        jAchievementConditionUpdateSignal.Dispatch();
    }

    public override void InitLocalStateData()
    {
        OnQuestAchive(null);
    }
}
