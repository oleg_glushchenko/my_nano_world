using NanoReality.Game.FoodSupply;
using NanoReality.GameLogic.Utilites;
using NanoReality.GameLogic.Utilities;

namespace NanoReality.GameLogic.Quests
{
	public class FoodSupplyFillingBarCondition : Condition, IFoodSupplyFillingBarCondition
	{
        [Inject] public IFoodSupplyService jFoodSupplyService { get; set; }
        
        private int _filledBarCount;

        private bool IsConditionComplete => _filledBarCount >= TargetFilledBarCount;
        
        public int TargetFilledBarCount { get; set; }

        public override string Description => GetDescription();

        public override void InitServerBalanceData()
        {
            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out var value))
            {
                TargetFilledBarCount = value;
            }
        }

        public override void InitServerStatesData()
        {
            base.InitServerStatesData();

            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out var value))
            {
                _filledBarCount = value;
            }
        }

        public override Requirement GetConditionRequirement()
        {
            return new Requirement(0, TargetFilledBarCount, _filledBarCount);
        }

        public override void StartEvaluateCondition()
        {
            if (IsConditionComplete)
            {
                IsAchived = true;
            }
            else
            {
                jFoodSupplyService.FoodSupplyBarFilled += OnProgressBarFilled;
            }
        }

        public override void InitLocalStateData()
        {
            //Ignored
        }
        
        public override void InitAchievedStateData()
        {
            base.InitAchievedStateData();

            _filledBarCount = TargetFilledBarCount;
        }
        
        public override void InitFromPrototype(ICondition condition)
        {
            base.InitFromPrototype(condition);
            
            var castedConditions = (IFoodSupplyFillingBarCondition) condition;
            TargetFilledBarCount = castedConditions.TargetFilledBarCount;
        }

        protected override ICondition GetNewConditionInstance => jInjectionBinder.GetInstance<IFoodSupplyFillingBarCondition>();

        protected override void CopyMembers(ICondition target)
        {
            base.CopyMembers(target);
            
            var castedTarget = (IFoodSupplyFillingBarCondition) target;
            castedTarget.TargetFilledBarCount = TargetFilledBarCount;
        }
        
        private void OnProgressBarFilled()
        {
            _filledBarCount++;

            if (IsConditionComplete)
            {
                IsAchived = true;
                jFoodSupplyService.FoodSupplyBarFilled -= OnProgressBarFilled;
            }
        }
        
        private string GetDescription()
        {
            int conditionTextType;
            object[] arguments;
            if (TargetFilledBarCount < 2)
            {
                conditionTextType = 0;
                arguments = new object[] {TargetFilledBarCount.ToString()};
            }
            else
            {
                conditionTextType = 1;
                arguments = new object[] {_filledBarCount, TargetFilledBarCount.ToString()};
            }

            var baseDescription = LocalizationUtils.GetQuestsConditionText((int) ConditionType, conditionTextType);
            return LocalizationUtils.SafeFormat(baseDescription, arguments);
        }
	}
}