﻿using GameLogic.Quests.Models.api.Extensions;
using I2.Loc;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilites;

namespace GameLogic.Quests.Models.impl.Extensions
{
    public class BuildingConstructByIdCondition : Condition, IBuildingConstructByIdCondition
    {
        public int BuildingId { get; set; }
        public int RequiredCount { get; set; }
        public int СurrentCount { get; private set; }

        public override string Description
        {
            get
            {
                string buildingName = LocalizationUtils.GetBuildingText(BuildingId);
                int requiredBuildingLevel = Level;

                if (jMapObjectsData.GetByBuildingIdAndLevel(BuildingId, Level).MapObjectType !=
                    MapObjectintTypes.DwellingHouse)
                {
                    requiredBuildingLevel += 1;
                }

                int conditionTextType = requiredBuildingLevel < 2 ? 0 : 1;

                var baseDescription =
                    LocalizationUtils.GetQuestsConditionText((int) ConditionType, conditionTextType);

                return LocalizationUtils.SafeFormat(baseDescription, СurrentCount,
                    RequiredCount, buildingName, requiredBuildingLevel);
            }
        }

        [Inject] public IMapObjectsData jMapObjectsData { get; set; }
        [Inject] public AchievementConditionUpdateSignal jAchievementConditionUpdateSignal { get; set; }
        [Inject] public SignalBuildFinishVerificated jSignalBuildFinishVerificated { get; set; }

        public override void InitServerBalanceData()
        {
            if (int.TryParse(GetParameterValue("value_1"), out int tmp))
                BuildingId = tmp;
            
            if (int.TryParse(GetParameterValue("value_2"), out tmp))
                Level = tmp;
            
            if (int.TryParse(GetParameterValue("value_3"), out tmp))
                RequiredCount = tmp;
        }
        
        public override void InitServerStatesData()
        {
            base.InitServerStatesData();

            if (int.TryParse(GetParameterValue("value_1"), out int tmp))
                BuildingId = tmp;
            
            if (int.TryParse(GetParameterValue("value_2"), out tmp))
                Level = tmp;
            
            if (int.TryParse(GetParameterValue("value_3"), out tmp))
                СurrentCount = tmp;
        }
        
        public override void InitLocalStateData() { }
        
        public override void InitAchievedStateData()
        {
            base.InitAchievedStateData();

            СurrentCount = RequiredCount;
        }

        public override Requirement GetConditionRequirement()
        {
            return new Requirement(BuildingId, RequiredCount, СurrentCount);
        }

        public override void StartEvaluateCondition()
        {
            if (AchievementCheck())
            {
                IsAchived = true;
            }
            else
            {
                jSignalBuildFinishVerificated.AddListener(OnBuildFinishVerificated);
            }
        }

        public override void InitFromPrototype(ICondition condition)
        {
            base.InitFromPrototype(condition);
            var castedConditions = (IBuildingConstructByIdCondition) condition;
            RequiredCount = castedConditions.RequiredCount;
            BuildingId = castedConditions.BuildingId;
            Level = castedConditions.Level;
        }

        protected override ICondition GetNewConditionInstance => jInjectionBinder.GetInstance<IBuildingConstructByIdCondition>();

        protected override void CopyMembers(ICondition target)
        {
            base.CopyMembers(target);
            var castedTarget = (IBuildingConstructByIdCondition) target;
            castedTarget.RequiredCount = RequiredCount;
            castedTarget.BuildingId = BuildingId;
            castedTarget.Level = Level;
        }
        
        private void OnBuildFinishVerificated(IMapObject mapObject)
        {
            if (mapObject.ObjectTypeID == BuildingId && mapObject.Level == Level && mapObject.IsConstructed)
            {
                СurrentCount++;
                if (AchievementCheck())
                {
                    IsAchived = true;
                    jSignalBuildFinishVerificated.RemoveListener(OnBuildFinishVerificated);
                }
                else
                {
                    jAchievementConditionUpdateSignal.Dispatch(); 
                }
            }
        }

        private bool AchievementCheck()
        {
            return СurrentCount >= RequiredCount;
        }
    }
}
