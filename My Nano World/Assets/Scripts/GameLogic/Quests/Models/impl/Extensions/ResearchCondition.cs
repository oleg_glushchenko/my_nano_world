﻿using NanoReality.GameLogic.Quests;
using UnityEngine;

// TODO: deprecated.
public class ResearchCondition : Condition, IResearchCondition
{
    public override void InitServerBalanceData()
    {

    }

    protected override ICondition GetNewConditionInstance => jInjectionBinder.GetInstance<IResearchCondition>();

    public override Requirement GetConditionRequirement()
    {
        return new Requirement(0, 0, 0);
    }

    public override void StartEvaluateCondition()
    {
        Debug.LogWarning("Condition not implemented");
    }

    public override void InitLocalStateData()
    {
        Debug.LogWarning("Condition not implemented");
    }
}