﻿using System;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilities;

/// <summary>
/// Условие общего количества купленных лотов на аукционе
/// </summary>
public class AuctionPurchaseCondition : Condition, IAuctionPurchaseCondition
{
    [Inject] public AchievementConditionUpdateSignal jAchievementConditionUpdateSignal { get; set; }

    public int PurchaseCount { get; set; }

    private int _purchaseCount;

    public override void InitServerBalanceData()
    {
        int countToAchieve;
        if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out countToAchieve))
        {
            PurchaseCount = countToAchieve;
        }
    }

    public override void InitServerStatesData()
    {
        base.InitServerStatesData();
        int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out _purchaseCount);
    }

    public override void InitAchievedStateData()
    {
        base.InitAchievedStateData();

        _purchaseCount = PurchaseCount;
    }

    public override void InitFromPrototype(ICondition condition)
    {
        base.InitFromPrototype(condition);
        var prot = condition as IAuctionPurchaseCondition;

        if (prot != null)
            PurchaseCount = prot.PurchaseCount;
    }

    protected override ICondition GetNewConditionInstance { get { return jInjectionBinder.GetInstance<IAuctionPurchaseCondition>(); } }

    protected override void CopyMembers(ICondition target)
    {
        base.CopyMembers(target);
        var prot = target as IAuctionPurchaseCondition;
        if (prot == null) return;
        prot.PurchaseCount = PurchaseCount;
    }

    public override Requirement GetConditionRequirement()
    {
        return new Requirement(0, PurchaseCount, _purchaseCount);
    }

    public override void StartEvaluateCondition()
    {
        throw new NotImplementedException();
    }

	private void CheckForUnlock()
    {
	    if (_purchaseCount >= PurchaseCount)
	    {
	        _purchaseCount = PurchaseCount;
	        IsAchived = true;
        }
    }

    public override void InitLocalStateData()
    {
        _purchaseCount = CityStatistics.AuctionPurchasedItemsCount;
        CheckForUnlock();
    }
}