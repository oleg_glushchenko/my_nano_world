﻿using Assets.NanoLib.UtilitesEngine.CustomDebugging.Models.api;
using NanoReality.GameLogic.Achievements;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilites;
using NanoReality.GameLogic.Utilities;

/// <summary>
/// Условие наличия премиум валюты. Используется для доступности квеста
/// </summary>
public class MPremiumGoldGainedCondition : Condition, IPremiumGoldGainedCondition
{
    #region Inject

	[Inject] public AchievementConditionUpdateSignal jAchievementConditionUpdateSignal { get; set; }
    [Inject] public IDebug jDebug { get; set; }


    #endregion


    #region Signals

    [Inject] public SignalOnHardCurrencyStateChanged SignalOnHardCurrencyStateChanged { get; set; }

    #endregion

    public int CountPremiumGoldToAchieve { get; set; }

    private int _currentCount;

    #region Json

    //[JsonProperty("params")]
    // JSON данные полей для разных типов условий
    // protected List<SerializebleKeyValuePair> ServerParams = new List<SerializebleKeyValuePair>();


    #endregion
    
    public override string Description
    {
        get { return LocalizationUtils.SafeFormat(base.Description, _currentCount, CountPremiumGoldToAchieve); }
    }

    public override void InitServerBalanceData()
    {
        int countPremiumGoldToAchieve;
        if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out countPremiumGoldToAchieve))
            CountPremiumGoldToAchieve = countPremiumGoldToAchieve;
        jDebug.Log("Count premium gold to achive: " + CountPremiumGoldToAchieve, true);
    }

    public override void InitAchievedStateData()
    {
        base.InitAchievedStateData();

        _currentCount = CountPremiumGoldToAchieve;
    }

    public override void InitFromPrototype(ICondition condition)
    {
        base.InitFromPrototype(condition);
        var goldProt = condition as IPremiumGoldGainedCondition;

        if (goldProt != null)
            CountPremiumGoldToAchieve = goldProt.CountPremiumGoldToAchieve;
    }

    /// <summary>
    /// Возвращает новый инстанс кондишена
    /// </summary>
    protected override ICondition GetNewConditionInstance { get { return jInjectionBinder.GetInstance<IPremiumGoldGainedCondition>(); } }

    /// <summary>
    /// Копирует переменные в копию объекта
    /// </summary>
    /// <param name="target"></param>
    protected override void CopyMembers(ICondition target)
    {
        base.CopyMembers(target);
        var goldProt = target as IPremiumGoldGainedCondition;
        if (goldProt == null) return;
        goldProt.CountPremiumGoldToAchieve = CountPremiumGoldToAchieve;
    }

    public override Requirement GetConditionRequirement()
    {
        return new Requirement(0, CountPremiumGoldToAchieve, _currentCount);
    }

    private void OnGoldChanged(int currentGold)
    {
		if (_currentCount == currentGold)
			return;
        _currentCount = currentGold;
        if (currentGold >= CountPremiumGoldToAchieve)
        {
            _currentCount = CountPremiumGoldToAchieve;
            IsAchived = true;
        }
		else
            jAchievementConditionUpdateSignal.Dispatch();

        SignalOnHardCurrencyStateChanged.RemoveListener(OnGoldChanged);
    }

    /// <summary>
    /// Входая точка вычисления условия
    /// </summary>
    public override void StartEvaluateCondition()
    {
        SignalOnHardCurrencyStateChanged.AddListener(OnGoldChanged);
    }

    public override void InitLocalStateData()
    {

    }
}
