﻿using System;
using System.Collections;
using Assets.NanoLib.UtilitesEngine;
using NanoReality.GameLogic.Services;
using NanoReality.GameLogic.Utilities;
using UnityEngine;

namespace NanoReality.GameLogic.Quests
{
    public class DateTimeCondition : Condition, IDateTimeCondition
    {
        [Inject] private ITimerManager jTimerManager { get; set; }
        
        public long Time { get; private set; }

        public long TimeLeft
        {
            get
            {
                if (jTimerManager.CurrentUTC < Time)
                {
                    return Time - jTimerManager.CurrentUTC;
                }

                return 0;
            }
        }

        public DateTimeCondition()
        {
            ConditionType = ConditionTypes.DateTime;
        }
        
        public void SetTime(long unixTimestamp)
        {
            Time = unixTimestamp;
        }
        
        public override void InitServerBalanceData()
        {
            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out int value))
            {
                Time = value;
            }
        }

        public override void InitLocalStateData()
        {
        }

        public override void InitFromPrototype(ICondition condition)
        {
            base.InitFromPrototype(condition);

            if (condition is IDateTimeCondition prototype)
            {
                Time = prototype.Time;
            }
        }

        protected override ICondition GetNewConditionInstance
        {
            get { return jInjectionBinder.GetInstance<IDateTimeCondition>(); }
        }

        protected override void CopyMembers(ICondition target)
        {
            base.CopyMembers(target);

            if (target is DateTimeCondition condition)
            {
                condition.Time = Time;
            }
        }

        public override Requirement GetConditionRequirement()
        {
            return new Requirement(0, 0, 0);
        }

        public override void StartEvaluateCondition()
        {
            if (!IsAchived)
            {
                jTimerManager.StartServerTimer(Time, () => {  IsAchived = true; }, null);
            }
        }
    }
}