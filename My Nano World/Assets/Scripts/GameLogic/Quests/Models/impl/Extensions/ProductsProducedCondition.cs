﻿using System.Collections.Generic;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.Analytics.Model;
using Assets.Scripts.Engine.UI.Extensions.QuestsBuildingPanel.ConditionLinks.Utils;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.api;
using NanoLib.Core;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Controllers.Agricultural;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilities;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api.Factories;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.Utilites;

/// <summary>
/// Условие производства новых товаров N в количестве M
/// </summary>
public class ProductsProducedCondition : Condition, IProductsProducedCondition
{
    public IntValue ProductType { get; set; }
    public int Amount { get; set; }

    public bool IsInProgress { get; private set; }

    private int _currentCount;
    
    [Inject] public AchievementConditionUpdateSignal jAchievementConditionUpdateSignal { get; set; }

    [Inject] public OnProductProduceAndCollectSignal jOnProductProduceAndCollectSignal { get; set; }
    
    [Inject] public SignalOnStartProduceGoodOnFactory jSignalOnStartProduceGoodOnFactory { get; set; }
    [Inject] public IGameManager jGameManager { get; set; }

    public override string Description
    {
        get { return LocalizationUtils.SafeFormat(base.Description, _currentCount, Amount, LocalizationUtils.GetProductText(ProductType)); }
    }

    public override void InitServerBalanceData()
    {
        int tmp;
        if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out tmp))
            ProductType = tmp;
        if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_2"), out tmp))
            Amount = tmp;
    }

    public override void InitServerStatesData()
    {
        base.InitServerStatesData();
        int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_2"), out _currentCount);

        if (_currentCount >= Amount)
        {
            _currentCount = Amount;
        }
    }

    public override void InitFromPrototype(ICondition condition)
    {
        base.InitFromPrototype(condition);
        var productsProducedConditionPrototype = condition as IProductsProducedCondition;

        if (productsProducedConditionPrototype != null)
        {
            ProductType = productsProducedConditionPrototype.ProductType;
            Amount = productsProducedConditionPrototype.Amount;
        }
    }

    /// <summary>
    /// Возвращает новый инстанс кондишена, с помощью
    /// </summary>
    protected override ICondition GetNewConditionInstance { get { return jInjectionBinder.GetInstance<IProductsProducedCondition>(); } }

    /// <summary>
    /// Копирует переменные в копию объекта
    /// </summary>
    /// <param name="target"></param>
    protected override void CopyMembers(ICondition target)
    {
        base.CopyMembers(target);
        var productsProducedCondition = target as IProductsProducedCondition;
        if (productsProducedCondition == null) return;
        productsProducedCondition.ProductType = ProductType;
        productsProducedCondition.Amount = Amount;
    }

    [Inject]
    public IProductsData ProductsData { get; set; }

    

    public override Requirement GetConditionRequirement()
    {
        return new Requirement(ProductType.Value, Amount, _currentCount);
    }

    public override void StartEvaluateCondition()
    {
        jOnProductProduceAndCollectSignal.AddListener(OnProductsUpdated);
        jSignalOnStartProduceGoodOnFactory.AddListener(OnStartProduceGoodOnFactory);

        if (_currentCount > 0 || ProductProduceUtils.FindProductInProgress(jGameManager, ProductType.Value))
        {
            SetInProgressState();    
        }        
    }
    
    public override void InitAchievedStateData()
    {
        base.InitAchievedStateData();

        _currentCount = Amount;
    }
    
    private void OnStartProduceGoodOnFactory(ProductionStartInfo obj)
    {
        CheckIfInProgress(obj.ProducingItemData);
    }

    private void CheckIfInProgress(IProducingItemData itemData)
    {
        if (ProductType == itemData.OutcomingProducts)
        {
            SetInProgressState();
        }
    }

    public void OnProductsUpdatedDict(Dictionary<Id<Product>, int> products)
    {
        CheckIsAchieved();
    }

	public void OnProductsUpdated(IMapObject mapObject, Id<Product> productID, int count)
    {
        if (ProductType == productID.Value)
        {
			if (count == 0)
				return;
            _currentCount += count;
			if (!CheckIsAchieved())
				jAchievementConditionUpdateSignal.Dispatch();
        }

    }

    public bool CheckIsAchieved()
    {
        if (_currentCount >= Amount)
        {
            IsAchived = true;
            _currentCount = Amount;
            jOnProductProduceAndCollectSignal.RemoveListener(OnProductsUpdated);
			return true;
        }
		return false;
    }

    public override void InitLocalStateData()
    {
		CityStatistics.ProducedProducts.TryGetValue (ProductType.Value, out _currentCount);
        CheckIsAchieved();
    }

    private void SetInProgressState()
    {
        jSignalOnStartProduceGoodOnFactory.RemoveListener(OnStartProduceGoodOnFactory);
        IsInProgress = true;
    }
    
}
