﻿using GameLogic.Bazaar.Controllers.Signals;
using GameLogic.Bazaar.Model.impl;
using GameLogic.Quests.Models.api.Extensions;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilites;
using NanoReality.GameLogic.Utilities;
using UnityEngine;

namespace GameLogic.Quests.Models.impl.Extensions
{
    public class BazaarPurchaseLootBoxCondition : Condition, IBazaarPurchaseLootBoxCondition
    {
        [Inject] public BazaarBuyLootBoxSignal jBazaarBuyLootBoxSignal { get; set; }
        
        public int RequiredLootBoxesCount { get; set; }
        
        public override string Description => LocalizationUtils.SafeFormat(base.Description, 
            Mathf.Clamp(_currentLootBoxesCount, 0, RequiredLootBoxesCount).ToString(),
            RequiredLootBoxesCount.ToString());
        
        private int _currentLootBoxesCount;
        
        public override void InitServerBalanceData()
        {
            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out int value))
            {
                RequiredLootBoxesCount = value;
            }
        }

        public override void InitServerStatesData()
        {
            base.InitServerStatesData();

            if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out int value))
            {
                _currentLootBoxesCount = value;
            }
        }

        public override Requirement GetConditionRequirement()
        {
            return new Requirement(0, RequiredLootBoxesCount, _currentLootBoxesCount);
        }

        public override void StartEvaluateCondition()
        {
            if (AchievementCheck())
            {
                IsAchived = true;
            }
            else
            {
                AddListeners();
            }
        }
        
        public override void InitAchievedStateData()
        {
            base.InitAchievedStateData();

            _currentLootBoxesCount = RequiredLootBoxesCount;
        }

        public override void InitLocalStateData()
        {
        }
        
        public override void InitFromPrototype(ICondition condition)
        {
            base.InitFromPrototype(condition);
            var castedConditions = (IBazaarPurchaseLootBoxCondition) condition;
            RequiredLootBoxesCount = castedConditions.RequiredLootBoxesCount;
        }

        protected override ICondition GetNewConditionInstance => jInjectionBinder.GetInstance<IBazaarPurchaseLootBoxCondition>();

        protected override void CopyMembers(ICondition target)
        {
            base.CopyMembers(target);
            var castedTarget = (IBazaarPurchaseLootBoxCondition) target;
            castedTarget.RequiredLootBoxesCount = RequiredLootBoxesCount;
        }

        private bool AchievementCheck()
        {
            return _currentLootBoxesCount >= RequiredLootBoxesCount;
        }
        
        private void RemoveListeners()
        {
            jBazaarBuyLootBoxSignal.RemoveListener(OnBazaarBuyLootBox);
        }
        
        private void AddListeners()
        {
            jBazaarBuyLootBoxSignal.AddListener(OnBazaarBuyLootBox);
        }

        private void OnBazaarBuyLootBox(BazaarLootBox bazaarLootBox)
        {
            _currentLootBoxesCount++;
            if (AchievementCheck())
            {
                IsAchived = true;
                RemoveListeners();
            }
        }
    }
}
