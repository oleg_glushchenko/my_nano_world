﻿using System.Linq;
using GameLogic.Quests.Models.api.Extensions;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilites;

namespace GameLogic.Quests.Models.impl.Extensions
{
    public class BuildingHaveByIdCondition : Condition, IBuildingHaveByIdCondition
    {
        public int BuildingId { get; set; }
        public int RequiredNumber { get; set; }
        public int CurrentNumber { get; private set; }
        
        public override string Description => GetDescription();


        private string GetDescription()
        {
            int conditionTextType = Level < 2 ? 0 : 1;

            var baseDescription =
                LocalizationUtils.GetQuestsConditionText((int) ConditionType, conditionTextType);

            return LocalizationUtils.SafeFormat(
                baseDescription,
                CurrentNumber.ToString(),
                RequiredNumber.ToString(),
                LocalizationUtils.GetBuildingText(BuildingId),
                (Level + (jMapObjectsData.GetByBuildingIdAndLevel(BuildingId, Level).MapObjectType == MapObjectintTypes.DwellingHouse ? 0 : 1)).ToString());
        }

        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public IMapObjectsData jMapObjectsData { get; set; }
        [Inject] public AchievementConditionUpdateSignal jAchievementConditionUpdateSignal { get; set; }
        [Inject] public SignalBuildFinishVerificated jSignalBuildFinishVerificated { get; set; }

        public override void InitServerBalanceData()
        {
            if (int.TryParse(GetParameterValue("value_1"), out int tmp))
                BuildingId = tmp;
            
            if (int.TryParse(GetParameterValue("value_2"), out tmp))
                Level = tmp;
            
            if (int.TryParse(GetParameterValue("value_3"), out tmp))
                RequiredNumber = tmp;
        }
        
        public override void InitServerStatesData()
        {
            base.InitServerStatesData();

            if (int.TryParse(GetParameterValue("value_1"), out int tmp))
                BuildingId = tmp;
            
            if (int.TryParse(GetParameterValue("value_2"), out tmp))
                Level = tmp;
            
            if (int.TryParse(GetParameterValue("value_3"), out tmp))
                CurrentNumber = tmp;
        }
        
        public override void InitLocalStateData() { }
        
        public override void InitAchievedStateData()
        {
            base.InitAchievedStateData();

            CurrentNumber = RequiredNumber;
        }

        public override Requirement GetConditionRequirement()
        {
            return new Requirement(BuildingId, RequiredNumber, CurrentNumber);
        }

        public override void StartEvaluateCondition()
        {
            RecalculateCurrentCount();
            if (AchievementCheck())
            {
                IsAchived = true;
            }
            else
            {
                jSignalBuildFinishVerificated.AddListener(OnBuildFinishVerificated);
            }
        }

        public override void InitFromPrototype(ICondition condition)
        {
            base.InitFromPrototype(condition);
            var castedConditions = (IBuildingHaveByIdCondition) condition;
            RequiredNumber = castedConditions.RequiredNumber;
            BuildingId = castedConditions.BuildingId;
            Level = castedConditions.Level;
        }

        protected override ICondition GetNewConditionInstance => jInjectionBinder.GetInstance<IBuildingHaveByIdCondition>();

        protected override void CopyMembers(ICondition target)
        {
            base.CopyMembers(target);
            var castedTarget = (IBuildingHaveByIdCondition) target;
            castedTarget.RequiredNumber = RequiredNumber;
            castedTarget.BuildingId = BuildingId;
            castedTarget.Level = Level;
        }
        
        private void OnBuildFinishVerificated(IMapObject mapObject)
        {
            if (mapObject.ObjectTypeID == BuildingId && mapObject.IsConstructed)
            {
                RecalculateCurrentCount();
                if (AchievementCheck())
                {
                    IsAchived = true;
                    jSignalBuildFinishVerificated.RemoveListener(OnBuildFinishVerificated);
                }
                else
                {
                    jAchievementConditionUpdateSignal.Dispatch(); 
                }
            }
        }

        private bool AchievementCheck()
        {
            return CurrentNumber >= RequiredNumber;
        }
        
        private void RecalculateCurrentCount()
        {
            CurrentNumber = 0;
            foreach (var entry in jGameManager.CityMapViews)
            {
                int count = entry.Value.CityMapModel.MapObjects.Count(b => b.ObjectTypeID == BuildingId && b.Level >= Level && b.IsConstructed);
                CurrentNumber += count;
            }
        }
    }
}
