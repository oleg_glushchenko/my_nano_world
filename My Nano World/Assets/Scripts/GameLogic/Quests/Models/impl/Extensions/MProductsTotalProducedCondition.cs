﻿using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.Achievements;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Controllers.Agricultural;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilities;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.Utilites;

/// <summary>
/// Условие общего числа произведенных продуктов
/// </summary>
public class MProductsTotalProducedCondition : Condition, IProductsTotalProducedCondition
{
    [Inject] public OnProductProduceAndCollectSignal jOnProductProduceAndCollectSignal { get; set; }
	[Inject] public AchievementConditionUpdateSignal jAchievementConditionUpdateSignal { get; set; }

    public int CountProducedProducts { get; set; }

    private int _currentCount;
    
    public override string Description
    {
        get { return LocalizationUtils.SafeFormat(base.Description, _currentCount, CountProducedProducts); }
    }

    public override void InitServerBalanceData()
    {
        int countProducedProducts;
        if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out countProducedProducts))
            CountProducedProducts = countProducedProducts;
    }

    public override void InitAchievedStateData()
    {
        base.InitAchievedStateData();

        _currentCount = CountProducedProducts;
    }


    public override void InitFromPrototype(ICondition condition)
    {
        base.InitFromPrototype(condition);
        var prodProt = condition as IProductsTotalProducedCondition;

        if (prodProt != null)
            CountProducedProducts = prodProt.CountProducedProducts;
    }

    /// <summary>
    /// Возвращает новый инстанс кондишена
    /// </summary>
    protected override ICondition GetNewConditionInstance { get { return jInjectionBinder.GetInstance<IProductsTotalProducedCondition>(); } }

    /// <summary>
    /// Копирует переменные в копию объекта
    /// </summary>
    /// <param name="target"></param>
    protected override void CopyMembers(ICondition target)
    {
        base.CopyMembers(target);
        var prodProt = target as IProductsTotalProducedCondition;
        if (prodProt == null) return;
        prodProt.CountProducedProducts = CountProducedProducts;
    }

    public override Requirement GetConditionRequirement()
    {
        return new Requirement(0, CountProducedProducts, _currentCount);
    }
    
	private void OnProductsTotalStateChanged(IMapObject mapObject, Id<Product> productId, int count)
    {
		if (count == 0)
			return;
        _currentCount += count;
		if (!CheckIsAchieved())
			jAchievementConditionUpdateSignal.Dispatch();
    }

    private bool CheckIsAchieved()
    {
		if (_currentCount < CountProducedProducts) return false;

        _currentCount = CountProducedProducts;
        IsAchived = true;
        jOnProductProduceAndCollectSignal.RemoveListener(OnProductsTotalStateChanged);
		return true;
    }

    /// <summary>
    /// Входная точка вычисления условия
    /// </summary>
    public override void StartEvaluateCondition()
    {
		jOnProductProduceAndCollectSignal.AddListener(OnProductsTotalStateChanged);
    }

    public override void InitLocalStateData()
    {
        _currentCount = CityStatistics.TotalProducedProducts;
    }
}
