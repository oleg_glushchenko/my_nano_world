﻿using System;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilites;
using NanoReality.GameLogic.Utilities;

/// <summary>
/// Условие общего количества заработанных денег на аукционе
/// </summary>
public class AuctionGetCoinsCondition : Condition, IAuctionGetCoinsCondition
{
    [Inject] public AchievementConditionUpdateSignal jAchievementConditionUpdateSignal { get; set; }

    public int GetCoinsCount { get; set; }

    private int _getCount;
    
    public override string Description
    {
        get { return LocalizationUtils.SafeFormat(base.Description, _getCount, GetCoinsCount); }
    }

    public override void InitServerBalanceData()
    {
        int countToAchieve;
        if (int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out countToAchieve))
        {
            GetCoinsCount = countToAchieve;
        }
    }

    public override void InitServerStatesData()
    {
        base.InitServerStatesData();
        int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"), out _getCount);
        if (_getCount > GetCoinsCount) //костыль на отображение текущего значения, со стороны сервера сделать не вышло
            _getCount = GetCoinsCount;
    }

    public override void InitAchievedStateData()
    {
        base.InitAchievedStateData();

        _getCount = GetCoinsCount;
    }

    public override void InitFromPrototype(ICondition condition)
    {
        base.InitFromPrototype(condition);
        var prot = condition as IAuctionGetCoinsCondition;

        if (prot != null)
            GetCoinsCount = prot.GetCoinsCount;
    }

    protected override ICondition GetNewConditionInstance { get { return jInjectionBinder.GetInstance<IAuctionGetCoinsCondition>(); } }

    protected override void CopyMembers(ICondition target)
    {
        base.CopyMembers(target);
        var prot = target as IAuctionGetCoinsCondition;
        if (prot == null) return;
        prot.GetCoinsCount = GetCoinsCount;
    }

    public override Requirement GetConditionRequirement()
    {
        return new Requirement(0, GetCoinsCount, _getCount);
    }

    public override void StartEvaluateCondition()
    {
        throw new NotImplementedException();
    }

    private void CheckForComplete()
    {
        if (_getCount >= GetCoinsCount)
        {
            _getCount = GetCoinsCount;
            IsAchived = true;
        }

        jAchievementConditionUpdateSignal.Dispatch();
    }

    public override void InitLocalStateData()
    {
       _getCount += CityStatistics.AuctionEarnedCoins;
        CheckForComplete();
    }
}