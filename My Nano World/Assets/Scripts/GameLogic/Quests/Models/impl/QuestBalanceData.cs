﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.Utilities;
using Assets.NanoLib.Utilities.DataManager.Models.api;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.StartApplicationSequence.Models.api;
using Newtonsoft.Json;

namespace NanoReality.GameLogic.Quests
{
    /// <summary>
    /// Объект контейнер хранящий список квестов в игре
    /// </summary>
    public class QuestBalanceData : IQuestBalanceData
    {
        /// <summary>
        /// Список квестов в игре
        /// </summary>
        [JsonProperty("quests")]
        public List<IQuest> QuestBalance { get; set; }

        /// <summary>
        /// Данные по уровню (на котором откроетя квест игроку) и сектору к которому квест относится (фарм, хеви, и тд.)
        /// </summary>
        public List<QuestLevelDataContainer> QuestLevelsData { get; set; }

        /// <summary>
        /// Поиск квеста по его айди
        /// </summary>
        /// <param name="questId">айди квеста</param>
        /// <returns>возвращает квест по его айди, или нулл, если не найдет такой квест</returns>
        public IQuest GetQuest(Id<IQuest> questId)
        {
            return QuestBalance.Find(curr=>curr.QuestId == questId.Value);
        }

        private void InitializeQuests(IEnumerable<IQuest> questBalance)
        {
            QuestLevelsData = new List<QuestLevelDataContainer>();
            QuestBalance = questBalance.ToList();
                
            foreach (var quest in QuestBalance)
            {
                quest.InitServerData();
                QuestLevelsData.Add(new QuestLevelDataContainer(
                    quest.BusinessSector, GetUserLevelForQuest(quest.UnlockConditions))
                );
            }
                
            QuestLevelsData = QuestLevelsData.OrderBy(o => o.QuestLevel).ToList();
        }

        #region GameBalanceData
        public string DataHash { get; set; }

        public void LoadData(IServerCommunicator serverCommunicator, ILocalDataManager localDataManager,
            Action<IGameBalanceData> callback)
        {
            serverCommunicator.LoadQuestBalanceData(data =>
            {
                InitializeQuests(data);
                callback(this);
            });
        }

        /// <summary>
        /// Получаем уровень на котором квест откроется игроку
        /// </summary>
        private int GetUserLevelForQuest(List<ICondition> conditions)
        {
            int userLevelForQuest = 0;
            foreach (var condition in conditions)
            {
                if (condition.ConditionType == ConditionTypes.UserLevelAchived)
                {
                    userLevelForQuest = ((UserLevelAchivedCondition) condition).TargetUserLevel;
                }
            }

            return userLevelForQuest;
        }

        #endregion

        [Serializable]
        public struct QuestLevelDataContainer
        {
            public BusinessSectorsTypes QuestSectorType;
            public int QuestLevel;

            public QuestLevelDataContainer(BusinessSectorsTypes type, int lvl)
            {
                QuestSectorType = type;
                QuestLevel = lvl;
            }
        }
    }
}
