﻿using System.Collections.Generic;
using NanoReality.GameLogic.Utilities;
using Newtonsoft.Json;

namespace NanoReality.GameLogic.Quests
{
    /// <summary>
    /// Базовый объект действия (награды) вызываемого по окончанию квеста
    /// </summary>
    public class AwardAction : IAwardAction
    {
        [JsonProperty("id")]
        public int AwardId { get; private set; }

        [JsonProperty("action_type")]
        public AwardActionTypes AwardType { get; set; }

        public virtual int AwardCount { get { return 0; } }

        public virtual AwardProduct GetAwardProduct { get { return new AwardProduct(AwardType, AwardCount.ToString()); } }

        public virtual void InitServerData()
        {
            
        }

        /// <summary>
        /// Выполняет действие, дать награду
        /// </summary>
        public virtual void Execute(AwardSourcePlace sourcePlace, string source)
        {
            // только в наследниках
        }

        public virtual string GetAwardDescriptionText(IAwardActionDescription baseDescription)
        {
			throw new System.NotImplementedException("GetAwardDescriptionText");
        }

        [JsonProperty("params")]
        // JSON данные полей для разных типов условий
        protected List<SerializebleKeyValuePair> ServerParams = new List<SerializebleKeyValuePair>();
    }
}