﻿using NanoReality.Game.Services.BuildingsUnlockService;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.Quests;
using NanoReality.GameLogic.Utilities;
using UnityEngine;

namespace GameLogic.Quests.Models.impl.AwardExtensions
{
    public class UnlockNewBuildingsAward : AwardAction, IUnlockNewBuildingsAward
    {
        public int BuildingID { get; set; }

        [Inject] public SignalOnBuildingUnlocked SignalOnBuildingUnlocked { get; set; }
        [Inject] public IBuildingsUnlockService jBuildingsUnlockService { get; set; }
        [Inject] public IPlayerExperience jPlayerExperience { get; set; }

        public override void Execute(AwardSourcePlace sourcePlace, string source)
        {
            base.Execute(sourcePlace, source);
            Debug.LogError("Not implemented");
            int unlockLevel = jBuildingsUnlockService.GetBuildingUnlockLevel(BuildingID);
            if (jPlayerExperience.CurrentLevel >= unlockLevel)
            {
                SignalOnBuildingUnlocked.Dispatch(BuildingID);
            }
        }

        public override void InitServerData()
        {
            base.InitServerData();
            BuildingID = int.Parse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"));
        }

        public override string GetAwardDescriptionText(IAwardActionDescription baseDescription)
        {
            return string.Format(baseDescription.Description, BuildingID);
        }
    }
}
