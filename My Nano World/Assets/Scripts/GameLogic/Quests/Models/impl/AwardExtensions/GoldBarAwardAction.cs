using Assets.Scripts.Engine.Analytics.Model.api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Impl;
using NanoReality.GameLogic.Utilities;

namespace NanoReality.GameLogic.Quests
{
    public sealed class GoldBarAwardAction : AwardAction, IGoldBarAwardAction
    {
        private AwardProduct _awardProduct;
        
        public int CurrencyCount { get; set; }
        
        public override int AwardCount => CurrencyCount;

        public override AwardProduct GetAwardProduct => _awardProduct;

        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        
        [Inject] public INanoAnalytics jNanoAnalytics { get; set; }

        public override void Execute(AwardSourcePlace sourcePlace, string source)
        {
            base.Execute(sourcePlace, source);
            jPlayerProfile.PlayerResources.AddGoldCurrency(CurrencyCount);
            jNanoAnalytics.OnPlayerCurrenciesIncrease(PlayerResources.CurencyType.Gold, CurrencyCount, sourcePlace, source);
        }

        public override void InitServerData()
        {
            base.InitServerData();
            CurrencyCount = int.Parse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"));
            _awardProduct = new AwardProduct(AwardType, CurrencyCount.ToString());
        }

        public override string GetAwardDescriptionText(IAwardActionDescription baseDescription)
        {
            return string.Format(baseDescription.Description, CurrencyCount);
        }
    }

    public interface IGoldBarAwardAction : IAwardAction
    {
        int CurrencyCount { get; set; }
    }
}