﻿using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.Utilities;

namespace NanoReality.GameLogic.Quests
{
    public class NanoGemsAward : AwardAction, INanoGemsAward
    {
        /// <summary>
        /// Сигнал получения нано гемов
        /// </summary>
        [Inject]
        public SignalOnNanoGemsDropped jSignalOnNanoGemsDropped { get; set; }

        public override int AwardCount { get { return NanoGemsCount; } }

        public override AwardProduct GetAwardProduct
        {
            get
            {
                return new AwardProduct(AwardType, NanoGemsCount.ToString());
            }
        }

        public int NanoGemsCount { get; set; }

        public override void Execute(AwardSourcePlace sourcePlace, string source)
        {
            base.Execute(sourcePlace, source);
            if (NanoGemsCount > 0) jSignalOnNanoGemsDropped.Dispatch(NanoGemsCount);
        }

        public override void InitServerData()
        {
            base.InitServerData();
            NanoGemsCount = int.Parse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"));
        }

        public override string GetAwardDescriptionText(IAwardActionDescription baseDescription)
        {
            return string.Format(baseDescription.Description, NanoGemsCount);
        }
    }
}