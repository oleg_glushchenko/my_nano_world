﻿using Assets.Scripts.Engine.Analytics.Model.api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Impl;
using NanoReality.GameLogic.Utilities;

namespace NanoReality.GameLogic.Quests
{
    public class PremiumCurrencyAwardActionAction : AwardAction, IPremiumCurrencyAwardAction
    {
        public override int AwardCount => CurrencyCount;

        public override AwardProduct GetAwardProduct => new AwardProduct(AwardType, CurrencyCount.ToString());

        public int CurrencyCount { get; set; }

        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public INanoAnalytics jNanoAnalytics { get; set; }

        public override void Execute(AwardSourcePlace sourcePlace, string source)
        {
            base.Execute(sourcePlace, source);
            jPlayerProfile.PlayerResources.AddHardCurrency(CurrencyCount);
            jNanoAnalytics.OnPlayerCurrenciesIncrease(PlayerResources.CurencyType.Bucks, CurrencyCount, sourcePlace, source);
        }

        public override void InitServerData()
        {
            base.InitServerData();
            CurrencyCount = int.Parse(SerializebleKeyValuePair.GetValue(ServerParams, "value_1"));
        }

        public override string GetAwardDescriptionText(IAwardActionDescription baseDescription)
        {
            return string.Format(baseDescription.Description, CurrencyCount);
        }
    }
}