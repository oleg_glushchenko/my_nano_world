﻿using Assets.NanoLib.Utilities;
using Assets.Scripts.GameLogic.BuildingSystem.Products.Models.api;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.Utilities;

namespace NanoReality.GameLogic.Quests
{
    public class GiveProductAward : AwardAction, IGiveProductAward
    {
        [Inject]
        public IProductsData Products { get; set; }

        [Inject]
        public IPlayerProfile jPlayerProfile { get; set; }

        /// <summary>
        /// Тип продукта для награды
        /// </summary>
        public Id<Product> Product { get; set; }

        /// <summary>
        /// Кол-во продукта для нагады
        /// </summary>
        public int Amount { get; set; }

        public override int AwardCount { get { return Amount; } }

        public override AwardProduct GetAwardProduct
        {
            get
            {
                InitServerData(); 
                return new AwardProduct(
                    AwardType,
                    Product != null ? Products.GetProduct(Product).ProductName : "Item_name");
            }
        }

        public override void Execute(AwardSourcePlace sourcePlace, string source)
        {
            base.Execute(sourcePlace, source);

            if (Product != null)
            {
                jPlayerProfile.PlayerResources.AddProduct(Product, Amount);
            }
        }

        /// <summary>
        /// Инициализирует данные с сервера
        /// </summary>
        public override void InitServerData()
        {
            base.InitServerData();
            var p = SerializebleKeyValuePair.GetValue(ServerParams, "value_1");
            if (p != null)
            {
                Product = new Id<Product>(int.Parse(p));
                int amount;
                int.TryParse(SerializebleKeyValuePair.GetValue(ServerParams, "value_2"), out amount);
                Amount = amount;
            }
        }

        public override string GetAwardDescriptionText(IAwardActionDescription baseDescription)
        {
            var productDes = Products.GetProduct(Product);
            if (productDes == null) return "Random product";
            return string.Format(baseDescription.Description, productDes.ProductName, Amount);
        }
    }
}