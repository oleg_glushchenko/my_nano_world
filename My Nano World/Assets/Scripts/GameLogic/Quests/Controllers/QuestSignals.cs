﻿using System.Collections.Generic;
using strange.extensions.signal.impl;
using NanoReality.Engine.UI.Extensions.QuestPanel;
using UnityEngine;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;

namespace NanoReality.GameLogic.Quests
{
    /// <summary>
    /// Вызывается когда квест становится доступным для выполнения
    /// </summary>
    public class SignalOnQuestUnlock : Signal<IQuest> { }

    /// <summary>
    /// Вызывается когда квест начинается
    /// </summary>
    public class SignalOnQuestStart : Signal<IQuest> { }

    /// <summary>
    /// Вызывается когда квест выполнен
    /// </summary>
    public class SignalOnQuestAchive : Signal<IQuest> { }

    /// <summary>
    /// Вызывается когда собрана награда за квест
    /// </summary>
	public class SignalOnQuestComplete : Signal<IQuest> { }

    /// <summary>
    /// Called when quest is failed
    /// </summary>
    public class SignalOnQuestFailed : Signal<IQuest> { }

    /// <summary>
    /// Вызывается, когда квесты игрока загружены с сервера
    /// </summary>
	public class SignalOnQuestsLoaded : Signal { }

    /// <summary>
    /// Вызывается, когда игрок тапнул на requirment итем
    /// </summary>
    public class SignalOnQuestRequirmentItemClick : Signal<QuestConditionIcon> { }

    /// <summary>
    /// Вызывается, когда игрок увидел информацию об квестах через юай панельку
    /// </summary>
    public class SignalOnQuestsShowedByUser : Signal<BusinessSectorsTypes> { }
}
