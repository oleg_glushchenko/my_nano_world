﻿using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.Utilities;
using Assets.Scripts.Engine.BuildingSystem.CityMap;
using Assets.Scripts.Engine.BuildingSystem.MapObjects;
using Assets.Scripts.Extensions;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Api;
using NanoReality.GameLogic.Common.Controllers.Signals;
using NanoReality.GameLogic.GameManager.Controllers;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.StartApplicationSequence.Controllers.Signals;
using Assets.Scripts.GameLogic.BuildingSystem.MapObjects.Models.api.OrderDesks;
using GameLogic.BuildingSystem.CityAoe;
using NanoReality.Engine.BuildingSystem.CityCamera;
using NanoReality.Engine.BuildingSystem.GlobalCityMap;
using NanoReality.Engine.BuildingSystem.MapObjects;
using NanoReality.Game.Attentions;
using NanoReality.Game.CitizenGifts;
using NanoReality.GameLogic.BuildingSystem.GlobalCityMap.Models;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Models.api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using Vector3 = UnityEngine.Vector3;

namespace NanoReality.GameLogic.GameManager.Models.api
{
    public class GameManager : IGameManager
    {
        private readonly Dictionary<BusinessSectorsTypes, ICityMap> _loadedCityMaps = new Dictionary<BusinessSectorsTypes, ICityMap>();
        private readonly Dictionary<BusinessSectorsTypes, CityMapView> _cityMapViews = new Dictionary<BusinessSectorsTypes, CityMapView>(4);

        private bool _isMapObjectSelectEnable = true;
        
        public IUserCity CurrentUserCity { get; set; }
        
        public Id<IPlayerProfile> CurrentProfileId { get; set; }
        
        public IMapObject CurrentSelectedMapObject { get; set; }
        
        public MapObjectView CurrentSelectedObjectView { get; set; }

        public IGlobalCityMap GlobalCityMap => CurrentUserCity.GlobalCityMap;

        public GlobalCityMapView GlobalCityMapView { get; private set; }

        public Dictionary<BusinessSectorsTypes, CityMapView> CityMapViews => _cityMapViews;
        
        public bool IsMapObjectSelectEnable
        {
            get { return _isMapObjectSelectEnable; }
            set { _isMapObjectSelectEnable = value; }
        }

        public bool IsApplicationPaused { get; private set; }

        #region Injects
        
        [Inject] public IGameCameraModel CurrentGameCamera { get; set; }
        [Inject] public IBuildingAttentionsController jBuildingAttentionsController { get; set; }
        [Inject] public ICityGiftsService jCityGiftService { get; set; }  
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public ICityAoeService jCityAoeService { get; set; }
        [Inject] public SignalOnCityMapLoaded SignalOnCityMapLoaded { get; set; }
        [Inject] public SignalOnMapObjectChangedSelect jSignalOnMapObjectChangedSelect { get; set; }
        [Inject] public SignalOnUserBusinessSectorsUpdate jSignalOnUserBusinessSectorsUpdate { get; set; }
        [Inject] public FinishApplicationInitSignal OnFinishApplicationInitSignal { get; set; }
        [Inject] public SignalOnApplicationPause SignalOnApplicationPause { get; set; }
        [Inject] public SignalOnFinishLoadCityMapSequence jSignalOnFinishLoadCityMapSequence { get; set; }
        [Inject] public SignalOnFinishRestoreCity jSignalOnFinishRestoreCity { get; set; }

        #endregion

        public ICityMap GetMap(Id<IBusinessSector> sectorId)
        {
            return GetMap(sectorId.ToBusinessSectorType());
        }

        public ICityMap GetMap(BusinessSectorsTypes sectorId)
        {
            return CurrentUserCity?.GetBusinessSectorById(sectorId);
        }
        /// <summary>
        /// Returns city map by world position.
        /// </summary>
        /// <param name="worldPosition"></param>
        /// <returns></returns>
        public ICityMap GetMap(Vector3 worldPosition)
        {
            foreach (var cityMapView in _cityMapViews)
            {
                if (cityMapView.Value.Contains(worldPosition))
                {
                    return cityMapView.Value.CityMapModel;
                }
            }

            return null;
        }

        public CityMapView GetMapView(Id<IBusinessSector> sectorId)
        {
            return _cityMapViews.TryGetValue(sectorId.ToBusinessSectorType(), out CityMapView view) ? view : null;
        }

        public CityMapView GetMapView(BusinessSectorsTypes sectorId)
        {
            return GetMapView(sectorId.GetBusinessSectorId());
        }

        public MapObjectView GetMapObjectView(IMapObject mapObject)
        {
            if (mapObject == null) return null;
            var sectorType = mapObject.SectorId.ToBusinessSectorType();

            switch (sectorType)
            {
                case BusinessSectorsTypes.Town:
                case BusinessSectorsTypes.Farm:
                case BusinessSectorsTypes.HeavyIndustry:
                    var mapView = GetMapView(mapObject.SectorId);
                    return mapView.GetMapObjectById(mapObject.MapID);
                    break;
                case BusinessSectorsTypes.Global:
                    return GlobalCityMapView.GetView(mapObject.MapID);
                    break;
            }

            return null;
        }

        public MapObjectView FindMapObjectViewOnMapByType(BusinessSectorsTypes sectorType, MapObjectintTypes objectType)
        {
            var mapObject = FindMapObjectOnMapByType(sectorType, objectType);
            return GetMapObjectView(mapObject);
        }
        
        public MapObjectView FindMapObjectViewOnMapByType(BusinessSectorsTypes sectorType, MapObjectintTypes objectType, int objectId)
        {
            var mapObject = FindMapObjectOnMapByType(sectorType, objectType, objectId);
            return GetMapObjectView(mapObject);
        }

        public T FindMapObjectView<T>(MapObjectintTypes objectType) where T: MapObjectView
        {
            var mapObject = FindMapObjectByType<IMapObject>(objectType);
            return GetMapObjectView(mapObject) as T;
        }

        public IMapObject FindMapObjectOnMapByType(BusinessSectorsTypes sectorType, MapObjectintTypes objectType)
        {
            var sectordId = sectorType.GetBusinessSectorId();
            IMapObject mapObject = null;

            switch (sectorType)
            {
                case BusinessSectorsTypes.Town:
                case BusinessSectorsTypes.Farm:
                case BusinessSectorsTypes.HeavyIndustry:
                    var map = GetMap(sectordId);
                    if (map != null)
                    {
                        mapObject = map.FindMapObject(objectType);
                    }
                    break;
                case BusinessSectorsTypes.Global:
                    if (GlobalCityMap != null)
                    {
                        mapObject = GlobalCityMap.FindMapObject(objectType);
                    }
                    break;
            }

            // DIRTY HACK!!!! TO CHECK FOR BUILDINGS IN GLOBAL SECTORS THAT INITIALLY BELONG TO OTHER SECTOR
            // TODO: REFACTOR IT LATER SOMEHOW!
            if (mapObject != null || GlobalCityMap == null)
                return mapObject;
            
            if (objectType == MapObjectintTypes.OrderDesk)
            {
                List<IMapObject> mapObjects = GlobalCityMap.FindAllMapObjects(MapObjectintTypes.OrderDesk);
                foreach (IMapObject mapObj in mapObjects)
                {
                    if (mapObj is IOrderDeskMapBuilding building && building.OrderDeskSectorType == sectorType)
                    {
                        return building;
                    }
                }
            }
            else if (objectType == MapObjectintTypes.CityOrderDesk)
            {
                mapObject = GlobalCityMap.FindMapObject(MapObjectintTypes.CityOrderDesk);
            }

            return mapObject;
        }

        public IMapObject FindMapObjectOnMapByType(BusinessSectorsTypes sectorType, MapObjectintTypes objectType, int objectId)
        {
            List<IMapObject> mapObjects = FindAllMapObjectsOnMapByType(sectorType, objectType);
            return mapObjects?.FirstOrDefault(mapObject => mapObject.Id.Id == objectId);
        }

        public List<IMapObject> FindAllMapObjectsOnMapByType(BusinessSectorsTypes sectorType, MapObjectintTypes objectType)
        {
            var sectordId = sectorType.GetBusinessSectorId();
            List<IMapObject> list = null;

            switch (sectorType)
            {
                case BusinessSectorsTypes.Town:
                case BusinessSectorsTypes.Farm:
                case BusinessSectorsTypes.HeavyIndustry:
                    var map = GetMap(sectordId);
                    if (map != null)
                    {
                        list = map.FindAllMapObjects(objectType);
                    }
                    break;
                case BusinessSectorsTypes.Global:
                    if (GlobalCityMap != null)
                    {
                        list = GlobalCityMap.FindAllMapObjects(objectType);
                    }
                    break;
            }

            // DIRTY HACK!!!! TO CHECK FOR QUEST BUILDINGS IN GLOBAL SECTORS THAT INITIALLY BELONG TO OTHER SECTOR
            // TODO: REFACTOR IT LAYER SOMEHOW!
            if (list != null || GlobalCityMap == null)
                return list;
            
            if (objectType == MapObjectintTypes.OrderDesk)
            {
                list = new List<IMapObject>();
                List<IMapObject> mapObjects = GlobalCityMap.FindAllMapObjects(MapObjectintTypes.OrderDesk);
                foreach (IMapObject mapObj in mapObjects)
                {
                    if (mapObj is IOrderDeskMapBuilding building && building.OrderDeskSectorType == sectorType)
                    {
                        list.Add(building);
                    }
                }
            }
            else if (objectType == MapObjectintTypes.CityOrderDesk)
            {
                list = GlobalCityMap.FindAllMapObjects(MapObjectintTypes.CityOrderDesk);
            }
            
            return list;
        }

        public List<IMapObject> FindAllMapObjectsOnMapByType(MapObjectintTypes objectType)
        {
            List<IMapObject> list = new List<IMapObject>();

            foreach (ICityMap cityMap in CurrentUserCity.CityMaps)
            {
                List<IMapObject> mapObjects = cityMap.MapObjects;
                for (int i = 0; i < mapObjects.Count; i++)
                {
                    if (mapObjects[i].MapObjectType == objectType)
                        list.Add(mapObjects[i]);
                }
            }

            return list;
        }

        public List<IMapObject> FindAllMapObjectsOnMapByType(BusinessSectorsTypes sectorType, Id_IMapObjectType objectTypeId)
        {
            var sectordId = sectorType.GetBusinessSectorId();
            List<IMapObject> list = null;

            switch (sectorType)
            {
                case BusinessSectorsTypes.Town:
                case BusinessSectorsTypes.Farm:
                case BusinessSectorsTypes.HeavyIndustry:
                    var map = GetMap(sectordId);
                    if (map != null)
                    {
                        list = map.FindAllMapObjects(objectTypeId);
                    }
                    break;
                case BusinessSectorsTypes.Global:
                    if (GlobalCityMap != null)
                    {
                        list = GlobalCityMap.FindAllMapObjects(objectTypeId);
                    }
                    break;
            }

            if (list == null && GlobalCityMap != null)
            {
                list = GlobalCityMap.FindAllMapObjects(objectTypeId);
            }
            
            return list;
        }
        
        public int GetMapObjectsCountInCity(int buildingId)
        {
            int totalCount = 0;

            foreach (ICityMap cityMap in CurrentUserCity.CityMaps)
            {
                List<IMapObject> mapObjects = cityMap.MapObjects;
                for (int i = 0, count = mapObjects.Count; i < count; i++)
                {
                    if (mapObjects[i].ObjectTypeID == buildingId)
                        totalCount++;
                }
            }

            return totalCount;
        }
        
        public List<IMapObject> GetMapObjectsById(int buildingId)
        {
            List<IMapObject> resultList = new List<IMapObject>();

            foreach (ICityMap cityMap in CurrentUserCity.CityMaps)
            {
                List<IMapObject> mapObjects = cityMap.MapObjects;
                for (int i = 0, count = mapObjects.Count; i < count; i++)
                {
                    IMapObject mapObject = mapObjects[i];
                    if (mapObject.ObjectTypeID == buildingId)
                        resultList.Add(mapObject);
                }
            }

            return resultList;
        }

        public T FindMapObjectByType<T>(MapObjectintTypes objectType) where T: class, IMapObject
        {
            if (_loadedCityMaps != null)
            {
                foreach (var pair in _loadedCityMaps)
                {
                    var map = pair.Value;
                    var mapObject = map.FindMapObject(objectType) as T;
                    if (mapObject != null)
                    {
                        return mapObject;
                    }
                }
            }

            if (GlobalCityMap != null)
            {
                return GlobalCityMap.FindMapObject(objectType) as T;
            }

            return null;
        }

        public T FindMapObjectByTypeId<T>(Id_IMapObjectType typeId) where T : class, IMapObject
        {
            if (_loadedCityMaps != null)
            {
                foreach (var pair in _loadedCityMaps)
                {
                    var map = pair.Value;
                    if (map.FindMapObject(typeId) is T mapObject)
                    {
                        return mapObject;
                    }
                }
            }

            if (GlobalCityMap != null)
            {
                return GlobalCityMap.FindMapObject(typeId) as T;
            }
            return null;
        }

        public T FindMapObjectById<T>(int buildingId) where T : class, IMapObject
        {
            IMapObject mapObject;
            foreach (KeyValuePair<BusinessSectorsTypes,ICityMap> keyValuePair in _loadedCityMaps)
            {
                ICityMap cityMap = keyValuePair.Value;
                mapObject = cityMap.GetMapObject(buildingId);
                if (mapObject != null)
                {
                    return (T)mapObject;
                }
            }
            
            Id_IMapObject id = buildingId;
            mapObject = GlobalCityMap.FindMapObject(id);

            return (T) mapObject;
        }

        public void RegisterMapView(CityMapView view)
        {
            if (!_cityMapViews.ContainsKey(view.BusinessSector))
            {
                _cityMapViews.Add(view.BusinessSector, view);
            }
        }

        public void RegisterGlobalCityMapView(GlobalCityMapView view)
        {
            GlobalCityMapView = view;
        }

        public void ClearMapsView()
        {
            foreach (var loadedCityMap in _loadedCityMaps)
            {
                loadedCityMap.Value.ClearMapView();
            }
            
            jBuildingAttentionsController.RemoveAllStatesAttentionView();
        }

        public void ClearAll()
        {
            foreach (var loadedCityMap in _loadedCityMaps)
            {
                loadedCityMap.Value.ClearMapView();
                loadedCityMap.Value.ClearMap();
            }
            _loadedCityMaps.Clear();

            if (GlobalCityMapView != null)
            {
                GlobalCityMapView.Clear();
            }

            if (CurrentUserCity != null && CurrentUserCity.GlobalCityMap != null)
            {
                CurrentUserCity.GlobalCityMap.Clear();
            }
        }

        public bool IsApplicationLoaded { get; private set; }

        /// <summary>
        /// Подписка на события в игре
        /// </summary>
        [PostConstruct]
        public void PostConstruct()
        {
            jSignalOnMapObjectChangedSelect.AddListener(OnMapObjectSelectChanged);
            jSignalOnFinishLoadCityMapSequence.AddListener(FinishSequence);
            OnFinishApplicationInitSignal.AddOnce(() => { IsApplicationLoaded = true; });
            SignalOnApplicationPause.AddListener(OnApplicationPause);
        }

        [Deconstruct]
        public void Deconstruct()
        {
            jSignalOnFinishLoadCityMapSequence.RemoveListener(FinishSequence);
        }

        private void OnApplicationPause(bool b)
        {
            IsApplicationPaused = b;
        }

        private void FinishSequence(LoadCityMapCommandData data)
        {
            foreach (KeyValuePair<BusinessSectorsTypes,ICityMap> sectorMap in data.LoadedCityMaps)
            {
                _loadedCityMaps.Add(sectorMap.Key, sectorMap.Value);
            }

            SignalOnCityMapLoaded.Dispatch(CurrentProfileId, CurrentUserCity, null);
            jSignalOnUserBusinessSectorsUpdate.Dispatch(data.AvailableBusinessSectors);
        }

        /// <summary>
		/// Регестрирует текущее выделенное здание и вьюшку
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="isActive">If set to <c>true</c> is active.</param>
		private void OnMapObjectSelectChanged(MapObjectView sender, bool isActive)
		{
			if (isActive)
			{
				CurrentSelectedObjectView = sender;
				CurrentSelectedMapObject = sender.MapObjectModel;
            }
			else if(CurrentSelectedObjectView == sender)
			{
				CurrentSelectedObjectView = null;
				CurrentSelectedMapObject = null;
			}
		}

        public void UpdateRoads()
        {
            _cityMapViews.ForEach(x => x.Value.UpdateRoads());
        }

        public void DeselectRoads()
        {
            _cityMapViews.ForEach(x => x.Value.DeselectRoads());
        }

        public void RestoreCityMapFromServer()
        {
            ClearMapsView();
            
            List<BusinessSectorsTypes> bssTypes = _loadedCityMaps.Keys.ToList();

            int cityCounter = 0;
            
            void LoadCity()
            {
                jServerCommunicator.LoadBusinessSectorMap(new LoadCityMapResponseModel()
                {
                    UserId =  jPlayerProfile.Id.Value.ToString(),
                    BusinessSectorId = bssTypes[cityCounter],
                    SuccessCallback = OnLoadCitySectorMap
                }); 
            }
            
            void OnLoadCitySectorMap(Id<IUserCity> cityId, ICityMap cityMap, Id<IPlayerProfile> userId,
                BusinessSectorsTypes sectorId)
            {
                var city = CurrentUserCity.CityMaps.First(m => m.BusinessSector == sectorId);
                city.MapObjects = cityMap.MapObjects;
                city.RestoreMap(() =>
                {
                    _cityMapViews[sectorId].RoadGrid.UpdateRoads();

                    if (++cityCounter < _loadedCityMaps.Count)
                    {
                        LoadCity();
                        return;
                    }

                    jCityAoeService.LoadAoeData();
                    jCityGiftService.LoadAvailableGifts(null);
                    jSignalOnFinishRestoreCity.Dispatch();
                });
            }
            
            LoadCity();
        }
    }
}
