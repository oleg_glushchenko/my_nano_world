﻿using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using strange.extensions.signal.impl;

namespace NanoReality.GameLogic.GameManager.Controllers
{
    public class SignalOnCityMapLoaded : Signal<Id<IPlayerProfile>, IUserCity, ICityMap> { }
    public class SignalLoadCityMapSequence : Signal<LoadCityMapCommandData> { }
    public class SignalOnFinishLoadCityMapSequence : Signal<LoadCityMapCommandData> { }
    public class SignalOnFinishRestoreCity : Signal { }
}