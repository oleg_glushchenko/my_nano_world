﻿using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.StrangeIoC;

namespace NanoReality.GameLogic.GameManager.Controllers
{
    public class InitUserCityCommand : AGameCommand
    {
        [Inject] public IPlayerProfile PlayerProfile { get; set; }
        [Inject] public IGameManager jGameManager { get; set; }
        
        public override void Execute()
        {
            base.Execute();
            jGameManager.CurrentUserCity =  PlayerProfile.UserCity;
            Release();
        }
    }
}