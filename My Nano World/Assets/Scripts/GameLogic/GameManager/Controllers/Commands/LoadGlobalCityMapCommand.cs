﻿using NanoLib.AssetBundles;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.GlobalCityMap.Models;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.StrangeIoC;

namespace NanoReality.GameLogic.GameManager.Controllers
{
    public class LoadGlobalCityMapCommand : AGameCommand
    {
        [Inject] public LoadCityMapCommandData jCommandInData { get; set; }

        [Inject] public IServerCommunicator jServerCommunicator { get; set; }

        public override void Execute()
        {
            base.Execute();

            Retain();
            jServerCommunicator.LoadGlobalCityMap(jCommandInData.CityId, jCommandInData.PlayerProfileId,
                BusinessSectorsTypes.Global.GetBusinessSectorId(), OnLoadGlobalCityMap);
        }

        private void OnLoadGlobalCityMap(IGlobalCityMap globalCityMap)
        {
            jCommandInData.GlobalCityMap = globalCityMap;
            Release();
        }
    }
}