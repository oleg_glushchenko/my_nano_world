﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.CityMap.Models.Api;
using NanoReality.GameLogic.BuildingSystem.GlobalCityMap.Models;
using NanoReality.GameLogic.BuildingSystem.MapObjects.Impl;
using NanoReality.GameLogic.GameManager.Models.api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.Services;
using NanoReality.StrangeIoC;

namespace NanoReality.GameLogic.GameManager.Controllers
{
    public class LoadCityMapCommand : AGameCommand
    {
        private int _mustLoadCities;
        private bool _hasLoadedSubSectors;
        private CitySubLockedMap _subLockedMap;

        [Inject] public LoadCityMapCommandData jCommandInData { get; set; }
        
        [Inject] public IGameManager jGameManager { get; set; }
        [Inject] public IServerCommunicator jServerCommunicator { get; set; }
        [Inject] public IPlayerProfile jPlayerProfile { get; set; }
        [Inject] public ICitySectorService JCitySectorService { get; set; }

        #region Methods

        public override void Execute()
        {
            base.Execute();
            
            Retain();

            foreach (var mapView in jGameManager.CityMapViews)
            {
                if (mapView.Value.CityMapModel != null)
                    mapView.Value.CityMapModel.ClearMapView();
            }

            Dictionary<BusinessSectorsTypes,ICityMap> loadedCityMaps = jCommandInData.LoadedCityMaps;
            IUserCity userCity = jPlayerProfile.UserCity;
            if (userCity.Id == jCommandInData.CityId && userCity.CityMaps.Any(
                map => map.BusinessSector == jCommandInData.BusinessSector))
            {
                foreach (ICityMap cityMap in userCity.CityMaps)
                {
                    if (cityMap != null && !loadedCityMaps.ContainsKey(cityMap.BusinessSector))
                        loadedCityMaps.Add(cityMap.BusinessSector, cityMap);
                }

                Release();
                return;
            }

            if (jGameManager.CurrentUserCity != null &&
                jGameManager.CurrentUserCity.Id == jCommandInData.CityId
                && loadedCityMaps.ContainsKey(jCommandInData.BusinessSector))
            {
                Release();
                return;
            }

            loadedCityMaps.Clear();
            _mustLoadCities = jCommandInData.AvailableBusinessSectors.Count();
            _hasLoadedSubSectors = false;
            jServerCommunicator.LoadSubSectors(jCommandInData.CityId, OnLoadSubSectors);

            foreach (var sectorId in jCommandInData.AvailableBusinessSectors)
            {
                if (loadedCityMaps.ContainsKey(sectorId)) continue;
                jServerCommunicator.LoadBusinessSectorMap(new LoadCityMapResponseModel()
                {
                    UserCityId = jCommandInData.CityId,
                    UserId = jCommandInData.PlayerProfileId,
                    BusinessSectorId = sectorId,
                    SuccessCallback = OnLoadCitySectorMap
                });
            }
        }

        private void OnLoadSubSectors(CitySubLockedMap citySubLockedMap)
        {
            _hasLoadedSubSectors = true;
            _subLockedMap = citySubLockedMap;
            if (_mustLoadCities <= 0)
            {
                SetSectors();
                Release();
            }
        }

        private void OnLoadCitySectorMap(Id<IUserCity> cityId, ICityMap cityMap, Id<IPlayerProfile> userId,
            BusinessSectorsTypes sectorId)
        {
            jCommandInData.LoadedCityMaps.Add(sectorId, cityMap);
            _mustLoadCities--;
            if (_mustLoadCities <= 0)
            {
                if (_hasLoadedSubSectors)
                {
                    SetSectors();
                    Release();
                }
            }

        }

        private void SetSectors()
        {
            foreach (var map in jCommandInData.LoadedCityMaps)
            {
                map.Value.SubLockedMap.LockedSectors =
                    _subLockedMap.LockedSectors.Where(s => s.BusinessSectorId.ToBusinessSectorType() == map.Key).ToList();
                map.Value.SubLockedMap.LockedSectors.ForEach(ls => ls.LockedSector.Init());

                if (JCitySectorService.SectorAreas(map.Key) != null)
                {
                    JCitySectorService.UpdateLockedMap(map.Value.MapSize, map.Value.SubLockedMap.LockedSectors, map.Value.BusinessSector);
                }
            }
        }

        #endregion
    }

    public class LoadCityMapResponseModel
    {
        public Id<IUserCity> UserCityId;
        public Id<IPlayerProfile> UserId;
        public BusinessSectorsTypes BusinessSectorId;
        public Action<Id<IUserCity>, ICityMap, Id<IPlayerProfile>, BusinessSectorsTypes> SuccessCallback;
    }

    public class LoadCityMapCommandData
    {
        public Id<IPlayerProfile> PlayerProfileId;
        public Id<IUserCity> CityId;
        public BusinessSectorsTypes BusinessSector;
        public Dictionary<BusinessSectorsTypes, ICityMap> LoadedCityMaps = new Dictionary<BusinessSectorsTypes, ICityMap>(10);
        public IEnumerable<BusinessSectorsTypes> AvailableBusinessSectors = new List<BusinessSectorsTypes>(3);
        
        public IGlobalCityMap GlobalCityMap { get; set; }
    }
}
