﻿using NanoReality.Game.FoodSupply;
using NanoReality.GameLogic.OrderDesk;
using NanoReality.GameLogic.StartApplicationSequence.Models.api;

namespace NanoReality.GameLogic.Configs
{
    public interface IGameConfigData : IGameBalanceData
    {
        float CollectedCoinsToShow { get; }

        SoukConfig SoukConfig { get; }

        /// <summary>
        /// How many time user need to wait for collect receive new daily bonus. [Seconds]
        /// </summary>
        int DailyBonusInterval { get; }

        MetroConfig MetroConfig { get; }
        
        FoodSupplyConfig FoodSupplyConfig { get; }
        
        TutorialConfigDate TutorialConfigDate { get; }
        
        int AdsLimit { get; }
    }
}