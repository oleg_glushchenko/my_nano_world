﻿using System;
using System.Collections.Generic;
using Assets.NanoLib.Utilities.DataManager.Models.api;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using NanoReality.GameLogic.StartApplicationSequence.Models.api;
using Newtonsoft.Json;

namespace NanoReality.GameLogic.Configs
{
	public class BuildingSlotsData : IBuildingSlotsData
	{
		public List<BuildingSlotData> BuildingSlotsDataList { get; private set; }
		public string DataHash { get; set; }

		public void LoadData(IServerCommunicator serverCommunicator, ILocalDataManager localDataManager, Action<IGameBalanceData> callback)
		{
			void Callback(List<BuildingSlotData> slotsData)
			{
				BuildingSlotsDataList = slotsData;
				callback.Invoke(this);
			}

			serverCommunicator.LoadBuildingSlotsData(this, Callback);
		}
	}

	public class BuildingSlotData
	{
		[JsonProperty("building_id")] 
		public int BuildingId { get; private set; }

		[JsonProperty("slots")] 
		public List<SlotData> SlotData { get; private set; }
	}

	public class SlotData
	{
		[JsonProperty("slot")] 
		public int SlotId { get; private set; }

		[JsonProperty("price")] 
		public int Price { get; private set; }
	}
}