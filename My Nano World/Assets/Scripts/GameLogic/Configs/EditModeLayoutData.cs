﻿using System.Collections.Generic;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.GameLogic.BuildingSystem.MapObjects;
using NanoReality.GameLogic.ServerCommunication.Models.Api;
using Newtonsoft.Json;
using UnityEngine;

namespace NanoReality.GameLogic.Configs
{
	public class EditModeLayoutData
	{
		[JsonProperty("id")] 
		public int LayoutId;
		
		[JsonProperty("happiness")] 
		public int Happiness;

		[JsonProperty("map")] 
		public List<EditModeMapBuildingData> MapBuildingData { get; private set; }
		
		[JsonProperty("storage")] 
		public List<EditModeStorageBuildingData> StorageBuildingData { get; set; }
	}
	
	public class EditModeLayout
	{
		[JsonProperty("id")] 
		public int LayoutId;

		[JsonProperty("image")] 
		public string LayoutImage;

		[JsonProperty("active")] 
		public bool IsLayoutActive;
	}
	
	public class EditModeMapBuildingData
	{
		[JsonProperty("layout_object_id")] 
		public int LayoutObjectId;	
		
		[JsonProperty("player_building_id")] 
		public int BuildingId;

		[JsonProperty("position")] 
		public Vector2 Position;
		
		[JsonProperty("bss_id")] 
		public BusinessSectorsTypes BusinessSectorsType;
		
		[JsonProperty("exit_road")] 
		public bool ExitMainRoad;

		[JsonProperty("aoe_type")] 
		public AoeLayout AoeType { get; set; }
	}

	public class EditModeHappinessMap
	{
		[JsonProperty("id")] 
		public int LayoutId;	
		
		[JsonProperty("happiness")] 
		public int Happiness;
		
		[JsonProperty("map")] 
		public EditModeMapBuildingData BuildingData;
		
		[JsonProperty("storage")] 
		public List<EditModeStorageBuildingData> StorageBuildingData { get; set; }
	}
	
	public class EditModeDeleteRoadResponse : DeleteRoadResponse
	{
		[JsonProperty("happiness")] 
		public int Happiness;
	}

	public class EditModeCreateRoadResponse : CreateRoadResponse
	{
		[JsonProperty("happiness")] 
		public int Happiness;
	}
	
	public class EditModeStorageBuildingData
	{
		[JsonProperty("bss_id")] 
		public BusinessSectorsTypes BusinessSectorsType;

		[JsonProperty("buildings")] 
		public List<int> Buildings { get; set; }
	}
}