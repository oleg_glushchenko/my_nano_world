using GameLogic.Bazaar.Model.impl;
using Newtonsoft.Json;

namespace NanoReality.GameLogic.Configs
{
    public class SoukConfig
    {
        [JsonProperty("available_reward_products")]
        public SoukTopSlot[] BazaarProducts { get; private set; }
        
        [JsonProperty("top_tradeline")]
        public TradeLineUnlockSlotPrice[] TopTradeline { get; private set; }

        [JsonProperty("bottom_tradeline")]
        public TradeLineUnlockSlotPrice[] BottomTradeline { get; private set; }
        
        [JsonProperty("loot_boxes")]
        public BazaarChances[] ChestsConfig { get; private set; }
    }
    
    public class TradeLineUnlockSlotPrice
    {
        [JsonProperty("id")] 
        public int SlotId;
        
        [JsonProperty("unlock_price")]
        public int UnlockSlotPrice;
    }

    public class BazaarChances
    {
        [JsonProperty("type")] 
        public int SlotId;
        
        [JsonProperty("special_item_chance")]
        public float SpecialItemChance;
        
        [JsonProperty("common_item_chance")]
        public float CommonItemChance;
        
        [JsonProperty("nano_bucks_chance")]
        public float NanoBucksChance;
        
        [JsonProperty("nano_coins")]
        public int NanoCoins;
    }
}