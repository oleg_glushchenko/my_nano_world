using System;
using System.Collections.Generic;
using NanoReality.Game.FoodSupply;
using NanoReality.GameLogic.Data;
using Newtonsoft.Json;

namespace NanoReality.GameLogic.Configs
{
    [Serializable]
    public class FoodSupplyCityInfo
    {
        [JsonProperty("zones")]
        public List<FoodSupplyZoneSettings> zoneSettings;

        [JsonProperty("status_bar")]
        public StatusBarData statusBarData;

        [JsonProperty("effects")]
        public List<int> activeEffects;

        [Serializable]
        public class StatusBarData
        {
            [JsonProperty("calories_to_coin_rate")]
            public float coinsPerCalorie;

            [JsonProperty("current_zone")]
            public int currentZone;

            [JsonProperty("bar_value")]
            public int caloriesValue;
            
            [JsonProperty("bar_max_value")]
            public int maxCalories;
            
            [JsonProperty("bar_level")]
            public int barLevel;
            
            [JsonProperty("buff_time")]
            public int statusEffectDuration;
            
            [JsonProperty("buff_time_end")]
            public long statusEffectEndTimeStamp;

            [JsonProperty("additional_reward")]
            public List<AdditionalReward> additionalRewards;

            [Serializable]
            public class AdditionalReward
            {
                [JsonProperty("reward")] 
                public Reward reward;

                [JsonProperty("opening_condition")]
                public int openFromPercent;
            }
        }
    }
}