namespace NanoReality.GameLogic.SocialCommunicator.api
{
    public interface IFacebookController
    {
        void Init();
        void Login();
        void Logout();
        bool IsLoggedIn { get; }
    }
}