using Assets.NanoLib.UtilitesEngine;
using Facebook.Unity;
using NanoLib.Core.Logging;
using NanoReality.GameLogic.SocialCommunicator.api;

namespace NanoReality.GameLogic.SocialCommunicator.impl
{
    public class FacebookController : IFacebookController
    {
        [Inject] public FBLoggedInSignal jFBLoggedInSignal { get; set; }
        [Inject] public ITimerManager jTimerManager { get; set; }

        public bool IsLoggedIn => FB.IsInitialized && FB.IsLoggedIn;

        public void Init()
        {
            if (!FB.IsInitialized)
            {
                Logging.Log(LoggingChannel.Facebook, "FB initialize status : START INITIALIZE");
                FB.Init(OnInitComplete, jTimerManager.OnHideUnity);
            }
            else
            {
                FB.ActivateApp();
            }
        }

        public void Login()
        {
            Logging.Log(LoggingChannel.Facebook, "FB login status : START LOGIN");
            FB.LogInWithReadPermissions(callback: LoginCallback);
        }

        private void LoginCallback(ILoginResult result)
        {
            if (FB.IsInitialized)
            {
                if (FB.IsLoggedIn)
                {
                    Logging.Log(LoggingChannel.Facebook, "FB login status : LOGGED IN");
                    jFBLoggedInSignal.Dispatch(true);
                }
                else
                {
                    Logging.Log(LoggingChannel.Facebook, "FB login status : FAILED, USER CANCELLED LOGIN");
                }
            }
        }

        private void OnInitComplete()
        {
            if (FB.IsInitialized)
            {
                Logging.Log(LoggingChannel.Facebook, "FB initialize status : INITIALIZED");
                FB.ActivateApp();
            }
            else
            {
                Logging.Log(LoggingChannel.Facebook, "FB initialize status : FAILED TO INITIALIZE THE FACEBOOK SDK");
            }
        }

        public void Logout()
        {
            Logging.Log(LoggingChannel.Facebook, "FB login status : LOGGED OUT");
            FB.LogOut();
            jFBLoggedInSignal.Dispatch(false);
        }
    }
}