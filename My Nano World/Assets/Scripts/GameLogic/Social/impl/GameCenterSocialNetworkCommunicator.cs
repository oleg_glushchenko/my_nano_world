﻿using System;
using Assets.NanoLib.UtilitesEngine.CustomDebugging.Models.api;
using NanoReality.GameLogic.PlayerProfile.Models.Api;
using UnityEngine;
using NanoReality.GameLogic.SocialCommunicator.api;
using strange.extensions.signal.impl;
using UnityEngine.SocialPlatforms;

public class GameCenterSocialNetworkCommunicator : ISocialNetworkCommunicator
{
    private const string GameCenterServicesAuthorizedKey = "GameCenterServicesAuthorized";
    
    #region Fields

    public Signal<ISocialNetworkCommunicator> OnInitializedSignal { get; private set; }

    public ILocalUser ActiveProfile
    {
        get { return Social.Active.localUser; }
    }

    public string SocialToken { get { return ""; } }

    /// <summary>
    /// Кешируем список друзей, что б каждый раз не выкачивать их с соц. сети
    /// </summary>
    private IUserProfile[] _cachedFriendsList;

    /// <summary>
    /// 
    /// </summary>
    private IAchievement[] _cachedAchievementsList;

    public bool IsUserAuthorized { get { return Social.Active.localUser.authenticated; } }
    public string SocialNetworkName { get { return "game_center"; } }
    public SocialNetworkName SocialNetworkType { get { return NanoReality.GameLogic.SocialCommunicator.api.SocialNetworkName.GameCenter; } }

    #endregion

    #region Injections

    [Inject]
    public IDebug Debug { get; set; }
    
    [Inject]
    public IPlayerProfile jPlayerProfile { get; set; }

    #endregion

    #region Signals

    [Inject]
    public SignalOnAuthorizeSocialProfile jSignalOnAuthorizeSocialProfile { get; set; }

    #endregion

    public GameCenterSocialNetworkCommunicator()
    {
        OnInitializedSignal = new Signal<ISocialNetworkCommunicator>();
    }
    
    #region Methods

    public void Init()
    {
        Debug.Log("Initializing Game Center plugin");
        if (PlayerPrefs.HasKey(GameCenterServicesAuthorizedKey) && 
            jPlayerProfile.SocialAccounts.ContainsKey(SocialNetworkName))
        {
            Social.localUser.Authenticate(successAuthorize =>
            {
                Debug.Log("Game Center Autologin result: " + successAuthorize);
                Debug.Log("Game Center ActiveProfile: " + (ActiveProfile == null ? "null" : ActiveProfile.userName), true);
                OnInitializedSignal.Dispatch(this);
            });
        }
        else
        {
            OnInitializedSignal.Dispatch(this);
        }
    }

    public void Authorize(Action<AuthorizeResult> callback = null)
    {
        Social.localUser.Authenticate(successAuthorize =>
        {
            if (successAuthorize)
            {
                PlayerPrefs.SetInt(GameCenterServicesAuthorizedKey, 1);
                PlayerPrefs.Save();
            }

            Debug.Log(ActiveProfile.image, true);
            var authorizeResult = new AuthorizeResult(successAuthorize, Social.localUser, this);
            if (callback != null) callback(authorizeResult);
            jSignalOnAuthorizeSocialProfile.Dispatch(authorizeResult);
        });
    }

    public void Logout(Action<bool, ISocialNetworkCommunicator> callback = null)
    {
        Debug.Log("Game Center logout");
        // Game center services not support logout, technically it must be reauthorizd each launch
        if (callback != null) callback(true, this);
    }

    public void LoadAchievementsList(Action<IAchievement[], ISocialNetworkCommunicator> callback = null)
    {
        if (Social.localUser.authenticated)
        {
            Social.LoadAchievements(achievements =>
            {
                if (achievements != null)
                {
                    _cachedAchievementsList = achievements;
                    Debug.Log("Loaded " + _cachedAchievementsList.Length + " achievements");
                        
                    if(callback != null)
                        callback(achievements, this);
                }

            });
            
            if (callback != null)
                callback(null, this);
        }

    }

    public void ReportAchivmentProgress(string achivmentId, double progress,
        Action<string, bool, ISocialNetworkCommunicator> callback = null)
    {
        Social.ReportProgress(achivmentId, progress, (result =>
        {
            if (callback != null)
                callback(achivmentId, result, this);
        }));
    }

    public void PostMessage(string message, Action<bool, ISocialNetworkCommunicator> callback = null)
    {
        if (callback != null) callback(false, this);
        throw new NotImplementedException("Game Center services not support posting");
    }

    public void PostMessage(string message, Texture2D texture2D,
        Action<bool, ISocialNetworkCommunicator> callback = null)
    {
        if (callback != null) callback(false, this);
        throw new NotImplementedException("Game Center services not support posting");
    }

    public void PostMessage(string message, string imgUrl, Action<bool, ISocialNetworkCommunicator> callback = null)
    {
        if (callback != null) callback(false, this);
        throw new NotImplementedException("Game Center services not support posting");
    }

    public void LoadUserFriends(Action<bool, IUserProfile[], ISocialNetworkCommunicator> callback = null)
    {
        if (Social.localUser.authenticated)
        {
            if (_cachedFriendsList != null)
            {
                if (callback != null)
                    callback(true, _cachedFriendsList, this);

                return;
            }


            Social.localUser.LoadFriends(success =>
            {
                if (success)
                {
                    _cachedFriendsList = Social.localUser.friends;
                    Debug.Log("Loaded " + _cachedFriendsList.Length + " friends");
                }
                else
                {
                    Debug.Log("Loading friends failed");
                }
            });
        }
        else
        {
            Debug.Log("User is not autentificated!");
        }
    }

    public void InviteFriends(string inivteMessage, Action<bool, ISocialNetworkCommunicator> callback = null)
    {
        throw new NotImplementedException("Game Center services not support inviting friends");
    }

    public void ResetAllAchievements(Action<bool, ISocialNetworkCommunicator> callback = null)
    {
    }

    #endregion
}