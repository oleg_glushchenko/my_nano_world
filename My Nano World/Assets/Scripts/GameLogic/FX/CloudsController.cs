﻿using UnityEngine;
using Assets.NanoLib.UtilitesEngine;
using NanoReality.GameLogic.GameManager.Controllers;
using strange.extensions.mediation.impl;

namespace NanoReality.GameLogic.FX
{
	[RequireComponent(typeof(MeshRenderer))]
	public class CloudsController : View
	{
		#region Injects
		
		[Inject]
		public SignalOnFinishLoadCityMapSequence jSignalOnFinishLoadCityMapSequence { get; set; }

		[Inject]
		public ITimerManager jTimerManager { get; set; }

		#endregion


		[SerializeField]
		private Camera cam;

		[SerializeField]
		private Vector2 cloudsSpeedMax;

		[SerializeField]
		private float cloudsSpeedChangeTime = 100.0f;

		private Vector2 cloudsSpeed = Vector2.zero;

		private Vector2 lastCloudsSpeed = Vector2.zero;

		private Vector4 cloudOffset = Vector4.zero;
		private Vector4 cloudMaskOffset = Vector4.zero;


		[SerializeField]
		private Vector4 mapCamSizeToTransp;

		[SerializeField]
		private Vector2 mapCamSizeToCloudFill;

		[SerializeField]
		private Vector2 mapCamSizeToCloudFillRain;

		private Vector2 SizeToCloud;

		[SerializeField]
		private float fillRandomize = 0.3f;

		[SerializeField]
		private float fillRandomizeTime = 20.0f;
		private float fillOffset = 0f;
		private float lastfillOffset = 0f;

		private float fillTimer = 20.0f;

		[SerializeField]
		private bool manualRainStart = false;

		private bool isRaining = false;

		[SerializeField]
		private Color cloudsColor;
		[SerializeField]
		private Color cloudsColorRain;
		private Color currCloudsColor;

		[SerializeField]
		private float cloudsContrast;
		[SerializeField]
		private float cloudsContrastRain;
		private float currCloudsContrast;

		[SerializeField]
		private Vector2 rainPower = new Vector2(4.0f,7f);

		private float currRainPower = 0f;

		[SerializeField]
		private float rainMinTime = 50.0f;

		[SerializeField]
		private float rainMaxTime = 70.0f;

		private float currRainLength = 0f;

		private float rainTimer = 0f;

		[SerializeField]
		private Vector2 nextLighteningTimeInterval = new Vector2(5f, 20f);

		[SerializeField]
		private Vector2 lighteningGrowFadeTime = new Vector2(0.1f, 0.2f);

		[SerializeField]
		private Vector2 lighteningColorMul = new Vector2(1.71f, 8f);

		[SerializeField]
		private float lighteningMultipleShootsTimeChance = 0.3f;

		private float nextLighteningTime = -1f;
		//private float nextLighteningSoundTime = -1f;

		[SerializeField]
		private AudioSource[] thunderAudioSource;

		private int currentThunder = 0;

		private bool isLighteningShoot = false;
		private int lighteningSteakCount = 0;

		[SerializeField]
		private float cloudsGatheringTime = 10.0f;

		[SerializeField]
		private float rainingDelayTime = 5.0f;

		[SerializeField]
		private float rainingIncDecTime = 10.0f;

		private int cloudDirChange = 0;

		[SerializeField] private float _rainTimerIntervalMin = 100f;	//600f;
		[SerializeField] private float _startRainTimerIntervalMax = 200f;	//1200f;

		[SerializeField]
		private float _rainChanceOnTimer = 0.8f;

		[SerializeField]
		private float _rainChanceOnSectorChange = 0.8f;

		private float _lastRainignTime;

		private ITimer _rainStartTimer;

		[SerializeField]
		private AudioSource rainSource;

		[SerializeField]
		private Vector2 mapCamSizeToRainVolume;

		[SerializeField]
		private Vector2 mapCamSizeToRainPitch;

		[SerializeField]
		private float pitchRandomize = 0.08f;

		[SerializeField]
		private float pitchRandomizeTime = 3f;

		private float pitchRandomizeTimer = 3f;
		private float lastPitch = 1f;
		private float targPitch = 1f;
    
		private Material _material;

		private void nextLightening(float firstTimeDisp)
		{
			nextLighteningTime = Time.time + Random.Range(nextLighteningTimeInterval.x, nextLighteningTimeInterval.y) + firstTimeDisp;
			if (thunderAudioSource.Length > 0)
			{
				currentThunder = (currentThunder + 1) % thunderAudioSource.Length;
				thunderAudioSource[currentThunder].PlayDelayed(cam.orthographicSize / 15);
			}
			else Debug.LogError("No thunder sounds added!");
			//nextLighteningSoundTime = nextLighteningTime + 1f;
		}

    
		private void newCloudsSpeed()
		{
			lastCloudsSpeed = cloudsSpeed;
			cloudsSpeed = new Vector2(Random.Range(-cloudsSpeedMax.x, cloudsSpeedMax.x), Random.Range(-cloudsSpeedMax.y, cloudsSpeedMax.y));
		}

		private void AddListeners()
		{
			jSignalOnFinishLoadCityMapSequence.AddListener(data =>
			{
				if (isRaining)
				{
					return;
				}

				StartRainWithChance(_rainChanceOnSectorChange);

				if (isRaining)
				{
					Debug.Log(string.Format("Starting rain from sector change ({0})", Time.time));
				}
			});
		}

		/// <param name="chance">0f to 1f chance to start rain</param>
		private void StartRainWithChance(float chance)
		{
			if (Random.value <= chance)
			{
				StartRainFromTime(0f);
				_rainStartTimer.CancelTimer();
				_rainStartTimer = null;
			}
			else
			{
				StartRainTimer();
			}
		}

		private void StartRainTimer()
		{
			if (_rainStartTimer != null)
			{
				_rainStartTimer.CancelTimer();
				_rainStartTimer = jTimerManager.StartServerTimer(Random.Range(_rainTimerIntervalMin, _startRainTimerIntervalMax), OnRainTimerElapsed, null);
				return;
			}

			_rainStartTimer = jTimerManager.StartServerTimer(Random.Range(_rainTimerIntervalMin, _startRainTimerIntervalMax), OnRainTimerElapsed, null);
		}

		//After rain timer elapsed, do random roll on either starting rain or   
		private void OnRainTimerElapsed()
		{
			if (isRaining || Time.time - _lastRainignTime < _rainTimerIntervalMin)
			{
				StartRainTimer();
				return;
			}

			StartRainWithChance(_rainChanceOnTimer);

			if (isRaining)
			{
				Debug.Log(string.Format("Starting rain from timer ({0})", Time.time));
			}
		}

		private void newPitch()
		{
			pitchRandomizeTimer = pitchRandomizeTime;
			lastPitch = targPitch;
			targPitch = Random.Range(-pitchRandomize, pitchRandomize);
		}


		private void newFill()
		{
			fillTimer = fillRandomizeTime;
			lastfillOffset = fillOffset;
			fillOffset = Random.Range(-fillRandomize, fillRandomize); 
		}

		public void StartRainFromTime(float percent)
		{
			Shader.EnableKeyword("RAIN_ON");

			nextLightening(rainingIncDecTime);

			isRaining = true;
			currRainLength = Random.Range(rainMinTime, rainMaxTime);
			rainTimer = currRainLength*(100f- percent)/100f;
		
			_lastRainignTime = Time.time + currRainLength;

			rainSource.Play();

		}

		private void StopRain()
		{
			isRaining = false;
			Shader.DisableKeyword("RAIN_ON");
			currRainLength = -1f;

			StartRainTimer();
			rainSource.Stop();

		}

		protected override void Start ()
		{
			base.Start();

			var meshRenderer = GetComponent<MeshRenderer>();
			_material = new Material(meshRenderer.sharedMaterial);
			meshRenderer.material = _material;

			jSignalOnFinishLoadCityMapSequence.AddOnce(data =>
			{
				Shader.DisableKeyword("RAIN_ON");
				newFill();
				newCloudsSpeed();

				AddListeners();
				StartRainTimer();
			});
		}

		// Update is called once per frame
		void Update()
		{
			if (manualRainStart)
			{
				StartRainFromTime(0f);
				manualRainStart = false;
			}

			if (Time.time >= nextLighteningTime && !isLighteningShoot && rainTimer > rainingIncDecTime )
			{
				isLighteningShoot = true;
				lighteningSteakCount = 1;
				if (Random.value <= lighteningMultipleShootsTimeChance)
					lighteningSteakCount++;
				if (Random.value <= lighteningMultipleShootsTimeChance)
					lighteningSteakCount++;
			}

			if (isLighteningShoot)
			{
				float colorMul = 0f;
				if (Time.time <= nextLighteningTime + lighteningGrowFadeTime.x)
					colorMul = Mathf.Lerp(lighteningColorMul.x, lighteningColorMul.y, (Time.time - nextLighteningTime) / lighteningGrowFadeTime.x);
				else if (Time.time <= nextLighteningTime + lighteningGrowFadeTime.x + lighteningGrowFadeTime.y) {
					colorMul = Mathf.Lerp(lighteningColorMul.y, lighteningColorMul.x, (Time.time - nextLighteningTime - lighteningGrowFadeTime.x ) / lighteningGrowFadeTime.y);
					if (lighteningSteakCount>1 && Time.time >= nextLighteningTime + lighteningGrowFadeTime.x + Random.Range(lighteningGrowFadeTime.x, lighteningGrowFadeTime.y))
					{
						lighteningSteakCount--;
						nextLighteningTime = Time.time - lighteningGrowFadeTime.x/2f;
					}

				} else
				{
					// if (Random.value <= lighteningMultipleShootsTimeChance)
					//     nextLighteningTime = Time.time;
					//  else {
					isLighteningShoot = false;
					colorMul = lighteningColorMul.x;
					nextLightening(0f);
					// }
				}

				_material.SetFloat("_Multiplier", colorMul);

			}


			float timeDiv60 = Time.time / cloudsSpeedChangeTime;

			if (Mathf.FloorToInt(timeDiv60) != cloudDirChange)
			{
				newCloudsSpeed();
				cloudDirChange = Mathf.FloorToInt(timeDiv60);
			}
        
			Vector2 currCloudsSpeed = Vector2.Lerp(lastCloudsSpeed, cloudsSpeed, timeDiv60 - Mathf.Floor(timeDiv60));

			cloudMaskOffset.x += currCloudsSpeed.x * Time.deltaTime;         
			cloudMaskOffset.y += currCloudsSpeed.y * Time.deltaTime;
			cloudMaskOffset.z = currCloudsSpeed.x * 10f;
			cloudMaskOffset.w = currCloudsSpeed.y * 10f;

			cloudOffset.x += currCloudsSpeed.x * Time.deltaTime * 0.66f;
			cloudOffset.y += currCloudsSpeed.y * Time.deltaTime * 0.66f;
			cloudOffset.z += currCloudsSpeed.x * Time.deltaTime * 0.33f;
			cloudOffset.w += currCloudsSpeed.y * Time.deltaTime * 0.33f;
        
			/*Vector4 _MaskSlide = _material.GetVector("_MaskSlide");
        _MaskSlide.x = currCloudsSpeed.x;
        _MaskSlide.y = currCloudsSpeed.y;
        _material.SetVector("_MaskSlide", _MaskSlide);
        _MaskSlide.x = currCloudsSpeed.x / 2f;
        _MaskSlide.y = currCloudsSpeed.y / 2f;
        _MaskSlide.z = currCloudsSpeed.x;
        _MaskSlide.w = currCloudsSpeed.y;
        _material.SetVector("_TimeSlide", _MaskSlide);*/

			_material.SetVector("_MaskSlide", cloudMaskOffset);
			_material.SetVector("_TimeSlide", cloudOffset);


			if (!isRaining)
			{
				SizeToCloud = mapCamSizeToCloudFill;
				currCloudsContrast = cloudsContrast;
				currCloudsColor = cloudsColor;
				rainSource.volume = 0f;
				foreach (AudioSource th in thunderAudioSource)
					th.volume = 0f;
			}
			else
			{
				rainTimer -= Time.deltaTime;

				if (rainTimer < 0f)
				{
					StopRain();
				}
				else
				{
					float a = rainTimer > cloudsGatheringTime ? 
						(currRainLength - rainTimer) / cloudsGatheringTime :
						rainTimer / cloudsGatheringTime;
                                        
					SizeToCloud = Vector2.Lerp(mapCamSizeToCloudFill, mapCamSizeToCloudFillRain, a);
					currCloudsColor = Color.Lerp(cloudsColor, cloudsColorRain, a);
					currCloudsContrast = Mathf.Lerp(cloudsContrast, cloudsContrastRain, a);

					a = rainTimer > rainingDelayTime + rainingIncDecTime ?
						(currRainLength - rainTimer - rainingDelayTime) / (rainingIncDecTime) :
						(rainTimer - rainingDelayTime) / rainingIncDecTime;

					currRainPower = Mathf.Lerp(0, Remap(cam.orthographicSize, mapCamSizeToTransp.x, mapCamSizeToTransp.y, rainPower.x, rainPower.y), a);

					a = rainTimer > rainingDelayTime + 5f + rainingIncDecTime ?
						(currRainLength - rainTimer - rainingDelayTime - 5f) / (rainingIncDecTime) :
						(rainTimer - rainingDelayTime - 5f) / rainingIncDecTime;

					rainSource.volume = Mathf.Lerp(0f, Remap(cam.orthographicSize, mapCamSizeToTransp.x, mapCamSizeToTransp.y, mapCamSizeToRainVolume.x, mapCamSizeToRainVolume.y), a);
					float camPitch = Remap(cam.orthographicSize, mapCamSizeToTransp.x, mapCamSizeToTransp.y, mapCamSizeToRainPitch.x, mapCamSizeToRainPitch.y);
					rainSource.pitch = Mathf.Lerp(camPitch + lastPitch, camPitch + targPitch, (pitchRandomizeTime - pitchRandomizeTimer) / pitchRandomizeTime);

					foreach (AudioSource th in thunderAudioSource)
					{
						th.volume = rainSource.volume;
						th.pitch = rainSource.pitch;
					}


					Vector4 rain = _material.GetVector("_Rain");
					rain.x = currRainPower;
					_material.SetVector("_Rain", rain);
					rainSource.pitch = Mathf.Lerp(1f + lastPitch, 1f + targPitch, (pitchRandomizeTime - pitchRandomizeTimer) / pitchRandomizeTime);

				}
			}

			if (fillTimer < 0f)
				newFill();

			fillTimer -= Time.deltaTime;

			if (pitchRandomizeTimer < 0f)
				newPitch();

			pitchRandomizeTimer -= Time.deltaTime;


			float transp = Remap(cam.orthographicSize, mapCamSizeToTransp.x, mapCamSizeToTransp.y, mapCamSizeToTransp.z, mapCamSizeToTransp.w);
			transp = Mathf.Clamp(transp, mapCamSizeToTransp.z, mapCamSizeToTransp.w);
			_material.SetFloat("_CloudAlpha", transp);

			float fill = Remap(cam.orthographicSize, mapCamSizeToTransp.x, mapCamSizeToTransp.y, SizeToCloud.x, SizeToCloud.y);
			fill = Mathf.Clamp(fill, SizeToCloud.x, SizeToCloud.y);
			Vector4 covering = _material.GetVector("_Covering");
			covering.x = Mathf.Lerp(lastfillOffset, fillOffset,(fillRandomizeTime - fillTimer)/fillRandomizeTime);
			_material.SetFloat("_RainFill", covering.x + SizeToCloud.y);
			covering.x += fill;
			_material.SetVector("_Covering", covering);
			_material.SetFloat("_CloudContrast", currCloudsContrast);
			_material.SetColor("_CloudsColor", currCloudsColor);
        
		}

		private float Remap(float value, float from1, float to1, float from2, float to2)
		{
			return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
		}

	}
}