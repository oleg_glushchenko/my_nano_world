﻿using System.Collections.Generic;
using Assets.NanoLib.UtilitesEngine.CustomDebugging.Models.api;
using NanoReality.GameLogic.BuildingSystem.City.Models.Api;
using NanoReality.StrangeIoC;
using strange.extensions.signal.impl;
using UnityEngine;

namespace NanoReality.GameLogic.FX
{
    public class SignalShowMapClouds : Signal<BusinessSectorsTypes> { }
    public class SignalRemoveMapClouds : Signal<BusinessSectorsTypes> { }

    public class MapCloudsControlller : AViewMediator
    {
        [SerializeField]
        private List<ParticleSystem> _farmParticleSystems;
        [SerializeField]
        private List<ParticleSystem> _heavyIndustryParticleSystems;

        [SerializeField]
        [Range(0.1f, 100f)]
        private float _removingSpeed = 25f;
        
        private readonly HashSet<BusinessSectorsTypes> _shownClouds = new HashSet<BusinessSectorsTypes>();

        #region Injects
        
        [Inject]
        public IDebug jDebug { get; set; }

        [Inject]
        public SignalShowMapClouds SignalShowMapClouds { get; set; }

        [Inject]
        public SignalRemoveMapClouds SignalRemoveMapClouds { get; set; }

        #endregion

        #region Overrides of AViewMediator

        protected override void PreRegister()
        {
            Clean(_farmParticleSystems);
            Clean(_heavyIndustryParticleSystems);
        }

        protected override void OnRegister()
        {
            SignalShowMapClouds.AddListener(OnShowMapClouds);
            SignalRemoveMapClouds.AddListener(OnRemoveMapClouds);
        }

        protected override void OnRemove()
        {
            SignalShowMapClouds.AddListener(OnShowMapClouds);
            SignalRemoveMapClouds.AddListener(OnRemoveMapClouds);
        }

        #endregion

        private void Clean(List<ParticleSystem> particleSystems)
        {
            for (int i = 0; i < particleSystems.Count; i++)
            {
                var ps = particleSystems[i];
                ps.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
            }
        }

        private List<ParticleSystem> GetParticleSystems(BusinessSectorsTypes sectorType)
        {
            switch (sectorType)
            {
                case BusinessSectorsTypes.Farm:
                    return _farmParticleSystems;
                case BusinessSectorsTypes.HeavyIndustry:
                    return _heavyIndustryParticleSystems;
                default:
                    return null;
            }
        }

        private void OnShowMapClouds(BusinessSectorsTypes sectorType)
        {
            if (_shownClouds.Contains(sectorType)) return;
            
            var particleSystems = GetParticleSystems(sectorType);

            if (particleSystems == null)
            {
                return;
            }

            for (int i = 0; i < particleSystems.Count; i++)
            {
                var ps = particleSystems[i];
                var main = ps.main;
                main.simulationSpeed = 1f;
                ps.Simulate(main.duration, true);
                ps.Play(true);
            }
            
            jDebug.Log("<b>SHOW MAP CLOUDS:</b> " + sectorType, true);
            
            _shownClouds.Add(sectorType);
        }

        private void OnRemoveMapClouds(BusinessSectorsTypes sectorType)
        {
            if (!_shownClouds.Contains(sectorType)) return;
            
            var particleSystems = GetParticleSystems(sectorType);

            if (particleSystems == null)
            {
                return;
            }

            for (int i = 0; i < particleSystems.Count; i++)
            {
                var ps = particleSystems[i];
                var main = ps.main;
                main.simulationSpeed = _removingSpeed;
                ps.Stop(true);
            }
            
            jDebug.Log("<b>HIDE MAP CLOUDS:</b> " + sectorType, true);

            _shownClouds.Remove(sectorType);
        }
    }
}
