﻿using System;
using DG.Tweening;
using NanoLib.Core.Services.Sound;
using strange.extensions.mediation.impl;
using UniRx;
using UnityEngine;
using Random = UnityEngine.Random;

namespace NanoReality.GameLogic.FX
{
    public class BirdsSinging : View
    {
        [SerializeField] private Vector4 mapCamSizeToVolume;
        [SerializeField] private SfxSoundTypes[] birdsSounds;

        [SerializeField] private float minNextTrackTime;

        [SerializeField] private float maxNextTrackTime;

        private Camera _camera;

        private float timer;
        
        [Inject] public ISoundManager jSoundManager { get; set; }

        protected override void Start()
        {
            base.Start();
            _camera = Camera.main;
            //PlayBirdsSinging();
        }

        private void PlayBirdsSinging()
        {
            timer = Random.Range(minNextTrackTime, maxNextTrackTime);
            SfxSoundTypes randomBirdsAudioClip =  birdsSounds[Random.Range(0, birdsSounds.Length)];

            jSoundManager.Play(randomBirdsAudioClip, SoundChannel.SoundFX, false, (s) =>
            {
                DOVirtual.DelayedCall(Random.Range(3, 10), PlayBirdsSinging);
            });
        }

        // private void Update()
        // {
        //     timer -= Time.deltaTime;
        //     if (timer < 0f)
        //         NewTime();
        //     // TODO: need re-implement it.
        //     // source.volume = Remap(_camera.orthographicSize, mapCamSizeToVolume.x, mapCamSizeToVolume.y,
        //     //     mapCamSizeToVolume.z, mapCamSizeToVolume.w);
        // }

        private float Remap(float value, float from1, float to1, float from2, float to2)
        {
            return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
        }
    }
}