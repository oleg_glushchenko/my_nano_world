﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

[Serializable]
public class ProductsToUnlock : IProductsToUnlock
{
    [JsonProperty("products_both")]
    public List<IProductsToUnlockItem> Products { get; set; }

}
