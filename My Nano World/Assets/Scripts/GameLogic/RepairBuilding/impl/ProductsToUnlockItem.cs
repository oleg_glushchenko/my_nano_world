﻿using System;
using Assets.NanoLib.Utilities;
using NanoReality.GameLogic.BuildingSystem.Products.Models.Api;
using Newtonsoft.Json;

[Serializable]
public class ProductsToUnlockItem : IProductsToUnlockItem
{
    [JsonProperty("product_id")]
    public Id<Product> ProductID { get; set; }

    [JsonProperty("count")]
    public int Count { get; set; }
}