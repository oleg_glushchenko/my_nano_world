﻿using System.Collections.Generic;

public interface IProductsToUnlock
{
    List<IProductsToUnlockItem> Products { get; set; }
}