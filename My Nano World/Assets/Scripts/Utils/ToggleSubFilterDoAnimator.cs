﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class ToggleSubFilterDoAnimator : MonoBehaviour
{
    [Serializable]
    public class SubFilterAnimationTargets
    {
        public Vector2 ActiveStateSize = new Vector2();
        public Vector2 InactiveStateSize = new Vector2();
        public Vector2 ActiveStatePosition = new Vector2();
        public Vector2 InactiveStatePosition = new Vector2();
        public RectTransform TargetRect;
        public float Delay = 0.0f;
        public float Duration = 0.7f;
        public Ease Ease = Ease.OutElastic;
    }

    [SerializeField] private List<SubFilterAnimationTargets> _elements;
    [SerializeField] private Toggle _toggle;


    public void StateChanged()
    {
        if (_toggle.isOn)
        {
            OpenSequences();
        }
        else
        {
            CloseSequences();
        }
    }

    private void OpenSequences()
    {
        foreach (SubFilterAnimationTargets element in _elements)
        {
            element.TargetRect.DOAnchorPos(element.ActiveStatePosition, element.Duration).SetEase(element.Ease);
            element.TargetRect.DOSizeDelta(element.ActiveStateSize, element.Duration).SetEase(element.Ease)
                .OnUpdate(UpdateLayout);
        }
    }

    private void CloseSequences()
    {
        foreach (SubFilterAnimationTargets element in _elements)
        {
            element.TargetRect.DOAnchorPos(element.InactiveStatePosition, element.Duration).SetEase(element.Ease);
            element.TargetRect.DOSizeDelta(element.InactiveStateSize, element.Duration).SetEase(element.Ease)
                .OnUpdate(UpdateLayout);
        }
    }

    private void UpdateLayout()
    {
        LayoutRebuilder.ForceRebuildLayoutImmediate(transform.parent as RectTransform);
    }
}