﻿using Engine.UI.Components;
using NanoLib.Core.Logging;
using UnityEngine;
using UnityEngine.UI;

namespace NanoReality.UI.Components
{
	public sealed class ResizableProgressBar : ProgressBar
	{
		[SerializeField] private Image _image;
		[SerializeField] private float _fullWidth;
		[SerializeField] private float _emptyWidth;
		[SerializeField] private float _currantValue;

		[SerializeField] private float _lowValueIs = 0.33f;
		[SerializeField] private float _hightValueIs = 0.66f;
		[SerializeField] private bool _swapSpritesOnChange;

		[SerializeField] private Sprite _lowValueSprite;
		[SerializeField] private Sprite _normalValueSprite;
		[SerializeField] private Sprite _hightValueSprite;

		public override float Value
		{
			get => _currantValue;
			set
			{
				_currantValue = value;

				if (!_image)
				{
					Logging.LogError("_image == null");
				}

				RectTransform rectTransform = _image.rectTransform;
				rectTransform.sizeDelta =
					new Vector2(Mathf.Lerp(_emptyWidth, _fullWidth, _currantValue), rectTransform.sizeDelta.y);
				InvokeProgressChanged();

				if (_swapSpritesOnChange)
				{
					SwapSprites();
				}
			}
		}

		private void SwapSprites()
		{
			if (_currantValue <= _lowValueIs && _lowValueSprite)
			{
				_image.sprite = _lowValueSprite;
			}

			if (_currantValue >= _hightValueIs && _hightValueSprite)
			{
				_image.sprite = _hightValueSprite;
			}

			if (_currantValue > _lowValueIs && _currantValue < _hightValueIs && _normalValueSprite)
			{
				_image.sprite = _normalValueSprite;
			}
		}
	}
}